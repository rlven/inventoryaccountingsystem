﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Logger.Models
{
    public class GameResultLog
    {
        public int UserId { get; set; }
        public int GameId { get; set; }
        public int MaxWincoins { get; set; }
        public int WincoinsExchange { get; set; }
        public int SessionsWincoinsSum { get; set; }
        public int CurrentSessionWincoins { get; set; }
        public int TotalWincoinsSum { get; set; }
        public int AddedWincoins { get; set; }
    }
}
