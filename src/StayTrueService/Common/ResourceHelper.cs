﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Resources;
using Common.Models;

namespace Common
{
    public static class ResourceHelper<T> where T : BaseAnswer, new()
    {
        public static T CreateLocalizedAnswer(string message)
        {
            return new T
            {
                ErrorMessage = message,
                LocalizedMessage = LocalizeEntry(message)
            };
        }

        public static T CreateLocalizedAnswer(string format, string[] strings)
        {
            var localized = strings.Aggregate(format, (current, s) => current.Replace(s, LocalizeEntry(s)));
            return new T
            {
                ErrorMessage = format,
                LocalizedMessage = localized
            };
        }

        private static string LocalizeEntry(string entry)
        {
            try
            {
                var manager = new ResourceManager(typeof(Resources));
                var set = manager.GetResourceSet(CultureInfo.InvariantCulture, true, true);
                var byValue = set.Cast<DictionaryEntry>().ToDictionary(x => x.Value.ToString(), x => x.Key.ToString());
                string key;
                var localizedMessage =
                    manager.GetString(!byValue.TryGetValue(entry, out key) ? byValue[Resources.Unauthorized] : key,
                        CultureInfo.GetCultureInfo("ru-RU"));
                return localizedMessage;
            }
            catch (Exception)
            {
                //TODO
                //LogManager.GetCurrentClassLogger().ErrorException("Resource manager", exc);
                return entry;
            }
        }
    }
}
