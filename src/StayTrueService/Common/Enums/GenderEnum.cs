﻿using System;

namespace Common.Enums
{
    [Serializable]
    public enum GenderEnum
    {
        Male = 1,
        Female = 2
    }
}
