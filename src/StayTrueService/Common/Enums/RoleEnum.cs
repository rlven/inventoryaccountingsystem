﻿using System;

namespace Common.Enums
{
    [Serializable]
    public enum RoleEnum
    {
        Admin = 1,
        SuperAdmin = 2,
        User = 3
    }
}
