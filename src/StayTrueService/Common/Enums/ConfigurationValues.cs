﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public static class ConfigurationValues
    {
        public static readonly string Brand = "Brand";
        public static readonly string LicenseRu = "TermOfUseRu";
        public static readonly string LicenseKg = "TermOfUseKg";
        public static readonly string RulesRu = "RulesOfUseRu";
        public static readonly string RulesKg = "RulesOfUsekg";
        
    }
}
