﻿using System;

namespace Common.Enums
{
    [Serializable]
    public enum StatusEnum
    {
        Active = 1,
        Blocked = 2,
        NotActive = 3
    }
}
