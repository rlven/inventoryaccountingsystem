﻿namespace Common.Enums
{
    public enum NewsCategoryEnum
    {
        FreedomMusic = 1,
        DestinationFreedom = 2,
        News = 3
    }
}