﻿using System;
using System.Runtime.Serialization;

namespace Common.ChatModels
{
    [Serializable]
    [DataContract]
    public class AccessTokenRequestModel
    {
        [DataMember(Name = "phone")]
        public string PhoneNumber { get; set; }
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }
    }

    [Serializable]
    [DataContract]
    public class AccessTokenResponseModel
    {
        [DataMember(Name = "success")]
        public bool Success { get; set; }
        [DataMember(Name = "error")]
        public string Error { get; set; }
    }
}
