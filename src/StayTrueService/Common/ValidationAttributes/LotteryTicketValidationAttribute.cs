﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ValidationAttributes
{
    public class LotteryTicketValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            BuyLotteryTicket ticket = value as BuyLotteryTicket;
            if (ticket.TicketsCount <= 0)
                return false;
            return true;
        }
    }
}
