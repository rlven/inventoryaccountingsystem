﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Answer
    {
        [DataMember]
        public string Text { get; set; }
    }
}
