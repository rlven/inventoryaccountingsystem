﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class ShopHistoryforAdmin
    {
        [DataMember]
        public int ShopHistoryId { set; get; }
        [DataMember]
        public User User { set; get; }
        [DataMember]
        public Product Product { set; get; }
        [DataMember]
        public int ShopId { set; get; }
        [DataMember]
        public int Amount { get; set; }
        [DataMember]
        public int IssuedBy { get; set; }
        [DataMember]
        public bool IsIssued { set; get; }
        [DataMember]
        public Admin Admin { set; get; }
        [DataMember]
        public DateTime? Date { set; get; }
        [DataMember]
        public int WhereIssued { set; get; }
    }
}
