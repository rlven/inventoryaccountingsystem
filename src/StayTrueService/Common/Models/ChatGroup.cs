﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]

    public class ChatGroup
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public String Image { get; set; }
        [DataMember]
        public Boolean IsPrivate { get; set; }
    }
}
