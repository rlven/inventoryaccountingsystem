﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class AlbumImage
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int CategoryId { set; get; }
        [DataMember]
        public string Image { set; get; }
        [DataMember]
        public string SmallImage { get; set; }
        [DataMember]
        public string Video { get; set; }
        [DataMember]
        public bool IsBrand { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }
}
