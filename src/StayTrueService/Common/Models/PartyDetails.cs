﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class PartyDetails : BaseAnswer
    {
        [DataMember]
        public List<PartyQuestion> Questions { get; set; }
        [DataMember]
        public string ImagePreview { get; set; }
        [DataMember]
        public string ImagePreviewSmall { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DescriptionForMobile { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<PartyGames> PartyGames { get; set; }
        [DataMember]
        public string RegistrationBegin { get; set; }
        [DataMember]
        public string EndDate { get; set; }
        [DataMember]
        public bool IsAnswered { get; set; }
        [DataMember]
        public List<PartyQuizzes> PartyQuizzes { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsFinished { get; set; }
        [DataMember]
        public bool IsInvited { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        [DataMember]
        public bool IsBlocked { get; set; }
    }
    [Serializable]
    [DataContract]
    public class PartyGames : BaseAnswer
    {
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string GameUrl { get; set; }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Points { get; set; }
        [DataMember]
        public DateTime? PublicationDate { get; set; }


    }
    [Serializable]
    [DataContract]
    public class PartyQuizzes : BaseAnswer
    {
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Points { get; set; }
        [DataMember]
        public DateTime? PublicationDate { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
    [Serializable]
    [DataContract]
    public class PartyQuestion : BaseAnswer
    {
        [DataMember]
        public string Question { get; set; }
    }
}
