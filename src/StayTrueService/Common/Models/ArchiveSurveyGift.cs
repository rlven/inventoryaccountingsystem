﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class ArchiveSurveyGift
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int SurveyGiftsHistoryId { set; get; }
        [DataMember]
        public DateTime? Date { set; get; }
    }
}