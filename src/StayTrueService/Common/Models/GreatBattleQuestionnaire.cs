﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleQuestionnaire : BaseAnswer
    {
        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public string Question1 { get; set; }

        [DataMember]
        public string Question2 { get; set; }

        [DataMember]
        public string Question3 { get; set; }

        [DataMember]
        public string Question4 { get; set; }
    }
}
