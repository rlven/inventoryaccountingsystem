﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class PartyUser : BaseAnswer
    {
        public int? userId;
        public string firstName;
        public string lastName;
        public string phone;

        [DataMember]
        public int? UserId
        {
            get
            {
                if (userId == null)
                {
                    userId = 0;
                }

                return userId;
            }
            set { userId = value;  }
        }
        [DataMember]
        public bool IsInvited { get; set; }
        [DataMember]
        public bool HasCome { get; set; }
        [DataMember]
        public bool IsError { get; set; }
        [DataMember]
        public string FirstName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(firstName))
                {
                    firstName = nameof(FirstName);
                }

                return firstName;
            }
            set { firstName = value; }
        }
        [DataMember]
        public string LastName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(lastName))
                {
                    lastName = nameof(LastName);
                }

                return lastName;
            }
            set { lastName = value; }
        }
        [DataMember]
        public string Phone
        {
            get
            {
                if (String.IsNullOrWhiteSpace(phone))
                {
                    phone = nameof(Phone);
                }

                return phone;
            }
            set { phone = value; }
        }
        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string Passport { get; set; }
    }
}
