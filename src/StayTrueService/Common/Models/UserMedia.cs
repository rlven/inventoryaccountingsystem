﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class UserMedia
    {
        [IgnoreDataMember]
        public int UserId { get; set; }
        [DataMember]
        public string PhoneHash { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string FrontSidePassport { get; set; }
        [DataMember]
        public string BackSidePassport { get; set; }
    }
}
