﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class SupportRequest
    {
        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public string Body { set; get; }

        [DataMember]
        public DateTime Date { set; get; }

        [DataMember]
        public bool Status { set; get; }

        [DataMember]
        public string PhoneNumber { set; get; }

        [DataMember]
        public string Email { set; get; }
    }
}
