﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class BestScore : BaseAnswer
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int GameId { set; get; }
        [DataMember]
        public int Score { set; get; }
        [DataMember]
        public DateTime LastLaunchDate { set; get; }
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public int Position { get; set; }
    }
}
