﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class PvpGameSessionScore
    {
        [DataMember]   
        public int GameId { get; set; }
        [DataMember]
        public string RoomId { get; set; }
        [DataMember]
        public int? UserId { get; set; }
        [DataMember]
        public string SessionStart { get; set; }
        [DataMember]
        public String SessionEnd { get; set; }
        [DataMember]
        public int Kills { get; set; }
        [DataMember]
        public int Death { get; set; }
        [DataMember]
        public int Score { get; set; }
        [DataMember]
        public int Bet { get; set; }
    }
}
