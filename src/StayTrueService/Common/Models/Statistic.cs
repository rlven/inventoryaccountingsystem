﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Statistic
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Amount { get; set; }
    }
}
