﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class PartyQuestionnaireAnswer : BaseAnswer
    {
        [DataMember]
        public PartyAnswers PartyAnswers { get; set; }
    }
    [Serializable]
    [DataContract]
    public class PartyAnswers
    {
        [DataMember]
        [Required]
        public string Answer1 { get; set; }
        [DataMember]
        [Required]
        public string Answer2 { get; set; }
        [DataMember]
        [Required]
        public string Answer3 { get; set; }
        [DataMember]
        [Required]
        public string Answer4 { get; set; }
        [DataMember]
        [Required]
        public string Answer5 { get; set; }
        [DataMember]
        [Required]
        public string Answer6 { get; set; }
        [DataMember]
        [Required]
        public string Answer7 { get; set; }
        [DataMember]
        [Required]
        public string Answer8 { get; set; }
        [DataMember]
        [Required]
        public string Answer9 { get; set; }
        [DataMember]
        [Required]
        public string Answer10 { get; set; }
    }
}
