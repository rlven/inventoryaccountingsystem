﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleUserRegistrationInfo : BaseAnswer
    {
        [DataMember]
        public DateTime? BattleBeginDate { get; set; }

        [DataMember]
        public DateTime? BattleEndDate { get; set; }

        [DataMember]
        public int BlueSidePoints { get; set; }

        [DataMember]
        public int RedSidePoints { get; set; }

        [DataMember]
        public Boolean IsRegistered { get; set; }

        [DataMember]
        public Boolean BattleIsFinished { get; set; }

        [DataMember]
        public int UserGroupId { get; set; }

        [DataMember]
        public int? WinnerGroupId { get; set; }

        [DataMember]
        public bool? IsAutofilled { get; set; }

        [DataMember]
        public bool IsBlocked { get; set; }

        [DataMember]
        public GreatBattleQuestionnaire Questionnaire { get; set; }
        [DataMember]
        public double BattleEndLeft { get; set; }
        [DataMember]
        public double BattleStartLeft { get; set; }
    }
}
