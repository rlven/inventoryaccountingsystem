﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Question
    {
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public String AnswerType { get; set; }
        [DataMember]
        public List<String> Answers { get; set; }
        [DataMember]
        public int SizeOfAvailableAnswers { get; set; }
    }
}
