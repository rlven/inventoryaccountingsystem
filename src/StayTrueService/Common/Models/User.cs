﻿using System;
using System.Runtime.Serialization;
namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class User
    {
        public int Id { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public System.DateTime BirthDate { get; set; }
        public System.DateTime RegistrationDate { get; set; }
        public Nullable<System.DateTime> RefreshDate { get; set; }
        [DataMember]
        public int CityId { get; set; }
        [DataMember]
        public int StatusId { get; set; }
        [DataMember]
        public int GenderId { get; set; }
        [DataMember]
        public int RoleId { get; set; }
        [DataMember]
        public int Wincoins { get; set; }
        [DataMember]
        public Nullable<System.DateTime> LastSignInM { get; set; }
        [DataMember]
        public Nullable<System.DateTime> LastSignInP { get; set; }
        public int SignInCounterM { get; set; }
        public int SignInCounterP { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsApproved { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string PassportFront { get; set; }
        [DataMember]
        public string PassportBack { get; set; }
        [DataMember]
        public string CigarettesFavorite { get; set; }
        [DataMember]
        public string CigarettesFavoriteType { get; set; }
        [DataMember]
        public string CigarettesAlternative { get; set; }
        [DataMember]
        public string CigarettesAlternativeType { get; set; }
        [DataMember]
        public string Captcha { get; set; }
        [DataMember]
        public int? Promocode { get; set; }
    }
    [Serializable]
    [DataContract]
    public class UserAuth
    {
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Captcha { get; set; }
    }
}

