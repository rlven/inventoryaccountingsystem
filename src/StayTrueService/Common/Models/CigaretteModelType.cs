﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class CigaretteModelType
    {
        [IgnoreDataMember]
        public int  Id { get; set; }
        [DataMember]
        public string TypeName { get; set; }
        [IgnoreDataMember]
        public int Position { get; set; }
        [IgnoreDataMember]
        public CigaretteModel CigaretteModel { get; set; }
    }
}
