﻿using System;
using System.Runtime.Serialization;
using Microsoft.SqlServer.Server;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class TermOfUse : BaseAnswer
    {
        [DataMember]
        public string TermOfUseRu { get; set; }
        [DataMember]
        public string TermOfUseKg { get; set; }
    }
}
