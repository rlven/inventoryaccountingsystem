﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class QuizScore : BaseAnswer
    {
        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public int? QuizId { get; set; }

        [DataMember]
        public int? UserId { get; set; }

        [DataMember]
        public string UserFullName { get; set; }
    }
}
