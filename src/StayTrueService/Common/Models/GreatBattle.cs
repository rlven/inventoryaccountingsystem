﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{

    [DataContract]
    [Serializable]
    public class GreatBattle : BaseAnswer
    {

        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public String Name { set; get; }

        [DataMember]
        public String Description { set; get; }

        [DataMember]
        public DateTime? CreationDate { get; set; }

        [DataMember]
        public DateTime? RegistrationBeginDate { get; set; }

        [DataMember]
        public DateTime? BattleBeginDate { get; set; }

        [DataMember]
        public Boolean IsRegistered { get; set; }

        [DataMember]
        public GreatBattleQuestionnaire Questionnaire { get; set; }

    }


}
