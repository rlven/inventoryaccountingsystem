﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class BuyProduct : BaseAnswer
    {
        [IgnoreDataMember]
        public int UserId { get; set; }

        [DataMember]
        public  int ProductId { get; set; }

        [DataMember]
        public int Amount { get; set; }

        [DataMember]
        public string ShopAddress { get; set; }

        [DataMember]
        public string ShopPhone { get; set; }
    }
}
