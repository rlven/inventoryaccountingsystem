﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class RulesOfUse : BaseAnswer
    {
        [DataMember]
        public string RulesOfUseRu { get; set; }
        [DataMember]
        public string RulesOfUseKg { get; set; }
    }
}
