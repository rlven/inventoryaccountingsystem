﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class CigaretteBrand
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string BrandCompany { get; set; }
        [DataMember]
        public string BrandName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public double Tar { get; set; }
        [DataMember]
        public double Nicotine { get; set; }
        public bool IsActive { get; set; }
        [DataMember]
        public int BrandPosition { get; set; }
        [DataMember]
        public DateTime CreationDate { get; set; }
        public bool IsDeleted { get; set; }
        [DataMember]
        public string AdditionalInfo1 { get; set; }
        [DataMember]
        public string AdditionalImage1 { get; set; }
        [DataMember]
        public string AdditionalInfo2 { get; set; }
        [DataMember]
        public string AdditionalImage2 { get; set; }
        [DataMember]
        public string AdditionalInfo3 { get; set; }
        [DataMember]
        public string AdditionalImage3 { get; set; }
        [DataMember]
        public string AdditionalInfo4 { get; set; }
        [DataMember]
        public string AdditionalImage4 { get; set; }
        [DataMember]
        public string BrandImageP { get; set; }
        [DataMember]
        public string BrandImageM { get; set; }
        [DataMember]
        public string DescriptionImageP { get; set; }
        [DataMember]
        public string DescriptionImageM { get; set; }
        public string AdminImage { get; set; }
        [DataMember]
        public string PackSize { get; set; }
    }
}
