﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class City
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
    }
}