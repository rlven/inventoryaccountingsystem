﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class PartyQuestionnaire
    {
        [DataMember]
        public string Question1 { get; set; }
        [DataMember]
        public string Question2 { get; set; }
        [DataMember]
        public string Question3 { get; set; }
        [DataMember]
        public string Question4 { get; set; }
        [DataMember]
        public string Question5 { get; set; }
        [DataMember]
        public string Question6 { get; set; }
        [DataMember]
        public string Question7 { get; set; }
        [DataMember]
        public string Question8 { get; set; }
        [DataMember]
        public string Question9 { get; set; }
        [DataMember]
        public string Question10 { get; set; }
    }
}
