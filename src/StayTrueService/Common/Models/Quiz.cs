﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class Quiz
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Url { set; get; }
        [DataMember]
        public string Image { set; get; }
        [DataMember]
        public int Points { get; set; }
        [DataMember]
        public DateTime? PublicationDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Description { set; get; }
    }
}
