﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class PushSubscribtion
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        [Required]
        public string PlayerId { get; set; }
    }
}
