﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class UserAccount
    {
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}
