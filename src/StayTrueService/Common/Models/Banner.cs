﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Banner
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string ImagePath { set; get; }
        [DataMember]
        public bool IsActive { get; set; }
    }
}
