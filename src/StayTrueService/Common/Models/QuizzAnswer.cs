﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class QuizzAnswer : BaseAnswer
    {
        [DataMember]
        public string Fullname { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Adress { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Pin { get; set; }
        [DataMember]
        public bool Ifsmoke { get; set; }
        [DataMember]
        public string Favorite { get; set; }
        [DataMember]
        public string FavoriteType { get; set; }
        [DataMember]
        public string Alternative { get; set; }
        [DataMember]
        public string AlternativeType { get; set; }
        [DataMember]
        [Required]
        public int QuizzId { get; set; }
    }
}
