﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Account : BaseAnswer
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public int Wincoins { get; set; }
        [DataMember]
        public string CityName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public DateTime? BirthDate { get; set; }
        [DataMember]
        public int RoleId { get; set; }
        [DataMember]
        public int StatusId { get; set; }
        [IgnoreDataMember]
        public bool IsApproved  { get; set; }
        [DataMember]
        public string CigarettesFavorite { get; set; }
        [DataMember]
        public string CigarettesFavoriteType { get; set; }
        [DataMember]
        public string CigarettesAlternative { get; set; }
        [DataMember]
        public string CigarettesAlternativeType { get; set; }
        [DataMember]
        public string ChatToken { get; set; }
        [DataMember]
        public int GreatBattlePoints { get; set; }
        [DataMember]
        public int GreatBattleGroupId { get; set; }
        [DataMember]
        public Boolean CanChangeBattleSide { get; set; }
        [DataMember]
        public int Rank { get; set; }
        [DataMember]
        public int TicketCoins { get; set; }
        [DataMember]
        public bool IsActiveParty { get; set; }
        [DataMember]
        public bool IsBlockedFromParty { get; set; }
        [DataMember]
        public bool IsInvitedToParty { get; set; }
        [DataMember]
        public int? PromoCode { get; set; }
    }
}
