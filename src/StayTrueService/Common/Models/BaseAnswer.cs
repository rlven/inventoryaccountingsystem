﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class BaseAnswer
    {
        [DataMember(EmitDefaultValue = false)]
        public string ErrorMessage { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string LocalizedMessage { get; set; }

        public BaseAnswer()
        {

        }

        public BaseAnswer(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }

}
