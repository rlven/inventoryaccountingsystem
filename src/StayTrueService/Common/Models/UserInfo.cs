﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class UserInfo
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int CityId { get; set; }
        [DataMember]
        public int StatusId { get; set; }
        [DataMember]
        public DateTime Birthdate { get; set; }
        [DataMember]
        public string PhoneNumber { set; get; }
        [DataMember]
        public DateTime RegistrationDate { get; set; }
        [DataMember]
        public int Wincoins { get; set; }
        [DataMember]
        public int STP { get; set; }
        [DataMember]
        public int STM { get; set; }
        [DataMember]
        public DateTime? LastSignInP { get; set; }
        [DataMember]
        public DateTime? LastSignInM { set; get; }
        [DataMember]
        public string Login { get; set; }
        [IgnoreDataMember]
        public string Password { get; set; }
        public string FrontSidePassport { get; set; }
        public string BackSidePassport { get; set; }
    }
}
