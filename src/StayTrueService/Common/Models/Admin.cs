﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Common.Enums;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class Admin
    {
        [DataMember]
        [Required]
        public int Id { get; set; }
        [DataMember]
        [Required]
        public string Login { get; set; }
        [DataMember]
        [Required]
        public string Password { get; set; }
        [DataMember]
        [Required]
        public string FirstName { get; set; }
        [DataMember]
        [Required]
        public string LastName { get; set; }
        [DataMember]
        [Required]
        public string PhoneNumber { get; set; }
        [DataMember]
        [Required]
        public string Email { get; set; }
        [DataMember]
        public DateTime? Birthdate { get; set; }
        [DataMember]
        [Required]
        public StatusEnum Status { get; set; }
        [DataMember]
        [Required]
        public RoleEnum Role { get; set; }
    }
}
