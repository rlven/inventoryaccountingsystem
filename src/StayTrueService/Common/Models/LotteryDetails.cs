﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class LotteryDetails : BaseAnswer
    {
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public int PurchasedTicketsCount { get; set; } 
        [DataMember]
        public int TicketPrice { get; set; }
        [DataMember]
        public int MaxTicketCountForUser { get; set; }
    }
}
