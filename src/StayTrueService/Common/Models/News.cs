﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class NewsModel : BaseAnswer
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string NameRu { get; set; }
        [DataMember]
        public string NameKg { get; set; }
        [DataMember]
        public string DescriptionRu { get; set; }
        [DataMember]
        public string DescriptionKg { get; set; }
        [DataMember]
        public string ImageP { get; set; }
        [DataMember]
        public string ImageM { get; set; }
        [DataMember]
        public string VideoP { get; set; }
        [DataMember]
        public string VideoM { get; set; }
        [DataMember]
        public int UsersLikesCount { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public DateTime? CreationDate { get; set; }
        [DataMember]
        public int NewsCategory { get; set; }
        [IgnoreDataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool UserIsLiked { get; set; }
        [DataMember]
        public bool IsBrand { get; set; }
    }
}
