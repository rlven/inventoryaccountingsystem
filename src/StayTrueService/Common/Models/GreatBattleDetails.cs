﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleDetails : BaseAnswer
    {
        [DataMember]
        public int GreatBattleId { set; get; }

        [DataMember]
        public int UserId { set; get; }

        [DataMember]
        public int UserGroupId { set; get; }

        [DataMember]
        public String Description { set; get; }

        [DataMember]
        public DateTime? BattleBeginDate { get; set; }

        [DataMember]
        public DateTime? BattleEndDate { get; set; }

        [DataMember]
        public int RedSideWinCount { set; get; }

        [DataMember]
        public int BlueSidewinCount { set; get; }

        [DataMember]
        public int RedSidePoints { set; get; }

        [DataMember]
        public int BlueSidePoints { set; get; }

        [DataMember]
        public List<GreatBattleTour> Tours { set; get; }

        [DataMember]
        public int WinnerGroupId { get; set; }
        [DataMember]
        public double BattleEndLeft { get; set; }
        [DataMember]
        public double BattleStartLeft { get; set; }
        [DataMember]
        public string DescriptionForMobile { get; set; }
    }
}
