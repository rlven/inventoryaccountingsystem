﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class UserSurvey
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int SurveyId { get; set; }
        [DataMember]
        public List<UserQuestion> Questions { get; set; }
    }

    [Serializable]
    [DataContract]
    public class UserQuestion
    {
        [DataMember]
        public int QuestionId { get; set; }
        public List<UserAnswer> Answers { get; set; }
    }

    [Serializable]
    [DataContract]
    public class UserAnswer
    {
        [DataMember]
        public int AnswerId { get; set; }
        [DataMember]
        public string Answer { get; set; }
    }
}
