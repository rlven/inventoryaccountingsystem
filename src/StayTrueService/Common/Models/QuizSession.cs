﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class QuizSession : BaseAnswer
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int QuizId { set; get; }
        [DataMember]
        public int Score { set; get; }
        [DataMember]
        public int QuestionIndex { set; get; }
        [DataMember]
        public bool IsCompleted { set; get; }
        [DataMember]
        public DateTime? CreationDate { set; get; }
        [DataMember]
        public int UserId { set; get; }
        [DataMember]
        public String Result { get; set; }
    }
}
