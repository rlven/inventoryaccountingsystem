﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Common.ValidationAttributes;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class BuyLotteryTicket : BaseAnswer
    {        
        [DataMember]
        public int TicketsCount { get; set; }
        [DataMember]
        public int UserCurrentWincoins { get; set; }
    }
}
