﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleQuestionnaireAnswer : BaseAnswer
    {
        [DataMember]
        public int QuestionnaireId { get; set; }
        
        [DataMember]
        public string Answer1 { get; set; }

        [DataMember]
        public string Answer2 { get; set; }

        [DataMember]
        public string Answer3 { get; set; }

        [DataMember]
        public string Answer4 { get; set; }
    }
}
