﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class UserGameHistory
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public DateTime SessionStart { get; set; }
        [DataMember]
        public int GameId { get; set; }
        [DataMember]
        public String GameName { get; set; }
        [DataMember]
        public int Score { get; set; }
        [DataMember]
        public int Wincoins { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
    }
}
