﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class CigaretteModel
    {
        [IgnoreDataMember]
        public int Id { get; set; }
        [DataMember]
        public string ModelName { get; set; }
        [IgnoreDataMember]
        public int Position { get; set; }
        [DataMember]
        public List<CigaretteModelType> CigaretteModelType { get; set; }
    }
}
