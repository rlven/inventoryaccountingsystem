﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GameScore : BaseAnswer
    {
        [DataMember]
        public String Result { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public int Points { get; set; }

        [DataMember]
        public int GameId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string UserFullName { get; set; }

    }

    public class PvpGameScore : BaseAnswer
    {
        [DataMember]
        public String Result { get; set; }

        [DataMember]
        public int? Score { get; set; }

        [DataMember]
        public int? Points { get; set; }

        [DataMember]
        public int? GameId { get; set; }

        [DataMember]
        public int? UserId { get; set; }

        [DataMember]
        public string UserFullName { get; set; }
    }
}
