﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleGameScore : BaseAnswer
    {
        [DataMember]
        public String Result { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public int Points { get; set; }

        [DataMember]
        public int GameId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int GreatBattleId { get; set; }

        [DataMember]
        public int GreatBattleTourId { get; set; }


    }
}
