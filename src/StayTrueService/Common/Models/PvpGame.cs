﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class PvpGame : BaseAnswer
    {
        [DataMember]
        public int GameId { get; set; }
        [DataMember]
        public int[] Bets { get; set; }
        [DataMember]
        public int PlayerQty { get; set; }
        [DataMember]
        public bool IncludeBots { get; set; }
        [DataMember]
        public int MatchTime { get; set; }
    }
}
