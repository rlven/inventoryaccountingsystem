﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class BlockUser
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public DateTime BlockDate { get; set; }
        
        [DataMember]
        public DateTime UnblockDate { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
