﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class QuestionnaireAnswers : BaseAnswer
    {
        [DataMember]
        public int QuestionnaireId { get; set; }
        [DataMember]
        public string Answer1 { get; set; }
        [DataMember]
        public string Answer2 { get; set; }
        [DataMember]
        public string Answer3 { get; set; }
        [DataMember]
        public string Answer4 { get; set; }
        [DataMember]
        public string Answer5 { get; set; }
        [DataMember]
        public string Answer6 { get; set; }
        [DataMember]
        public string Answer7 { get; set; }
        [DataMember]
        public string Answer8 { get; set; }
        [DataMember]
        public string Answer9 { get; set; }
        [DataMember]
        public string Answer10 { get; set; }
        [DataMember]
        public string Answer11 { get; set; }
        [DataMember]
        public string Answer12 { get; set; }
        [DataMember]
        public int UserId { get; set; }
    }
}