﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Product : BaseAnswer
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string NameRu { get; set; }
        [DataMember]
        public string InformationRu { get; set; }
        [DataMember]
        public string NameKg { get; set; }
        [DataMember]
        public string InformationKg { get; set; }
        [DataMember]
        public int Amount { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public int ShopId { get; set; }
        [DataMember]
        public int AvailableAmount { get; set; }
        [DataMember]
        public int Price { get; set; }
        [DataMember]
        public bool IsAvailable { get; set; }
    }
}
