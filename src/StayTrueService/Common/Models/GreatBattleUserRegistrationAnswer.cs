﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleUserRegistrationAnswer : BaseAnswer
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public GreatBattleQuestionnaireAnswer QuestionnaireAnswer { get; set; }
    }
}
