﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleQuizResult : BaseAnswer
    {
        [DataMember]
        public int GreatBattleId { get; set; }
        [DataMember]
        public int GreatBattleTourId { get; set; }
        [DataMember]
        public int Score { set; get; }
        [DataMember]
        public int QuestionIndex { set; get; }
        [DataMember]
        public bool IsCompleted { set; get; }
        [DataMember]
        public DateTime? CreationDate { set; get; }
        [DataMember]
        public int UserId { set; get; }
        [DataMember]
        public String Result { get; set; }
    }
}
