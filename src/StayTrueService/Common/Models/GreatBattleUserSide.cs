﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class GreatBattleUserSide : BaseAnswer
    {
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public int UserId { get; set; }
    }
}
