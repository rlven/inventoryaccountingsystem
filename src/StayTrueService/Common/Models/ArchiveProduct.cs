﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class ArchiveProduct
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int ShopHistoryId { set; get; }
        [DataMember]
        public DateTime? Date { set; get; }
    }
}
