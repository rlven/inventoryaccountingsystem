﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    class UsersInfo
    {
        [Serializable]
        [DataContract]
        public class UserInfo
        {
            public int Id { get; set; }
            [DataMember]
            public string FirstName { get; set; }
            [DataMember]
            public string LastName { get; set; }
            [DataMember]
            public string Email { get; set; }
            [DataMember]
            public int CityId { get; set; }
            [DataMember]
            public int StatusId { get; set; }
            [DataMember]
            public DateTime Birthdate { get; set; }
            [DataMember]
            public DateTime RegistrationDate { get; set; }
            [DataMember]
            public int Wincoins { get; set; }
            [DataMember]
            public int STP { get; set; }
            [DataMember]
            public int STM { get; set; }
            [DataMember]
            public DateTime LastSingIn { get; set; }
            [DataMember]
            public string Login { get; set; }
            [DataMember]
            public string Password { get; set; }
            public byte[] FrontSidePassport { get; set; }
            public byte[] BackSidePassport { get; set; }
        }
    }
}
