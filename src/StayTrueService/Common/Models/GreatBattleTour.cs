﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleTour : BaseAnswer
    {
        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public String GameTourName { set; get; }

        [DataMember]
        public String GameTourImagePath { set; get; }

        [DataMember]
        public String GameTourPath { set; get; }

        [DataMember]
        public String QuizzTourName { set; get; }

        [DataMember]
        public String QuizzTourPath { set; get; }

        [DataMember]
        public String QuizzTourImagePath { set; get; }

        [DataMember]
        public DateTime? BeginDateTime { set; get; }

        [DataMember]
        public DateTime? EndDateTime { set; get; }

        [DataMember]
        public int Status { set; get; }

        [DataMember]
        public int? WinnerGroupId { set; get; }

        [DataMember]
        public int RedSidePoints { set; get; }

        [DataMember]
        public int BlueSidePoints { set; get; }
        [DataMember]
        public double TourEndLeft { get; set; }
        [DataMember]
        public double TourStartLeft { get; set; }
    }
}
