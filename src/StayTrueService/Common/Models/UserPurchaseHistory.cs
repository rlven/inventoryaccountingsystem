﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class UserPurchaseHistory
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public String ProductName { get; set; }
        [DataMember]
        public DateTime CreationDate { get; set; }
        [DataMember]
        public int Amount { get; set; }
        [DataMember]
        public int Price { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
    }
}
