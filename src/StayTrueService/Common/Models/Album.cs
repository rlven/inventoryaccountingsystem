﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{

    [Serializable]
    [DataContract]
    public class Album
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }
                
        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string Image { set; get; }
    }
}