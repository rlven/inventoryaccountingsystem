﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    class UserPassword
    {
        public string Login { get; set; }
        public string NewPassword { get; set; }
    }
}
