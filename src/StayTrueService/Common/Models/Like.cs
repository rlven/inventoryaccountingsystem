﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Like : BaseAnswer
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int NewsId { get; set; }
    }
}
