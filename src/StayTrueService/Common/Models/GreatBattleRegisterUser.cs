﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [DataContract]
    [Serializable]
    public class GreatBattleRegisterUser
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int GreatBattleId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public GreatBattleQuestionnaireAnswer QuestionnaireAnswer { get; set; }
    }
}
