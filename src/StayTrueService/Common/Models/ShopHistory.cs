﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class ShopHistory : BaseAnswer
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public Product ProductModel { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public int Amount { get; set; }

        [DataMember]
        public bool Received { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }
        [DataMember]
        public bool IsInArchive { get; set; }
        [DataMember]
        public DateTime? DateSendingArchive { get; set; }


    }
}