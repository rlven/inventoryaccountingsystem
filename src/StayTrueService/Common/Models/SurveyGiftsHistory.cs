﻿using System;
using System.Runtime.Serialization;


namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class SurveyGiftHistory
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int QuestionnairesId { set; get; }
        [DataMember]
        public int UserId { set; get; }
        [DataMember]
        public int GiftId { set; get; }
        [DataMember]
        public bool IsIssued { set; get; }
        [DataMember]
        public DateTime? Date { set; get; }
        [DataMember]
        public bool IsInArchive { set; get; }
        [DataMember]
        public DateTime? DateSendingArchive { set; get; }

    }
}
