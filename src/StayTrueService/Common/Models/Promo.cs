﻿using System;
using System.Runtime.Serialization;

namespace Common.Models
{
    [Serializable]
    [DataContract]
    public class Promo : BaseAnswer
    {
        [DataMember]
        public int Promocode { get; set; }
    }
}
