﻿CREATE TABLE [dbo].[Administrators] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Login]       NVARCHAR (100) NOT NULL,
    [Password]    NVARCHAR (MAX) NOT NULL,
    [FirstName]   NVARCHAR (100) NOT NULL,
    [LastName]    NVARCHAR (100) NOT NULL,
    [PhoneNumber] NVARCHAR (50)  NOT NULL,
    [Email]       NVARCHAR (100) NOT NULL,
    [BirthDate]   DATE           NOT NULL,
    [Role]        INT            NOT NULL,
    [LastSignIn]  DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Roles_Administrators] FOREIGN KEY ([Role]) REFERENCES [dbo].[Roles] ([Id]),
    UNIQUE NONCLUSTERED ([Login] ASC)
);


