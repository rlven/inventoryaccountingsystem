﻿CREATE TABLE [dbo].[Banners]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Name] NVARCHAR(50) NOT NULL, 
    [ImagePath] NVARCHAR(MAX) NULL, 
    [IsActive] BIT NOT NULL
)
