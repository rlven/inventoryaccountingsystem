﻿CREATE TABLE [dbo].[Questions]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [QuestionnaireId] INT NOT NULL CONSTRAINT FK_Questionnaires_Questions FOREIGN KEY REFERENCES Questionnaires (Id), 
    [Question] NVARCHAR(MAX) NOT NULL, 
    [QuestionType] INT NOT NULL CONSTRAINT FK_QuestionnTypes_Questions FOREIGN KEY REFERENCES QuestionTypes (Id), 
    [AnswerAmount] INT NOT NULL
)
