﻿CREATE TABLE [dbo].[PurchaseStatuses] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (500) NULL,
    CONSTRAINT [PK_ProductsStatuses] PRIMARY KEY CLUSTERED ([Id] ASC)
);

