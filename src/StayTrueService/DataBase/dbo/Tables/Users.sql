﻿CREATE TABLE [dbo].[Users] (
    [Id]                        INT            IDENTITY (1, 1) NOT NULL,
    [Phone]                     NVARCHAR (12)  NOT NULL,
    [Password]                  NVARCHAR (MAX) NULL,
    [FirstName]                 NVARCHAR (MAX) NOT NULL,
    [LastName]                  NVARCHAR (MAX) NOT NULL,
    [MiddleName]                NVARCHAR (MAX) NULL,
    [Email]                     NVARCHAR (100) NOT NULL,
    [BirthDate]                 DATE           NOT NULL,
    [RegistrationDate]          DATETIME       NOT NULL,
    [RefreshDate]               DATETIME       NULL,
    [CityId]                    INT            NOT NULL,
    [StatusId]                  INT            NOT NULL,
    [GenderId]                  INT            NOT NULL,
    [RoleId]                    INT            CONSTRAINT [DF_Users_RoleId] DEFAULT ((2)) NOT NULL,
    [Wincoins]                  INT            NOT NULL,
    [PromoCode]                 VARCHAR (10)   NULL,
    [LastSignInM]               DATETIME       NULL,
    [LastSignInP]               DATETIME       NULL,
    [SignInCounterM]            INT            CONSTRAINT [DF_Users_SignInCounterM] DEFAULT ((0)) NOT NULL,
    [SignInCounterP]            INT            CONSTRAINT [DF_Users_SignInCounterP] DEFAULT ((0)) NOT NULL,
    [IsDeleted]                 BIT            CONSTRAINT [DF_Users_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsApproved]                BIT            CONSTRAINT [DF_Users_IsApproved] DEFAULT ((0)) NOT NULL,
    [Avatar]                    NVARCHAR (250) NULL,
    [PassportFront]             NVARCHAR (250) NULL,
    [PassportBack]              NVARCHAR (250) NULL,
    [CigarettesFavorite]        NVARCHAR (250) NULL,
    [CigarettesFavoriteType]    NVARCHAR (250) NULL,
    [CigarettesAlternative]     NVARCHAR (250) NULL,
    [CigarettesAlternativeType] NVARCHAR (250) NULL,
    [SignInAttemptCount]        INT            CONSTRAINT [DF_Users_SignInAttempt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Users_AccountStatuses] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[AccountStatuses] ([Id]),
    CONSTRAINT [FK_Users_Cities] FOREIGN KEY ([CityId]) REFERENCES [dbo].[Cities] ([Id]),
    CONSTRAINT [FK_Users_Genders] FOREIGN KEY ([GenderId]) REFERENCES [dbo].[Genders] ([Id]),
    CONSTRAINT [FK_Users_Users] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([Id])
);














