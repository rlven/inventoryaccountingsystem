﻿CREATE TABLE [dbo].[SurveyGifts]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
    [NameRu] NVARCHAR(MAX) NULL, 
    [InformationRu] NVARCHAR(MAX) NULL, 
    [NameKg] NVARCHAR(MAX) NULL, 
    [InformationKg] NVARCHAR(MAX) NULL, 
    [Amount] INT NOT NULL, 
    [ImagePath] NVARCHAR(MAX) NULL, 
    [IsPosm] BIT NOT NULL
)
