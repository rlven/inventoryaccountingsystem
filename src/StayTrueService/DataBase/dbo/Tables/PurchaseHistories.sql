﻿CREATE TABLE [dbo].[PurchaseHistories] (
    [Id]              BIGINT   IDENTITY (1, 1) NOT NULL,
    [UserId]          INT      NOT NULL,
    [ProductId]       INT      NOT NULL,
    [CreationDate]    DATETIME NOT NULL,
    [UpdateDate]      DATETIME NULL,
    [Amount]          INT      NOT NULL,
    [Price]           INT      NOT NULL,
    [StatusId]        INT      CONSTRAINT [DF_ShopHistories_Delivered] DEFAULT ((0)) NOT NULL,
    [AdministratorId] INT      NULL,
    CONSTRAINT [PK__ShopHist__3214EC072B90BED2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Products_ShopHistory] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([Id]),
    CONSTRAINT [FK_ShopHistories_Administrators] FOREIGN KEY ([AdministratorId]) REFERENCES [dbo].[Administrators] ([Id]),
    CONSTRAINT [FK_ShopHistories_ProductsStatuses] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[PurchaseStatuses] ([Id]),
    CONSTRAINT [FK_Users_ShopHistory] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

