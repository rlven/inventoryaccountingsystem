﻿CREATE TABLE [dbo].[CigaretteBrands]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [BrandName] NVARCHAR(100) NOT NULL
)
