﻿CREATE TABLE [dbo].[News] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [NameRu]          NVARCHAR (100) NOT NULL,
    [NameKg]          NVARCHAR (100) NOT NULL,
    [DescriptionRu]   NVARCHAR (MAX) NOT NULL,
    [DescriptionKg]   NVARCHAR (MAX) NOT NULL,
    [ImageP]          NVARCHAR (MAX) NULL,
    [ImageM]          NVARCHAR (MAX) NULL,
    [VideoP]          NVARCHAR (MAX) NULL,
    [VideoM]          NVARCHAR (MAX) NULL,
    [NewsType]        INT            NOT NULL,
    [NewsCategory]    INT            NOT NULL,
    [CreationDate]    DATETIME       NOT NULL,
    [PublicationDate] DATETIME       NULL,
    [IsActive]        BIT            NOT NULL,
    [IsBrand]         BIT            NULL,
    [IsDeleted]       BIT            NULL,
    CONSTRAINT [PK__tmp_ms_x__3214EC072B665EB6] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_News_NewsCategories] FOREIGN KEY ([NewsCategory]) REFERENCES [dbo].[NewsCategories] ([Id]),
    CONSTRAINT [FK_News_NewsTypes] FOREIGN KEY ([NewsType]) REFERENCES [dbo].[NewsTypes] ([Id])
);






