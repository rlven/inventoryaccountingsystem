﻿CREATE TABLE [dbo].[ProductsCategories] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (250) NOT NULL,
    [IsActive]  BIT            CONSTRAINT [DF_ProductsCategories_IsActive] DEFAULT ((1)) NOT NULL,
    [IsDeleted] BIT            CONSTRAINT [DF_ProductsCategories_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ProductsCategories] PRIMARY KEY CLUSTERED ([Id] ASC)
);

