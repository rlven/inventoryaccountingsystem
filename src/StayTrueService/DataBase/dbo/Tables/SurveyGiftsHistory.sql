﻿CREATE TABLE [dbo].[SurveyGiftsHistory]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [QuestionnairesId] INT NOT NULL CONSTRAINT FK_Questionnaires_SurveyGiftsHistory FOREIGN KEY REFERENCES Questionnaires (Id), 
    [UserId] INT NOT NULL CONSTRAINT FK_Users_SurveyGiftsHistory FOREIGN KEY REFERENCES Users (Id), 
    [GiftId] INT NOT NULL, 
    [IsIssued] BIT NOT NULL, 
    [WhereIssued] INT NOT NULL,
    [Date] DATETIME NOT NULL,	
    [IsInArchive] BIT NOT NULL, 
    [DateSendingArchive] DATETIME NOT NULL,

)
