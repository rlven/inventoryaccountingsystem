﻿CREATE TABLE [dbo].[AlbumImages] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [CategoryId] INT            NOT NULL,
    [Image]      NVARCHAR (MAX) NULL,
    [Video]      NVARCHAR (MAX) NULL,
    [SmallImage] NVARCHAR (MAX) NULL,
    [IsBrand]    BIT            NULL,
    [IsActive]   BIT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Albums_AlbumImages] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Albums] ([Id])
);


