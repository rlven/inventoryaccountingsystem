﻿CREATE TABLE [dbo].[GameSession] (
    [Id]           INT      IDENTITY (1, 1) NOT NULL,
    [UserId]       INT      NOT NULL,
    [GameId]       INT      NOT NULL,
    [SessionStart] DATETIME NULL,
    [SessionEnd]   DATETIME NULL,
    [Points]       INT      NULL,
    [Wincoins]     INT      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Games_GameSession] FOREIGN KEY ([GameId]) REFERENCES [dbo].[Games] ([Id]),
    CONSTRAINT [FK_Users_GameSession] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);


