﻿CREATE TABLE [dbo].[AccountStatuses]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Status] NVARCHAR(50) NOT NULL
)
