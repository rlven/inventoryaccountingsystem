﻿CREATE TABLE [dbo].[ArchiveSurveyGifts]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [SurveyGiftHistoryId] INT NOT NULL  CONSTRAINT FK_ArchiveSurveyGifts_SurveyGiftsHistory FOREIGN KEY REFERENCES SurveyGiftsHistory (Id), 
    [Date] DATETIME NOT NULL
)
