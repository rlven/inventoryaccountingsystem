﻿CREATE TABLE [dbo].[Shops] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (150) NOT NULL,
    [Address]   NVARCHAR (350) NOT NULL,
    [Phone]     NVARCHAR (50)  NULL,
    [CityId]    INT            NOT NULL,
    [IsActive]  BIT            CONSTRAINT [DF_Shops_IsActive] DEFAULT ((1)) NOT NULL,
    [IsDeleted] BIT            CONSTRAINT [DF_Shops_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Shops__3214EC07C2B5952E] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Shops_Cities] FOREIGN KEY ([CityId]) REFERENCES [dbo].[Cities] ([Id])
);


