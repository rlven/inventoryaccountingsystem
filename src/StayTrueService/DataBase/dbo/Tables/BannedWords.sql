﻿CREATE TABLE [dbo].[BannedWords] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [word] VARCHAR (150) NOT NULL,
    PRIMARY KEY NONCLUSTERED ([id] ASC)
);



