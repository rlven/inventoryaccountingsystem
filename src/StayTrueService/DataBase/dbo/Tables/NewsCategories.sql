﻿CREATE TABLE [dbo].[NewsCategories] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_NewsCategories] PRIMARY KEY CLUSTERED ([Id] ASC)
);

