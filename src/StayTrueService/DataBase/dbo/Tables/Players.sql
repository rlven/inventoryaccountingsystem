﻿CREATE TABLE [dbo].[Players]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [UserId] INT NOT NULL CONSTRAINT [FK_Users_Players] FOREIGN KEY REFERENCES [dbo].[Users] ([Id]), 
    [GameId] INT NOT NULL CONSTRAINT [FK_Games_Players] FOREIGN KEY REFERENCES [dbo].[Games] ([Id])
)
