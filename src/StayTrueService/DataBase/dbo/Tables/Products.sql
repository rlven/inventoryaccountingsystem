﻿CREATE TABLE [dbo].[Products] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [NameRu]          NVARCHAR (MAX) NULL,
    [InformationRu]   NVARCHAR (MAX) NULL,
    [NameKg]          NVARCHAR (MAX) NULL,
    [InformationKg]   NVARCHAR (MAX) NULL,
    [Price]           INT            NOT NULL,
    [Amount]          INT            NOT NULL,
    [IsAvailable]     BIT            NOT NULL,
    [ImagePath]       NVARCHAR (MAX) NULL,
    [ShopId]          INT            NOT NULL,
    [CategoryId]      INT            NOT NULL,
    [AvailableAmount] INT            NOT NULL,
    [CreationDate]    DATETIME       NULL,
    [PublicationDate] DATETIME       NULL,
    [LanguageId]      INT            CONSTRAINT [DF_Products_LanguageId] DEFAULT ((0)) NOT NULL,
    [IsDeleted]       BIT            CONSTRAINT [DF__Products__IsDele__3A4CA8FD] DEFAULT ((0)) NOT NULL,
    [IsBrand]         BIT            CONSTRAINT [DF_Products_IsBrand] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__tmp_ms_x__3214EC079C94CBF7] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Products_ProductsCategories] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[ProductsCategories] ([Id])
);




