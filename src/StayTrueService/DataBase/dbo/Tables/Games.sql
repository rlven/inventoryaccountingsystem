﻿CREATE TABLE [dbo].[Games] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (50)  NOT NULL,
    [GameUrl]         NVARCHAR (MAX) NULL,
    [ImagePath]       NVARCHAR (MAX) NULL,
    [Points]          INT            NULL,
    [PublicationDate] DATETIME       NULL,
    [IsActive]        BIT            NOT NULL,
    [IsDeleted]       BIT            CONSTRAINT [DF__Games__IsDeleted__367C1819] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Games__3214EC07043EAB39] PRIMARY KEY CLUSTERED ([Id] ASC)
);


