﻿CREATE TABLE [dbo].[BestScores]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [UserId] INT NOT NULL, 
    [GameId] INT NOT NULL, 
    [Score] INT NOT NULL, 
    [LastLaunchDate] DATETIME NOT NULL,
)
