﻿CREATE TABLE [dbo].[Albums] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (100) NOT NULL,
    [CreationDate]    DATE           NOT NULL,
    [IsActive]        BIT            NOT NULL,
    [Image]           NVARCHAR (MAX) NULL,
    [IsBrended]       BIT            NULL,
    [IsDeleted]       BIT            DEFAULT ((0)) NOT NULL,
    [PublicationDate] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);



