﻿CREATE TABLE [dbo].[ChatTokens] (
    [id]          BIGINT       IDENTITY (1, 1) NOT NULL,
    [insert_date] BIGINT       NOT NULL,
    [token]       VARCHAR (36) NULL,
    CONSTRAINT [PK_ChatTokens] PRIMARY KEY CLUSTERED ([id] ASC)
);





