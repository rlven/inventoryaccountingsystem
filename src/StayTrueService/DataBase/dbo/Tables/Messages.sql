﻿CREATE TABLE [dbo].[Messages] (
    [id]         BIGINT   NOT NULL,
    [user_id]    INT      NOT NULL,
    [message]    TEXT     NOT NULL,
    [date]       DATETIME DEFAULT (getdate()) NULL,
    [sticker_id] INT      NULL,
    [forward]    INT      DEFAULT (NULL) NULL,
    [is_deleted] BIT      DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [Messages__fk] FOREIGN KEY ([user_id]) REFERENCES [dbo].[Users] ([Id])
);



