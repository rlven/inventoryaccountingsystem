﻿CREATE TABLE [dbo].[SupportRequests] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [UserId]       INT            NULL,
    [Body]         NVARCHAR (MAX) NOT NULL,
    [CreationDate] DATETIME       NOT NULL,
    [Phone]        NVARCHAR (12)  NULL,
    [Email]        NVARCHAR (250) NULL,
    [IsAnswered]   BIT            CONSTRAINT [DF_SupportRequests_IsAnswered] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__SupportR__3214EC0713C8814F] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SupportRequests_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);




