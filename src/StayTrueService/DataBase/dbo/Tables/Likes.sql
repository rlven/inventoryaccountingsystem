﻿CREATE TABLE [dbo].[Likes] (
    [Id]     INT IDENTITY (1, 1) NOT NULL,
    [UserId] INT NOT NULL,
    [NewsId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Likes_News] FOREIGN KEY ([NewsId]) REFERENCES [dbo].[News] ([Id]),
    CONSTRAINT [FK_Users_Likes] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);




