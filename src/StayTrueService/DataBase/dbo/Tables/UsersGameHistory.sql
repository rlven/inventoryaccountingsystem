﻿CREATE TABLE [dbo].[UsersGameHistory]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1,1), 
    [UserId] INT NOT NULL CONSTRAINT FK_Users_UsersGameHistory FOREIGN KEY REFERENCES Users (Id), 
    [LastLaunchDate] DATE NOT NULL, 
    [Score] INT NOT NULL, 
    [GameId] INT NOT NULL CONSTRAINT FK_Games_UsersGameHistory FOREIGN KEY REFERENCES Games (Id), 
    [Wincoins] INT NULL
)
