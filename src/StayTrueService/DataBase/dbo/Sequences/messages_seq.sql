﻿CREATE SEQUENCE [dbo].[messages_seq]
    AS BIGINT
    INCREMENT BY 1
    MINVALUE 1
    CACHE 10;

