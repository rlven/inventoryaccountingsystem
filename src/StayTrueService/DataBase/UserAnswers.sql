﻿CREATE TABLE [dbo].[UserAnswers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [UserId] INT NOT NULL CONSTRAINT FK_Users_UserAnswers FOREIGN KEY REFERENCES Users (Id), 
    [QuestionnaireId] INT NOT NULL CONSTRAINT FK_Questionnaires_UserAnswers FOREIGN KEY REFERENCES Questionnaires (Id), 
    [QuestionId] INT NOT NULL CONSTRAINT FK_Questions_UserAnswers FOREIGN KEY REFERENCES Questions (Id), 
    [AnswerId] INT NOT NULL CONSTRAINT FK_Answers_UserAnswers FOREIGN KEY REFERENCES Answers (Id), 
    [Answer] NVARCHAR(MAX) NULL
)
