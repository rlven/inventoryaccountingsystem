﻿using System;
using Common.Models;
using Common.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using StayTrueService.BusinessLogic.Managers;

namespace StayTrueServiceTests
{
    [TestClass]
    public class GameManagerTest
    {
        public TestContext TestContext { get; set; }
        public UserManager UserManager = new UserManager();

        [TestMethod]
        //[DeploymentItem("StayTrueServiceTests\\AddGameResult.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\AddGameResult.xml", "GameResult", DataAccessMethod.Sequential)]
        public void AddGameResultTest()
        {
            var gameScore = JsonConvert.DeserializeObject<GameScore>(TestContext.DataRow["ScoreObject"].ToString());
            var expectedResult = int.Parse(TestContext.DataRow["Wincoins"].ToString());
            var actualResult = UserManager.AddGameResult(gameScore).Score;
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}

/*
 * var decryptScore = AesCrypto.DecryptStringAES("206607", "dXH7j8wjGCtKWBRurBM/GSboDbJOLZNyiNtRtObG/cg=");
            var score = JsonConvert.DeserializeObject<GameScore>(decryptScore);
            Assert.AreEqual(score.Score, 690);

    new GameScore {GameId = 16, UserId = 104827, Points = 25};
 */
