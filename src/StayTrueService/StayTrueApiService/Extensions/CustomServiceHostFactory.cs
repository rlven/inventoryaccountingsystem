﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace StayTrueApiService.Extensions
{
    public class CustomServiceHostFactory : WebServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            ServiceHost host = base.CreateServiceHost(serviceType, baseAddresses);
            var endpoints = host.AddDefaultEndpoints();
            foreach (var endpoint in endpoints)
            {
                endpoint.Behaviors.Add(new WebHttpBehaviorException());
            }

            return host;
        }
    }
}