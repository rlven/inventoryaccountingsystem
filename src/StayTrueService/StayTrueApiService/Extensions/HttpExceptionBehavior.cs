﻿using System;
using System.ServiceModel.Configuration;

namespace StayTrueApiService.Extensions
{
    public class HttpExceptionBehavior : BehaviorExtensionElement
    {
        public override Type BehaviorType => typeof(WebHttpBehaviorException);

        protected override object CreateBehavior()
        {
            return new WebHttpBehaviorException();
        }
    }
}