﻿using Newtonsoft.Json;

namespace StayTrueApiService.Extensions
{
    public class CustomErrorMessage
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("code")]
        public int Code { get; set; }
    }
}