﻿namespace StayTrueApiService.Models
{
    public class AlbumImageModel
    {
        public int Id { set; get; }
        public int CategoryId { set; get; }
        public string Image { set; get; }
        public string SmallImage { get; set; }
        public string Video { get; set; }
        public bool IsBrand { get; set; }
        public bool IsActive { get; set; }
    }
}