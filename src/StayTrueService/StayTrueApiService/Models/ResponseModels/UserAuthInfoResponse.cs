﻿using System;

namespace StayTrueApiService.Models.ResponseModels
{
    public class UserAuthInfoResponse : BaseResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Wincoins { get; set; }
        public int RoleId { get; set; }
        public string CigarettesFavorite { get; set; }
        public string CigarettesFavoriteType { get; set; }
        public string CigarettesAlternative { get; set; }
        public string CigarettesAlternativeType { get; set; }
        public string ChatToken { get; set; }
    }
}