﻿namespace StayTrueApiService.Models.ResponseModels
{
    public class AvatarResponse
    {
        public int Id { get; set; }
        public string AvatarPath { get; set; }
    }
}