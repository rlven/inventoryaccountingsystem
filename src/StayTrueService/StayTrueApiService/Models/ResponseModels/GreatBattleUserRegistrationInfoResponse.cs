﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GreatBattleUserRegistrationInfoResponse : BaseResponse
    {
        public DateTime? BattleBeginDate { get; set; }

        public DateTime? BattleEndDate { get; set; }

        public int BlueSidePoints { get; set; }

        public int RedSidePoints { get; set; }

        public bool IsRegistered { get; set; }

        public bool BattleIsFinished { get; set; }

        public int UserGroupId { get; set; }

        public int? WinnerGroupId { get; set; }

        public bool? IsAutofilled { get; set; }

        public bool IsBlocked { get; set; }

        public GreatBattleQuestionnaire Questionnaire { get; set; }

        public double BattleEndLeft { get; set; }

        public double BattleStartLeft { get; set; }
    }
}