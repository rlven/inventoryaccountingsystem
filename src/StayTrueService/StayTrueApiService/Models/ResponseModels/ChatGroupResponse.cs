﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class ChatGroupResponse : BaseResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public bool IsPrivate { get; set; }
    }
}