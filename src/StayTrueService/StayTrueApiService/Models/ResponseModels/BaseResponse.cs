﻿using Newtonsoft.Json;

namespace StayTrueApiService.Models.ResponseModels
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class BaseResponse
    {
        public string ErrorMessage { get; set; }
        public string LocalizedMessage { get; set; }

        public BaseResponse() { }

        public BaseResponse(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}
