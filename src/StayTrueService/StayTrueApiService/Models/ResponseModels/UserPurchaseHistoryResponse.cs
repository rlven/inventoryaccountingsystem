﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class UserPurchaseHistoryResponse
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int Amount { get; set; }
        public int Price { get; set; }
        public string ImagePath { get; set; }
    }
}