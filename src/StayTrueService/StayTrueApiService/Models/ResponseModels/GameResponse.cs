﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GameResponse : BaseResponse
    {
        public int Id { set; get; }

        public string Name { set; get; }

        public string GameUrl { set; get; }

        public int Points { get; set; }

        public string Image { set; get; }

        public DateTime? PublicationDate { get; set; }

        public bool IsActive { get; set; }
    }
}