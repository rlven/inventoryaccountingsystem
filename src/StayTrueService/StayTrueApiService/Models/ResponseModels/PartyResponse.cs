﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class PartyResponse : BaseResponse
    {
        public List<PartyQuestion> Questions { get; set; }

        public string ImagePreview { get; set; }

        public string ImagePreviewSmall { get; set; }

        public string Description { get; set; }

        public string DescriptionForMobile { get; set; }

        public string Name { get; set; }

        public List<PartyGames> PartyGames { get; set; }

        public string RegistrationBegin { get; set; }

        public string EndDate { get; set; }

        public bool IsAnswered { get; set; }

        public List<PartyQuizzes> PartyQuizzes { get; set; }

        public bool IsActive { get; set; }

        public bool IsFinished { get; set; }

        public bool IsInvited { get; set; }

        public bool IsDisabled { get; set; }

        public bool IsBlocked { get; set; }
    }
    public class PartyGames 
    {
        public string Image { get; set; }

        public string GameUrl { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public int Points { get; set; }

        public DateTime? PublicationDate { get; set; }


    }

    public class PartyQuizzes 
    {
        public string Image { get; set; }

        public string Url { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public int Points { get; set; }

        public DateTime? PublicationDate { get; set; }

        public string Description { get; set; }
    }


    public class PartyQuestion 
    {
        public string Question { get; set; }
    }
}