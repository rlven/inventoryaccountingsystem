﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class UserGameHistoryResponse : BaseResponse
    {
        public long Id { get; set; }
        public string GameName { get; set; }
        public int Score { get; set; }
        public DateTime SessionTime { get; set; }
        public string ImagePath { get; set; }
    }
}