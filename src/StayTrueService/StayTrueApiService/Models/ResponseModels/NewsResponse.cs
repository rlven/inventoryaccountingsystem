﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Models;

namespace StayTrueApiService.Models.ResponseModels
{
    public class NewsResponse : BaseResponse
    {
        public IEnumerable<NewsModel> MainNews { get; set; }
        public IEnumerable<NewsModel> SecondaryNews { get; set; }
        public IEnumerable<BannerModel> Banners { get; set; }
    }
}