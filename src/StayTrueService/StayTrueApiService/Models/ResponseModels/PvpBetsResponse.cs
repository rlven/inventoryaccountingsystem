﻿namespace StayTrueApiService.Models.ResponseModels
{
    public class PvpBetsResponse : BaseResponse
    {
        public int GameId { get; set; }
        public int[] Bets { get; set; }
    }
}