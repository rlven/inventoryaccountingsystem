﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GameScoreResponse : BaseResponse
    {
        public int UserId { get; set; }
        public int GameId { get; set; }
        public int BestScore { get; set; }
    }
}