﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class IsExistResponse : BaseResponse
    {
        public bool IsExist;

        public string Message;
    }
}