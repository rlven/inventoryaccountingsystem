﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class CigaretteBrandResponse : BaseResponse
    {
        public int Id { get; set; }

        public string BrandCompany { get; set; }

        public string BrandName { get; set; }

        public string Description { get; set; }

        public double Tar { get; set; }

        public double Nicotine { get; set; }
        public bool IsActive { get; set; }

        public int BrandPosition { get; set; }

        public DateTime CreationDate { get; set; }

        public bool IsDeleted { get; set; }

        public string AdditionalInfo1 { get; set; }

        public string AdditionalImage1 { get; set; }

        public string AdditionalInfo2 { get; set; }

        public string AdditionalImage2 { get; set; }

        public string AdditionalInfo3 { get; set; }

        public string AdditionalImage3 { get; set; }

        public string AdditionalInfo4 { get; set; }

        public string AdditionalImage4 { get; set; }

        public string BrandImageP { get; set; }

        public string BrandImageM { get; set; }

        public string DescriptionImageP { get; set; }

        public string DescriptionImageM { get; set; }

        public string AdminImage { get; set; }

        public string PackSize { get; set; }
    }
}