﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GreatBattleDetailsResponseModel : BaseResponse
    {
        public int GreatBattleId { set; get; }

        public int UserId { set; get; }

        public int UserGroupId { set; get; }

        public string Description { set; get; }

        public DateTime? BattleBeginDate { get; set; }

        public DateTime? BattleEndDate { get; set; }

        public int RedSideWinCount { set; get; }

        public int BlueSidewinCount { set; get; }

        public int RedSidePoints { set; get; }

        public int BlueSidePoints { set; get; }

        public List<GreatBattleTour> Tours { set; get; }

        public int WinnerGroupId { get; set; }

        public double BattleEndLeft { get; set; }

        public double BattleStartLeft { get; set; }

        public string DescriptionForMobile { get; set; }
    }
}