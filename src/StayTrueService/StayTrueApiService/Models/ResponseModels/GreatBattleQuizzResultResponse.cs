﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GreatBattleQuizzResultResponse : BaseResponse
    {
        public int GreatBattleId { get; set; }

        public int GreatBattleTourId { get; set; }

        public int Score { set; get; }

        public int QuestionIndex { set; get; }

        public bool IsCompleted { set; get; }

        public DateTime? CreationDate { set; get; }

        public int UserId { set; get; }

        public string Result { get; set; }
    }
}