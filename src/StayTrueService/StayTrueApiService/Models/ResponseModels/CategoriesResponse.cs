﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class CategoriesResponse
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public List<ProductsResponse> Products { get; set; }
    }
}