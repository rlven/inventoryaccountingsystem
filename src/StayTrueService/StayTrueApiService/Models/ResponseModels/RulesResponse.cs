﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Models;

namespace StayTrueApiService.Models.ResponseModels
{
    public class RulesResponse : BaseResponse
    {
        public string RulesOfUseRu { get; set; }

        public string RulesOfUseKg { get; set; }
    }
}