﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class ProductsResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Information { get; set; }
        public int Price { get; set; }
        public int Amount { get; set; }
        public string ImagePath { get; set; }
        public int Available { get; set; }
    }
}