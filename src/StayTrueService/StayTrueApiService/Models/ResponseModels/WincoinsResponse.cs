﻿namespace StayTrueApiService.Models.ResponseModels
{
    public class WincoinsResponse
    {
        public int Id { get; set; }
        public int Wincoins { get; set; }
    }
}