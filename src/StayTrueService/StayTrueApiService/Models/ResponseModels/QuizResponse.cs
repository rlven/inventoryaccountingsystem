﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class QuizResponse : BaseResponse
    {
        public int Id { set; get; }

        public string Name { set; get; }

        public string Url { set; get; }

        public string Image { set; get; }

        public int Points { get; set; }

        public DateTime? PublicationDate { get; set; }

        public bool IsActive { get; set; }

        public string Description { set; get; }
    }

}