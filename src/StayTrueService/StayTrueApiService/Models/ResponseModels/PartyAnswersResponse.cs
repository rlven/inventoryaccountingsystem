﻿namespace StayTrueApiService.Models.ResponseModels
{
    public class PartyAnswersResponse : BaseResponse
    {
        public PartyAnswers PartyAnswers { get; set; }
    }
}