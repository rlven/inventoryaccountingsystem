﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class InvitedUsersResponse : BaseResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AvatarPath { get; set; }
    }
}