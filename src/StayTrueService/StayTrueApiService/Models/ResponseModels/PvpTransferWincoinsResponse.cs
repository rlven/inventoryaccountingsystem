﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class PvpTransferWincoinsResponse : BaseResponse
    {
        public List<int?> UserIds { get; set; }
    }
}