﻿using System;

namespace StayTrueApiService.Models.ResponseModels
{
    public class QuizSessionResponse : BaseResponse
    {
        public int Id { set; get; }

        public int QuizId { set; get; }

        public int Score { set; get; }

        public int QuestionIndex { set; get; }

        public bool IsCompleted { set; get; }

        public DateTime? CreationDate { set; get; }

        public int UserId { set; get; }

        public string Result { get; set; }
    }
}