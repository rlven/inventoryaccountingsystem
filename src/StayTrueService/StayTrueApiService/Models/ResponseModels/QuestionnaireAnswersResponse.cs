﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class QuestionnaireAnswersResponse : BaseResponse
    {
        public int QuestionnaireId { get; set; }
        public int UserId { get; set; }
    }
}