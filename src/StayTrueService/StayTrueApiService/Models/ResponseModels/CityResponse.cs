﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class CityResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}