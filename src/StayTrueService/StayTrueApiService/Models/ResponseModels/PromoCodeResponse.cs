﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class PromoCodeResponse
    {
        public bool IsExist { get; set; }
        public int Promocode { get; set; }
    }
}