﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GreatBattleGameScoreResponse : BaseResponse
    {
        public string Result { get; set; }

        public int Score { get; set; }

        public int Points { get; set; }

        public int GameId { get; set; }

        public int UserId { get; set; }

        public int GreatBattleId { get; set; }

        public int GreatBattleTourId { get; set; }
    }
}