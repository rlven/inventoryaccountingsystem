﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class BuyProductResponse : BaseResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
        public int Sum { get; set; }
        public string ShopAddress { get; set; }
        public string ShopPhone { get; set; }
        public List<CategoriesResponse> Products { get; set; }
    }
}