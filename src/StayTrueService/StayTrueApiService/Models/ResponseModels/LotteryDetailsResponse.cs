﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class LotteryDetailsResponse : BaseResponse
    {
        public bool IsActive { get; set; }

        public int PurchasedTicketsCount { get; set; }

        public int TicketPrice { get; set; }

        public int MaxTicketCountForUser { get; set; }
    }
}