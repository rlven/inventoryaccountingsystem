﻿using System.Collections.Generic;

namespace StayTrueApiService.Models.ResponseModels
{
    public class PvpGamesResponse
    {
        public List<PvpGame> PvpGames { get; set; }
        public int Pages { get; set; }
    }
}