﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class LotteryTicketResponse : BaseResponse
    {
        public int TicketsCount { get; set; }

        public int UserCurrentWincoins { get; set; }
    }
}