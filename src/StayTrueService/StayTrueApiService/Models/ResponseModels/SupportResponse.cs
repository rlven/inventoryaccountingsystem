﻿namespace StayTrueApiService.Models.ResponseModels
{
    public class SupportResponse : BaseResponse
    {
        public string Body { set; get; }
        public string Email { set; get; }
        public string PhoneNumber { set; get; }
        public bool IsPasswordForgot { get; set; }
    }
}