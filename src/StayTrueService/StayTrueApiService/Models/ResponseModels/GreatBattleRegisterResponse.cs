﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class GreatBattleRegisterResponse : BaseResponse
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }
}