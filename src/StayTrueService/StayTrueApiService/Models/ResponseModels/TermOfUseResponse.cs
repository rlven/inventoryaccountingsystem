﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Models;

namespace StayTrueApiService.Models.ResponseModels
{
    public class TermOfUseResponse : BaseResponse
    {
        public string TermOfUseRu { get; set; }

        public string TermOfUseKg { get; set; }
    }
}