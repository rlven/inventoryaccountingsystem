﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models.ResponseModels
{
    public class UserAuthResponse : BaseResponse
    {
        public UserAuthInfoResponse User { get; set; }
        public string Token { get; set; }
    }
}