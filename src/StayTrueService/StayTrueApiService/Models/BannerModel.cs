﻿namespace StayTrueApiService.Models
{
    public class BannerModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string ImagePath { set; get; }
        public bool IsActive { get; set; }
    }
}