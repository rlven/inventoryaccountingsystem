﻿using System;

namespace StayTrueApiService.Models.ResponseModels
{
    public class NewsModel
    {
        public int Id { get; set; }

        public string NameRu { get; set; }

        public string NameKg { get; set; }

        public string DescriptionRu { get; set; }

        public string DescriptionKg { get; set; }

        public string ImageP { get; set; }

        public string ImageM { get; set; }

        public string VideoP { get; set; }

        public string VideoM { get; set; }

        public int UsersLikesCount { get; set; }

        public int NewsType { get; set; }

        public DateTime? CreationDate { get; set; }

        public int NewsCategory { get; set; }

        public bool IsActive { get; set; }

        public bool UserIsLiked { get; set; }

        public bool IsBrand { get; set; }
    }
}