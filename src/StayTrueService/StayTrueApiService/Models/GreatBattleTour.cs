﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models
{
    public class GreatBattleTour
    {
        public int Id { set; get; }

        public string GameTourName { set; get; }

        public string GameTourImagePath { set; get; }

        public string GameTourPath { set; get; }

        public string QuizzTourName { set; get; }

        public string QuizzTourPath { set; get; }

        public string QuizzTourImagePath { set; get; }

        public DateTime? BeginDateTime { set; get; }

        public DateTime? EndDateTime { set; get; }

        public int Status { set; get; }

        public int? WinnerGroupId { set; get; }

        public int RedSidePoints { set; get; }

        public int BlueSidePoints { set; get; }

        public double TourEndLeft { get; set; }

        public double TourStartLeft { get; set; }
    }
}