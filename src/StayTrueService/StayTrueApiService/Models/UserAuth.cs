﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models
{
    public class UserAuth
    {        
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Captcha { get; set; }
    }
}