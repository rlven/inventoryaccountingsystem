﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Models
{
    public class GameScore : BaseResponse
    {
        [Required]
        public string Result { get; set; }

        public int Score { get; set; }

        public int Points { get; set; }

        public int GameId { get; set; }

        public int UserId { get; set; }

        public string UserFullName { get; set; }
    }
}