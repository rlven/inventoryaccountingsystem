﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models
{
    public class GreatBattleQuestionnaireAnswer
    {
        public int QuestionnaireId { get; set; }

        public string Answer1 { get; set; }

        public string Answer2 { get; set; }

        public string Answer3 { get; set; }

        public string Answer4 { get; set; }
    }
}