﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Models
{
    public class QuizSession
    {
        public int Id { set; get; }

        public int QuizId { set; get; }

        public int Score { set; get; }

        public int QuestionIndex { set; get; }

        public bool IsCompleted { set; get; }

        public DateTime? CreationDate { set; get; }

        public int UserId { set; get; }

        [Required]
        public string Result { get; set; }
    }
}