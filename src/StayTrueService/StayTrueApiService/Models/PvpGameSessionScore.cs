﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models
{
    public class PvpGameSessionScore
    {
        public int GameId { get; set; }

        public string RoomId { get; set; }

        public int? UserId { get; set; }

        public string SessionStart { get; set; }

        public string SessionEnd { get; set; }

        public int Kills { get; set; }

        public int Death { get; set; }

        public int Score { get; set; }

        public int Bet { get; set; }
    }
}