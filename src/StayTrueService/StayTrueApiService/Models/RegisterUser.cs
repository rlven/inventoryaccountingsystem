﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StayTrueApiService.Models
{
    public class RegisterUser
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        public int CityId { get; set; }
        [Required]
        public int GenderId { get; set; }
        [Required]
        public string CigarettesFavorite { get; set; }
        [Required]
        public string CigarettesFavoriteType { get; set; }
        [Required]
        public string CigarettesAlternative { get; set; }
        [Required]
        public string CigarettesAlternativeType { get; set; }
        [Required]
        public string PassportFront { get; set; }
        [Required]
        public string PassportBack { get; set; }

        public int PromoCode { get; set; }
    }
}