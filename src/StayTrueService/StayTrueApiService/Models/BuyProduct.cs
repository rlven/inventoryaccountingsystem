﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace StayTrueApiService.Models
{
    public class BuyProduct
    {
        [Required]
        public int ProductId { get; set; }

        public int Amount { get; set; } = 1;
    }
}