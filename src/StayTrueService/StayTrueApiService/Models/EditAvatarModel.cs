﻿using System;
using System.Runtime.Serialization;

namespace StayTrueApiService.Models
{
    public class EditAvatarModel
    {
        public string Avatar { get; set; }
    }
}