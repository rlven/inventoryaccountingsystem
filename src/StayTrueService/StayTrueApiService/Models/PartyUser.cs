﻿using StayTrueApiService.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueApiService.Models
{
    public class PartyUser : BaseResponse
    {
        public int? userId;
        public string firstName;
        public string lastName;
        public string phone;

        public int? UserId
        {
            get
            {
                if (userId == null)
                {
                    userId = 0;
                }

                return userId;
            }
            set { userId = value; }
        }

        public bool IsInvited { get; set; }

        public bool HasCome { get; set; }

        public string FirstName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(firstName))
                {
                    firstName = nameof(FirstName);
                }

                return firstName;
            }
            set { firstName = value; }
        }

        public string LastName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(lastName))
                {
                    lastName = nameof(LastName);
                }

                return lastName;
            }
            set { lastName = value; }
        }

        public string Phone
        {
            get
            {
                if (String.IsNullOrWhiteSpace(phone))
                {
                    phone = nameof(Phone);
                }

                return phone;
            }
            set { phone = value; }
        }
    }
}