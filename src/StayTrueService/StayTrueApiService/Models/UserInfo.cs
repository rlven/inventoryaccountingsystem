﻿using System;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Models
{
    public class UserInfo : BaseResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public int Wincoins { get; set; }
        public string CityName { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public int RoleId { get; set; }
        public int StatusId { get; set; }
        public bool IsApproved { get; set; }
        public string CigarettesFavorite { get; set; }
        public string CigarettesFavoriteType { get; set; }
        public string CigarettesAlternative { get; set; }
        public string CigarettesAlternativeType { get; set; }
        public int GreatBattlePoints { get; set; }
        public int GreatBattleGroupId { get; set; }
        public Boolean CanChangeBattleSide { get; set; }
        public int Rank { get; set; }
        public int TicketCoins { get; set; }
        public bool IsActiveParty { get; set; }
        public bool IsBlockedFromParty { get; set; }
        public bool IsInvitedToParty { get; set; }
    }
}