﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Common;

namespace StayTrueApiService.Models
{
    public class SupportRequest
    {
        [Required(ErrorMessage = "Body field required")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "Length of Body is not correct")]
        public string Body { set; get; }
        [Required]
        public string Email { set; get; }
        [Required]
        public string PhoneNumber { set; get; }
        [Required]
        public bool IsPasswordForgot { get; set; }
    }
}