﻿using System.ComponentModel.DataAnnotations;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Models
{
    public class GreatBattleRegisterModel
    {
        [Required]
        public int GroupId { get; set; }

        public GreatBattleQuestionnaireAnswer QuestionnaireAnswer { get; set; }
    }
}