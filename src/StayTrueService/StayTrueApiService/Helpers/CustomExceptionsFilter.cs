﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Filters;
using NLog;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Helpers
{
    public class CustomExceptionsFilter : ExceptionFilterAttribute
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override void OnException(HttpActionExecutedContext context)
        {
            Logger.Error(context.Exception);
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new ObjectContent<BaseResponse>(
                    new BaseResponse("An unhandled exception was thrown by service."), new JsonMediaTypeFormatter(),
                    "application/some-format"),
                ReasonPhrase = "Internal Server Error. Please Contact your Administrator. Watch service logs.",
                StatusCode = HttpStatusCode.InternalServerError
            };
            context.Response = response;
        }
    }
}