﻿using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Web;

namespace StayTrueApiService.Helpers
{
    internal static class HttpHelper
    {
        public static object Request { get; private set; }

        internal static string SendRequest(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "GET";

            var response = (HttpWebResponse)request.GetResponse();

            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
        internal static string GetHeaderValue(string headerKey)
        {
           var header = HttpContext.Current.Request.Headers.GetValues(headerKey) ?? null;
           return header != null ? header.First() : null;
        }
    }
}