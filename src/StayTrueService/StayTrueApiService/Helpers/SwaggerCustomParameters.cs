﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http.Description;
using StayTrueApiService.Attributes;
using Swagger.Net;

namespace StayTrueApiService.Helpers
{
    public class SwaggerCustomParameters : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                operation.parameters = new List<Parameter>();

            var authorizeAttributes = apiDescription.GetControllerAndActionAttributes<JwtAuthenticationAttribute>();
            if (authorizeAttributes.Any())
            {
                operation.parameters.Add(new Parameter
                {
                    name = "Authorization",
                    @in = "header",
                    type = "string",
                    description = "Authorization Token",
                    @default = "Bearer ",
                    required = true
                });

                operation.parameters.Add(new Parameter
                {
                    name = "SecureKey",
                    @in = "header",
                    type = "string",
                    description = "Secure Key",
                    @default = ConfigurationManager.AppSettings["SecureKey"],
                    required = true
                });
            }


        }
    }
}