﻿using Common.Logger;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Managers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace StayTrueApiService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(SwaggerConfig.Register);

            Timer timer2 = new Timer();

            timer2.Elapsed += GreatBattleMonitoring;
            timer2.Interval = 5 * 60 * 1000; //5 min
            timer2.Enabled = true;
            timer2.Start();

            Timer timer3 = new Timer();

            timer3.Elapsed += GetAllOperatorCodes;
            timer3.Interval = 1440 * 60 * 1000; //1 day
            timer3.Enabled = true;
            timer3.Start();

            Timer timer4 = new Timer();

            timer3.Elapsed += new ElapsedEventHandler(UserFirstSignInGift);
            timer3.Interval = 5 * 60 * 1000;//5 min
            timer3.Enabled = true;
            timer3.Start();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (exception != null)
            {
                Log.Error(exception);
                Log.Error(exception.StackTrace);
            }
        }

        void UserFirstSignInGift(object sender, ElapsedEventArgs e)
        {
            try
            {
                BallancePaymentService.UserFirstSignInGift();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        void GetAllOperatorCodes(object sender, ElapsedEventArgs e)
        {
            try
            {
                BallancePaymentService.SaveAllOperatorCodes();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        void GreatBattleMonitoring(object sender, ElapsedEventArgs e)
        {
            try
            {
                new GreatBattleManager().CheckBattleExpirationDate();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }
    }
}
