using System;
using System.Configuration;
using System.IO;
using System.Web.Http;
using System.Linq;
using System.Reflection;
using StayTrueApiService.Helpers;
using Swagger.Net.Application;

namespace StayTrueApiService
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration httpConfiguration)
        {
            httpConfiguration.EnableSwagger(c =>
            {
                c.SingleApiVersion("new", "StayTrue API");
                c.OperationFilter<SwaggerCustomParameters>();
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                c.PrettyPrint();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.RootUrl(req => ConfigurationManager.AppSettings["SwaggerRootUrl"]);
            }).EnableSwaggerUi(c => { });
        }
    }
}
