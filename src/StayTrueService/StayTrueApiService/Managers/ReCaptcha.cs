﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Newtonsoft.Json;

namespace StayTrueApiService.Managers
{
    public class ReCaptcha
    {

        [JsonProperty("success")]
        public Boolean Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }


        public static bool Validate(string encodedResponse)
        {
            var client = new System.Net.WebClient();

            string privateKey = ConfigurationManager.AppSettings["ReCaptchaPrivateKey"];

            var googleReply = client.DownloadString($"https://www.google.com/recaptcha/api/siteverify?secret={privateKey}&response={encodedResponse}");

            var captchaResponse = JsonConvert.DeserializeObject<ReCaptcha>(googleReply);

            return captchaResponse.Success;
        }

        public static bool ValidateTime(DateTime? signTime)
        {
            if (signTime is null)
            {
                return true;
            }

            var diffInSeconds = (DateTime.Now - (DateTime)signTime).TotalSeconds;

            return diffInSeconds >= 30;
        }


    }
}