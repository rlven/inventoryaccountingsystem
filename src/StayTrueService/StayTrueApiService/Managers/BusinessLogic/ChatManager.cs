﻿using Common.Models;
using DataAccessLayer;
using StayTrueApiService.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class ChatManager
    {
        internal List<ChatGroupResponse> GetChatGroupsByUserId(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var chatGroups = context.ChatGroups
                    .Where(x => x.IsDeleted == false && x.IsActive && x.IsPrivate == false)
                    .OrderByDescending(x => x.Id);
                if (chatGroups == null)
                    return null;


                var userChats = chatGroups.Select(x => new ChatGroupResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    Image = x.Image,
                    IsPrivate = false

                }).ToList();

                var privateChatGroups = context.ChatGroups
                    .Where(x => x.IsDeleted == false &&
                                x.IsActive &&
                                x.IsPrivate &&
                                x.ChatPrivateGroupsUsers.Any(u => u.UserId == userId))
                    .OrderByDescending(x => x.Id);

                if (privateChatGroups == null)
                    return null;

                userChats.AddRange(privateChatGroups.ToList().Select(x => new ChatGroupResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    Image = x.Image,
                    IsPrivate = true

                }).ToList());

                return userChats;
            }
        }

        public string GetChatAuth()
        {
            string login = ConfigurationManager.AppSettings["ChatLogin"];
            string password = ConfigurationManager.AppSettings["ChatPassword"];
            if(string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
                return string.Empty;;
            return "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes($"{login}:{password}"));
        }
    }
}