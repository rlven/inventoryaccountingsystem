﻿using DataAccessLayer;
using System.Collections.Generic;
using System.Linq;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class CitiesManager
    {
        public IEnumerable<CityResponse> GetAllCities()
        {
            using (var context = new StayTrueDBEntities())
            {
                return context.Cities.Select(x => new CityResponse
                {
                    Id = x.Id,
                    Name = x.CityName
                }).ToList();
            }
        }
    }
}