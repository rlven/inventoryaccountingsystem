﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using DataAccessLayer;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers.BusinessLogic
{
    public class PvpGameManager
    {
        private static readonly int GamesCountOnPage = 15;

        public List<PvpGame> GetPvpGames(int pageSize, int page)
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<PvpGames> games = context.PvpGames
                    .Where(x => x.IsActive && !x.IsDelete && DateTime.Now >= x.PublicationDate)
                    .OrderByDescending(x => x.Id).Skip(pageSize * page).Take(pageSize);

                return games.ToList().Select(x => new PvpGame
                {
                    Id = x.Id,
                    Name = x.Name,
                    GameUrl = x.GameUrl,
                    Image = x.ImagePath
                }).ToList();
            }
        }

        public int CountPages(int pageSize)
        {
            var context = new StayTrueDBEntities();
            return (context.PvpGames.Count() + pageSize - 1) / pageSize;
        }

        public PvpBetsResponse PvpGameBets(int gameId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var game = context.PvpGames.FirstOrDefault(g => g.GameId == gameId && g.IsActive && !g.IsDelete);
                if (game == null)
                    return new PvpBetsResponse {ErrorMessage = Resources.GameNotFound};
                return new PvpBetsResponse {Bets = game.PvpGameBets.Select(b => b.Bet).ToArray(), GameId = gameId};
            }
        }

        public PvpTransferWincoinsResponse PvpSessionDepositWincoins(List<PvpGameSessions> scores)
        {
            using (var context = new StayTrueDBEntities())
            {
                if (scores.Select(s => s.Bet).Distinct().Count() != 1)
                    return new PvpTransferWincoinsResponse {ErrorMessage = "Error. Different bets in one game session, can't deposit money"};
                var sessions = scores.Select(s => new PvpGameSession
                {
                    GameId = s.GameId,
                    RoomId = s.RoomId,
                    Kills = s.Kills,
                    Death = s.Death,
                    Score = s.Score,
                    SessionEnd = !string.IsNullOrEmpty(s.SessionEnd) ? DateTime.Parse(s.SessionEnd) : (DateTime?)null,
                    SessionStart = !string.IsNullOrEmpty(s.SessionStart) ? DateTime.Parse(s.SessionStart) : (DateTime?)null,
                    UserId = s.UserId == -1 ? null : s.UserId,
                    IsBot = s.UserId == -1,
                    Bet = s.Bet
                }).ToList();

                var winnerStat = sessions.OrderByDescending(s => s.Score).FirstOrDefault();
                if (winnerStat == null)
                    return new PvpTransferWincoinsResponse { ErrorMessage = "Error. Empty session" };

                var winners = sessions.Where(s => s.Score == winnerStat.Score && !s.IsBot).Select(s => s.UserId).ToArray();
                if (winners.Any())
                {
                    sessions.Where(s => winners.Contains(s.UserId)).ToList().ForEach(s => s.Wincoins = winnerStat.Bet * scores.Count / winners.Length);
                    context.Users.Where(u => winners.Contains(u.Id)).ToList().ForEach(u => u.Wincoins += winnerStat.Bet * scores.Count / winners.Count());
                }

                context.PvpGameSession.AddRange(sessions);
                context.SaveChanges();
                return new PvpTransferWincoinsResponse {UserIds = winners.ToList()};
            }
        }

        public PvpTransferWincoinsResponse PvpSessionWithdrawWincoins(List<PvpGameSessions> scores)
        {
            using (var context = new StayTrueDBEntities())
            {
                var session = scores.Select(s => new PvpGameSession
                {
                    GameId = s.GameId,
                    RoomId = s.RoomId,
                    SessionStart = !string.IsNullOrEmpty(s.SessionStart) ? DateTime.Parse(s.SessionStart) : (DateTime?)null,
                    UserId = s.UserId == -1 ? null : s.UserId,
                    Bet = s.Bet,
                    Wincoins = -s.Bet
                });

                if (!session.Any())
                    return new PvpTransferWincoinsResponse {ErrorMessage = "Error. Empty session"};

                var bet = session.FirstOrDefault().Bet;
                var ids = scores.Select(s => s.UserId).ToArray();
                context.Users.Where(u=>ids.Contains(u.Id)).ToList().ForEach(u=>u.Wincoins -= bet);
                context.PvpGameSession.AddRange(session);
                context.SaveChanges();

                return new PvpTransferWincoinsResponse {UserIds = ids.ToList()};
            }
        }
    }
}