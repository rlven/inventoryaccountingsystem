﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Common.Logger;
using DataAccessLayer;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers.BusinessLogic
{
    public class LotteryManager
    {
        public LotteryDetailsResponse GetLotteryDetails(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var activeLottery = context.Lottery.FirstOrDefault(l => l.IsActive && !l.IsDeleted);
                if (activeLottery == null)
                    return new LotteryDetailsResponse() { ErrorMessage = Resources.NoActiveLottery };
                var curUserPurchases = context.LotteryUsers.FirstOrDefault(u => u.UserId == userId && u.LotteryId == activeLottery.Id);
                int userPurchasesCount = 0;
                if (curUserPurchases != null)
                {
                    userPurchasesCount = curUserPurchases.TicketsCount;
                }
                return new LotteryDetailsResponse()
                {
                    IsActive = activeLottery.IsActive,
                    TicketPrice = activeLottery.TicketPrice,
                    MaxTicketCountForUser = activeLottery.MaxTicketsForUser,
                    PurchasedTicketsCount = userPurchasesCount
                };
            }
        }

        public LotteryTicketResponse BuyLotteryTicket(LotteryTicket ticket, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                if (ticket.TicketsCount <= 0)
                    return new LotteryTicketResponse() { ErrorMessage = Resources.TicketsCountMustBeMoreThan0 };
                var activeLottery = context.Lottery.FirstOrDefault(l => l.IsActive && !l.IsDeleted);
                if (activeLottery == null)
                    return new LotteryTicketResponse() { ErrorMessage = Resources.NoActiveLottery };
                var lotteryUser = context.LotteryUsers.FirstOrDefault(l => l.UserId == userId && l.LotteryId == activeLottery.Id);
                var currUser = context.Users.Find(userId);
                if (currUser == null)
                    return new LotteryTicketResponse() { ErrorMessage = Resources.UserNotFound };
                if (!IsUserHaveWincoins(currUser, activeLottery, ticket.TicketsCount))
                    return new LotteryTicketResponse() { ErrorMessage = Resources.NotEnoughWincoins, UserCurrentWincoins = currUser.Wincoins };
                try
                {
                    if (lotteryUser == null)
                    {
                        if (!IsTicketAvailable(activeLottery, ticket))
                            return new LotteryTicketResponse() { ErrorMessage = Resources.NoAvailableTickets };
                        var newLotteryUser = new LotteryUsers()
                        {
                            LotteryId = activeLottery.Id,
                            TicketsCount = ticket.TicketsCount,
                            UserId = userId
                        };
                        activeLottery.AvailableTickets -= ticket.TicketsCount;
                        currUser.Wincoins -= activeLottery.TicketPrice * ticket.TicketsCount;
                        context.LotteryUsers.Add(newLotteryUser);
                        context.SaveChanges();
                        return new LotteryTicketResponse()
                        {
                            LocalizedMessage = Resources.Ok,
                            TicketsCount = ticket.TicketsCount,
                            UserCurrentWincoins = currUser.Wincoins
                        };
                    }
                    if (!IsTicketAvailable(activeLottery, ticket, lotteryUser))
                        return new LotteryTicketResponse() { ErrorMessage = Resources.NoAvailableTickets };

                    activeLottery.AvailableTickets -= ticket.TicketsCount;
                    lotteryUser.TicketsCount += ticket.TicketsCount;
                    currUser.Wincoins -= activeLottery.TicketPrice * ticket.TicketsCount;
                    context.SaveChanges();
                    return new LotteryTicketResponse()
                    {
                        LocalizedMessage = Resources.Ok,
                        TicketsCount = ticket.TicketsCount,
                        UserCurrentWincoins = currUser.Wincoins
                    };
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return new LotteryTicketResponse() { ErrorMessage = ex.Message };
                }

            }
        }


        private bool IsTicketAvailable(Lottery lottery, LotteryTicket ticket)
        {
            int availableTickets = lottery.AvailableTickets;
            int maxAvailablePurchaseCountForUser = lottery.MaxTicketsForUser;
            int purchasedTickets = ticket.TicketsCount;

            if (maxAvailablePurchaseCountForUser < purchasedTickets)
                return false;

            if (availableTickets < purchasedTickets)
                return false;

            return true;
        }

        private bool IsUserHaveWincoins(Users user, Lottery lottery, int ticketsCount)
        {
            return lottery.TicketPrice * ticketsCount < user.Wincoins ? true : false;
        }

        private bool IsTicketAvailable(Lottery lottery, LotteryTicket ticket, LotteryUsers user)
        {
            int availableTickets = lottery.AvailableTickets;
            int maxAvailablePurchaseCountForUser = lottery.MaxTicketsForUser;
            int userPurchasedTicketsCount = user.TicketsCount + ticket.TicketsCount;

            if (maxAvailablePurchaseCountForUser < userPurchasedTicketsCount)
                return false;

            if (availableTickets < ticket.TicketsCount)
                return false;

            return true;
        }
    }
}