﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Models;
using DataAccessLayer;
using StayTrueApiService.Models.ResponseModels;
using StayTrueApiService.Helpers;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class ShopManager
    {
        public Product GetProduct(int productId, StayTrueDBEntities context)
        {
            var dbProducts = context.Products.SingleOrDefault(x => x.Id == productId);
            if (dbProducts != null)
                return new Product
                {
                    Id = dbProducts.Id,
                    ImagePath = dbProducts.ImagePath,
                    InformationRu = dbProducts.InformationRu,
                    InformationKg = dbProducts.InformationKg,
                    NameRu = dbProducts.NameRu,
                    NameKg = dbProducts.NameKg,
                    Price = dbProducts.Price,
                };

            return null;
        }

        public List<Shop> GetShops()
        {
            var context = new StayTrueDBEntities();
            var shops = context.Shops.OrderByDescending(x => x.Id).ToList();
            var shopsToReturn = shops.Select(x => new Shop
            {
                Id = x.Id,
                Name = x.Name,
                IsActive = x.IsActive
            }).ToList();

            if (shopsToReturn.Count != 0)
            {
                return shopsToReturn;
            }
            return null;
        }

        public Product AddProduct(Product product, string folderPath)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbProduct = new Products
            {
                NameRu = product.NameRu,
                NameKg = product.NameKg,
                InformationRu = product.InformationRu,
                InformationKg = product.InformationKg,
                Amount = product.Amount,
                Price = product.Price,
                ShopId = product.ShopId,
                AvailableAmount = product.AvailableAmount,
                IsAvailable = product.IsAvailable
            };
            context.Products.Add(dbProduct);
            context.SaveChanges();
            if (!string.IsNullOrEmpty(product.ImagePath))
            {
                dbProduct.ImagePath = ConvertFileHelper.SaveProductImage(folderPath, product.ImagePath, $"{dbProduct.Id}");
            }
            context.SaveChanges();
            return new Product
            {
                Id = dbProduct.Id,
                NameRu = dbProduct.NameRu,
                NameKg = dbProduct.NameKg,
                InformationRu = dbProduct.InformationRu,
                InformationKg = dbProduct.InformationKg,
                Amount = dbProduct.Amount,
                ImagePath = dbProduct.ImagePath,
                ShopId = dbProduct.ShopId,
                AvailableAmount = dbProduct.AvailableAmount,
                Price = dbProduct.Price,
                IsAvailable = dbProduct.IsAvailable
            };
        }

        public Product ChangeProduct(Product product, string folderPath)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var productContext = context.Products.FirstOrDefault(x => x.Id == product.Id);
                    productContext.NameRu = product.NameRu;
                    productContext.NameKg = product.NameKg;
                    productContext.InformationRu = product.InformationRu;
                    productContext.InformationKg = product.InformationKg;
                    productContext.Amount = product.Amount;
                    productContext.Price = product.Price;
                    productContext.ShopId = product.ShopId;
                    productContext.AvailableAmount = product.AvailableAmount;
                    productContext.IsAvailable = product.IsAvailable;
                    context.SaveChanges();
                    if (!string.IsNullOrEmpty(product.ImagePath))
                    {
                        productContext.ImagePath = ConvertFileHelper.SaveProductImage(folderPath, product.ImagePath, $"{product.Id}");
                    }
                    context.SaveChanges();

                    return new Product
                    {
                        Id = productContext.Id,
                        NameRu = productContext.NameRu,
                        InformationRu = productContext.InformationRu,
                        Amount = productContext.Amount,
                        ImagePath = productContext.ImagePath,
                        ShopId = productContext.ShopId,
                        AvailableAmount = productContext.AvailableAmount,
                        Price = productContext.Price
                    };
                }
                catch (Exception)
                {
                    return null;
                }
            }

        }

        public string RemoveProduct(Product product)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var productContext = context.Products.FirstOrDefault(x => x.Id == product.Id);
            if (productContext != null)
            {
                context.Products.Remove(productContext);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        public List<Product> GetProductShop(int categoryId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {

                IQueryable<Products> products = context.Products.Where(x => x.IsDeleted == false && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);


                if (categoryId > 0)
                    products = products.Where(x => x.CategoryId == categoryId);


                return products.ToList().Select(x => new Product
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    AvailableAmount = GetProductAvailableAmount(x.Id, userId),
                    ImagePath = x.ImagePath,
                    InformationRu = x.InformationRu,
                    InformationKg = x.InformationKg,
                    IsAvailable = x.IsAvailable,
                    NameRu = x.NameRu,
                    NameKg = x.NameKg,
                    Price = x.Price,
                    ShopId = x.ShopId

                }).ToList();

            }
        }

        private int GetProductAvailableAmount(int productId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var product = context.Products.SingleOrDefault(x => x.Id == productId);
                if (product == null)
                    return 0;

                var userProducts = context.PurchaseHistories.Where(x => x.ProductId == productId && x.UserId == userId);
                int productAmount = 0;
                foreach (var item in userProducts)
                {
                    productAmount += item.Amount;
                }

                return product.AvailableAmount - productAmount;
            }
        }

        public BuyProductResponse BuyProduct(Models.BuyProduct buyProduct, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var product = context.Products.SingleOrDefault(x => x.Id == buyProduct.ProductId);
                var user = context.Users.SingleOrDefault(x => x.Id == userId);
                int userPurchasesCount = context.PurchaseHistories.Count(x => x.ProductId == buyProduct.ProductId && x.UserId == userId);

                if (product == null)
                    return new BuyProductResponse { ErrorMessage = Resources.ProductNotFound };

                if (user == null)
                    return new BuyProductResponse { ErrorMessage = Resources.UserNotFound };

                if (userPurchasesCount >= product.AvailableAmount)
                    return new BuyProductResponse { ErrorMessage = Resources.PurchaseLimitExceeded };

                if (product.Price > user.Wincoins)
                    return new BuyProductResponse { ErrorMessage = Resources.NotEnoughWincoinsToPay };

                if (buyProduct.Amount > product.Amount || buyProduct.Amount < 1 || product.Amount == 0)
                    return new BuyProductResponse { ErrorMessage = Resources.NotEnoughProductAmount };


                product.Amount -= 1;
                user.Wincoins -= product.Price;

                PurchaseHistories histories = new PurchaseHistories
                {
                    Amount = 1,
                    CreationDate = DateTime.Now,
                    Price = product.Price,
                    ProductId = product.Id,
                    UserId = user.Id,
                    StatusId = 1 // PurchaseStatuses: 1 - Не выдан
                };
                context.PurchaseHistories.Add(histories);

                context.SaveChanges();

                return new BuyProductResponse
                {
                    Id = product.Id,
                    Name = product.NameRu,
                    Amount = buyProduct.Amount,
                    Sum = buyProduct.Amount * product.Price,
                    ShopAddress = product.Shops.Address,
                    ShopPhone = product.Shops.Phone
                };

            }
        }

        public int GetProductPagesCount(int shopId, int quantity)
        {
            var context = new StayTrueDBEntities();
            return (context.Products.Count(x => x.ShopId == shopId) + quantity - 1) / quantity;
        }

        public List<CategoriesResponse> GetProducts(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var categories = context.ProductsCategories.Where(c => c.IsActive && !c.IsDeleted).ToList();
                return categories.Select(c => new CategoriesResponse
                    {
                        Name = c.Name,
                        Image = c.Image,
                        Products = c.Products.Where(p =>
                                !p.IsDeleted & p.IsAvailable && p.PublicationDate <= DateTime.Now).ToList()
                            .Select(p => new ProductsResponse
                            {
                                Id = p.Id,
                                Name = p.NameRu,
                                Information = p.InformationRu,
                                Amount = p.Amount,
                                Available = GetProductAvailableAmount(p.Id, userId),
                                ImagePath = p.ImagePath,
                                Price = p.Price
                            }).ToList()
                    }).ToList();
            }
        }
    }
}
