﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common;
using DataAccessLayer;
using StayTrueApiService.Helpers;
using StayTrueApiService.Models;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class AlbumManager
    {
        #region AlbumModel
        public AlbumModel AddCategoryAlbum(AlbumModel albumModel, string folderPath)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var dbAlbum = new Albums
                        {
                            Name = albumModel.Name,
                            CreationDate = DateTime.Now,
                            IsActive = albumModel.IsActive
                        };
                        context.Albums.Add(dbAlbum);
                        context.SaveChanges();
                        if (!string.IsNullOrEmpty(albumModel.Image))
                            dbAlbum.Image =
                                ConvertFileHelper.SaveAlbumImage(folderPath, albumModel.Image, $"{dbAlbum.Id}");
                        context.SaveChanges();

                        dbContextTransaction.Commit();
                        return new AlbumModel
                        {
                            Id = dbAlbum.Id,
                            Name = dbAlbum.Name,
                            CreationDate = dbAlbum.CreationDate,
                            IsActive = dbAlbum.IsActive,
                            Image = dbAlbum.Image
                        };
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return null;
                    }
                }
            }
        }


        public AlbumImageModel EditImageInAlbum(AlbumImageModel ai, string folderPath)
        {
            var context = new StayTrueDBEntities();
            var dbImage = context.AlbumImages.SingleOrDefault(x => x.Id == ai.Id);
            try
            {
                if (dbImage != null)
                {
                    dbImage.Image = ConvertFileHelper.SaveImageInAlbum(folderPath, ai.Image, ai.Id, ai.CategoryId);
                    context.SaveChanges();
                    return new AlbumImageModel
                    {
                        Id = ai.Id,
                        CategoryId = ai.CategoryId,
                        Image = ai.Image
                    };
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<AlbumModel> GetAlbums(bool? isBranded, int pageSize, int page)
        {
            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    IQueryable<Albums> albums = context.Albums
                        .Where(x => x.IsDeleted == false && x.IsActive && DateTime.Now >= x.PublicationDate)
                        .OrderByDescending(x => x.Id);

                    if (isBranded.HasValue)
                        albums = albums.Where(x => x.IsBrended == isBranded);

                    if (pageSize > 0)
                        albums = albums.Skip(pageSize * page);

                    if (page > 0)
                        albums = albums.Take(pageSize);

                    return albums.ToList().Select(x => new AlbumModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        CreationDate = x.CreationDate,
                        IsActive = x.IsActive,
                        Image = x.Image

                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string GetUrl(string folderUrl, string imageUrl)
        {
            return string.IsNullOrEmpty(imageUrl) ? null : $"{folderUrl}{imageUrl}";
        }
        #endregion

        #region AlbumModel gallery

        public AlbumImageModel AddAlbumImage(AlbumImageModel albumImage, string folderPath)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var dbImage = new AlbumImages
                        {
                            CategoryId = albumImage.CategoryId,
                            IsBrand = albumImage.IsBrand
                        };
                        context.AlbumImages.Add(dbImage);
                        context.SaveChanges();

                        if (!string.IsNullOrEmpty(albumImage.Image))
                        {
                            dbImage.Image = ConvertFileHelper.SaveImageInAlbum(folderPath, albumImage.Image, dbImage.Id, albumImage.CategoryId);
                            dbImage.SmallImage = ConvertFileHelper.SaveSmallImageInAlbum(folderPath,
                                dbImage.Id, albumImage.CategoryId);
                        }
                        if (!string.IsNullOrEmpty(albumImage.Video))
                        {
                            dbImage.Video = ConvertFileHelper.SaveGalleryVideo(folderPath, albumImage.Video, $"{dbImage.Id}");
                        }
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        return new AlbumImageModel
                        {
                            Id = dbImage.Id,
                            CategoryId = dbImage.CategoryId,
                            Image = dbImage.Image,
                            SmallImage = dbImage.SmallImage
                        };
                    }
                    catch (Exception e)
                    {
                        File.AppendAllText(@"C:\Error.txt", $"{e.Message}\n");
                        dbContextTransaction.Rollback();
                        return null;
                    }
                }
            }
        }

        public List<AlbumImageModel> GetAlbumImages(int albumId, bool? isBranded, int pageSize, int page)
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<AlbumImages> albumsImages = context.AlbumImages
                    .Where(x => x.CategoryId == albumId && x.IsActive)
                    .OrderByDescending(x => x.Id);

                if (isBranded.HasValue)
                    albumsImages = albumsImages.Where(x => x.IsBrand == isBranded);

                if (pageSize > 0)
                    albumsImages = albumsImages.Skip(pageSize * page);

                if (page > 0)
                    albumsImages = albumsImages.Take(pageSize);

                return albumsImages.ToList().Select(x => new AlbumImageModel()
                {
                    Id = x.Id,
                    CategoryId = x.CategoryId,
                    Image = x.Image,
                    SmallImage = x.SmallImage,
                    IsActive = x.IsActive,
                    IsBrand = (bool)x.IsBrand,
                    Video = x.Video
                }).ToList();
            }
        }

        public AlbumModel EditAlbum(AlbumModel albumModel, string folderPath)
        {
            var context = new StayTrueDBEntities();
            var contextAlbum = context.Albums.SingleOrDefault(x => x.Id == albumModel.Id);
            if (contextAlbum != null)
            {
                contextAlbum.Name = albumModel.Name;
                contextAlbum.IsActive = albumModel.IsActive;
                if (!contextAlbum.Image.Equals(albumModel.Image))
                {
                    contextAlbum.Image = ConvertFileHelper.SaveImage(folderPath, albumModel.Image, $"{albumModel.Id}");
                }
                context.SaveChanges();
                return new AlbumModel
                {
                    Id = contextAlbum.Id,
                    Name = contextAlbum.Name,
                    IsActive = contextAlbum.IsActive,
                    CreationDate = contextAlbum.CreationDate,
                    Image = contextAlbum.Image
                };
            }
            return null;
        }

        public string DeleteImageInAlbum(AlbumImageModel ai)
        {
            var context = new StayTrueDBEntities();
            var dbImage = context.AlbumImages.SingleOrDefault(x => x.Id == ai.Id);

            if (dbImage != null)
            {
                context.AlbumImages.Remove(dbImage);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        public string DeleteCategory(AlbumModel albumModel)
        {
            var context = new StayTrueDBEntities();
            var dbCategory = context.Albums.SingleOrDefault(x => x.Id == albumModel.Id);
            if (dbCategory != null)
            {
                var images = context.AlbumImages.Where(x => x.CategoryId == albumModel.Id).OrderByDescending(x => x.Id);
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        if (images.Count() != 0)
                        {
                            context.AlbumImages.RemoveRange(images);
                            context.SaveChanges();
                        }
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return Resources.UnknownError;
                    }
                    dbContextTransaction.Commit();
                }
                context.Albums.Remove(dbCategory);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        #endregion
    }
}