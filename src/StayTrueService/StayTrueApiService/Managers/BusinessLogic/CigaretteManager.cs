﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class CigaretteManager
    {

        public Dictionary<string, string[]> GetAllModels()
        {
            using (var context = new StayTrueDBEntities())
            {
                var cigaretteModels =  context.CigaretteModels.ToList();
                if (cigaretteModels==null)
                    return null;
                var res = cigaretteModels.Select(m => new
                        {brand = m.ModelName, types = m.CigaretteModelTypes.Select(t => t.TypeName)}).ToList()
                    .Select(m => new {m.brand, types = m.types.ToArray()}).ToDictionary(m => m.brand, m => m.types);
                return res;
            }
        }

        public List<CigaretteBrandResponse> GetAllBrands()
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<CigaretteBrands> brands = context.CigaretteBrands.Where(b => b.IsActive && !b.IsDeleted)
                    .OrderBy(x => x.BrandPosition);

                if (brands==null)
                    return null;

                return brands.Select(b => new CigaretteBrandResponse
                {
                    Id = b.Id,
                    BrandPosition = b.BrandPosition,
                    BrandName = b.BrandName,
                    Description = b.Description,
                    Tar = b.Tar,
                    PackSize = b.CigarettePackSizes.SizeName,
                    Nicotine = b.Nicotine,
                    CreationDate = b.CreationDate,
                    IsActive = b.IsActive,
                    IsDeleted = b.IsDeleted,
                    AdditionalInfo1 = b.AdditionalInfo1,
                    AdditionalInfo2 = b.AdditionalInfo2,
                    AdditionalInfo3 = b.AdditionalInfo3,
                    AdditionalInfo4 = b.AdditionalInfo4,
                    AdditionalImage1 = b.AdditionalImage1,
                    AdminImage = b.AdminImage,
                    BrandImageP = b.BrandImageP,
                    AdditionalImage2 = b.AdditionalImage2,
                    AdditionalImage3 = b.AdditionalImage3,
                    DescriptionImageM = b.DescriptionImageM,
                    BrandImageM = b.BrandImageM,
                    DescriptionImageP = b.DescriptionImageP,
                    AdditionalImage4 = b.AdditionalImage4,
                    BrandCompany = b.BrandCompany
                }).ToList();
            }
        }

        public CigaretteBrandResponse GetBrand(int id)
        {
            using (var context = new StayTrueDBEntities())
            {
                var brand = context.CigaretteBrands.Find(id);
                if (brand != null)
                {
                    return new CigaretteBrandResponse
                    {
                        Id = brand.Id,
                        BrandPosition = brand.BrandPosition,
                        BrandName = brand.BrandName,
                        Description = brand.Description,
                        Tar = brand.Tar,
                        PackSize = brand.CigarettePackSizes.SizeName,
                        Nicotine = brand.Nicotine,
                        CreationDate = brand.CreationDate,
                        IsActive = brand.IsActive,
                        IsDeleted = brand.IsDeleted,
                        AdditionalInfo1 = brand.AdditionalInfo1,
                        AdditionalInfo2 = brand.AdditionalInfo2,
                        AdditionalInfo3 = brand.AdditionalInfo3,
                        AdditionalInfo4 = brand.AdditionalInfo4,
                        AdditionalImage1 = brand.AdditionalImage1,
                        AdminImage = brand.AdminImage,
                        BrandImageP = brand.BrandImageP,
                        AdditionalImage2 = brand.AdditionalImage2,
                        AdditionalImage3 = brand.AdditionalImage3,
                        DescriptionImageM = brand.DescriptionImageM,
                        BrandImageM = brand.BrandImageM,
                        DescriptionImageP = brand.DescriptionImageP,
                        AdditionalImage4 = brand.AdditionalImage4
                    };
                }

                return null;
            }
        }
    }
}