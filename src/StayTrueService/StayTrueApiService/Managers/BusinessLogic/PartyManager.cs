﻿using Common;
using Common.Security;
using DataAccessLayer;
using Newtonsoft.Json;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace StayTrueApiService.Managers.BusinessLogic
{
    public class PartyManager
    {
        public QuizSessionResponse GetPartyQuizSession(int quizId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.PartyQuizzSession.Where(q => q.UserId == userId && q.QuizzId == quizId)
                    .OrderByDescending(q => q.Id).FirstOrDefault();


                if (dbQuizSession == null)
                    return new QuizSessionResponse() { ErrorMessage = Resources.SessionNotFound };

                QuizSessionResponse quizSession = new QuizSessionResponse
                {
                    Id = dbQuizSession.Id,
                    UserId = dbQuizSession.UserId,
                    IsCompleted = dbQuizSession.IsCompleted,
                    QuestionIndex = dbQuizSession.QuestionIndex,
                    QuizId = dbQuizSession.QuizzId,
                    Score = dbQuizSession.Score
                };
                return quizSession;
            }
        }
        public PartyResponse GetPartyDetails(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Party party = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false).Include(p => p.PartyQuestionnaire).OrderByDescending(p => p.Id).FirstOrDefault();

                if (party == null)
                {
                    return new PartyResponse { ErrorMessage = Resources.PartyWasNotFound, IsActive = false };
                }

                PartyUsers users = context.PartyUsers.Where(p => p.PartyId == party.Id && p.UserId == userId).FirstOrDefault();

                if (users != null)
                {
                    if (users.IsBlocked)
                    {
                        return new PartyResponse { ErrorMessage = Resources.PartyUserNotFound, IsBlocked = false };
                    }
                }

                if (DateTime.Now > party.EndDate)
                {
                    party.IsDisabled = true;
                    context.SaveChanges();
                }

                PartyQuestionnaire questionnaire = party.PartyQuestionnaire.FirstOrDefault(q => q.PartyId == party.Id);


                List<Models.ResponseModels.PartyGames> games = new List<Models.ResponseModels.PartyGames>();
                List<Models.ResponseModels.PartyQuizzes> quizzes = new List<Models.ResponseModels.PartyQuizzes>();
                List<PartyQuestion> questions = new List<PartyQuestion>();

                if (questionnaire != null)
                {
                    AddQuestions(questionnaire, questions);
                }

                PartyResponse partyDetails = new PartyResponse()
                {
                    Name = party.Name,
                    Description = party.Description,
                    DescriptionForMobile = party.DescriptionForMobile,
                    Questions = questions,
                    ImagePreview = party.ImagePreview,
                    ImagePreviewSmall = party.ImagePreviewSmall,
                    IsActive = party.IsActive,
                    IsDisabled = party.IsDisabled,
                    IsInvited = false
                };

                SerializeGames(party.PartyGames.ToList(), games);
                SerializeQuizzess(party.PartyQuizzes.ToList(), quizzes);

                partyDetails.PartyGames = games;
                partyDetails.PartyQuizzes = quizzes;

                if (users != null)
                {
                    partyDetails.IsAnswered = true;
                    partyDetails.IsInvited = users.IsInvited;
                    partyDetails.IsBlocked = users.IsBlocked;
                }

                return partyDetails;

            }
        }

        public async Task<PartyAnswersResponse> PartyRegistrationAnswer(PartyAnswers partyAnswers, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Party party = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false)
                    .Include(p => p.PartyQuestionnaire).OrderByDescending(p => p.Id).FirstOrDefault();

                if (party == null)
                    return new PartyAnswersResponse {ErrorMessage = Resources.PartyWasNotFound};

                var questionnaire = party.PartyQuestionnaire.FirstOrDefault(q => q.PartyId == party.Id);

                if (questionnaire == null)
                    return new PartyAnswersResponse {ErrorMessage = Resources.QuestionnaireNotFound};

                var answerUser = questionnaire.PartyQuestionnaireAnswer.FirstOrDefault(p => p.UserId == userId);
                if (answerUser != null)
                    return new PartyAnswersResponse {ErrorMessage = Resources.QuestionnaireAnswered};

                var answer = new PartyQuestionnaireAnswer
                {
                    PartyQuestionnaireId = questionnaire.Id,
                    Answer1 = partyAnswers.Answer1,
                    Answer2 = partyAnswers.Answer2,
                    Answer3 = partyAnswers.Answer3,
                    Answer4 = partyAnswers.Answer4,
                    Answer5 = partyAnswers.Answer5,
                    Answer6 = partyAnswers.Answer6,
                    Answer7 = partyAnswers.Answer7,
                    Answer8 = partyAnswers.Answer8,
                    Answer9 = partyAnswers.Answer9,
                    Answer10 = partyAnswers.Answer10,
                    CreationDate = DateTime.Now,
                    UserId = userId
                };

                var user = new PartyUsers
                {
                    UserId = userId,
                    PartyId = party.Id,
                    IsBlocked = false,
                    IsInvited = false,
                    HasCome = false,
                    CreationDate = DateTime.Now,
                    IsAnswered = true
                };

                context.PartyQuestionnaireAnswer.Add(answer);
                context.PartyUsers.Add(user);

                context.SaveChanges();

                return new PartyAnswersResponse {PartyAnswers = partyAnswers};
            }
        }

        public GameScore SavePartyGameResult(GameScore gameScore, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                string score = AesCrypto.DecryptStringAES(userId.ToString(), gameScore.Result);

                gameScore = JsonConvert.DeserializeObject<GameScore>(score);

                var party = context.Party.OrderByDescending(p => p.Id).FirstOrDefault();

                if(party==null)
                    return new GameScore(){ErrorMessage = Resources.PartyWasNotFound};

                PartyGameSession gameSession = new PartyGameSession
                {
                    SessionStart = DateTime.Now,
                    SessionEnd = DateTime.Now,
                    Score = gameScore.Points,
                    UserId = userId,
                    GameId = gameScore.GameId
                };

                PartyUsers user = context.PartyUsers.FirstOrDefault(u => u.UserId == gameScore.UserId && u.PartyId == party.Id);

                user.Points += gameScore.Points;

                context.PartyGameSession.Add(gameSession);
                context.SaveChanges();

                return gameScore;
            }
        }

        public PartyUser CheckPartyUserStatus(int id)
        {
            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.Find(id);

                if (user == null)
                {
                    return new PartyUser { ErrorMessage = Resources.PartyUserNotFound};
                }
                var activeParty = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false).Include(p => p.PartyUsers).OrderByDescending(p => p.Id).FirstOrDefault();

                if (activeParty == null)
                {
                    return new PartyUser {ErrorMessage = Resources.PartyWasNotFound};
                }

                var partyUser = activeParty.PartyUsers.FirstOrDefault(u => u.UserId == user.Id);

                if (partyUser == null)
                {
                    return new PartyUser {ErrorMessage = Resources.UserNotInParty};
                }

                if (partyUser.IsInvited == false)
                {
                    return new PartyUser {ErrorMessage = Resources.PartyUserNotInvited};
                }

                if (partyUser.HasCome)
                {
                    return new PartyUser {ErrorMessage = Resources.UserHasCome};
                }

                return new PartyUser
                {
                    UserId = user.Id,
                    Phone = user.Phone,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
            }
        }

        public string ChangePartyUserHasComeStatus(int? id)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var activeParty = context.Party
                        .Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false)
                        .Include(p => p.PartyUsers).OrderByDescending(p => p.Id).FirstOrDefault();

                    if (activeParty == null)
                        return Resources.PartyWasNotFound;

                    var user = activeParty.PartyUsers.FirstOrDefault(p => p.UserId == id);

                    if (user == null)
                        return Resources.PartyUserNotFound;

                    if (user.IsInvited == false)
                        return Resources.PartyUserNotInvited;

                    user.HasCome = true;
                    context.SaveChanges();
                    return null;
                }
                catch (DbUpdateException ex)
                {
                    return ex.Message;
                }
            }
        }

        private List<Models.ResponseModels.PartyGames> SerializeGames(List<DataAccessLayer.PartyGames> partyGames, List<Models.ResponseModels.PartyGames> games)
        {
            if (partyGames != null)
            {

                foreach (var par in partyGames)
                {
                    Models.ResponseModels.PartyGames game = new Models.ResponseModels.PartyGames()
                    {
                        GameUrl = par.GamePath,
                        Image = par.GameImage,
                        Id = par.Id,
                        IsActive = par.IsActive,
                        Name = par.Name,
                        PublicationDate = par.PublicationDate,
                        Points = par.Points
                    };
                    games.Add(game);
                }
            }

            return games;
        }
        private List<Models.ResponseModels.PartyQuizzes> SerializeQuizzess(List<DataAccessLayer.PartyQuizzes> partyQuizzes, List<Models.ResponseModels.PartyQuizzes> quizzes)
        {
            if (partyQuizzes != null)
            {
                partyQuizzes = partyQuizzes.OrderBy(x => x.Id).ToList();
                Models.ResponseModels.PartyQuizzes quizz = new Models.ResponseModels.PartyQuizzes()
                {
                    Url = partyQuizzes[partyQuizzes.Count - 1].QuizzPth,
                    Image = partyQuizzes[partyQuizzes.Count - 1].QuizzImage,
                    Id = partyQuizzes[partyQuizzes.Count - 1].Id,
                    Name = partyQuizzes[partyQuizzes.Count - 1].QuizzName
                };
                quizzes.Add(quizz);
            }


            return quizzes;
        }

        private void AddQuestions(PartyQuestionnaire questionnaire, List<PartyQuestion> questions)
        {
            if (questionnaire.Question1 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question1 });
            }
            if (questionnaire.Question2 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question2 });
            }

            if (questionnaire.Question3 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question3 });
            }
            if (questionnaire.Question4 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question4 });
            }
            if (questionnaire.Question5 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question5 });
            }
            if (questionnaire.Question6 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question6 });
            }
            if (questionnaire.Question7 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question7 });
            }
            if (questionnaire.Question8 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question8 });
            }
            if (questionnaire.Question9 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question9 });
            }
            if (questionnaire.Question10 != null)
            {
                questions.Add(new PartyQuestion() { Question = questionnaire.Question10 });
            }
        }
    }
}