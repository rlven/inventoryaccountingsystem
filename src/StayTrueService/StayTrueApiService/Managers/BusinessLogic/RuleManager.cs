﻿using Common.Enums;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers.BusinessLogic
{
    public class RuleManager
    {
        public RulesResponse GetRulesOfUse()
        {
            using (var context = new StayTrueDBEntities())
            {
                var ru = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesRu))?.Value;
                var kg = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesKg))
                    ?.Value;
                if(string.IsNullOrEmpty(ru) || string.IsNullOrEmpty(kg))
                    return new RulesResponse {ErrorMessage = Resources.RulesNotFound};

                return new RulesResponse
                {
                    RulesOfUseRu = ru,
                    RulesOfUseKg = kg
                };
            }
        }

        public TermOfUseResponse GeTermOfUse()
        {
            using (var context = new StayTrueDBEntities())
            {
                var ru = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseRu))?.Value;
                var kg = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseKg))
                    ?.Value;
                if (string.IsNullOrEmpty(ru) || string.IsNullOrEmpty(kg))
                    return new TermOfUseResponse {ErrorMessage = Resources.RulesNotFound};

                return new TermOfUseResponse()
                {
                    TermOfUseKg = kg,
                    TermOfUseRu = ru
                };
            }

        }
    }
}