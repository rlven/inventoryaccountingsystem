﻿using Common.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Common;
using StayTrueApiService.Models.ResponseModels;
using Common.Logger;
using StayTrueApiService.Models;
using Common.Security;
using Newtonsoft.Json;
using GameScore = StayTrueApiService.Models.GameScore;
using System.Threading.Tasks;
using QuizSession = StayTrueApiService.Models.QuizSession;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class GameManager
    {
        public IEnumerable<GameResponse> GetGames()
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<Games> games = context.Games.Where(x => x.IsActive == true && x.IsDeleted == false && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);

                if (games == null)
                    return null;

                return games.ToList().Select(x => new GameResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    GameUrl = x.GameUrl,
                    Image = x.ImagePath,
                    Points = x.Wincoins
                }).ToList();
            }
        }
        public List<UserGameHistoryResponse> GameHistory(int userId, int pageSize, int page)
        {
            using (var context = new StayTrueDBEntities())
            {
                var gameSessions = context.GameSession.Where(x => x.UserId == userId)
                    .OrderByDescending(x => x.Id).Skip(pageSize * page).Take(pageSize).ToList();

                if (gameSessions.Count != 0)
                {
                    return gameSessions.Select(x => new UserGameHistoryResponse
                    {
                        Id = x.Id,
                        SessionTime = x.SessionEnd,
                        GameName = x.Games.Name,
                        ImagePath = x.Games.ImagePath,
                        Score = x.Score
                    }).ToList();
                }
            }

            return new List<UserGameHistoryResponse>();
        }

        public GameScoreResponse UserBestScore(int gameId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Id == userId);

                if (user == null)
                    return new GameScoreResponse { ErrorMessage = Resources.UserNotFound };

                var game = context.Games.Find(gameId);
                if (game == null)
                    return new GameScoreResponse { ErrorMessage = Resources.GameNotFound };

                var bestScore = game.GameSession
                                    .Where(s => s.UserId == userId)
                                    .OrderByDescending(s => s.Score)
                                    .FirstOrDefault()?.Score ?? 0;

                return new GameScoreResponse { UserId = userId, GameId = gameId, BestScore = bestScore };
            }
        }
        public async Task<GameScore> AddGameResultAsync(GameScore gameScore, int userId)
        {
            string score = AesCrypto.DecryptStringAES(userId.ToString(), gameScore.Result);

            gameScore = JsonConvert.DeserializeObject<GameScore>(score);
            gameScore.Points = gameScore.Score;
            gameScore.UserId = userId;

            int userMaxScore = 0;
            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    var userGameSessions =
                        await context.GameSession.Where(s => s.GameId == gameScore.GameId && s.UserId == gameScore.UserId).ToListAsync();

                    if (userGameSessions != null && userGameSessions.Count != 0)
                    {
                        userMaxScore = userGameSessions.Select(s => s.Score).Max();
                    }

                    if (userMaxScore >= gameScore.Score)
                    {
                        return new GameScore() { ErrorMessage = Resources.MaxScoreNotBitten };
                    }

                    GameSession gameSession = new GameSession();

                    gameSession.SessionStart = DateTime.Now;
                    gameSession.SessionEnd = DateTime.Now;
                    gameSession.Score = gameScore.Points;
                    gameSession.UserId = gameScore.UserId;
                    gameSession.GameId = gameScore.GameId;

                    Games game = context.Games.SingleOrDefault(x => x.Id == gameScore.GameId);
                    Users user = context.Users.SingleOrDefault(x => x.Id == gameScore.UserId);

                    int wincoinExchange = game.WincoinsExchange;
                    int maxWincoins = game.Wincoins;

                    int lastWincoinsSum = context.GameSession
                        .Where(x => x.UserId == gameScore.UserId && x.GameId == gameScore.GameId)
                        .Select(s => s.Wincoins).DefaultIfEmpty(0).Sum();

                    int newWincoinsSum = gameScore.Points / wincoinExchange;

                    int totalWincoinsSum = lastWincoinsSum + newWincoinsSum;

                    if (!(lastWincoinsSum >= maxWincoins))
                    {
                        if (totalWincoinsSum >= maxWincoins)
                        {
                            gameSession.Wincoins = maxWincoins - lastWincoinsSum;
                        }
                        else
                        {
                            gameSession.Wincoins = newWincoinsSum - lastWincoinsSum;
                        }

                        user.Wincoins += gameSession.Wincoins;
                    }

                    context.GameSession.Add(gameSession);
                    await context.SaveChangesAsync();

                    return gameScore;
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return new GameScore() { ErrorMessage = e.Message };
            }
        }

        public QuizSessionResponse SaveQuizSession(QuizSession quizSession, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var score = AesCrypto.DecryptStringAES(userId.ToString(), quizSession.Result);
                quizSession = JsonConvert.DeserializeObject<QuizSession>(score);
                quizSession.UserId = userId;
                var dbQuizSession = context.QuizzesSession.SingleOrDefault(q => q.QuizId == quizSession.QuizId && q.UserId == quizSession.UserId);

                if (dbQuizSession == null)
                {
                    var newQuizSession = new QuizzesSession
                    {
                        UserId = quizSession.UserId,
                        CreationDate = DateTime.Now,
                        IsCompleted = quizSession.IsCompleted,
                        QuestionIndex = quizSession.QuestionIndex,
                        QuizId = quizSession.QuizId,
                        Score = quizSession.Score
                    };

                    context.QuizzesSession.Add(newQuizSession);
                    context.SaveChanges();

                    return new QuizSessionResponse
                    {
                        UserId = quizSession.UserId,
                        CreationDate = DateTime.Now,
                        IsCompleted = quizSession.IsCompleted,
                        QuestionIndex = quizSession.QuestionIndex,
                        QuizId = quizSession.QuizId,
                        Score = quizSession.Score
                    };
                }

                dbQuizSession.IsCompleted = quizSession.IsCompleted;
                dbQuizSession.QuestionIndex = quizSession.QuestionIndex;
                dbQuizSession.Score = quizSession.Score;

                if (quizSession.IsCompleted)
                {
                    var user = context.Users.SingleOrDefault(x => x.Id == quizSession.UserId);
                    if (user == null)
                    {
                        return new QuizSessionResponse {ErrorMessage = Resources.UserNotFound};
                    }
                    user.Wincoins += dbQuizSession.Quizzes.Wincoins;
                }

                context.SaveChanges();
                quizSession.Id = dbQuizSession.Id;

                return new QuizSessionResponse
                {
                    UserId = quizSession.UserId,
                    CreationDate = DateTime.Now,
                    IsCompleted = quizSession.IsCompleted,
                    QuestionIndex = quizSession.QuestionIndex,
                    QuizId = quizSession.QuizId,
                    Score = quizSession.Score
                };
            }
        }
        public List<QuizResponse> GetQuizzes()
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<Quizzes> quizzes = context.Quizzes.Where(x => x.IsActive && x.IsDeleted == false && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);

                return quizzes.ToList().Select(x => new QuizResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    Url = x.Url,
                    Image = x.ImagePath,
                    Points = x.Wincoins,
                    Description = x.Description
                }).ToList();
            }
        }

        public QuizSessionResponse GetQuizSession(int quizId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.QuizzesSession.SingleOrDefault(q => q.UserId == userId && q.QuizId == quizId);

                if (dbQuizSession == null)
                    return new QuizSessionResponse {ErrorMessage = Resources.SessionNotFound};

                var quizSession = new QuizSessionResponse
                {
                    Id = dbQuizSession.Id,
                    UserId = dbQuizSession.UserId,
                    CreationDate = dbQuizSession.CreationDate,
                    IsCompleted = dbQuizSession.IsCompleted,
                    QuestionIndex = dbQuizSession.QuestionIndex,
                    QuizId = dbQuizSession.QuizId,
                    Score = dbQuizSession.Score
                };

                return quizSession;
            }
        }

        public GreatBattleQuizzResultResponse GetGreatBattleQuizSession(int tourId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.GreatBattleQuizzesSession.SingleOrDefault(q => q.UserId == userId && q.QuizId == tourId);

                if(dbQuizSession==null)
                    return new GreatBattleQuizzResultResponse() { ErrorMessage = Resources.SessionNotFound };

                GreatBattleQuizzResultResponse quizSession = new GreatBattleQuizzResultResponse
                {
                    UserId = dbQuizSession.UserId,
                    CreationDate = dbQuizSession.CreationDate,
                    IsCompleted = dbQuizSession.IsCompleted,
                    QuestionIndex = dbQuizSession.QuestionIndex,
                    GreatBattleTourId = dbQuizSession.QuizId,
                    Score = dbQuizSession.Score
                };

                return quizSession;
            }
        }


        public async Task<GreatBattleGameScoreResponse> SaveGreatBattleGameScore(GreatBattleGameScoreResponse gameScore)
        {
            using (var context = new StayTrueDBEntities())
            {

                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();

                if (greatBattle == null)
                {
                    return new GreatBattleGameScoreResponse { ErrorMessage = Resources.TargetNotFound };
                }


                GreatBattlesUsers users = context.GreatBattlesUsers.FirstOrDefault(u =>
                    u.UserID == gameScore.UserId && !u.IsBlocked && u.GreatBattleID == greatBattle.ID);

                if (users == null)
                {
                    return new GreatBattleGameScoreResponse { ErrorMessage = Resources.UserNotFound };
                }

                GreatBattleTours tour = context.GreatBattleTours.FirstOrDefault(t => t.GreatBattleID == greatBattle.ID && t.ID == gameScore.GreatBattleTourId);

                if (tour == null)
                    return new GreatBattleGameScoreResponse() { ErrorMessage = Resources.TourNotFound };

                GreatBattleTourPoints tourPoints = tour.GreatBattleTourPoints.FirstOrDefault(p => p.UserID == gameScore.UserId && p.GroupID == users.GroupID);

                int? maxUserScore = context.GreatBattleGamesSession.Where(s => s.TourId == gameScore.GreatBattleTourId && s.UserId == gameScore.UserId).Select(s => s.Score).Max();

                if (maxUserScore >= gameScore.Score)
                {
                    return new GreatBattleGameScoreResponse() { ErrorMessage = Resources.MaxScoreNotBitten };
                }

                if (tourPoints == null)
                {
                    GreatBattleTourPoints battleTourPoint = new GreatBattleTourPoints
                    {
                        GroupID = users.GroupID,
                        TourID = gameScore.GreatBattleTourId,
                        PointsCount = (int)(gameScore.Points * tour.PointRate),
                        UserID = gameScore.UserId
                    };

                    context.GreatBattleTourPoints.Add(battleTourPoint);
                }
                else
                {
                    tourPoints.PointsCount += (int)(gameScore.Points * tour.PointRate);
                }

                users.Points += (int)(gameScore.Points * tour.PointRate);

                GreatBattleGamesSession greatBattleGamesSession = new GreatBattleGamesSession()
                {
                    UserId = gameScore.UserId,
                    TourId = gameScore.GreatBattleTourId,
                    SessionEnd = DateTime.Now,
                    Score = gameScore.Score
                };
                context.GreatBattleGamesSession.Add(greatBattleGamesSession);
                await context.SaveChangesAsync();

                return gameScore;
            }
        }
    }
}