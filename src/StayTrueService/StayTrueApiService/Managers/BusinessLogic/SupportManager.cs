﻿using Common;
using DataAccessLayer;
using System;
using System.Linq;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers.BusinessLogic
{
    public class SupportManager
    {
        public SupportResponse SupportRequest(SupportRequest supportRequest)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var user = context.Users.SingleOrDefault(u => u.Phone == supportRequest.PhoneNumber);

                    if (user == null) return
                        new SupportResponse{ErrorMessage = Resources.AccountWasNotFound};

                    var userSupportPasswordDropRequest = context.SupportRequests.FirstOrDefault(r => r.UserId == user.Id && r.IsPasswordLost == true);
                    if (userSupportPasswordDropRequest != null && supportRequest.IsPasswordForgot)
                    {
                        userSupportPasswordDropRequest.Email = supportRequest.Email;
                        userSupportPasswordDropRequest.Phone = supportRequest.PhoneNumber;
                        userSupportPasswordDropRequest.Body = supportRequest.Body;
                        userSupportPasswordDropRequest.UserId = user.Id;
                        userSupportPasswordDropRequest.CreationDate = DateTime.Now;
                        userSupportPasswordDropRequest.IsPasswordLost = true;
                        userSupportPasswordDropRequest.IsAnswered = false;
                        userSupportPasswordDropRequest.IsMessageSent = false;

                        context.SaveChanges();
                    }
                    else
                    {

                        var dbRequest = new SupportRequests
                        {
                            UserId = user.Id,
                            Body = supportRequest.Body,
                            CreationDate = DateTime.Now,
                            Phone = supportRequest.PhoneNumber,
                            Email = supportRequest.Email,
                            IsAnswered = false,
                            IsPasswordLost = supportRequest.IsPasswordForgot
                        };
                        context.SupportRequests.Add(dbRequest);

                        context.SaveChanges();
                    }

                    return new SupportResponse
                    {
                        Body = supportRequest.Body,
                        Email = supportRequest.Email,
                        PhoneNumber = supportRequest.PhoneNumber,
                        IsPasswordForgot = supportRequest.IsPasswordForgot
                    };

                }
                catch (Exception ex)
                {
                    return new SupportResponse {ErrorMessage = ex.Message};
                }
            }
        }
    }
}