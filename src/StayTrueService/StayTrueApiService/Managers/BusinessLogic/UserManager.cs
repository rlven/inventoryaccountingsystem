﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using Common;
using Common.Enums;
using Common.Logger;
using Common.Models;
using Common.Security;
using DataAccessLayer;
using StayTrueApiService.Helpers;
using StayTrueApiService.Models;
using StayTrueApiService.Managers.Services;
using StayTrueApiService.Models.ResponseModels;
using UserAuth = StayTrueApiService.Models.UserAuth;
using UserInfo = StayTrueApiService.Models.UserInfo;
using System.Threading.Tasks;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class UserManager
    {
        private static readonly string ChatUrl = ConfigurationManager.AppSettings["ChatServiceUrl"];
        private static readonly string ChatLogin = ConfigurationManager.AppSettings["ChatLogin"];
        private static readonly string ChatPassword = ConfigurationManager.AppSettings["ChatPassword"];
        private static readonly string FolderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly int SignInAttemptLimit = 3;
        private const int imageHeight = 150;
        private static readonly string FirstSignInGiftAmount = ConfigurationManager.AppSettings["FirstSignInGiftAmount"];

        public async Task<UserAuthInfoResponse> AppAuthorizationUser(UserAuth account)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = await Task.Run(() =>  context.Users.SingleOrDefault(x => x.Phone == account.Phone));

                if (user == null)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.StatusId != (int)StatusEnum.Active)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.SignInAttemptCount >= SignInAttemptLimit)
                {
                    if (!ReCaptcha.ValidateTime(user.RefreshDate))
                    {
                        return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                    }
                }
                else
                {
                    user.RefreshDate = DateTime.Now;
                    await context.SaveChangesAsync();
                }

                if (!user.IsApproved)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.IsDeleted)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (!(ValidateBCryptePassword(account.Password, user.Password) && user.StatusId == (int)StatusEnum.Active))
                {
                    Log.Info(Resources.IncorrectLoginOrPassword);

                    user.SignInAttemptCount += 1;
                    await context.SaveChangesAsync();

                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                CheckUserFirstSignIn(user);

                user.SignInCounterM++;
                user.LastSignInM=DateTime.Now;
                user.SignInAttemptCount = 0;

                long utcTimeSec = (Int64)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                long utcTimeMs = (Int64)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

                var chatToken = CrossPlatformAESEncryption.Encrypt($"{utcTimeMs}/{user.Id}");

                context.ChatTokens.Add(new ChatTokens { insert_date = utcTimeSec, token = chatToken });

                context.SaveChanges();

                return new UserAuthInfoResponse
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Wincoins = user.Wincoins,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType,
                    ChatToken = chatToken
                };

            }
        }

        public async Task<UserAuthInfoResponse> AuthorizationUser(UserAuth account)
        {

            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = await Task.Run(() => context.Users.SingleOrDefault(x => x.Phone == account.Phone));

                if (user == null)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.StatusId != (int)StatusEnum.Active)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.SignInAttemptCount >= SignInAttemptLimit)
                {
                    if (!ReCaptcha.Validate(account.Captcha))
                    {
                        return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                    }
                }

                if (!user.IsApproved)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.IsDeleted)
                {
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (!ValidateBCryptePassword(account.Password, user.Password))
                {
                    user.SignInAttemptCount += 1; await context.SaveChangesAsync();
                    return new UserAuthInfoResponse { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }


                user.SignInCounterP++;
                user.LastSignInP=DateTime.Now;
                user.SignInAttemptCount = 0;

                long utcTimeSec = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                long utcTimeMs = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;

                String chatToken = CrossPlatformAESEncryption.Encrypt($"{utcTimeMs}/{user.Id}");

                context.ChatTokens.Add(new ChatTokens { insert_date = utcTimeSec, token = chatToken });

                await context.SaveChangesAsync();

                return new UserAuthInfoResponse
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Wincoins = user.Wincoins,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType,
                    ChatToken = chatToken
                };
            }
        }

        public RegisterUserResponse RegisterUser(RegisterUser user)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {

                if (context.Users.Any(x => x.Phone == user.Phone))
                {
                    return new RegisterUserResponse { ErrorMessage = Resources.UserExist };
                }

                Users dbUser = CreateNewUser(user);

                context.Users.Add(dbUser);
                context.SaveChanges();

                SavePassportToDirectory(user, dbUser);
                UpdateImagePath(dbUser);

                context.SaveChanges();

                return new RegisterUserResponse
                {
                    Id = dbUser.Id,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName
                };
            }
        }

        public bool UserIsExist(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var isExist = context.Users.Any(x => x.Id == userId);
                return isExist;
            }
        }

        public UserInfo UserInfo(int userId)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == userId);

                if (user == null)
                    return new UserInfo { ErrorMessage = Resources.AccountWasNotFound };

                UserInfo userInfo = new UserInfo
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Email = user.Email,
                    BirthDate = user.BirthDate,
                    IsApproved = user.IsApproved,
                    StatusId = user.StatusId,
                    Phone = user.Phone,
                    Wincoins = user.Wincoins,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType,
                    CityName = user.Cities.CityName,
                    IsActiveParty = false
                };

                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();
                Party party = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false)
                    .OrderByDescending(x => x.Id).FirstOrDefault();
                if (party != null)
                {
                    userInfo.IsActiveParty = true;
                    PartyUsers partyUser = party.PartyUsers.FirstOrDefault(u => u.UserId == userId && u.PartyId == party.Id);
                    if (partyUser != null)
                    {
                        userInfo.IsInvitedToParty = partyUser.IsInvited;
                        userInfo.IsBlockedFromParty = partyUser.IsBlocked;
                        userInfo.TicketCoins = partyUser.Points;
                    }
                }

                if (greatBattle != null)
                {
                    GreatBattlesUsers greatBattlesUser = greatBattle.GreatBattlesUsers.FirstOrDefault(u => u.UserID == userId);
                    
                    if (greatBattlesUser != null)
                    {
                        if (!greatBattlesUser.IsBlocked)
                        {
                            var firstTour = context.GreatBattleTours.Where(x => x.GreatBattleID == greatBattle.ID)
                                .OrderBy(x => x.BeginDateTime)
                                .FirstOrDefault();

                            userInfo.GreatBattlePoints = greatBattlesUser.Points;
                            userInfo.GreatBattleGroupId = greatBattlesUser.GroupID;
                            userInfo.CanChangeBattleSide =
                                !(firstTour?.EndDateTime < DateTime.Now) && greatBattlesUser.CanChangeSide;
                            userInfo.Rank = GetUserRank(greatBattle, userInfo.GreatBattlePoints);
                        }
                    }
                }

                return userInfo;
            }
        }

        public IsExistResponse PhoneIsExist(string phone)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = context.Users.FirstOrDefault(x => x.Phone == phone && x.IsDeleted == false);

                if (user == null)
                {
                    return new IsExistResponse { IsExist = false};
                }

                return new IsExistResponse { IsExist = true, ErrorMessage = Resources.PhoneIsAlreadyExist };
            }
        }

        public string EditAvatar(string avatar, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Id == userId);
                if (user == null) return string.Empty;
                user.NewAvatar = SaveAvatarToDirectory(avatar, userId.ToString(), "/Images/Avatars/NotApproved/");
                if (File.Exists($@"{FolderPath}{user.NewAvatar}"))
                {
                    user.IsAvatarApproved = false;
                }
                context.SaveChanges();
                return user.NewAvatar;
            }
        }

        public int GetUserWincoins(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.Find(userId);
                if (user != null)
                    return user.Wincoins;
            }
            return 0;
        }

        public string ChangeUserPassword(User user)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbUser = context.Users.SingleOrDefault(x => x.Phone == user.Phone);
            if (dbUser != null)
            {
                dbUser.Password = CreateBCrypt("");
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }


        public List<UserPurchaseHistoryResponse> PurchaseHistory(int userId, int pageSize, int pageNumber)
        {
            using (var context = new StayTrueDBEntities())
            {
                var purchaseHistory = context.PurchaseHistories.Where(x => x.UserId == userId)
                    .OrderByDescending(x => x.Id).Skip(pageSize * pageNumber).Take(pageSize).ToList();

                if (purchaseHistory.Any())
                {
                    return purchaseHistory.Select(x => new UserPurchaseHistoryResponse
                    {
                        Id = x.Id,
                        ProductName = x.Products.NameRu,
                        PurchaseDate = x.CreationDate,
                        Amount = x.Amount,
                        Price = x.Price,
                        ImagePath = x.Products.ImagePath,
                    }).ToList();
                }
            }

            return new List<UserPurchaseHistoryResponse>();
        }

        private Product GetProduct(int id, StayTrueDBEntities context)
        {
            var dbProd = context.Products.SingleOrDefault(x => x.Id == id);
            return new Product
            {
                Id = dbProd.Id,
                NameRu = dbProd.NameRu,
                NameKg = dbProd.NameKg,
                InformationKg = dbProd.InformationKg,
                InformationRu = dbProd.InformationRu,
                Amount = dbProd.Amount,
                AvailableAmount = dbProd.AvailableAmount,
                Price = dbProd.Price,
                IsAvailable = dbProd.IsAvailable,
                ImagePath = dbProd.ImagePath,
                ShopId = dbProd.ShopId
            };
        }

      
        public string ValidateUser(User user)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbUser = context.Users.SingleOrDefault(x => x.Phone == user.Phone);
                return dbUser == null ? Resources.Ok : Resources.UserExist;
            }
        }


        public string AuthorizationAdmin(AdminAccount account)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var admin = context.Administrators.SingleOrDefault(x => x.Login == account.Login);
                if (admin != null)
                {
                    var authorize = ValidateBCryptePassword(account.Password, admin.Password);
                    if (authorize)
                    {
                        return Resources.Ok;
                    }
                    return Resources.IncorrectLoginOrPassword;
                }
                return Resources.UserNotFound;
            }
        }

        public Account RegisterAdmin(Admin admin)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbAdmin = new Administrators
            {
                Login = admin.Login,
                Password = CreateBCrypt(admin.Password),
                FirstName = admin.FirstName,
                LastName = admin.LastName,
                PhoneNumber = admin.PhoneNumber,
                Email = admin.Email,
                BirthDate = (DateTime)admin.Birthdate,
                Role = (int)admin.Role
            };
            context.Administrators.Add(dbAdmin);
            context.SaveChanges();
            return new Account
            {
                BirthDate = dbAdmin.BirthDate,
                Email = dbAdmin.Email,
                FirstName = dbAdmin.FirstName,
                LastName = dbAdmin.LastName,
                Phone = dbAdmin.PhoneNumber,
                RoleId = (int)RoleEnum.Admin,
                StatusId = (int)StatusEnum.Active
            };
        }

        public Admin ChangeAdminProfile(Admin admin)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var adminFromContext = context.Administrators.FirstOrDefault(x => x.Id == admin.Id);

            if (adminFromContext != null)
            {
                adminFromContext.FirstName = admin.FirstName;
                adminFromContext.LastName = admin.LastName;
                adminFromContext.PhoneNumber = admin.PhoneNumber;
                adminFromContext.Email = admin.Email;
                adminFromContext.BirthDate = (DateTime)admin.Birthdate;
                adminFromContext.Role = (int)RoleEnum.Admin;
                context.SaveChanges();
                return new Admin
                {
                    FirstName = adminFromContext.FirstName,
                    LastName = adminFromContext.LastName,
                    PhoneNumber = adminFromContext.PhoneNumber,
                    Email = adminFromContext.Email,
                    Birthdate = adminFromContext.BirthDate,
                    Role = RoleEnum.Admin
                };
            }
            return null;
        }

        public string ChangeAdminPassword(Admin admin)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var adminFromContext = context.Administrators.FirstOrDefault(x => x.Login == admin.Login);
            if (adminFromContext != null)
            {
                adminFromContext.Password = CreateBCrypt(admin.Password);
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        private Game GetGame(int gameId, StayTrueDBEntities context)
        {
            var dbGame = context.Games.SingleOrDefault(x => x.Id == gameId);

            if (dbGame != null)
                return new Game
                {
                    Id = dbGame.Id,
                    Name = dbGame.Name,
                    Image = dbGame.ImagePath,
                    GameUrl = dbGame.GameUrl
                };
            return null;
        }


        public string BlockUser(BlockUser user)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var userDb = context.Users.SingleOrDefault(x => x.Id == user.UserId);
                        var blockUser = new BlockedUsers
                        {
                            BlockDate = DateTime.Now,
                            Reason = user.Reason,
                            UnblockDate = user.UnblockDate,
                            UserId = user.UserId
                        };
                        context.BlockedUsers.Add(blockUser);
                        userDb.StatusId = (int)StatusEnum.Blocked;
                        context.SaveChanges();
                        dbTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbTransaction.Rollback();
                        return null;
                    }

                }
            }
            return Resources.Ok;
        }

        public string ChangeUserStatus(List<User> users, int status)
        {
            using (var context = new StayTrueDBEntities())
            {
                foreach (var user in users)
                {
                    var dbUser = context.Users.SingleOrDefault(x => x.Id == user.Id);
                    if (dbUser != null) dbUser.StatusId = status;
                    context.SaveChanges();
                }
                return Resources.Ok;
            }
        }



        private string GeneratePassword()
        {
            bool includeLowercase = true;
            bool includeUppercase = true;
            bool includeNumeric = true;
            bool includeSpecial = false;
            bool includeSpaces = false;
            int lengthOfPassword = 8;

            string password = PasswordGenerator.GeneratePassword(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);

            while (!PasswordGenerator.PasswordIsValid(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, password))
            {
                password = PasswordGenerator.GeneratePassword(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);
            }
            return password;
        }

        private static string CreateBCrypt(string password)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            string hash = BCrypt.Net.BCrypt.HashPassword(password, salt);
            return hash;
        }

        private static bool ValidateBCryptePassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }

        private static string CreateMd5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        private byte[] GetImageBytes(string base64)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            return Convert.FromBase64String(base64.Substring(base64.IndexOf(',') + 1));
        }

        async private void UserChatAuthorizeAsync(string token)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(5);
  
                client.DefaultRequestHeaders.Add("Authorization", String.Format("Basic {0}", Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes($"{ChatLogin}:{ChatPassword}"))));

                await client.GetStringAsync(ChatUrl + "admin/authUser?token=" + token);

            }
        }


        private Account UnblockUser(int userId, string application)
        {
            using (var context = new StayTrueDBEntities())
            {
                var brand = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.Brand));
                var isBrand = brand != null && Convert.ToBoolean(brand.Value);
                var blockedUser = context.BlockedUsers.SingleOrDefault(x => x.UserId == userId);
                var user = context.Users.SingleOrDefault(x => x.Id == userId);
                context.BlockedUsers.Remove(blockedUser ?? throw new InvalidOperationException());
                user.StatusId = (int)StatusEnum.Active;
                context.SaveChanges();

                #region Online counter

                switch (application)
                {
                    case "web":
                        user.SignInCounterP += 1;
                        user.LastSignInP = DateTime.Now;
                        break;
                    case "mobile":
                        user.SignInCounterM += 1;
                        user.LastSignInM = DateTime.Now;
                        break;
                }
                context.SaveChanges();

                #endregion

                return new Account
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Email = user.Email,
                    BirthDate = DateTime.Now,
                    IsApproved = user.IsApproved,
                    StatusId = user.StatusId,
                    Phone = user.Phone,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType
                };
            }
        }

        private Users UpdateImagePath(Users user)
        {
            user.PassportBack = @"/Images/Passports/Back/" + user.Id + ".jpg";
            user.PassportFront = @"/Images/Passports/Front/" + user.Id + ".jpg";

            return user;
        }

        private Users CreateNewUser(RegisterUser user)
        {
            var newUser = new Users
            {
                Phone = user.Phone,
                Password = null, // Пароль создается при отправки смс сообщения
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = user.MiddleName,
                BirthDate = user.BirthDate,
                CityId = user.CityId,
                GenderId = user.GenderId,
                Email = user.Email,
                Wincoins = 0,
                RoleId = 2, //dbo.Roles
                RegistrationDate = DateTime.Now,
                StatusId = (int)StatusEnum.NotActive,
                IsApproved = false,
                IsAvatarApproved = true,
                PassportBack = "",
                PassportFront = "",
                CigarettesAlternative = user.CigarettesAlternative,
                CigarettesAlternativeType = user.CigarettesAlternativeType,
                CigarettesFavorite = user.CigarettesFavorite,
                CigarettesFavoriteType = user.CigarettesFavoriteType
                
            };

            return newUser;
        }

        private void SavePassportToDirectory(RegisterUser user, Users dbUser)
        {
            string pathFront = FolderPath + @"\Images\Passports\Front\";
            string pathBack = FolderPath + @"\Images\Passports\Back\";

            byte[] passportFrontImg = Convert.FromBase64String(ImageHelper.ParseBase64Value(user.PassportFront));
            byte[] passportBackImg = Convert.FromBase64String(ImageHelper.ParseBase64Value(user.PassportBack));

            File.WriteAllBytes(pathFront + dbUser.Id + ".jpg", passportFrontImg);
            File.WriteAllBytes(pathBack + dbUser.Id + ".jpg", passportBackImg);
        }

        private string SaveAvatarToDirectory(string avatar, string userId, string directory = "/Images/Avatars/")
        {
            string avatarImgPath = @directory + userId + ".jpg";

            var filePath = $@"{FolderPath}{avatarImgPath}";
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }

            byte[] avatarImg = Convert.FromBase64String(ImageHelper.ParseBase64Value(avatar));

            File.WriteAllBytes(filePath, avatarImg);

            return avatarImgPath;
        }

        private string SaveSmallAvatar(int userId, string fileName)
        {
            var image = Image.FromFile($@"{FolderPath}{fileName}");

            if (image.Equals(null)) return null;

            var newImage = ScaleImage(image);

            var filePath = $@"{FolderPath}{fileName.Replace("Avatars", "SmallAvatars")}";

            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            image.Dispose();

            string smallImagePath = fileName.Replace("Avatars", "SmallAvatars");

            Bitmap clone = new Bitmap(newImage);
            newImage.Dispose();

            clone.Save(filePath);
            clone.Dispose();


            return smallImagePath;
        }

        private int GetUserRank(GreatBattles greatBattles, int userPoints)
        {
            if (userPoints > greatBattles.Rank5Limit)
                return 5;
            if (userPoints > greatBattles.Rank4Limit)
                return 4;
            if (userPoints > greatBattles.Rank3Limit)
                return 3;
            if (userPoints > greatBattles.Rank2Limit)
                return 2;
            if (userPoints > greatBattles.Rank1Limit)
                return 1;
            return 0;
        }
        private static Image ScaleImage(Image image)
        {

            var newImage = new Bitmap(imageHeight * image.Width / image.Height, imageHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, imageHeight * image.Width / image.Height, imageHeight);
                graphics.Dispose();
            }

            return newImage;
        }

        private void CheckUserFirstSignIn(Users user)
        {
            if (!user.LastSignInM.HasValue)
            {
                int operatorId = BallancePaymentService.GetServiceId(user.Phone);
                if (operatorId != 0)
                {
                    using (var context = new StayTrueDBEntities())
                    {
                        try
                        {
                            var ballanceUpHistory =
                                context.UserBallanceUpHistory.FirstOrDefaultAsync(b => b.UserId == user.Id);
                            //                    string userReceiptId = BallancePaymentService.UpUserBallance(user.Phone, int.Parse(FirstSignInGiftAmount), operatorId);
                            if (ballanceUpHistory.Result == null)
                            {
                                UserBallanceUpHistory history = new UserBallanceUpHistory()
                                {
                                    UserId = user.Id,
                                    Date = DateTime.Now
                                };
                                context.UserBallanceUpHistory.Add(history);
                                context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }
                    }
                }
            }
        }

        public string GetSmallAvatar(int userId)
        {
            var defaultImg = "/Images/Avatars/Default.jpg";

            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Id == userId);

                if (user == null)
                    return defaultImg;

                return user.avatarsmall ?? defaultImg;
            }
        }

        public string GetAvatar(int userId)
        {
            var defaultImg = "/Images/Avatars/Default.jpg";

            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Id == userId);

                if (user == null)
                    return defaultImg;

                return user.Avatar ?? defaultImg;
            }
        }

        public List<InvitedUsersResponse> GetInvitedUsers(int userId, string appType)
        {
            using (var context = new StayTrueDBEntities())
            {
                List<DataAccessLayer.InvitedUsers> invUsers = new List<DataAccessLayer.InvitedUsers>();
                if (appType.Contains("mobile"))
                {
                    invUsers = context.InvitedUsers.Where(u => u.InvitingUserId == userId).OrderByDescending(u => u.InvitedDate).Take(20).ToList();
                }
                else
                {
                    invUsers = context.InvitedUsers.Where(u => u.InvitingUserId == userId).OrderByDescending(u => u.InvitedDate).ToList();
                }

                if (invUsers != null)
                {
                    return invUsers.Select(x => new InvitedUsersResponse
                    {
                        FirstName = x.Users1.FirstName,
                        LastName = x.Users1.LastName,
                        AvatarPath = x.Users1.Avatar
                    }).ToList();
                }
                return new List<InvitedUsersResponse>();
            }
        }

    }
}