﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Common;
using Common.Models;
using DataAccessLayer;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using NewsModel = StayTrueApiService.Models.ResponseModels.NewsModel;

namespace StayTrueApiService.Managers.BusinessLogic
{
    internal class NewsManager
    {
        public NewsResponse GetAllNews(int userId, bool includeBanners = true)
        {
            using (var context = new StayTrueDBEntities())
            {
                var response = new NewsResponse();
                if (includeBanners)
                {
                    var banners = GetBanners();
                    if (response.Banners != null)
                        response.Banners = banners;
                }

                var now = DateTime.Now;
                IQueryable<News> news = context.News
                    .Where(x => x.IsDeleted == false && x.IsActive && now >= x.PublicationDate)
                    .OrderByDescending(x => x.Id);

                response.MainNews = news.Where(n => n.NewsType == 1).ToList().Select(x => new NewsModel
                {
                    Id = x.Id,
                    NameRu = x.NameRu,
                    NameKg = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    DescriptionRu = x.DescriptionRu,
                    DescriptionKg = x.DescriptionKg,
                    ImageP = x.ImageP,
                    ImageM = x.ImageM,
                    VideoP = x.VideoP,
                    VideoM = x.VideoM,
                    CreationDate = x.CreationDate,
                    NewsType = x.NewsType,
                    UsersLikesCount = NewsLikesCount(context, x.Id),
                    NewsCategory = x.NewsCategory,
                    UserIsLiked = UserIsLiked(context, userId, x.Id)
                }).ToList();

                response.SecondaryNews = news.Where(n => n.NewsType == 2).ToList().Select(x => new NewsModel
                {
                    Id = x.Id,
                    NameRu = x.NameRu,
                    NameKg = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    DescriptionRu = x.DescriptionRu,
                    DescriptionKg = x.DescriptionKg,
                    ImageP = x.ImageP,
                    ImageM = x.ImageM,
                    VideoP = x.VideoP,
                    VideoM = x.VideoM,
                    CreationDate = x.CreationDate,
                    NewsType = x.NewsType,
                    UsersLikesCount = NewsLikesCount(context, x.Id),
                    NewsCategory = x.NewsCategory,
                    UserIsLiked = UserIsLiked(context, userId, x.Id)
                }).ToList();

                return response;
            }
        }

        public LikeResponse AddLike(int userId, int newsId)
        {
            using (var context = new StayTrueDBEntities())
            {
                if (context.Likes.Any(l => l.UserId == userId && l.NewsId == newsId))
                    return new LikeResponse {ErrorMessage = Resources.AlreadyLiked};

                context.Likes.Add(new Likes {UserId = userId, NewsId = newsId});
                context.SaveChanges();
                return new LikeResponse{NewsId = newsId, UserId = userId};
            }
        }

        private int NewsLikesCount(StayTrueDBEntities context, int id)
        {
            return context.Likes.Count(n => n.NewsId == id);
        }

        private bool UserIsLiked(StayTrueDBEntities context, int userId, int id)
        {
            return context.Likes.Any(n => n.NewsId == id && n.UserId == userId);
        }

        public List<BannerModel> GetBanners()
        {
            using (var context = new StayTrueDBEntities())
            {
                return context.Banners.Where(x => x.IsActive).OrderByDescending(x => x.Id).Take(4).Select(x => new BannerModel
                {
                    Id = x.Id,
                    ImagePath = x.ImagePath,
                    Name = x.Name
                }).ToList();
            }

        }
    }
}