﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using DataAccessLayer;
using StayTrueApiService.Helpers;
using Log = Common.Logger.Log;

namespace StayTrueApiService.Managers.Services
{
    public static class BallancePaymentService
    {
        private static readonly string login = ConfigurationManager.AppSettings["PaymentServiceLogin"];

        private static readonly string password = ConfigurationManager.AppSettings["PaymentServicePassword"];

        private static readonly string addres = ConfigurationManager.AppSettings["PaymentServiceUrl"];

        private static readonly string FirstSignInGiftAmount = ConfigurationManager.AppSettings["FirstSignInGiftAmount"];

        internal static string UpUserBallance(string number, int amount, int serviceId)
        {
            DisableCertificateValidation();

            var localId = Guid.NewGuid();

            var ballanceUpFor = addres +
                                $"paymentasync?login={login}&password={password}&localId={localId}&serviceId={serviceId}&requisite={number}&amount={amount}";

            //     var cancelPayment = addres + $"cancelpaymentasync?login={login}&password={password}&receiptId=19042462964";
            return GetBetween(HttpHelper.SendRequest(ballanceUpFor), "<ReceiptNum>", "</ReceiptNum>");
        }

        internal static void SaveAllOperatorCodes()
        {
            Dictionary<int,string> data = GetParsedResult();
            if (data == null)
            {
                Log.Error("error getting operator codes, see logs for detailed errror");
            }

            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    foreach (var d in data)
                    {
                        var oldCode = context.PaymentServiceCodes.FirstOrDefault(q => q.ServiceId == d.Key);
                        if (oldCode == null)
                        {
                            PaymentServiceCodes code = new PaymentServiceCodes()
                            {
                                ServiceId = d.Key,
                                ServiceRegex = d.Value
                            };
                            context.PaymentServiceCodes.Add(code);
                        }
                        else
                        {
                            oldCode.ServiceId = d.Key;
                            oldCode.ServiceRegex = d.Value;
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }

                context.SaveChanges();
            }
        }

        internal static void UserFirstSignInGift()
        {
            using (var context = new StayTrueDBEntities())
            {
                var usersForUp = context.UserBallanceUpHistory.Where(u => u.ReiceiptNumber == null);
                foreach (var usr in usersForUp)
                {
                    int operatorId = GetServiceId(usr.Users.Phone);
                    if (operatorId != 0)
                    {
                        string userReceiptId =
                            UpUserBallance(usr.Users.Phone, int.Parse(FirstSignInGiftAmount), operatorId);
                        if (!string.IsNullOrEmpty(userReceiptId))
                        {
                            try
                            {
                                usr.ReiceiptNumber = userReceiptId;
                                usr.Amount = int.Parse(FirstSignInGiftAmount);

                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.Message);
                            }
                        }
                    }
                }
                context.SaveChanges();
            }
        }

        internal static int GetServiceId(string number)
        {
            using (var context = new StayTrueDBEntities())
            {
                var codes = context.PaymentServiceCodes;

                foreach (var code in codes)
                {
                    Regex regex = new Regex(code.ServiceRegex);
                    Match match = regex.Match(number);
                    if (match.Success)
                    {
                        return code.ServiceId;
                    }
                }
                return 0;
            }
        }

        private static Dictionary<int, string> GetParsedResult()
        {
            string result = GetAllOperatorCodes();
            if (string.IsNullOrEmpty(result))
            {
                return null;
            }

            return ParseResult(result);
        }

        private static Dictionary<int, string> ParseResult(string data)
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            var str = string.Empty;
            var str2 = string.Empty;
            do
            {
                str = GetBetween(data, "<ServiceID>", "</ServiceID");
                str2 = GetBetween(data, "<ServicePrefix>", "</ServicePrefix>");
                if (str2.Length > 6)
                {
                    list.Add(Convert.ToInt32(str), str2);
                    data = data.Substring(data.IndexOf(str2));
                }
                else
                {
                    return list;
                }
            } while (str != "");

            return list;
        }

        private static string GetAllOperatorCodes()
        {
            try
            {
                DisableCertificateValidation();
                var url = addres + $"getservices?login={login}&password={password}";
                return HttpHelper.SendRequest(url);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return string.Empty;
            }
        }

        private static void DisableCertificateValidation()
        {

            ServicePointManager.ServerCertificateValidationCallback =
                delegate (
                    object s,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors
                ) {
                    return true;
                };

        }

        private static string GetBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        public static string GetPaymentReport(DateTime dateFrom, DateTime dateTo)
        {
            string dateFromStr = dateFrom.ToString("yyyy-MM-dd");
            string dateToStr = dateTo.ToString("yyyy-MM-dd");
            DisableCertificateValidation();
            var url = addres + $"getreportsdata?login={login}&password={password}&reportID=1&dateFrom={dateFromStr}&dateTo={dateToStr}";
            try
            {
                var result = HttpHelper.SendRequest(url);
                result = result.Replace("&lt;", "<");
                result = result.Replace("&gt;", ">");
                result = result.Replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">",
                    string.Empty);
                result = result.Replace("</string>", string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return string.Empty;
            }
        }      
    }
}