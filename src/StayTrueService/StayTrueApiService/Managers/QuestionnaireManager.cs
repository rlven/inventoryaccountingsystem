﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using DataAccessLayer;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Managers
{
    public class QuestionnaireManager
    {
        public QuestionnaireResponse GetQuestionnaire(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Questionnaires questionnaire = context.Questionnaires
                    .OrderByDescending(q => q.Id)
                    .FirstOrDefault(x => x.IsActive && x.IsDeleted == false && DateTime.Now >= x.StartDate);

                if (questionnaire == null)
                    return new QuestionnaireResponse {ErrorMessage = Resources.QuestionnaireNotFound};

                var answer = questionnaire.QuestionnaireAnswers.FirstOrDefault(q => q.UserId == userId);
                if (answer != null)
                    return new QuestionnaireResponse {ErrorMessage = Resources.QuestionnaireAnswered};

                return new QuestionnaireResponse
                {
                    Id = questionnaire.Id,
                    Name = questionnaire.Name,
                    Description = questionnaire.Description,
                    Wincoins = questionnaire.Wincoins,
                    Questions = questionnaire.Questions
                };
            }
        }

        public QuestionnaireAnswersResponse SaveQuestionnaireAnswers(Models.QuestionnaireAnswers answers, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var questionnaire = context.Questionnaires.FirstOrDefault(q => q.Id == answers.QuestionnaireId);
                if (questionnaire == null)
                    return new QuestionnaireAnswersResponse {ErrorMessage = Resources.QuestionnaireNotFound};

                var user = context.Users.SingleOrDefault(x => x.Id == userId);
                if(user == null)
                    return new QuestionnaireAnswersResponse{ErrorMessage = Resources.AccountWasNotFound};

                var newAnswers = new QuestionnaireAnswers
                {
                    Answer1 = answers.Answer1,
                    Answer2 = answers.Answer2,
                    Answer3 = answers.Answer3,
                    Answer4 = answers.Answer4,
                    Answer5 = answers.Answer5,
                    Answer6 = answers.Answer6,
                    Answer7 = answers.Answer7,
                    Answer8 = answers.Answer8,
                    Answer9 = answers.Answer9,
                    Answer10 = answers.Answer10,
                    Answer11 = answers.Answer11,
                    Answer12 = answers.Answer12,
                    UserId = userId,
                    QuestionnaireId = answers.QuestionnaireId,
                    CreationDate = DateTime.Now
                };
                context.QuestionnaireAnswers.Add(newAnswers);
                user.Wincoins += questionnaire.Wincoins;
                context.SaveChanges();

                return new QuestionnaireAnswersResponse {QuestionnaireId = answers.QuestionnaireId, UserId = userId};
            }
        }
    }
}