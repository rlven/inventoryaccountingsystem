﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Common;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    public class SupportController : ApiController
    {
        private readonly SupportManager supportManager = new SupportManager();

        /// <summary>
        /// Create new support request
        /// </summary>
        /// <param name="supportRequest">Support request model</param>
        /// <returns>Returns created support request</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and support request created", typeof(List<SupportResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are no active quizzes", typeof(BaseResponse))]
        public IHttpActionResult Post([FromBody]SupportRequest supportRequest)
        {
            if (ModelState.IsValid)
            {
                var result = supportManager.SupportRequest(supportRequest);
                if (string.IsNullOrEmpty(result.ErrorMessage))
                    return Content(HttpStatusCode.OK, result);
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            }
            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }
    }
}
