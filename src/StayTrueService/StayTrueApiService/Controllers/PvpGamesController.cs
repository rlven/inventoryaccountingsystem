﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/PvpGames")]
    public class PvpGamesController : ApiController
    {
        private readonly PvpGameManager gameManager = new PvpGameManager();

        /// <summary>
        /// Get available pvp games list
        /// </summary>
        /// <returns>List of pvp games</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and games list returned", typeof(List<PvpGamesResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no games is database")]
        public IHttpActionResult Get(int pageSize = 20, int page = 1)
        {
            var games = gameManager.GetPvpGames(pageSize, page - 1);
            return Content(HttpStatusCode.OK, new PvpGamesResponse {PvpGames = games, Pages = gameManager.CountPages(pageSize)});
        }

        /// <summary>
        /// Get bets for specific pvp game
        /// </summary>
        /// <param name="gameId">Custom id of the pvp game</param>
        /// <remarks>GameId - not primary key of the game table</remarks>
        /// <returns>Returns bets list for the game</returns>
        [HttpGet]
        [Route("GetGameBets")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and games list returned", typeof(List<PvpBetsResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to get game bets", typeof(BaseResponse))]
        public IHttpActionResult GetGameBets(int gameId)
        {
            var bets = gameManager.PvpGameBets(gameId);
            if(string.IsNullOrEmpty(bets.ErrorMessage))
                return Content(HttpStatusCode.OK, bets);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(bets.ErrorMessage));
        }

        /// <summary>
        /// Deposit wincoins to user for pvp game session
        /// </summary>
        /// <param name="session">Game session for every user</param>
        /// <returns>Returns list of the winner ids</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("DepositWincoins")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and wincoins added to winner users", typeof(List<PvpTransferWincoinsResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to add wincoins to winner users", typeof(List<PvpTransferWincoinsResponse>))]
        public IHttpActionResult DepositWincoins(List<PvpGameSessions> session)
        {
            var deposit = gameManager.PvpSessionDepositWincoins(session);
            return Content(!string.IsNullOrEmpty(deposit.ErrorMessage) ? HttpStatusCode.BadRequest : HttpStatusCode.OK, deposit);
        }

        /// <summary>
        /// Withdraw wincoins from the user for entered pvp game session
        /// </summary>
        /// <param name="sessions"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("WithdrawWincoins")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and wincoins withdrawn", typeof(List<PvpTransferWincoinsResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to withdraw wincoins", typeof(List<PvpTransferWincoinsResponse>))]
        public IHttpActionResult WithdrawWincoins(List<PvpGameSessions> sessions)
        {
            var withdraw = gameManager.PvpSessionWithdrawWincoins(sessions);
            return Content(!string.IsNullOrEmpty(withdraw.ErrorMessage) ? HttpStatusCode.BadRequest : HttpStatusCode.OK, withdraw);
        }
    }
}