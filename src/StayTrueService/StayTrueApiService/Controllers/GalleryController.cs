﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    public class GalleryController : ApiController
    {
        private readonly AlbumManager albumManager = new AlbumManager();

        /// <summary>
        /// Get list of albums in the gallery
        /// </summary>
        /// <param name="isBranded">Indicate that only branded albums should be returned</param>
        /// <param name="pageSize">Number of albums per page</param>
        /// <param name="page">Page number</param>
        /// <returns>List of albums</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and albums list returned", typeof(List<AlbumModel>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no albums in gallery")]
        public IHttpActionResult Get(bool? isBranded = null, int pageSize = 0, int page = 0)
        {
            var albums = albumManager.GetAlbums(isBranded, pageSize, page);
            if (albums != null && albums.Any())
                return Content(HttpStatusCode.OK, albums);

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get album images by its id
        /// </summary>
        /// <param name="id">Album id</param>
        /// <param name="isBranded">Indicate that only branded images should be returned</param>
        /// <param name="pageSize">Number of albums per page</param>
        /// <param name="page">Page number</param>
        /// <returns>List of images in the album</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and images list returned", typeof(List<AlbumImageModel>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no images for this album in gallery")]
        public IHttpActionResult Get(int id, bool? isBranded = null, int pageSize = 0, int page = 0)
        {
            var images = albumManager.GetAlbumImages(id, isBranded, pageSize, page);
            if (images != null && images.Any())
                return Content(HttpStatusCode.OK, images);

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}