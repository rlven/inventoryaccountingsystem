﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/Quiz")]
    public class QuizController : ApiController
    {
        private readonly GameManager gameManager = new GameManager();

        /// <summary>
        /// Get list of quizzes
        /// </summary>
        /// <returns>Returns list of quizzes</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and quizzes list returned", typeof(List<QuizResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no active quizzes")]
        public IHttpActionResult Get()
        {
            var result = gameManager.GetQuizzes();
            if (result.Any())
                return Content(HttpStatusCode.OK, result);
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get user session in the specific quiz
        /// </summary>
        /// <param name="quizId">Id of the quiz</param>
        /// <returns>Returns user session in the specific quiz</returns>
        [HttpGet]
        [Route("GetQuizSession")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user session returned", typeof(List<QuizResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are no user's session in this quiz or quiz doesn't exist", typeof(BaseResponse))]
        public IHttpActionResult GetQuizSession(int quizId)
        {
            var userId = User.Identity.GetUserId<int>();
            var result = gameManager.GetQuizSession(quizId, userId);
            if (!string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            return Content(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Save user session for the specific quiz
        /// </summary>
        /// <param name="session">Session model</param>
        /// <returns>Returns saved quiz</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user session saved", typeof(List<QuizSessionResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to save session", typeof(BaseResponse))]
        public IHttpActionResult Post([FromBody]QuizSession session)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var result = gameManager.SaveQuizSession(session, userId);
                if (string.IsNullOrEmpty(result.ErrorMessage))
                    return Content(HttpStatusCode.OK, result);
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            }

            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }
    }
}