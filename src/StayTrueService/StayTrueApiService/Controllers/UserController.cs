﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly UserManager userManager = new UserManager();
        
        /// <summary>
        /// Get user info
        /// </summary>
        /// <returns>Return user info</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user info returned", typeof(UserInfo))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to get user info", typeof(BaseResponse))]
        public IHttpActionResult Get()
        {
            var userInfo = userManager.UserInfo(User.Identity.GetUserId<int>());
            if (string.IsNullOrEmpty(userInfo.ErrorMessage))
            {
                return Content(HttpStatusCode.OK, userInfo);
            }

            return Content(HttpStatusCode.BadRequest, new BaseResponse(userInfo.ErrorMessage));
        }

        /// <summary>
        /// Get user's wincoins
        /// </summary>
        /// <returns>Returns user's wincoins</returns>
        [HttpGet]
        [Route("GetWincoins")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and wincoins returned", typeof(WincoinsResponse))]
        public IHttpActionResult GetWincoins()
        {
            var userId = User.Identity.GetUserId<int>();
            var wincoins = userManager.GetUserWincoins(userId);
            return Content(HttpStatusCode.OK, new WincoinsResponse {Id = userId, Wincoins = wincoins});
        }

        /// <summary>
        /// Get user's purchase history
        /// </summary>
        /// <param name="pageSize">Products per page</param>
        /// <param name="page">Page number</param>
        /// <returns>Returns list of user's purchases</returns>
        [HttpGet]
        [Route("GetPurchaseHistory")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and purchase history returned", typeof(List<UserPurchaseHistoryResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that user's purchase history is empty")]
        public IHttpActionResult GetPurchaseHistory(int pageSize = 20, int page = 1)
        {
            var userId = User.Identity.GetUserId<int>();
            var purchases = userManager.PurchaseHistory(userId, pageSize, --page);
            if (purchases.Any())
                return Content(HttpStatusCode.OK, purchases);
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get the path to the user's avatar
        /// </summary>
        /// <remarks>if user has no avatar returned the path will lead to the default avatar</remarks>
        /// <param name="isSmall">If true, returns path to small avatar, if false to the normal size avatar</param>
        /// <returns>Returns path to the avatar</returns>
        [HttpGet]
        [Route("GetAvatar")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and path to the avatar returned", typeof(WincoinsResponse))]
        public IHttpActionResult GetAvatar(bool isSmall = true)
        {
            var userId = User.Identity.GetUserId<int>();
            var avatar = isSmall ? userManager.GetSmallAvatar(userId) : userManager.GetAvatar(userId);
            return Content(HttpStatusCode.OK, new AvatarResponse {Id = userId, AvatarPath = avatar});
        }

        /// <summary>
        /// Validate promocode
        /// </summary>
        /// <param name="promocode">Promocode number</param>
        /// <returns>Returns promocode and its status</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("ValidatePromocode")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and promocode status returned", typeof(PromoCodeResponse))]
        public IHttpActionResult ValidatePromo(int promocode)
        {
            var isExist = userManager.UserIsExist(promocode);
            return Content(HttpStatusCode.OK, new PromoCodeResponse {Promocode = promocode, IsExist = isExist});
        }

        /// <summary>
        /// Change user's avatar
        /// </summary>
        /// <param name="avatar">Base64 string of the avatar</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ChangeAvatar")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that the request successfully completed and avatar saved")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the error occured while trying to save avatar", typeof(BaseResponse))]
        public IHttpActionResult ChangeAvatar(string avatar)
        {
            if (string.IsNullOrEmpty(avatar))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));

            var avatarPath = userManager.EditAvatar(avatar, User.Identity.GetUserId<int>());
            if (!string.IsNullOrEmpty(avatarPath))
                return StatusCode(HttpStatusCode.NoContent);

            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.ImageSaveError));
        }

        /// <summary>
        /// Check phone is exist
        /// </summary>
        /// <param name="phone">IsExist model</param>
        /// <returns>IsExist info</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("PhoneIsExist")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the phone is not exist", typeof(IsExistResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that an error has occurred while checking the number", typeof(BaseResponse))]
        public IHttpActionResult PhoneIsExist([FromUri][StringLength(12)]string phone)
        {
            if (ModelState.IsValid)
            {
                var isExist = userManager.PhoneIsExist(phone);
                return Content(HttpStatusCode.OK, isExist);
            }

            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }

        /// <summary>
        /// GetInvitedUsers
        /// </summary>
        /// <returns>invited users</returns>
        [HttpGet]
        [Route("GetInvitedUsers")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates list of invited users", typeof(InvitedUsersResponse))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no invited users", typeof(InvitedUsersResponse))]
        public IHttpActionResult GetInvitedUsers([FromHeader]string appType = "web")
        {
            var userId = User.Identity.GetUserId<int>();
            var invitedUsers = userManager.GetInvitedUsers(userId, appType);
            if (invitedUsers.Count == 0)
            {
                return Content(HttpStatusCode.NoContent, new InvitedUsersResponse(){LocalizedMessage = Resources.ThereIsNoInvitedUsers });
            }

            return Content(HttpStatusCode.OK, invitedUsers);
        }
    }
}