﻿using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/Lottery")]
    public class LotteryController : ApiController
    {
        private readonly LotteryManager lotteryManager = new LotteryManager();

        /// <summary>
        /// Get great lottery detailed information
        /// </summary>
        /// <returns>Returns currently active lottery information</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully get lottery details", typeof(List<LotteryDetailsResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are error getting response", typeof(BaseResponse))]
        public IHttpActionResult Get()
        {
            var userId = User.Identity.GetUserId<int>();
            var lotteryDetails = lotteryManager.GetLotteryDetails(userId);
            if (!string.IsNullOrEmpty(lotteryDetails.ErrorMessage))
            {
                return Content(HttpStatusCode.BadRequest,
                    new BaseResponse() { ErrorMessage = lotteryDetails.ErrorMessage });
            }

            return Content(HttpStatusCode.OK, lotteryDetails);
        }

        /// <summary>
        /// Buy new lottery ticket
        /// </summary>
        /// <param name="ticket">Tickets count</param>
        /// <returns>Purchase info</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully bought tickets", typeof(List<LotteryTicketResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are error buying tickets", typeof(BaseResponse))]
        public IHttpActionResult Post([FromBody]LotteryTicket ticket)
        {
            var userId = User.Identity.GetUserId<int>();
            var buyLotteryTicket = lotteryManager.BuyLotteryTicket(ticket, userId);
            if (!string.IsNullOrEmpty(buyLotteryTicket.ErrorMessage))
            {
                return Content(HttpStatusCode.BadRequest,
                    new BaseResponse() { ErrorMessage = buyLotteryTicket.ErrorMessage });
            }

            return Content(HttpStatusCode.OK, buyLotteryTicket);
        }
    }
}
