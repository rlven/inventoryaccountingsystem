﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Swagger.Net.Annotations;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Controllers
{
    [AllowAnonymous]
    public class CitiesController : ApiController
    {
        private readonly CitiesManager citiesManager = new CitiesManager();

        /// <summary>
        /// Get list of all cities
        /// </summary>
        /// <returns>List of cities</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and list of cities returned", typeof(List<CityResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are no cities in database")]
        public IHttpActionResult Get()
        {
            var cities = citiesManager.GetAllCities();
            if (cities != null)
                return Content(HttpStatusCode.OK, cities);
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
