﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/Party")]
    public class PartyController : ApiController
    {
        private readonly PartyManager partyManager = new PartyManager();

        /// <summary>
        /// Get party detailed information
        /// </summary>
        /// <returns>Returns detailed information of the currently running party </returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and party information returned", typeof(List<PartyResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to get party information", typeof(BaseResponse))]
        public IHttpActionResult Get()
        {
            var userId = User.Identity.GetUserId<int>();
            var result = partyManager.GetPartyDetails(userId);
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.OK, result);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
        }

        /// <summary>
        /// Get quiz session of the running party
        /// </summary>
        /// <param name="quizId">Quiz id</param>
        /// <returns>Returns party quiz session</returns>
        [HttpGet]
        [Route("QuizSession")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and quiz session returned", typeof(QuizSessionResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to get quiz session", typeof(BaseResponse))]
        public IHttpActionResult GetPartyQuizSession(int quizId)
        {
            var userId = User.Identity.GetUserId<int>();
            var result = partyManager.GetPartyQuizSession(quizId, userId);
            if (!string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            return Content(HttpStatusCode.OK, result);
        }
        
        /// <summary>
        /// Get information about user participation in the party
        /// </summary>
        /// <returns>Returns information about user</returns>
        [HttpGet]
        [Route("UserStatus")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user information returned", typeof(PartyUser))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to get user participation information", typeof(BaseResponse))]
        public IHttpActionResult CheckUserStatus()
        {
            var userId = User.Identity.GetUserId<int>();
            var result = partyManager.CheckPartyUserStatus(userId);
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.OK, result);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
        }

        /// <summary>
        /// Register user to the party
        /// </summary>
        /// <param name="partyAnswers">Registration model for the </param>
        /// <returns>Return user answers</returns>
        [HttpPost]
        [Route("RegisterUser")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and the user has been registered", typeof(PartyAnswersResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to register user", typeof(BaseResponse))]
        public async Task<IHttpActionResult> PartyUserRegistrationAnswer([FromBody]PartyAnswers partyAnswers)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var result = await partyManager.PartyRegistrationAnswer(partyAnswers, userId);
                if (string.IsNullOrEmpty(result.ErrorMessage))
                    return Content(HttpStatusCode.OK, result);
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            }
            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }

        /// <summary>
        /// Save user score in the party game
        /// </summary>
        /// <param name="gameScore">Score model</param>
        /// <returns>Returns saved score</returns>
        [HttpPost]
        [Route("SaveGameResult")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user's game score saved", typeof(GameScore))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to save user score", typeof(BaseResponse))]
        public IHttpActionResult SavePartyGameResult([FromBody]GameScore gameScore)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var result = partyManager.SavePartyGameResult(gameScore, userId);
                return Content(HttpStatusCode.OK, result);
            }
            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }

        /// <summary>
        /// Mark that user came to the party
        /// </summary>
        /// <returns>Returns NoContent status code if user's status successfully changed</returns>
        [HttpPost]
        [Route("UserStatus")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that the request successfully completed and user's status successfully changed")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to change user status", typeof(BaseResponse))]
        public IHttpActionResult ChangePartyUserHasComeStatus()
        {
            var userId = User.Identity.GetUserId<int>();
            var result = partyManager.ChangePartyUserHasComeStatus(userId);
            if (string.IsNullOrEmpty(result))
                return StatusCode(HttpStatusCode.NoContent);
            return Content(HttpStatusCode.OK, new BaseResponse(result));
        }
    }
}
