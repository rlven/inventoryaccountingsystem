﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    public class ShopController : ApiController
    {
        private readonly ShopManager shopManager = new ShopManager();

        /// <summary>
        /// Get products list
        /// </summary>
        /// <returns>Returns available products list</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and products returned", typeof(List<CategoriesResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no available products in the shop")]
        public IHttpActionResult Get()
        {
            var products = shopManager.GetProducts(User.Identity.GetUserId<int>());
            if(products.Any())
                return Content(HttpStatusCode.OK, products);
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Buy product
        /// </summary>
        /// <param name="product">Product id and it's amount</param>
        /// <remarks>Default value of product amount = 1</remarks>
        /// <returns>Returns bought product details and available products list</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and products returned", typeof(List<BuyProductResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to buy product", typeof(BaseResponse))]
        public IHttpActionResult Post([FromBody] BuyProduct product)
        {
            if (!ModelState.IsValid)
                return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));

            var boughtProduct = shopManager.BuyProduct(product, User.Identity.GetUserId<int>());
            if (string.IsNullOrEmpty(boughtProduct.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(boughtProduct.ErrorMessage));

            boughtProduct.Products = shopManager.GetProducts(User.Identity.GetUserId<int>());
            return Content(HttpStatusCode.OK, boughtProduct);
        }
    }
}