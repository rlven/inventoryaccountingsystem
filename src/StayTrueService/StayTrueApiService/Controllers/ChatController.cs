﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Common;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/Chat")]
    public class ChatController : ApiController
    {
        private readonly ChatManager chatManager = new ChatManager();

        /// <summary>
        /// Get list of chat groups
        /// </summary>
        /// <returns>List of chat groups</returns>
        [HttpGet]
        [Route("GetChatGroups")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and list of the chats returned", typeof(List<ChatGroupResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no available chats")]
        public IHttpActionResult ChatGroups()
        {
            var userId = User.Identity.GetUserId<int>();
            var chatGroups =  chatManager.GetChatGroupsByUserId(userId);
            if (chatGroups != null)
                return Content(HttpStatusCode.OK, chatGroups);
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get chat token
        /// </summary>
        /// <returns>Chat token string</returns>
        [HttpGet]
        [Route("GetChatAuth")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the authorization successfully completed and auth token returned", typeof(TokenResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that an error has occurred while authorize in the chat", typeof(BaseResponse))]
        public IHttpActionResult GetChatAuth()
        {
            var chatAuth = chatManager.GetChatAuth();
            if (!string.IsNullOrEmpty(chatAuth))
                return Content(HttpStatusCode.OK, new TokenResponse{ Token = chatAuth});
            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.ChatAuthError));
        }
    }
}
