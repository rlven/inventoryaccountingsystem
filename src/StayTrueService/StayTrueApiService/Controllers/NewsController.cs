﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/News")]
    public class NewsController : ApiController
    {
        private readonly NewsManager newsManager = new NewsManager();

        /// <summary>
        /// Get news list
        /// </summary>
        /// <returns>Return news list</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and news list returned", typeof(List<NewsResponse>))]
        public IHttpActionResult Get()
        {
            var userId = User.Identity.GetUserId<int>();
            var allNews = newsManager.GetAllNews(userId);
            return Content(HttpStatusCode.OK, allNews);
        }

        /// <summary>
        /// Get banners list
        /// </summary>
        /// <returns>Returns banners list</returns>
        [HttpGet]
        [Route("Banner")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and banners list returned", typeof(List<BannerModel>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no banners in database")]
        public IHttpActionResult GetBanners()
        {
            var banners = newsManager.GetBanners();
            if (banners.Any())
                return Content(HttpStatusCode.OK, banners);
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Like specific news
        /// </summary>
        /// <param name="newsId">Specific news id</param>
        /// <returns>UserId and newsId</returns>
        [HttpPost]
        [Route("Like")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and like added", typeof(List<GreatBattleDetailsResponseModel>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that error occured while trying to add like to the news")]
        public IHttpActionResult Like(int newsId)
        {
            var userId = User.Identity.GetUserId<int>();
            var result = newsManager.AddLike(userId, newsId);
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.OK, result);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
        }
    }
}
