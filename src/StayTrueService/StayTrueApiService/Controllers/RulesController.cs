﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [RoutePrefix("api/Rules")]
    public class RulesController : ApiController
    {
        private readonly RuleManager ruleManager = new RuleManager();

        /// <summary>
        /// Get rules of use
        /// </summary>
        /// <returns>Returns the text of the usage rules</returns>
        [HttpGet]
        [Route("RulesOfUse")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and rules returned", typeof(List<RulesResponse>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Indicates that the rules of use not found", typeof(BaseResponse))]
        public IHttpActionResult RulesOfUse()
        {
            var rules = ruleManager.GetRulesOfUse();
            if (string.IsNullOrEmpty(rules.ErrorMessage))
                return Content(HttpStatusCode.OK, rules);
            return Content(HttpStatusCode.NotFound, new BaseResponse(rules.ErrorMessage));
        }
        
        /// <summary>
        /// Get term of use
        /// </summary>
        /// <returns>Returns the text of the terms of use</returns>
        [HttpGet]
        [Route("TermOfUse")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and term returned", typeof(List<TermOfUseResponse>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Indicates that the term of use not found", typeof(BaseResponse))]
        public IHttpActionResult TermOfUse()
        {
            var terms = ruleManager.GeTermOfUse();
            if (string.IsNullOrEmpty(terms.ErrorMessage))
                return Content(HttpStatusCode.OK, terms);
            return Content(HttpStatusCode.NotFound, new BaseResponse(terms.ErrorMessage));
        }
    }
}
