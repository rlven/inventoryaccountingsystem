﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/GreatBattle")]
    public class GreatBattleController : ApiController
    {
        private readonly GameManager gameManager = new GameManager();
        private readonly GreatBattleManager greatBattleManager = new GreatBattleManager();

        /// <summary>
        /// Get great battle detailed information
        /// </summary>
        /// <returns>Returns currently running great battle detailed information</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and great battle information returned", typeof(List<GreatBattleDetailsResponseModel>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to get great battle information")]
        public IHttpActionResult Get()
        {
            var userId = User.Identity.GetUserId<int>();
            var result = greatBattleManager.GetGreatBattleDetails(userId);
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.OK, result);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
        }

        /// <summary>
        /// Get running great battle quiz session
        /// </summary>
        /// <param name="tourId">Great battle tour id</param>
        /// <returns>Returns great battle quiz session</returns>
        [HttpGet]
        [Route("QuizSession")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and quiz session returned", typeof(GreatBattleQuizzResultResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to get quiz session", typeof(BaseResponse))]
        public IHttpActionResult GreatBattleQuizSession(int tourId)
        {
            var userId = User.Identity.GetUserId<int>();
            var result = gameManager.GetGreatBattleQuizSession(tourId, userId);
            if (!string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            return Content(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Get information about user participation in the great battle
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("UserRegistration")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user registration info returned", typeof(GreatBattleUserRegistrationInfoResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to get user registration", typeof(BaseResponse))]
        public IHttpActionResult GreatBattleUserRegistration()
        {
            var userId = User.Identity.GetUserId<int>();
            var result = greatBattleManager.GetUserRegistrationInfo(userId);
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.OK, result);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
        }

        /// <summary>
        /// Save user score in the great battle game
        /// </summary>
        /// <param name="gameScore">Score model</param>
        /// <returns>Returns saved score</returns>
        [HttpPost]
        [Route("SaveGameResult")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and user's game score saved", typeof(GreatBattleGameScoreResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured when trying to save user score", typeof(BaseResponse))]
        public async Task<IHttpActionResult> GreatBattleGameResult(GreatBattleGameScoreResponse gameScore)
        {
            var result = await gameManager.SaveGreatBattleGameScore(gameScore);
            if (!string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            return Content(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Change user's side in the great battle
        /// </summary>
        /// <param name="groupId">Id of the great battle side</param>
        /// <returns>Returns user id with new great battle side</returns>
        [HttpPost]
        [Route("ChangeSide")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and the user switched to the other side", typeof(GreatBattleUserSideResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that user can't change side", typeof(BaseResponse))]
        public IHttpActionResult GreatBattleChangeUserSide(int groupId)
        {
            var userId = User.Identity.GetUserId<int>();
            var result = greatBattleManager.UserChangeSide(userId, groupId);
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Content(HttpStatusCode.OK, result);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
        }

        /// <summary>
        /// Register user to the great battle
        /// </summary>
        /// <param name="model">Registration model for the great battle</param>
        /// <returns>Returns user id and his great battle group</returns>
        [HttpPost]
        [Route("RegisterUser")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and the user has been registered", typeof(GreatBattleRegisterResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to register user", typeof(BaseResponse))]
        public IHttpActionResult GreatBattleUserRegistrationAnswer(GreatBattleRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var result = greatBattleManager.RegisterUser(model, userId);
                if (string.IsNullOrEmpty(result.ErrorMessage))
                    return Content(HttpStatusCode.OK, result);
                return Content(HttpStatusCode.BadRequest, new BaseResponse(result.ErrorMessage));
            }

            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }
    }
}