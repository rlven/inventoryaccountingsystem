﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Common;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers.BusinessLogic;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Models;
using System.Threading.Tasks;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/Games")]
    public class GamesController : ApiController
    {
        private readonly GameManager gameManager = new GameManager();

        /// <summary>
        /// Get available games list
        /// </summary>
        /// <returns>List of games</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and games list returned", typeof(List<GameResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no games is database")]
        public IHttpActionResult Get()
        {
            var games = gameManager.GetGames();
            if (games != null)
                return Content(HttpStatusCode.OK, games);
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get user's best game score for specific game
        /// </summary>
        /// <param name="gameId">Specific game id</param>
        /// <returns>User's best game score for specific game</returns>
        [HttpGet]
        [Route("GetBestScore")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and best score of the game returned", typeof(GameScoreResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to get user's best score for the game", typeof(BaseResponse))]
        public IHttpActionResult GetBestScore(int gameId)
        {
            var score = gameManager.UserBestScore(gameId, User.Identity.GetUserId<int>());
            if (string.IsNullOrEmpty(score.ErrorMessage))
                return Content(HttpStatusCode.OK, score);

            return Content(HttpStatusCode.BadRequest, new BaseResponse(score.ErrorMessage));
        }

        /// <summary>
        /// Get user's game history
        /// </summary>
        /// <param name="pageSize">Number of games per page</param>
        /// <param name="page">Page number</param>
        /// <returns>List of games played by the user</returns>
        [HttpGet]
        [Route("GetGameHistory")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and list of the games returned", typeof(List<UserGameHistoryResponse>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that user has no game history")]
        public IHttpActionResult GetGameHistory(int pageSize = 20, int page = 1)
        {
            var userId = User.Identity.GetUserId<int>();
            var purchases = gameManager.GameHistory(userId, pageSize, --page);
            if(purchases.Any())
                return Content(HttpStatusCode.OK, purchases);

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Save user's game result (score)
        /// </summary>
        /// <param name="gameScore">Game score model</param>
        /// <returns>Returns sent game score</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the user's game score successfully saved and saved data returned", typeof(GameScoreResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that received model is invalid or that the error occured while " +
                                                    "trying to save user's game result", typeof(BaseResponse))]
        public async Task<IHttpActionResult> Put([FromBody]GameScore gameScore)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var result = await gameManager.AddGameResultAsync(gameScore, userId);
                if (string.IsNullOrEmpty(result.ErrorMessage))
                    return Content(HttpStatusCode.OK, result);
                return Content(HttpStatusCode.BadRequest, result.ErrorMessage);
            }

            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }
    }
}
