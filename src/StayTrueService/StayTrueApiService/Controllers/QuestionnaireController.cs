﻿using System.Net;
using System.Web.Http;
using Common;
using Microsoft.AspNet.Identity;
using StayTrueApiService.Attributes;
using StayTrueApiService.Managers;
using StayTrueApiService.Models;
using StayTrueApiService.Models.ResponseModels;
using Swagger.Net.Annotations;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    public class QuestionnaireController : ApiController
    {
        private readonly QuestionnaireManager questionnaireManager = new QuestionnaireManager();

        /// <summary>
        /// Get questionnaire
        /// </summary>
        /// <returns>Returns questionnaire</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and questionnaire returned", typeof(QuestionnaireResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are no active questionnaire", typeof(BaseResponse))]
        public IHttpActionResult Get()
        {
            var questionnaire = questionnaireManager.GetQuestionnaire(User.Identity.GetUserId<int>());
            if (!string.IsNullOrEmpty(questionnaire.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(questionnaire.ErrorMessage));
            return Content(HttpStatusCode.OK, questionnaire);
        }

        /// <summary>
        /// Save questionnaire answers
        /// </summary>
        /// <param name="answers">List of questionnaire answers</param>
        /// <returns>Returns questionnaire id and user id</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and questionnaire answers saved", typeof(QuestionnaireAnswersResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that the error occured while trying to save questionnaire answers", typeof(BaseResponse))]
        public IHttpActionResult Post(QuestionnaireAnswers answers)
        {
            if (!ModelState.IsValid)
                return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));

            var answersResponse = questionnaireManager.SaveQuestionnaireAnswers(answers, User.Identity.GetUserId<int>());
            if (string.IsNullOrEmpty(answersResponse.ErrorMessage))
                return Content(HttpStatusCode.BadRequest, new BaseResponse(answersResponse.ErrorMessage));
            return Content(HttpStatusCode.OK, answersResponse);
        }
    }
}