﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Swagger.Net.Annotations;
using Common;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Attributes;
using StayTrueApiService.Models.ResponseModels;

namespace StayTrueApiService.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/Cigarettes")]
    public class CigarettesController : ApiController
    {
        private readonly CigaretteManager cigaretteManager = new CigaretteManager();

        /// <summary>
        /// Get list of available cigarette brands
        /// </summary>
        /// <returns>Returns list of cigarette brands</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and brands list returned", typeof(List<CigaretteBrandResponse>))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Indicates that there are no available brands")]
        public IHttpActionResult Get()
        {
            var cigaretteBrands = cigaretteManager.GetAllBrands();
            if (cigaretteBrands != null)
                return Content(HttpStatusCode.OK, cigaretteBrands );
            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Get specific cigarette brand by id
        /// </summary>
        /// <returns>Return specific cigarette brand</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and brands list returned", typeof(CigaretteBrandResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that requested brand doesn't exist", typeof(BaseResponse))]
        public IHttpActionResult Get(int id)
        {
            var brands = cigaretteManager.GetBrand(id);
            if (brands != null)
                return Content(HttpStatusCode.OK, brands);
            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.TargetNotFound));
        }

        /// <summary>
        /// Get cigarette types grouped by brands
        /// </summary>
        /// <remarks>
        /// Cigarette types returns as dictionary, where key is brand name and value is array of type names
        /// </remarks>
        /// <returns>Cigarette types grouped by brands as dictionary</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetAllCigaretteModels")]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the request successfully completed and types returned", typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that there are no available brands")]
        public IHttpActionResult GetCigaretteBrandTypes()
        {
            var cigaretteModels = cigaretteManager.GetAllModels();
            if (cigaretteModels != null)
                return Content(HttpStatusCode.OK, cigaretteModels);
            return StatusCode(HttpStatusCode.NoContent);
        }

    }
}
