﻿using System.Net;
using System.Web.Http;
using System.Threading.Tasks;
using Swagger.Net.Annotations;
using Common;
using StayTrueApiService.Models;
using StayTrueApiService.Managers.BusinessLogic;
using StayTrueApiService.Models.ResponseModels;
using StayTrueApiService.Managers;

namespace StayTrueApiService.Controllers
{
    [AllowAnonymous]
    public class AccountController : ApiController
    {
        private readonly UserManager userManager = new UserManager();

        /// <summary>
        /// Authorize user
        /// </summary>
        /// <param name="user">Model with authorization data</param>
        /// <param name="app">Application that sends request; For web application value "web", for mobile value "mobile"</param>
        /// <returns>Authorized user info, authorization token</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that user authorization successfully complete and user info with auth token returned", typeof(UserAuthResponse))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "Indicates that the user can't be authorized", typeof(BaseResponse))]
        public async Task<IHttpActionResult> Post([FromBody]UserAuth user, string app = "web")
        {
            var authUser = app.Equals("mobile") ? await userManager.AppAuthorizationUser(user) : await userManager.AuthorizationUser(user);
            if (authUser != null && string.IsNullOrEmpty(authUser.ErrorMessage))
                return Content(HttpStatusCode.OK, new UserAuthResponse {User = authUser, Token = JwtManager.GenerateToken(authUser.Id, user.Phone)});

            return Content(HttpStatusCode.Unauthorized, new BaseResponse(authUser?.ErrorMessage));
        }

        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="user">Register model</param>
        /// <returns>Registered user info</returns>
        [HttpPut]
        [SwaggerResponse(HttpStatusCode.OK, "Indicates that the user successfully registered and user info returned", typeof(RegisterUserResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Indicates that an error has occurred while register user", typeof(BaseResponse))]
        public IHttpActionResult Put([FromBody]RegisterUser user)
        {
            if (ModelState.IsValid)
            {
                var registered = userManager.RegisterUser(user);
                if (string.IsNullOrEmpty(registered.ErrorMessage))
                    return Content(HttpStatusCode.OK, registered);

                return Content(HttpStatusCode.BadRequest, new BaseResponse(registered.ErrorMessage));
            }

            return Content(HttpStatusCode.BadRequest, new BaseResponse(Resources.InvalidModel));
        }


    }
}
