﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace StayTrueService.Extensions
{
    public class HttpExceptionBehavior : BehaviorExtensionElement
    {
        public override Type BehaviorType => typeof(WebHttpBehaviorException);

        protected override object CreateBehavior()
        {
            return new WebHttpBehaviorException();
        }
    }
}