﻿using System;
using System.Net;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

namespace StayTrueService.Extensions
{
    public class ErrorOperationInvoker : IOperationInvoker
    {
        public bool IsSynchronous => true;

        public object[] AllocateInputs()
        {
            return new object[1];
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            var responeObject = new CustomErrorMessage { Message = "Something was wrong." };

            if (WebOperationContext.Current != null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "Something was wrong";
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
            }

            outputs = new object[] { responeObject };

            return Message.CreateMessage(MessageVersion.None, null, responeObject);
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            throw new NotImplementedException();
        }

    }
}