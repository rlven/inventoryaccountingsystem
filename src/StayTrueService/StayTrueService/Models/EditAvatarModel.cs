﻿using System;
using System.Runtime.Serialization;

namespace StayTrueService.Models
{
    [Serializable]
    [DataContract]
    public class EditAvatarModel
    {
        [DataMember]
        public string Avatar { get; set; }
    }
}