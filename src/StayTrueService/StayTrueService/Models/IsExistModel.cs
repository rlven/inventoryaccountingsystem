﻿using System;
using System.Runtime.Serialization;

namespace StayTrueService.Models
{
    [Serializable]
    [DataContract]
    public class IsExistModel
    {
        [DataMember]
        public bool IsExist;
        [DataMember]
        public string Message;

    }
}