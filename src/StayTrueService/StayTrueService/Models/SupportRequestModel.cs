﻿using System;
using System.Runtime.Serialization;

namespace StayTrueService.Models
{
    [DataContract]
    [Serializable]
    public class SupportRequestModel
    {
        [DataMember]
        public int? UserId { set; get; } 
        [DataMember]
        public string Body { set; get; }
        [DataMember]
        public string Email { set; get; }
        [DataMember]
        public string PhoneNumber { set; get; }
        [DataMember]
        public bool IsPasswordForgot { get; set; }
    }
}