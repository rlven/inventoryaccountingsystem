﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Common.Models;

namespace StayTrueService.Models
{
    [Serializable]
    [DataContract]
    public class MobileVersion : BaseAnswer
    {
        [DataMember]
        public string AndroidV { get; set; }
        [DataMember]
        public string IosV { get; set; }
        [DataMember]
        public string AndroidPath { get; set; }
        [DataMember]
        public string IosPath { get; set; }
    }
}