﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using Common.Models;
using NLog;
using StayTrueService.Helpers;

namespace StayTrueService.Behaviors
{
    public class AuthorizationOperationBehaviorAttribute : Attribute, IOperationBehavior
    {
        public void Validate(OperationDescription operationDescription)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new AuthorizationOperationInvoker(dispatchOperation.Invoker,
                operationDescription.SyncMethod);
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }
    }

    public class AuthorizationOperationInvoker : IOperationInvoker
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IOperationInvoker _invoker;
        private readonly MethodInfo _methodInfo;
        public AuthorizationOperationInvoker(IOperationInvoker invoker, MethodInfo methodInfo)
        {
            _invoker = invoker;
            _methodInfo = methodInfo;
        }

        public object[] AllocateInputs()
        {
            return _invoker.AllocateInputs();
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            outputs = null;
            var ctx = WebOperationContext.Current;
            try
            {
                var invokationResult = _invoker.Invoke(instance, inputs, out outputs);

                if (!(invokationResult is BaseAnswer))
                {
                    return invokationResult;
                }

                
                var baseAnswer = invokationResult as BaseAnswer;

                if (!string.IsNullOrWhiteSpace(baseAnswer.ErrorMessage))
                {
                    return invokationResult;
                }

                
                var tokenHelper = new TokenHelper();
                var account = baseAnswer as Account;

                ctx = WebOperationContext.Current;

                if (account != null)
                {
                    string token = tokenHelper.GenerateToken($"{account.Id}", DateTime.Now);

                    Service.TokensStore.Add(new KeyValuePair<string, DateTime>(token, DateTime.Now));

                    ctx?.OutgoingResponse.Headers.Add(HttpResponseHeader.WwwAuthenticate, token);
                }
                    
                return invokationResult;
            }
            catch (Exception exc)
            {
                _logger.Error(exc, exc.Message, null);
                ctx?.OutgoingResponse.Headers.Add(HttpResponseHeader.Warning, exc.Message);
                var val = Activator.CreateInstance(_methodInfo.ReturnType);
                if (_methodInfo.ReturnType.IsSubclassOf(typeof(BaseAnswer)))
                    ((BaseAnswer) val).ErrorMessage = exc.Message;
                ((Account)val).BirthDate = DateTime.Now;
                return val;
            }
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return _invoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return _invoker.InvokeEnd(instance, out outputs, result);
        }

        public bool IsSynchronous => true;
    }
}