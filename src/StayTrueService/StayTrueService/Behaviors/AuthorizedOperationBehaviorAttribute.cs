﻿using System;
using System.Net;
using System.Reflection;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using Common;
using Common.Logger;
using NLog;
using StayTrueService.Helpers;

namespace StayTrueService.Behaviors
{
    public class AuthorizedOperationBehaviorAttribute : Attribute, IOperationBehavior
    {
        public void Validate(OperationDescription operationDescription)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new AuthorizedOperationInvoker(dispatchOperation.Invoker, operationDescription.SyncMethod);
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }
    }

    public class AuthorizedOperationInvoker : IOperationInvoker
    {
        private readonly IOperationInvoker _invoker;
        private readonly MethodInfo _methodInfo;
        private readonly Logger _logger;

        public AuthorizedOperationInvoker(IOperationInvoker invoker, MethodInfo methodInfo)
        {
            _invoker = invoker;
            _methodInfo = methodInfo;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public object[] AllocateInputs()
        {
            return _invoker.AllocateInputs();
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            var ctx = WebOperationContext.Current;
            try
            {
                String authToken = ctx?.IncomingRequest.Headers[HttpRequestHeader.Authorization];

                Log.Info("token: " + authToken);

                string[] tokenPart = authToken != null ? authToken.Split(' ') : new string [] {""};
                
                var tokenHelper = new TokenHelper();

                if (tokenHelper.ValidateToken(tokenPart.Length > 1 ? tokenPart[1] : tokenPart[0], out var clientId) != TokenValidationResult.Valid)
                {
                    outputs = null;
                    if (ctx != null)
                    {
                        ctx.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                    }

                    var retType = _methodInfo.ReturnType;

                    return retType == typeof(string) ? Resources.Unauthorized : null;
                }

                if (!(instance is IService service))
                {
                    return _invoker.Invoke(instance, inputs, out outputs);
                }
                
                service.UserId = clientId;

                return _invoker.Invoke(instance, inputs, out outputs);
            }
            catch (Exception exception)
            {

                Log.Error(exception);
                Log.Error(exception.StackTrace);

                _logger.Error(exception, exception.Message, null);

                if (ctx != null)
                {
                    ctx.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                }
                outputs = null;
                return exception.Message;
            }
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return _invoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return _invoker.InvokeEnd(instance, out outputs, result);
        }

        public bool IsSynchronous => true;
    }
}