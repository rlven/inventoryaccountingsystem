﻿using System.Configuration;
using Microsoft.Practices.Unity.Configuration;
using Unity;

namespace BusinessLogicLayer
{

    public class Container : UnityContainer
    {

    }
    public class DependencyFactory
    {
        public static IUnityContainer Container { get; set; }

        static DependencyFactory()
        {
            var container = new UnityContainer();

            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");

            section?.Configure(container);
            container.RegisterType<BusinessLogicProvider>();
            Container = container;
        }
    }
}
