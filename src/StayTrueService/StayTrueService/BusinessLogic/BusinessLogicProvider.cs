﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BusinessLogicLayer.Extensions;
using BusinessLogicLayer.Managers;
using Common;
using Common.ChatModels;
using Common.Models;
using DataAccessLayer;
using StayTrueService.BusinessLogic.Managers;
using StayTrueService.Models;

namespace BusinessLogicLayer
{
    public class BusinessLogicProvider
    {


        private static readonly string ChatUrl = "https://webapp-msfwex01-21-staytrueapp-qa.azurewebsites.net/chat/api/v1/auth/set_access_token";



        public Account AuthorizationUser(UserAuth user, string application)
        {
            UserManager userManager = new UserManager();
            return userManager.AuthorizationUser(user, application);
        }

        public string ValidateUser(User user)
        {
            UserManager manager = new UserManager();
            return manager.ValidateUser(user);
        }

        public User EditUserProfile(User user)
        {
            UserManager manager = new UserManager();
            return manager.ChangeUserProfile(user);
        }

        public Stream GetUserFrontPassport(int userId)
        {
            UserManager manager = new UserManager();
            return null;
        }

        public Stream GetUserBackPassport(int userId)
        {
            UserManager manager = new UserManager();
            return null;
        }

        public String GetAvatarImage(int userId)
        {
            string defaultImg = "/Images/Avatars/Default.jpg";

            using (var context = new StayTrueDBEntities())
            {
                Users user = context.Users.SingleOrDefault(x => x.Id == userId);

                if (user == null)
                    return defaultImg;

                return user.avatarsmall != null ? user.avatarsmall : defaultImg;
            }
        }

        public string ChangeUserPassword(User user)
        {
            UserManager manager = new UserManager();
            return manager.ChangeUserPassword(user);
        }


        public int GetWincoin(int id)
        {
            UserManager manager = new UserManager();
            return manager.GetUserWincoins(id);
        }


        public Config AddConfigurations(Config config)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.AddConfigurations(config);
        }

        public Config EditConfigurations(Config config)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.EditConfigurations(config);
        }

        public string DeleteConfigurations(Config config)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.DeleteConfigurations(config);
        }

        public List<Config> GetAllConfigurations()
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.GetAllConfigurations();
        }

        public TermOfUse AddTermOfUse(TermOfUse termOfUse)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.AddTermOfUse(termOfUse);
        }

        public TermOfUse EditTermOfUse(TermOfUse termOfUse)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.EditTermOfUse(termOfUse);
        }

        public TermOfUse GetTermOfUse(string appType)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.GeTermOfUse(appType);
        }

        public RulesOfUse AddRulesOfUse(RulesOfUse rulesOfUse)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.AddRulesOfUse(rulesOfUse);
        }

        public RulesOfUse EditRulesOfUse(RulesOfUse rulesOfUse)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.EditRulesOfUse(rulesOfUse);
        }

        public RulesOfUse GetRulesOfUse(string appType)
        {
            ConfigurationsManager manager = new ConfigurationsManager();
            return manager.GetRulesOfUse(appType);
        }

        public List<NewsModel> GetNews(int userId, int newsCategory, int page, int quantity, string application, string folderUrl)
        {
            NewsManager newsManager = new NewsManager();
            return newsManager.GetNews(userId, newsCategory, page, quantity, application, folderUrl);
        }

        public List<Banner> GetBanners()
        {
            using (var context = new StayTrueDBEntities())
            {
                return context.Banners.Where(x => x.IsActive).OrderByDescending(x => x.Id).Take(4).Select(x => new Banner
                {
                    Id = x.Id,
                    ImagePath = x.ImagePath,
                    Name = x.Name
                }).ToList();
            }

        }

        public Banner AddBanner(Banner banner, string folderPath)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbBanner = new Banners
            {
                Name = banner.Name,
                IsActive = banner.IsActive
            };
            context.Banners.Add(dbBanner);
            context.SaveChanges();
            if (!string.IsNullOrEmpty(banner.ImagePath))
            {
                dbBanner.ImagePath = ConvertFileHelper.SaveBannerImage(folderPath, banner.ImagePath, $"{dbBanner.Id}");
            }
            context.SaveChanges();
            return new Banner
            {
                Id = dbBanner.Id,
                Name = dbBanner.Name,
                ImagePath = dbBanner.ImagePath,
                IsActive = dbBanner.IsActive
            };
        }

        public Banner EditBanner(Banner banner, string folderPath)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var bannerFromContext = context.Banners.SingleOrDefault(x => x.Id == banner.Id);
            if (bannerFromContext != null)
            {
                bannerFromContext.Id = banner.Id;
                bannerFromContext.Name = banner.Name;
                bannerFromContext.ImagePath = banner.ImagePath;

                if (!string.IsNullOrEmpty(banner.ImagePath))
                {
                    bannerFromContext.ImagePath = ConvertFileHelper.SaveBannerImage(folderPath, banner.ImagePath, $"{bannerFromContext.Id}");
                }
                context.SaveChanges();
                return new Banner
                {
                    Id = bannerFromContext.Id,
                    Name = bannerFromContext.Name,
                    ImagePath = bannerFromContext.ImagePath
                };
            }
            return null;
        }

        public string DeleteBanner(Banner banner)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var bannerFromContext = context.Banners.SingleOrDefault(x => x.Id == banner.Id);
            if (bannerFromContext != null)
            {
                context.Banners.Remove(bannerFromContext);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        public int GetProductPagesCount(int shopId, int quantity)
        {
            ShopManager manager = new ShopManager();
            return manager.GetProductPagesCount(shopId, quantity);
        }



        public int GetGamesPagesCount()
        {
            GameManager manager = new GameManager();
            return manager.GetGamesPagesCount();
        }

        public Album AddCategoryAlbum(Album album, string folderPath)
        {
            AlbumManager manager = new AlbumManager();
            return manager.AddCategoryAlbum(album, folderPath);
        }

        public AlbumImage AddImageInAlbum(AlbumImage albumImage, string folderPath)
        {
            AlbumManager manager = new AlbumManager();
            return manager.AddAlbumImage(albumImage, folderPath);
        }

        public AlbumImage EditImageInAlbum(AlbumImage ai, string folderPath)
        {
            AlbumManager manager = new AlbumManager();
            return manager.EditImageInAlbum(ai, folderPath);
        }

        public string DeleteImageInAlbum(AlbumImage ai)
        {
            AlbumManager manager = new AlbumManager();
            return manager.DeleteImageInAlbum(ai);
        }

        public string DeleteCategory(Album album)
        {
            AlbumManager manager = new AlbumManager(); ;
            return manager.DeleteCategory(album);
        }

        public Album EditAlbum(Album album, string folderPath)
        {
            AlbumManager manager = new AlbumManager();
            return manager.EditAlbum(album, folderPath);
        }

        public int GetLikeAmount(int newsId, StayTrueDBEntities context)
        {
            LikeManager likeManager = new LikeManager();
            return likeManager.GetLikeAmount(newsId, context);
        }

        public string SupportRequest(SupportRequestModel supportRequest)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                try
                {
                    Users user = new Users();
                    if (supportRequest.UserId.HasValue)
                    {
                        user = context.Users.Find(supportRequest.UserId);
                    }
                    else
                    {
                        user = context.Users.SingleOrDefault(u => u.Phone == supportRequest.PhoneNumber);
                    }

                    if (user != null)
                    {
                        var userSupportPasswordDropRequest = context.SupportRequests.FirstOrDefault(r => r.UserId == user.Id
                                                                        && r.IsPasswordLost && !r.IsAnswered && !r.IsDeleted);
                        if (string.IsNullOrEmpty(supportRequest.PhoneNumber))
                            supportRequest.PhoneNumber = user.Phone;
                        if (string.IsNullOrEmpty(supportRequest.Email))
                            supportRequest.Email = user.Email;
                        if (userSupportPasswordDropRequest != null && supportRequest.IsPasswordForgot)
                        {
                            userSupportPasswordDropRequest.Email = supportRequest.Email;
                            userSupportPasswordDropRequest.Phone = supportRequest.PhoneNumber;
                            userSupportPasswordDropRequest.Body = supportRequest.Body;
                            userSupportPasswordDropRequest.UserId = user.Id;
                            userSupportPasswordDropRequest.CreationDate = DateTime.Now;
                            userSupportPasswordDropRequest.IsPasswordLost = true;
                            userSupportPasswordDropRequest.IsAnswered = false;
                            userSupportPasswordDropRequest.IsMessageSent = false;
                       
                            context.SaveChanges();
                            return Resources.Ok;
                        } 

                        var dbRequest = new SupportRequests
                        {
                            UserId = user.Id,
                            Body = supportRequest.Body,
                            CreationDate = DateTime.Now,
                            Phone = supportRequest.PhoneNumber,
                            Email = supportRequest.Email,
                            IsAnswered = false,
                            IsPasswordLost = supportRequest.IsPasswordForgot
                        };

                        context.SupportRequests.Add(dbRequest);

                        context.SaveChanges();

                        return Resources.Ok;
                    }

                    return Resources.AccountWasNotFound;
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        public List<SupportRequest> GetSupportRequests(int page)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                try
                {
                    var requests = context.SupportRequests.OrderByDescending(x => x.Id).ToList();
                    var totalPages = (requests.Count + 20 - 1) / 20;
                    if (page > totalPages) return null;
                    return requests.Page(page, 20).Select(x => new SupportRequest
                    {
                        Id = x.Id,
                        Body = x.Body
                    }).ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }


        public List<Shop> GetShops()
        {
            ShopManager manager = new ShopManager();
            return manager.GetShops();
        }



        public City AddCity(City city)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbCity = new Cities
            {
                Id = city.Id,
                CityName = city.Name
            };
            context.Cities.Add(dbCity);
            context.SaveChanges();
            return new City
            {
                Id = dbCity.Id,
                Name = dbCity.CityName
            };
        }

        public List<City> GetAllCities()
        {
            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    return context.Cities.Select(x => new City
                    {
                        Id = x.Id,
                        Name = x.CityName
                    }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }



        private T Execute<T>(Func<T> request) where T : BaseAnswer, new()
        {
            try
            {
                return request();
            }
            catch (Exception exc)
            {
                return ResourceHelper<T>.CreateLocalizedAnswer(exc.Message);
            }
        }

        private string Execute(Func<string> request)
        {
            try
            {
                return request();
            }
            catch (Exception exc)
            {
                //Logger.Log(LogLevel.Error, CultureInfo.InvariantCulture, exc);
                return exc.Message;
            }
        }



        public void SendAuthToken(int id, string token, bool isAdmin)
        {
            using (var context = new StayTrueDBEntities())
            {
                AsyncWebRequestSender asyncWebRequestSender;
                Dictionary<string, string> body;
                switch (isAdmin)
                {
                    case true:
                        var admin = context.Administrators.SingleOrDefault(x => x.Id == id);
                        asyncWebRequestSender = new AsyncWebRequestSender();
                        body = new Dictionary<string, string>();
                        body.Add("phone", admin.Login);
                        body.Add("accessToken", token);
                        var a = asyncWebRequestSender.PostFormDataAsync<AccessTokenResponseModel>(ChatUrl, body);

                        break;
                    case false:
                        var user = context.Users.SingleOrDefault(x => x.Id == id);
                        asyncWebRequestSender = new AsyncWebRequestSender();
                        body = new Dictionary<string, string>();
                        body.Add("phone", user.Phone);
                        body.Add("accessToken", token);
                        var b = asyncWebRequestSender.PostFormDataAsync<AccessTokenResponseModel>(ChatUrl, body);
                        break;
                }
            }
        }
    }
}