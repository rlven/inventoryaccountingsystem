﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Extensions;
using Common;
using Common.Enums;
using Common.Models;
using DataAccessLayer;
using NewsDb = DataAccessLayer.News;

namespace BusinessLogicLayer.Managers
{
    internal class NewsManager
    {

        private int GetNewsLikesCount(int newsId, StayTrueDBEntities context)
        {
            return new LikeManager().GetLikeAmount(newsId, context);
        }

        public NewsModel AddNews(string folderPath, NewsModel newsModel, byte[] imageM, byte[] imageP, byte[] videoM, byte[] videoP)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var dbNews = new NewsDb
                        {
                            NameRu = newsModel.NameRu,
                            NameKg = newsModel.NameKg,
                            DescriptionRu = newsModel.DescriptionRu,
                            DescriptionKg = newsModel.DescriptionKg,
                            NewsCategory = newsModel.NewsCategory,
                            NewsType = newsModel.NewsType,
                            IsActive = true,
                            CreationDate = DateTime.Now,
                            IsBrand = newsModel.IsBrand
                        };
                        context.News.Add(dbNews);
                        context.SaveChanges();


                        if (imageP !=null)
                        {
                            dbNews.ImageP = ConvertFileHelper.SaveImageP(folderPath, imageP, $"{dbNews.Id}");
                        }
                        if (imageM!=null)
                        {
                            dbNews.ImageM = ConvertFileHelper.SaveImageM(folderPath, imageM, $"{dbNews.Id}");
                        }
                        if (videoM!=null)
                            dbNews.VideoM = ConvertFileHelper.SaveVideoM(folderPath, videoM, $"{dbNews.Id}");

                        if (videoP!=null)
                            dbNews.VideoP = ConvertFileHelper.SaveVideoP(folderPath, videoP, $"{dbNews.Id}");
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        return new NewsModel
                        {
                            Id = dbNews.Id,
                            ImageP = dbNews.ImageP,
                            ImageM = dbNews.ImageM,
                            NameKg = dbNews.NameKg,
                            NameRu = dbNews.NameRu,
                            DescriptionKg = dbNews.DescriptionKg,
                            DescriptionRu = dbNews.DescriptionRu,
                            IsActive = dbNews.IsActive,
                            NewsType = dbNews.NewsType,
                            VideoP = dbNews.VideoP,
                            VideoM = dbNews.VideoM,
                            NewsCategory = dbNews.NewsCategory,
                            CreationDate = dbNews.CreationDate
                        };
                    }
                    catch (Exception e)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return null;
        }

        public string DeleteNews(NewsModel newsModel)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var dbNews = context.News.SingleOrDefault(x => x.Id == newsModel.Id);
                    if (dbNews == null) return Resources.NewsNotFound;
                    context.News.Remove(dbNews);
                    context.SaveChanges();
                    return Resources.Ok;
                }
                catch (Exception)
                {
                    return Resources.UnknownError;
                }
            }
        }

        public NewsModel EditNews(string folderPath, NewsModel newsModel)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var imageP = ConvertFileHelper.SaveImage(folderPath, newsModel.ImageP, $"{newsModel.Id}");
                    var imageM = ConvertFileHelper.SaveImage(folderPath, newsModel.ImageM, $"{newsModel.Id}");
                    var videoP = ConvertFileHelper.SaveVideo(folderPath, newsModel.VideoP, $"{newsModel.Id}");
                    var videoM = ConvertFileHelper.SaveVideo(folderPath, newsModel.VideoM, $"{newsModel.Id}");
                    var dbNews = context.News.SingleOrDefault(x => x.Id == newsModel.Id);
                    dbNews.DescriptionRu = newsModel.DescriptionRu;
                    dbNews.DescriptionKg = newsModel.DescriptionKg;
                    dbNews.NameRu = newsModel.NameRu;
                    dbNews.NameKg = newsModel.NameKg;
                    dbNews.ImageM = imageM;
                    dbNews.ImageP = imageP;
                    dbNews.VideoM = videoM;
                    dbNews.VideoP = videoP;
                    dbNews.NewsCategory = newsModel.NewsCategory;
                    dbNews.NewsType = newsModel.NewsType;
                    dbNews.IsBrand = newsModel.IsBrand;
                    context.SaveChanges();
                    return new NewsModel
                    {
                        Id = newsModel.Id,
                        NameRu = newsModel.NameRu,
                        NameKg = newsModel.NameKg,
                        DescriptionRu = newsModel.DescriptionRu,
                        DescriptionKg = newsModel.DescriptionKg,
                        ImageM = imageM,
                        ImageP = imageP,
                        VideoM = videoM,
                        VideoP = videoP,
                        NewsCategory = newsModel.NewsCategory,
                        NewsType = newsModel.NewsType
                    };
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<NewsModel> GetAllNews(int categoryId, int typeId, int userId, int skip, int take)
        {
            try
            {
                using (var context = new StayTrueDBEntities())
                {

                    DateTime now = DateTime.Now;

                    IQueryable<News> news = context.News.Where(x => x.IsDeleted == false && x.IsActive && now >= x.PublicationDate).OrderByDescending(x => x.Id);


                    if (categoryId > 0)
                        news = news.Where(x => x.NewsCategory == categoryId);

                    if (typeId > 0)
                        news = news.Where(x => x.NewsType == typeId);

                    if (skip > 0)
                        news = news.Skip(skip);

                    if (take > 0)
                        news = news.Take(take);


                    return news.ToList().Select(x => new NewsModel
                    {
                        Id = x.Id,
                        NameRu = x.NameRu,
                        NameKg = DateTime.Now.ToString(),
                        DescriptionRu = x.DescriptionRu,
                        DescriptionKg = x.DescriptionKg,
                        ImageP = x.ImageP,
                        ImageM = x.ImageM,
                        VideoP = x.VideoP,
                        VideoM = x.VideoM,
                        CreationDate = x.CreationDate,
                        NewsType = x.NewsType,
                        UsersLikesCount = GetNewsLikesCount(x.Id, context),
                        NewsCategory = x.NewsCategory,
                        UserIsLiked = CheckUserLike(x.Id, userId, context)

                    }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        private string GetUrl(string folderUrl, string imageUrl)
        {
            return string.IsNullOrEmpty(imageUrl) ? null : $"{folderUrl}{imageUrl}";
        }
 

        private bool CheckUserLike(int newsId, int userId, StayTrueDBEntities context)
        {
            return context.Likes.Any(x => x.NewsId == newsId && x.UserId == userId);
        }

        public List<NewsModel> GetNews(int userId, int newsCategory, int page, int quantity, string application, string folderUrl)
        {
            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    var brand = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.Brand));
                    var isBrand = brand != null && Convert.ToBoolean(brand.Value);
                    List<NewsDb> news;
                    switch (application)
                    {
                        case "web":
                            news = context.News.Where(x => x.NewsCategory == newsCategory && x.NewsType == 
                            (int)NewsTypeEnum.Medium && x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();
                            break;
                        case "mobile":
                            news = context.News.Where(x => (isBrand || x.IsBrand != true) && x.NewsCategory ==
                            newsCategory && x.NewsType == (int)NewsTypeEnum.Medium && x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();
                            break;
                            default:
                                news = context.News.Where(x => x.NewsCategory == newsCategory && x.NewsType == 
                                (int)NewsTypeEnum.Medium && x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();
                            break;
                    }
                    
                    var totalPages = (news.Count + quantity - 1) / quantity;
                    if (page > totalPages) return null;
                    return news.Page(page, quantity).Select(x => new NewsModel
                    {
                        Id = x.Id,
                        NameRu = x.NameRu,
                        NameKg = x.NameKg,
                        DescriptionRu = x.DescriptionRu,
                        DescriptionKg = x.DescriptionKg,
                        ImageP = x.ImageP,
                        ImageM = x.ImageM,
                        VideoP = x.VideoP,
                        VideoM = x.VideoM,
                        NewsType = x.NewsType,
                        CreationDate = x.CreationDate,
                        UsersLikesCount = GetNewsLikesCount(x.Id, context),
                        NewsCategory = x.NewsCategory,
                        UserIsLiked = CheckUserLike(x.Id, userId, context)
                    }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool CheckUser(int userId, int newsId)
        {
            using (var context = new StayTrueDBEntities())
            {
                return context.Likes.Any(x => x.NewsId == newsId && x.UserId == userId);
            }
        }
    }
}
