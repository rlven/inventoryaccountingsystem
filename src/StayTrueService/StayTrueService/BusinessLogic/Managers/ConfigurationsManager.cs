﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Enums;
using Common.Models;
using DataAccessLayer;
using StayTrueService.Models;

namespace BusinessLogicLayer.Managers
{
    internal class ConfigurationsManager
    {

        public Config AddConfigurations(Config config)
        {
            var context = new StayTrueDBEntities();
            var dbConfigurations = new Configurations
            {
                Name = config.Name
            };
            context.Configurations.Add(dbConfigurations);
            context.SaveChanges();
            return new Config
            {
                Id = dbConfigurations.Id,
                Name = dbConfigurations.Name,
                Value = dbConfigurations.Value
                
            };
        }
        public Config EditConfigurations(Config config)
        {
            var context = new StayTrueDBEntities();
            var dbConfigurations = context.Configurations.SingleOrDefault(x => x.Id == config.Id);

            if (dbConfigurations != null)
            {
                dbConfigurations.Id = config.Id;
                dbConfigurations.Name = config.Name;
                dbConfigurations.Value = config.Value;
                context.SaveChanges();
                return new Config
                {
                    Id = dbConfigurations.Id,
                    Name = dbConfigurations.Name,
                    Value = dbConfigurations.Value
                };
            }
            return null;
        }

        public string DeleteConfigurations(Config config)
        {
            var context = new StayTrueDBEntities();
            var dbConfigurations = context.Configurations.SingleOrDefault(x => x.Id == config.Id);

            if (dbConfigurations != null)
            {
                context.Configurations.Remove(dbConfigurations);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        #region TermOFUse

        public MobileVersion GetMobileVersions()
        {
            using (var context = new StayTrueDBEntities())
            {
                var currVersion = context.MobileVersions.OrderByDescending(r => r.Id).FirstOrDefault();
                if(currVersion==null)
                    return new MobileVersion(){ErrorMessage = Resources.ConfigNotFound};
                return new MobileVersion()
                {
                    IosPath = currVersion.IosPath,
                    AndroidPath = currVersion.AndroidPath,
                    AndroidV = currVersion.AndroidVersion,
                    IosV = currVersion.IosVersion
                };
            }
        }

        public TermOfUse GeTermOfUse(string appType)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var ru = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseRu))?.Value;
                    var kg = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseKg))
                        ?.Value;


                    if (String.IsNullOrEmpty(appType))
                    {
                        string kgText = kg.Replace("<p>", "\n\t");
                        kgText = kgText.Replace("</p>", string.Empty);

                        string ruText = ru.Replace("<p>", "\n\t");
                        ruText = ruText.Replace("</p>", string.Empty);

                        return new TermOfUse()
                        {
                            TermOfUseKg = kgText,
                            TermOfUseRu = ruText
                        };
                    }
                    return new TermOfUse()
                    {
                        TermOfUseRu = ru,
                        TermOfUseKg = kg
                    };


                }
                catch (Exception)
                {
                    return null;
                }
            }

        }
    

     

        public TermOfUse EditTermOfUse(TermOfUse termOfUse)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var termOfUseRu =
                        context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseRu));
                    var termOfUseKg =
                        context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseKg));
                    if (termOfUseRu != null && termOfUseKg != null)
                    {
                        termOfUseRu.Value = termOfUse.TermOfUseRu;
                        termOfUseRu.Value = termOfUse.TermOfUseKg;
                        context.SaveChanges();
                        return new TermOfUse()
                        {
                            TermOfUseKg = termOfUseKg.Value,
                            TermOfUseRu = termOfUseRu.Value
                        };
                    }
                    return new TermOfUse()
                    {
                        ErrorMessage = Resources.EmptyFields
                    };
                    
                }
                catch (Exception)
                {
                    return new TermOfUse()
                    {
                        ErrorMessage = Resources.UnknownError
                    };
                }
            }
        }

        public TermOfUse AddTermOfUse(TermOfUse termOfUse)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var termOfUseRu =
                            context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseRu));
                        var termOfUseKg =
                            context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.LicenseKg));
                        if (termOfUseRu == null && termOfUseKg == null)
                        {
                            context.Configurations.Add(new Configurations{Name = ConfigurationValues.LicenseRu, Value = termOfUse.TermOfUseRu});
                            context.Configurations.Add(new Configurations{Name = ConfigurationValues.LicenseKg, Value = termOfUse.TermOfUseKg});
                            context.SaveChanges();
                            dbTransaction.Commit();
                            return termOfUse;
                        }
                        return new TermOfUse()
                        {
                            ErrorMessage = Resources.DataExist
                        };
                    }
                    catch (Exception)
                    {
                        dbTransaction.Rollback();
                        return new TermOfUse()
                        {
                            ErrorMessage = Resources.UnknownError
                        };
                    }
                }
            }
        }
        #endregion

        #region RulesOfUse
        public RulesOfUse GetRulesOfUse(string appType)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var ru = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesRu))?.Value;
                    var kg = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesKg))
                        ?.Value;

                    if (String.IsNullOrEmpty(appType))
                    {
                        string kgText = kg.Replace("<p>", "\n\t");
                        kgText = kgText.Replace("</p>", string.Empty);

                        string ruText = ru.Replace("<p>", "\n\t");
                        ruText = ruText.Replace("</p>", string.Empty);

                        return new RulesOfUse()
                        {
                            RulesOfUseKg = kgText,
                            RulesOfUseRu = ruText
                        };
                    }
                    return new RulesOfUse
                    {
                        RulesOfUseRu = ru,
                        RulesOfUseKg = kg
                    };
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }



        public RulesOfUse EditRulesOfUse(RulesOfUse rulesOfUse)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var rulesOfUseRu =
                        context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesRu));
                    var rulesOfUseKg =
                        context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesKg));
                    if (rulesOfUseRu != null && rulesOfUseKg != null)
                    {
                        rulesOfUseRu.Value = rulesOfUse.RulesOfUseRu;
                        rulesOfUseKg.Value = rulesOfUse.RulesOfUseKg;
                        context.SaveChanges();
                        return new RulesOfUse()
                        {
                            RulesOfUseKg = rulesOfUseKg.Value,
                            RulesOfUseRu = rulesOfUseRu.Value
                        };
                    }
                    return new RulesOfUse()
                    {
                        ErrorMessage = Resources.EmptyFields
                    };

                }
                catch (Exception)
                {
                    return new RulesOfUse()
                    {
                        ErrorMessage = Resources.UnknownError
                    };
                }
            }
        }

        public RulesOfUse AddRulesOfUse(RulesOfUse rulesOfUse)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var rulesOfUseRu =
                            context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesRu));
                        var rulesOfUseKg =
                            context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.RulesKg));
                        if (rulesOfUseRu == null && rulesOfUseKg == null)
                        {
                            context.Configurations.Add(new Configurations { Name = ConfigurationValues.RulesRu, Value = rulesOfUse.RulesOfUseRu });
                            context.Configurations.Add(new Configurations { Name = ConfigurationValues.RulesKg, Value = rulesOfUse.RulesOfUseKg });
                            context.SaveChanges();
                            dbTransaction.Commit();
                            return rulesOfUse;
                        }
                        return new RulesOfUse()
                        {
                            ErrorMessage = Resources.DataExist
                        };
                    }
                    catch (Exception)
                    {
                        dbTransaction.Rollback();
                        return new RulesOfUse()
                        {
                            ErrorMessage = Resources.UnknownError
                        };
                    }
                }
            }
        }

        #endregion
        public List<Config> GetAllConfigurations()
        {
            var context = new StayTrueDBEntities();
            var dbConfigurations = context.Configurations.OrderBy(x => x.Id).ToList();
           
            return dbConfigurations.Select(x => new Config()
            {
                Id = x.Id,
                Name = x.Name,
                Value = x.Value
            }).ToList();
           
        }
    }
}
