﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BusinessLogicLayer.Managers;
using Common;
using Common.Enums;
using Common.Logger;
using Common.Logger.Models;
using Common.Models;
using Common.Security;
using DataAccessLayer;
using Newtonsoft.Json;
using StayTrueService.BusinessLogic.Extensions;
using StayTrueService.Helpers;
using StayTrueService.Models;
using InvitedUsers = Common.Models.InvitedUsers;

namespace StayTrueService.BusinessLogic.Managers
{
    public class UserManager
    {
        private static readonly string ChatUrl = ConfigurationManager.AppSettings["ChatServiceUrl"];
        private static readonly string ChatLogin = ConfigurationManager.AppSettings["ChatLogin"];
        private static readonly string ChatPassword = ConfigurationManager.AppSettings["ChatPassword"];
        private static string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static string folderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly int SignInAttemptLimit = 3;
        private const int ImageHeight = 150;

        public Account AppAuthorizationUser(User account, string applicationType)
        {
            Account accountResult = new Account();

            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Phone == account.Phone);

                if (user == null)
                {
                    accountResult.ErrorMessage = Resources.IncorrectLoginOrPassword;
                    return accountResult;
                }

                if (user.SignInAttemptCount >= SignInAttemptLimit)
                {
                    if (!ReCaptcha.ValidateTime(user.RefreshDate))
                    {
                        accountResult.ErrorMessage = Resources.IncorrectLoginOrPassword;
                        return accountResult;
                    }
                }
                else
                {
                    user.RefreshDate = DateTime.Now;
                    context.SaveChanges();
                }


                if (!(ValidateBCryptePassword(account.Password, user.Password) && user.StatusId == (int)StatusEnum.Active))
                {
                    Log.Info(Resources.IncorrectLoginOrPassword);

                    user.SignInAttemptCount += 1;
                    context.SaveChanges();

                    accountResult.ErrorMessage = Resources.IncorrectLoginOrPassword;
                    return accountResult;
                }

                CheckUserFirstSignIn(user);

                user.SignInCounterM++;
                user.LastSignInM=DateTime.Now;
                user.SignInAttemptCount = 0;

                long utcTimeSec = (Int64)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                long utcTimeMs = (Int64)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

                String chatToken = CrossPlatformAESEncryption.Encrypt($"{utcTimeMs}/{user.Id}");

                context.ChatTokens.Add(new ChatTokens { insert_date = utcTimeSec, token = chatToken });

                context.SaveChanges();

                //UserChatAuthorizeAsync(chatToken);

                return new Account
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Email = user.Email,
                    BirthDate = user.BirthDate,
                    IsApproved = user.IsApproved,
                    StatusId = user.StatusId,
                    Phone = user.Phone,
                    Wincoins = user.Wincoins,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType,
                    ChatToken = chatToken,
                    CityName = user.Cities.CityName
                };

            }
        }

        public Account AuthorizationUser(UserAuth account, string applicationType)
        {

            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Phone == account.Phone);

                if (user == null)
                {
                    return new Account { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.StatusId != (int)StatusEnum.Active)
                {
                    // return new Account { ErrorMessage = Resources.AccountIsNotActive }; -безопасность - возвращать одинаковый ответ
                    return new Account { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.SignInAttemptCount >= SignInAttemptLimit)
                {
                    if (!ReCaptcha.Validate(account.Captcha))
                    {
                        return new Account { ErrorMessage = Resources.IncorrectLoginOrPassword };
                    }
                }

                if (!user.IsApproved)
                {
                    //   return new Account { ErrorMessage = Resources.AccountIsNotApproved };
                    return new Account { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (user.IsDeleted)
                {
                    // return new Account { ErrorMessage = Resources.AccountIsNotActive };
                    return new Account { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }

                if (!ValidateBCryptePassword(account.Password, user.Password))
                {
                    user.SignInAttemptCount += 1; context.SaveChanges();
                    return new Account { ErrorMessage = Resources.IncorrectLoginOrPassword };
                }


                user.SignInCounterP++;
                user.LastSignInP=DateTime.Now;
                user.SignInAttemptCount = 0;

                long utcTimeSec = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                long utcTimeMs = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;

                String chatToken = CrossPlatformAESEncryption.Encrypt($"{utcTimeMs}/{user.Id}");
               

                context.ChatTokens.Add(new ChatTokens { insert_date = utcTimeSec, token = chatToken });

                context.SaveChanges();

                //UserChatAuthorizeAsync(chatToken);

                return new Account
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Email = user.Email,
                    BirthDate = user.BirthDate,
                    IsApproved = user.IsApproved,
                    StatusId = user.StatusId,
                    Phone = user.Phone,
                    Wincoins = user.Wincoins,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType,
                    ChatToken = chatToken,
                    CityName = user.Cities.CityName
                };
                    
            }
        }

        public Account RegisterUser(User user)
        {

            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {

                if (context.Users.Any(x => x.Phone == user.Phone || user.IsDeleted))
                {
                    return new Account { ErrorMessage = Resources.UserExist, BirthDate = DateTime.Now };
                }

                Users dbUser = CreateNewUser(user);

                context.Users.Add(dbUser);
                
                SavePassportToDirectory(user, dbUser);
                UpdateImagePath(dbUser);

                context.SaveChanges();

                return new Account
                {
                    Id = dbUser.Id,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName,
                    MiddleName = dbUser.MiddleName,
                    Email = dbUser.Email,
                    BirthDate = dbUser.BirthDate,
                    IsApproved = dbUser.IsApproved,
                    StatusId = dbUser.StatusId,
                    Phone = dbUser.Phone,
                    Wincoins = user.Wincoins,
                    RoleId = dbUser.RoleId,
                    CigarettesAlternative = dbUser.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = dbUser.CigarettesFavorite,
                    CigarettesFavoriteType = dbUser.CigarettesFavoriteType,
                    PromoCode = dbUser.PromoCode
                };

            }
        }

        public bool UserIsExist(int userId)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                return context.Users.Any(x => x.Id == userId);
            }
        }

        public Account UserInfo(int userId)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == userId);

                if (user == null)
                    return new Account { ErrorMessage = Resources.AccountWasNotFound };

                Account account = new Account
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Email = user.Email,
                    BirthDate = user.BirthDate,
                    IsApproved = user.IsApproved,
                    StatusId = user.StatusId,
                    Phone = user.Phone,
                    Wincoins = user.Wincoins,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType,
                    CityName = user.Cities.CityName,
                    IsActiveParty = false
                };

                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();
                Party party = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false)
                    .OrderByDescending(x => x.Id).FirstOrDefault();
                if (party != null)
                {
                    account.IsActiveParty = true;
                    PartyUsers partyUser = party.PartyUsers.FirstOrDefault(u => u.UserId == userId && u.PartyId == party.Id);
                    if (partyUser != null)
                    {
                        account.IsInvitedToParty = partyUser.IsInvited;
                        account.IsBlockedFromParty = partyUser.IsBlocked;
                        account.TicketCoins = partyUser.Points;
                    }
                }

                if (greatBattle != null)
                {
                    GreatBattlesUsers greatBattlesUser = greatBattle.GreatBattlesUsers.FirstOrDefault(u => u.UserID == userId);
                    
                    if (greatBattlesUser != null)
                    {
                        if (!greatBattlesUser.IsBlocked)
                        {
                            var firstTour = context.GreatBattleTours.Where(x => x.GreatBattleID == greatBattle.ID)
                                .OrderBy(x => x.BeginDateTime)
                                .FirstOrDefault();

                            account.GreatBattlePoints = greatBattlesUser.Points;
                            account.GreatBattleGroupId = greatBattlesUser.GroupID;
                            account.CanChangeBattleSide =
                                !(firstTour?.EndDateTime < DateTime.Now) && greatBattlesUser.CanChangeSide;
                            account.Rank = GetUserRank(greatBattle, account.GreatBattlePoints);
                        }
                    }
                }

                return account;
            }
        }

        public IsExistModel PhoneIsExist(string phone)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var user = context.Users.FirstOrDefault(x => x.Phone == phone && x.IsDeleted == false);

                if (user == null)
                {
                    return new IsExistModel() { IsExist = false};
                }
                else
                {
                    return new IsExistModel() { IsExist = true, Message = "Phone already is exist"};
                }
            }
        }

        public List<InvitedUsers> GetInvitedUsers(int userId, string appType)
        {
            using (var context = new StayTrueDBEntities())
            {
                List<DataAccessLayer.InvitedUsers> invUsers = new List<DataAccessLayer.InvitedUsers>();
                if (appType.Contains("mobile"))
                {
                    invUsers = context.InvitedUsers.Where(u => u.InvitingUserId == userId).OrderByDescending(u=>u.InvitedDate).Take(20).ToList();
                }
                else
                {
                    invUsers = context.InvitedUsers.Where(u => u.InvitingUserId == userId).OrderByDescending(u => u.InvitedDate).ToList();
                }

                if (invUsers != null)
                {
                    return invUsers.Select(x => new InvitedUsers
                    {
                        FirstName = x.Users1.FirstName,
                        LastName = x.Users1.LastName,
                        AvatarPath = x.Users1.Avatar ?? "/Images/Avatars/Default.jpg"
                    }).ToList();
                }
                return new List<InvitedUsers>();
            }
        }

        public void EditAvatar(EditAvatarModel editAvatar, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Users user = context.Users.SingleOrDefault(x => x.Id == userId);
                user.NewAvatar = SaveAvatarToDirectory(editAvatar, userId.ToString(), "/Images/Avatars/NotApproved/");
                if (File.Exists($@"{folderPath}{user.NewAvatar}"))
                {
                    user.IsAvatarApproved = false;
                }
                context.SaveChanges();
            }
        }

        public int GetUserWincoins(int userId)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            int id = userId;
            var user = context.Users.SingleOrDefault(x => x.Id == id);
            if (user != null)
                return user.Wincoins;
            return 0;
        }

        public User ChangeUserProfile(User user)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var userFromContext = context.Users.FirstOrDefault(x => x.Id == user.Id);

            if (userFromContext != null)
            {
                userFromContext.FirstName = user.FirstName;
                userFromContext.LastName = user.LastName;
                userFromContext.MiddleName = user.MiddleName;
                userFromContext.Email = user.Email;
                userFromContext.BirthDate = (DateTime)user.BirthDate;
                userFromContext.GenderId = user.GenderId;
                userFromContext.CityId = userFromContext.CityId;
                context.SaveChanges();
             
                return new User
                {
                    FirstName = userFromContext.FirstName,
                    LastName = userFromContext.LastName,
                    MiddleName = userFromContext.MiddleName,
                    Email = userFromContext.Email,
                    BirthDate = DateTime.Now,
                    GenderId = userFromContext.GenderId,
                    CityId = userFromContext.CityId,
                    RegistrationDate = userFromContext.RegistrationDate,
                    Wincoins = userFromContext.Wincoins
                };
            }
            return null;
        }

        public string ChangeUserPassword(User user)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbUser = context.Users.SingleOrDefault(x => x.Phone == user.Phone);
            if (dbUser != null)
            {
                dbUser.Password = CreateBCrypt("");
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        public GameScore AddGameResult(GameScore gameScore)
        {
            int userMaxScore = 0;
            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    var userGameSessions =
                        context.GameSession.Where(s => s.GameId == gameScore.GameId && s.UserId == gameScore.UserId).ToList();

                    if (userGameSessions.Count!=0)
                    {
                        userMaxScore = userGameSessions.Select(s => s.Score).Max();
                    }

                    if (userMaxScore >= gameScore.Points)
                    {
                        return new GameScore {ErrorMessage = "Рекорд не побит"};
                    }

                    GameSession gameSession = new GameSession();

                    gameSession.SessionStart = DateTime.Now;
                    gameSession.SessionEnd = DateTime.Now;
                    gameSession.Score = gameScore.Points;
                    gameSession.UserId = gameScore.UserId;
                    gameSession.GameId = gameScore.GameId;

                    Games game = context.Games.SingleOrDefault(x => x.Id == gameScore.GameId);
                    Users user = context.Users.SingleOrDefault(x => x.Id == gameScore.UserId);

                    int wincoinExchange = game.WincoinsExchange;
                    int maxWincoins = game.Wincoins;

                    int lastWincoinsSum = context.GameSession
                        .Where(x => x.UserId == gameScore.UserId && x.GameId == gameScore.GameId)
                        .Select(s => s.Wincoins).DefaultIfEmpty(0).Sum();

                    int newWincoinsSum = gameScore.Points / wincoinExchange;

                    int totalWincoinsSum = lastWincoinsSum + (newWincoinsSum - lastWincoinsSum);

                    if (!(lastWincoinsSum >= maxWincoins))
                    {
                        if (totalWincoinsSum >= maxWincoins)
                        {
                            gameSession.Wincoins = maxWincoins - lastWincoinsSum;
                        }
                        else
                        {
                            gameSession.Wincoins = newWincoinsSum - lastWincoinsSum;
                        }

                        user.Wincoins += gameSession.Wincoins;
                    }

                    context.GameSession.Add(gameSession);
                    context.SaveChanges();

                    Task.Run(() =>
                    {
                        var interim = new GameResultLog
                        {
                            UserId = gameScore.UserId,
                            GameId = gameScore.GameId,
                            WincoinsExchange = wincoinExchange,
                            MaxWincoins = maxWincoins,
                            SessionsWincoinsSum = lastWincoinsSum,
                            CurrentSessionWincoins = newWincoinsSum,
                            TotalWincoinsSum = totalWincoinsSum,
                            AddedWincoins = gameSession.Wincoins
                        };
                        Log.Info($"AddGameResult | Input data: {JsonConvert.SerializeObject(gameScore)}\nOutput data: {JsonConvert.SerializeObject(interim)}");
                    });
                    gameScore.Score = gameSession.Wincoins;
                    return gameScore;
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return new GameScore(){ErrorMessage = e.Message};
            }
        }

        public static User GetUser(int userId, StayTrueDBEntities context, string serviceUrl)
        {
            var dbUser = context.Users.SingleOrDefault(x => x.Id == userId);
            if (dbUser != null)
                return new User
                {
                    Id = dbUser.Id,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName,
                    MiddleName = dbUser.MiddleName,
                    Email = dbUser.Email,
                    BirthDate = dbUser.BirthDate,
                    GenderId = dbUser.GenderId,
                    CityId = dbUser.CityId,
                    Wincoins = dbUser.Wincoins,
                    StatusId = dbUser.StatusId,
                    RegistrationDate = dbUser.RegistrationDate,
                    Phone = dbUser.Phone
                   
                };
            return null;
        }

        public List<UserGameHistory> GameHistory(int userId, string appType)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            bool isMobile = appType == "mobile";
            var gameSessions = isMobile ? context.GameSession.Where(x => x.UserId == userId).Take(20).OrderByDescending(x => x.Id).ToList()
                : context.GameSession.Where(x => x.UserId == userId).OrderByDescending(x => x.Id).ToList();

            if (gameSessions.Count != 0)
            {
                return gameSessions.Select(x => new UserGameHistory
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    SessionStart = x.SessionStart,
                    GameId = x.GameId,
                    GameName = x.Games.Name,
                    ImagePath = x.Games.ImagePath,
                    Score = x.Score,
                    Wincoins = x.Wincoins
                }).ToList();
            }

            return new List<UserGameHistory>();
        }


        public List<UserPurchaseHistory> PurchaseHistory(int userId)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var purchaseHistory = context.PurchaseHistories.Where(x => x.UserId == userId).OrderByDescending(x => x.Id).ToList();

            if (purchaseHistory.Count != 0)
            {
                return purchaseHistory.Select(x => new UserPurchaseHistory
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    ProductId = x.ProductId,
                    ImagePath = x.Products.ImagePath,
                    ProductName = x.Products.NameRu,
                    Amount = x.Amount,
                    Price = x.Price,
                    CreationDate = x.CreationDate
                }).ToList();
            }

            return new List<UserPurchaseHistory>();
        }

        public GameScore UserBestScore(int gameId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.SingleOrDefault(x => x.Id == userId);

                if (user == null)
                    return new GameScore { ErrorMessage = Resources.UserNotFound };
               
                GameScore gameBestScore = context.Database.SqlQuery<GameScore>("SELECT UserId, GameId, MAX(Score) as Score FROM GameSession GROUP BY UserId, GameId ORDER BY MAX(Score) DESC").Where(g => g.GameId == gameId && g.UserId == userId).FirstOrDefault();

                if (gameBestScore == null)
                    return new GameScore { ErrorMessage = Resources.Ok };

                gameBestScore.UserFullName = user.FirstName + " " + user.LastName;

                return gameBestScore;
            }
        }

        private Product GetProduct(int id, StayTrueDBEntities context)
        {
            var dbProd = context.Products.SingleOrDefault(x => x.Id == id);
            return new Product
            {
                Id = dbProd.Id,
                NameRu = dbProd.NameRu,
                NameKg = dbProd.NameKg,
                InformationKg = dbProd.InformationKg,
                InformationRu = dbProd.InformationRu,
                Amount = dbProd.Amount,
                AvailableAmount = dbProd.AvailableAmount,
                Price = dbProd.Price,
                IsAvailable = dbProd.IsAvailable,
                ImagePath = dbProd.ImagePath,
                ShopId = dbProd.ShopId
            };
        }

      
        public string ValidateUser(User user)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbUser = context.Users.SingleOrDefault(x => x.Phone == user.Phone);
                return dbUser == null ? Resources.Ok : Resources.UserExist;
            }
        }

        public GameScore SavePartyGameResult(GameScore gameScore)
        {
            using (var context = new StayTrueDBEntities())
            {
                var party = context.Party.OrderByDescending(p => p.Id).FirstOrDefault();

                PartyGameSession gameSession = new PartyGameSession();

                gameSession.SessionStart = DateTime.Now;
                gameSession.SessionEnd = DateTime.Now;
                gameSession.Score = gameScore.Points;
                gameSession.UserId = gameScore.UserId;
                gameSession.GameId = gameScore.GameId;

                PartyUsers user = context.PartyUsers.FirstOrDefault(u => u.UserId == gameScore.UserId && u.PartyId == party.Id && !u.IsBlocked);

                user.Points += gameScore.Points;              
                
                context.PartyGameSession.Add(gameSession);
                context.SaveChanges();

                return gameScore;
            }
        }

        public string AuthorizationAdmin(AdminAccount account)
        {
            using (StayTrueDBEntities context = new StayTrueDBEntities())
            {
                var admin = context.Administrators.SingleOrDefault(x => x.Login == account.Login);
                if (admin != null)
                {
                    var authorize = ValidateBCryptePassword(account.Password, admin.Password);
                    if (authorize)
                    {
                        return Resources.Ok;
                    }
                    return Resources.IncorrectLoginOrPassword;
                }
                return Resources.UserNotFound;
            }
        }

        public Account RegisterAdmin(Admin admin)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbAdmin = new Administrators
            {
                Login = admin.Login,
                Password = CreateBCrypt(admin.Password),
                FirstName = admin.FirstName,
                LastName = admin.LastName,
                PhoneNumber = admin.PhoneNumber,
                Email = admin.Email,
                BirthDate = (DateTime)admin.Birthdate,
                Role = (int)admin.Role
            };
            context.Administrators.Add(dbAdmin);
            context.SaveChanges();
            return new Account
            {
                BirthDate = dbAdmin.BirthDate,
                Email = dbAdmin.Email,
                FirstName = dbAdmin.FirstName,
                LastName = dbAdmin.LastName,
                Phone = dbAdmin.PhoneNumber,
                RoleId = (int)RoleEnum.Admin,
                StatusId = (int)StatusEnum.Active
            };
        }

        public Admin ChangeAdminProfile(Admin admin)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var adminFromContext = context.Administrators.FirstOrDefault(x => x.Id == admin.Id);

            if (adminFromContext != null)
            {
                adminFromContext.FirstName = admin.FirstName;
                adminFromContext.LastName = admin.LastName;
                adminFromContext.PhoneNumber = admin.PhoneNumber;
                adminFromContext.Email = admin.Email;
                adminFromContext.BirthDate = (DateTime)admin.Birthdate;
                adminFromContext.Role = (int)RoleEnum.Admin;
                context.SaveChanges();
                return new Admin
                {
                    FirstName = adminFromContext.FirstName,
                    LastName = adminFromContext.LastName,
                    PhoneNumber = adminFromContext.PhoneNumber,
                    Email = adminFromContext.Email,
                    Birthdate = adminFromContext.BirthDate,
                    Role = RoleEnum.Admin
                };
            }
            return null;
        }

        public string ChangeAdminPassword(Admin admin)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var adminFromContext = context.Administrators.FirstOrDefault(x => x.Login == admin.Login);
            if (adminFromContext != null)
            {
                adminFromContext.Password = CreateBCrypt(admin.Password);
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        private Game GetGame(int gameId, StayTrueDBEntities context)
        {
            var dbGame = context.Games.SingleOrDefault(x => x.Id == gameId);

            if (dbGame != null)
                return new Game
                {
                    Id = dbGame.Id,
                    Name = dbGame.Name,
                    Image = dbGame.ImagePath,
                    GameUrl = dbGame.GameUrl
                };
            return null;
        }

        public List<GameScore> GetBestScores(int userId, int gameId, int skip, int take)
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<GameSession> gameSession = context.GameSession.OrderByDescending(x => x.Id);

                if (gameId > 0)
                    gameSession = gameSession.Where(x => x.GameId == gameId);

                if (skip > 0)
                    gameSession = gameSession.Skip(skip);

                if (take > 0)
                    gameSession = gameSession.Take(take);
                
                List<GameScore> gamesScores = gameSession.ToList().Select(g => new GameScore { UserFullName = g.Users.FirstName + " " + g.Users.LastName, GameId = g.GameId, Points = g.Score, UserId = g.UserId}).ToList();
                
                List<GameScore> groupedGamesScores = gamesScores.GroupBy(gs => new {gs.UserId, gs.GameId, gs.UserFullName}).Select(gs =>
                    new GameScore
                    {
                        GameId = gs.Key.GameId,
                        UserId = gs.Key.UserId,
                        UserFullName = gs.Key.UserFullName,
                        Score = gs.Max(m => m.Points)
                    }).Take(20).OrderByDescending(x => x.Score).ToList();

                if (groupedGamesScores.Any(g => g.UserId == userId))
                {
                    return groupedGamesScores;
                }
                else
                {
                    GameScore userGameScore = context.GameSession.Where(g => g.UserId == userId && g.GameId == gameId).Select(g => new GameScore { UserFullName = g.Users.FirstName + " " + g.Users.LastName, GameId = g.GameId, Points = g.Score, UserId = g.UserId } ).FirstOrDefault();

                    if (userGameScore == null)
                    {
                        return groupedGamesScores;
                    }
                    else
                    {
                        if (groupedGamesScores.Count >= 20)
                        {
                            groupedGamesScores.RemoveAt(groupedGamesScores.Count - 1);
                        }
                        
                        groupedGamesScores.Add(userGameScore);
                    }
                }
                
                return groupedGamesScores;
            }
        }

        public string BlockUser(BlockUser user)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var userDb = context.Users.SingleOrDefault(x => x.Id == user.UserId);
                        var blockUser = new BlockedUsers
                        {
                            BlockDate = DateTime.Now,
                            Reason = user.Reason,
                            UnblockDate = user.UnblockDate,
                            UserId = user.UserId
                        };
                        context.BlockedUsers.Add(blockUser);
                        userDb.StatusId = (int)StatusEnum.Blocked;
                        context.SaveChanges();
                        dbTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        dbTransaction.Rollback();
                        return null;
                    }

                }
            }
            return Resources.Ok;
        }

        public string ChangeUserStatus(List<User> users, int status)
        {
            using (var context = new StayTrueDBEntities())
            {
                foreach (var user in users)
                {
                    var dbUser = context.Users.SingleOrDefault(x => x.Id == user.Id);
                    if (dbUser != null) dbUser.StatusId = status;
                    context.SaveChanges();
                }
                return Resources.Ok;
            }
        }



        private string GeneratePassword()
        {
            bool includeLowercase = true;
            bool includeUppercase = true;
            bool includeNumeric = true;
            bool includeSpecial = false;
            bool includeSpaces = false;
            int lengthOfPassword = 8;

            string password = PasswordGenerator.GeneratePassword(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);

            while (!PasswordGenerator.PasswordIsValid(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, password))
            {
                password = PasswordGenerator.GeneratePassword(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);
            }
            return password;
        }

        private static string CreateBCrypt(string password)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            string hash = BCrypt.Net.BCrypt.HashPassword(password, salt);
            return hash;
        }

        private static bool ValidateBCryptePassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }

        private static string CreateMd5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        private byte[] GetImageBytes(string base64)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            return Convert.FromBase64String(base64.Substring(base64.IndexOf(',') + 1));
        }

        async private void UserChatAuthorizeAsync(string token)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(5);
  
                client.DefaultRequestHeaders.Add("Authorization", String.Format("Basic {0}", Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes($"{ChatLogin}:{ChatPassword}"))));

                await client.GetStringAsync(ChatUrl + "admin/authUser?token=" + token);

            }
        }


        private Account UnblockUser(int userId, string application)
        {
            using (var context = new StayTrueDBEntities())
            {
                var brand = context.Configurations.SingleOrDefault(x => x.Name.Equals(ConfigurationValues.Brand));
                var isBrand = brand != null && Convert.ToBoolean(brand.Value);
                var blockedUser = context.BlockedUsers.SingleOrDefault(x => x.UserId == userId);
                var user = context.Users.SingleOrDefault(x => x.Id == userId);
                context.BlockedUsers.Remove(blockedUser ?? throw new InvalidOperationException());
                user.StatusId = (int)StatusEnum.Active;
                context.SaveChanges();

                #region Online counter

                switch (application)
                {
                    case "web":
                        user.SignInCounterP += 1;
                        user.LastSignInP = DateTime.Now;
                        break;
                    case "mobile":
                        user.SignInCounterM += 1;
                        user.LastSignInM = DateTime.Now;
                        break;
                }
                context.SaveChanges();

                #endregion

                return new Account
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Email = user.Email,
                    BirthDate = DateTime.Now,
                    IsApproved = user.IsApproved,
                    StatusId = user.StatusId,
                    Phone = user.Phone,
                    RoleId = user.RoleId,
                    CigarettesAlternative = user.CigarettesAlternative,
                    CigarettesAlternativeType = user.CigarettesAlternativeType,
                    CigarettesFavorite = user.CigarettesFavorite,
                    CigarettesFavoriteType = user.CigarettesFavoriteType
                };
            }
        }

        private Users UpdateImagePath(Users user)
        {
            user.PassportBack = @"/Images/Passports/Back/" + user.Id + ".jpg";
            user.PassportFront = @"/Images/Passports/Front/" + user.Id + ".jpg";

            return user;
        }

        private Users CreateNewUser(User user)
        {
            var newUser = new Users
            {
                Phone = user.Phone,
                Password = null, // Пароль создается при отправки смс сообщения
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = user.MiddleName,
                BirthDate = user.BirthDate,
                CityId = user.CityId,
                GenderId = user.GenderId,
                Email = user.Email,
                Wincoins = 0,
                RoleId = 2, //dbo.Roles
                RegistrationDate = DateTime.Now,
                StatusId = (int)StatusEnum.NotActive,
                IsApproved = false,
                IsAvatarApproved = true,
                PromoCode = user.Promocode, 
                PassportBack = "",
                PassportFront = "",
                CigarettesAlternative = user.CigarettesAlternative,
                CigarettesAlternativeType = user.CigarettesAlternativeType,
                CigarettesFavorite = user.CigarettesFavorite,
                CigarettesFavoriteType = user.CigarettesFavoriteType                
            };

            return newUser;
        }

        private void SavePassportToDirectory(User user, Users dbUser)
        {
            string appPath = $@"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}";

            string pathFront = appPath + @"Images\Passports\Front\";
            string pathBack = appPath + @"Images\Passports\Back\";

            byte[] passportFrontImg = Convert.FromBase64String(ImageHelper.ParseBase64Value(user.PassportFront));
            byte[] passportBackImg = Convert.FromBase64String(ImageHelper.ParseBase64Value(user.PassportBack));

            File.WriteAllBytes(pathFront + dbUser.Id + ".jpg", passportFrontImg);
            File.WriteAllBytes(pathBack + dbUser.Id + ".jpg", passportBackImg);
        }


        private string SaveAvatarToDirectory(EditAvatarModel editAvatarModel, string userId, string directory = "/Images/Avatars/")
        {
            string avatarImgPath = @directory + userId + ".jpg";

            var filePath = $@"{folderPath}{avatarImgPath}";
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }

            byte[] avatarImg = Convert.FromBase64String(ImageHelper.ParseBase64Value(editAvatarModel.Avatar));

            File.WriteAllBytes(filePath, avatarImg);

            return avatarImgPath;
        }

        private string SaveSmallAvatar(int userId, string fileName)
        {
            var image = Image.FromFile($@"{folderPath}{fileName}");

            if (image.Equals(null)) return null;

            var newImage = ScaleImage(image);

            var filePath = $@"{folderPath}{fileName.Replace("Avatars", "SmallAvatars")}";

            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            image.Dispose();

            string smallImagePath = fileName.Replace("Avatars", "SmallAvatars");

            Bitmap clone = new Bitmap(newImage);
            newImage.Dispose();

            clone.Save(filePath);
            clone.Dispose();


            return smallImagePath;
        }

        private int GetUserRank(GreatBattles greatBattles, int userPoints)
        {
            if (userPoints > greatBattles.Rank5Limit)
                return 5;
            if (userPoints > greatBattles.Rank4Limit)
                return 4;
            if (userPoints > greatBattles.Rank3Limit)
                return 3;
            if (userPoints > greatBattles.Rank2Limit)
                return 2;
            if (userPoints > greatBattles.Rank1Limit)
                return 1;
            return 0;
        }
        private static Image ScaleImage(Image image)
        {

            var newImage = new Bitmap(ImageHeight * image.Width / image.Height, ImageHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, ImageHeight * image.Width / image.Height, ImageHeight);
                graphics.Dispose();
            }

            return newImage;
        }

        private void CheckUserFirstSignIn(Users user)
        {
            if (!user.LastSignInM.HasValue)
            {
                int operatorId = BallancePaymentService.GetServiceId(user.Phone);
                if (operatorId != 0)
                {
                    using (var context = new StayTrueDBEntities())
                    {
                        try
                        {
                            var ballanceUpHistory =
                                context.UserBallanceUpHistory.FirstOrDefaultAsync(b => b.UserId == user.Id);
                            //                    string userReceiptId = BallancePaymentService.UpUserBallance(user.Phone, int.Parse(FirstSignInGiftAmount), operatorId);
                            if (ballanceUpHistory.Result == null)
                            {
                                UserBallanceUpHistory history = new UserBallanceUpHistory()
                                {
                                    UserId = user.Id,
                                    Date = DateTime.Now
                                };
                                context.UserBallanceUpHistory.Add(history);
                                context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }
                    }
                }
            }
        }
    }
}