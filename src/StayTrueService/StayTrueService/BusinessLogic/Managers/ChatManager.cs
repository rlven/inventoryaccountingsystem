﻿using System.Collections.Generic;
using System.Linq;
using Common.Models;
using DataAccessLayer;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class ChatManager
    {
        internal List<ChatGroup> GetChatGroupsByUserId(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                IOrderedQueryable<ChatGroups> chatGroups = context.ChatGroups.Where(x => x.IsDeleted == false && x.IsActive && x.IsPrivate == false).OrderByDescending(x => x.Id);

                List<ChatGroup> userChats = chatGroups.ToList().Select(x => new ChatGroup
                {
                    Id = x.Id,
                    Name = x.Name,
                    Image = x.Image,
                    IsPrivate = false

                }).ToList();

                IOrderedQueryable<ChatGroups> privateChatGroups = context.ChatGroups
                    .Where(x => x.IsDeleted == false && x.IsActive && x.IsPrivate && x.ChatPrivateGroupsUsers.Any(u => u.UserId == userId) ).OrderByDescending(x => x.Id);

                userChats.AddRange(privateChatGroups.ToList().Select(x => new ChatGroup
                {
                    Id = x.Id,
                    Name = x.Name,
                    Image = x.Image,
                    IsPrivate = true

                }).ToList()); 

                return userChats;
            }
        }
    }
}