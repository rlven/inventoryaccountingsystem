﻿using DataAccessLayer;
using System.Collections.Generic;
using System.Linq;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class GenderManager
    {
        public List<Genders> GetGenders()
        {
            using (var context = new StayTrueDBEntities())
            {
                return context.Genders.Select(x => new Genders { Id = x.Id ,Name = x.Name } ).ToList();
            }
        }
    }
}