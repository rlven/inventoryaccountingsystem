﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;

namespace BusinessLogicLayer.Managers
{
    internal static class ConvertFileHelper
    {
        private static readonly string ImageFolderName = @"/Image/Images/";
        private static readonly string VideoFolderName = @"/Image/Videos/";
        private static readonly string GalleryVideoFolderName = @"/Image/Gallery/Videos/";
        private static readonly string GalleryFolderName = @"/Image/Gallery/";
        private static readonly string AlbumImagesFolderName = @"/Image/Album/";
        private static readonly string ProductImagesFolderName = @"/Image/Product/";
        private static readonly string BannerImagesFolderName = @"/Image/Banner/";
        private static readonly string GamePreviewFolderName = @"/Image/GamePreview/";
        private static readonly string GameFolderName = @"/Image/Games/";
        private static readonly string AvatarFolderName = @"/Image/Avatars/";
        private static readonly string PassportFolderName = @"/Image/Passports/";

        private static readonly int ImageHeight = 360;

        public static string SaveImage(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{ImageFolderName}{fileName}.jpg";
            if (!Directory.Exists($"{folderPath}{ImageFolderName}"))
                Directory.CreateDirectory($"{folderPath}{ImageFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"{ImageFolderName}{fileName}.jpg";
            return fileUrl;
        }

        public static string SaveGame(string folderPath, byte[] file, string filePath, string fileName)
        {
            if (file == null || file.Length == 0) return null;
            if (!Directory.Exists($"{folderPath}{GamePreviewFolderName}"))
                Directory.CreateDirectory($"{folderPath}{GamePreviewFolderName}");
            ZipFile.ExtractToDirectory(filePath, GameFolderName);
            return $"/{GameFolderName}/{fileName}";
        }

        public static string SaveGamePreview(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{GamePreviewFolderName}{fileName}.jpg";
            if (!Directory.Exists($"{folderPath}{GamePreviewFolderName}"))
                Directory.CreateDirectory($"{folderPath}{GamePreviewFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"/{GamePreviewFolderName}{fileName}.jpg";
            return fileUrl;
        }

        public static string SaveAvatar(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{AvatarFolderName}{fileName}/avatar.jpg";
            if (!Directory.Exists($"{folderPath}{AvatarFolderName}{fileName}"))
                Directory.CreateDirectory($"{folderPath}{AvatarFolderName}{fileName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"{AvatarFolderName}{fileName}/avatar.jpg";
            return fileUrl;
        }

        public static string SavePassportFront( string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{PassportFolderName}{fileName}/passportFront.jpg";
            if (!Directory.Exists($"{folderPath}{PassportFolderName}{fileName}"))
                Directory.CreateDirectory($"{folderPath}{PassportFolderName}{fileName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"{fileName}/passportFront.jpg";
            return fileUrl;
        }

        public static string SavePassportBack(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{PassportFolderName}{fileName}/passportBack.jpg";
            if (!Directory.Exists($"{folderPath}{PassportFolderName}{fileName}"))
                Directory.CreateDirectory($"{folderPath}{PassportFolderName}{fileName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"{fileName}/passportBack.jpg";
            return fileUrl;
        }

        public static string SaveImageInAlbum(string folderPath, string base64, int fileName, int categoryId)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $@"{folderPath}{GalleryFolderName}{categoryId}/{fileName}.jpg";
            if (!Directory.Exists($"{folderPath}{GalleryFolderName}{categoryId}"))
                Directory.CreateDirectory($@"{folderPath}{GalleryFolderName}{categoryId}/");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $@"{GalleryFolderName}{categoryId}/{fileName}.jpg";
            return fileUrl;
        }
        public static string SaveProductImage(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{ProductImagesFolderName}{fileName}.png";
            if (!Directory.Exists($"{folderPath}{ProductImagesFolderName}"))
                Directory.CreateDirectory($"{folderPath}{ProductImagesFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"{ProductImagesFolderName}{fileName}.png";
            return fileUrl;
        }

        public static string SaveBannerImage(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{BannerImagesFolderName}{fileName}.jpg";
            if (!Directory.Exists($"{folderPath}{BannerImagesFolderName}"))
                Directory.CreateDirectory($"{folderPath}{BannerImagesFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"{BannerImagesFolderName}{fileName}.jpg";
            return fileUrl;
        }


        public static string SaveVideo(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{VideoFolderName}{fileName}.mp4";
            if (!Directory.Exists($"{folderPath}{VideoFolderName}"))
                Directory.CreateDirectory($"{folderPath}{VideoFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"/{VideoFolderName}{fileName}.mp4";
            return fileUrl;
        }

        public static string SaveGalleryVideo(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $"{folderPath}{GalleryVideoFolderName}{fileName}.mp4";
            if (!Directory.Exists($"{folderPath}{GalleryVideoFolderName}"))
                Directory.CreateDirectory($"{folderPath}{GalleryVideoFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $"/{GalleryVideoFolderName}{fileName}.mp4";
            return fileUrl;
        }

        public static string SaveImageInAlbum(string folderPath, byte[] image, int fileName, int categoryId)
        {
            if (image == null || image.Length == 0) return null;
            var filePath = $@"{folderPath}{GalleryFolderName}{categoryId}/{fileName}.jpg";
            if (!Directory.Exists($"{folderPath}{GalleryFolderName}{categoryId}"))
                Directory.CreateDirectory($@"{folderPath}{GalleryFolderName}{categoryId}/");
            File.WriteAllBytes(filePath, image);
            var fileUrl = $@"{GalleryFolderName}{categoryId}/{fileName}.jpg";
            return fileUrl;
        }

        public static string SaveSmallImageInAlbum(string folderPath,  int fileName,
            int categoryId)
        {
            var image = Image.FromFile($@"{folderPath}{GalleryFolderName}{categoryId}/{fileName}.jpg");
            if (image.Equals(null)) return null;
            var newImage = ScaleImage(image);
            var filePath = $@"{folderPath}{GalleryFolderName}{categoryId}/{fileName}small.jpg";
            if (!Directory.Exists($"{folderPath}{GalleryFolderName}{categoryId}"))
                Directory.CreateDirectory($@"{folderPath}{GalleryFolderName}{categoryId}/");
            newImage.Save(filePath, ImageFormat.Jpeg);
            var fileUrl = $@"/{GalleryFolderName}{categoryId}/{fileName}small.jpg";
            return fileUrl;
        }

        public static Image ScaleImage(Image image)
        {

            var newImage = new Bitmap(ImageHeight * image.Width / image.Height, ImageHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, ImageHeight * image.Width / image.Height, ImageHeight);           
            }
            return newImage;
        }

        public static string SaveAlbumImage(string folderPath, string base64, string fileName)
        {
            if (string.IsNullOrEmpty(base64)) return null;
            var filePath = $@"{folderPath}{AlbumImagesFolderName}{fileName}.jpg";
            if (!Directory.Exists($@"{folderPath}{AlbumImagesFolderName}"))
                Directory.CreateDirectory($@"{folderPath}{AlbumImagesFolderName}");
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
            var fileUrl = $@"{AlbumImagesFolderName}{fileName}.jpg";
            return fileUrl;
        }

        public static byte[] GetImageBytes(string base64)
        {
            return Convert.FromBase64String(base64.Substring(base64.IndexOf(',') + 1));
        }

        public static string SaveImageP(string folderPath, byte[] image, string fileName)
        {
            if (image == null || image.Length == 0) return null;
            var filePath = $"{folderPath}{ImageFolderName}{fileName}p.jpg";
            if (!Directory.Exists($"{folderPath}{ImageFolderName}"))
                Directory.CreateDirectory($"{folderPath}{ImageFolderName}");
            File.WriteAllBytes(filePath, image);
            var fileUrl = $"{ImageFolderName}{fileName}p.jpg";
            return fileUrl;
        }

        public static string SaveImageM(string folderPath, byte[] image, string fileName)
        {
            if (image == null || image.Length == 0) return null;
            var filePath = $"{folderPath}{ImageFolderName}{fileName}m.jpg";
            if (!Directory.Exists($"{folderPath}{ImageFolderName}"))
                Directory.CreateDirectory($"{folderPath}{ImageFolderName}");
            File.WriteAllBytes(filePath, image);
            var fileUrl = $"{ImageFolderName}{fileName}m.jpg";
            return fileUrl;
        }

        public static string SaveVideoP(string folderPath, byte[] videoP, string fileName)
        {
            if (videoP == null || videoP.Length == 0) return null;
            var filePath = $"{folderPath}{VideoFolderName}{fileName}p.mp4";
            if (!Directory.Exists($"{folderPath}{VideoFolderName}"))
                Directory.CreateDirectory($"{folderPath}{VideoFolderName}");
            File.WriteAllBytes(filePath, videoP);
            var fileUrl = $"/{VideoFolderName}{fileName}p.mp4";
            return fileUrl;
        }

        public static string SaveVideoM(string folderPath, byte[] videoM, string fileName)
        {
            if (videoM == null || videoM.Length == 0) return null;
            var filePath = $"{folderPath}{VideoFolderName}{fileName}m.mp4";
            if (!Directory.Exists($"{folderPath}{VideoFolderName}"))
                Directory.CreateDirectory($"{folderPath}{VideoFolderName}");
            File.WriteAllBytes(filePath, videoM);
            var fileUrl = $"/{VideoFolderName}{fileName}m.mp4";
            return fileUrl;
        }
        #region Converters

        private static byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image image = Image.FromStream(ms);
            return new Bitmap(image, new Size(ImageHeight * image.Width / image.Height, ImageHeight));
        }

        #endregion
    }
}
