﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Models;
using DataAccessLayer;
using Unity.Interception.Utilities;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class GameManager
    {
        private static readonly int GamesCountOnPage = 15;

        public List<Game> GetGames()
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<Games> games = context.Games.Where(x => x.IsActive == true && x.IsDeleted == false && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);

                return games.ToList().Select(x => new Game
                {
                    Id = x.Id,
                    Name = x.Name,
                    GameUrl = x.GameUrl,
                    Image = x.ImagePath,
                    Points = (int)x.Wincoins
                }).ToList();
            }
        }

        private string GetUrl(string folderUrl, string imageUrl)
        {
            return string.IsNullOrEmpty(imageUrl) ? null : $"{folderUrl}{imageUrl}";
        }

        public int GetGamesPagesCount()
        {
            var context = new StayTrueDBEntities();
            return (context.Games.Count() + GamesCountOnPage - 1)/GamesCountOnPage;
        }

        ///Pvp Games

        public List<Game> GetPvpGames()
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<PvpGames> games = context.PvpGames.Where(x => x.IsActive == true && x.IsDelete == false && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);

                return games.ToList().Select(x => new Game()
                {
                    Id = x.Id,
                    Name = x.Name,
                    GameUrl = x.GameUrl,
                    Image = x.ImagePath
                }).ToList();
            }
        }

        public int GetPvpGamesPagesCount()
        {
            var context = new StayTrueDBEntities();
            return (context.PvpGames.Count() + GamesCountOnPage - 1) / GamesCountOnPage;
        }

        public PvpGame PvpGame(int gameId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var game = context.PvpGames.FirstOrDefault(g => g.GameId == gameId && g.IsActive && !g.IsDelete);
                if (game == null)
                    return new PvpGame {ErrorMessage = Resources.GameNotFound};
                return new PvpGame
                {
                    GameId = game.GameId ?? 0,
                    Bets = game.PvpGameBets.Select(b => b.Bet).ToArray(),
                    IncludeBots = game.IncludeBots,
                    MatchTime = game.MatchTime,
                    PlayerQty = game.PlayerQty
                };
            }
        }

        public void PvpSessionTransferWincoins(List<PvpGameSessionScore> scores)
        {
            using (var context = new StayTrueDBEntities())
            {
                if(scores.Select(s => s.Bet).Distinct().Count() != 1)
                    return;
                var sessions = scores.Select(s => new PvpGameSession
                {
                    GameId = s.GameId,
                    RoomId = s.RoomId,
                    Kills = s.Kills,
                    Death = s.Death,
                    Score = s.Score,
                    SessionEnd = !string.IsNullOrEmpty(s.SessionEnd) ? DateTime.Parse(s.SessionEnd) : (DateTime?)null,
                    SessionStart = !string.IsNullOrEmpty(s.SessionStart) ? DateTime.Parse(s.SessionStart) : (DateTime?)null,
                    UserId = s.UserId == -1 ? null : s.UserId,
                    IsBot = s.UserId == -1 ? true : false,
                    Bet = s.Bet
                }).ToList();
                var winnerStat = sessions.OrderByDescending(s => s.Score).FirstOrDefault();

               var winners = sessions.Where(s => s.Score == winnerStat.Score && !s.IsBot).Select(s => s.UserId).ToArray();
                if (winners.Any())
                {
                    var game = context.PvpGames.FirstOrDefault(g => g.GameId == winnerStat.GameId && g.IsActive && !g.IsDelete);
                    var playersQty = game?.PlayerQty ?? 1;

                    sessions.Where(s => winners.Contains(s.UserId)).ForEach(s => s.Wincoins = winnerStat.Bet * playersQty / winners.Count());
                    context.Users.Where(u => winners.Contains(u.Id)).ForEach(u => u.Wincoins += winnerStat.Bet * playersQty / winners.Count());
                }
                context.PvpGameSession.AddRange(sessions);
                context.SaveChanges();
            }
        }

        public void PvpSessionWithdrawWincoins(List<PvpGameSessionScore> scores)
        {
            using (var context = new StayTrueDBEntities())
            {
                var session = scores.Select(s => new PvpGameSession
                {
                    GameId = s.GameId,
                    RoomId = s.RoomId,
                    SessionStart = !string.IsNullOrEmpty(s.SessionStart) ? DateTime.Parse(s.SessionStart) : (DateTime?)null,
                    UserId = s.UserId == -1 ? null : s.UserId,
                    Bet = s.Bet,
                    Wincoins = -s.Bet
                });
                var bet = session.FirstOrDefault().Bet;
                var ids = scores.Select(s => s.UserId).ToArray();
                context.Users.Where(u=>ids.Contains(u.Id)).ForEach(u=>u.Wincoins -= bet);
                context.PvpGameSession.AddRange(session);
                context.SaveChanges();
            }
        }
    }
}
