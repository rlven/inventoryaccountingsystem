﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BusinessLogicLayer.Managers
{
    public class AsyncWebRequestSender
    {

        public async Task<T> PostFormDataAsync<T>(string url, Dictionary<string, string> values) where T : class, new()
        {
            using (var client = new HttpClient { BaseAddress = new Uri(url) })
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var content = new FormUrlEncodedContent(values);

                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                content.Headers.Add("api_key", "8C3F01F46D061948C8E1BC6811D3043E4D41ED6640664C744A4760CECD1638AF");

                try
                {
                    var response = await client.PostAsync(url, content).ConfigureAwait(false);
                    return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message, e);
                }
            }
        }
    }
}
