﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Timers;
using Common.Models;
using DataAccessLayer;

using System.Configuration;

using GreatBattleQuestionnaire = DataAccessLayer.GreatBattleQuestionnaire;
using GreatBattleQuestionnaireAnswer = DataAccessLayer.GreatBattleQuestionnaireAnswer;
using GreatBattleTour = Common.Models.GreatBattleTour;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class GreatBattleManager
    {

        internal void CheckBattleExpirationDate()
        {
            using (var context = new StayTrueDBEntities())
            {

                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false && x.IsFinished && !x.IsResulted).OrderByDescending(x => x.ID).FirstOrDefault();


                if (greatBattle != null)
                {
                    int finishedTours = greatBattle.GreatBattleTours.Count(t => t.IsFinished);
                    int totalTours = greatBattle.GreatBattleTours.Count();

                    if (finishedTours >= totalTours && totalTours != 0)
                    {
                        int blueSidePoints=0;
                        int redSidePoints=0;

                        foreach (var tour in greatBattle.GreatBattleTours)
                        {
                            blueSidePoints += tour.GreatBattleTourPoints.Where(p => p.GroupID == 1).Sum(p => p.PointsCount);
                            redSidePoints += tour.GreatBattleTourPoints.Where(p => p.GroupID == 2).Sum(p => p.PointsCount);
                        }


                        int winnerSide = blueSidePoints > redSidePoints ? 1 : 2;
                        int loserSide = blueSidePoints < redSidePoints ? 1 : 2;

                        int winnerSidePoints = winnerSide == 1 ? blueSidePoints : redSidePoints;
                        int loserSidePoints = loserSide == 1 ? blueSidePoints : redSidePoints;

                        greatBattle.WinnerGroupId = winnerSide; 


                        int winnerSideUserCount = greatBattle.GreatBattlesUsers.Count(u => u.GroupID == winnerSide);
                        int looserSideUserCount = greatBattle.GreatBattlesUsers.Count(u => u.GroupID == loserSide);

                        int totalWincoinsToWinnerSide = (winnerSidePoints * 2 / winnerSideUserCount);
                        int totalWincoinsToLoserSide = loserSidePoints / looserSideUserCount;

                        double convertedCoinsToWinner = Convert.ToDouble(totalWincoinsToWinnerSide) * greatBattle.PointCoefficient;
                        double convertedCoinsToLoser = Convert.ToDouble(totalWincoinsToLoserSide) * greatBattle.PointCoefficient;
                        int wincoinsToWinner = Convert.ToInt32(convertedCoinsToWinner);
                        int wincoinsToLoser = Convert.ToInt32(convertedCoinsToLoser);
                        
                        foreach (GreatBattlesUsers user in greatBattle.GreatBattlesUsers.Where(u=>!u.IsBlocked))
                        {
                            Users users = context.Users.FirstOrDefault(u => u.Id == user.UserID);

                            if (users != null)
                            {
                                users.Wincoins += user.GroupID == winnerSide ? wincoinsToWinner : wincoinsToLoser;
                                users.Wincoins += Convert.ToInt32(user.Points * greatBattle.PointCoefficient);
                            }
                            user.Points = 0;
                        }
                    }

                    greatBattle.IsResulted = true;
                    context.SaveChanges();
                }
            }
        }

        internal GreatBattleUserRegistrationAnswer RegisterUser(GreatBattleUserRegistrationAnswer registerUser)
        {
            using (var context = new StayTrueDBEntities())
            {
                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();

                if (greatBattle == null)
                {
                    return new GreatBattleUserRegistrationAnswer { LocalizedMessage = "Нет активной битвы" };
                }

                if (greatBattle.GreatBattlesUsers.Any(x => x.UserID == registerUser.UserId))
                {
                    return new GreatBattleUserRegistrationAnswer { LocalizedMessage = "Пользователь уже зарегистрирован" };
                }

                var greatBattleUsers = greatBattle.GreatBattlesUsers;

                if (greatBattle.autofillPlayer && greatBattleUsers.Count!=0)
                {
                    var blueCount = greatBattleUsers.Count(u => u.GroupID == 1);

                    var redCount = greatBattleUsers.Count(u => u.GroupID == 2);

                    registerUser.GroupId = blueCount >= redCount ? 2 : 1;
                }

                GreatBattleQuestionnaireAnswer answer = new GreatBattleQuestionnaireAnswer
                {
                    QuestionnaireId = registerUser.QuestionnaireAnswer.QuestionnaireId,
                    UserId = registerUser.UserId,
                    Answer1 = registerUser.QuestionnaireAnswer.Answer1,
                    Answer2 = registerUser.QuestionnaireAnswer.Answer2,
                    Answer3 = registerUser.QuestionnaireAnswer.Answer3,
                    Answer4 = registerUser.QuestionnaireAnswer.Answer4,
                    CreationDate = DateTime.Now
                };
                
                context.GreatBattleQuestionnaireAnswer.Add(answer);

                GreatBattlesUsers greatBattlesUser = new GreatBattlesUsers
                {
                    GreatBattleID = greatBattle.ID,
                    GroupID = registerUser.UserId,
                    CreationDate = DateTime.Now,
                    Score = 0
                };

                greatBattlesUser.GroupID = registerUser.GroupId;
                greatBattlesUser.UserID = registerUser.UserId;
                greatBattlesUser.CanChangeSide = true;

                context.GreatBattlesUsers.Add(greatBattlesUser);

                AddUserToGreatBattleChat(registerUser.UserId, registerUser.GroupId);
                
                context.SaveChanges();
            }

            return registerUser;
        }

        internal GreatBattleUserRegistrationInfo GetUserRegistrationInfo(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();


                if (greatBattle == null)
                {
                    return new GreatBattleUserRegistrationInfo { LocalizedMessage = "Нет активной битвы", IsAutofilled = false};
                }

                if (greatBattle.IsFinished)
                {
                    return new GreatBattleUserRegistrationInfo{ BattleIsFinished = true, WinnerGroupId = greatBattle.WinnerGroupId, IsAutofilled = false};
                }

                GreatBattleUserRegistrationInfo battle = new GreatBattleUserRegistrationInfo();

                GreatBattlesUsers battlesUsers = greatBattle.GreatBattlesUsers.FirstOrDefault(u => u.UserID == userId);

                if (battlesUsers != null)
                {
                    battle.IsRegistered = true;
                    battle.IsBlocked = battlesUsers.IsBlocked;
                    battle.UserGroupId = battlesUsers.GroupID;
                }
                
                GreatBattleQuestionnaire battleQuestionnaire = greatBattle.GreatBattleQuestionnaire.FirstOrDefault();

                if (battleQuestionnaire == null)
                {
                    return new GreatBattleUserRegistrationInfo { LocalizedMessage = "Нет опросника для битвы" };
                }

                battle.Questionnaire = new Common.Models.GreatBattleQuestionnaire
                {
                    Id = battleQuestionnaire.ID,
                    Question1 = battleQuestionnaire.Question1,
                    Question2 = battleQuestionnaire.Question2,
                    Question3 = battleQuestionnaire.Question3,
                    Question4 = battleQuestionnaire.Question4
                };

                GreatBattleTours tourFirst = greatBattle.GreatBattleTours.FirstOrDefault();
                GreatBattleTours tourLast = greatBattle.GreatBattleTours.OrderByDescending(t =>t.ID).FirstOrDefault();

                if (tourFirst == null && tourLast == null)
                {
                    return new GreatBattleUserRegistrationInfo { LocalizedMessage = "Нет туров для битвы" };
                }

                battle.BattleBeginDate = tourFirst.BeginDateTime;
                battle.BattleEndDate = tourLast.EndDateTime;
                TimeSpan timeLeft = tourLast.EndDateTime - DateTime.Now;
                TimeSpan timeStart = tourFirst.BeginDateTime - DateTime.Now;
                battle.BattleEndLeft = timeLeft.TotalSeconds+5;//reserve 5 seconds
                battle.BattleStartLeft = timeStart.TotalSeconds+5;

                battle.BlueSidePoints = greatBattle.GreatBattleTours.Sum(t => t.GreatBattleTourPoints.Where(p => p.GroupID == 1).Sum(p => p.PointsCount));
                battle.RedSidePoints = greatBattle.GreatBattleTours.Sum(t => t.GreatBattleTourPoints.Where(p => p.GroupID == 2).Sum(p => p.PointsCount));
                battle.IsAutofilled = greatBattle.autofillPlayer;
                
                return battle;
            }
        }

        internal GreatBattleDetails GetGreatBattleDetails(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {

                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();

                if (greatBattle == null)
                {
                    return new GreatBattleDetails { LocalizedMessage = "Нет активной битвы" };
                }

                GreatBattlesUsers battlesUser = greatBattle.GreatBattlesUsers.FirstOrDefault(u => u.UserID == userId);
                
                if (battlesUser == null || battlesUser.IsBlocked)
                {
                    return new GreatBattleDetails { LocalizedMessage = "Пользователь не прошел регистрацию." };
                }

                GreatBattleDetails battleDetails = new GreatBattleDetails
                {
                    UserId = userId,
                    UserGroupId = battlesUser.GroupID,
                    Description = greatBattle.Description,
                    DescriptionForMobile = greatBattle.DescriptionForMobile,
                    BlueSidePoints = greatBattle.GreatBattleTours.Sum(t => t.GreatBattleTourPoints.Where(p => p.GroupID == 1).Sum(p => p.PointsCount)),
                    RedSidePoints = greatBattle.GreatBattleTours.Sum(t => t.GreatBattleTourPoints.Where(p => p.GroupID == 2).Sum(p => p.PointsCount)),
                    BlueSidewinCount = greatBattle.GreatBattleTours.Count(t => t.WinnerID == 1),
                    RedSideWinCount = greatBattle.GreatBattleTours.Count(t => t.WinnerID == 2),
                    GreatBattleId = greatBattle.ID,
                    Tours = new List<GreatBattleTour>()
                };

                GreatBattleTours tourFirst = greatBattle.GreatBattleTours.OrderBy(t => t.ID).FirstOrDefault();
                GreatBattleTours tourLast = greatBattle.GreatBattleTours.OrderByDescending(t => t.ID).FirstOrDefault();

                if (tourFirst == null || tourLast == null)
                {
                    return new GreatBattleDetails();
                }

                battleDetails.BattleBeginDate = tourFirst.BeginDateTime;
                battleDetails.BattleEndDate = tourLast.EndDateTime;
                TimeSpan battleEndLeft = tourLast.EndDateTime - DateTime.Now;
                TimeSpan battleStartLeft = tourFirst.BeginDateTime - DateTime.Now;
                battleDetails.BattleEndLeft = battleEndLeft.TotalSeconds+5;//reserve 5 seconds
                battleDetails.BattleStartLeft = battleStartLeft.TotalSeconds+5;

                List<GreatBattleTours> greatBattleTours = greatBattle.GreatBattleTours.ToList();


                foreach (var tour in greatBattleTours)
                {
                    int bluesidePoints = tour.GreatBattleTourPoints.Where(p => p.GroupID == 1).Sum(p => p.PointsCount);
                    int redSidePoints = tour.GreatBattleTourPoints.Where(p => p.GroupID == 2).Sum(p => p.PointsCount);

                    GreatBattleTour tourModel = new GreatBattleTour();
                    tourModel.BlueSidePoints = bluesidePoints;
                    tourModel.RedSidePoints = redSidePoints;
                    tourModel.Id = tour.ID;
                    tourModel.BeginDateTime = tour.BeginDateTime;
                    tourModel.EndDateTime = tour.EndDateTime;
                    TimeSpan tourEndLeft = tour.EndDateTime - DateTime.Now;
                    TimeSpan tourStartLeft = tour.BeginDateTime - DateTime.Now;

                    tourModel.TourEndLeft = tourEndLeft.TotalSeconds+5;//reserve 5 seconds
                    tourModel.TourStartLeft = tourStartLeft.TotalSeconds+5;

                    tourModel.GameTourName = tour.GameTourName;
                    tourModel.GameTourImagePath = tour.GameTourImagePath;
                    tourModel.QuizzTourName = tour.QuizzTourName;
                    tourModel.QuizzTourImagePath = tour.QuizzTourImagePath;
                    tourModel.Status = 1; // Не началась

                    if (DateTime.Now >= tourModel.BeginDateTime && DateTime.Now <= tourModel.EndDateTime)
                    {
                        tourModel.GameTourPath = tour.GameTourPath;
                        tourModel.QuizzTourPath = tour.QuizzTourPath;
                        tourModel.BlueSidePoints = bluesidePoints;
                        tourModel.RedSidePoints = redSidePoints;
                        tourModel.Status = 2; // активная
                    }

                    if (DateTime.Now.AddSeconds(1) >= tourModel.EndDateTime)
                    {
                        tourModel.WinnerGroupId = tourModel.BlueSidePoints > tourModel.RedSidePoints ? 1 : 2;
                        tourModel.Status = 3; // завершена
                        tour.WinnerID = tourModel.BlueSidePoints > tourModel.RedSidePoints ? 1 : 2;
                        tour.IsFinished = true;
                    }


                    battleDetails.Tours.Add(tourModel);
                }

                if (tourLast.IsFinished)
                {
                    greatBattle.IsFinished = true;
                    battleDetails.WinnerGroupId = battleDetails.BlueSidePoints > battleDetails.RedSidePoints ? 1 : 2;
                    greatBattle.WinnerGroupId = battleDetails.WinnerGroupId;
                }


                context.SaveChanges();

                return battleDetails;
            }
        }

        internal GreatBattleGameScore SaveGameResult(GreatBattleGameScore gameScore)
        {
            using (var context = new StayTrueDBEntities())
            {

                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();

                if (greatBattle == null)
                {
                    return new GreatBattleGameScore { LocalizedMessage = "Нет активной битвы" };
                }


                GreatBattlesUsers users = context.GreatBattlesUsers.FirstOrDefault(u =>
                    u.UserID == gameScore.UserId && !u.IsBlocked && u.GreatBattleID == greatBattle.ID);

                if (users == null)
                {
                    return new GreatBattleGameScore { ErrorMessage = "Пользователь не найден" };
                }
                
                GreatBattleTours tour = context.GreatBattleTours.FirstOrDefault(t => t.GreatBattleID == greatBattle.ID && t.ID == gameScore.GreatBattleTourId);

                if (tour == null)
                    return new GreatBattleGameScore(){ErrorMessage = "Тур не найден"};

                GreatBattleTourPoints tourPoints = tour.GreatBattleTourPoints.FirstOrDefault(p => p.UserID == gameScore.UserId && p.GroupID == users.GroupID);

                int? maxUserScore = context.GreatBattleGamesSession.Where(s=>s.TourId==gameScore.GreatBattleTourId && s.UserId==gameScore.UserId).Select(s => s.Score).Max();

                if (maxUserScore >= gameScore.Score)
                {
                    return new GreatBattleGameScore() { ErrorMessage = "Рекорд не побит" };
                }

                if (tourPoints == null)
                {
                    GreatBattleTourPoints battleTourPoint = new GreatBattleTourPoints
                    {
                        GroupID = users.GroupID,
                        TourID = gameScore.GreatBattleTourId,
                        PointsCount = (int)(gameScore.Points * tour.PointRate),
                        UserID = gameScore.UserId
                    };

                    context.GreatBattleTourPoints.Add(battleTourPoint);
                }
                else
                {
                    tourPoints.PointsCount += (int)(gameScore.Points * tour.PointRate);
                }

                users.Points += (int)(gameScore.Points * tour.PointRate);

                GreatBattleGamesSession greatBattleGamesSession = new GreatBattleGamesSession()
                {
                    UserId = gameScore.UserId,
                    TourId = gameScore.GreatBattleTourId,
                    SessionEnd = DateTime.Now,
                    Score = gameScore.Score
                };
                context.GreatBattleGamesSession.Add(greatBattleGamesSession);
                context.SaveChanges();

                return gameScore;
            }
        }

        internal GreatBattleQuizResult SaveQuizResult(GreatBattleQuizResult quizSession)
        {
            using (var context = new StayTrueDBEntities())
            {
                GreatBattles greatBattle = context.GreatBattles.Where(x => x.IsActive && x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();

                if (greatBattle == null)
                {
                    return new GreatBattleQuizResult { LocalizedMessage = "Нет активной битвы" };
                }
                var dbQuizSession = context.GreatBattleQuizzesSession.FirstOrDefault(q => q.QuizId == quizSession.GreatBattleTourId && q.UserId == quizSession.UserId);
                quizSession.Score = quizSession.Score * greatBattle.QuizCoefficient;

                if (dbQuizSession == null)
                {
                    GreatBattleQuizzesSession newQuizSession = new GreatBattleQuizzesSession
                    {
                        UserId = quizSession.UserId,
                        CreationDate = DateTime.Now,
                        IsCompleted = quizSession.IsCompleted,
                        QuestionIndex = quizSession.QuestionIndex,
                        QuizId = quizSession.GreatBattleTourId,
                        Score = quizSession.Score
                    };

                    context.GreatBattleQuizzesSession.Add(newQuizSession);
                    context.SaveChanges();

                    context.SaveChanges();

                    return quizSession;
                }
                else
                {
                    dbQuizSession.IsCompleted = quizSession.IsCompleted;
                    dbQuizSession.QuestionIndex = quizSession.QuestionIndex;
                    dbQuizSession.Score = quizSession.Score;

                    if (quizSession.IsCompleted)
                    {

                        GreatBattlesUsers users = context.GreatBattlesUsers.FirstOrDefault(u => u.UserID == quizSession.UserId && u.GreatBattleID == greatBattle.ID);

                        if (users == null)
                        {
                            return new GreatBattleQuizResult { ErrorMessage = "Пользователь не найден" };
                        }

                        GreatBattleTours tour = context.GreatBattleTours.FirstOrDefault(t => t.GreatBattleID == greatBattle.ID && t.ID == quizSession.GreatBattleTourId);

                        if (tour == null)
                            return new GreatBattleQuizResult() { ErrorMessage = "Тур не найден" };

                        GreatBattleTourPoints tourPoints = tour.GreatBattleTourPoints.FirstOrDefault(p => p.UserID == quizSession.UserId && p.GroupID == users.GroupID);

                        if (tourPoints == null)
                        {
                            GreatBattleTourPoints battleTourPoint = new GreatBattleTourPoints
                            {
                                GroupID = users.GroupID,
                                TourID = quizSession.GreatBattleTourId,
                                PointsCount = quizSession.Score,
                                UserID = quizSession.UserId
                            };

                            context.GreatBattleTourPoints.Add(battleTourPoint);
                        }
                        else
                        {
                            tourPoints.PointsCount += quizSession.Score;
                        }

                        users.Points += quizSession.Score;
                    }
                    

                    context.SaveChanges();

                    return quizSession;
                }
            }
        }

        internal GreatBattleQuizResult GetQuizSession(int tourId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.GreatBattleQuizzesSession.SingleOrDefault(q => q.UserId == userId && q.QuizId == tourId);

                if (dbQuizSession != null)
                {
                    GreatBattleQuizResult quizSession = new GreatBattleQuizResult();

                    quizSession.UserId = dbQuizSession.UserId;
                    quizSession.CreationDate = dbQuizSession.CreationDate;
                    quizSession.IsCompleted = dbQuizSession.IsCompleted;
                    quizSession.QuestionIndex = dbQuizSession.QuestionIndex;
                    quizSession.GreatBattleTourId = dbQuizSession.QuizId;
                    quizSession.Score = dbQuizSession.Score;

                    return quizSession;
                }
                else
                {
                    return new GreatBattleQuizResult();
                }
            }
        }

       internal GreatBattleUserSide UserChangeSide(GreatBattleUserSide userSide)
       {
            using (var context = new StayTrueDBEntities())
            {
                GreatBattlesUsers user = context.GreatBattlesUsers.OrderByDescending(u => u.ID)
                    .FirstOrDefault(u => u.UserID == userSide.UserId && !u.IsBlocked);

                if (user == null)
                {
                    return new GreatBattleUserSide() { ErrorMessage = "Пользователь не найден"};
                }

                var greatBattle = context.GreatBattles.Where(x => x.IsActive && !x.IsDeleted)
                    .OrderBy(x => x.ID)
                    .FirstOrDefault();
                if (greatBattle != null)
                {
                    var firstTour = context.GreatBattleTours.Where(x => x.GreatBattleID == greatBattle.ID)
                        .OrderBy(x => x.BeginDateTime)
                        .FirstOrDefault();
                    if (firstTour != null && DateTime.Now < firstTour.EndDateTime && user.CanChangeSide)
                    {
                        user.GroupID = userSide.GroupId;
                        user.CanChangeSide = false;

                        AddUserToGreatBattleChat(userSide.UserId, userSide.GroupId);

                        context.SaveChanges();
                    }
                }

                return userSide;
            }
       }
       
       internal void RemoveUserFromGreatBattleChats(int userId)
       {
           using (var context = new StayTrueDBEntities())
           {
               var greatBattleGroups = context.GreatBattleGroups.ToList();
               List<ChatGroups> groups = new List<ChatGroups>();
               foreach (var group in greatBattleGroups)
               {
                   var chatGroup = context.ChatGroups
                       .Where(x => x.IsActive && !x.IsDeleted && x.Name == group.Name)
                       .OrderBy(g => g.CreationDate)
                       .FirstOrDefault();
                   groups.Add(chatGroup);
               }

               List<ChatPrivateGroupsUsers> groupUsers = new List<ChatPrivateGroupsUsers>();
               foreach (var chatGroup in groups)
               {
                   var groupUser = context.ChatPrivateGroupsUsers.FirstOrDefault(x =>
                       x.ChatGroupId == chatGroup.Id && x.UserId == userId);
                   if (groupUser != null)
                   {
                       groupUsers.Add(groupUser);
                   }
               }

               context.ChatPrivateGroupsUsers.RemoveRange(groupUsers);
               context.SaveChanges();
           }
       }

       internal void AddUserToGreatBattleChat(int userId, int greatBattleGroupId)
       {
            using (var context = new StayTrueDBEntities())
            {
                RemoveUserFromGreatBattleChats(userId);
                var groupName = context.GreatBattleGroups.Find(greatBattleGroupId)?.Name;
                var groupChat = context.ChatGroups.Where(g => g.IsActive && !g.IsDeleted && g.Name == groupName)
                    .OrderBy(g => g.CreationDate)
                    .FirstOrDefault();
                if (groupChat != null)
                {
                    ChatPrivateGroupsUsers chatUser = new ChatPrivateGroupsUsers
                    {
                        ChatGroupId = groupChat.Id,
                        UserId = userId
                    };
                    context.ChatPrivateGroupsUsers.Add(chatUser);
                }

                context.SaveChanges();
            }
       }

       /*
       internal string GetTourResults()
       {
           using (var context = new StayTrueDBEntities())
           {
               GreatBattles greatBattle = context.GreatBattles
                   .Where(x => x.IsActive && x.IsDeleted == false && x.IsFinished == false).OrderByDescending(x => x.ID)
                   .FirstOrDefault();

               if (greatBattle != null)
               {
                   foreach (GreatBattleTours tour in greatBattle.GreatBattleTours)
                   {
                       if (DateTime.Now >= tour.EndDateTime && tour.IsFinished == false)
                       {
                           int blueSidePoints = tour.GreatBattleTourPoints.Where(p => p.GroupID == 1)
                               .Sum(p => p.PointsCount);
                           int redSidePoints = tour.GreatBattleTourPoints.Where(p => p.GroupID == 2)
                               .Sum(p => p.PointsCount);

                           tour.WinnerID = blueSidePoints > redSidePoints ? 1 : 2;

                           tour.IsFinished = true;

                           context.SaveChanges();
                           return tour.WinnerID.ToString();
                       }
                   }
                }


               return String.Empty;
           }
       }

       internal string GetBattleWinnerSide()
       {
           using (var context = new StayTrueDBEntities())
           {
               GreatBattles greatBattle = context.GreatBattles
                   .Where(x => x.IsActive && x.IsDeleted == false && x.IsFinished == false && x.IsResulted==false).OrderByDescending(x => x.ID)
                   .FirstOrDefault();

               if (greatBattle != null)
               {
                   int blueSidePoints = 0;
                   int redSidePoints = 0;

                   foreach (var tour in greatBattle.GreatBattleTours)
                   {
                       blueSidePoints += tour.GreatBattleTourPoints.Where(p => p.GroupID == 1).Sum(p => p.PointsCount);
                       redSidePoints += tour.GreatBattleTourPoints.Where(p => p.GroupID == 2).Sum(p => p.PointsCount);
                   }


                   greatBattle.IsResulted = true;

                   int winnerSide = blueSidePoints > redSidePoints ? 1 : 2;
                   greatBattle.WinnerGroupId = winnerSide;
                   context.SaveChanges();
                   return JsonConvert.SerializeObject(winnerSide);
                }

               return String.Empty;

           }
       }*/

    }
}