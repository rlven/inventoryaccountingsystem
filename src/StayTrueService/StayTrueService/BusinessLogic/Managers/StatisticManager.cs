﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using Common.Models;
using DataAccessLayer;

namespace BusinessLogicLayer.Managers
{
    internal class StatisticManager
    {
        public List<Statistic> GetUsersGender()
        {
            using (var context = new StayTrueDBEntities())
            {
                var femaleUsers = context.Users.Count(x => x.GenderId == (int) GenderEnum.Female);
                var maleUsers = context.Users.Count(x => x.GenderId == (int) GenderEnum.Male);
                return new List<Statistic>
                {
                    new Statistic{Name = "Жен", Amount = femaleUsers},
                    new Statistic{Name = "Муж", Amount = maleUsers}
                };
            }
        }

        public List<Statistic> GetUsersCity()
        {
            using (var context = new StayTrueDBEntities())
            {
                var cities = context.Cities.ToList();
                var result = new List<Statistic>();
                foreach (var city in cities)
                {
                    var users = context.Users.Count(x => x.CityId == city.Id);
                    result.Add(new Statistic{Name = city.CityName, Amount = users});
                }
                return result;
            }
        }

        public List<Statistic> GetUsersAge()
        {
            using (var context = new StayTrueDBEntities())
            {
                var users18 = context.Users.Count(x =>
                    x.BirthDate < DateTime.Now.AddYears(-25) && x.BirthDate >= DateTime.Now.AddYears(-18));
                var users25 = context.Users.Count(x =>
                    x.BirthDate < DateTime.Now.AddYears(-40) && x.BirthDate >= DateTime.Now.AddYears(-25));
                var users40 = context.Users.Count(x =>
                    x.BirthDate >= DateTime.Now.AddYears(-40));
                return new List<Statistic>
                {
                    new Statistic{Name = "18 - 25", Amount = users18},
                    new Statistic{Name = "25 - 40", Amount = users25},
                    new Statistic{Name = "40 +", Amount = users40}
                };
            }
        }

        public List<Statistic> GetUsersPassport()
        {
            using (var context = new StayTrueDBEntities())
            {
                var usersAmount = context.Users.Count();
                var usersPassport = context.Users.Count(x => x.PassportBack != null && x.PassportFront != null);
                return new List<Statistic>
                {
                    new Statistic{Name = "Без паспорта", Amount = usersAmount - usersPassport},
                    new Statistic{Name = "С паспортом", Amount = usersPassport}
                };
            }
        }

        private int GetUserAge(DateTime birthDate)
        {
            var today = DateTime.Today;
            var age = today.Year - birthDate.Year;
            if (birthDate > today.AddYears(-age)) age--;
            return age;
        }
    }
}
