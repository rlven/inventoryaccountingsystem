﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Common;
using Common.Logger;
using Common.Models;
using DataAccessLayer;

namespace BusinessLogicLayer.Managers
{
    internal class ShopManager
    {

        public Product GetProduct(int ProductId, StayTrueDBEntities context)
        {
            var dbProducts = context.Products.SingleOrDefault(x => x.Id == ProductId);
            if (dbProducts != null)
                return new Product
                {
                    Id = dbProducts.Id,
                    ImagePath = dbProducts.ImagePath,
                    InformationRu = dbProducts.InformationRu,
                    InformationKg = dbProducts.InformationKg,
                    NameRu = dbProducts.NameRu,
                    NameKg = dbProducts.NameKg,
                    Price = dbProducts.Price,
                };

            return null;
        }


        public List<Shop> GetShops()
        {
            var context = new StayTrueDBEntities();
            var shops = context.Shops.OrderByDescending(x => x.Id).ToList();
            var shopsToReturn = shops.Select(x => new Shop
            {
                Id = x.Id,
                Name = x.Name,
                IsActive = x.IsActive
            }).ToList();

            if (shopsToReturn.Count != 0)
            {
                return shopsToReturn;
            }
            return null;
        }

        public Product AddProduct(Product product, string folderPath)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var dbProduct = new Products
            {
                NameRu = product.NameRu,
                NameKg = product.NameKg,
                InformationRu = product.InformationRu,
                InformationKg = product.InformationKg,
                Amount = product.Amount,
                Price = product.Price,
                ShopId = product.ShopId,
                AvailableAmount = product.AvailableAmount,
                IsAvailable = product.IsAvailable
            };
            context.Products.Add(dbProduct);
            context.SaveChanges();
            if (!string.IsNullOrEmpty(product.ImagePath))
            {
                dbProduct.ImagePath = ConvertFileHelper.SaveProductImage(folderPath, product.ImagePath, $"{dbProduct.Id}");
            }
            context.SaveChanges();
            return new Product
            {
                Id = dbProduct.Id,
                NameRu = dbProduct.NameRu,
                NameKg = dbProduct.NameKg,
                InformationRu = dbProduct.InformationRu,
                InformationKg = dbProduct.InformationKg,
                Amount = dbProduct.Amount,
                ImagePath = dbProduct.ImagePath,
                ShopId = dbProduct.ShopId,
                AvailableAmount = dbProduct.AvailableAmount,
                Price = dbProduct.Price,
                IsAvailable = dbProduct.IsAvailable
            };
        }



        public Product ChangeProduct(Product product, string folderPath)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var productContext = context.Products.FirstOrDefault(x => x.Id == product.Id);
                    productContext.NameRu = product.NameRu;
                    productContext.NameKg = product.NameKg;
                    productContext.InformationRu = product.InformationRu;
                    productContext.InformationKg = product.InformationKg;
                    productContext.Amount = product.Amount;
                    productContext.Price = product.Price;
                    productContext.ShopId = product.ShopId;
                    productContext.AvailableAmount = product.AvailableAmount;
                    productContext.IsAvailable = product.IsAvailable;
                    context.SaveChanges();
                    if (!string.IsNullOrEmpty(product.ImagePath))
                    {
                        productContext.ImagePath = ConvertFileHelper.SaveProductImage(folderPath, product.ImagePath, $"{product.Id}");
                    }
                    context.SaveChanges();

                    return new Product
                    {
                        Id = productContext.Id,
                        NameRu = productContext.NameRu,
                        InformationRu = productContext.InformationRu,
                        Amount = productContext.Amount,
                        ImagePath = productContext.ImagePath,
                        ShopId = productContext.ShopId,
                        AvailableAmount = productContext.AvailableAmount,
                        Price = productContext.Price
                    };
                }
                catch (Exception)
                {
                    return null;
                }
            }

        }

        public string RemoveProduct(Product product)
        {
            StayTrueDBEntities context = new StayTrueDBEntities();
            var productContext = context.Products.FirstOrDefault(x => x.Id == product.Id);
            if (productContext != null)
            {
                context.Products.Remove(productContext);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        public List<Product> GetProductShop(int categoryId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {

                DateTime now = DateTime.Now;

                IQueryable<Products> products = context.Products.Where(x => x.IsDeleted == false && now >= x.PublicationDate && x.IsAvailable).OrderByDescending(x => x.Id);


                if (categoryId > 0)
                    products = products.Where(x => x.CategoryId == categoryId);


                return products.ToList().Select(x => new Product
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    AvailableAmount = GetProductAvailableAmount(x.Id, userId, context),
                    ImagePath = x.ImagePath,
                    InformationRu = x.InformationRu,
                    InformationKg = x.InformationKg,
                    IsAvailable = x.IsAvailable,
                    NameRu = x.NameRu,
                    NameKg = x.NameKg,
                    Price = x.Price,
                    ShopId = x.ShopId

                }).ToList();

            }
        }

        private int GetProductAvailableAmount(int productId, int userId, StayTrueDBEntities context)
        {
            var product = context.Products.SingleOrDefault(x => x.Id == productId);
            var userProducts = context.PurchaseHistories.Where(x => x.ProductId == productId && x.UserId == userId);
            int productAmount = 0;
            foreach (var item in userProducts)
            {
                productAmount += item.Amount;
            }
            return product.AvailableAmount - productAmount;
        }

        public BuyProduct BuyProduct(BuyProduct buyProduct)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var product = context.Products.SingleOrDefault(x => x.Id == buyProduct.ProductId);
                    var user = context.Users.SingleOrDefault(x => x.Id == buyProduct.UserId);
                    int userPurchasesCount = context.PurchaseHistories.Count(x =>
                        x.ProductId == buyProduct.ProductId && x.UserId == buyProduct.UserId);

                    if (product == null)
                        return new BuyProduct {ErrorMessage = Resources.ProductNotFound};

                    if (user == null)
                        return new BuyProduct {ErrorMessage = Resources.UserNotFound};

                    if (userPurchasesCount >= product.AvailableAmount)
                        return new BuyProduct {ErrorMessage = Resources.PurchaseLimitExceeded};

                    if (product.Price > user.Wincoins)
                        return new BuyProduct {ErrorMessage = Resources.NotEnoughWincoinsToPay};

                    if (buyProduct.Amount > product.Amount || buyProduct.Amount < 1 || product.Amount == 0)
                        return new BuyProduct {ErrorMessage = Resources.NotEnoughProductAmount};


                    product.Amount -= 1;
                    user.Wincoins -= product.Price;

                    PurchaseHistories histories = new PurchaseHistories();
                    histories.Amount = 1;
                    histories.CreationDate = DateTime.Now;
                    histories.Price = product.Price;
                    histories.ProductId = product.Id;
                    histories.UserId = user.Id;
                    histories.StatusId = 1; // PurchaseStatuses: 1 - Не выдан
                    context.PurchaseHistories.Add(histories);

                    context.SaveChanges();

                    buyProduct.ShopAddress = product.Shops.Address;
                    buyProduct.ShopPhone = product.Shops.Phone;

                    return buyProduct;
                }
                catch (DbUpdateException ex)
                {
                    Log.Error(ex.Message);
                    Log.Error(ex.InnerException);
                    return new BuyProduct(){ErrorMessage = ex.Message};
                }
            }
        }


        public int GetProductPagesCount(int shopId, int quantity)
        {
            var context = new StayTrueDBEntities();
            return (context.Products.Count(x => x.ShopId == shopId) + quantity - 1) / quantity;
        }
    }
}
