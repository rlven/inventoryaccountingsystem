﻿using Common.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using QuestionnaireAnswers = Common.Models.QuestionnaireAnswers;
using System.Reflection;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class QuizManager
    {
        internal List<Quiz> GetQuizzes()
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<Quizzes> quizzes = context.Quizzes.Where(x => x.IsActive && x.IsDeleted == false && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);

                return quizzes.ToList().Select(x => new Quiz
                {
                    Id = x.Id,
                    Name = x.Name,
                    Url = x.Url,
                    Image = x.ImagePath,
                    Points = (int)x.Wincoins,
                    Description = x.Description
                }).ToList();
            }
        }

        internal List<QuizScore> GetBestScores(int userId, int quizId)
        {
            using (var context = new StayTrueDBEntities())
            {
                IQueryable<QuizzesSession> quizzesSession = context.QuizzesSession.OrderByDescending(x => x.Id);

                if (quizId > 0)
                {
                    quizzesSession = quizzesSession.Where(x => x.QuizId == quizId);
                }

                List<QuizScore> quizScores = quizzesSession.ToList().Select( q => new QuizScore
                {
                    UserFullName = q.Users.FirstName + " " + q.Users.LastName,
                    QuizId = q.QuizId,
                    UserId = q.UserId,
                    Score = q.Score

                }).ToList();

                List<QuizScore> groupedQuizScores = quizScores.GroupBy(gs => new { gs.UserId, gs.QuizId, gs.UserFullName }).Select(gs =>
                      new QuizScore
                      {
                          QuizId = gs.Key.QuizId,
                          UserId = gs.Key.UserId,
                          UserFullName = gs.Key.UserFullName,
                          Score = gs.Max(m => m.Score)

                      }).Take(20).OrderByDescending(x => x.Score).ToList();

                if (groupedQuizScores.Any(g => g.UserId == userId))
                {
                    return groupedQuizScores;
                }
                else
                {
                    QuizScore userQuizScore = context.QuizzesSession.Where(q => q.UserId == userId && q.QuizId == quizId).Select( q => 
                        new QuizScore { UserFullName = q.Users.FirstName + " " + q.Users.LastName,
                            QuizId = q.QuizId,
                            Score = q.Score,
                            UserId = q.UserId
                        }).FirstOrDefault();

                    if (userQuizScore == null)
                    {
                        return groupedQuizScores;
                    }
                    else
                    {
                        if (groupedQuizScores.Count >= 20)
                        {
                            groupedQuizScores.RemoveAt(groupedQuizScores.Count - 1);
                        }

                        groupedQuizScores.Add(userQuizScore);
                    }
                }

                return groupedQuizScores;
            }
        }

        internal QuizSession GetQuizSession(int quizId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.QuizzesSession.SingleOrDefault(q => q.UserId == userId && q.QuizId == quizId);

                if (dbQuizSession != null)
                {
                    QuizSession quizSession = new QuizSession();

                    quizSession.Id = dbQuizSession.Id;
                    quizSession.UserId = dbQuizSession.UserId;
                    quizSession.CreationDate = dbQuizSession.CreationDate;
                    quizSession.IsCompleted = dbQuizSession.IsCompleted;
                    quizSession.QuestionIndex = dbQuizSession.QuestionIndex;
                    quizSession.QuizId = dbQuizSession.QuizId;
                    quizSession.Score = dbQuizSession.Score;

                    return quizSession;
                }
                else
                {
                    return new QuizSession();
                }
            }
        }

        internal QuizSession GetPartyQuizSession(int quizId, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.PartyQuizzSession.Where(q => q.UserId == userId && q.QuizzId == quizId)
                    .OrderByDescending(q => q.Id).FirstOrDefault();
                   

                if (dbQuizSession != null)
                {
                    QuizSession quizSession = new QuizSession();

                    quizSession.Id = dbQuizSession.Id;
                    quizSession.UserId = dbQuizSession.UserId;
                    quizSession.IsCompleted = dbQuizSession.IsCompleted;
                    quizSession.QuestionIndex = dbQuizSession.QuestionIndex;
                    quizSession.QuizId = dbQuizSession.QuizzId;
                    quizSession.Score = dbQuizSession.Score;

                    return quizSession;
                }
                else
                {
                    return new QuizSession();
                }
            }
        }

        internal QuizSession SaveQuizSession(QuizSession quizSession)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.QuizzesSession.SingleOrDefault(q => q.QuizId == quizSession.QuizId && q.UserId == quizSession.UserId);

                if (dbQuizSession == null)
                {
                    QuizzesSession newQuizSession = new QuizzesSession
                    {
                        UserId = quizSession.UserId,
                        CreationDate = DateTime.Now,
                        IsCompleted = quizSession.IsCompleted,
                        QuestionIndex = quizSession.QuestionIndex,
                        QuizId = quizSession.QuizId,
                        Score = quizSession.Score
                    };

                    context.QuizzesSession.Add(newQuizSession);
                    context.SaveChanges();

                    context.SaveChanges();

                    return quizSession;
                }
                else
                {
                    dbQuizSession.IsCompleted = quizSession.IsCompleted;
                    dbQuizSession.QuestionIndex = quizSession.QuestionIndex;
                    dbQuizSession.Score = quizSession.Score;

                    if (quizSession.IsCompleted)
                    {

                        Users user = context.Users.SingleOrDefault(x => x.Id == quizSession.UserId);

                        if (user == null)
                        {
                            return new QuizSession { ErrorMessage = "User id was not found" };
                        }

                        user.Wincoins += dbQuizSession.Quizzes.Wincoins;
                    }

                    context.SaveChanges();

                    quizSession.Id = dbQuizSession.Id;

                    return quizSession;
                }
            }
        }

        internal QuizSession SavePartyQuizzSession(QuizSession quizSession)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.PartyQuizzSession.SingleOrDefault(q => q.QuizzId == quizSession.QuizId && q.UserId == quizSession.UserId);
                var party = context.Party.OrderByDescending(p => p.Id).FirstOrDefault();
                if (dbQuizSession == null)
                {
                    PartyQuizzSession newQuizSession = new PartyQuizzSession()
                    {
                        UserId = quizSession.UserId,
                        IsCompleted = quizSession.IsCompleted,
                        QuestionIndex = quizSession.QuestionIndex,
                        Score = quizSession.Score,
                        QuizzId = quizSession.QuizId
                    };

                    context.PartyQuizzSession.Add(newQuizSession);
                }
                else
                {
                    dbQuizSession.IsCompleted = quizSession.IsCompleted;
                    dbQuizSession.QuestionIndex = quizSession.QuestionIndex;
                    dbQuizSession.Score = quizSession.Score;
                    quizSession.Id = dbQuizSession.Id;

                }

                if (quizSession.IsCompleted)
                {

                    PartyUsers user = context.PartyUsers.SingleOrDefault(x => x.UserId == quizSession.UserId && x.PartyId == party.Id && !x.IsBlocked);

                    if (user == null)
                    {
                        return new QuizSession { ErrorMessage = "User id was not found" };
                    }

                    if (dbQuizSession != null)
                    {
                        if (!dbQuizSession.IsCompleted)
                            user.Points += quizSession.Score;
                    }
                    else
                    {
                        user.Points += quizSession.Score;
                    }
                }

                context.SaveChanges();
                return quizSession;
            }
        }

        internal QuizzAnswer SaveQuestionnaireQuizzAnswer(QuizzAnswer answers, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var dbQuizSession = context.QuizzesSession.SingleOrDefault(q => q.QuizId == answers.QuizzId && q.UserId == userId);

                if (dbQuizSession == null)
                {
                    QuizzesSession newQuizSession = new QuizzesSession
                    {
                        UserId = userId,
                        CreationDate = DateTime.Now,
                        IsCompleted = true,
                        QuizId = answers.QuizzId,
                        IsIssued = false
                    };
                    
                    List<QuizzSessionAnswers> quizzAnswers = new List<QuizzSessionAnswers>();


                    foreach (PropertyInfo propertyInfo in answers.GetType().GetProperties())
                    {
                        if(propertyInfo.Name.Contains("QuizzId") 
                           
                           || propertyInfo.Name.Contains("LocalizedMessage") 
                                                                 
                           || propertyInfo.Name.Contains("ErrorMessage"))

                            continue;
                        var sessionAnswers = new QuizzSessionAnswers()
                        {
                            QuizzSessionId = newQuizSession.Id,
                            Question = propertyInfo.Name,
                            Answer = propertyInfo.GetValue(answers, null).ToString(),
                            IsIssued = false
                        };
                        quizzAnswers.Add(sessionAnswers);
                    }

                    context.QuizzSessionAnswers.AddRange(quizzAnswers);
                    context.QuizzesSession.Add(newQuizSession);
                    context.SaveChanges();

                    return answers;
                }
                return new QuizzAnswer(){ErrorMessage = "Session already exist"};
            }
        }
    }
}