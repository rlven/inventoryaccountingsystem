﻿using Common.Models;
using DataAccessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using Common;
using Common.Logger;
using ModelState = System.Web.Http.ModelBinding.ModelState;
using PartyGames = Common.Models.PartyGames;
using PartyQuestionnaire = Common.Models.PartyQuestionnaire;
using PartyQuizzes = Common.Models.PartyQuizzes;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class PartyManager
    {
        internal PartyDetails GetPartyDetails(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Party party = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false).Include(p=>p.PartyQuestionnaire).OrderByDescending(p=>p.Id).FirstOrDefault();

                if (party == null)
                {
                    return new PartyDetails { LocalizedMessage = "Нет активной вечеринки", IsActive = false };
                }

                PartyUsers users = context.PartyUsers.Where(p => p.PartyId == party.Id && p.UserId == userId).FirstOrDefault();

                if (users != null)
                {
                    if (users.IsBlocked)
                    {
                        return new PartyDetails { LocalizedMessage = "Пользователь заблокирован", IsBlocked = false };
                    }
                }

                if (DateTime.Now > party.EndDate)
                {
                    party.IsDisabled = true;
                    context.SaveChanges();
                }

                DataAccessLayer.PartyQuestionnaire questionnaire = party.PartyQuestionnaire.FirstOrDefault(q=>q.PartyId==party.Id);


                List<PartyGames> games = new List<PartyGames>();
                List<PartyQuizzes> quizzes = new List<PartyQuizzes>();
                List<PartyQuestion> questions = new List<PartyQuestion>();

                if (questionnaire != null)
                {
                    if (questionnaire.Question1 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question1 });
                    }
                    if (questionnaire.Question2 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question2 });
                    }

                    if (questionnaire.Question3 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question3 });
                    }
                    if (questionnaire.Question4 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question4 });
                    }
                    if (questionnaire.Question5 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question5 });
                    }
                    if (questionnaire.Question6 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question6 });
                    }
                    if (questionnaire.Question7 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question7 });
                    }
                    if (questionnaire.Question8 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question8 });
                    }
                    if (questionnaire.Question9 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question9 });
                    }
                    if (questionnaire.Question10 != null)
                    {
                        questions.Add(new PartyQuestion() { Question = questionnaire.Question10 });
                    }

                }

                PartyDetails partyDetails = new PartyDetails()
                {
                    Name = party.Name,
                    Description = party.Description,
                    DescriptionForMobile = party.DescriptionForMobile,
                    Questions = questions,
                    ImagePreview = party.ImagePreview,
                    ImagePreviewSmall = party.ImagePreviewSmall,
                    IsActive = party.IsActive,
                    IsDisabled = party.IsDisabled,
                    IsInvited = false
                };

                SerializeGames(party.PartyGames.ToList(), games);
                SerializeQuizzess(party.PartyQuizzes.ToList(), quizzes);

                partyDetails.PartyGames = games;
                partyDetails.PartyQuizzes = quizzes;

                if (users != null)
                {
                    partyDetails.IsAnswered = true;
                    partyDetails.IsInvited = users.IsInvited;
                    partyDetails.IsBlocked = users.IsBlocked;
                }

                return partyDetails;

            }
        }

        internal Common.Models.PartyQuestionnaireAnswer PartyRegistrationAnswer(Common.Models.PartyQuestionnaireAnswer partyQuestionnaireAnswer, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Party party = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false).Include(p => p.PartyQuestionnaire).OrderByDescending(p => p.Id).FirstOrDefault();

                if (party == null)
                {
                    return new Common.Models.PartyQuestionnaireAnswer { LocalizedMessage = "Нет активной вечеринки" };
                }

                DataAccessLayer.PartyQuestionnaire questionnaire = party.PartyQuestionnaire.FirstOrDefault(q => q.PartyId == party.Id);

                var answerUser = questionnaire.PartyQuestionnaireAnswer.FirstOrDefault(p => p.UserId == userId);

                if (answerUser == null)
                {                    
                    DataAccessLayer.PartyQuestionnaireAnswer answer = new DataAccessLayer.PartyQuestionnaireAnswer()
                    {
                        PartyQuestionnaireId = questionnaire.Id,
                        Answer1 = partyQuestionnaireAnswer.PartyAnswers.Answer1,
                        Answer2 = partyQuestionnaireAnswer.PartyAnswers.Answer2,
                        Answer3 = partyQuestionnaireAnswer.PartyAnswers.Answer3,
                        Answer4 = partyQuestionnaireAnswer.PartyAnswers.Answer4,
                        Answer5 = partyQuestionnaireAnswer.PartyAnswers.Answer5,
                        Answer6 = partyQuestionnaireAnswer.PartyAnswers.Answer6,
                        Answer7 = partyQuestionnaireAnswer.PartyAnswers.Answer7,
                        Answer8 = partyQuestionnaireAnswer.PartyAnswers.Answer8,
                        Answer9 = partyQuestionnaireAnswer.PartyAnswers.Answer9,
                        Answer10 = partyQuestionnaireAnswer.PartyAnswers.Answer10,
                        CreationDate = DateTime.Now,
                        UserId = userId
                    };

                    PartyUsers user = new PartyUsers()
                    {
                        UserId = userId,
                        PartyId = party.Id,
                        IsBlocked = false,
                        IsInvited = false,
                        HasCome = false,
                        CreationDate = DateTime.Now,
                        IsAnswered = true
                    };

                    context.PartyQuestionnaireAnswer.Add(answer);
                    context.PartyUsers.Add(user);

                    context.SaveChanges();
                }


                return partyQuestionnaireAnswer;
            }
        }

        private List<PartyGames> SerializeGames(List<DataAccessLayer.PartyGames> partyGames, List<PartyGames> games)
        {
            if (partyGames != null)
            {

                foreach (var par in partyGames)
                {
                    PartyGames game = new PartyGames()
                    {
                        GameUrl = par.GamePath,
                        Image = par.GameImage,
                        Id = par.Id,
                        IsActive = par.IsActive,
                        Name = par.Name,
                        PublicationDate = par.PublicationDate,
                        Points = par.Points
                    };
                    games.Add(game);
                }
            }

            return games;
        }
        private List<PartyQuizzes> SerializeQuizzess(List<DataAccessLayer.PartyQuizzes> partyQuizzes, List<Common.Models.PartyQuizzes> quizzes)
        {
            if (partyQuizzes != null)
            {
                partyQuizzes = partyQuizzes.OrderBy(x => x.Id).ToList();
                PartyQuizzes quizz = new PartyQuizzes()
                {
                    Url = partyQuizzes[partyQuizzes.Count-1].QuizzPth,
                    Image = partyQuizzes[partyQuizzes.Count - 1].QuizzImage,
                    Id = partyQuizzes[partyQuizzes.Count - 1].Id,
                    Name = partyQuizzes[partyQuizzes.Count - 1].QuizzName
                };
                quizzes.Add(quizz);
            }


            return quizzes;
        }

        public PartyUser CheckPartyUserStatus(int id)
        {
            using (var context = new StayTrueDBEntities())
            {
                var user = context.Users.Find(id);

                if (user == null)
                {
                    return new PartyUser() { LocalizedMessage = "Пользователь не найден" , IsError = true};
                }
                var activeParty = context.Party.Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false).Include(p=>p.PartyUsers).OrderByDescending(p => p.Id).FirstOrDefault(); 

                if (activeParty == null)
                {
                    return new PartyUser() { LocalizedMessage = "Нет активной вечеринки" ,IsError = true};
                }

                var partyUser = activeParty.PartyUsers.FirstOrDefault(u=>u.UserId==user.Id && u.PartyId==activeParty.Id);

                if (partyUser == null)
                {
                    return new PartyUser() {LocalizedMessage = "Пользователь не учавствует в вечеринке", IsError = true};
                }

                if (partyUser.IsInvited == false)
                {
                    return new PartyUser() { LocalizedMessage = "Пользователь не приглашен", IsError = true};
                }

                if (partyUser.HasCome)
                {
                    return new PartyUser() { LocalizedMessage = "Пользователь уже пришел", IsError = true };
                }

                var partyQuestions = context.PartyQuestionnaire.FirstOrDefault(u => u.PartyId == activeParty.Id);
                var userAnswers = context.PartyQuestionnaireAnswer.FirstOrDefault(u =>
                    u.UserId == id && u.PartyQuestionnaireId == partyQuestions.Id);
                if (userAnswers == null)
                {
                    return new PartyUser() { LocalizedMessage = "Пользователь не прошел опросник", IsError = true };
                }

                return new PartyUser()
                {
                     UserId = user.Id,
                     Phone = user.Phone,
                     FirstName = user.FirstName,
                     LastName = user.LastName,
                     MiddleName = user.MiddleName,
                     Passport = userAnswers.Answer5,
                     IsInvited = partyUser.IsInvited,
                     HasCome = partyUser.HasCome,
                     IsError = false                     
                };
            }
        }

        public PartyUser ChangePartyUserHasComeStatus(int? id)
        {
            using (var context = new StayTrueDBEntities())
            {
                try
                {
                    var activeParty = context.Party
                        .Where(p => p.IsActive && p.IsDeleted == false && p.IsFinished == false)
                        .Include(p => p.PartyUsers).OrderByDescending(p => p.Id).FirstOrDefault();

                    if (activeParty == null)
                    {
                        return new PartyUser() {LocalizedMessage = Resources.PartyWasNotFound};
                    }

                    var user = activeParty.PartyUsers.FirstOrDefault(p => p.UserId == id);

                    if (user == null)
                    {
                        return new PartyUser() { LocalizedMessage = Resources.PartyUserNotFound };
                    }

                    if (user.IsInvited == false)
                    {
                        return new PartyUser() { LocalizedMessage = Resources.PartyUserNotInvited };
                    }

                    if (user.HasCome)
                    {
                        return new PartyUser() { LocalizedMessage = Resources.PartyUserHasComeAlready };
                    }

                    user.HasCome = true;

                    context.SaveChanges();

                    return new PartyUser()
                    {
                        HasCome = user.HasCome,
                        UserId = id
                    };
                }
                catch (DbUpdateException ex)
                {
                    Log.Error(ex.Message);
                    return new PartyUser() { ErrorMessage = ex.Message };
                }
            }
        }
    }
}