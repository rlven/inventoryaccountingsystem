﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;
using Common;
using Common.Models;
using DataAccessLayer;
using NLog.Fluent;
using Log = Common.Logger.Log;

namespace StayTrueService.BusinessLogic.Managers
{
    public class LotteryManager 
    {
        public LotteryDetails GetLotteryDetails(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                var activeLottery = context.Lottery.FirstOrDefault(l => l.IsActive && !l.IsDeleted);
                if(activeLottery==null)
                    return new LotteryDetails(){ErrorMessage = Resources.NoActiveLottery};
                var curUserPurchases = context.LotteryUsers.FirstOrDefault(u => u.UserId == userId && u.LotteryId==activeLottery.Id);
                int userPurchasesCount = 0;
                if (curUserPurchases != null)
                {
                    userPurchasesCount = curUserPurchases.TicketsCount;
                }
                return new LotteryDetails()
                {
                    IsActive = activeLottery.IsActive,
                    TicketPrice = activeLottery.TicketPrice,
                    MaxTicketCountForUser = activeLottery.MaxTicketsForUser,
                    PurchasedTicketsCount = userPurchasesCount
                };
            }
        }

        public BuyLotteryTicket BuyLotteryTicket(BuyLotteryTicket ticket, int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                if(ticket.TicketsCount<=0)
                    return new BuyLotteryTicket() { ErrorMessage = Resources.TicketsCountMustBeMoreThan0 };
                var activeLottery = context.Lottery.FirstOrDefault(l => l.IsActive && !l.IsDeleted);
                if (activeLottery == null)
                    return new BuyLotteryTicket() { ErrorMessage = Resources.NoActiveLottery };
                var lotteryUser = context.LotteryUsers.FirstOrDefault(l => l.UserId == userId && l.LotteryId == activeLottery.Id);
                var currUser = context.Users.Find(userId);
                if (currUser == null)
                    return new BuyLotteryTicket() { ErrorMessage = Resources.UserNotFound };
                if (!IsUserHaveWincoins(currUser, activeLottery, ticket.TicketsCount))
                    return new BuyLotteryTicket() { ErrorMessage = Resources.NotEnoughWincoins, UserCurrentWincoins = currUser.Wincoins };
                try
                {
                    if (lotteryUser == null)
                    {
                        if (!IsTicketAvailable(activeLottery, ticket))
                            return new BuyLotteryTicket() { ErrorMessage = Resources.NoAvailableTickets };
                        var newLotteryUser = new LotteryUsers()
                        {
                            LotteryId = activeLottery.Id,
                            TicketsCount = ticket.TicketsCount,
                            UserId = userId
                        };
                        activeLottery.AvailableTickets -= ticket.TicketsCount;
                        currUser.Wincoins -= activeLottery.TicketPrice * ticket.TicketsCount;
                        context.LotteryUsers.Add(newLotteryUser);
                        context.SaveChanges();
                        return new BuyLotteryTicket()
                        {
                            LocalizedMessage = Resources.Ok,
                            TicketsCount = ticket.TicketsCount,
                            UserCurrentWincoins = currUser.Wincoins
                        };
                    }
                    if (!IsTicketAvailable(activeLottery, ticket, lotteryUser))
                        return new BuyLotteryTicket() { ErrorMessage = Resources.NoAvailableTickets };

                    activeLottery.AvailableTickets -= ticket.TicketsCount;
                    lotteryUser.TicketsCount += ticket.TicketsCount;
                    currUser.Wincoins -= activeLottery.TicketPrice * ticket.TicketsCount;
                    context.SaveChanges();
                    return new BuyLotteryTicket()
                    {
                        LocalizedMessage = Resources.Ok,
                        TicketsCount = ticket.TicketsCount,
                        UserCurrentWincoins = currUser.Wincoins
                    };
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return new BuyLotteryTicket() { ErrorMessage = ex.Message };
                }

            }
        }


        private bool IsTicketAvailable(Lottery lottery, BuyLotteryTicket ticket)
        {
            int availableTickets = lottery.AvailableTickets;
            int maxAvailablePurchaseCountForUser = lottery.MaxTicketsForUser;
            int purchasedTickets = ticket.TicketsCount;

            if (maxAvailablePurchaseCountForUser < purchasedTickets)
                return false;

            if (availableTickets < purchasedTickets)
                return false;

            return true;
        }

        private bool IsUserHaveWincoins(Users user, Lottery lottery, int ticketsCount)
        {
            return lottery.TicketPrice* ticketsCount < user.Wincoins ? true : false;
        }

        private bool IsTicketAvailable(Lottery lottery, BuyLotteryTicket ticket, LotteryUsers user)
        {
            int availableTickets = lottery.AvailableTickets;
            int maxAvailablePurchaseCountForUser = lottery.MaxTicketsForUser;
            int userPurchasedTicketsCount = user.TicketsCount + ticket.TicketsCount;

            if (maxAvailablePurchaseCountForUser < userPurchasedTicketsCount)
                return false;

            if (availableTickets < ticket.TicketsCount)
                return false;

            return true;
        }
    }
}