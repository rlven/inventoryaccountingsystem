﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common;
using Common.Enums;
using Common.Models;
using DataAccessLayer;

namespace BusinessLogicLayer.Managers
{
    internal class AlbumManager
    {
        #region Album
        public Album AddCategoryAlbum(Album album, string folderPath)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var dbAlbum = new Albums
                        {
                            Name = album.Name,
                            CreationDate = DateTime.Now,
                            IsActive = album.IsActive
                        };
                        context.Albums.Add(dbAlbum);
                        context.SaveChanges();
                        if (!string.IsNullOrEmpty(album.Image))
                            dbAlbum.Image =
                                ConvertFileHelper.SaveAlbumImage(folderPath, album.Image, $"{dbAlbum.Id}");
                        context.SaveChanges();

                        dbContextTransaction.Commit();
                        return new Album
                        {
                            Id = dbAlbum.Id,
                            Name = dbAlbum.Name,
                            CreationDate = dbAlbum.CreationDate,
                            IsActive = dbAlbum.IsActive,
                            Image = dbAlbum.Image
                        };
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return null;
                    }
                }
            }
        }


        public AlbumImage EditImageInAlbum(AlbumImage ai, string folderPath)
        {
            var context = new StayTrueDBEntities();
            var dbImage = context.AlbumImages.SingleOrDefault(x => x.Id == ai.Id);
            try
            {
                if (dbImage != null)
                {
                    dbImage.Image = ConvertFileHelper.SaveImageInAlbum(folderPath, ai.Image, ai.Id, ai.CategoryId);
                    context.SaveChanges();
                    return new AlbumImage
                    {
                        Id = ai.Id,
                        CategoryId = ai.CategoryId,
                        Image = ai.Image
                    };
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Album> GetAlbum(bool? isBrended, int skip, int take)
        {
            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    IQueryable<Albums> albums = context.Albums.Where(x => x.IsDeleted == false && x.IsActive && DateTime.Now >= x.PublicationDate).OrderByDescending(x => x.Id);

                    if (isBrended.HasValue)
                        albums = albums.Where(x => x.IsBrended == isBrended);

                    if (skip > 0)
                        albums = albums.Skip(skip);

                    if (take > 0)
                        albums = albums.Take(take);


                    return albums.ToList().Select(x => new Album
                    {
                        Id = x.Id,
                        Name = x.Name,
                        CreationDate = x.CreationDate,
                        IsActive = x.IsActive,
                        Image = x.Image

                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string GetUrl(string folderUrl, string imageUrl)
        {
            return string.IsNullOrEmpty(imageUrl) ? null : $"{folderUrl}{imageUrl}";
        }
        #endregion

        #region Album gallery

        public AlbumImage AddAlbumImage(AlbumImage albumImage, string folderPath)
        {
            using (var context = new StayTrueDBEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var dbImage = new AlbumImages
                        {
                            CategoryId = albumImage.CategoryId,
                            IsBrand = albumImage.IsBrand
                        };
                        context.AlbumImages.Add(dbImage);
                        context.SaveChanges();

                        if (!string.IsNullOrEmpty(albumImage.Image))
                        {
                            dbImage.Image = ConvertFileHelper.SaveImageInAlbum(folderPath, albumImage.Image, dbImage.Id, albumImage.CategoryId);
                            dbImage.SmallImage = ConvertFileHelper.SaveSmallImageInAlbum(folderPath,
                                dbImage.Id, albumImage.CategoryId);
                        }
                        if (!string.IsNullOrEmpty(albumImage.Video))
                        {
                            dbImage.Video = ConvertFileHelper.SaveGalleryVideo(folderPath, albumImage.Video, $"{dbImage.Id}");
                        }
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        return new AlbumImage
                        {
                            Id = dbImage.Id,
                            CategoryId = dbImage.CategoryId,
                            Image = dbImage.Image,
                            SmallImage = dbImage.SmallImage
                        };
                    }
                    catch (Exception e)
                    {
                        File.AppendAllText(@"C:\Error.txt", $"{e.Message}\n");
                        dbContextTransaction.Rollback();
                        return null;
                    }
                }
            }
        }

        public List<AlbumImage> GetAlbumImages(int albumId, bool? isBrended, int skip, int take)
        {
            using (var context = new StayTrueDBEntities())
            {

                IQueryable<AlbumImages> albumsImages = context.AlbumImages.Where(x => x.IsActive).OrderByDescending(x => x.Id);

                if (isBrended.HasValue)
                    albumsImages = albumsImages.Where(x => x.IsBrand == isBrended);

                if (albumId > 0)
                    albumsImages = albumsImages.Where(x => x.CategoryId == albumId);

                if (skip > 0)
                    albumsImages = albumsImages.Skip(skip);

                if (take > 0)
                    albumsImages = albumsImages.Take(take);


                return albumsImages.ToList().Select(x => new AlbumImage()
                {
                    Id = x.Id,
                    CategoryId = x.CategoryId,
                    Image = x.Image,
                    SmallImage = x.SmallImage,
                    IsActive = x.IsActive,
                    IsBrand = (bool)x.IsBrand,
                    Video = x.Video
                }).ToList();
            }
        }

        public Album EditAlbum(Album album, string folderPath)
        {
            var context = new StayTrueDBEntities();
            var contextAlbum = context.Albums.SingleOrDefault(x => x.Id == album.Id);
            if (contextAlbum != null)
            {
                contextAlbum.Name = album.Name;
                contextAlbum.IsActive = album.IsActive;
                if (!contextAlbum.Image.Equals(album.Image))
                {
                    contextAlbum.Image = ConvertFileHelper.SaveImage(folderPath, album.Image, $"{album.Id}");
                }
                context.SaveChanges();
                return new Album
                {
                    Id = contextAlbum.Id,
                    Name = contextAlbum.Name,
                    IsActive = contextAlbum.IsActive,
                    CreationDate = contextAlbum.CreationDate,
                    Image = contextAlbum.Image
                };
            }
            return null;
        }

        public string DeleteImageInAlbum(AlbumImage ai)
        {
            var context = new StayTrueDBEntities();
            var dbImage = context.AlbumImages.SingleOrDefault(x => x.Id == ai.Id);

            if (dbImage != null)
            {
                context.AlbumImages.Remove(dbImage);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        public string DeleteCategory(Album album)
        {
            var context = new StayTrueDBEntities();
            var dbCategory = context.Albums.SingleOrDefault(x => x.Id == album.Id);
            if (dbCategory != null)
            {
                var images = context.AlbumImages.Where(x => x.CategoryId == album.Id).OrderByDescending(x => x.Id);
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        if (images.Count() != 0)
                        {
                            context.AlbumImages.RemoveRange(images);
                            context.SaveChanges();
                        }
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return Resources.UnknownError;
                    }
                    dbContextTransaction.Commit();
                }
                context.Albums.Remove(dbCategory);
                context.SaveChanges();
                return Resources.Ok;
            }
            return Resources.UnknownError;
        }

        #endregion
    }
}