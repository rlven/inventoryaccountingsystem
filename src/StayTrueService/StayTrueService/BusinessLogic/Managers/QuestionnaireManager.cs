﻿using System;
using System.Linq;
using Common.Models;
using DataAccessLayer;
using QuestionnaireAnswers = Common.Models.QuestionnaireAnswers;

namespace StayTrueService.BusinessLogic.Managers
{
    public class QuestionnaireManager
    {
        public Questionnaire GetQuestionnaires(int userId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Questionnaires questionnaire = context.Questionnaires.OrderByDescending(q => q.Id).FirstOrDefault(x => x.IsActive && x.IsDeleted == false && DateTime.Now >= x.StartDate);

                if (questionnaire != null)
                {
                    var answer = questionnaire.QuestionnaireAnswers.FirstOrDefault(q => q.UserId == userId);
                    if (answer == null)
                    {
                        return new Questionnaire
                        {
                            Id = questionnaire.Id,
                            Name = questionnaire.Name,
                            Description = questionnaire.Description,
                            Wincoins = questionnaire.Wincoins,
                            Questions = questionnaire.Questions
                        };
                    }
                }
                
                return new Questionnaire { ErrorMessage = "Questionnaire was not found" };
            }
        }

        public QuestionnaireAnswers SaveQuestionnaireAnswers(QuestionnaireAnswers answers)
        {
            using (var context = new StayTrueDBEntities())
            {

                var questionnaire = context.Questionnaires.FirstOrDefault(q => q.Id == answers.QuestionnaireId);

                if (questionnaire == null)
                {
                    return new QuestionnaireAnswers { ErrorMessage = "Questionnaire was not found" };
                }

                DataAccessLayer.QuestionnaireAnswers newAnswers = new DataAccessLayer.QuestionnaireAnswers();

                newAnswers.Answer1 = answers.Answer1;
                newAnswers.Answer2 = answers.Answer2;
                newAnswers.Answer3 = answers.Answer3;
                newAnswers.Answer4 = answers.Answer4;
                newAnswers.Answer5 = answers.Answer5;
                newAnswers.Answer6 = answers.Answer6;
                newAnswers.Answer7 = answers.Answer7;
                newAnswers.Answer8 = answers.Answer8;
                newAnswers.Answer9 = answers.Answer9;
                newAnswers.Answer10 = answers.Answer10;
                newAnswers.Answer11 = answers.Answer11;
                newAnswers.Answer12 = answers.Answer12;
                newAnswers.UserId = answers.UserId;
                newAnswers.QuestionnaireId = answers.QuestionnaireId;
                newAnswers.CreationDate = DateTime.Now;

                context.QuestionnaireAnswers.Add(newAnswers);

                Users user = context.Users.SingleOrDefault(x => x.Id == answers.UserId);
                user.Wincoins += questionnaire.Wincoins;

                context.SaveChanges();

                return answers;
            }
        }
    }
}