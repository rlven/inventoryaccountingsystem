﻿using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Common;

namespace BusinessLogicLayer.Managers
{
    internal static class NotificationHelper
    {
        public static string SendNotification(string text)
        {
            if (WebRequest.Create("https://onesignal.com/api/v1/notifications") is HttpWebRequest request)
            {
                request.KeepAlive = true;
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                request.Headers.Add("authorization", "Basic YjgxOWM0MTItYjU1Ny00MTdhLTg0YjUtYmIxYTc2NzZhODIz");

                var serializer = new JavaScriptSerializer();
                var obj = new
                {
                    app_id = "e002a2f7-0c24-4e93-bc39-40cf3ae785e2",
                    contents = new {en = "English Message"},
                    included_segments = new[] {"All"}
                };
                var param = serializer.Serialize(obj);
                byte[] byteArray = Encoding.UTF8.GetBytes(param);

                try
                {
                    using (var writer = request.GetRequestStream())
                    {
                        writer.Write(byteArray, 0, byteArray.Length);
                    }

                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            var responseContent = reader.ReadToEnd();
                            return responseContent;
                        }
                    }
                }
                catch (WebException ex)
                {
                    return ex.Message;
                }
            }
            return Resources.UnknownError;
        }
    }
}
