﻿using System.Linq;
using Common.Models;
using DataAccessLayer;

namespace StayTrueService.BusinessLogic.Managers
{
    internal class PushManager
    {
        public void AddSubscription(PushSubscribtion subscribtion)
        {
            using (var context = new StayTrueDBEntities())
            {
                var subscriber = context.PushSubscribtions.FirstOrDefault(s => s.PlayerId == subscribtion.PlayerId);
                if(subscriber == null)
                {
                    var pushSubscription = new PushSubscribtions
                    {
                        UserId = subscribtion.UserId,
                        PlayerId = subscribtion.PlayerId
                    };

                    context.PushSubscribtions.Add(pushSubscription);
                    context.SaveChanges();
                } else if (subscriber.UserId != subscribtion.UserId)
                {
                    subscriber.UserId = subscribtion.UserId;
                    context.SaveChanges();
                }
            }
        }
    }
}