﻿using System;
using System.Linq;
using Common;
using Common.Models;
using DataAccessLayer;

namespace BusinessLogicLayer.Managers
{
    internal class LikeManager
    {
        public int GetLikeAmount(int newsId, StayTrueDBEntities context)
        {
            return context.Likes.Count(x => x.NewsId == newsId);
        }

        public bool CheckUser(int userId, int newsId)
        {
            using (var context = new StayTrueDBEntities())
            {
                return context.Likes.Any(x => x.NewsId == newsId && x.UserId == userId);
            }
        }

        public Like AddLike(int userId, int newsId)
        {
            using (var context = new StayTrueDBEntities())
            {
                Like like = new Like();

                if (context.Likes.Any(l => l.UserId == userId && l.NewsId == newsId))
                { 
                    like.ErrorMessage = Resources.AlreadyLiked;
                }
                else
                {
                    context.Likes.Add(new Likes() { UserId = userId, NewsId = newsId });
                    context.SaveChanges();

                    like.UserId = userId;
                    like.NewsId = newsId;
                }

                return like;
            }
        }
    }
}
