﻿using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Common.Models;
using DataAccessLayer;
using StayTrueService.Behaviors;
using StayTrueService.BusinessLogic.Managers;
using StayTrueService.Models;
using SwaggerWcf.Attributes;
using InvitedUsers = Common.Models.InvitedUsers;
using PartyQuestionnaireAnswer = Common.Models.PartyQuestionnaireAnswer;
using QuestionnaireAnswers = Common.Models.QuestionnaireAnswers;

namespace StayTrueService
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IService
    {
        int UserId { get; set; }

        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
        string Ping();

        [SwaggerWcfPath("Get chat auth", "Getting chat authentication")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetChatAuth();

        [SwaggerWcfPath("Log out", "Log out user")]
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        void LogOut();

        [SwaggerWcfPath("Questionnaires", "Questionnaires")]
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        Questionnaire Questionnaires();

        [SwaggerWcfPath("Validate promo", "Validating promo code")]
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Promo ValidatePromo(int promocode);

        [SwaggerWcfPath("Questionnaire answers", "Answers of questionnaires")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        QuestionnaireAnswers QuestionnaireAnswers(QuestionnaireAnswers questionnaire);

        [SwaggerWcfPath("Chat groups", "Getting chat groups")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ChatGroup> ChatGroups();

        [SwaggerWcfPath("Like", "Like")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Like Like(Like like);

        [SwaggerWcfPath("Get main news", "Get main news")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<NewsModel> GetMainNews(int category);

        [SwaggerWcfPath("Get all news", "Get all news")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<NewsModel> GetAllNews(int categoryId, int typeId, int skip, int take);

        [SwaggerWcfPath("Phone is exist", "Phone is exist")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        IsExistModel PhoneIsExist(string phone);

        [SwaggerWcfPath("Get product shop", "Get product shop")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<Product> GetProductShop(int categoryId);

        [SwaggerWcfPath("Get banners", "Get banners")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<Banner> GetBanners();

        [SwaggerWcfPath("Registration", "Register user")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Account RegisterUser(User user);

        [SwaggerWcfPath("Edit user profile", "Edit user profile")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        User EditUserProfile(User user);

        [SwaggerWcfPath("Authorize", "Authorization user")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizationOperationBehavior]
        Account AuthorizationUser(UserAuth user);

        [SwaggerWcfPath("AuthorizeApp", "Authorization userApp")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizationOperationBehavior]
        Account AuthorizationUserApp(User user);

        [SwaggerWcfPath("User info", "User info")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        Account UserInfo();

        [SwaggerWcfPath("Get user wincoins", "Get user wincoins")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        int GetUserWincoins();

        [SwaggerWcfPath("Change password", "Change User Password")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ChangeUserPassword(User user);

        [SwaggerWcfPath("Get avatar image", "Get avatar image")]
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        Stream GetAvatarImage(int userId);

        [SwaggerWcfPath("Edit avatar image", "Edit avatar image")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebInvoke(RequestFormat = WebMessageFormat.Json)]
        void EditAvatarImage(EditAvatarModel editAvatar);

        [SwaggerWcfPath("Save game result", "Save game result")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GameScore SaveGameResult(GameScore score);

        [SwaggerWcfPath("Get game history", "Get game history")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<UserGameHistory> GetGameHistory();

        [SwaggerWcfPath("Get purchase history", "Get purchase history")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<UserPurchaseHistory> GetPurchaseHistory();

        [SwaggerWcfPath("Get best scores", "Get best scores")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<GameScore> GetBestScores(int gameId, int skip, int take);

        [SwaggerWcfPath("QuizBestScores", "QuizBestScores")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<QuizScore> QuizBestScores(int quizId);

        [SwaggerWcfPath("GetUserBestScore", "GetUserBestScore")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GameScore GetUserBestScore(int gameId);

        [SwaggerWcfPath("GreatBattleUserRegistration", "GreatBattleUserRegistration")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleUserRegistrationInfo GreatBattleUserRegistration();

        [SwaggerWcfPath("GreatBattleDetails", "GreatBattleDetails")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleDetails GreatBattleDetails();

        [SwaggerWcfPath("GreatBattleUserRegistrationAnswer", "GreatBattleUserRegistrationAnswer")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleUserRegistrationAnswer GreatBattleUserRegistrationAnswer(GreatBattleUserRegistrationAnswer registerUser);

        [SwaggerWcfPath("GreatBattleChangeUserSide", "GreatBattleChangeUserSide")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleUserSide GreatBattleChangeUserSide(GreatBattleUserSide userSide);

        [SwaggerWcfPath("Save GreatBattle game result", "Save GreatBattle game result")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleGameScore GreatBattleGameResult(GreatBattleGameScore score);

        [SwaggerWcfPath("Save quizz session", "Save quizz session")]
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleQuizResult GreatBattleQuizSession(GreatBattleQuizResult quizSession);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GreatBattleQuizResult GetGreatBattleQuizSession(int tourId);

        [SwaggerWcfPath("Get all game history", "Get all game history")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<UserGameHistory> GetAllGameHistory();

        [SwaggerWcfPath("Validate user", "User validation")]
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string ValidateUser(User user);

        [SwaggerWcfPath("Support request", "Supporting request")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string SupportRequest(SupportRequestModel sr);

        [SwaggerWcfPath("Get support request", "Get support request")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<SupportRequest> GetSupportRequests(int page);

        [SwaggerWcfPath("Get all cities", "Get all cities")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<City> GetAllCities();

        [SwaggerWcfPath("Get shops", "Get shops")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<Shop> GetShops();

        [SwaggerWcfPath("Get product pages count", "Get product pages count")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        int GetProductPagesCount(int shopId, int quantity);

        [SwaggerWcfPath("Buy product", "Buy product")]
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        BuyProduct BuyProduct(BuyProduct buyProduct);

        [SwaggerWcfPath("Get games", "Get games")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<Game> GetGames();

        [SwaggerWcfPath("Get quizzes", "Get quizzes")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<Quiz> GetQuizzes();

        [SwaggerWcfPath("Save quizz session", "Save quizz session")]
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        QuizSession SaveQuizSession(QuizSession quizSession);

        [SwaggerWcfPath("Get quizz session", "Get quizz session")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        QuizSession GetQuizSession(int quizId);

        [SwaggerWcfPath("Get games pages count", "Get game pages count")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        int GetGamesPagesCount();

        [SwaggerWcfPath("Get album", "Get album")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<Album> GetAlbum(bool isBrended, int skip, int take);

        [SwaggerWcfPath("Get album images", "Get album images")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<AlbumImage> GetAlbumImages(int albumId, bool isBrended, int skip, int take);

        [SwaggerWcfPath("Get all configurations", "Get all configurations")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<Config> GetAllConfigurations();

        [SwaggerWcfPath("Term of use", "Term of use")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        TermOfUse TermOfUse(string appType);

        [SwaggerWcfPath("Rules of use", "Rules of use")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        RulesOfUse RulesOfUse(string appType);

        [SwaggerWcfPath("Add user answer", "Add user answer")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        UserSurvey AddUserAnswer();

        [SwaggerWcfPath("Add survey gift", "Add survey gift")]
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        SurveyGift AddSurveyGift(SurveyGift surveyGift);

        [SwaggerWcfPath("Get survey gift history", "Get survey gift history")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<SurveyGiftHistory> GetSurveyGiftsHistory(int userId);

        [SwaggerWcfPath("Get all cigarette brands", "Get all cigarette brands")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<CigaretteBrand> GetAllCigaretteBrands();

        [SwaggerWcfPath("Get cigarette brand", "Get cigarette brand")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        CigaretteBrand GetCigaretteBrand(int id);


        [SwaggerWcfPath("Get all cigarette brands", "Get all cigarette brands")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string[]> GetAllCigaretteModels();

        [SwaggerWcfPath("PartyDetails", "PartyDetails")]
        [OperationContract]
        [AuthorizedOperationBehavior]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        PartyDetails PartyDetails();

        [SwaggerWcfPath("PartyUserRegistrationAnswer", "PartyUserRegistrationAnswer")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        PartyQuestionnaireAnswer PartyUserRegistrationAnswer(PartyQuestionnaireAnswer partyQuestionnaireAnswer);

        [SwaggerWcfPath("SavePartyGameResult", "SavePartyGameResult")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        GameScore SavePartyGameResult(GameScore score);


        [SwaggerWcfPath("Get quizz session", "Get quizz session")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        QuizSession GetPartyQuizSession(int quizzId);

        [SwaggerWcfPath("SavePartyQuizzSession", "SavePartyQuizzSession")]
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        QuizSession SavePartyQuizzSession(QuizSession quizSession);

        [SwaggerWcfPath("Subscribe", "Subscribe")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        string Subscribe(PushSubscribtion subscribtion);

        [SwaggerWcfPath("CheckPartyStatus", "CheckPartyStatus")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        PartyUser CheckPartyStatus(int id);

        [SwaggerWcfPath("ChangePartyUserHasComeStatus", "ChangePartyUserHasComeStatus")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PartyUser ChangePartyUserHasComeStatus(PartyUser partyUser);

        [SwaggerWcfPath("SavePvpSessionRemoveWincoins", "SavePvpSessionRemoveWincoins")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json)]
        void SavePvpSessionRemoveWincoins(List<PvpGameSessionScore> scores);

        [SwaggerWcfPath("SavePvpSessionAddWincoins", "SavePvpSessionAddWincoins")]
        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json)]
        void SavePvpSessionAddWincoins(List<PvpGameSessionScore> scores);

        [SwaggerWcfPath("Get pvp games", "Get pvp games")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<Game> GetPvpGames();

        [SwaggerWcfPath("Get pvp game", "Get pvp game")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        PvpGame GetPvpGame(int gameId);

        [SwaggerWcfPath("GetPvpGamesPagesCount", "GetPvpGamesPagesCount")]
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        int GetPvpGamesPagesCount();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        MobileVersion GetCurrentMobileVersions();

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        QuizzAnswer SaveQuizzSessionAnswer(QuizzAnswer answer);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        List<InvitedUsers> GetInvitedUsers();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        LotteryDetails GetLotteryDetails();

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AuthorizedOperationBehavior]
        BuyLotteryTicket BuyLotterTicket(BuyLotteryTicket ticket);

        //     webGet - Get
        //     WebInvoke - Post
        //     [AuthorizedOperationBehavior] - Required Token
        //     [AuthorizationOperationBehavior] - Generate Token 
    }
}
