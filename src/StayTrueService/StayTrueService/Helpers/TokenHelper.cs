﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using BusinessLogicLayer;
using Common.Logger;

namespace StayTrueService.Helpers
{
    public enum TokenValidationResult
    {
        Valid,
        NotProvided,
        Expired,
        Invalid
    }
    public class TokenHelper
    {
        private readonly double _tokenTime;
        private readonly RSACryptoServiceProvider _rsaProvider;

        public TokenHelper(double tokenTime = 30)
        {
            var keys = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent><P>/aULPE6jd5IkwtWXmReyMUhmI/nfwfkQSyl7tsg2PKdpcxk4mpPZUdEQhHQLvE84w2DhTyYkPHCtq/mMKE3MHw==</P><Q>3WV46X9Arg2l9cxb67KVlNVXyCqc/w+LWt/tbhLJvV2xCF/0rWKPsBJ9MC6cquaqNPxWWEav8RAVbmmGrJt51Q==</Q><DP>8TuZFgBMpBoQcGUoS2goB4st6aVq1FcG0hVgHhUI0GMAfYFNPmbDV3cY2IBt8Oj/uYJYhyhlaj5YTqmGTYbATQ==</DP><DQ>FIoVbZQgrAUYIHWVEYi/187zFd7eMct/Yi7kGBImJStMATrluDAspGkStCWe4zwDDmdam1XzfKnBUzz3AYxrAQ==</DQ><InverseQ>QPU3Tmt8nznSgYZ+5jUo9E0SfjiTu435ihANiHqqjasaUNvOHKumqzuBZ8NRtkUhS6dsOEb8A2ODvy7KswUxyA==</InverseQ><D>cgoRoAUpSVfHMdYXW9nA3dfX75dIamZnwPtFHq80ttagbIe4ToYYCcyUz5NElhiNQSESgS5uCgNWqWXt5PnPu4XmCXx6utco1UVH8HGLahzbAnSy6Cj3iUIQ7Gj+9gQ7PkC434HTtHazmxVgIR5l56ZjoQ8yGNCPZnsdYEmhJWk=</D></RSAKeyValue>"; 
            _tokenTime = tokenTime;
            _rsaProvider = new RSACryptoServiceProvider(1024);
            _rsaProvider.FromXmlString(keys);
        }

        public string GenerateToken(string code, DateTime timestamp)
        {
            var message = $"{code}+{timestamp.ToFileTime()}";
            var mod = message.Length % 4;
            if (mod > 0)
            {
                message += new string('+', 4 - mod);
            }
            var token = Convert.ToBase64String(_rsaProvider.Encrypt(Convert.FromBase64String(message), true));
            BusinessLogicProvider bll = new BusinessLogicProvider();
            bll.SendAuthToken(int.Parse(code), token, false);
            return token;
        }

        public TokenValidationResult ValidateToken(string token, out int code)
        {
            code = 0;

            if (string.IsNullOrWhiteSpace(token))
            {
                return TokenValidationResult.NotProvided;
            }

            try
            {
                var resp = Convert.ToBase64String(_rsaProvider.Decrypt(Convert.FromBase64String(token), true));
                var splitted = resp.Split(new[] { '+' }, StringSplitOptions.RemoveEmptyEntries);

                code = Int32.Parse(splitted[0]);

                KeyValuePair<string, DateTime> tokenKeyValue = Service.TokensStore.SingleOrDefault(store => store.Key == token);

                if (tokenKeyValue.Key is null)
                {
                    return TokenValidationResult.Invalid;
                }

                var timespan = tokenKeyValue.Value.Subtract(DateTime.Now);

                if (Math.Abs(timespan.TotalMinutes) > _tokenTime)
                {
                    return TokenValidationResult.Expired;
                }
                else
                {
                    Service.TokensStore.Remove(tokenKeyValue);
                    Service.TokensStore.Add(new KeyValuePair<string, DateTime>(token, DateTime.Now));

                    return TokenValidationResult.Valid;
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                return TokenValidationResult.Invalid;
            }
        }
    }
}