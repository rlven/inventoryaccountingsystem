﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using BusinessLogicLayer;
using BusinessLogicLayer.Managers;
using Common.Models;
using Common.Security;
using DataAccessLayer;
using Newtonsoft.Json;
using NLog.Fluent;
using StayTrueService.BusinessLogic.Managers;
using StayTrueService.Models;
using SwaggerWcf.Attributes;
using Unity;
using InvitedUsers = Common.Models.InvitedUsers;
using Log = Common.Logger.Log;
using QuestionnaireAnswers = Common.Models.QuestionnaireAnswers;

namespace StayTrueService
{
    [SwaggerWcf("/api/Service.svc")]
    public class Service : IService
    {
        public int UserId { get; set; }
        private static readonly string SecureKey = "f1c188fe-8024-4663-960d-197054d9658e";
        private static readonly string PhotonSecureKey = "42531bdb-0ba3-4919-9c22-c74ca45a0457";

        private readonly BusinessLogicProvider businessLogicProvider;

        public static List<KeyValuePair<string, DateTime>> TokensStore = new List<KeyValuePair<string, DateTime>>(); 

        public Service()    
        {
            businessLogicProvider = DependencyFactory.Container.Resolve<BusinessLogicProvider>();
        }

        public string Ping()
        {
            return "ping";
        }
            
        public Promo ValidatePromo(int promocode)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                if (new UserManager().UserIsExist(promocode))
                {
                    return new Promo {Promocode = promocode };
                }
                else
                {
                    return new Promo { ErrorMessage = "Промокод не найден" };
                }
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public void LogOut()
        {
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;

            string applicationheader = woc.Headers["Authorization"].Split(' ')[1];

            var token = TokensStore.FirstOrDefault(t => t.Key == applicationheader);

            TokensStore.Remove(token);
        }

        public Questionnaire Questionnaires()
        {

            if (GetHeaderValue().Equals(SecureKey))
            {
                return new QuestionnaireManager().GetQuestionnaires(UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public QuestionnaireAnswers QuestionnaireAnswers(QuestionnaireAnswers questionnaire)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                questionnaire.UserId = UserId;
                return new QuestionnaireManager().SaveQuestionnaireAnswers(questionnaire);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public List<Banner> GetBanners()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string folderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
                return businessLogicProvider.GetBanners();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }


        public TermOfUse TermOfUse(string appType)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetTermOfUse(appType);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }


        public RulesOfUse RulesOfUse(string appType)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetRulesOfUse(appType);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Like Like(Like like)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new LikeManager().AddLike(UserId, like.NewsId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }

        }

        public List<Product> GetProductShop(int categoryId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new ShopManager().GetProductShop(categoryId, UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public Banner AddBanner(Banner banner)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string apPath = $"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}/Image/";
                return businessLogicProvider.AddBanner(banner, apPath);
            }
            throw new InvalidOperationException("Invalid Secure key");
        }

        public Banner EditBanner(Banner banner)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string apPath = $"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}/Image/";
                return businessLogicProvider.EditBanner(banner, apPath);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public string DeleteBanner(Banner banner)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.DeleteBanner(banner);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Account RegisterUser(User user)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().RegisterUser(user);
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }


        public IsExistModel PhoneIsExist(string phone)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().PhoneIsExist(phone);
            }
            throw new InvalidOperationException("Invalid Secure Key");
            
        }

        public System.IO.Stream GetAvatarImage(int userId)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                string avatar = businessLogicProvider.GetAvatarImage(userId);
                byte[] resultBytes = Encoding.UTF8.GetBytes(avatar);
                return new MemoryStream(resultBytes);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public User EditUserProfile(User user)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                user.Id = UserId;
                return businessLogicProvider.EditUserProfile(user);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public string ChangeUserPassword(User user)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.ChangeUserPassword(user);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Account AuthorizationUser(UserAuth user)
        {

            if (GetHeaderValue().Equals(SecureKey))
            {
                var application = GetApplicationHeaderValue();
                return new UserManager().AuthorizationUser(user, application);
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Account AuthorizationUserApp(User user)
        {

            if (GetHeaderValue().Equals(SecureKey))
            {
                var application = GetApplicationHeaderValue();
                return new UserManager().AppAuthorizationUser(user, application);
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public string ValidateUser(User user)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.ValidateUser(user);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public string SupportRequest(SupportRequestModel supportRequest)
        { 
            if (GetHeaderValue().Equals(SecureKey))
            {
                return businessLogicProvider.SupportRequest(supportRequest);
            }
            else
            {
                throw new InvalidOperationException("Invalid secure key");
            }
        }

        public List<SupportRequest> GetSupportRequests(int page)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetSupportRequests(page);
            }
            throw new InvalidOperationException("Invalid secure key");
        }

        public List<Shop> GetShops()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetShops();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public City AddCity(City city)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.AddCity(city);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public GameScore SaveGameResult(GameScore score)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                string scoreJson = AesCrypto.DecryptStringAES(UserId.ToString(), score.Result);

                var gameScore = JsonConvert.DeserializeObject<GameScore>(scoreJson);
                gameScore.Result = score.Result;
                gameScore.Points = gameScore.Score;
                gameScore.UserId = UserId;
                return new UserManager().AddGameResult(gameScore);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }
        
        public GreatBattleQuizResult GreatBattleQuizSession(GreatBattleQuizResult quizSession)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                string score = AesCrypto.DecryptStringAES(UserId.ToString(), quizSession.Result);

                quizSession = JsonConvert.DeserializeObject<GreatBattleQuizResult>(score);

                quizSession.UserId = UserId;

                return new GreatBattleManager().SaveQuizResult(quizSession);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }

        }

        public GreatBattleGameScore GreatBattleGameResult(GreatBattleGameScore gameScore)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                string score = AesCrypto.DecryptStringAES(UserId.ToString(), gameScore.Result);

                gameScore = JsonConvert.DeserializeObject<GreatBattleGameScore>(score);
                gameScore.Points = Int32.Parse(gameScore.Score.ToString());
                gameScore.UserId = UserId;

                return new GreatBattleManager().SaveGameResult(gameScore);
 
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public List<UserGameHistory> GetGameHistory()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().GameHistory(UserId, GetApplicationHeaderValue());
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public List<UserPurchaseHistory> GetPurchaseHistory()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().PurchaseHistory(UserId);
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public List<GameScore> GetBestScores(int gameId, int skip, int take)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().GetBestScores(UserId, gameId, skip, take);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public List<QuizScore> QuizBestScores(int quizId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new QuizManager().GetBestScores(UserId, quizId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public GameScore GetUserBestScore(int gameId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().UserBestScore(gameId, UserId); 
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public GreatBattleUserSide GreatBattleChangeUserSide(GreatBattleUserSide userSide)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                userSide.UserId = UserId;
                return new GreatBattleManager().UserChangeSide(userSide);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public GreatBattleUserRegistrationInfo GreatBattleUserRegistration()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GreatBattleManager().GetUserRegistrationInfo(UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public GreatBattleDetails GreatBattleDetails()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GreatBattleManager().GetGreatBattleDetails(UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

    /*    public string GetBattleResult()
        {
            if (GetHeaderValue().Equals(SecureKey) && GetBattleHeaderValue().Equals(BattleKey))
            {
                return new GreatBattleManager().GetBattleWinnerSide();
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }*/

    /*    public string GetTourResult()
        {
            if (GetHeaderValue().Equals(SecureKey) && GetBattleHeaderValue().Equals(BattleKey))
            {
                return new GreatBattleManager().GetTourResults();
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }*/

        public GreatBattleUserRegistrationAnswer GreatBattleUserRegistrationAnswer(GreatBattleUserRegistrationAnswer registerUser)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                registerUser.UserId = UserId;

                return new GreatBattleManager().RegisterUser(registerUser);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public List<Game> GetGames()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GameManager().GetGames();
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public List<Quiz> GetQuizzes()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new QuizManager().GetQuizzes();
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public QuizSession GetQuizSession(int quizId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new QuizManager().GetQuizSession(quizId, UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }
        public QuizSession GetPartyQuizSession(int quizzId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new QuizManager().GetPartyQuizSession(quizzId, UserId); 
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }

        }

        public GreatBattleQuizResult GetGreatBattleQuizSession(int tourId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GreatBattleManager().GetQuizSession(tourId, UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public QuizSession SaveQuizSession(QuizSession quizSession)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                string score = AesCrypto.DecryptStringAES(UserId.ToString(), quizSession.Result);

                quizSession = JsonConvert.DeserializeObject<QuizSession>(score);

                quizSession.UserId = UserId;

                return new QuizManager().SaveQuizSession(quizSession);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public void EditAvatarImage(EditAvatarModel editAvatar)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                new UserManager().EditAvatar(editAvatar, UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public int GetGamesPagesCount()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetGamesPagesCount();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public int GetProductPagesCount(int shopId, int quantity)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetProductPagesCount(shopId, quantity);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Account UserInfo()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().UserInfo(UserId);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }
        
        public AlbumImage AddImageInAlbum(AlbumImage ai)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string apPath = $"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}/Image/";
                return businessLogicProvider.AddImageInAlbum(ai, apPath);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Album AddCategoryAlbum(Album album)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string apPath = $"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}/Image/";
                return businessLogicProvider.AddCategoryAlbum(album, apPath);
            }
            throw new InvalidOperationException("Invalid Secure Key");

        }

        public List<ChatGroup> ChatGroups()
        {
            return new ChatManager().GetChatGroupsByUserId(UserId);
        }

        public List<AlbumImage> GetAlbumImages(int albumId, bool isBrended, int skip, int take)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new AlbumManager().GetAlbumImages(albumId, isBrended, skip, take);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public AlbumImage EditImageInAlbum(AlbumImage ai)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string apPath = $"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}/Image/";
                return businessLogicProvider.EditImageInAlbum(ai, apPath);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Stream GetUserFrontPassport(int id)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpg";
                return businessLogicProvider.GetUserFrontPassport(id);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Stream GetUserBackPassport(int id)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetUserBackPassport(id);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Album EditAlbum(Album album)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                string apPath = $"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}/Image/";
                return businessLogicProvider.EditAlbum(album, apPath);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public List<Album> GetAlbum(bool isBrended, int skip, int take)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new AlbumManager().GetAlbum(isBrended, skip, take);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public string DeleteImageInAlbum(AlbumImage ai)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.DeleteImageInAlbum(ai);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public string DeleteCategory(Album album)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.DeleteCategory(album);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }


        public List<NewsModel> GetAllNews(int categoryId, int typeId, int skip, int take)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new NewsManager().GetAllNews(categoryId, typeId, UserId, skip, take);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
            
        }

        public List<City> GetAllCities()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetAllCities();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        private string GetHeaderValue()
        {
            IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
            WebHeaderCollection headers = request.Headers;
            return headers["SecureKey"] ?? "";
        }
        private string GetBattleHeaderValue()
        {
            IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
            WebHeaderCollection headers = request.Headers;
            return headers["BattleKey"] ?? "";
        }

        private string GetApplicationHeaderValue()
        {
            IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
            WebHeaderCollection headers = request.Headers;
            return headers["ApplicationType"] ?? "";
        }

        public Config AddConfigurations(Config config)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.AddConfigurations(config);
            }
            throw new InvalidOperationException("Invalid secure key");
        }

        public Config EditConfigurations(Config config)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.EditConfigurations(config);
            }
            throw new InvalidOperationException("Invalid secure key");
        }


        public string DeleteConfigurations(Config config)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.DeleteConfigurations(config);
            }
            throw new InvalidOperationException("Invalid secure key");
        }

        public List<Config> GetAllConfigurations()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetAllConfigurations();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public int GetUserWincoins()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return businessLogicProvider.GetWincoin(UserId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public BuyProduct BuyProduct(BuyProduct buyProduct)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                buyProduct.UserId = UserId;
                return new ShopManager().BuyProduct(buyProduct);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }           
        }

        public List<UserGameHistory> GetAllGameHistory()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                int userId = UserId;
                return new UserManager().GameHistory(userId, GetApplicationHeaderValue());
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public List<CigaretteBrand> GetAllCigaretteBrands()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new CigaretteBrandsManager().GetAllBrands();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public CigaretteBrand GetCigaretteBrand(int id)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new CigaretteBrandsManager().GetBrand(id);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Dictionary<string, string[]> GetAllCigaretteModels()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new CigaretteBrandsManager().GetAllModels();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public PartyDetails PartyDetails()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                int userId = UserId;
                return new PartyManager().GetPartyDetails(userId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public Common.Models.PartyQuestionnaireAnswer PartyUserRegistrationAnswer(Common.Models.PartyQuestionnaireAnswer partyQuestionnaireAnswer)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                int userId = UserId;
                return new PartyManager().PartyRegistrationAnswer(partyQuestionnaireAnswer, userId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        #region Survey

        public SurveyGift AddSurveyGift(SurveyGift surveyGift)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new SurveyGift();
            }
            throw new InvalidOperationException("Invalid secure key");
        }

        public SurveyGift DeleteSurveyGift(SurveyGift surveyGift)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new SurveyGift();
            }
            throw new InvalidOperationException("Invalid secure key");
        }

        public UserSurvey AddUserAnswer()
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new UserSurvey();
            }
            throw new InvalidOperationException("Invalid secure key");
        }

        public List<SurveyGiftHistory> GetSurveyGiftsHistory(int userId)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {

                return new List<SurveyGiftHistory>();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public string GetChatAuth()
        {
            string login = ConfigurationManager.AppSettings["ChatLogin"];
            string password = ConfigurationManager.AppSettings["ChatPassword"];
            return "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", login, password)));
        }



        public List<Genders> GetGenders()
        {
            return new GenderManager().GetGenders();
        }

        public List<NewsModel> GetMainNews(int category)
        {
            throw new NotImplementedException();
        }

        public List<NewsModel> GetAllNews()
        {
            throw new NotImplementedException();
        }

        public GameScore SavePartyGameResult(GameScore gameScore)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                string score = AesCrypto.DecryptStringAES(UserId.ToString(), gameScore.Result);

                gameScore = JsonConvert.DeserializeObject<GameScore>(score);
                gameScore.Points = Int32.Parse(gameScore.Score.ToString());
                gameScore.UserId = UserId;

                return new UserManager().SavePartyGameResult(gameScore);

            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }

        }
        public QuizSession SavePartyQuizzSession(QuizSession quizSession)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                string score = AesCrypto.DecryptStringAES(UserId.ToString(), quizSession.Result);

                quizSession = JsonConvert.DeserializeObject<QuizSession>(score);

                quizSession.UserId = UserId;

                return new QuizManager().SavePartyQuizzSession(quizSession);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }

        }

        public string Subscribe(PushSubscribtion subscribtion)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                subscribtion.UserId = UserId;
                new PushManager().AddSubscription(subscribtion);
                return subscribtion.UserId.ToString();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }
        #endregion

        public PartyUser CheckPartyStatus(int id)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {                
                return new PartyManager().CheckPartyUserStatus(id);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public PartyUser ChangePartyUserHasComeStatus(PartyUser partyUser)
        {
            var key = GetHeaderValue();
            if (key.Equals(SecureKey))
            {
                return new PartyManager().ChangePartyUserHasComeStatus(partyUser.UserId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public void SavePvpSessionAddWincoins(List<PvpGameSessionScore> scores)
        {
            if (GetHeaderValue().Equals(PhotonSecureKey))
            {
                new GameManager().PvpSessionTransferWincoins(scores);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public void SavePvpSessionRemoveWincoins(List<PvpGameSessionScore> scores)
        {
            if (GetHeaderValue().Equals(PhotonSecureKey))
            {
                new GameManager().PvpSessionWithdrawWincoins(scores);
            }
            else
            {
                throw new InvalidOperationException("Invalid Secure Key");
            }
        }

        public List<Game> GetPvpGames()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GameManager().GetPvpGames();
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public PvpGame GetPvpGame(int gameId)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GameManager().PvpGame(gameId);
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public int GetPvpGamesPagesCount()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new GameManager().GetPvpGamesPagesCount();
            }

            throw new InvalidOperationException("Invalid Secure Key");
        }

        public MobileVersion GetCurrentMobileVersions()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new ConfigurationsManager().GetMobileVersions();
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public QuizzAnswer SaveQuizzSessionAnswer(QuizzAnswer answer)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new QuizManager().SaveQuestionnaireQuizzAnswer(answer, UserId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public List<InvitedUsers> GetInvitedUsers()
        {
            var apptype = GetApplicationHeaderValue();
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new UserManager().GetInvitedUsers(UserId, apptype);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public LotteryDetails GetLotteryDetails()
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new LotteryManager().GetLotteryDetails(UserId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }

        public BuyLotteryTicket BuyLotterTicket(BuyLotteryTicket ticket)
        {
            if (GetHeaderValue().Equals(SecureKey))
            {
                return new LotteryManager().BuyLotteryTicket(ticket, UserId);
            }
            throw new InvalidOperationException("Invalid Secure Key");
        }
    }
}
