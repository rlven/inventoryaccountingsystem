﻿using System;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Timers;
using System.Web;
using System.Web.Routing;
using Common.Logger;
using Common.Models;
using StayTrueService.BusinessLogic.Extensions;
using StayTrueService.BusinessLogic.Managers;
using SwaggerWcf;

namespace StayTrueService
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.Add(new ServiceRoute("api-docs", new WebServiceHostFactory(), typeof(SwaggerWcfEndpoint)));
            RouteTable.Routes.Add(new ServiceRoute("api/Service.svc", new WebServiceHostFactory(), typeof(Service)));

            Timer timer = new Timer();

            timer.Elapsed += new ElapsedEventHandler(WCFService_Elapsed);
            timer.Interval = 10 * 60 * 1000; //10 min
            timer.Enabled = true;
            timer.Start();
           
            Timer timer2 = new Timer();

            timer2.Elapsed += new ElapsedEventHandler(GreatBattleMonitoring);
            timer2.Interval = 5 * 60 * 1000; //5 min
            timer2.Enabled = true;
            timer2.Start();

            Timer timer3 = new Timer();

            timer3.Elapsed += new ElapsedEventHandler(GetAllOperatorCodes);
            timer3.Interval = 1440 * 60 * 1000;//1 day
            timer3.Enabled = true;
            timer3.Start();

            Timer timer4 = new Timer();

            timer3.Elapsed += new ElapsedEventHandler(UserFirstSignInGift);
            timer3.Interval = 5 * 60 * 1000;//1 day
            timer3.Enabled = true;
            timer3.Start();
        }

        void GreatBattleMonitoring(object sender, ElapsedEventArgs e)
        {
            try
            {
                new GreatBattleManager().CheckBattleExpirationDate();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }

        void GetAllOperatorCodes(object sender, ElapsedEventArgs e)
        {
            try
            {
                BallancePaymentService.SaveAllOperatorCodes();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        void UserFirstSignInGift(object sender, ElapsedEventArgs e)
        {
            try
            {
                BallancePaymentService.UserFirstSignInGift();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        void WCFService_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Log.Info("WCFService_Elapsed. Count: " + Service.TokensStore.Count);

                foreach (var token in Service.TokensStore)
                {
                    var timespan = token.Value.Subtract(DateTime.Now);

                    if (Math.Abs(timespan.TotalMinutes) > 60)
                    {
                        Service.TokensStore.Remove(token);
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }


        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
                
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (exception != null)
            {
                Log.Error(exception);
                Log.Error(exception.StackTrace);
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
          
        }

    }
}