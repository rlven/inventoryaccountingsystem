function myFunction(a, b) {
    var keyTeamplate = '9029384756473829';
    var keyValue = a + keyTeamplate;

    var key = CryptoJS.enc.Utf8.parse(keyValue.substring(0, 16));
    var iv = CryptoJS.enc.Utf8.parse(keyValue.split("").reverse().join("").substring(0, 16));
    var encrypted = CryptoJS.AES.encrypt(b, key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    return encrypted;
}

function checkId() {
    console.log('test');
    if (Cookies.get('Id') != undefined) {
        // alert('Это id с куки: ' + Cookies.get('Id'));
        return Cookies.get('Id');
    } else if (sessionStorage.getItem('Id') != undefined) {
        // alert('Это id с сессии: ' + sessionStorage.getItem('Id'));
        return sessionStorage.getItem('Id');
    } else {
        // alert('id не найден');
        window.location.replace("index.html");
        return false;
    }
}

function checkAuth() {
    console.log('test1');
    if (Cookies.get('Token') != undefined) {
        // alert('Это токен с куки: ' + Cookies.get('Id'));
        return Cookies.get('Token');
    } else if (sessionStorage.getItem('Token') != undefined) {
        // alert('Это токен с сессии: ' + sessionStorage.getItem('Id'));
        return sessionStorage.getItem('Token');
    } else {
        // alert('токен не найден');
        window.location.replace("index.html");
        return false;
    }
}

function getQuizOptions(url, method) {
    console.log('test3');
    if (url != null && method != null) {
        return {
            url: '../../../../Service.svc/' + url,
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'SecureKey': 'f1c188fe-8024-4663-960d-197054d9658e',
                'ApplicationType': 'web',
                'Authorization': 'Bearer ' + checkAuth()
            }
        };
    } else {
        return false;
    }
}