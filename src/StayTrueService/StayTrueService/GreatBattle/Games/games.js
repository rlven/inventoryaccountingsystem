function myFunction(a, b) {
    var keyTeamplate = '9029384756473829';
    var keyValue = a + keyTeamplate;

    var key = CryptoJS.enc.Utf8.parse(keyValue.substring(0, 16));
    var iv = CryptoJS.enc.Utf8.parse(keyValue.split("").reverse().join("").substring(0, 16));
    var encrypted = CryptoJS.AES.encrypt(b, key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    return encrypted;
}

function checkId() {
    if (Cookies.get('Id') != undefined) {
        return Cookies.get('Id');
    } else if (sessionStorage.getItem('Id') != undefined) {
        return sessionStorage.getItem('Id');
    } else {
        window.location.replace("index.html");
        return false;
    }
}