﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Meta
{
    public class QuestionMeta
    {
        [Display(Name = "Вопрос")]
        [Required(ErrorMessage = "Поле Вопрос обязательное")]
        [StringLength(4000, MinimumLength = 3, ErrorMessage = "Поле Вопрос должно содержать от 3 до 4000 символов")]
        public string Text { get; set; }

        [Display(Name = "Тип вопроса")]
        [Required(ErrorMessage = "Поле Тип вопроса обязательное")]
        public String AnswerType { get; set; }

        [Display(Name = "Тип ответа")]
        public List<String> Answers { get; set; }

        [Display(Name = "Количество ответов")]
        [Required(ErrorMessage = "Поле Количество ответов обязательное")]
        public int SizeOfAvailableAnswers { get; set; }
    }
}