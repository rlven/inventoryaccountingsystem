﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using Common.Models;

namespace StayTrueWebAdmin.Meta
{
    public class QuestionnaireMeta
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Поле Название обязательное")]
        [StringLength(4000, MinimumLength = 3, ErrorMessage = "Поле Название должно содержать от 3 до 4000 символов")]
        public string Name { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public List<Question> Questions { get; set; }

        [Display(Name = "Дата начала")]
        [Required(ErrorMessage = "Поле Дата начала обязательное")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Дата окончания")]
        [Required(ErrorMessage = "Поле Дата окончания обязательное")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Статус")]
        [Required(ErrorMessage = "Поле Статус обязательное")]
        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        [Display(Name = "Wincoins")]
        [Required(ErrorMessage = "Поле Wincoins обязательное")]
        public int Wincoins { get; set; }
    }
}