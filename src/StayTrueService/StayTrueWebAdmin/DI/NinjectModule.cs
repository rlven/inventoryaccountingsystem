﻿using StayTrueWebAdmin.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using Ninject.Planning.Bindings;
using Ninject.Web.Common;
using StayTrueWebAdmin.Repositories;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.DI
{
    public class NinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(DbContext)).To(typeof(ApplicationDbContext)).InRequestScope();
            Bind(typeof(IUserStore<ApplicationUser>)).To(typeof(CustomUserStore<ApplicationUser>)).InRequestScope();
            Bind(typeof(IRoleStore<IdentityRole, string>)).To(typeof(RoleStore<IdentityRole>)).InRequestScope();
            Bind(typeof(UserManager<ApplicationUser>)).ToSelf().InRequestScope();
            Bind(typeof(RoleManager<IdentityRole>)).ToSelf().InRequestScope();
            Bind<IAlbumsRepository>().To<AlbumsRepository>();
            Bind<IProductsRepository>().To<ProductsRepository>();
            Bind<IAdministratorRepository>().To<AdministratorRepository>(); 
            Bind<IShopsRepository>().To<ShopsRepository>();
            Bind<IAlbumImagesRepository>().To<AlbumImagesRepository>(); 
            Bind<IUsersRepository>().To<UsersRepository>();
            Bind<IGamesRepository>().To<GameRepository>();
            Bind<INewsRepository>().To<NewsRepository>();
            Bind<ISupportRepository>().To<SupportRepository>();
            Bind<INewsCategoryRepository>().To<NewsCategoryRepository>();
            Bind<IBlockUsersRepository>().To<BlockUsersRepository>();
            Bind<INewsTypeRepository>().To<NewsTypeRepository>();
            Bind<ICitiesRepository>().To<CitiesRepository>();
            Bind<IAccountStatusesRepository>().To<AccountStatusesRepository>();
            Bind<IRolesRepository>().To<RolesRepository>();
            Bind<IGendersRepository>().To<GendersRepository>();
            Bind<IProductsCategoriesRepository>().To<ProductsCategoriesRepository>();
            Bind<IPurchaseStatusesRepository>().To<PurchaseStatusesRepository>();
            Bind<IPurchaseHistoriesRepository>().To<PurchaseHistoriesRepository>();
            Bind<IChatTokensRepository>().To<ChatTokensRepository>();
            Bind<IQuizzesRepository>().To<QuizzesRepository>();
            Bind<IQuestionnairesRepository>().To<QuestionnairesRepository>();
            Bind<IChatGroupsRepository>().To<ChatGroupsRepository>();
            Bind<IChatPrivateGroupsUsersRepository>().To<ChatPrivateGroupsUsersRepository>();
            Bind<IQuestionnaireAnswersRepository>().To<QuestionnaireAnswersRepository>();
            Bind<IGreatBattlesRepository>().To<GreatBattlesRepository>();
            Bind<IGreatBattleQuestionnaireRepository>().To<GreatBattleQuestionnaireRepository>();
            Bind<IGreatBattleToursRepository>().To<GreatBattleToursRepository>();
            Bind<IGreatBattleMembersRepository>().To<GreatBattleMembersRepository>();
            Bind<IGreatBattleQuestionnaireAnswersRepository>().To<GreatBattleQuestionnaireAnswersRepository>();
            Bind<IGreatBattleTourPointsRepository>().To<GreatBattleTourPointsRepository>();
            Bind<ICigaretteBrandsRepository>().To<CigaretteBrandsRepository>();
            Bind<IPartyRepository>().To<PartyRepository>();
            Bind<IPartyMembersRepository>().To<PartyMembersRepository>();
            Bind<IPartyQuestionnaireRepository>().To<PartyQuestionnaireRepository>();
            Bind<IPartyGamesRepository>().To<PartyGamesRepository>();
            Bind<IPartyQuizzesRepository>().To<PartyQuizzesRepository>();
            Bind<ISmsTemplateRepository>().To<SmsTemplateRepository>();
            Bind<ICigarettePackSizesRepository>().To<CigarettePackSizesRepository>();
            Bind<IPushTemplatesRepository>().To<PushTemplatesRepository>();
            Bind<ISmsMessageRepository>().To<SmsMessageRepository>();
            Bind<IPushSubscribtionsRepository>().To<PushSubscriptionsRepository>();
            Bind<IPushNotificationsRepository>().To<PushNotificationsRepository>();
            Bind<ISmsMessageMsgRepository>().To<SmsMessageMsgRepository>();
            Bind<IPvpGamesRepository>().To<PvpGamesRepository>();
            Bind<IPvpGameBetsRepository>().To<PvpGameBetsRepository>();
            Bind<IQuizzSessionAnswerRepository>().To<QuizzSessionAnswerRepository>();
            Bind<IQuizzSessionRepository>().To<QuizzSessionRepository>();
            Bind<IUserBallanceUpHistory>().To<UserBallanceUpHistory>();
            Bind<IInvitedUsersRepository>().To<InvitedUsersRepository>();
            Bind<ILotteryRepository>().To<LotteryRepository>();
            Bind<ILotteryUsersRepository>().To<LotteryUsersRepository>();
            Bind<IMobileVersionRepository>().To<MobileVersionRepository>();
            Bind<IFortuneProductsRepository>().To<FortuneProductsRepository>();
            Bind<IFortunePurchasesHistoryRepository>().To<FortunePurchaseHistoryRepository>();
            Bind<IFortuneWheelRepository>().To<FortuneWheelRepository>();
            Bind<IFortuneWheelComponentsRepository>().To<FortuneWheelComponentsRepository>();
            Bind<ISmsStatusesRepository>().To<SmsMessageStatusesRepository>();
            Bind<IBannersRepository>().To<BannersRepository>();
        }
    }
}