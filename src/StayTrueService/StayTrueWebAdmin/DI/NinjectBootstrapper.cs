﻿using System.Web.Mvc;
using Ninject;

namespace StayTrueWebAdmin.DI
{
    public class NinjectBootstrapper
    {
        public static IKernel Kernel { get; private set; }
        public static void Initialize()
        {
            Kernel = new StandardKernel(new NinjectModule());
            DependencyResolver.SetResolver(new NinjectDependencyResolver(Kernel));
        }
    }
}