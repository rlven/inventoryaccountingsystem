function editWarning() {
    $.fancybox.open({
        src: '#edit-warning'
    });
    setTimeout(function () {
        $.fancybox.close({
            src: '#edit-warning'
        });

        $('.content').animate({
            scrollTop: $(".on-edit").offset().top
        }, 400);
    }, 800);
}


$(function () {
    $.fn.extend({
        toggleText: function (a, b) {
            return this.text(this.text() == b ? a : b);
        }
    });

    //--- Поля содержащие даты -----------------------------------------
    $(document).on('focus', '.datepicker', function () {
        $('.datepicker').not('.timepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-100:+0",
        });
    });

    $(document).on('focus', '.timepicker', function () {
        $('.timepicker').datetimepicker({
            controlType: 'select',
            oneLine: true,
            dateFormat: 'dd/mm/yy',
            timeFormat: 'HH:mm',
            yearRange: "-100:+0",
            changeMonth: true,
            changeYear: true,
        });
    });

    $(".sub-nav .active").closest('.nav-section').addClass('active');
    $(".sub-nav .active")
        .parent()
        .show();

    $(".nav-section span").click(function () {
        $(".sub-nav")
            .not($(this).next())
            .slideUp();

        $(this)
            .next()
            .slideToggle();
    });
    //--- Маски -----------------------------------------

    $('.mask_field_telephone').mask('000 000 00 00 00', {
        placeholder: "996 XXX XX XX XX"
    }).click(function () {
        $(this).val('996 ');
    }).keyup(function () {
        let numberAsArray = $(this).val().split(' ');
        if (numberAsArray.length < 2 && !$(this).val().match(/^996 .*$/))
            numberAsArray[0] = '996 ';
        $(this).val(numberAsArray.join(' '));
    });

    //--- Маски Конец -----------------------------------------

    //--- Селекты -----------------------------------------

    var scroll = 0;
    var scrollIn = 0;

    $('.content').scroll(function () {
        if ($(this).scrollLeft() > 0) {
            scroll = $(this).scrollLeft();
        }
    });

    $.fn.scrollFix = function () {
        scrollIn = scroll;
        $('.content').scrollLeft(scroll);
        scroll = scrollIn;
    };

    $('.st-selectize').selectize({
        sortField: 'value',
        allowEmptyOption: true,
        placeholder: $(this).attr('placeholder'),
        onFocus: function () {
            $(this).scrollFix();
        },
        onBlur: function () {
            $(this).scrollFix();
        },
        onDropdownClose: function () {
            $(this).scrollFix();
        },
        onDropdownOpen: function () {
            $(this).scrollFix();
        },
    });

    //--- Прикрепление картинки -----------------------------------------

    $(document).on('change', '.st-upload-input', function (event) {
        console.log(event);
        if (event.target.files[0] !== '' && !$(this).hasClass('gallery-upload')) {
            var url = URL.createObjectURL(event.target.files[0]);

            console.log($(this).val());

            if (event.target.files[0]['type'].includes("image")) {
                $(this).parent().css('background-image', 'url("' + url + '")');
                $(this).parent().find('a').attr('href', url);
            }

            if (event.target.files[0]['type'].includes("video")) {
                var videoId = $(this).parent().find('video').attr('id');
                $(this).parent().find('video').remove();
                $(this).parent().append('<video controls="false" id="' + videoId + '"><source src = "' + url + '" type = "video/mp4"></video>');
            }

            if ($(this).val() == '') {
                console.log($(this).val("empty"));
                $(this).parent().css('background-image', 'url(/Content/img/upload-plus.png) !important');
            }
        }
    });

    //--------------------------------------------
    //--------------------------------------------
    //--------------------------------------------

    // при клике на кнопку удалить
    $('.delete-btn').click(function (e) {

        // сохраняю значение datat-id в глобальный объект
        $.delete_id = $(this).attr('data-id');

        console.log($.delete_id);

        $('#delete-id-item').val($.delete_id);
    });

    $(document).on("click", ".reset-btn",
        function(e) {
            e.preventDefault();
            window.location = window.location.pathname;
        });

    $(document).on("click", ".reset-btn-bypath", function(e) {
        e.preventDefault();
        window.location = window.location.pathname + window.location.search;
    });
});