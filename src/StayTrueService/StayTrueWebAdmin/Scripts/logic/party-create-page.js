﻿jQuery(function ($) {
    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Party/DeleteConfirmed',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });
        });

    $(".delete-btn").on("click",
        function () {
            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });

    $(".edit-btn, .members-list-btn").on("click",
        function (e) {
            location.href = $(this).data("url");
        });

});