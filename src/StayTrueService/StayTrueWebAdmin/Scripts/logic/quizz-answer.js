﻿
$(document).on("click", ".edit-answer", function (e) {

    $(this).parent().find('button').prop('hidden', false);

    $(this).prop('hidden', true);
    var userWrap = $(this).closest('.asnwer-list');

    userWrap.find('.answer-issued').prop('disabled', false);
});

$(document).on("click", ".save-answer", function (e) {

    var id = $(this).closest('.asnwer-list').find('.userId')[0].valueAsNumber;

    var isChecked = $(this).closest('.asnwer-list').find('.isIssued')[0].value;

    console.log(isChecked);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/QuizzSessionAnswer/ChangeAnswerStatus',
        data: { id: id, isChecked: isChecked },
        success: function (data) {
            console.log(data);
        }
    });


    var userWrap = $(this).closest('.asnwer-list');

    userWrap.find('.answer-issued').prop('disabled', true);

    $(this).parent().find('button').prop('hidden', false);

    $(this).prop('hidden', true);

});

$(document).on("click", "#templateFilterReset", function (e) {


    e.preventDefault();

    window.location = window.location.pathname;

});

$(document).on("click", "#exportAnswers", function (e) {

    var filter = getFilterForm();

    $.ajax({
        type: "POST",
        url: '/QuizzSessionAnswer/CreateExcelReport/',
        data: { filter: filter },
    }).done(function (data) {
        var url = data;
        var a = document.createElement('a');
        a.href = data;
        a.download = 'report.xlsx';
        a.click();
        window.URL.revokeObjectURL(url);
    }).fail(function (error) {
        alert(error)
    });
});


function getFilterForm() {
    return $('#filter-form').serializeArray()
        .reduce(function (a, x) { a[x.name.replace("Filter.", "")] = x.value; return a; }, {});
}
