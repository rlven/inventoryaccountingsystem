﻿//Custom properties for requests in general checkbox
CustomProperties = {
    PartyId: $("#partyId").val()
};

$(document).ready(function () {
    $(document).on("click", "#block-party-user", function () {
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        var partyMembers = {
            filter: filter,
            membersId: JSON.parse($("#entity-id").val()),
            partyId: $("#partyId").val()
        }
        $.ajax({
            url: 'PartyMembers/BlockMembers',
            type: 'POST',
            data: JSON.stringify(partyMembers),
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on("click", "#unblock-party-user", function () {
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        var partyMembers = {
            filter: filter,
            membersId: JSON.parse($("#entity-id").val()),
            partyId: $("#partyId").val()
        }
        $.ajax({
            url: 'PartyMembers/UnblockMembers',
            type: 'POST',
            data: JSON.stringify(partyMembers),
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on("click", "#invite-party-user", function () {
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        var partyMembers = {
            filter: filter,
            membersId: JSON.parse($("#entity-id").val()),
            partyId: $("#partyId").val()
        }
        $.ajax({
            url: 'PartyMembers/InviteUsers',
            type: 'POST',
            data: JSON.stringify(partyMembers),
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on("click", "#cancel-party-users-invitation", function () {
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        var partyMembers = {
            filter: filter,
            membersId: JSON.parse($("#entity-id").val()),
            partyId: $("#partyId").val()
        }
        $.ajax({
            url: 'PartyMembers/CancelUsersInvitation',
            type: 'POST',
            data: JSON.stringify(partyMembers),
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on("submit", "#user-add-to-chat-form", function (e) {
        e.preventDefault();
        var form = $(this);
        var ids = JSON.parse($("#entity-id").val());
        var id = $('input[name="chatAddGroupId"]:checked').val();
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        var partyId = $('#partyId').val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: { filter: filter, chatGroupId: id, userIds: ids, partyId: partyId },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                } else {
                    alert(data.message);
                }
            }
        });
    });

    $(document).on("submit", "#user-add-to-chat-form", function(e) {
        e.preventDefault();
    });

    $(document).on("click", "#excel-export-users", createUsersReport);
    function createUsersReport(event) {
        event.preventDefault();
        var filter = getFilterForm();
        var partyId = $('#partyId').val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/PartyMembers/CreateExcelReport/',
            data: { filter: filter, partyId: partyId},
            success: function (data) {
                if (data.success) {
                    var url = data.url;
                    var a = document.createElement('a');
                    a.href = url;
                    a.download = 'report.xlsx';
                    a.click();
                    window.URL.revokeObjectURL(url);
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    }
});