﻿


function showUserPanel(isVisiable, length = 0) {
    if (isVisiable) {
        $('.users-control-menu').find('#checked-users-amount').text(length);
        $('.users-control-menu').addClass('on');
    } else {
        $('.users-control-menu').removeClass('on');
        $('.users-control-menu').find('#checked-users-amount').text(0);
    }
}


$(document).on("submit", "#user-delete-form", function (e) {
    e.preventDefault();
    var ids = JSON.parse($("#entity-id").val());

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/Support/ResetUserPasswordConfirm',
        data: {ids: ids },
        success: function (data) {
            console.log(data)
            if (data.success) {
                location.reload();
            }
            else if (data.isDeleted) {
                $("#closeDialogReset").click();
                alert('в списке есть удаленные пользователи, номера:'+data.deletList)
                location.reload();
            } else {
                console.log(data.phonelist);
                $("#closeDialogReset").click();
                $("#aproove-user").click();
                $("#phone-list").val(data.phonelist);
            }
        }
    });
});


$(document).on("submit", "#user-deleteReport-form", function (e) {
    e.preventDefault();
    var ids = JSON.parse($("#entity-id").val());

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/Support/DeleteSupportReport',
        data: { ids: ids },
        success: function (data) {
            if (data.success) {
                location.reload();
            } else {
                alert("Проверьте введенные данные");
            }
        }
    });
});

//$(document).on('click', '.user-bun-btn', function () {
  //  var ids = [];
   // ids.push($(this).closest('.st-user').find('.operation_checkbox').prev().val());
    //$("#entity-id").val(JSON.stringify(ids));
//});

$(document).on('click', '#operation-checkbox-column input', function () {
    var ids = [];
    var entityId = $('#entity-id').attr('value');
    if (typeof entityId !== typeof undefined && entityId !== false) {
        ids = JSON.parse($("#entity-id").val());
    }
    var id = $(this).prev().val();
    if (!ids.includes(id)) {
        ids.push(id);
    } else {
        var index = ids.indexOf(id);
        if (index > -1) {
            ids.splice(index, 1);
        }
    }
    $("#entity-id").val(JSON.stringify(ids));

    if ($('#select-all-users').is(":checked")) {
        showUserPanel(true, $("#entity-count").val() - ids.length);
    } else {
        if (ids.length > 0) {
            showUserPanel(true, ids.length);
        } else {
            showUserPanel(false);
        }
    }
});