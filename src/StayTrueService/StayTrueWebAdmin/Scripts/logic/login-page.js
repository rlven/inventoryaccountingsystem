﻿$(function () {
    $('#authentication-form').submit(function (e) {

        e.preventDefault();

        var errorMessage = $(this).find('.error-message');

        var email = $(this).find('#email');
        var password = $(this).find('#password');

        email.parent().removeClass('on-error');
        password.parent().removeClass('on-error');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/Account/Login",
            data: $(this).serialize(),
            success: function (data) {

                errorMessage.removeClass('on');

                console.log(data);

                if (!data.success) {
                    errorMessage.find('span').text(data.errorMessages);
                    errorMessage.addClass('on');
                    return false;
                }

                window.location.replace(data.redirectTo);
                return true;
                //window.location.href = data['redirectTo'];
            }
        });
        return false;
    });

    $('.message-btn').click(function () {
        $('.pop-up-message').hide();
        $('#restore-form').show();
    });

});