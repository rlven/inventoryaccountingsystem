$(function () {
    $("#add-pattern #add-new-tgl").click(function () {
        var patternWrap = $(this).parent();

        patternWrap.find(".summernote").summernote({
            height: 150,
            toolbar: [
                ['view', ['codeview']]
            ],
            callbacks: {
                onChange: function (contents) {
                    var priceWrap = $(this).closest('form').find('.sms-price-value');
                    var priceWrapInput = $(this).closest('form').find('.sms-price-input');
                    var boolIsPasswordRequired = $(this).closest('form').find('.smsTemplateCheckBox');
                    var isChecked = (boolIsPasswordRequired.is(":checked"));
                    getSmsPrice(contents, isChecked, function (data) {
                        priceWrap.text(data);
                        priceWrapInput.val(data);
                    });
                }
            }
        });

        patternWrap.find(".st-pattern-body").show();
    });

    $(".pattern-add-abort").click(function () {
        var patternWrap = $(this).closest("form");
        patternWrap.find(".summernote").summernote("destroy");
        patternWrap.find(".st-pattern-body").hide();
    });

    $('.smsTemplateCheckBox').change(function() {
        var contents = $('#createSmsTemplateText').val();

        var boolIsPasswordRequired = $(this).closest('form').find('.smsTemplateCheckBox');
        var isChecked = (boolIsPasswordRequired.is(":checked"));
        getSmsP(contents, isChecked);

    });

    $('.smsCheckBoxId').change(function() {

        var contents = $(this).closest('form').find('.createSmsTemplateTextEdit').text();
        var boolIsPasswordRequired = $(this).closest('form').find('.smsCheckBoxId');
        var isChecked = (boolIsPasswordRequired.is(":checked"));
        getSmsP(contents, isChecked);
    });

    $(".pattern-edit").click(function () {
        var patternWrap = $(this).closest(".st-pattern");

        patternWrap.find(".summernote").summernote({
            height: 150,
            toolbar: [
                ['view', ['codeview']]
            ],
            callbacks: {
                onChange: function (contents) {

                    var priceWrap = $(this).closest('form').find('.sms-price-value');
                    var priceWrapInput = $(this).closest('form').find('.sms-price-input');
                    var boolIsPasswordRequired = $(this).closest('form').find('.smsTemplateCheckBox');
                    var isChecked = (boolIsPasswordRequired.is(":checked"));
                    getSmsPrice(contents, isChecked, function (data) {
                        priceWrap.text(data);
                        priceWrapInput.val(data);
                    });
                }
            }
        });
        patternWrap.addClass("on-edit");
    });

    $(".pattern-change-abort").click(function () {
        var patternWrap = $(this).closest(".st-pattern");
        patternWrap.find(".summernote").summernote("destroy");
        patternWrap.removeClass("on-edit");
    });

    function getSmsP(contents, isChecked) {
        var smsCost = 1.80;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/Sms/GetMessageCost',
            data: { smsText: contents, smsCost: smsCost, isChecked: isChecked },
            success: function (data) {
                var priceWrap = $(this).closest('form').find('.sms-price-value');
                var priceWrapInput = $(this).closest('form').find('.sms-price-input');
                priceWrap.text(data);
                priceWrapInput.val(data);
            }
        });
    }

    function getSmsPrice(contents, isChecked,  callback) {
        var smsCost = 1.80;
        $.ajax({    
            type: "GET",
            dataType: "json",
            url: '/Sms/GetMessageCost',
            data: { smsText: contents, smsCost: smsCost, isChecked: isChecked },
            success: function (data) {
                callback(data);
            }
        });
    }
});

$(document).on("click", "#templateFilterReset", function() {

    location.reload();

});

$(document).on("click", "#deleteSmsTemplate", function (e) {

    var id = $(this).data('id');

    $.ajax({
        type: "POST",
        url: '/Sms/DeleteSmsTemplate/',
        data: { id: id },
        success: function () {
            location.reload();
        }
    });
});
