﻿$(document).on("click", "#content-list-pager a", function () {
    var url = $(this).attr("href").concat("&IsAllSelected=" + $("#select-all-users").is(":checked"));
    $.ajax({
        url: url,
        type: "GET",
        cache: false,
        success: function (result) {
            $("#content").html(result);
            checkAll($("#select-all-users").is(":checked"));
            checkSelected();
        }
    });
    return false;
});

$(document).on("submit", "#FilterForm", function(e) {
    e.preventDefault();
    var formData = $('#FilterForm').serializeArray();
    formData.push({ name: "isAllSelected", value: $('#select-all-users').is(":checked") });
    $.ajax({
        type: "POST",
        url: $("#FilterForm").prop("action"),
        data: formData,
        success: function (result) {
            $('#content').html(result);
            checkAll($('#select-all-users').is(":checked"));
            $("#entity-id").val("[]");
        }
    });
});

function getFilterForm() {
    return $('#FilterForm').serializeArray()
        .reduce(function (a, x) { a[x.name.replace("Filter.", "")] = x.value; return a; }, {});
}

//-------------- Check users in the list ---------------------
// For this script to work html page needs to contain:
// <input type="hidden" name="entity-id" id="entity-id" value="[]" /> - here will be all ids
// <input type="hidden" name="entity-count" id="entity-count" value="0" /> - here will be ids quantity
// Hidden input with id as it's value, it needs to be right before checkbox
// Checkboxes, has to have operation_checkbox class and to be inside the div with id = operation-checkbox-column
// General checkbox has to be inside Filter form and have id = select-all-users
// Every page that use this file can have own js file with the object named "CustomProperties",
// this properties will be used inside request bodies for select functions here
$(document).on("click", "#operation-checkbox-column input", function () {
    var ids = [];
    var entityId = $('#entity-id').attr("value");
    if (typeof entityId !== typeof undefined && entityId !== false) {
        ids = JSON.parse($("#entity-id").val());
    }
    var id = $(this).prev().val();
    if (!ids.includes(id)) {
        ids.push(id);
    } else {
        var index = ids.indexOf(id);
        if (index > -1) {
            ids.splice(index, 1);
        }
    }
    $("#entity-id").val(JSON.stringify(ids));

    if ($("#select-all-users").is(":checked")) {
        showUserPanel(true, $("#entity-count").val() - ids.length);
    } else {
        if (ids.length > 0) {
            showUserPanel(true, ids.length);
        } else {
            showUserPanel(false);
        }
    }
});

$(document).on("click", "#select-all-users", function () {
    checkAll($("#select-all-users").is(":checked"));
    $("#entity-id").val("[]");
    if ($(this).is(":checked")) {
        var data = getRequestProperties();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: window.location.pathname + "/CountFilteredUsers",
            data: data,
            success: function (data) {
                if (data.success) {
                    $("#entity-count").val(data.count);
                    showUserPanel(true, data.count);
                } else {
                    alert("Проверьте введенные данные");
                }
            },
            error: function () {
                location.reload();
            }
        });
    } else {
        showUserPanel(false);
    }
});

function checkAll(check = false) {
    var items = $('#operation-checkbox-column input[type="checkbox"]');
    items.prop('checked', check);
}

function checkSelected() {
    var ids = [];
    var entityId = $('#entity-id').attr('value');
    if (typeof entityId !== typeof undefined && entityId !== false) {
        ids = JSON.parse($("#entity-id").val());
    }
    var items = $('#operation-checkbox-column input[type="checkbox"]');
    for (var i = 0; i < items.length; i++) {
        var id = $(items[i]).prev();
        if (ids.includes((id).val())) {
            $('#' + items[i].id).prop('checked', !$('#select-all-users').is(":checked"));
        }
    }
}

function getRequestProperties() {
    var filter = getFilterForm();
    filter.IsAllSelected = $('#select-all-users').is(":checked");
    if (typeof CustomProperties != "undefined") {
        return { filter: filter, ...CustomProperties }
    } else {
        return { filter: filter }
    }
}

function showUserPanel(isVisible, length = 0) {
    if (isVisible) {
        $('.users-control-menu').find('#checked-users-amount').text(length);
        $('#smsFormCheckedUsersAmount').text(length);

        $('.users-control-menu').addClass('on');
    } else {
        $('.users-control-menu').removeClass('on');
        $('.users-control-menu').find('#checked-users-amount').text(0);
    }
}

//--------------------------------------------------------------------