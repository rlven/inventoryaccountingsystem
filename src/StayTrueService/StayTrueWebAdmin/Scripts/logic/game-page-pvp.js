﻿jQuery(function ($) {

    $('#createGameForm').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert(data.debug);
                    }
                }
            });

        });

    $('.entity-edit').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);



            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        //location.reload();
                        var btn = $(form).find('.game-save');

                        $(btn).parent().find('.st-btn').hide();
                        $(btn).parent().find('.game-edit').show();

                        var gameWrap = $(btn).closest('.st-game-header');
                        gameWrap.find('.game-zip').hide();
                        gameWrap.next('.st-game-footer').slideUp();
                        gameWrap.closest('.st-game').removeClass('on-edit');
                        gameWrap.find('.st-upload').removeClass('edit');
                        gameWrap.closest('.st-game').find('select.game-wincoin-currency').each(function () {
                            var selectVal = $(this).val();
                            $(this)[0].selectize.destroy();
                            $(this).val(selectVal);
                        });
                        gameWrap.parent().find('.game-wincoin-currency').prop('disabled', true);
                        gameWrap.find('.game-edit-item').prop('disabled', true);
                        gameWrap.find('.rate-value').prop('disabled', true);
                    } else {
                        alert(data.debug);
                    }
                }
            });

        });

    $(".delete-btn").on("click",
        function () {

            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });

    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/PvpGames/DeleteConfirmed',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert(data.debug);
                    }
                }
            });

        });
});

function loadUserLiders(raw) {
    /*var raw = $(this).closest(".st-game");*/
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/PvpGames/GetBestScores',
        data: { gameId: $(raw).find("#item_Id").val() },
        success: function (data) {
            if (data.success) {
                var str = "";
                if (data.liders.length > 0) {
                    var liders = data.liders;
                    liders.forEach(function (item, index, liders) {
                        str += ' <div class="st-row"><div class="st-column" ><span class="column-title">' + (index + 1) + '. ' + item.UserFullName + '</span ></div>';
                        str += ' <div class="st-column"><span class="column-title" >' + item.Score + ' </span></div><div class="st-column"></div></div> ';
                    });
                } else {
                    str += '<div class="st-row"><span class="column-title" > Нет данных для отображения </span ></div> ';
                }
                $(raw).find(".user-statistic").empty();
                $(raw).find(".user-statistic").append(str);
                $(raw).find('.st-game-footer').slideToggle();
            }
        }
    });
}


$(function () {
    $('.st-game-header').click(function () {
        if ($('div.st-game.on-edit').length > 0 && !$(this).closest('.st-game').hasClass('on-edit')) {
            editWarning();
        } else {
            if (event.target.nodeName != 'BUTTON' && !$(this).closest('.st-game').hasClass('on-edit')) {
                $('.st-game-footer').not($(this).closest('.st-game').find('.st-game-footer')).not($('#add-game').find('.st-game-footer')).slideUp();
                var raw = $(this).closest(".st-game");
                loadUserLiders(raw);

            }
        }
    });


    $('#add-game #add-new-tgl').click(function () {
        if ($('div.st-game.on-edit').length > 0 && !$(this).next('.st-game').hasClass('on-edit')) {
            editWarning();
        } else {
            $("#Name").attr("placeholder", "Название игры");
            document.getElementById("Name").value = "";
            document.getElementById("gameZip").value = "";

            $(this).parent().find('.st-game').addClass('on-edit').slideDown();
            $(this).parent().find('.game-zip').show();


        }
    });
    /*
    
        $('.game-add').click(function () {
            $(this).closest('.st-game').slideUp();
            $(this).closest('.st-game').find('.game-zip').hide();
            $(this).closest('.st-game').removeClass('on-edit');
        });*/

    $('.game-add-abort').click(function () {
        $(this).closest('.st-game').slideUp();
        $(this).closest('.st-game').find('.game-zip').hide();
        $(this).closest('.st-game').removeClass('on-edit');
        $(this).closest('form')[0].reset();
        $(this).closest('form').find('.st-upload').css('background-image', 'url(/Content/img/upload-plus.png)');
    });

    $('.game-edit-abort').click(function () {
        $(this).parent().find('.st-btn').hide();
        $(this).parent().find('.game-edit').show();
        $(this).closest('.st-game').removeClass('on-edit');
        $(this).closest('.st-game').find('.st-game-footer').slideUp();
        $(this).closest('.st-game').find('.game-edit-item').prop('disabled', true);
        $(this).closest('.st-game').find('.rate-value').prop('disabled', true);
        $(this).closest('.st-game').find('.game-zip').hide();
        $(this).closest('form')[0].reset();
    });

    $('.game-edit').click(function () {
        if ($('div.st-game.on-edit').length > 0) {
            editWarning();
        } else {
            $(this).parent().find('.st-btn').show();
            $(this).hide();
            var gameWrap = $(this).closest('.st-game');

            gameWrap.addClass('on-edit');
            gameWrap.find('.st-upload').addClass('edit');
            $('.st-game-footer').not(gameWrap.find('.st-game-footer')).slideUp();
            /*gameWrap.find('.game-wincoin-currency').prop('disabled', false);
            gameWrap.find('.game-wincoin-currency').selectize({
                allowEmptyOption: true
            });
            gameWrap.find('.st-game-footer').not('.add-new-wrap .st-game-footer').slideDown();*/
            gameWrap.find('.game-edit-item').prop('disabled', false);
            gameWrap.find('.rate-value').prop('disabled', false);
            gameWrap.find('.game-zip').show();

            loadUserLiders(gameWrap);
        }
    });

    $(".add-rate button").click(function () {
        var rateItems = $(this).closest('.st-game-header').find('.rate-item');
        var counter = rateItems.length;
        

        if (rateItems.length < 10) {

            $(this).closest('.st-game-header').find('.game-rate-list').append(
                '<div class="rate-item"><input type="number" class="rate-value" id="rateId' + counter + '"  name="bets[' + counter +']'+'"/><div class="rate-remove">x</div></div>'
            );
            counter++;

        } else {
            alert("Максимальное количество ставок 10!");
        }
    });

    $(document).on("click", ".rate-remove", function () {
        $(this)
            .closest(".rate-item")
            .remove();
    });


});