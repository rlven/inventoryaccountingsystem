﻿jQuery(function ($) {
   
    $('.checkbox-column input[type=checkbox]').click(function (e) {
        e.stopPropagation();
    });

   

    $('.checkbox-column input').click(function () {
        var items = $('.checkbox-column input[type="checkbox"]:checked');
        if (items.length > 0) {
            $('.users-control-menu').find('#checked-users-amount').text(items.length);

            var user_ids = [];
            var item_ids = [];
            for (var i = 0; i < items.length; i++) {
                var user_id = $(items[i]).prev();
                user_ids.push($(user_id).val());
                var item_id = $(items[i]).prev().prev();
                item_ids.push($(item_id).val());
            }

            $("#entity-id").val(JSON.stringify({ 'user_ids': user_ids, 'item_ids': item_ids}));

            $('.users-control-menu').addClass('on');

        } else {
            $('.users-control-menu').removeClass('on');
            $('.users-control-menu').find('#checked-users-amount').text(0);
        }
    });


    $("#user-delete-form").on("submit",
        function (e) {
            e.preventDefault();

            var form = $(this);
            var ids = JSON.parse($("#entity-id").val());

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: { ids: ids.item_ids },
                success: function (data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        alert(data.message);
                    }
                }
            });
            
        });

    

    $("#user-add-to-chat-form").on("submit", function (e) {
        e.preventDefault();
        var form = $(this);
        var ids = JSON.parse($("#entity-id").val());
        var id = $('input[name="chatAddGroupId"]:checked').val();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: { chatGroupId: id, userIds: ids.user_ids },
                success: function (data) {
                    if (data.success) {
                        window.location.reload();
                    } else {
                        alert(data.message);
                    }
                }
            });
        });

        
});