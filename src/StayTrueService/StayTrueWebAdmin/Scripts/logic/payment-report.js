﻿$(document).on("click", "#exportExcel", function (e) {

    var from = $('#fromId').val();
    var to = $('#toId').val();
    console.log(from);
    console.log(to);

    $.ajax({
        type: "POST",
        url: '/PaymentReport/ExportExcel/',
        data: { from: from, to: to}
    }).done(function (data) {
        var url = data;
        var a = document.createElement('a');
        a.href = data;
        a.download = 'report.xlsx';
        a.click();
        window.URL.revokeObjectURL(url);
    }).fail(function (error) {
        alert(error)
    });
});

$(document).on("click", "#reset-btn", function (e) {


    e.preventDefault();

    window.location = window.location.pathname;

});