﻿$(function() {
    $(document).on("click", "#create-brand", function (e) {
        e.preventDefault();
        window.location.href = "/CigaretteBrands/Create";
    });

    $(document).on("click", "#create-brand-cancel", function (e) {
        e.preventDefault();
        window.location.href = "/CigaretteBrands/Index";
    });

    $('.summernote').summernote({
        height: 250,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    $(document).on("submit", "#create-cigarette-brand-form", function(e) {
        e.preventDefault();
        var form = $(this);
        form.find("#Description").val(form.find(".note-editable").text());
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.success) {
                    location.href = "/CigaretteBrands/Index";
                } else {
                    alert(data.debug);
                }
            }
        });
    });

    $(document).on("submit", "#edit-cigarette-brand-form", function (e) {
        e.preventDefault();
        var form = $(this);
        form.find("#Description").val(form.find(".note-editable").text());
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.success) {
                    location.href = "/CigaretteBrands/Index";
                } else {
                    alert(data.debug);
                }
            }
        });
    });

    $(document).on("click", "#reset-filter", function () {
        window.location.href = "/CigaretteBrands/Index";
    });

    $(document).on("click", ".brand-edit", function () {
        location.href = "/CigaretteBrands/Edit?id=" + $(this).val();
    });

    $(document).on("submit", "#delete-brand-form", function (e) {
        e.preventDefault();
        var id = $("#brand-id").val();
        console.log(id);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/CigaretteBrands/DeleteConfirmed",
            data: { id: id },
            success: function (data) {
                if (data.success) {
                    location.href = "/CigaretteBrands/Index";
                } else {
                    alert(data.debug);
                }
            }
        });
    });

    $(document).on("change", ".st-upload", function () {
        $(this).find("input[type=hidden]").val($(this).find("input[type=file]").val());
    });

    function DeleteAdditionalImage(item) {
        item.find('.addition-icon-href').attr('href', '/Content/img/news-app-image-upload.png');
        item.find('.addition-icon-href')
            .attr('style', 'background-image: url(/Content/img/news-app-image-upload.png)');
    }

    $(document).ready(function () {
        $('.delete-additional-image').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            e.stopImmediatePropagation();

            var item = $(this).closest(".advantage-item");
            item.find('.news_edit_items').val("");
            item.find('.st-upload-input').val('');
            var image = item.find("#InputNumber").val();
            if (window.location.pathname.includes("Edit") && !image.includes("fakepath")) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "/CigaretteBrands/DeleteAdditionalImage?id=" +
                        $("#brand-id").val() +
                        "&image=" + image,
                    success: function(data) {
                        if (data.success) {
                            DeleteAdditionalImage(item);
                        } else {
                            alert(data.debug);
                        }
                    }
                });
            } else {
                DeleteAdditionalImage(item);
            }
        });
    });
});