﻿jQuery(function ($) {

    $('#createQuizForm').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });

    $('.entity-edit').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        //location.reload();
                        var btn = $(form).find('.quiz-save');

                        $(btn).parent().find('.st-btn').hide();
                        $(btn).parent().find('.quiz-edit').show();

                        var wrap = $(btn).closest('.st-game-header');
                 
                        wrap.find('.game-zip').hide();
                        wrap.next('.st-game-footer').slideUp();
                        wrap.closest('.st-game').removeClass('on-edit');
                        wrap.find('.st-upload').removeClass('edit');

                        wrap.find('.game-edit-item').prop('disabled', true);
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });

    $(".delete-btn").on("click",
        function () {

            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });

    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Quiz/DeleteConfirmed',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });
});

/*function loadUserLiders(raw) {
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/Game/GetBestScores',
        data: { gameId: $(raw).find("#item_Id").val() },
        success: function (data) {
            if (data.success) {
                var str = "";
                if (data.liders.length > 0) {
                    var liders = data.liders;
                    liders.forEach(function (item, index , liders) {
                        str += ' <div class="st-row"><div class="st-column" ><span class="column-title">' + (index + 1) + '. ' + item.UserFullName + '</span ></div>';
                        str += ' <div class="st-column"><span class="column-title" >' + item.Score + ' </span></div><div class="st-column"></div></div> ';
                    });
                } else {
                    str += '<div class="st-row"><span class="column-title" > Нет данных для отображения </span ></div> ';
                }
                $(raw).find(".user-statistic").empty();
                $(raw).find(".user-statistic").append(str);
                $(raw).find('.st-game-footer').slideToggle();
            }
        }
    });
}*/

$(function () {
    $('.st-game-header').click(function () {
        if ($('div.st-game.on-edit').length > 0 && !$(this).closest('.st-game').hasClass('on-edit')) {
            editWarning();
        } else {
            /*if (event.target.nodeName != 'BUTTON' && !$(this).closest('.st-game').hasClass('on-edit')) {
                $('.st-game-footer').not($(this).closest('.st-game').find('.st-game-footer')).not($('#add-game').find('.st-game-footer')).slideUp();
                var raw = $(this).closest(".st-game");
                loadUserLiders(raw);

            }*/
        }
    });

    $('#add-quiz #add-new-tgl').click(function () {
        if ($('div.st-game.on-edit').length > 0 && !$(this).next('.st-game').hasClass('on-edit')) {
            editWarning();
        } else {
            $("#Name").attr("placeholder", "Название");
            document.getElementById("Name").value = "";
            document.getElementById("quizZip").value = "";

            $(this).parent().find('.summernote').summernote({
                height: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            $(this).parent().find('.st-game').addClass('on-edit').slideDown();
            $(this).parent().find('.game-zip').show();


        }
    });


    $('.quiz-add-abort').click(function () {
        $(this).closest('.st-game').slideUp();
        $(this).closest('.st-game').find('.quiz-zip').hide();
        $(this).closest('.st-game').removeClass('on-edit');
        $(this).closest('form')[0].reset();
        $(this).closest('form').find('.st-upload').css('background-image', 'url(/Content/img/upload-plus.png)');
    });

    $('.quiz-edit-abort').click(function () {
        $(this).parent().find('.st-btn').hide();
        $(this).parent().find('.quiz-edit').show();
        $(this).closest('.st-game').removeClass('on-edit');
        $(this).closest('.st-game').find('.st-game-footer').slideUp();
        $(this).closest('.st-game').find('.game-edit-item').prop('disabled', true);
        $(this).closest('.st-game').find('.game-zip').hide();
        $(this).closest('form')[0].reset();
    });

    $('.quiz-edit').click(function () {
        if ($('div.st-game.on-edit').length > 0) {
            editWarning();
        } else {
            $(this).parent().find('.st-btn').show();
            $(this).hide();
            var gameWrap = $(this).closest('.st-game');
           
            gameWrap.addClass('on-edit');
            gameWrap.find('.st-upload').addClass('edit');
            $('.st-game-footer').not(gameWrap.find('.st-game-footer')).slideUp();

            gameWrap.find('.summernote').summernote({
                height: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            gameWrap.find('.game-edit-item').prop('disabled', false);
            gameWrap.find('.game-zip').show();

           // loadUserLiders(gameWrap);
        }
    });

});