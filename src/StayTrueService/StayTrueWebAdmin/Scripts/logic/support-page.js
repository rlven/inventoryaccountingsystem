﻿jQuery(function ($) {

    $('.entity-edit').each(function () {
        $(this).find("#item_IsAnswered").val($(this).find("span.support-answered").data("support-isanswered"));
    });;

    //--- Подготовка полей пользователя к редактированию -----------------------------------------

    $('.support-edit').click(function () {
        if ($('div.st-user.on-edit').length > 0) {
            editWarning();
        } else {
            $(this).parent().find('button').prop('hidden', false);

            $(this).prop('hidden', true);
            var userWrap = $(this).closest('.st-user');
            userWrap.addClass('on-edit');
            
            userWrap.find('.user_edit_items').prop('disabled', false);
            userWrap.find('.datepicker').prop('disabled', false);
            userWrap.find('select.user_edit_items').selectize({
                sortField: 'value',
                allowEmptyOption: true,
                placeholder: $(this).attr('placeholder'),
                onFocus: function () {
                    $(this).scrollFix();
                },
                onBlur: function () {
                    $(this).scrollFix();
                },
                onDropdownClose: function () {
                    $(this).scrollFix();
                },
                onDropdownOpen: function () {
                    $(this).scrollFix();
                },
            });
        }
    });

    $('.support-change-abort').click(function () {
        $(this).parent().find('button').prop('hidden', true);
        $(this).parent().find('.support-edit').prop('hidden', false);
        var userWrap = $(this).closest('.st-user');
        userWrap.removeClass('on-edit');
        userWrap.find('.user_edit_items').prop('disabled', true);
        userWrap.find('select.user_edit_items').each(function () {
            $(this)[0].selectize.destroy();
        });
        $(this).closest('form')[0].reset();
    });


    $('.entity-edit').on("submit", function (e) {
        e.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        var submitBtn = form.find('button:submit');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                form.find('.formError').remove();

                if (data.success) {
                    submitBtn.parent().find('.support-save').prop('hidden', true);
                    submitBtn.parent().find('.support-change-abort').prop('hidden', true);
                    submitBtn.parent().find('.support-edit').prop('hidden', false);
                    var userWrap = submitBtn.closest('.st-user');
                    
                    userWrap.removeClass('on-edit');
                    userWrap.find('.user_edit_items').prop('disabled', true);
                    userWrap.find('.st-user-header').find('.datepicker').prop('disabled', true);
                    userWrap.find('select.user_edit_items').each(function () {
                        var selectVal = $(this).val();
                        $(this)[0].selectize.destroy();
                        $(this).val(selectVal);
                    });

                } else {
                    form.find('.st-user-header').after('<div class="formError"><span><ion-icon name="alert"></ion-icon> Проверьте введенные данные!</span></div>');
                }
            }
        });

    });

    $(document).on('click', '.st-user-header', function (event) {
        var table = $(this).closest('form').find('.requestHistoryList');
        table.empty();
        $('.st-user-body').not($(this).parent().find('.st-user-body')).slideUp();
        $(this).closest('.st-user').find('.st-user-body').slideToggle();

        var userId = $(this).closest('.st-user').find('.userId')[0].defaultValue;
        $.ajax({
                type: "GET",
                url: "/Support/GetRequestHistory",
            dataType: "json",
                data: {userId:userId },
            })
            .done(function (data) {

                $.each(data, function (index, item) {
                    console.log(item.CreationDate);
                    table.append(
                        "<tr>" +
                        "<th>" + item.Email+"</th>" +
                        "<th>" + item.Phone + "</th>" +
                        "<th>" + item.CreationDate + "</th>" +
                        "<th>" + item.Body + "</th>" +
                        "<th>" + item.IsAnswered + "</th>" +
                        "<th>" + item.AnswerDate + "</th>" +
                        "<th>" + item.IsDeleted + "</th>" +
                        "</tr>"
                    );
                });
            })
            .fail(function (xhr) {
                alert(xhr.responseText);
            });

    });

    $(document).on('click', '#operation-checkbox-column input[type=checkbox]', function (e) {
        e.stopPropagation();
    });

});