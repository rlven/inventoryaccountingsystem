﻿$(function () {

    $('.entity-edit').each(function () {
        $(this).find("#item_ShopId").val($(this).find(".news-shop").data('shop'));
        $(this).find("#item_CategoryId").val($(this).find(".news-category").data('category'));
    });

    $(".list-body .st-row").each(function () {
        $(this).find("#history_StatusId").val($(this).find("#history_purchase_status").data('statusid'));
    });

    $('.product-history').click(function () {
        if ($('div.st-news.on-edit').length > 0) {
            editWarning();
        } else {
            $('.news-bottom').slideUp();
            $(this).closest('.news-midddle').next().toggle();
        }
    });

    $('#add-new-tgl').click(function () {
        if ($('div.st-news.on-edit').length > 0) {
            editWarning();
        } else {
            userWrap = $(this).next();
            userWrap.addClass('on-edit');
            userWrap.find('.txt-description').hide();
            userWrap.find('.news_edit_items').prop('disabled', false);
            userWrap.find('.summernote-product').summernote({
                height: 250,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            userWrap.find('.summernote-product').summernote('code', userWrap.find('.txt-description').html());
            userWrap.slideDown();
        }
    });

    $('.filterButton').click(function () {
        var filtervalue = $(this).closest('form').find('.inputValue');

        var oTable = $(this).closest('form').find('.tableList');

        ResetFilter(oTable);

        for (i = 0; i < oTable.length; i++) {
            if (!oTable[i].childNodes[3].innerText.includes(filtervalue.val())) {
                oTable[i].style.display = 'none';
            }
            if (filtervalue.val() == "") {
                ResetFilter(oTable);
            }
        }

    });

    $('.resetFilterButton').click(function () {
        var oTable = $(this).closest('form').find('.tableList');
        ResetFilter(oTable);
    });

    function ResetFilter(table) {
        for (i = 0; i < table.length; i++) {
            table[i].style.display = '';
        }
    }

    $('.st-product .product-edit').click(function () {
        if ($('div.st-news.on-edit').length > 0) {
            editWarning();
        } else {
            userWrap = $(this).closest('.st-product');
            userWrap.addClass('on-edit');
            userWrap.find('.shop-btns button').not('.product-history').prop('hidden', false);
            $(this).prop('hidden', true);
            userWrap.find('.txt-description').hide();
            userWrap.find('.product_img_upload').addClass('edit');
            userWrap.find('.news_edit_items').prop('disabled', false);
            userWrap.find('select.news_edit_items').selectize();

            userWrap.find('.summernote-product').summernote({
                height: 250,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            userWrap.find('.summernote-product').summernote('code', userWrap.find('.txt-description').html());
            userWrap.find('.news-bottom').show();
        }
    });

    $('.st-product .product-add-abort').click(function () {
        userWrap.slideUp();
        userWrap = $(this).closest('.st-product');
        userWrap.removeClass('on-edit');
        userWrap.find('select.news_edit_items').each(function () {
            var selectVal = $(this).val();
            $(this).val(selectVal);
        });
        userWrap.find('form')[0].reset();
        userWrap.find('.summernote-product').summernote('reset');
        userWrap.find('.summernote-product').summernote('destroy');
    });

    $('.st-product .product-abort').click(function () {
        userWrap = $(this).closest('.st-product');
        userWrap.removeClass('on-edit');
        userWrap.find('.shop-btns button').not('.product-history').prop('hidden', true);
        userWrap.find('.product-edit').prop('hidden', false);
        userWrap.find('select.news_edit_items').each(function () {
            var selectVal = $(this).val();
            $(this)[0].selectize.destroy();
            $(this).val(selectVal);
        });
        userWrap.find('.product_img_upload').removeClass('edit');
        userWrap.find('form')[0].reset();
        userWrap.find('.news_edit_items').prop('disabled', true);
        userWrap.find('.summernote-product').summernote('reset');
        userWrap.find('.summernote-product').summernote('destroy');
        userWrap.find('.txt-description').show();
        userWrap.find('.news-bottom').hide();
    });
    
    $('#createProductForm').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });

    $('.entity-edit').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        var btn = $(form).find('.product-save');
                        userWrap = $(btn).closest('.st-product');

                        userWrap.removeClass('on-edit');
                        userWrap.find('.shop-btns button').not('.product-history').prop('hidden', true);
                        userWrap.find('.product-edit').prop('hidden', false);
                        userWrap.find('.product_img_upload').removeClass('edit');

                        userWrap.find('select.news_edit_items').each(function () {
                            var selectVal = $(this).val();
                            $(this)[0].selectize.destroy();
                            $(this).val(selectVal);
                        });
                        userWrap.find('.news_edit_items').prop('disabled', true);
                        userWrap.find('.txt-description').html($(btn).closest('.st-product').find('.summernote-product').summernote('code')).show();
                        userWrap.find('.summernote-product').summernote('reset');
                        userWrap.find('.summernote-product').summernote('destroy');
                        userWrap.find('.news-bottom').hide();

                        if ($(form).find("#item_IsAvailable").val() === "true") {
                            $(form).find(".delete-btn").show();
                        } else {
                            $(form).find(".delete-btn").hide();
                        }
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });

    $(".delete-btn").on("click",
        function () {
            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });

    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/FortuneProducts/DeleteProduct',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        var btn = $(".delete-btn[data-id='" + $("#entity-id").val() + "']");
                        $(btn).hide();
                        $(btn).closest("form").find("#item_IsAvailable").val("false");
                        $(".fancybox-close-small").click();
                        location.reload();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });

    $('.history-item-edit').click(function () {
        $(this).prop('hidden', true);
        $(this).next().prop('hidden', false);
        $(this).closest('.st-row').find('.history-edit-item').prop('disabled', false);
    });

    $('.history-item-save').click(function (e) {
        e.preventDefault();
        var btn = $(this);
        var row = $(btn).closest('.st-row');
        $(btn).prop('hidden', true);
        $(btn).prev().prop('hidden', false);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(this).parents().find("#purchase-history-form").attr('action'),
            data: { id: $(row).find("#history_Id").val(), statusId: $(row).find("#history_StatusId").val() },
            success: function (data) {
                if (data.success) {
                    $(row).find('.history-edit-item').prop('disabled', true);
                    $(row).find("#history_administrator_name").text(data.administratorName);

                } else {
                    alert(data.message);
                }
            }
        });
    });
});