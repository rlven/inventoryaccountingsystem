﻿

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

function getQuestionsBlocks(questions) {
    var quesionBlock = "";
    $.each(questions,
        function (key, value) {
            quesionBlock +=
                '<div class="battle-question">' +
                '<div class="question-title">' +
                '<label for="">Вопрос</label>' +
            '<input class="poll_edit_items" id="Questions_' + key + '__Question" name="PartyQuestionnaireModels[' + key + '].Question" placeholder="Введите вопрос" type="text" value="' + value.Question + '">' +
                '</div>' +
                '<button type="button" class="border-danger bg-danger text-white delete-question" data-id="' + value.Id + '">Удалить вопрос</button>' +
                ' </div>';
        });


    return quesionBlock;
}

$(function () {
    $('.abort-edit-create').click(function () {
        window.history.back();
    });

    $("#poll-description, .summernote").summernote({
        height: 150,
        toolbar: [
            ["style", ["bold", "italic", "underline", "clear"]],
            ["font", ["strikethrough", "superscript", "subscript"]],
            ["fontsize", ["fontsize"]],
            ["color", ["color"]],
            ["para", ["ul", "ol", "paragraph"]],
            ["height", ["height"]]
        ]
    });

    $(document).on('click', '.add-battle-question', function () {
        var questions = new Array();
        var questionList = $("#battle-questions .row .battle-question");

        if (questionList.length > 9) {
            alert('Максимальное количество вопросов: 10');
            return false;
        }

        $.each($(questionList),
            function (key, value) {
                questions.push({ "Id": $(value).find("input:hidden").val(), "Question": $(value).find(".poll_edit_items").val() });
            });
        questions.push({ "Id": 0, "Question": "" });

        $("#battle-question-list").html(getQuestionsBlocks(questions));
    });

    $(document).on("click", ".delete-question", function () {
        $(this)
            .closest(".battle-question")
            .remove();
    });

    $(document).on("change", ".answer-type", function () {
        console.log("changed");
        if ($(this).val() == "multiple-select") {
            $(this)
                .closest(".question-top")
                .find(".answer-quantity")
                .prop("disabled", false);
        } else {
            $(this)
                .closest(".question-top")
                .find(".answer-quantity")
                .prop("disabled", true)
                .val("");
        }
    });

    $(document).on("click", ".add-new-answer", function () {
        var questionsRow = $(this)
            .closest(".question-answers")
            .find(".row");
        if (questionsRow.find(".question-answers-item").length < 8) {
            questionsRow.append(answerBlock);
        } else {
            alert("Максимальное количество вариантов: 8");
        }
    });

    $(document).on("click", ".answer-delete", function () {
        $(this)
            .closest(".question-answers-item")
            .remove();
    });

    // -----------------------------------------------------------------------------------

    function getGameBlocks(key) {
        key = key - 1;
        quesionBlock =
            '<div class="entertainment-section" id="entertainment-party">' +
            '<div class="st-upload entertainment_img_upload edit">' +
            '<label for="Games_' + key + '__GameImage"></label>' +
            '<input type="file" id="Games_' +
            key +
            '__GameImage" accept="image/*" name="PartyGameModels[' +
            key +
            '].GameImage" class="st-upload-input"/>' +
            '</div>' +
            '<div class="entertainment-section-right">' +
            '<div class="game-title">' +
            '<span>Добавить игру:</span>' +
            '<br />' +
            '<input type="text" class="game-edit-item" placeholder="Название игры" id="Games_' +
            key +
            '__GameName" name="PartyGameModels[' +
            key +
            '].GameName"/>' +
            '</div>' +
            '<div class="game-zip">' +
            '<span>Загрзуите архив игры:</span>' +
            '<br />' +
            '<input type="file" class="add-new-quiz_zip" accept=".zip" name="PartyGameModels[' +
            key +
            '].GameArchive" id="Games_' +
            key +
            '__GameArchive"/>' +
            '</div>' +
            '</div>' +
            '</div>';            

        return quesionBlock;
    }


    $(document).on('click', '#add-battle-level #add-new-tgl', function (e) {
        e.preventDefault();
        var key = $('.entertainment-section').length;
        console.log(key);
        var tourList = $(".entertainment-section");

        if (tourList.length >= 9) {
            alert('Максимальное количество игр: 10');
            return false;
        }

        console.log(key);
        $('.party-entertainment-list').append(getGameBlocks(key));
        key++;
    });


    $(document).on("click", ".battle-level-delete", function () {
        $(this)
            .closest("form")
            .remove();
    });
});