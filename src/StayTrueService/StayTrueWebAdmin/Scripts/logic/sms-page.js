﻿$(function () {
    $('#sms-template').change(function () {
        var template = JSON.parse($(this).val());

        $("#smsText").val(template.Content);

    });

    $('#sms-template-single').change(function () {
        var template = JSON.parse($(this).val());

        $("#smsText").val(template.Content);
        $("#smsSendCost").text(template.cost.toFixed(2));

    });

    $("#cancel-sms-send").click(function(e) {
        e.preventDefault();
        location.href = '/Sms/GetSmsReports';
    });

    $('#sms-templatePhones').change(function () {
        var template = JSON.parse($(this).val());
        console.log(template.Content);
        $('.smsTextPhones').val(template.Content);
        var usersGroup = $("#sms-users-group").val();
        var costText = $("#smsSendCost");
        var usersCount = $("#smsFormCheckedUsersAmount");
        $("#smsMsgCost").text(template.cost.toFixed(2));

        var template = JSON.parse($("#sms-templatePhones").val());
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Sms/GetSmsMessageCostByTemplate',
            data: { templateId: template.Id, usersGroup: usersGroup },
            success: function (data) {
                if (data.success) {
                    console.log(data.cost);
                    costText.text(data.cost.toFixed(2));
                    console.log(data.userCount);
                    usersCount.text(data.userCount);
                } else {
                    alert(data.message);
                }
            }
        });
    });

    $('#sms-users-group').change(function () {

        var templateBlock = $("#sms-templatePhones").val();
        var template = JSON.parse(templateBlock);
        console.log(template.Content);

        var usersGroup = $("#sms-users-group").val();
        var costText = $("#smsSendCost");
        var usersCount = $("#smsFormCheckedUsersAmount");
        $("#smsMsgCost").text(template.cost.toFixed(2));

        var template = JSON.parse($("#sms-templatePhones").val());
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Sms/GetSmsMessageCostByTemplate',
            data: { templateId: template.Id, usersGroup: usersGroup },
            success: function (data) {
                if (data.success) {
                    console.log(data.cost);
                    costText.text(data.cost.toFixed(2));
                    console.log(data.userCount);
                    usersCount.text(data.userCount);
                } else {
                    alert(data.message);
                    console.log(data.cost);
                    costText.text(data.cost.toFixed(2));
                    console.log(data.userCount);
                    usersCount.text(data.userCount);
                }
            }
        });
    });

    $('#sms-template-form').submit(function (e) {
        e.preventDefault();
        var model = getNotificationForm();
        var template = JSON.parse($("#sms-template").val());
        model["TemplateId"] = template.Id;

        var userIds = JSON.parse($("#entity-id").val());

        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");

        model["UserIds"] = userIds;
        console.log(model);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Sms/SendSms',
            data: {  model:model, filter:filter },
            success: function (data) {
                if (data.success) {
                    document.getElementById("closeSmsForm").click();
                } else {
                    alert("Проверьте введенные данные");
                }
            }
        });
    });


    $('#sms-template-users-group').submit(function (e) {
        e.preventDefault();
        var model = getNotificationForm();
        var template = JSON.parse($("#sms-templatePhones").val());
        var usersGroup = $("#sms-users-group").val();
        var sendDate = $("#sendDate").val();

        model["TemplateId"] = template.Id;
        model["Phones"] = usersGroup;
        model["SendDate"] = sendDate;

        console.log(model);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Sms/SendSmsUsersGroup',
            data: { model: model },
            success: function (data) {
                if (data.success) {
                    document.getElementById("closeSmsForm").click();

                    alert('Рассылка успешно создана');

                } else {
                    alert(data.message);
                }
            }
        });
    });

    function getNotificationForm() {
        return $('#sms-template-form').serializeArray()
            .reduce(function (a, x) { a[x.name.replace("notification.", "")] = x.value; return a; }, {});
    }


    $(document).on("click", "#reset-btn", function (e) {


        $.ajax({
            type: "POST",
            url: '/Sms/GetSmsReports/',
            data: {},
            success: function (result) {
                $("#smsReportSendDate").val('');
                $("#smsReportSendDateEnd").val('');
                $("#smsReportTitle").val('');
                $("#filter-textarea").val('');
                $("#smsReportUserCount").val('');
                $("#smsReportUserCountEnd").val('');
                $("#smsReportCost").val('');
                $("#smsReportCostEnd").val('');
                $("#smsReportAdmin").val('');
                
            }
        });
    });

    function getFilterForm() {
        return $('#FilterForm').serializeArray()
            .reduce(function (a, x) { a[x.name.replace("Filter.", "")] = x.value; return a; }, {});
    }


    $(document).on("click", "#deleteSmsReport", function (e) {

        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            url: '/Sms/DeleteSmsReport/',
            data: {id:id},
            success: function () {
                location.reload();
            }
        });
    });

    $(document).on("click", "#exportExcel", function (e) {

        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            url: '/Sms/GetReportMessageStatus/',
            data: { id: id }
        }).done(function (data) {
            var url = data;
            var a = document.createElement('a');
            a.href = data;
            a.download = 'report.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
        }).fail(function(error) {
            alert(error)
        });
    });

    $(document).on("click", "#add-new-tgl", function (e) {

        e.preventDefault();

        document.getElementById("uploadExcelBlock").style.display = "block";
    });

    $("#cancelSms").click(function(e) {
        document.getElementById("uploadExcelBlock").style.display = "none";

    });

    $(document).on("click", "#editSmsReport", function (e) {

        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            url: '/Sms/CancelSmsReport/',
            data: { id: id },
            success: function (data) {
                if (data.success) {
                    location.reload();
                } else {
                    alert(data.message);
                }
            }
        });
    });

    $("#checkNumber").click(function (e) {
        var phone = $("#user-phone").val();
        $.ajax({
            type: "POST",
            url: '/Sms/GetUserStatus/',
            data: { phone: phone },
            success: function (data) {
                if (data.success === true) {
                    $("#sendsms-btn").click();
                } else {
                    alert(data.message);
                }
            }
        });
    });


    $('#sms-template-form-single').submit(function (e) {
        e.preventDefault();

        var template = JSON.parse($("#sms-template-single").val());
        var date = $("#sms-send-date").val();
        var phone = $("#user-phone").val();

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Sms/SendSingleSms',
            data: { templateId: template.Id, phone: phone, date: date },
            success: function (data) {
                if (data.success) {
                    document.getElementById("closeSmsForm").click();

                    alert('Сообщение отправлено');

                } else {
                    alert(data.message);
                }
            }
        });

    });
});

