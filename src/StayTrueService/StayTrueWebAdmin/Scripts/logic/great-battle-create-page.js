﻿jQuery(function ($) {
    var tourCounter = $('.st-game').length;

    function getQuestionsBlocks(questions) {
        var quesionBlock = "";
        $.each(questions,
            function(key, value) {
                quesionBlock += 
                      '<div class="battle-question">' +
                        '<div class="question-title">' +
                            '<label for="">Вопрос</label>' +
                            '<input class="poll_edit_items" id="Questions_'+ key+'__Question" name="QuestionsModel[' + key +'].Question" placeholder="Введите вопрос" type="text" value="'+ value.Question+'">' +
                        '</div>' +
                        '<button type="button" class="border-danger bg-danger text-white delete-question" data-id="'+ value.Id +'">Удалить вопрос</button>' +
                    ' </div>';
            });


        return quesionBlock;
    }

    $(function () {
        $('#Desription').summernote({
            height: 150,
            toolbar: [
                ["style", ["bold", "italic", "underline", "clear"]],
                ["font", ["strikethrough", "superscript", "subscript"]],
                ["fontsize", ["fontsize"]],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["height", ["height"]]
            ]
        });

        $(document).on('click', '.add-battle-question', function () {
            var questions = new Array();
            var questionList = $("#battle-questions .row .battle-question");

            if (questionList.length > 3) {
                alert('Максимальное количество вопросов: 4');
                return false;
            }

            $.each($(questionList),
                function (key, value) {
                    questions.push({ "Id": $(value).find("input:hidden").val(), "Question": $(value).find(".poll_edit_items").val() });
                });
            questions.push({ "Id": 0, "Question": "" });

            $("#battle-question-list").html(getQuestionsBlocks(questions));
        });

        $(document).on('click', '.delete-question', function () {
            $(this).closest('.battle-question').remove();

            var questions = new Array();
            $.each($("#battle-questions .row .battle-question"),
                function(key, value) {
                    questions.push({"Id" : $(value).find("input:hidden").val(), "Question": $(value).find(".poll_edit_items").val()});
                });

            $("#battle-question-list").html(getQuestionsBlocks(questions));

        });

        $('#createGreatBattle, #editGreatBattle').on("submit",
            function(e) {
                e.preventDefault();
                var tourList = $(".battle-levels .st-row");

                $.each($(tourList),
                    function(key, value) {
                        $(value).find("input[id*='Id']").attr("name", "ToursModel[" + key + "].Id");
                        $(value).find("input[id*='GameImage']").attr("name", "ToursModel[" + key + "].GameImage");
                        $(value).find("input[id*='GameName']").attr("name", "ToursModel[" + key + "].GameName");
                        $(value).find("input[id*='GameArchive']").attr("name", "ToursModel[" + key + "].GameArchive");
                        $(value).find("input[id*='QuizzImage']").attr("name", "ToursModel[" + key + "].QuizzImage");
                        $(value).find("input[id*='QuizzName']").attr("name", "ToursModel[" + key + "].QuizzName");
                        $(value).find("input[id*='QuizzArchive']").attr("name", "ToursModel[" + key + "].QuizzArchive");
                        $(value).find("input[id*='BeginDateTime']").attr("name", "ToursModel[" + key + "].BeginDateTime");
                        $(value).find("input[id*='EndDateTime']").attr("name", "ToursModel[" + key + "].EndDateTime");
                        $(value).find("input[id*='PointRate']").attr("name", "ToursModel[" + key + "].PointRate");
                    }); 

                var form = $(this);
                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: $(form).attr('action'),
                    data: formData,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        if (data.success) {
                            location.href = "/GreatBattle";
                        } else {
                            alert(data.debug);
                        }
                    }
                });

            });

        // -----------------------------------------------------------------------------------
        function getLevelBlock(tourCounter) {
        var levelBloack = 
            '<div class="st-row st-game on-edit">' +
                '<input data-val="true" data-val-number="Значением поля Id должно быть число." data-val-required="The Id field is required." id="ToursModel_0__Id'+tourCounter+'" name="ToursModel[0].Id" type="hidden" value="0">'+
                    '<div class="st-game-header">' +
                        '<div class="header-left">' + 
                            '<div class="battle-section">' +
                                '<div class="st-upload game_img_upload edit">'+
                                    '<label for="ToursModel_0__GameImage_'+tourCounter+'"></label>' +
                                    '<input type="file" id="ToursModel_0__GameImage_'+tourCounter+'" accept=\'image/*\' name="ToursModel[0].GameImage" class="st-upload-input user_edit_items">' +
                                '</div>' +
                                '<div class="game-title">' +
                                    '<div class="game-title">' +
                                        '<input type="text" id="ToursModel_0__GameName_'+tourCounter+'" class="game-edit-item" name="ToursModel[0].GameName" placeholder="Название игры">' +
                                    '</div>' +
                                    '<div class="game-zip" style="display: block; margin-bottom: 10px;">' +
                                        '<span>Загрузите архив игры:</span>'+
                                        '<br>'+
                                        '<input type="file" id="ToursModel_0__GameArchive_'+tourCounter+'" name="ToursModel[0].GameArchive" class="add-new-game_zip" accept=".zip">' +
                                    '</div>' +
                                    '<div class="wincoins-amount">'+
                                    '<span>Конвертация очков: </span>' +
                                        '<input type="text" class="game-edit-item" id="ToursModel_0_PointRate_'+tourCounter+' name="ToursModel[0].PointRate"">'+
                                    '</div>'+   
                               ' </div>' +
                            '</div>' +

                            '<div class="battle-section">' +
                                '<div class="st-upload game_img_upload edit">' +
                                    '<label for="ToursModel_0__QuizzImage_'+tourCounter+'"></label>' +
                                    '<input type="file" id="ToursModel_0__QuizzImage_'+tourCounter+'" accept=\'image/*\' name="ToursModel[0].QuizzImage" class="st-upload-input user_edit_items">' +
                                '</div>' +
                                '<div class="game-title">' +
                                    '<div class="game-title">' +
                                        '<input type="text" id="ToursModel_0__QuizzName_'+tourCounter+'" class="game-edit-item" placeholder="Название викторины" name="ToursModel[0].QuizzName">' +
                                    '</div>' +
                                    '<div class="game-zip" style="display: block; margin-bottom: 10px;">' +
                                        '<span>Загрузите архив викторины:</span>' +
                                        '<br>' +
                                        '<input type="file" id="ToursModel_0__QuizzArchive_'+tourCounter+'" name="ToursModel[0].QuizzArchive" class="add-new-quiz_zip" accept=".zip">' +
                                    '</div>' +
                                '</div>'+
                            '</div>'+
                        '</div>' +
                        '<div class="header-right battle-create-section">' +
                            '<div class="level-period">' +
                                '<div class="level-date">' +
                                    '<span>Дата начала: </span>' +
                                    '<input type="text" value="" id="ToursModel_0__BeginDateTime_'+tourCounter+'" class="timepicker game-edit-item" placeholder="дд/мм/гггг чч:мм" name="ToursModel[0].BeginDateTime">' +
                                    '<ion-icon name="calendar"></ion-icon>' +
                                '</div>' +
                                '<div class="level-date">' +
                                    '<span>Дата конца: </span>' +
                                    '<input type="text" value="" id="ToursModel_0__EndDateTime_'+tourCounter+'"  class="timepicker game-edit-item" placeholder="дд/мм/гггг чч:мм" name="ToursModel[0].EndDateTime">' +
                                    '<ion-icon name="calendar"></ion-icon>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="game-btns">' +
                            '<div class="st-btn battle-level-delete">' +
                                '<button type="submit" class="bg-danger">Удалить</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>'+
                '</div>';
            return levelBloack;
        }

        $(document).on('click', '#add-battle-level #add-new-tgl', function (e) {
            e.preventDefault();

           var tourList = $(".battle-levels .st-row");

            if (tourList.length >= 5) {
                alert('Максимальное количество туров: 5');
                return false;
            }

            console.log(tourCounter);
            $('.battle-levels').append(getLevelBlock(tourCounter));
            tourCounter++;
        });

        $(document).on('click', '.battle-level-delete', function () {
            $(this).closest('.st-row').remove();
        });
    });









    $('.st-game-header').click(function () {
        if ($('div.st-game.on-edit').length > 0 && !($(this).closest('.st-game').hasClass('on-edit'))) {
            editWarning();
        } else {
            if (event.target.nodeName != 'BUTTON' && !$(this).closest('.st-game').hasClass('on-edit')) {
                $('.st-game-footer').not($(this).closest('.st-game').find('.st-game-footer')).not($('#add-game').find('.st-game-footer')).slideUp();
                $(this).closest('.st-game').find('.st-game-footer').slideToggle();
            }
        }
    });

    $('#add-game #add-new-tgl').click(function () {
        if ($('div.st-game.on-edit').length > 0 && !$(this).next('.st-game').hasClass('on-edit')) {
            editWarning();
        } else {
            $(this).parent().find('.st-upload').addClass('edit');
            $(this).parent().find('.summernote').summernote({
                height: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            $(this).parent().find('.st-game').addClass('on-edit').slideDown();
            $(this).parent().find('.game-zip').show();
        }
    });

    $('.game-add').click(function () {
        $(this).closest('.st-game').slideUp();
        $(this).closest('.st-game').find('.game-zip').hide();
        $(this).closest('.st-game').find('.summernote').summernote('destroy');
        $(this).closest('.st-game').removeClass('on-edit');
    });

    $('.battle-add-abort').click(function (event) {
        $(this).closest('.st-game').slideUp();
        $(this).closest('.st-game').find('.game-zip').hide();
        $(this).closest('.st-game').removeClass('on-edit');
        $(this).closest('.st-game').find('.summernote').summernote('destroy');
        $(this).closest('form')[0].reset();
        $(this).closest('form').find('.st-upload').css('background-image', 'url(img/upload-plus.png)');
    });

});

$('input[name="Description"]').change(function () {
    $('input[name="DescriptionMobile"]').val($(this).val());
});