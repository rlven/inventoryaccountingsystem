﻿jQuery(function ($) {
    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Lottery/Delete',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });
        });

    $(".delete-btn").on("click",
        function () {
            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });
    
    $(".edit-btn, .members-list-btn").on("click",
        function (e) {
            location.href = $(this).data("url");
        });
    $("#resetLotteryMembersPage").on("click",
        function (e) {
            location.href = $(this).data("url");
        });
    $('.abort-edit-createLottery').click(function () {
        window.history.back();
    });

    $(document).on("click", "#exportExcel", function (e) {

        var lotteryId = $('#lotteryId').val();
        var filter = getFilterForm();
        console.log(lotteryId);

        $.ajax({
            type: "POST",
            url: '/Lottery/ExportExcel/',
            data: { lotteryId: lotteryId, filter: filter }
        }).done(function (data) {
            var url = data;
            var a = document.createElement('a');
            a.href = data;
            a.download = 'report.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
        }).fail(function (error) {
            alert(error)
        });
    });

    function getFilterForm() {
        return $('#FilterForm').serializeArray()
            .reduce(function (a, x) { a[x.name.replace("Filter.", "")] = x.value; return a; }, {});
    }
});