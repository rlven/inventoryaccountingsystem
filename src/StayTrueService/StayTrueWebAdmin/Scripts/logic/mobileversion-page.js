﻿$(function () {
    $(document).on("click", "#create-brand", function (e) {
        e.preventDefault();
        window.location.href = "/MobileVersion/Create";
    });

    $(document).on("click", "#create-brand-cancel", function (e) {
        e.preventDefault();
        window.location.href = "/MobileVersion/Index";
    });



    $(document).on("change", ".st-upload", function () {
        $(this).find("input[type=hidden]").val($(this).find("input[type=file]").val());
    });
});