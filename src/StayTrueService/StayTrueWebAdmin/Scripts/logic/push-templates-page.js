﻿$(function () {
    $(document).ready(function () {
        $("#popup-selected-users").text("все подписавшиеся");
    });

    $("#add-pattern #add-new-tgl").click(function () {
        var patternWrap = $(this).parent();

        patternWrap.find(".summernote").summernote({
            height: 150,
            toolbar: [
                ["style", ["bold", "italic", "underline", "clear"]],
                ["font", ["strikethrough", "superscript", "subscript"]],
                ["fontsize", ["fontsize"]],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["height", ["height"]]
            ]
        });

        patternWrap.find(".st-pattern-body").show();
    });

    $(".pattern-add-abort").click(function () {
        var patternWrap = $(this).closest("form");
        patternWrap.find(".summernote").summernote("destroy");
        patternWrap.find(".st-pattern-body").hide();
        $("#form-error").hide();
    });

    // ---------------------------------------------------------------

    $(".pattern-edit").click(function () {
        var patternWrap = $(this).closest(".st-pattern");

        patternWrap.find(".summernote").summernote({
            height: 150,
            toolbar: [
                ["style", ["bold", "italic", "underline", "clear"]],
                ["font", ["strikethrough", "superscript", "subscript"]],
                ["fontsize", ["fontsize"]],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["height", ["height"]]
            ]
        });
        patternWrap.addClass("on-edit");
    });

    $(".pattern-change-abort").click(function () {
        var patternWrap = $(this).closest(".st-pattern");
        patternWrap.find(".summernote").summernote("destroy");
        patternWrap.removeClass("on-edit");
        $("#form-error").hide();
    });

    $(document).on("submit", "#create-push-template", function (e) {
        e.preventDefault();
        var form = $(this);
        var model = getCreateForm(this);
        var content = $(model["PushContent"]).text();
        if (content.length > 0) {
            model["PushContent"] = content;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: { model },
            success: function (data) {
                if (data.success) {
                    location.reload();
                } else {
                    form.next('.formError').show();
                    var $contents = form.next('.formError').contents();
                    $contents[$contents.length - 1].nodeValue = "Проверьте введенные данные";
                }
            }
        });
    });

    $(document).on("submit", "#edit-push-template", function (e) {
        e.preventDefault();
        
        var form = $(this);
        var item = getEditForm(this);

        //console.log(item["PushContent"]);

        var content = item["PushContent"];
        
        if (content.length > 0) {
            item["PushContent"] = content;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: { item },
            success: function (data) {
                if (data.success) {
                    location.reload();
                } else {
                    form.next('.formError').show();
                    var $contents = form.next('.formError').contents();
                    $contents[$contents.length - 1].nodeValue = "Проверьте введенные данные";
                }
            }
        });
    });

    $(document).on("click", ".send-notification", function () {
        var templateName = $(this).closest(".st-row").find("#item_Name").val();
        var template = $('#push-templates option').filter(function () { return $(this).text() === templateName; }).val();
        $("#push-templates").val(template).change();
    });

    $(document).on("submit", "#push-notification-form", function (e) {
        e.preventDefault();
        var notification = getNotificationForm();
        var template = JSON.parse($("#push-templates").val());
        notification["TemplateId"] = template.Id;
        $(".fancybox-close-small").click();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Push/CreateNotification',
            data: { notification },
            success: function (data) {
                alert(data.message);
            }
        });
    });

    $(document).on("click", ".delete-template", function () {
        var templateId = $(this).closest(".st-row").find("#item_Id").val();
        $("#delete-template-form").find("input[type=hidden]").val(templateId);
    });

    $(document).on("submit", "#delete-template-form", function (e) {
        e.preventDefault();
        var id = $(this).find("input[type=hidden]").val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/PushTemplates/DeleteConfirmed/',
            data: {id: id},
            success: function (data) {
                if (data.success) {
                    $(".fancybox-close-small").click();
                    location.reload();
                } else {
                    alert("Проверьте введенные данные");
                }
            }
        });
    });

    $(document).on("change", "#push-templates", function () {
        var template = JSON.parse($(this).val());
        $("#push-title").val(template.Title);
        $("#push-content").val(template.Content);
    });

    function getEditForm(form) {
        return $(form).serializeArray()
            .reduce(function (a, x) { a[x.name.replace("item.", "")] = x.value; return a; }, {});
    }

    function getCreateForm(form) {
        return $(form).serializeArray()
            .reduce(function (a, x) { a[x.name.replace("model.", "")] = x.value; return a; }, {});
    }

    function getNotificationForm() {
        return $('#push-notification-form').serializeArray()
            .reduce(function (a, x) { a[x.name.replace("notification.", "")] = x.value; return a; }, {});
    }
});