﻿$(document).ready(function() {
    $(document).on("click", ".sms-delete", function() {
        var itemId = $(this).closest(".st-row").find("#item_Id").val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/PushReport/DeleteConfirmed',
            data: { ids: itemId },
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on("click", ".push-report", function () {
        var itemId = $(this).closest(".st-row").find("#item_Id").val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/PushReport/CreateExcelReport',
            data: { reportId: itemId },
            success: function (data, textStatus, xhr) {
                var url = data.url;
                var a = document.createElement('a');
                a.href = url;
                a.download = 'report.xlsx';
                a.click();
                window.URL.revokeObjectURL(url);
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });
});