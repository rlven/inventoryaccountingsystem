﻿jQuery(function ($) {
    $('.st-upload-input.gallery-upload').on('change', function (event) {
        if (event.target.files.length > 0) {
            for (let i = 0; i < event.target.files.length; i++) {
                var url = URL.createObjectURL(event.target.files[i]);

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm
                today = '<p>' + dd + '/' + mm + '/' + yyyy + '</p>';

                // var elementHide = '<div class="item-off"><span>Скрыть:</span><span><input type="checkbox"><span></div>';
                var elementHide = '';
                var elementDelete = '<div class="item-delete"><span>Удалить:</span><span><ion-icon name="ios-close-circle-outline"></ion-icon></span></div>';
                var elementControl = '<div class="gallery-item-controls">' + elementHide + elementDelete + '</div>';

                if (event.target.files[0]['type'].includes("image")) {
                    var currentNewElement = '<div class="image-wrap" style="background-image: url(' + url + ')"><a href="' + url + '" data-fancybox="gallery-image"></a>' + elementControl + '</div>';

                    /* var images = '<div class="gallery-item new"><div class="image-wrap" style="background-image: url(' + url + ')"><a href="' + url + '" data-fancybox="gallery-image"></a><div class="gallery-item-controls"><div class="item-delete"><span>Удалить:</span><span><ion-icon name="ios-close-circle-outline"></ion-icon></span></div></div></div><p>' + today + '</p></div>';
                    $(this).closest('.st-gallery-footer').find('.gallery-images').append(images); */
                }

                if (event.target.files[0]['type'].includes("video")) {
                    var videoElement = '<video controls="false" id="new-added-video-' + i + '"><source src = "' + url + '" type = "video/mp4"></video>';
                    var currentNewElement = '<div class="image-wrap">' + videoElement + '<a href="#new-added-video-' + i + '" data-fancybox="gallery-video"></a>' + elementControl + '</div>';

                    /* var videos = '<div class="gallery-item new"><div class="image-wrap"><video width="320" height="240" controls><source src = "' + url + '" type = "video/mp4"></video><a href="' + url + '" data-fancybox="gallery-video"></a><div class="gallery-item-controls"><div class="item-delete"><span>Удалить:</span><span><ion-icon name="ios-close-circle-outline"></ion-icon></span></div></div></div><p>' + today + '</p></div>';
                    $(this).closest('.st-gallery-footer').find('.gallery-images').append(videos); */
                }

                $(this).closest('.st-gallery-footer').find('.gallery-images').append('<div class="gallery-item new">' + currentNewElement + today + '</div>');
            }
        }
    });

    $('.st-gallery-header').click(function (event) {
        if ($('div.st-gallery.on-edit').length > 0 && !$(this).closest('.st-gallery').hasClass('on-edit')) {
            editWarning();
        } else {
            var clickedItem = event.target.tagName;
            if (clickedItem != 'INPUT' && clickedItem != 'LABEL' && clickedItem != 'A' && clickedItem != 'BUTTON' && !$(this).closest('.st-gallery').hasClass('on-edit') && $(this).closest('.st-gallery').prev().attr('id') != 'add-new-tgl') {
                $('.st-gallery-footer').not($(this).next()).not($('#add-gallery').find('.st-gallery-footer')).slideUp();
                $(this).next('.st-gallery-footer').slideToggle();
            }
        }
    });

    $('#add-gallery #add-new-tgl').click(function () {
        if ($('div.st-gallery.on-edit').length > 0) {
            editWarning();
        } else {
            $("#Name").attr("placeholder", "Название альбома");
            document.getElementById("Name").value = "";

            $(this).parent().find('.st-gallery').find('.st-gallery-footer').slideDown();
            $(this).parent().find('.st-gallery').addClass('on-edit');
            $(this).parent().find('.gallery-selectize').prop('disabled', false).addClass('st-selectize');
            $(this).parent().find('select.gallery-selectize').selectize();
            $(this).parent().find('.st-gallery').slideDown();
        }
    });

    $('.gallery-edit').click(function() {
        if ($('div.st-gallery.on-edit').length > 0) {
            editWarning();
        } else {
            $(this).closest('.gallery-btns').find('button').prop('hidden', false);
            $(this).prop('hidden', true);
            var galleryWrap = $(this).closest('.st-gallery');
            galleryWrap.find('.st-upload').addClass('edit');
            $('.st-gallery').find('.st-gallery-footer').not(galleryWrap.find('.st-gallery-footer')).slideUp();
            galleryWrap.find('.st-gallery-footer').slideDown();
            galleryWrap.find('.gallery-selectize').prop('disabled', false).addClass('st-selectize');
            galleryWrap.find('select.gallery-selectize').selectize();
            galleryWrap.find('.gallery-title input').prop('disabled', false);
            galleryWrap.find('.gallery-date input').prop('disabled', false);
            galleryWrap.addClass('on-edit');
        }
    });

    $('#createAlbumForm').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    form.closest('.add-new-wrap').find('.formError').remove();
                    if (data.success) {
                        location.reload();
                    } else {
                        form.parent().before('<div class="formError"><span><ion-icon name="alert"></ion-icon> ' + data.message + '!</span></div>');
                    }
                }
            });

        });

    $('.entity-edit').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    form.parent().prev('.formError').remove();
                    if (data.success) {
                        var btn = $(form).find('.gallery-save');
                        $(btn).closest('.gallery-btns').find('button').prop('hidden', true);
                        $(btn).closest('.gallery-btns').find('.gallery-edit').prop('hidden', false);

                        var galleryWrap = $(btn).closest('.st-gallery');
                        galleryWrap.find('.st-upload').removeClass('edit');
                        galleryWrap.removeClass('on-edit');
                        galleryWrap.find('.gallery-title input').prop('disabled', true);
                        galleryWrap.find('.gallery-date input').prop('disabled', true);

                        galleryWrap.find('select.gallery-selectize').each(function () {
                            var selectVal = $(this).val();
                            $(this)[0].selectize.destroy();
                            $(this).val(selectVal);
                        });
                        galleryWrap.find('.gallery-selectize').removeClass('st-selectize').prop('disabled', true);

                        galleryWrap.find('.st-gallery-footer').slideUp();

                    } else {
                        form.parent().before('<div class="formError"><span><ion-icon name="alert"></ion-icon> ' + data.message + '!</span></div>');
                    }
                }
            });

        });
    $(".delete-btn").on("click",
        function () {
          
            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });

    $('.gallery-item-controls .item-delete').click(function () {
        albumImageDelete(this);
    });


    $(document).on("click", '.gallery-item.new .gallery-item-controls .item-delete', function () {
        $(this).closest('.gallery-item').remove();
    });

    function albumImageDelete(block) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Gallery/AlbumImageDeleteConfirmed',
            data: {
                id: $(block).closest(".gallery-item-controls").find("#image-id").val()
            },
            success: function (data) {
                if (data.success) {
                    $(block).closest('.gallery-item').remove();
                } else {
                    alert(data.message);
                }
            }
        });
    };

    $(".gallery-images .image-status-checkbox").on("click",
        function(e) {
            var statusCheckbox = $(this);
            var status = $(this).prop("checked");

            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Gallery/AlbumImageStatusChange',
                data: { id: $(statusCheckbox).closest(".gallery-item-controls").find("#image-id").val(), status: status},
                success: function (data) {
                    if (data.success) {

                    } else {
                        alert(data.message);
                    }
                }
            });
        });

    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Gallery/DeleteConfirmed',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        location.reload();
                        $(".fancybox-close-small").click();
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });

        });

        $('.gallery-edit-abort').click(function () {
            $(this).closest('.st-gallery').prev('.formError').remove();
            $(this).closest('.gallery-btns').find('button').prop('hidden', true);
            $(this).closest('.gallery-btns').find('.gallery-edit').prop('hidden', false);
            var galleryWrap = $(this).closest('.st-gallery');
            galleryWrap.find('.st-gallery-footer').slideUp();
            galleryWrap.removeClass('on-edit');
            galleryWrap.find('.st-upload').removeClass('edit');
            galleryWrap.find('select.gallery-selectize').each(function () {
                $(this)[0].selectize.destroy();
            });
            galleryWrap.find('.gallery-selectize').removeClass('st-selectize').prop('disabled', true);
            galleryWrap.find('form')[0].reset();
            galleryWrap.find('.gallery-title input').prop('disabled', true)
            galleryWrap.find('.gallery-date input').prop('disabled', true)
        });

        $('.gallery-add-abort').click(function () {
            $(this).closest('.st-gallery').prev('.formError').remove();
            $("#Name").attr("placeholder", "Название альбома");
            document.getElementById("Name").value = "";

            $(this).closest('form')[0].reset();
            var galleryWrap = $(this).closest('.st-gallery');
            galleryWrap.find('#user_avatar_1').parent().css('background-image', 'url(img/upload-plus.png)');
            galleryWrap.slideUp();
            galleryWrap.removeClass('on-edit');
            galleryWrap.find('.gallery-item').not(galleryWrap.find('.gallery-item:first')).remove();
        });

});