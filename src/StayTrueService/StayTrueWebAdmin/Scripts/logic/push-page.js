﻿$(function () {
    $(document).ready(function() {
        showUserPanel(true, "все подписавшиеся");
    });

    $(document).on('click',
        '#operation-checkbox-column input',
        function() {
            var ids = [];
            var entityId = $('#entity-id').attr('value');
            if (typeof entityId !== typeof undefined && entityId !== false) {
                ids = JSON.parse($("#entity-id").val());
            }
            var id = $(this).prev().val();
            if (!ids.includes(id)) {
                ids.push(id);
            } else {
                var index = ids.indexOf(id);
                if (index > -1) {
                    ids.splice(index, 1);
                }
            }
            $("#entity-id").val(JSON.stringify(ids));

            if ($('#select-all-users').is(":checked")) {
                showUserPanel(true, $("#entity-count").val() - ids.length);
            } else {
                if (ids.length > 0) {
                    showUserPanel(true, ids.length);
                } else {
                    showUserPanel(true, "все подписавшиеся");
                }
            }
        });

    $(document).on('click',
        '#select-all-users',
        function() {
            checkAll($('#select-all-users').is(":checked"));
            $("#entity-id").val("[]");
            if ($(this).is(':checked')) {
                var filter = getFilterForm();
                filter.IsAllSelected = $('#select-all-users').is(":checked");
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: '/Push/CountFilteredUsers',
                    data: { filter: filter },
                    success: function(data) {
                        if (data.success) {
                            $("#entity-count").val(data.count);
                            showUserPanel(true, data.count);
                        } else {
                            alert("Проверьте введенные данные");
                        }
                    }, error: function () {
                        location.reload();
                    }
                });
            } else {
                showUserPanel(true, "все подписавшиеся");
            }
        });


    function showUserPanel(isVisiable, length = 0) {
        if (isVisiable) {
            $('.users-control-menu').find('#checked-users-amount').text(length);
            $('.users-control-menu').addClass('on');
            $('#popup-selected-users').text(length);
        } else {
            $('.users-control-menu').removeClass('on');
            $('.users-control-menu').find('#checked-users-amount').text(0);
            $('#popup-selected-users').text(0);
        }
    }

    $(document).on("click",
        "#content-list-pager a",
        function() {
            var url = $(this).attr("href").concat("&IsAllSelected=" + $('#select-all-users').is(":checked"));
            $.ajax({
                url: url,
                type: 'GET',
                cache: false,
                success: function(result) {
                    $('#content').html(result);
                    checkAll($('#select-all-users').is(":checked"));
                    checkSelected();
                }
            });
            return false;
        });
    
    $(document).on("submit", "#FilterForm", function (e) {
        e.preventDefault();
        var formData = $('#FilterForm').serializeArray();
        formData.push({ name: "isAllSelected", value: $('#select-all-users').is(":checked") });
        $.ajax({
            type: "POST",
            url: '/Push/GetUsers/',
            data: formData,
            success: function (result) {
                $('#content').html(result);
                checkAll($('#select-all-users').is(":checked"));
                $("#entity-id").val("[]");
                if ($('#select-all-users').is(":checked")) {
                    var filter = getFilterForm();
                    filter.IsAllSelected = $('#select-all-users').is(":checked");
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: '/Push/CountFilteredUsers',
                        data: { filter: filter },
                        success: function (data) {
                            if (data.success) {
                                $("#entity-count").val(data.count);
                                
                                $('.users-control-menu').find('#checked-users-amount').text(data.count);
                                $('.users-control-menu').addClass('on');
                            } else {
                                alert("Проверьте введенные данные");
                            }
                        }
                    });
                }
            }
        });
    });

$(document).on("click", "#reset-filter", function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: '/Push/GetUsers/',
        data: {},
        success: function (result) {
            $('#content').html(result);
            checkAll($('#select-all-users').is(":checked"));
            $("#entity-id").val("[]");
            $("#entity-count").val(0);
            $('.users-control-menu').removeClass('on');
            $('.users-control-menu').find('#checked-users-amount').text(0);
        }
    });
});
    
    function checkAll(check = false) {
        var items = $('#operation-checkbox-column input[type="checkbox"]');
        items.prop('checked', check);
    }


    function checkSelected() {
        var ids = [];
        var entityId = $('#entity-id').attr('value');
        if (typeof entityId !== typeof undefined && entityId !== false) {
            ids = JSON.parse($("#entity-id").val());
        }
        var items = $('#operation-checkbox-column input[type="checkbox"]');
        for (var i = 0; i < items.length; i++) {
            var id = $(items[i]).prev();
            if (ids.includes((id).val())) {
                $('#' + items[i].id).prop('checked', !$('#select-all-users').is(":checked"));
            }
        }
    }

    $(document).on("submit", "#push-notification-form", function (e) {
        e.preventDefault();
        var notification = getNotificationForm();
        var template = JSON.parse($("#push-templates").val());
        notification["TemplateId"] = template.Id;
        var filter = getFilterForm();
        filter.IsAllSelected = $("#select-all-users").is(":checked");
        var userIds = JSON.parse($("#entity-id").val());
        notification["UserIds"] = userIds;
        $(".fancybox-close-small").click();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Push/CreateNotification',
            data: { filter, notification },
            success: function (data) {
                alert(data.message);
            }, error: function () {
                location.reload();
            }
        });
    });

    $(document).on("change", "#push-templates", function () {
        var template = JSON.parse($(this).val());
        $("#push-title").val(template.Title);
        $("#push-content").val(template.Content);
    });

    function getFilterForm() {
        return $('#FilterForm').serializeArray()
            .reduce(function (a, x) { a[x.name.replace("Filter.", "")] = x.value; return a; }, {});
    }

    function getNotificationForm() {
        return $('#push-notification-form').serializeArray()
            .reduce(function (a, x) { a[x.name.replace("notification.", "")] = x.value; return a; }, {});
    }
});