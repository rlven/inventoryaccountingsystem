﻿jQuery(function ($) {

    var forms = $('.entity-edit').each(function () {
        $(this).find("#item_NewsCategory").val($(this).find(".news-category").data('category'));
        $(this).find("#item_NewsType").val($(this).find(".news-type").data('type'));
    });

    $('#add-news #add-new-tgl').click(function () {
        if ($('div.st-news.on-edit').length > 0) {
            editWarning();
        } else {
            document.getElementById("NameRu").value = "";
            document.getElementById("DescriptionRu").value = "";
            document.getElementById("ImageP").value = "";
            document.getElementById("ImageM").value = "";
            document.getElementById("VideoP").value = "";
            document.getElementById("VideoM").value = "";
        //    document.getElementById("NewsType").checked = false;
            document.getElementById('uploadip').style.backgroundImage = "url('/Content/img/news-web-image-upload.png')";
            document.getElementById('uploadim').style.backgroundImage = "url('/Content/img/news-app-image-upload.png')";
            document.getElementById('uploadvp').style.backgroundImage = "url('/Content/img/news-web-video-upload.png')";
            document.getElementById('uploadvm').style.backgroundImage = "url('/Content/img/news-app-video-upload.png')";

            $(this).parent().find("#NewsCategory").selectize(
                {
                    allowEmptyOption: true
                });
            $(this).parent().find("#IsBrand").prop("disabled", false);
            $(this).parent().find("#IsBrand").selectize({
                allowEmptyOption: true
            });

            $(this).parent().find('.st-news').find('.summernote').summernote({
                height: 250,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            $(this).parent().find('.st-news').addClass('on-edit');
            $(this).parent().find('.st-news').show();
        }
    });

    $('.news-edit').click(function () {
        if ($('div.st-news.on-edit').length > 0) {
            editWarning();
        } else {
            var userWrap = $(this).closest('.st-news');
            $(this).closest('.btns-wrap').find('button').prop('hidden', false);
            $(this).prop('hidden', true);
            userWrap.find('.news-abort').prop('hidden', false);
            userWrap.find('.news-publish').prop('hidden', false);
            userWrap.addClass('on-edit');
            userWrap.find('.st-upload').addClass('edit');
            userWrap.find('.news_edit_items').prop('disabled', false);
            userWrap.find('select.news_edit_items').selectize({
                allowEmptyOption: true
            });
            userWrap.find('.summernote').summernote({
                height: 250,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
        }
    });

    $('.news-abort').click(function () {
        var userWrap = $(this).closest('.st-news');
        userWrap.find('.news-abort').prop('hidden', true);
        userWrap.find('.news-publish').prop('hidden', true);
        userWrap.find('.news-delete').prop('hidden', true);
        userWrap.find('.news-edit').prop('hidden', false);
        userWrap.removeClass('on-edit');
        userWrap.find('.st-upload').removeClass('edit');
        userWrap.find('.news_edit_items').prop('disabled', true);
        userWrap.find('.datepicker').prop('disabled', true);
        userWrap.find('select.news_edit_items').each(function () {
            var selectVal = $(this).val();
            $(this)[0].selectize.destroy();
            $(this).val(selectVal);
        });
        userWrap.find('.summernote').summernote('reset');
        userWrap.find('.summernote').summernote('destroy');
        $(this).closest('form')[0].reset();
        userWrap.next('.formError').hide();
    });

    $('.news-adding-abort').click(function () {
        document.getElementById("NameRu").value = "";
        document.getElementById("DescriptionRu").value = "";
        document.getElementById("NewsType").checked = false;
        document.getElementById("news-visibility-add").checked = false;

        var newsWrap = $(this).closest('.st-news');
        newsWrap.hide();
        newsWrap.removeClass('on-edit');
        newsWrap.find('.summernote').summernote('destroy');
        $(this).closest('form')[0].reset();
        newsWrap.find('#news_web_img_new').parent().css('background-image', 'url(img/news-web-image-upload.png)');
        newsWrap.find('#news_web_video_new').next().remove();
        newsWrap.find('#news_app_img_new').parent().css('background-image', 'url(img/news-app-image-upload.png)');
        newsWrap.find('#news_app_video_new').next().remove();

        newsWrap.next().hide();
    });

    $('#createNewsForm').on("submit", function (e) {
        e.preventDefault();
        var form = $(this);
        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                form.closest('.st-news').css('opacity', '.5');
                form.closest('.st-news').find('.news-publish').prop('disabled', true).text('Загрузка...').css('color', '#fff');
            },
            complete: function () {
                form.closest('.st-news').css('opacity', '1');
                form.closest('.st-news').find('.news-publish').prop('disabled', false).text('Опубликовать');
            },
            success: function (data) {
                if (data.success) {
                    location.reload();
                } else {
                    form.closest('.st-news').next('.formError').show();
                    var $contents = form.closest('.st-news').next('.formError').contents();
                    $contents[$contents.length - 1].nodeValue = "Проверьте введенные данные";
                }
            }
        });
    });

    $('.entity-edit').on("submit", function (e) {
        e.preventDefault();
        var form = $(this);
        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                form.closest('.st-news').css('opacity', '.5');
                form.closest('.st-news').find('.news-publish').prop('disabled', true).text('Загрузка...').css('color', '#fff');
            },
            complete: function () {
                form.closest('.st-news').css('opacity', '1');
                form.closest('.st-news').find('.news-publish').prop('disabled', false).text('Опубликовать');
            },
            success: function (data) {
                if (data.success) {
                    form.closest('.st-news').next('.formError').hide();

                    var btn = $(form).find('.news-edit');

                    userWrap = $(btn).closest('.st-news');

                    userWrap.find('.news-abort').prop('hidden', true);
                    userWrap.find('.news-publish').prop('hidden', true);
                    userWrap.find('.news-delete').prop('hidden', true);
                    userWrap.find('.news-edit').prop('hidden', false);

                    userWrap.removeClass('on-edit');
                    userWrap.find('.st-upload').removeClass('edit');
                    userWrap.find('.news_edit_items').prop('disabled', true);
                    userWrap.find('.datepicker').prop('disabled', true);
                    userWrap.find('select.news_edit_items').each(function () {
                        var selectVal = $(this).val();
                        $(this)[0].selectize.destroy();
                        $(this).val(selectVal);
                    });
                    userWrap.find('.summernote').summernote('destroy');

                } else {
                    form.closest('.st-news').next('.formError').show();
                    var $contents = form.closest('.st-news').next('.formError').contents();
                    $contents[$contents.length - 1].nodeValue = "Проверьте введенные данные";
                }
            }
        });
    });

    $(".delete-btn").on("click",
        function () {

            $("#entity-id").val($(this).data("id"));
            console.log($("#entity-id").val());
        });

    $("#delete-form").on("submit",
        function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/News/DeleteConfirmed',
                data: { id: $("#entity-id").val() },
                success: function (data) {
                    if (data.success) {
                        $("button[data-id='" + $("#entity-id").val() + "']").closest('.st-news').remove();
                        $(".fancybox-close-small").click();
                    } else {
                        $('.formError').show();
                        var $contents = $('.formError').contents();
                        $contents[$contents.length - 1].nodeValue = "Проверьте введенные данные";
                    }
                }
            });
        });
});