﻿jQuery(function ($) {

    function getQuestionsBlock(questions) {
        var questionsBlocks = "";
        if ($(questions).length > 0) {
       
        $.each(questions, function (key, value) {
            
            var quesionBlock = '<div class="poll-question">' +
                                '<input type="hidden" value=\'' +JSON.stringify(value)+'\' class="question-body">' +
                                '<div class="question-top">'+
                                    '<div class="row">'+
                                        '<div class="col-md-7">'+
                                            '<label for="">Вопрос</label>'+
                                            '<input class="poll_edit_items" id="Questions_' + key + '__Text" name="Questions[' + key + '].Text" placeholder="Введите вопрос" type="text" value="' + value.Text+ '">'+
                                       ' </div>'+
                                       ' <div class="col-md-2">'+
                                           ' <label for="">Тип ответа</label>'+
                                          ' <select class="st-selectize poll_edit_items answer-type" id="Questions_' + key + '__AnswerType" name="Questions[' + key + '].AnswerType">'+
                                                '<option value="select" ' + (value.AnswerType === "select"? "selected='selected'" : "") +'>Один выбор</option>'+
                                               ' <option value="multiple-select" ' + ( value.AnswerType === "multiple-select"? "selected='selected'" : "") +'>Множественный выбор</option>'+
                                               ' <option value="text" ' + ( value.AnswerType === "text"? "selected='selected'" : "" ) +'>Текстовый</option>'+
                                           ' </select>'+
                                       ' </div>'+
                                       ' <div class="col-md-3">' +
                                           ' <label for="">Пользователь может выбрать</label> '+
                                            ' <input class="poll_edit_items answer-quantity" readonly id="Questions_' + key + '__SizeOfAvailableAnswers" name="Questions[' + key + '].SizeOfAvailableAnswers" placeholder="Введите количество" type="text" value="' + value.SizeOfAvailableAnswers+'"> ' +
                                       ' </div>' +
                                   ' </div>' +
                               '</div>' +
                               ' <div class="question-answers">' +
                                   '  <div class="question-answers-top">' +
                                       ' <label>Варианты</label>' +
                                       ' <button type="button" class="border-primary bg-primary text-white add-new-answer">Добавить вариант</button>' +
                                   ' </div>' +
                                   ' <div class="row">'+ 
                                        '<input type="hidden" value="' + key + '" class="question-number" >' +
                                        getAnswers(value.Answers, key) +
                                    '</div>' +
                               ' </div>'+
                               ' <div class="qustion-bottom">' +
                                   ' <button type="button" class="border-danger bg-danger text-white delete-question">Удалить вопрос</button>' +
                               ' </div>' +
                        '  </div>';
            questionsBlocks += quesionBlock;

            });
           
        }
        return questionsBlocks;
    }

    function getQuestionsArray() {
        var questions = new Array();
        var qBlocks = $("#poll-question-list .poll-question");
        $.each(qBlocks,
            function (key, value) {
                var answers = [];
                $.each($(value).find(".question-answers .row .question-answers-item input"),
                    function(anskey, ansValue) {
                        answers.push($(ansValue).val());
                    });
                questions.push({
                    Text: $(value).find("input[name*='.Text']").val(),
                    AnswerType: $(value).find("select").val(),
                    SizeOfAvailableAnswers: parseInt($(value).find("input[name*='.SizeOfAvailableAnswers']").val()),
                    'Answers': answers
                });
            });
        return questions;
    }


    function getAnswers(answers, questionId) {
        var answerBlocks = "";

        $.each(answers,
            function(key, value) {
                var answerBlock = '<div class="col-md-3 question-answers-item">' +
                    ' <input type="text" id="Questions_' +
                    questionId +
                    '__Answers_' + key + '_" placeholder="Введите вариант ответа" value="' + value +'" name="Questions[' + questionId +'].Answers[' + key+']">' +
                    ' <button type="button" class="border-danger bg-danger text-white answer-delete">Удалить</button>' +
                    '</div>';
                answerBlocks += answerBlock;
               
            });
        return answerBlocks;
    }



    $(function () {
        $('#poll-description').summernote({
            height: 150
        });

        $(document).on('click', '.poll-question-add', function () {
            var questions = getQuestionsArray();
            
            var item = { Text: "", AnswerType: "select", SizeOfAvailableAnswers: 1 };
            questions.push(item);

            $("#questionsArray").val(JSON.stringify(questions));

            var questionBlocksArray = getQuestionsBlock(questions);
            $('#poll-question-list').empty();

            $('#poll-question-list').prepend(questionBlocksArray);
            console.log('click');
        });

        $(document).on('click', '.delete-question', function () {
            $(this).closest('.poll-question').remove();

            var questions = getQuestionsArray();
            $("#questionsArray").val(JSON.stringify(questions));

            var questionBlocksArray = getQuestionsBlock(questions);
            $('#poll-question-list').empty();

            $('#poll-question-list').prepend(questionBlocksArray);
        });

        $(document).on('change', '.answer-type', function () {
            console.log('changed');
            if ($(this).val() === 'multiple-select') {
                $(this).closest('.question-top').find('.answer-quantity').prop('readonly', false);
            } else {
                $(this).closest('.question-top').find('.answer-quantity').prop('readonly', true).val('');
                $(this).closest('.question-top').find('.answer-quantity').val("1");
            }
        });

        $(document).on('click', '.add-new-answer', function () {
            var questionsRow = $(this).closest('.question-answers').find('.row');
            if (questionsRow.find('.question-answers-item').length < 8) {

                var answers = new Array();
                $.each($(questionsRow.find(".question-answers-item")), function(key, value) {
                    answers.push($(value).find("input").val());
                });
               
                answers.push("");
                questionsRow.find('.question-answers-item').remove();
                questionsRow.append(getAnswers(answers, questionsRow.find('.question-number').val()));

            } else {
                alert('Максимальное количество вариантов: 8');
            }
        });

        $(document).on('click', '.answer-delete', function () {
            var questionsRow = $(this).closest('.question-answers').find('.row');
            $(this).closest('.question-answers-item').remove();

            var answers = new Array();
            $.each($(questionsRow.find(".question-answers-item")), function (key, value) {
                answers.push($(value).find("input").val());
            });

            questionsRow.find('.question-answers-item').remove();
            questionsRow.append(getAnswers(answers, questionsRow.find('.question-number').val()));

        });

        $(document).ready(function () {
            $.each($(".content-body .st-row.st-poll .poll-questions-count"),
                function(key, value) {
                    $(value).next("span").text(JSON.parse($(value).val()).length);
                });
        });

        $(document).on('click', '.questionnaire-delete', function () {
            var item = $(this);
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/Questionnaire/Delete",
                data: { 'id': $(this).closest('.st-row.st-poll').find('#item_Id').val() },
                success: function (data) {
                    if (data.success) {
                       location.reload();
                    } else {
                        alert(data.message);
                    }
                }
            });

        });
      
        $(document).on("submit", "#createQuestionnaire", function (e) {
           /* if ($("#poll-title-input").val() === "") {
                alert("Поле Название необходимо заполнить");
                return false;
            }*/

            if ($("#poll-question-list .poll-question").length === 0) {
                alert( "Необходимо добавить хотя бы один вопрос");
                return false;
            }

         /*  if ($("#poll-create-at").val() === "") {
                alert("Поле Дата начала обязательное");
                return false;
            }

            if ($("#poll-end-date").val() === "") {
                alert("Поле Дата окончания обязательное");
                return false;
            }
            */
            return true;


        });

    });

});