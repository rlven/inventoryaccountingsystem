﻿$(document).ready(function () {
    $("#delete-great-battle-user").click(function () {
        var battleMembers = {
            membersId: $("input[id='greatBattleMembers[]']:checkbox:checked").map(function () { return $(this).val(); }).get(),
            greatBattleId: $("#great-battle-id").val()
        }
        $.ajax({
            url: 'GreatBattleMembers/BlockMembers',
            type: 'POST',
            data: JSON.stringify(battleMembers),
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $("#unblock-great-battle-user").click(function () {
        var battleMembers = {
            membersId: $("input[id='greatBattleMembers[]']:checkbox:checked").map(function () { return $(this).val(); }).get(),
            greatBattleId: $("#great-battle-id").val()
        }
        $.ajax({
            url: 'GreatBattleMembers/UnblockMembers',
            type: 'POST',
            data: JSON.stringify(battleMembers),
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, xhr) {
                location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on("click", "#exportExcel", function (e) {

        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            url: '/GreatBattleMembers/GetBattleUsersReport/',
            data: { id: id }
        }).done(function (data) {
            var url = data;
            var a = document.createElement('a');
            a.href = data;
            a.download = 'report.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
        }).fail(function (error) {
            alert(error)
        });
    });

});