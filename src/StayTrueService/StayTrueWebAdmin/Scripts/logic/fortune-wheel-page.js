﻿$(document).on("submit", "#create-form", function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var formData = getFormData("#create-form")
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: $("#create-form").attr("action"),
        data: formData,
        success: function (data) {
            if (data.success) {
                window.location.href = "/FortuneWheel/Index"
            }
            else {
                alert(data.debug);
            }
        }
    });
});

$(document).on("submit", "#edit-form", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var formData = getFormData("#edit-form");

    $.ajax({
        type: "POST",
        dataType: "json",
        url: $("#edit-form").attr("action"),
        data: formData,
        success: function (data) {
            if (data.success) {
                window.location.href = "/FortuneWheel/Index"
            }
            else {
                alert(data.debug);
            }
        }
    });
});

//Clone block
$(document).on("click", ".add-battle-question", function () {
    var question = $("#battle-questions .row .battle-question").first();
    var clone = question.clone(true);
    $(clone).find("input").val(0);
    $(clone).find('.wheel-component-product').val("");
    $(clone).find(".wheel-component-product").parent().show();
    $(clone).find(".wheel-component-wincoins").parent().hide();
    $(clone).find('.wheel-component-is-product').prop("checked", true);
    $("#battle-question-list").append(clone);
});

$(document).on("click", '.switch-component-type', function () {
    var component = $(this).closest(".battle-question");
    var checkbox = $(component).find(".wheel-component-is-product");
    checkbox.prop('checked', !checkbox.is(':checked'));
    $(component).find(".wheel-component-wincoins").val(0);
    $(component).find('.wheel-component-product').val("");
    if ($(checkbox).is(':checked')) {
        component.find(".wheel-component-product").parent().show();
        component.find(".wheel-component-wincoins").parent().hide();
    } else {
        component.find(".wheel-component-product").parent().hide();
        component.find(".wheel-component-wincoins").parent().show();
    }
});

$(document).on('click', '.delete-question', function () {
    if ($(".battle-question").length > 1) {
        var component = $(this).closest('.battle-question');
        var id = component.find(".wheel-component-id").val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/FortuneWheel/DeleteComponent?id=" + id,
            success: function (data) {
                if (data.success) {
                    component.closest('.battle-question').remove();
                }
                else {
                    alert(data.message);
                }
            }
        });
    } else {
        alert("Минимум 1 слот обязателен");
    }
});

$(document).ready(function () {
    var components = $("#battle-questions .row .battle-question");
    for (var i = 0; i < components.length; i++) {
        if ($(components[i]).find('.wheel-component-is-product').is(":checked")) {
            $(components[i]).find(".wheel-component-product").parent().show();
            $(components[i]).find(".wheel-component-wincoins").parent().hide();
        }
        else {
            $(components[i]).find(".wheel-component-product").parent().hide();
            $(components[i]).find(".wheel-component-wincoins").parent().show();
        }
    }
});

$(document).on("submit", "#delete-form", function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/FortuneWheel/Delete?Id=' + $("#entity-id").val(),
        success: function (data) {
            if (data.success) {
                location.reload();
            } else {
                alert(data.message);
            }
        }
    });
});

$(document).on("submit", "#activate-jackpot-form", function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/FortuneWheel/ActivateJackpot?Id=' + $("#entity-id").val(),
        success: function (data) {
            if (data.success) {
                location.reload();
            } else {
                alert(data.message);
            }
        }
    });
});

$(document).on("click", ".edit-btn", function (e) {
    location.href = $(this).data("url");
});

$(document).on("click", ".delete-btn", function () {
    $("#entity-id").val($(this).data("id"));
});

$(document).on("click", ".cancel-edit", function () {
    window.history.back();
});

$(document).on("click", ".activate-jackpot", function () {
    $("#entity-id").val($(this).attr('data-id'));
});

function getFormData(form) {
    var formData = $(form).serializeArray();
    var listComponents = $(".battle-question");
    for (var i = 0; i < listComponents.length; i++) {
        formData.push({ name: "Components[" + i + "][Id]", value: $(listComponents[i]).find(".wheel-component-id").val()});
        formData.push({ name: "Components[" + i + "][IsProduct]", value: $(listComponents[i]).find(".wheel-component-is-product").is(":checked") });
        formData.push({ name: "Components[" + i + "][ProductId]", value: $(listComponents[i]).find(".wheel-component-product option:selected").val() });
        formData.push({ name: "Components[" + i + "][Wincoins]", value: $(listComponents[i]).find(".wheel-component-wincoins").val() });
    }
    return formData;
}