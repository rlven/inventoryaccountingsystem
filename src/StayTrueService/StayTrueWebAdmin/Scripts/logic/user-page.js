﻿$(function () {
    
    $(document).on('click', ".toggle-table", function () {
        $(this).toggleText("Посмотреть все", "Свернуть");
        $(this)
            .closest(".statistic-section")
            .toggleClass("extended");
    });


    $(document).on('keypress', 'input.ban-end-date-input', function (e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    function createUsersReport(event) {
        event.preventDefault();
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        var ids = JSON.parse($("#entity-id").val());

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Users/CreateExcelReport/',
            data: { filter: filter, ids: ids },
            success: function (data) {
                if (data.success) {
                    var url = data.url;
                    var a = document.createElement('a');
                    a.href = url;
                    a.download = 'report.xlsx';
                    a.click();
                    window.URL.revokeObjectURL(url);
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    }

    $(document).on('click', '#excel-export-users', createUsersReport);
    //--- Открыть подробности пользователя -----------------------------------------

    function editWarning() {
        $.fancybox.open({
            src: '#edit-warning'
        });
        setTimeout(function () {
            $.fancybox.close({
                src: '#edit-warning'
            });

            $('.content').animate({
                scrollTop: $(".on-edit").offset().top
            }, 400);
        }, 800);
    }

    function changeStateToDefault(userWrap) {
        userWrap.find('.st-user-body').slideUp();
        userWrap.removeClass('on-edit');
        userWrap.find('.st-upload').removeClass('edit');
        userWrap.find('.user_edit_items').prop('disabled', true);
        userWrap.find('.promocode').prop('disabled', true);
        userWrap.find('.st-user-header').find('.datepicker').prop('disabled', true);
        userWrap.find('select.user_edit_items').each(function () {
            $(this)[0].selectize.destroy();
        });
    }

    $(document).on('click', '.st-user-header', function (event) {
        if (event.target.nodeName !== 'BUTTON' && !$(this).parent().hasClass('on-edit')) {
            if ($('div.st-user.on-edit').length > 0) {
                editWarning();
            } else {
                $('.st-user-body').not($(this).parent().find('.st-user-body')).slideUp();
                $(this).closest('.st-user').find('.st-user-body').slideToggle();
                var block = $(this);
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: '/Users/GetPassport',
                    data: { id: $(block).find("#item_Id").val() },
                    success: function (data) {
                        if (data.success) {
                            //alert(JSON.stringify(data));

                            var entityEdit = $(block).closest('.entity-edit');
                            if (data.front != null) {
                                var urlFront = data.front.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
                                //download(urlFront);
                                $(entityEdit).find(".st-user-body").find(".user-passport-image .front").css("background-image", "url(" + urlFront + ")");
                                $(entityEdit).find(".st-user-body").find(".user-passport-image .front a").attr("href", urlFront);
                            }

                            if (data.back != null) {
                                var urlBack = data.back.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
                                $(entityEdit).find(".st-user-body").find(".user-passport-image .back").css("background-image", "url(" + urlBack + ")");
                                $(entityEdit).find(".st-user-body").find(".user-passport-image .back a").attr("href", urlBack);
                            }

                            
                        } else {
                            alert("Проверьте введенные данные");
                        }
                    }
                });
            }
        }
    });

    $(document).on("click", "#download-passport", function() {
        var stBody = $(this).closest(".st-user-body");
        console.log(stBody);
        var front = stBody.find("#passport-front-url").prop("href");
        var back = stBody.find("#passport-back-url").prop("href");
        download(front);
        download(back);
    });
    
    $(document).on('click', '#operation-checkbox-column input[type=checkbox]', function (e) {
        e.stopPropagation();
    });

    //--- Подготовка полей пользователя к редактированию -----------------------------------------

    $(document).on('click', '.user-edit', function () {
        if ($('div.st-user.on-edit').length > 0) {
            editWarning();
            console.log('warning');
        } else {
            $(this).parent().find('button').prop('hidden', false);

            $(this).prop('hidden', true);
            var userWrap = $(this).closest('.st-user');
            userWrap.addClass('on-edit');
            userWrap.find('.st-upload').addClass('edit');
            $('.st-user-body').not(userWrap.find('.st-user-body')).slideUp();
            userWrap.find('.st-user-body').slideDown();
            userWrap.find('.user_edit_items').prop('disabled', false);
            userWrap.find('.datepicker').prop('disabled', false);
            userWrap.find('.promocode').prop('disabled', false);
            userWrap.find('select.user_edit_items').selectize({
                sortField: 'value',
                allowEmptyOption: true,
                placeholder: $(this).attr('placeholder'),
                onFocus: function () {
                    $(this).scrollFix();
                },
                onBlur: function () {
                    $(this).scrollFix();
                },
                onDropdownClose: function () {
                    $(this).scrollFix();
                },
                onDropdownOpen: function () {
                    $(this).scrollFix();
                },
            });
        }
    });

    $(document).on('click', '.user-change-abort', function () {
        $(this).parent().find('button').prop('hidden', true);
        $(this).parent().find('.user-edit').prop('hidden', false);
        var userWrap = $(this).closest('.st-user');
        changeStateToDefault(userWrap);
        $(this).closest('form')[0].reset();
    });


    //-------------- Выбор пользователей в списке ---------------------
    

    $(document).on('click', '#selectUserActive', function (e) {
        $("#filterMode").val(1);
        $("#FilterForm").submit();
        checkAll($("#select-all-users").is(":checked"));
        $("#entity-id").val("[]");
        if ($("#select-all-users").is(':checked')) {
            var filter = getFilterForm();
            filter.IsAllSelected = $('#select-all-users').is(":checked");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Users/CountFilteredUsers',
                data: { filter: filter },
                success: function (data) {
                    if (data.success) {
                        $("#entity-count").val(data.count);
                        showUserPanel(true, data.count);
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });
        } else {
            showUserPanel(false);
        }
    });
    
    $(document).on('click', '#selectUserBanned', function () {
        $("#filterMode").val(2);
        $("#FilterForm").submit();
        checkAll($('#select-all-users').is(":checked"));
        $("#entity-id").val("[]");
        if ($("#select-all-users").is(':checked')) {
            var filter = getFilterForm();
            filter.IsAllSelected = $('#select-all-users').is(":checked");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Users/CountFilteredUsers',
                data: { filter: filter },
                success: function (data) {
                    if (data.success) {
                        $("#entity-count").val(data.count);
                        showUserPanel(true, data.count);
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });
        } else {
            showUserPanel(false);
        }
    }); 

    $(document).on('click', '#selectUserNotConfirmed', function () {
        $("#filterMode").val(3);
        $("#FilterForm").submit();
        checkAll($('#select-all-users').is(":checked"));
        $("#entity-id").val("[]");
        if ($("#select-all-users").is(':checked')) {
            var filter = getFilterForm();
            filter.IsAllSelected = $('#select-all-users').is(":checked");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/Users/CountFilteredUsers',
                data: { filter: filter },
                success: function (data) {
                    if (data.success) {
                        $("#entity-count").val(data.count);
                        showUserPanel(true, data.count);
                    } else {
                        alert("Проверьте введенные данные");
                    }
                }
            });
        } else {
            showUserPanel(false);
        }
    });
    

    //--------------------------------------------------------------------

    $(document).on('change', '#sms-template', function (e) {
        getSmsMessagesCost(e);
    });

    $(document).on('input', '#smsText', function (e) {
        getSmsMessagesCost(e);
    });



    function getSmsMessagesCost(e) {

        var smsTemplateId = $("#sms-template").val();

        var id = JSON.parse(smsTemplateId).Id;

        var selectedUsersCount = document.getElementById("smsFormCheckedUsersAmount").innerHTML;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/Sms/GetMessageCostUserPage',
            data: { id: id },
            success: function (data) {

                $('#smsSendCost').text(data * selectedUsersCount);
                $('#smsCost').val(data * selectedUsersCount);
            },
            error: function () {
                location.reload();
            }
        });
    }

    $(document).on('click', '.user-bun-btn', function () {
        var ids = [];
        ids.push($(this).closest('.st-user').find('.operation_checkbox').prev().val());
        $("#entity-id").val(JSON.stringify(ids));
    });

    $(document).on('click', '.delete-btn', function () {
        var ids = [];
        ids.push($(this).data("id"));
        $("#entity-id").val(JSON.stringify(ids));
        console.log($("#entity-id").val());
    });

    $(document).on("submit", "#user-delete-form", function (e) {
        e.preventDefault();
        var ids = JSON.parse($("#entity-id").val());
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Users/DeleteConfirmed',
            data: { filter: filter, ids: ids },
            success: function (data) {
                if (data.success) {
                    location.reload();
                } else {
                    alert("Проверьте введенные данные");
                }
            },
            error: function () {
                location.reload();
            }
        });
    });

    $(document).on("submit", "#ban-form", function (e) {
        e.preventDefault();
        var form = $(this);
        var reason = $(this).find('.ban-warn-text').val();
        //var unblockDate = $(this).find('.ban-end-date-input').datepicker('getDate');
        var unblockDate = $(this).find('.ban-end-date-input').datepicker({ dateFormat: "dd/mm/yy" }).val();
        var ids = JSON.parse($("#entity-id").val());
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        if (unblockDate === "" || reason === "") {
            alert("Поля обязательны для заполнения");
            return false;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Users/BanConfirmed',
            data: {filter: filter, ids: ids, reason: reason, unblockDate: unblockDate },
            success: function (data) {
                if (data.success) {
                    if ($('#select-all-users').is(":checked")) {
                        var items = $('#operation-checkbox-column input[type="checkbox"]:checked');
                        ids = [];
                        for (var i = 0; i < items.length; i++) {
                            var id = $(items[i]).prev();
                            ids.push($(id).val());
                        }
                    }
                    for (var i = 0; i < ids.length; i++) {
                        $("#account_statuses_" + ids[i]).val("2");
                        $("#operation_checkbox_" + ids[i]).prop('checked', false);
                        $("#operation_checkbox_" + ids[i]).closest('.st-user').find('.user-bun-btn').prop('hidden', true);
                    };
                    $(".fancybox-close-small").click();
                    $('.users-control-menu').removeClass('on');
                    $(form).trigger("reset");
                    $("#select-all-users").prop("checked", false);
                } else {
                    alert("Проверьте введенные данные");
                }
            },
            error: function () {
                location.reload();
            }
        });
    });

    $(document).on("submit", "#user-unban-popup", function (e) {
        e.preventDefault();
        var form = $(this);
        var ids = JSON.parse($("#entity-id").val());
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Users/UnbanConfirmed',
            data: {filter: filter, ids: ids },
            success: function (data) {
                if (data.success) {
                    if ($('#select-all-users').is(":checked")) {
                        var items = $('#operation-checkbox-column input[type="checkbox"]:checked');
                        ids = [];
                        for (var i = 0; i < items.length; i++) {
                            var id = $(items[i]).prev();
                            ids.push($(id).val());
                        }
                        $("select-all-users").trigger('click');
                    }
                    for (var i = 0; i < ids.length; i++) {
                        $("#account_statuses_" + ids[i]).val("1");
                        $("#operation_checkbox_" + ids[i]).trigger('click');
                    }
                    $("#select-all-users").prop("checked", false);
                    $(".fancybox-close-small").click();
                    $('.users-control-menu').removeClass('on');
                } else {
                    alert("Проверьте введенные данные");
                }
            },
            error: function () {
                location.reload();
            }
        });
    });

    $(document).on("submit", "#user-confirm-popup", function (e) {
        e.preventDefault();
        var ids = JSON.parse($("#entity-id").val());
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        $.ajax({
            type: "POST",
            url: '/Users/ActivateConfirmed/',
            dataType: 'json',
            data: { filter, ids },
            success: function (data) {
                if (data.success) {
                    if ($('#select-all-users').is(":checked")) {
                        var items = $('#operation-checkbox-column input[type="checkbox"]:checked');
                        ids = [];
                        for (var i = 0; i < items.length; i++) {
                            var id = $(items[i]).prev();
                            ids.push($(id).val());
                        }
                    }
                    for (var i = 0; i < ids.length; i++) {
                        $("#account_statuses_" + ids[i]).val("1");
                        $("#operation_checkbox_" + ids[i]).prop('checked', false);
                    }
                    $("#select-all-users").prop("checked", false);
                    $(".fancybox-close-small").click();
                    $('.users-control-menu').removeClass('on');
                } else {
                    alert("Проверьте введенные данные");
                }
            },
            error: function() {
                location.reload();
            }
        });
    });

    $(document).on("submit", '.entity-edit', function (e) {
        e.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        var submitBtn = form.find('button:submit');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                form.find('.formError').remove();

                if (data.success) {
                    submitBtn.parent().find('.user-save').prop('hidden', true);
                    submitBtn.parent().find('.user-change-abort').prop('hidden', true);
                    submitBtn.parent().find('.user-edit').prop('hidden', false);
                    var userWrap = submitBtn.closest('.st-user');
                    userWrap.find('.st-user-body').slideUp();
                    userWrap.removeClass('on-edit');
                    userWrap.find('.st-upload').removeClass('edit');
                    userWrap.find('.user_edit_items').prop('disabled', true);
                    userWrap.find('.promocode').prop('disabled', true);
                    userWrap.find('.st-user-header').find('.datepicker').prop('disabled', true);
                    userWrap.find('select.user_edit_items').each(function () {
                        var selectVal = $(this).val();
                        $(this)[0].selectize.destroy();
                        $(this).val(selectVal);
                    });
                } else {
                    form.find('.st-user-header')
                        .after('<div class="formError"><span><ion-icon name="alert"></ion-icon>' + data.message + '</span></div>');
                }
            },
            error: function () {
                location.reload();
            }
        });
    });

    $(document).on("submit", "#user-add-to-chat-form", function (e) {
        e.preventDefault();
        var form = $(this);
        var ids = JSON.parse($("#entity-id").val());
        var radio = $('input[name="chatAddGroupId"]:checked');
        var id = $(radio).val();
        var filter = getFilterForm();
        filter.IsAllSelected = $('#select-all-users').is(":checked");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(form).attr('action'),
            data: { chatGroupId: id, filter: filter, userIds: ids },
            success: function (data) {
                if (data.success) {
                    if ($('#select-all-users').is(":checked")) {
                        var items = $('#operation-checkbox-column input[type="checkbox"]:checked');
                        ids = [];
                        for (var i = 0; i < items.length; i++) {
                            var id = $(items[i]).prev();
                            ids.push($(id).val());
                        }
                    }
                    for (var i = 0; i < ids.length; i++) {
                        $("#account_statuses_" + ids[i]).val("1");
                        $("#operation_checkbox_" + ids[i]).trigger('click');
                    }
                    $("#select-all-users").prop("checked", false);
                    $(radio).closest(".chatgroup").find(".chatgroup-user-count span").html(data.data);
                    $(".fancybox-close-small").click();
                    $('.users-control-menu').removeClass('on');
                    //   window.location.reload();
                } else {
                    alert(data.message);
                }
            },
            error: function () {
                location.reload();
            }
        });
    });

    //отправка сообщения
    $('[data-fancybox="sms-send-form"]').fancybox({
        afterClose: function (instance, current) {
            $("#sms-template-form")[0].reset();
            $('#smsSendCost').text("0");
        }
    });


    function download(dataUrl) {
        var url = window.URL.createObjectURL(dataUrItoBlob(dataUrl));
        var link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'passport.jpg');
        document.body.appendChild(link);
        link.click();
    }

    function dataUrItoBlob(dataUri, callback) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataUri.split(',')[1]);

        // separate out the mime component
        var mimeString = dataUri.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // write the ArrayBuffer to a blob, and you're done
        var bb = new Blob([ab]);
        return bb;
    }

    $("#filters-header").keydown(function (e) {
        if (e.keyCode === 13) {
            $("#FilterForm").submit();
            e.preventDefault();
        }
    });
});
