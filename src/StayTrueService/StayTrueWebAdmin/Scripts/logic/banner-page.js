﻿$('#add-news #add-new-tgl').click(function () {

     $(this).parent().find('.st-news').show();
    }
);
$('#cancel-btn').click(function() {
    $("#add-banner").hide();
});
$(".delete-btn").on("click",
    function () {

        $("#entity-id").val($(this).data("id"));
        console.log($("#entity-id").val());
    });


$("#delete-form").on("submit",
    function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/Banner/DeleteBanner',
            data: { id: $("#entity-id").val() },
            success: function (data) {
                if (data.success) {
                    $("button[data-id='" + $("#entity-id").val() + "']").closest('.st-news').remove();
                    $(".fancybox-close-small").click();
                } else {
                    $('.formError').show();
                    var $contents = $('.formError').contents();
                    alert(data.message);
                }
            }
        });
    });