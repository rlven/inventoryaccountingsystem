﻿jQuery(function ($) {
    $(function () {
        $('#add-new-admin #add-new-tgl').click(function () {
            $(this).next().slideDown();
        });

       /* $('.add-admin').click(function () {
            
        });*/
    });

    $('#createAdministratorForm').on("submit",
        function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.success) {
                        //form.find('.formError').remove();
                        location.reload();
                        //$(form).closest('.st-user').slideUp();
                    } else {
                        console.log(data.message);
                        form.find('.st-user-header').after('<div class="formError"><span><ion-icon name="alert"></ion-icon> ' + data.message + '!</span></div>');
                    }
                }
            });

        });
});