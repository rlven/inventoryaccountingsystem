﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class FortuneWheelController : BaseController
    {
        private readonly IFortuneWheelRepository fortuneWheelRepository;
        private readonly IFortuneWheelComponentsRepository wheelComponentsRepository;
        private readonly IFortuneProductsRepository productsRepository;

        public FortuneWheelController(IFortuneWheelRepository fortuneWheelRepository,
            IFortuneWheelComponentsRepository wheelComponentsRepository,
            IFortuneProductsRepository productsRepository)
        {
            this.fortuneWheelRepository = fortuneWheelRepository;
            this.wheelComponentsRepository = wheelComponentsRepository;
            this.productsRepository = productsRepository;
        }

        public ActionResult Index(FortuneWheelFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var wheels = fortuneWheelRepository.All;
            var filteredModel = new FortuneWheelFilteredModel(filter);
            filteredModel.ProcessData(wheels, "Id", true, pageNumber);

            return View(filteredModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Products = productsRepository.All;
            return View(new FortuneWheelEditModel {CreationTime = DateTime.Now});
        }

        [HttpPost]
        public ActionResult Create(FortuneWheelEditModel item)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" });
            try
            {
                if (ModelState.IsValid)
                {
                    var wheel = new FortuneWheel
                    {
                        Name = item.Name,
                        Bet = item.Bet,
                        CreationTime = item.CreationTime,
                        IsActive = item.IsActive,
                        IsJackpotActive = item.IsJackpotActive,
                    };

                    if (wheel.IsActive)
                    {
                        DeactivateWheels();
                    }

                    fortuneWheelRepository.InsertOrUpdate(wheel);
                    fortuneWheelRepository.Save();

                    var components = item.Components.Select(s => new FortuneWheelComponents
                    {
                        WheelId = wheel.Id,
                        IsProduct = s.IsProduct,
                        ProductId = s.ProductId,
                        Wincoins = s.Wincoins
                    }).ToList();
                    wheelComponentsRepository.AddRange(components);
                    wheelComponentsRepository.Save();

                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var wheel = fortuneWheelRepository.Find(id);
            var editWheel = new FortuneWheelEditModel
            {
                Id = wheel.Id,
                Bet = wheel.Bet,
                CreationTime = wheel.CreationTime,
                IsActive = wheel.IsActive,
                Name = wheel.Name,
                IsJackpotActive = wheel.IsJackpotActive,
                Components = wheel.FortuneWheelComponents.Select(c => new FortuneWheelComponentsEditModel
                {
                    Id = c.Id,
                    IsProduct = c.IsProduct,
                    ProductId = c.ProductId,
                    Wincoins = c.Wincoins
                }).ToArray()
            };

            ViewBag.Products = productsRepository.All;
            return View(editWheel);
        }

        [HttpPost]
        public ActionResult Edit(FortuneWheelEditModel item)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" });
            try
            {
                if (ModelState.IsValid)
                {
                    var wheel = fortuneWheelRepository.Find(item.Id);
                    if (wheel == null)
                    {
                        return Json(new
                        {
                            success = false,
                            message = Resources.WheelNotFound
                        });
                    }

                    wheel.Name = item.Name;
                    wheel.Bet = item.Bet;
                    wheel.CreationTime = item.CreationTime;
                    wheel.IsActive = item.IsActive;
                    wheel.IsJackpotActive = item.IsJackpotActive;

                    if (wheel.IsActive)
                    {
                        DeactivateWheels(wheel.Id);
                    }

                    fortuneWheelRepository.InsertOrUpdate(wheel);
                    fortuneWheelRepository.Save();

                    var components = item.Components.Select(c => c.Id);
                    var editableComponents = wheelComponentsRepository.All.Where(w => components.Contains(w.Id));

                    foreach (var component in editableComponents)
                    {
                        var edited = item.Components.FirstOrDefault(c => c.Id == component.Id);
                        if (edited == null) continue;
                        component.IsProduct = edited.IsProduct;
                        component.ProductId = edited.ProductId;
                        component.Wincoins = edited.Wincoins;
                        wheelComponentsRepository.InsertOrUpdate(component);
                    }

                    var newComponents = item.Components.Where(c => c.Id == 0).Select(s => new FortuneWheelComponents
                    {
                        WheelId = wheel.Id,
                        IsProduct = s.IsProduct,
                        ProductId = s.ProductId,
                        Wincoins = s.Wincoins
                    }).ToList();
                    wheelComponentsRepository.AddRange(newComponents);
                    wheelComponentsRepository.Save();

                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        public ActionResult Delete(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" },
                    JsonRequestBehavior.AllowGet);
            try
            {
                fortuneWheelRepository.Delete(id);
                fortuneWheelRepository.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Ошибка удаления", debug = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteComponent(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" },
                    JsonRequestBehavior.AllowGet);
            try
            {
                wheelComponentsRepository.Delete(id);
                wheelComponentsRepository.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Ошибка удаления",  debug = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActivateJackpot(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" },
                    JsonRequestBehavior.AllowGet);
            try
            {
                var wheel = fortuneWheelRepository.Find(id);
                if (!wheel.IsJackpotActive && wheel.Jackpot > 0)
                {
                    wheel.JackpotActivationTime = DateTime.Now;
                    wheel.IsJackpotActive = true;
                    wheel.AdministratorId = AuthenticationContext.CurrentUser.Id;

                    fortuneWheelRepository.InsertOrUpdate(wheel);
                    fortuneWheelRepository.Save();
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Ошибка обновления", debug = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private void DeactivateWheels(int currentId = 0)
        {
            foreach (var fortuneWheel in fortuneWheelRepository.All.Where(w => w.IsActive && w.Id != currentId))
            {
                fortuneWheel.IsActive = false;
                fortuneWheelRepository.InsertOrUpdate(fortuneWheel);
            }
        }
    }
}