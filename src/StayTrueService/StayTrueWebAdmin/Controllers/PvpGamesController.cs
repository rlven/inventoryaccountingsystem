﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Models;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class PvpGamesController : Controller
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private readonly IPvpGamesRepository PvpGamesRepository;
        private readonly IPvpGameBetsRepository pvpGameBetsRepository;

        public PvpGamesController(IPvpGamesRepository pvpGamesRepository, IPvpGameBetsRepository pvpGameBetsRepository)
        {
            PvpGamesRepository = pvpGamesRepository;
            this.pvpGameBetsRepository = pvpGameBetsRepository;
        }

        public ActionResult Index(PvpGamesFilterModel filter, int? page)
        {
            int pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var games = PvpGamesRepository.All;

            var filteredModel = new PvpGamesFilteredModel(filter);
            filteredModel.ProcessData(games, "Id", true, pageNumber);

            filteredModel.Games.ForEach(x =>
            {
                x.ImagePath = $"{FolderPath}{x.ImagePath}";
            });

            return View(filteredModel);
        }

        public ActionResult CreateGame()
        {
            var model = new PvpGames();
            model.PublicationDate = DateTime.Now;
            return PartialView("_CreateGame", model);
        }

        [HttpPost]
        public ActionResult GetBestScores(int gameId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос напрямую. Вы че хакер?" });

            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    IQueryable<PvpGameSession> gameSession = context.PvpGameSession.OrderByDescending(x => x.Id);

                    if (gameId > 0)
                        gameSession = gameSession.Where(x => x.GameId == gameId);

                    List<PvpGameScore> gamesScores = gameSession.ToList().Select(g => new PvpGameScore { UserFullName = g.Users.FirstName + " " + g.Users.LastName, GameId = g.GameId, Points = g.Score, UserId = g.UserId }).ToList();


                    List<PvpGameScore> groupedGamesScores = gamesScores.GroupBy(gs => new { gs.UserId, gs.GameId, gs.UserFullName }).Select(gs =>
                        new PvpGameScore()
                        {
                            GameId = gs.Key.GameId,
                            UserId = gs.Key.UserId,
                            UserFullName = gs.Key.UserFullName,
                            Score = gs.Max(m => m.Points)
                        }).ToList();

                    return Json(new { success = true, liders = groupedGamesScores });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult CreateGamePost(PvpGames model, int[] bets, HttpPostedFileBase preview,
            HttpPostedFileBase gameZip)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, message = "Запрос  напрямую. Вы че хакер?"});

            if (PvpGamesRepository.All.Any(g => g.GameId == model.GameId && g.Id != model.Id))
            {
                ModelState.AddModelError("GameId", "Игра с таким ID уже существует");
            }

            if (bets == null || !bets.Any())
            {
                ModelState.AddModelError("Bets", "Ставки должны содержать хотя бы один элемент");
            }

            var maxPlayers = int.Parse(ConfigurationManager.AppSettings["PvpMaxMatchPlayers"]);
            if (model.PlayerQty < 1 || model.PlayerQty > maxPlayers)
            {
                ModelState.AddModelError("PlayerQty", $"Недопустимое количество игроков, интервал 1-{maxPlayers}");
            }

            var maxTime = int.Parse(ConfigurationManager.AppSettings["PvpMaxMatchTime"]);
            if (model.MatchTime < 1 || model.MatchTime > maxTime)
            {
                ModelState.AddModelError("PlayerQty", $"Недопустимое время матча, интервал 1-{maxTime}");
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.IsActive = true;
                    PvpGamesRepository.InsertOrUpdate(model);
                    PvpGamesRepository.Save();

                    if (preview != null)
                        model.ImagePath = ImageHelper.SaveScaledPngImage(preview, "PvpGames", model.GameId.ToString());
                    if (gameZip != null)
                        model.GameUrl = FileHelper.UploadZip(gameZip, "PvpGames", model.Id.ToString());
                    model.PvpGameBets = bets.Distinct().Select(b => new PvpGameBets {Bet = b, PvpGameId = model.Id}).ToList();
                    PvpGamesRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        public ActionResult Edit(Models.EditModels.PvpGameEditModel item, int[] bets, HttpPostedFileBase preview, HttpPostedFileBase gameZip)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });

            if (PvpGamesRepository.All.Any(g => g.GameId == item.GameId && g.Id != item.Id))
            {
                ModelState.AddModelError("GameId", "Игра с таким ID уже существует");
            }

            if (bets == null || !bets.Any())
            {
                ModelState.AddModelError("Bets", "Ставки должны содержать хотя бы один элемент");
            }

            var maxPlayers = int.Parse(ConfigurationManager.AppSettings["PvpMaxMatchPlayers"]);
            if (item.PlayerQty < 1 || item.PlayerQty > maxPlayers)
            {
                ModelState.AddModelError("PlayerQty", $"Недопустимое количество игроков, интервал 1-{maxPlayers}");
            }

            var maxTime = int.Parse(ConfigurationManager.AppSettings["PvpMaxMatchTime"]);
            if (item.MatchTime < 1 || item.MatchTime > maxTime)
            {
                ModelState.AddModelError("MatchTime", $"Недопустимое время матча, интервал 1-{maxTime}");
            }

            try
            {
                if (ModelState.IsValid)
                {
                    PvpGames model = PvpGamesRepository.Find(item.Id);
                    if (model == null)
                    {
                        return Json(new { success = false, message = "Такой игры не существует" });
                    }
                    model.IsActive = item.IsActive;
                    model.Name = item.Name;
                    model.PublicationDate = item.PublicationDate;
                    model.GameId = item.GameId;
                    model.IncludeBots = item.IncludeBots;
                    model.MatchTime = item.MatchTime > 3600 ? 3600 : item.MatchTime;
                    model.PlayerQty = item.PlayerQty;

                    if (preview != null)
                    {
                        model.ImagePath = ImageHelper.SaveScaledPngImage(preview, "PvpGames", item.GameId.ToString());
                    }

                    if (gameZip != null)
                    {
                        FileHelper.DeleteFolder(item.GameUrl);
                        model.GameUrl = FileHelper.UploadZip(gameZip, "PvpGames", item.Id.ToString());
                    }

                    // item.GameUrl = FileHelper.SaveGame(gameZip, item.Id.ToString());
                    pvpGameBetsRepository.All.Where(b => b.PvpGameId == model.Id)
                        .ForEach(b => pvpGameBetsRepository.Delete(b.Id));
                    pvpGameBetsRepository.Save();
                    model.PvpGameBets = bets.Distinct().Select(b => new PvpGameBets { Bet = b, PvpGameId = model.Id }).ToList();
                    PvpGamesRepository.Save();

                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            var game = PvpGamesRepository.All.FirstOrDefault(x => x.Id == id);

            if (game == null)
            {
                return Json(new { success = true });
            }

            game.GameId = -1;
            PvpGamesRepository.Delete(id);
            PvpGamesRepository.Save();

            return Json(new { success = true });
        }
    }
}