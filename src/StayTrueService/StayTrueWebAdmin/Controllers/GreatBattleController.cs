﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;
using System.Web.Mvc;
using Common.Enums;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using NLog.Fluent;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using StayTrueWebAdmin.Models.ViewModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class GreatBattleController : BaseController
    {
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        public GreatBattleController(IGreatBattlesRepository greatBattlesRepository,
            IGreatBattleQuestionnaireRepository greatBattleQuestionnaireRepository, IGreatBattleToursRepository greatBattleToursRepository)
        {
            GreatBattlesRepository = greatBattlesRepository;
            GreatBattleQuestionnaireRepository = greatBattleQuestionnaireRepository;
            GreatBattleToursRepository = greatBattleToursRepository;
        }

        public IGreatBattlesRepository GreatBattlesRepository { get; set; }
        public IGreatBattleQuestionnaireRepository GreatBattleQuestionnaireRepository { get; set; }
        public IGreatBattleToursRepository GreatBattleToursRepository { get; set; }


        // GET: GreatBattle 
        public ActionResult Index(GreatBattlesFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var greatBattles = GreatBattlesRepository.All.Where(x => x.IsDeleted == false);

            var filteredModel = new GreatBattlesFilteredModel(filter);
            filteredModel.ProcessData(greatBattles, "ID", true, pageNumber);

            ViewBag.GetBatllesViewModel = filteredModel.GreatBattles.Select(x => new GreatBattleViewModel()
            {
                IsActive = x.IsActive,
                BeginDateTime = x.RegistrationBeginDate,
                EndDateTime = x.RegistrationBeginDate,
                Name = x.Name,
                Id = x.ID,
                BlueCommand = new BattleCommand()
                {
                    Name = "Синие",
                    ParticipantsCount = x.GreatBattlesUsers.Count(y => y.GroupID == (int) GreatBattleGroupEnum.Blue),
                    Score = x.GreatBattleTours.Count(r => r.WinnerID != null && r.WinnerID == (int)GreatBattleGroupEnum.Blue),
                    TotalPoinsCount = x.GreatBattleTours.SelectMany(y => y.GreatBattleTourPoints).Any() ? x.GreatBattleTours.SelectMany(y => y.GreatBattleTourPoints).Where(y => y.GroupID == (int) GreatBattleGroupEnum.Blue).Sum(y => y.PointsCount): 0
                },
                RedCommand = new BattleCommand()
                {
                    Name = "Красные",
                    ParticipantsCount = x.GreatBattlesUsers.Count(y => y.GroupID == (int) GreatBattleGroupEnum.Red),
                    Score = x.GreatBattleTours.Count(r => r.WinnerID != null && r.WinnerID == (int) GreatBattleGroupEnum.Red),
                    TotalPoinsCount = x.GreatBattleTours.SelectMany(y => y.GreatBattleTourPoints).Any()? x.GreatBattleTours.SelectMany(y => y.GreatBattleTourPoints).Where(y => y.GroupID == (int) GreatBattleGroupEnum.Red).Sum(y => y.PointsCount): 0
                }
            }).ToList();

            return View(filteredModel);
            // return View(greatBattles);
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, message = "Запрос  напрямую. Вы че хакер?"},
                    JsonRequestBehavior.AllowGet);
            try
            {
                var greatBattle = GreatBattlesRepository.Find(id);
                if (greatBattle != null)
                {
                    greatBattle.IsDeleted = true;
                    GreatBattlesRepository.InsertOrUpdate(greatBattle);
                }

                GreatBattlesRepository.Save();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new GreatBattleCreateEditModel
            {
                BeginDateTime = DateTime.Today,
                QuestionsModel = new GreatBattleQuizzQuestionModel[1],
                autofillPlayer = false
            };
            model.QuestionsModel[0] = new GreatBattleQuizzQuestionModel()
            {
                Id = 0,
                Question = ""
            };

            model.ToursModel = new GreatBattleTourEditModel[1];
            model.ToursModel[0] = new GreatBattleTourEditModel()
            {
                Id = 0,
                BeginDateTime = DateTime.Today,
                EndDateTime = DateTime.Today,
                CreateDateTime = DateTime.Now,
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(GreatBattleCreateEditModel model)
        {
            try
            {
                string pointCoefStr = model.PointCoef;
                pointCoefStr = pointCoefStr.Replace('.', ',');
                double pointCoef = double.Parse(pointCoefStr);

                string mobileDescription = Regex.Replace(model.Desription, "<p.*?>", "\n\t");
                mobileDescription = Regex.Replace(mobileDescription, "<.*?>", String.Empty).Replace("&nbsp;", String.Empty);


                CheckGreatBattleModel(model);

                if (ModelState.IsValid)
                {
                    var greatBattle = new GreatBattles()
                    {
                        ID = 0,
                        IsActive = model.IsActive,
                        Name = model.Name,
                        Description = model.Desription,
                        DescriptionForMobile = mobileDescription,
                        IsDeleted = false,
                        IsFinished = false,
                        CreationDate = DateTime.Now,
                        RegistrationBeginDate = model.BeginDateTime,
                        GreatBattleQuestionnaire = new List<GreatBattleQuestionnaire>(),
                        autofillPlayer =  model.autofillPlayer,
                        PointCoefficient = pointCoef,
                        Rank1Limit = model.Rank1Limit,
                        Rank2Limit = model.Rank2Limit,
                        Rank3Limit = model.Rank3Limit,
                        Rank4Limit = model.Rank4Limit,
                        Rank5Limit = model.Rank5Limit,
                        QuizCoefficient = model.QuizCoefficient ?? 1
                    };

                    GreatBattlesRepository.InsertOrUpdate(greatBattle);
                    GreatBattlesRepository.Save();

                    var greatBattlesQuestion = new GreatBattleQuestionnaire()
                    {
                        ID = 0,
                        IDGreatBattle = greatBattle.ID
                    };

                    if (model.QuestionsModel.Length >= 1)
                    {
                        greatBattlesQuestion.Question1 = model.QuestionsModel[0].Question;
                    }

                    if (model.QuestionsModel.Length >= 2)
                    {
                        greatBattlesQuestion.Question2 = model.QuestionsModel[1].Question;
                    }

                    if (model.QuestionsModel.Length >= 3)
                    {
                        greatBattlesQuestion.Question3 = model.QuestionsModel[2].Question;
                    }

                    if (model.QuestionsModel.Length >= 4)
                    {
                        greatBattlesQuestion.Question4 = model.QuestionsModel[3].Question;
                    }
                    greatBattle.GreatBattleQuestionnaire.Add(greatBattlesQuestion);

                   // var tours = new List<GreatBattleTours>();
                    foreach (var tour in model.ToursModel)
                    {
                        var greatBattleTour = new GreatBattleTours()
                        {
                            ID = 0,
                            BeginDateTime = tour.BeginDateTime,
                            EndDateTime = tour.EndDateTime,
                            GameTourName = tour.GameName,
                            GreatBattleID = greatBattle.ID,
                            IsFinished = false,
                            QuizzTourName = tour.QuizzName,
                            IsDeleted = false
                        };
                        
                        bool isSuccessParse = double.TryParse(tour.PointRate, out double pointRate);
                        if (isSuccessParse)
                        {
                            greatBattleTour.PointRate = pointRate;
                        }
                        GreatBattleToursRepository.InsertOrUpdate(greatBattleTour);
                        GreatBattleToursRepository.Save();

                        if (tour.GameImage != null)
                        {
                            greatBattleTour.GameTourImagePath = ImageHelper.SaveImage(tour.GameImage,"GreatBattles/Games", greatBattleTour.ID.ToString());
                        }

                        if (tour.QuizzImage != null)
                        {
                            greatBattleTour.QuizzTourImagePath = ImageHelper.SaveImage(tour.QuizzImage, "GreatBattles/Quizzes", greatBattleTour.ID.ToString());
                        }

                        if (tour.GameArchive != null)
                        {
                            greatBattleTour.GameTourPath = FileHelper.UploadZip(tour.GameArchive, "GreatBattle/Games", greatBattleTour.ID.ToString());
                        }

                        if (tour.QuizzArchive != null)
                        {
                            greatBattleTour.QuizzTourPath = FileHelper.UploadZip(tour.QuizzArchive, "GreatBattle/Quizzes", greatBattleTour.ID.ToString());
                        }

//                       tours.Add(greatBattleTour);

                        GreatBattleToursRepository.InsertOrUpdate(greatBattleTour);
                        GreatBattleToursRepository.Save();
                    }

                    //                    greatBattle.GreatBattleTours = tours;

                    

                    GreatBattlesRepository.Save();


                    return Json(new {success = true}, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message}, JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var greatBattle = GreatBattlesRepository.Find(id);
            if (greatBattle == null)
            {
                return Json(new { success = false, message = "Битва не найдена" }, JsonRequestBehavior.AllowGet);
            }


            var model = new GreatBattleCreateEditModel
            {
                BeginDateTime = greatBattle.RegistrationBeginDate,
                Id = greatBattle.ID,
                IsActive = greatBattle.IsActive,
                IsFinished = greatBattle.IsFinished,
                IsDeleted = greatBattle.IsDeleted,
                Desription = greatBattle.Description,
                Name = greatBattle.Name,
                autofillPlayer = greatBattle.autofillPlayer,
                PointCoef = greatBattle.PointCoefficient.ToString(),
                Rank1Limit = greatBattle.Rank1Limit,
                Rank2Limit = greatBattle.Rank2Limit,
                Rank3Limit = greatBattle.Rank3Limit,
                Rank4Limit = greatBattle.Rank4Limit,
                Rank5Limit = greatBattle.Rank5Limit,
                QuizCoefficient = greatBattle.QuizCoefficient
           };

            var questionList = new List<GreatBattleQuizzQuestionModel>();
            var questionaire = greatBattle.GreatBattleQuestionnaire.FirstOrDefault();
            if (!questionaire.Question1.IsNullOrWhiteSpace())
            {
                questionList.Add(new GreatBattleQuizzQuestionModel()
                {
                    Id = questionaire.ID,
                    Question = questionaire.Question1
                });
            }

            if (!questionaire.Question2.IsNullOrWhiteSpace())
            {
                questionList.Add(new GreatBattleQuizzQuestionModel()
                {
                    Id = questionaire.ID,
                    Question = questionaire.Question2
                });
            }

            if (!questionaire.Question3.IsNullOrWhiteSpace())
            {
                questionList.Add(new GreatBattleQuizzQuestionModel()
                {
                    Id = questionaire.ID,
                    Question = questionaire.Question3
                });
            }

            if (!questionaire.Question4.IsNullOrWhiteSpace())
            {
                questionList.Add(new GreatBattleQuizzQuestionModel()
                {
                    Id = questionaire.ID,
                    Question = questionaire.Question4
                });
            }

            model.QuestionsModel = questionList.ToArray();

            model.ToursModel = greatBattle.GreatBattleTours.Where(x=>!x.IsDeleted).Select(x => new GreatBattleTourEditModel()
            {
                Id = x.ID,
                BeginDateTime = x.BeginDateTime,
                EndDateTime = x.EndDateTime,
                GameName = x.GameTourName,
                QuizzName = x.QuizzTourName,
                GameImagePath = $"{FolderUrl}{x.GameTourImagePath}",
                QuizzImagePath = $"{FolderUrl}{x.QuizzTourImagePath}",
                PointRate = x.PointRate.ToString(CultureInfo.CurrentCulture)
            }).ToArray();
            

            return View(model);

        }

        [HttpPost]
        public ActionResult Edit(GreatBattleCreateEditModel model)
        {
            try
            {
                string pointCoefStr = model.PointCoef;
                pointCoefStr = pointCoefStr.Replace('.', ',');
                double pointCoef = double.Parse(pointCoefStr);

                string mobileDescription = Regex.Replace(model.Desription, "<p.*?>", "\n\t");
                mobileDescription = Regex.Replace(mobileDescription, "<.*?>", String.Empty).Replace("&nbsp;", String.Empty);

                CheckGreatBattleModel(model);

                if (ModelState.IsValid)
                {
                   
                    var greatBattle = GreatBattlesRepository.Find(model.Id);
                    if (greatBattle == null)
                    {
                        return Json(new { success = false, message="Битва не найдена" }, JsonRequestBehavior.AllowGet);
                    }

                    greatBattle.Description = model.Desription;
                    greatBattle.DescriptionForMobile = mobileDescription;
                    greatBattle.IsActive = model.IsActive;
                    greatBattle.IsFinished = model.IsFinished;
                    greatBattle.IsDeleted = model.IsDeleted;
                    greatBattle.Name = model.Name;
                    greatBattle.RegistrationBeginDate = model.BeginDateTime;
                    greatBattle.autofillPlayer = model.autofillPlayer;
                    greatBattle.PointCoefficient = pointCoef;
                    greatBattle.Rank1Limit = model.Rank1Limit;
                    greatBattle.Rank2Limit = model.Rank2Limit;
                    greatBattle.Rank3Limit = model.Rank3Limit;
                    greatBattle.Rank4Limit = model.Rank4Limit;
                    greatBattle.Rank5Limit = model.Rank5Limit;
                    greatBattle.QuizCoefficient = model.QuizCoefficient ?? 1;
                    GreatBattlesRepository.InsertOrUpdate(greatBattle);
                    GreatBattlesRepository.Save();
                    var greatBattlesQuestion = greatBattle.GreatBattleQuestionnaire.FirstOrDefault();

                    if (greatBattlesQuestion != null)
                    {

                        greatBattlesQuestion.Question1 = model.QuestionsModel.Length >= 1 ? model.QuestionsModel[0].Question : null;

                        greatBattlesQuestion.Question2 = model.QuestionsModel.Length >= 2 ? model.QuestionsModel[1].Question : null;

                        greatBattlesQuestion.Question3 = model.QuestionsModel.Length >= 3 ? model.QuestionsModel[2].Question : null;

                        greatBattlesQuestion.Question4 = model.QuestionsModel.Length >= 4 ? model.QuestionsModel[3].Question : null;

                    }

                    var tours = greatBattle.GreatBattleTours.Where(x=>x.IsDeleted == false).ToList();

                    foreach (var tour in model.ToursModel)
                    {
                        if (tour.Id == 0)
                        {
                            var greatBattleTour = new GreatBattleTours()
                            {
                                ID = 0,
                                BeginDateTime = tour.BeginDateTime,
                                EndDateTime = tour.EndDateTime,
                                GameTourName = tour.GameName,
                                GreatBattleID = greatBattle.ID,
                                IsFinished = false,
                                QuizzTourName = tour.QuizzName
                            };
                            bool isSuccessParse = double.TryParse(tour.PointRate, out double pointRate);
                            if (isSuccessParse)
                            {
                                greatBattleTour.PointRate = pointRate;
                            }
                            GreatBattleToursRepository.InsertOrUpdate(greatBattleTour);
                            GreatBattleToursRepository.Save();

                            if (tour.GameImage != null)
                            {
                                greatBattleTour.GameTourImagePath = ImageHelper.SaveImage(tour.GameImage,
                                    "GreatBattles/Games", greatBattleTour.ID.ToString());
                            }

                            if (tour.QuizzImage != null)
                            {
                                greatBattleTour.QuizzTourImagePath = ImageHelper.SaveImage(tour.QuizzImage,
                                    "GreatBattles/Quizzes", greatBattleTour.ID.ToString());
                            }

                            if (tour.GameArchive != null)
                            {
                                greatBattleTour.GameTourPath = FileHelper.UploadZip(tour.GameArchive,
                                    "GreatBattle/Games", greatBattleTour.ID.ToString());
                            }

                            if (tour.QuizzArchive != null)
                            {
                                greatBattleTour.QuizzTourPath = FileHelper.UploadZip(tour.QuizzArchive,
                                    "GreatBattle/Quizzes", greatBattleTour.ID.ToString());
                            }

                            tours.Add(greatBattleTour);
                            GreatBattleToursRepository.InsertOrUpdate(greatBattleTour);
                            GreatBattleToursRepository.Save();
                        }

                        else
                        {
                           var tourExist = greatBattle.GreatBattleTours.FirstOrDefault(x=>x.ID ==tour.Id);
                            tourExist.BeginDateTime = tour.BeginDateTime;
                            tourExist.EndDateTime = tour.EndDateTime;
                            tourExist.GameTourName = tour.GameName;
                            tourExist.GreatBattleID = greatBattle.ID;
                            tourExist.IsFinished = false;
                            tourExist.QuizzTourName = tour.QuizzName;

                            bool isSuccessParse = double.TryParse(tour.PointRate, out double pointRate);
                            if (isSuccessParse)
                            {
                                tourExist.PointRate = pointRate;
                            }
                            if (tour.GameImage != null)
                            {
                                tourExist.GameTourImagePath = ImageHelper.SaveImage(tour.GameImage,
                                    "GreatBattles/Games", tourExist.ID.ToString());
                            }

                            if (tour.QuizzImage != null)
                            {
                                tourExist.QuizzTourImagePath = ImageHelper.SaveImage(tour.QuizzImage,
                                    "GreatBattles/Quizzes", tourExist.ID.ToString());
                            }

                            if (tour.GameArchive != null)
                            {
                                tourExist.GameTourPath = FileHelper.UploadZip(tour.GameArchive,
                                    "GreatBattle/Games", tourExist.ID.ToString());
                            }

                            if (tour.QuizzArchive != null)
                            {
                                tourExist.QuizzTourPath = FileHelper.UploadZip(tour.QuizzArchive,
                                    "GreatBattle/Quizzes", tourExist.ID.ToString());
                            }

                        }

                    }

                    foreach (var tour in tours)
                    {
                        var temp = model.ToursModel.FirstOrDefault(x => x.Id == tour.ID);
                        if (temp == null)
                        {
                            tour.IsDeleted = true;
                        }
                    }

                    GreatBattlesRepository.Save();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message }, JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("Index");
        }

        private bool CheckGreatBattleModel(GreatBattleCreateEditModel model)
        {
            var result = true;

            if (model.QuestionsModel == null)
            {
                ModelState.AddModelError("QuestionsModel", "Опросник не может быть пустым");
                result = false;
            }
            else
            if (model.QuestionsModel.Length > 4)
            {
                ModelState.AddModelError("QuestionsModel", "Количество вопросов не может быть больше 4");
                result = false;
            }

            if (model.ToursModel == null)
            {
                ModelState.AddModelError("ToursModel", "Должен существовать хотя бы 1 тур");
                result = false;
            }
            else
            if (model.ToursModel.Length > 5)
            {
                ModelState.AddModelError("ToursModel", "Количество туров не может быть больше 5");
                result = false;
            }
            else
            foreach (var tour in model.ToursModel)
            {
                if ((tour.GameArchive != null && !tour.GameArchive.FileName.EndsWith(".zip")) ||
                    (tour.QuizzArchive != null && !tour.QuizzArchive.FileName.EndsWith(".zip")))
                {
                    ModelState.AddModelError("ToursModel", "Игра или викторина должна быть в zip архиве");
                    result = false;
                }
            }

            return result;
        }

    }

}