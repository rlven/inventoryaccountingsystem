using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.Metas;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin, moderator")]
    public class UsersController : BaseController
    {
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryPath"];

        public IUsersRepository UsersRepository;
        public ICitiesRepository CitiesRepository;
        public IBlockUsersRepository BlockUsersRepository;
        public IAccountStatusesRepository AccountStatusesRepository;
        public IGendersRepository GendersRepository;
        public IInvitedUsersRepository InvitedUsersRepository;
        public IChatGroupsRepository ChatGroupRepository { get; set; }
        public IChatPrivateGroupsUsersRepository PrivateGroupsUsersRepository { get; }

        public UsersController(IUsersRepository usersRepository, ICitiesRepository citiesRepository, IAccountStatusesRepository accountStatusesRepository,
            IBlockUsersRepository blockUsersRepository, IGendersRepository gendersRepository, IChatGroupsRepository chatGroupRepository,
            IInvitedUsersRepository invitedUsersRepository, IChatPrivateGroupsUsersRepository privateGroupsUsersRepository)
        {
            UsersRepository = usersRepository;
            CitiesRepository = citiesRepository;
            AccountStatusesRepository = accountStatusesRepository;
            BlockUsersRepository = blockUsersRepository;
            GendersRepository = gendersRepository;
            ChatGroupRepository = chatGroupRepository;
            PrivateGroupsUsersRepository = privateGroupsUsersRepository;
            InvitedUsersRepository = invitedUsersRepository;
        }

        public ActionResult Index()
        {
            var chatsGroups = ChatGroupRepository.All.Where(x => x.IsPrivate && x.IsDeleted == false && x.IsActive).ToList();
            foreach (var chatGroup in chatsGroups)
            {
                chatGroup.Image = $"{FolderUrl}{chatGroup.Image}";
            }
            ViewBag.Chats = chatsGroups;
            return View();
        }

        public ActionResult GetUsers(UsersFilterModel filter,  int? page)
        {
            var sortColumn = "Id";

            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var users = UsersRepository.All.OrderByDescending(x => x.Id).Where(x => !x.IsDeleted);

            if (filter.FilterMode == 1)
            {
                sortColumn = "SignInCounterP";
            }

            var filteredModel = new UsersFilteredModel(filter);
            string iDate = "2019-03-20";
            DateTime oDate = DateTime.Parse(iDate);
            filteredModel.NotApprovedUsersCount = users.Where(u => !u.IsApproved && u.RegistrationDate > oDate).Count();
            filteredModel.Cities = GetCitiesListAll(filter.City);
            filteredModel.AccountStatuses = GetAccountStatusesAll(filter.Status);
            filteredModel.Genders = GetGendersAll();

            filteredModel.ProcessData(users, sortColumn, true, pageNumber);

            filteredModel.Users.ForEach(x =>
            {
                x.Avatar = System.IO.File.Exists($"{FolderPath}{x.Avatar}") ? $"{FolderUrl}{x.Avatar}" : $"{FolderUrl}/Images/Avatars/Default.jpg";
                x.NewAvatar = System.IO.File.Exists($"{FolderPath}{x.NewAvatar}") ? $"{FolderUrl}{x.NewAvatar}" : null;

                x.PassportFront = $"{FolderUrl}/Images/id-template.jpg";
                x.PassportBack = $"{FolderUrl}/Images/id-template.jpg";

                x.Password = "";

            });

            return PartialView("_Users", filteredModel);
        }

        [HttpPost]
        public ActionResult ApproveAvatar()
        {
            return null;
        }


        [HttpPost]
        public ActionResult Edit(UsersEditModel item, HttpPostedFileBase userAvatar, HttpPostedFileBase passportFront,
            HttpPostedFileBase passportBack)
        {
            try
            {
                var user = UsersRepository.Find(item.Id);
                if (user == null)
                {
                    return Json(new {success = false, message = "Пользователь не найден"});
                }

                if (UsersRepository.All.Any(u =>
                    (u.Phone == item.Phone || (item.Email != user.Email && u.Email == item.Email)) && u.Id != user.Id))
                {
                    return Json(new
                        {success = false, message = "Данный номер телефона или адрес эл. почты уже используются"});
                }

                if (item.CityId == 0)
                {
                    item.CityId = user.CityId;
                }

                if (item.StatusId == 0)
                {
                    item.StatusId = user.StatusId;
                }

                user.FirstName = item.FirstName;
                user.Email = item.Email;
                user.LastName = item.LastName;
                user.MiddleName = item.MiddleName;
                user.BirthDate = item.BirthDate;
                user.GenderId = item.GenderId;
                user.Phone = item.Phone;
                user.IsApproved = item.IsApproved;
                user.IsAvatarApproved = item.IsAvatarApproved;
                user.CityId = item.CityId;
                user.PromoCode = item.PromoCode;

                if (user.IsAvatarApproved && user.NewAvatar != null)
                {
                    if (System.IO.File.Exists($@"{FolderPath}{user.NewAvatar}"))
                    {
                        user.Avatar = ImageHelper.SaveApprovedAvatar(user.Id, user.NewAvatar);
                        user.avatarsmall = ImageHelper.SaveSmallAvatar(user.Id, user.Avatar);
                        ImageHelper.DeleteImage(user.NewAvatar);
                    }

                    user.NewAvatar = null;
                }

                if (!String.IsNullOrEmpty(item.Password))
                    user.Password = GetPassword(item.Password);
                user.SignInCounterM = item.SignInCounterM;
                user.LastSignInM = item.LastSignInM;
                user.LastSignInP = item.LastSignInP;
                user.SignInCounterP = item.SignInCounterP;
                if (item.StatusId == 1 && user.StatusId == 2)
                {
                    var blockedUsers = BlockUsersRepository.All.Where(x => x.UserId == item.Id)
                        .OrderByDescending(x => x.Id).FirstOrDefault();
                    if (blockedUsers != null)
                    {
                        blockedUsers.UnblockDate = DateTime.Now;
                    }

                    BlockUsersRepository.InsertOrUpdate(blockedUsers);
                }

                user.StatusId = item.StatusId;
                user.Wincoins = item.Wincoins;
                if (userAvatar != null)
                {
                    user.Avatar = ImageHelper.SaveImage(userAvatar, "Avatars", item.Id.ToString());
                }

                if (passportBack != null)
                {
                    user.PassportBack = ImageHelper.SaveImage(passportBack, "Passports/Back", item.Id.ToString());
                }

                if (passportFront != null)
                {
                    user.PassportFront =
                        ImageHelper.SaveImage(passportFront, "Passports/Front", item.Id.ToString());
                }

                user.RefreshDate = DateTime.Now;

                UsersRepository.InsertOrUpdate(user);
                UsersRepository.Save();
                BlockUsersRepository.Save();


                return Json(new {success = true});
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult GetPassport(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                var user = UsersRepository.Find(id);
                if (user == null)
                {
                    return Json(new { success = false, message = "Пользователь не найден" });
                }

                string frontPath = System.IO.File.Exists($"{FolderPath}{user.PassportFront}") ? $"{FolderPath}{user.PassportFront}" : null;
                string backPath = System.IO.File.Exists($"{FolderPath}{user.PassportBack}") ? $"{FolderPath}{user.PassportBack}" : null;

                string base64StringBack = ImageHelper.ConvertToBase64(backPath);
                string base64StringFront = ImageHelper.ConvertToBase64(frontPath);

                var res = Json(new { success = true, front = base64StringFront, back = base64StringBack });
                res.MaxJsonLength = int.MaxValue;
                return res;
            }
            catch(Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult ActivateConfirmed(UsersFilterModel filter, int[] ids)
        {
            try
            {
                var users = SelectUsers(filter, ids);
                foreach (var user in users)
                {
                    if (user.StatusId != 1 && !user.IsApproved && user.StatusId!=2 && !user.IsDeleted)
                    {
                        if (user.PromoCode.HasValue)
                        {
                            var promoUser = UsersRepository.Find((int)user.PromoCode);
                            if (promoUser != null)
                            {
                                promoUser.PromoUsersCount++;
                                promoUser.Wincoins += 100;

                                var invitedUsers = new InvitedUsers()
                                {
                                    InvitingUserId = promoUser.Id,
                                    InvitedUserId = user.Id
                                };
                                InvitedUsersRepository.InsertOrUpdate(invitedUsers);
                                UsersRepository.InsertOrUpdate(promoUser);
                            }
                        }
                        user.StatusId = 1;
                        user.IsApproved = true;
                    }
                }
                InvitedUsersRepository.Save();
                UsersRepository.Save();
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });

            }
        }


        public ActionResult UnbanConfirmed(UsersFilterModel filter, int[] ids)
        {
            try
            {
                var users = SelectUsers(filter, ids).Select(u=>u.Id).ToArray();
                foreach (var id in users)
                {
                    var user = UsersRepository.All.FirstOrDefault(x => x.Id == id);
                    if (user != null)
                    {
                        var blockedUsers = BlockUsersRepository.All.Where(x => x.UserId == id).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (blockedUsers != null)
                        {
                            blockedUsers.UnblockDate = DateTime.Now;
                        }

                        user.StatusId = 1;
                        BlockUsersRepository.InsertOrUpdate(blockedUsers);
                    }
                }

                BlockUsersRepository.Save();
                UsersRepository.Save();
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });

            }
        }


        public ActionResult BanConfirmed(UsersFilterModel filter, int[] ids, string reason, DateTime unblockDate)
        {
            try
            {
                var users = SelectUsers(filter, ids);
                foreach (var user in users)
                {
                    var isBlockedUser = user.BlockedUsers.FirstOrDefault(x => x.UnblockDate >= DateTime.Now);
                    if (isBlockedUser != null)
                        continue;

                    BlockedUsers blockedUsers = new BlockedUsers()
                    {
                        UserId = user.Id,
                        Reason = reason,
                        BlockDate = DateTime.Now,
                        UnblockDate = unblockDate
                    };
                    user.StatusId = 2;
                    BlockUsersRepository.InsertOrUpdate(blockedUsers);
                }
                
                BlockUsersRepository.Save();
                UsersRepository.Save();
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });

            }
        }

        [AdminAuthorize(Roles = "administrator, superadmin")]
        [HttpPost]
        public ActionResult DeleteConfirmed(UsersFilterModel filter, int[] ids)
        {
            try
            {
                var users = SelectUsers(filter, ids);
                foreach (var user in users)
                {
                    user.IsDeleted = true;
                    user.Phone = string.Empty;
                }
                
                UsersRepository.Save();
                return Json(new {success = true});
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });

            }
        }

        public ActionResult ApproveAvatar(int userId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                var user = UsersRepository.Find(userId);
                if (user == null)
                {
                    return Json(new { success = false, message = "Пользователь не найден" });
                }
                user.IsAvatarApproved = true;
                user.Avatar = ImageHelper.SaveApprovedAvatar(user.Id, user.NewAvatar);
                user.avatarsmall = ImageHelper.SaveSmallAvatar(user.Id, user.Avatar);
                ImageHelper.DeleteImage(user.NewAvatar);
                user.NewAvatar = null;
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        public ActionResult AddUserToGroup(List<ChatGroups> chatGroups)
        {
            var model = chatGroups;
            return PartialView("_AddUserToGroupPartial", chatGroups);
        }

        public ActionResult CreateExcelReport(UsersFilterModel filter, int[] ids)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Direct Request" });
            try
            {
                if (!filter.IsAllSelected && ids == null)
                    return Json(new { success = false, message = "Отчет не может быть создан. Выбрано 0 пользователей" });

                var users = SelectUsers(filter, ids);
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Пользователи");
                    var headers = GetHeaders();
                    var headerRow = new List<string[]> {headers.ToArray()};
                    // Determine the header range (e.g. A1:D1)
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    // Target a worksheet
                    var worksheet = excel.Workbook.Worksheets["Пользователи"];

                    // Popular header row data
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    if (!Directory.Exists($"{FolderPath}/Reports/UserReports"))
                        Directory.CreateDirectory($"{FolderPath}/Reports/UserReports");
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[2,1].LoadFromCollection(users.Select(u => new
                    {
                        u.Id,
                        u.Phone,
                        u.FirstName,
                        u.LastName,
                        u.MiddleName,
                        u.Email,
                        u.BirthDate,
                        u.RegistrationDate,
                        u.Cities.CityName,
                        u.AccountStatuses.Status,
                        u.Genders.Name,
                        u.Wincoins,
                        u.PromoCode,
                        u.LastSignInM,
                        u.LastSignInP,
                        u.SignInCounterM,
                        u.SignInCounterP,
                        u.IsDeleted,
                        u.IsApproved,
                        u.CigarettesFavorite,
                        u.CigarettesFavoriteType,
                        u.CigarettesAlternative,
                        u.CigarettesAlternativeType
                    }));
                    worksheet.Cells.AutoFitColumns();
                    var dates = headers.Where(h => h.Contains("Дата"));
                    var columns = dates.Select(h => headers.IndexOf(h) + 1);
                    columns.ForEach(h => worksheet.Column(h).Style.Numberformat.Format = "dd-MM-yyyy HH:mm");

                    var fileId = Guid.NewGuid();
                    var path = $"{FolderPath}/Reports/UserReports/{fileId}.xlsx";
                    FileInfo excelFile = new FileInfo(path);
                    excel.SaveAs(excelFile);
                    var urlPath = $"{FolderUrl}/Reports/UserReports/{fileId}.xlsx";
                    return Json(new { success = true, url = urlPath });
                }
            }
            catch(Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }
        
        public ActionResult CountFilteredUsers(UsersFilterModel filter)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                var filtered = new UsersFilteredModel(filter);
                var users = UsersRepository.All.Where(u => !u.IsDeleted);
                var usersQuantity = filtered.FilterData(users).Count();
                return Json(new { success = true, count = usersQuantity });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        public SelectList GetCitiesListAll(int? selected)
        {
            return new SelectList(CitiesRepository.All
                .OrderBy(a => a.CityName)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.CityName })
                .ToList(), "Value", "Text", selected);
        }

        private IEnumerable<SelectListItem> GetAccountStatusesAll(int? selected)
        {
            return new SelectList(AccountStatusesRepository.All
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Status })
                .ToList(), "Value", "Text", selected);
        }

        private IEnumerable<Genders> GetGendersAll()
        {
            return GendersRepository.All.OrderBy(x => x.Id).ToList();
        }

        private string GetPassword(string password)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            string hash = BCrypt.Net.BCrypt.HashPassword(password, salt);
            return hash;
        }

        private IQueryable<Users> SelectUsers(UsersFilterModel filter, int[] ids)
        {
            IQueryable<Users> users;
            if (filter != null && filter.IsAllSelected)
            {
                var filteredModel = new UsersFilteredModel(filter);
                users = UsersRepository.All.Where(x => !x.IsDeleted);
                if (ids != null)
                {
                    users = users.Where(x => !ids.Contains(x.Id));
                }
                users = filteredModel.FilterData(users);
            }
            else
            {
                users = UsersRepository.All.Where(x => !x.IsDeleted && ids.Contains(x.Id));
            }

            return users;
        }

        private List<string> GetHeaders()
        {
            List<string> names = new List<string>();
            var properties = typeof(UsersMetas).GetProperties();
            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;
                if (attribute != null)
                {
                    names.Add(attribute.Name);
                }
            }

            return names;
        }

        public ActionResult AddChatUsers(UsersFilterModel filter, int[] userIds, int chatGroupId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);

            try
            {
                var chatGroup = ChatGroupRepository.Find(chatGroupId);
                if (chatGroup == null)
                {
                    return Json(new { success = false, message = $"Группы [{chatGroupId}] не существует" }, JsonRequestBehavior.AllowGet);
                }

                int userCounts = chatGroup.ChatPrivateGroupsUsers.Count;
                var users = SelectUsers(filter, userIds).Select(u => u.Id).ToArray();
                var distinctUsers = users.Distinct();

                foreach (var id in distinctUsers)
                {
                    var chatPrivateGroudUser = chatGroup.ChatPrivateGroupsUsers.FirstOrDefault(x => x.UserId == id);
                    if (chatPrivateGroudUser == null)
                    {
                        var newChatUser = new ChatPrivateGroupsUsers();
                        newChatUser.ChatGroupId = chatGroupId;
                        newChatUser.UserId = id;

                        PrivateGroupsUsersRepository.InsertOrUpdate(newChatUser);
                        userCounts++;
                    }
                }

                PrivateGroupsUsersRepository.Save();

                return Json(new { success = true, data = userCounts }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }
    }
}