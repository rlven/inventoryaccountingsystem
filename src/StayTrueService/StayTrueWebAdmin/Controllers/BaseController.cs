﻿using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Attributes;
using System;
using System.IO;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using Hangfire;
using Postal;

namespace StayTrueWebAdmin.Controllers
{
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// The authentication context
        /// </summary>
        protected AuthenticationContext AuthenticationContext { get; private set; }/// <summary>
                                                                                   /// Initializes data that might not be available when the constructor is called.
                                                                                   /// </summary>
        protected override void Initialize(RequestContext requestContext)
        {
            if (requestContext == null)
            {
                throw new ArgumentNullException("requestContext");
            }

            base.Initialize(requestContext);

            Initialize(null, ControllerContext.HttpContext.User.Identity.Name, new FormsAuthenticationWrapperHelper());
        }

        /// <summary>
        /// Custom contoller initialization.
        /// </summary>
        public void Initialize(AdminUserModel user, string username, IAuthenticationWrapperHelper authenticationWrapper)
        {
            AuthenticationContext = new AuthenticationContext(ControllerContext.HttpContext, authenticationWrapper, user, username);
            ViewBag.AuthenticationContext = AuthenticationContext;
        }

        [AutomaticRetry(Attempts = 2, OnAttemptsExceeded = AttemptsExceededAction.Delete)]
        [LogFailure]
        public static void EmailSendProfileData(string userName, string to, string login, string password, string approveCode)
        {
            // Prepare Postal classes to work outside of ASP.NET request
            var viewsPath = Path.GetFullPath(HostingEnvironment.MapPath(@"~/Views/Emails"));
            var engines = new ViewEngineCollection();
            engines.Add(new FileSystemRazorViewEngine(viewsPath));
            var emailService = new Postal.EmailService(engines);

            var email = new EmailRegisterUserModel()
            {
                To = new[] { to },
                Subject = "RegisterAccountEmail",
                UserName = userName,
                Login = login,
                Password = password,
                ApproveCode = approveCode
            };
            emailService.Send(email);
        }

        [AutomaticRetry(Attempts = 2, OnAttemptsExceeded = AttemptsExceededAction.Delete)]
        [LogFailure]
        public static void EmailSendProfileChangedData(string userName, string to)
        {
            // Prepare Postal classes to work outside of ASP.NET request
            var viewsPath = Path.GetFullPath(HostingEnvironment.MapPath(@"~/Views/Emails"));
            var engines = new ViewEngineCollection();
            engines.Add(new FileSystemRazorViewEngine(viewsPath));
            var emailService = new Postal.EmailService(engines);
            var email = new EmailProfileChangedModel()
            {
                To = new[] { to },
                Subject = "ProfileChangedEmail",
                UserName = userName
            };
            emailService.Send(email);
        }

    }
}