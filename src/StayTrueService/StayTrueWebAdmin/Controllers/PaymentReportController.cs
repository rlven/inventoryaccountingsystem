﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using DataAccessLayer;
using Ninject.Infrastructure.Language;
using OfficeOpenXml;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "superadmin, administrator")]
    public class PaymentReportController : Controller
    {
        private IUsersRepository usersRepository;
        private IUserBallanceUpHistory userBallanceUpHistory;

        private static readonly string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static readonly string apiUrl = $"{ConfigurationManager.AppSettings["VirtualDirectoryUrl"]}";


        public PaymentReportController(IUsersRepository usersRepository, IUserBallanceUpHistory userBallanceUpHistory)
        {
            this.usersRepository = usersRepository;
            this.userBallanceUpHistory = userBallanceUpHistory;
        }

        public ActionResult Index(PaymentReportFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var paymentReports = GetPaymentReportList(filter.DateFrom, filter.DateTo);
            if (paymentReports == null)
                return View("ServerResponseError");

            var filteredModel = new PaymentReportFilteredModel(filter);

            filteredModel.ProcessData(paymentReports.AsQueryable(), pageNumber);

            return View(filteredModel);
        }

        private ArrayOfRestReportData GetReports(DateTime from, DateTime to)
        {
            var reports =
                PaymentServiceHelper.GetPaymentReport(from,
                    to);
            if (string.IsNullOrWhiteSpace(reports))
                return null;

            XmlSerializer serializer = new XmlSerializer(typeof(ArrayOfRestReportData),
                new XmlRootAttribute("ArrayOfRestReportData"));

            StringReader stringReader = new StringReader(reports);

            ArrayOfRestReportData PaymentResponse = (ArrayOfRestReportData) serializer.Deserialize(stringReader);

            return PaymentResponse;
        }

        private IEnumerable<PaymentReport> GetPaymentReportList(DateTime from, DateTime to)
        {
            var ressponse = GetReports(from,to);
            if (ressponse == null)
                return null;
            var reports = ressponse.RestReportData;
            ViewBag.Statuses = reports.Select(x => x.PaymentStatusName).Distinct();
   //         var users = usersRepository.All;
    //        List<PaymentReport> paymentReports = new List<PaymentReport>();
       /*     foreach (var rep in reports)
            {
                DoStuff(rep, users, paymentReports);
            }*/
            var userBallanceHistories = userBallanceUpHistory.All;
                 var query =
                     from rep in reports
                     join ballanceHistory in userBallanceHistories on rep.ReceiptID equals ballanceHistory.ReiceiptNumber
                     select new PaymentReport(){Phone = ballanceHistory.Users.Phone, Date = rep.TransactionDateTime,
                         Amount = rep.PaymentAmount, Status = rep.PaymentStatusName, Id = rep.ReceiptID, FirstName = ballanceHistory.Users.FirstName,
                         LastName = ballanceHistory.Users.LastName};

            return query;
        }

        public string ExportExcel(DateTime from, DateTime to)
        {
            var reports = GetPaymentReportList(from, to);
            string reportName = "Отчет платежки " + DateTime.Now.ToString();
            reportName = reportName.Replace(":", ".");
            reportName = reportName.Replace("/", ".");

            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("PamentReports");
                var headerRow = new List<string[]>()
                {
                    new string[]
                        {"Id", "Имя", "Фамилия", "Номер телефона", "Дата пополнения", "Сумма пополнения", "Статус"}
                };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets["PamentReports"];

                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                if (!Directory.Exists($"{folderPath}/Reports/PamentReports"))
                    Directory.CreateDirectory($"{folderPath}/Reports/PamentReports");
                worksheet.Cells["E1:E100"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";
                worksheet.Cells["A1:G100"].AutoFitColumns();

                worksheet.Cells[2, 1].LoadFromCollection(reports.Select(x =>
                    new {x.Id, x.FirstName, x.LastName, x.Phone, x.Date, x.Amount, x.Status}));

                var path = $"{folderPath}/Reports/PamentReports/{reportName}.xlsx";

                FileInfo excelFile = new FileInfo(path);
                excel.SaveAs(excelFile);

                var uriPath = $"{apiUrl}/Reports/PamentReports/{reportName}.xlsx";
                return uriPath;

            }
        }
    }
}