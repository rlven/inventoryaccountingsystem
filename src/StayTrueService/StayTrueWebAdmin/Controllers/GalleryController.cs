using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using WebGrease.Css.Extensions;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class GalleryController : BaseController
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly string[] VideoExt = {"mp4", "mov", "avi", "flv", "wmv", "mpg", "3gp", "asf", "swf"};
        private static readonly string[] ImageExt = {"jpeg", "jpg", "tiff", "gif", "bmp", "png", "tif", "raw", "exif"};
        public GalleryController(IAlbumsRepository albumsRepository, IAlbumImagesRepository albumImagesRepository)
        {
            AlbumsRepository = albumsRepository;
            AlbumImagesRepository = albumImagesRepository;
        }

        public IAlbumsRepository AlbumsRepository { get; set; }
        public IAlbumImagesRepository AlbumImagesRepository { get; set; }
        // GET: Galleries

        public ActionResult Index(AlbumsFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var albums = AlbumsRepository.All.Where(x => x.IsDeleted == false);

            var filteredModel = new AlbumsFilteredModel(filter);
            filteredModel.ProcessData(albums, "Id", true, pageNumber);

            filteredModel.Albums.ForEach(x =>
            {
                x.Image = $"{FolderPath}{x.Image}";
                x.AlbumImages.ForEach(y =>
                {
                    y.SmallImage =  y.SmallImage!=null? $"{FolderPath}{y.SmallImage}": null;
                    y.Image = y.Image !=null? $"{FolderPath}{y.Image}": null;
                    y.Video = y.Video!=null? $"{FolderPath}{y.Video}" : null;
                });
            });

            return View(filteredModel);
        }

 
        public ActionResult CreateAlbum()
        {
            var model = new Albums();
            model.PublicationDate = DateTime.Now;
            model.IsBrended = true;
            return PartialView("_CreateAlbum", model);
        }


        [HttpPost]
        public ActionResult CreateAlbumPost(Albums model, HttpPostedFileBase albumcover, IEnumerable<HttpPostedFileBase> albumImgs)
        {
            if(!Request.IsAjaxRequest())
                return Json(new { success = false, message = "������  ��������. �� �� �����?" });
            try
            {
                if (ModelState.IsValid)
                {
                    model.CreationDate = DateTime.Now;
                    AlbumsRepository.InsertOrUpdate(model);
                    AlbumsRepository.Save();
                    if (albumcover != null)
                        model.Image = ImageHelper.SaveImage(albumcover, "Gallery/Albums", model.Id.ToString());
                    AlbumsRepository.Save();

                    foreach (var albumimage in albumImgs)
                    {
                        if (albumimage == null) continue;

                        //�������� ���������� �����
                        var isVideo = VideoExt.Any(x => albumimage.FileName.EndsWith(x)) ? true :
                            ImageExt.Any(x => albumimage.FileName.EndsWith(x)) ? false : (bool?)null;
                            // ���� ���� �� ����� � �� �������� �� �� ���������
                        if(!isVideo.HasValue) continue;

                        var albumImage = new AlbumImages()
                        {
                            CategoryId = model.Id,
                            IsBrand = model.IsBrended,
                            IsActive = true
                        };

                        AlbumImagesRepository.InsertOrUpdate(albumImage);
                        AlbumImagesRepository.Save();

                        if (isVideo.Value)
                            albumImage.Video =
                                VideoHelper.SaveVideo(albumimage, "GalleryVideos", albumImage.Id.ToString());
                        else
                        {
                            albumImage.Image = ImageHelper.SaveImage(albumimage, "Gallery/Wallpapers/Big",
                                albumImage.Id.ToString());
                            albumImage.SmallImage = ImageHelper.SaveSmallImage(albumImage.Image);
                        }


                        AlbumImagesRepository.Save();
                    }

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage)
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "������ ���������� ���������",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult Edit(AlbumsEditModel item, HttpPostedFileBase albumcover, IEnumerable<HttpPostedFileBase> albumImgs)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "������  ��������. �� �� �����?" });
            try
            {
                if (ModelState.IsValid)
                {
                    Albums model = AlbumsRepository.Find(item.Id);
                    model.PublicationDate = item.PublicationDate;
                    model.IsActive = item.IsActive;
                    model.IsBrended = item.IsBrended;
                    model.Name = item.Name;
                    if (albumcover != null)
                    {
                        ImageHelper.DeleteImage(model.Image);
                        model.Image = ImageHelper.SaveImage(albumcover, "Gallery/Albums", item.Id.ToString());
                    }

                    AlbumsRepository.InsertOrUpdate(model);
                    AlbumsRepository.Save();


                    foreach (var albumimage in albumImgs)
                    {
                        if (albumimage == null) continue;

                        //�������� ���������� �����
                        var isVideo = VideoExt.Any(x => albumimage.FileName.EndsWith(x)) ? true :
                            ImageExt.Any(x => albumimage.FileName.EndsWith(x)) ? false : (bool?)null;
                        // ���� ���� �� ����� � �� �������� �� �� ���������
                        if (!isVideo.HasValue) continue;

                        var albumImage = new AlbumImages()
                        {
                            CategoryId = item.Id,
                            IsBrand = item.IsBrended,
                            IsActive = true
                        };

                        AlbumImagesRepository.InsertOrUpdate(albumImage);
                        AlbumImagesRepository.Save();

                        if (isVideo.Value)
                            albumImage.Video =
                                VideoHelper.SaveVideo(albumimage, "GalleryVideos", albumImage.Id.ToString());
                        else
                        {
                            albumImage.Image = ImageHelper.SaveImage(albumimage, "Gallery/Wallpapers/Big",
                                albumImage.Id.ToString());
                            albumImage.SmallImage = ImageHelper.SaveSmallImage(albumImage.Image);
                        }
                    }

                    AlbumImagesRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message =  ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage)
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "������ ���������� ���������",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false , message = "������  ��������. �� �� �����?" });
            try
            {
                var album = AlbumsRepository.All.FirstOrDefault(x => x.Id == id);

                if (album == null)
                {
                    return Json(new {success = true});
                }

                AlbumsRepository.Delete(id);
                AlbumsRepository.Save();

                return Json(new {success = true});
            }
            catch (Exception ex)
            {
                return Json(new { success = false, debug = ex.Message, message = "������ ��� ��������, ���������� � ��������������" });
            }
        }

        [HttpPost]
        public ActionResult AlbumImageDeleteConfirmed(int id)
        {
            if(!Request.IsAjaxRequest())
                return Json(new { success = false, message = "������  ��������. �� �� �����?" });
            try
            {
                var album = AlbumImagesRepository.All.FirstOrDefault(x => x.Id == id);
                if (album == null)
                {
                    return Json(new {success = true});
                }

                AlbumImagesRepository.Delete(id);
                AlbumImagesRepository.Save();

                return Json(new {success = true});
            }
            catch (Exception ex)
            {
                return Json(new { success = false ,debug = ex.Message, message = "������ ��� ��������, ���������� � ��������������"});
            }
        }

        [HttpPost]
        public ActionResult AlbumImageStatusChange(int id, bool status)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "������  ��������. �� �� �����?" });
            try
            {
                var albumImages = AlbumImagesRepository.All.FirstOrDefault(x => x.Id == id);
                if (albumImages == null)
                {
                    return Json(new { success = true });
                }

                albumImages.IsActive = status;
                AlbumImagesRepository.Save();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, debug = ex.Message, message = "������ ��� ��������, ���������� � ��������������" });
            }
        }
    }
}