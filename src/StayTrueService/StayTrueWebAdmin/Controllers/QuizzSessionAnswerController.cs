﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Logger;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.Style;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;
using WebGrease.Css.Extensions;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin, retailer")]
    public class QuizzSessionAnswerController : Controller
    {
        private readonly IQuizzSessionAnswerRepository QuizzQuizzSessionAnswerRepository;
        private readonly IQuizzSessionRepository QuizzSessionRepository;
        private readonly IUsersRepository UsersRepository;
        private static readonly string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static readonly string apiUrl = $"{ConfigurationManager.AppSettings["VirtualDirectoryUrl"]}";

        public QuizzSessionAnswerController(IQuizzSessionAnswerRepository quizzSessionAnswerRepository, IQuizzSessionRepository quizzSessionRepository,
            IUsersRepository usersRepository)
        {
            QuizzQuizzSessionAnswerRepository = quizzSessionAnswerRepository;
            QuizzSessionRepository = quizzSessionRepository;
            UsersRepository = usersRepository;
        }

        public ActionResult Index(QuizzAnswerFilterModel filter, int? page)
        {
            try
            {
                var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

                var answers = QuizzSessionRepository.All.Where(q => q.IsCompleted && q.Score == 0);

                var result = from an in answers

                    select new UserAnswer
                    {
                        Id = an.Users.Id,
                        FirstName = an.Users.FirstName,
                        LastName = an.Users.LastName,
                        Phone = an.Users.Phone,
                        Date = an.CreationDate,
                        IsIssued = (bool) an.IsIssued
                    };

                var filteredModel = new QuizzAnswerFilteredModel(filter);

                filteredModel.ProcessData(result, "Date", true, pageNumber);

                filteredModel.Statuses = new SelectList(
                    new List<SelectListItem>
                    {
                        new SelectListItem {Selected = true, Text = string.Empty, Value = ""},
                        new SelectListItem {Selected = false, Text = "Выдан", Value = "true"},
                        new SelectListItem {Selected = false, Text = "Не выдан", Value = "false"},
                    }, "Value", "Text", 1);
                filteredModel.StatusesList = new SelectList(
                    new List<SelectListItem>
                    {
                        new SelectListItem {Selected = false, Text = "Выдан", Value = "true"},
                        new SelectListItem {Selected = false, Text = "Не выдан", Value = "false"},
                    }, "Value", "Text", 1);

                return View(filteredModel);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        public void ChangeAnswerStatus(int? id, bool? isChecked)
        {
            if (id == null)
                return;
            if (isChecked == null)
                return;
            var quizzSession = QuizzSessionRepository.All.Where(q=>q.IsCompleted).FirstOrDefault(q => q.UserId == id);
            if (quizzSession == null)
                return;
            try
            {
                quizzSession.IsIssued = isChecked;
                QuizzSessionRepository.InsertOrUpdate(quizzSession);
                QuizzSessionRepository.Save();
            }
            catch (DbUpdateException ex)
            {
                Log.Error(ex.Message);
            }
        }

        public void ExportExcel()
        {
            var answers = QuizzSessionRepository.All.Where(q => q.IsCompleted && q.Score == 0);


            foreach (var report in answers)
            {
                var answer = report.QuizzSessionAnswers;
                var userName = report.Users.FirstName;
                var userLastName = report.Users.LastName;
                var userPhone = report.Users.Phone;

                var date = report.CreationDate;

            }
        }
        public string CreateExcelReport(QuizzAnswerFilterModel filter)
        {
                var answers = QuizzSessionRepository.All.Where(q => q.IsCompleted && q.Score == 0);

                var result = from an in answers

                    select new UserAnswer
                    {
                        Id = an.Users.Id,
                        FirstName = an.Users.FirstName,
                        LastName = an.Users.LastName,
                        Phone = an.Users.Phone,
                        Date = an.CreationDate,
                        Answers = an.QuizzSessionAnswers,
                        IsIssued = (bool)an.IsIssued
                    };
                

            var filteredModel = new QuizzAnswerFilteredModel(filter);

            filteredModel.ProcessFilterData(result, "Date", true);

            using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Ответы");
                    var headerRow = new List<string[]>()
                {
                    new string[]
                    {
                        "Имя", "Фамилия", "Телефон", "Дата прохождения", "ФИО", "Phone", "Номер Телефона", "эл. Почта", "Номер пасспорта", "Курит", "Предпочитаемая сигарета", "Предпочитаемая сигарета", "Алтернативная сигарета",
                        "Алтернативная сигарета"
                    }
                };

                    // Determine the header range (e.g. A1:D1)
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    // Target a worksheet
                    var worksheet = excel.Workbook.Worksheets["Ответы"];

                    if (!Directory.Exists($"{folderPath}/Reports/UserAnswers"))
                        Directory.CreateDirectory($"{folderPath}/Reports/UserAnswers");
                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    if (!Directory.Exists($"{folderPath}/UserAnswers"))
                        Directory.CreateDirectory($"{folderPath}/UserAnswers");
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 14;
                    worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);
                    worksheet.Cells["A1:G100"].AutoFitColumns();
                    worksheet.Cells["D1:D100"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                    var data = filteredModel.Answers.ToList();
                    for (int i = 0; i < data.Count(); i++)
                    {
                        var allAnswers = data[i].Answers.ToList();

                    worksheet.Cells[i + 3, 1].LoadFromText($"{data[i].FirstName},{data[i].LastName}, " +
                                                           $"{data[i].Phone},{data[i].Date}");
                    var counter = 0;
                    for (int j = 0; j < allAnswers.Count; j++)
                    {
                        var answer = allAnswers[j].Answer.Replace("True", "Да");
                        answer = answer.Replace("False", "Нет");
                        worksheet.Cells[i + 3, 5+counter].LoadFromText($"{answer}");
                        counter ++;
                    }


                }

                    var name = DateTime.Now.ToString();
                    name = name.Replace("/", "-");
                    name = name.Replace(":", "-");
                    name = name.Replace(".", "-");
                    name = name.Replace(" ", "-");

                var path = $"{folderPath}/Reports/UserAnswers/{name}.xlsx";

                    FileInfo excelFile = new FileInfo(path);
                    excel.SaveAs(excelFile);

                    var uriPath = $"{apiUrl}/Reports/UserAnswers/{name}.xlsx";
                    return uriPath;
                
            }
        }

    }
}