﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class PushTemplatesController : Controller
    {
        private readonly IPushTemplatesRepository pushTemplatesRepository;

        public PushTemplatesController(IPushTemplatesRepository pushTemplatesRepository)
        {
            this.pushTemplatesRepository = pushTemplatesRepository;
        }

        public ActionResult Index(PushTemplatesFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
            var templates = pushTemplatesRepository.All;
            var filteredModel = new PushTemplatesFilteredModel(filter);
            filteredModel.ProcessData(templates, "Id", true, pageNumber);
            return View(filteredModel);
        }

        public ActionResult CreatePushTemplate()
        {
            return PartialView("_CreatePushTemplate");
        }

        [HttpPost]
        public ActionResult Create(PushTemplateEditModel model)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Direct request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var pushTemplate = new PushTemplates
                    {
                        Name = model.Name,
                        PushCaption = model.PushCaption,
                        PushContent = model.PushContent,
                        IsDeleted = model.IsDeleted,
                        CreationDate = DateTime.Now
                    };
                    
                    pushTemplatesRepository.InsertOrUpdate(pushTemplate);
                    pushTemplatesRepository.Save();
                    return Json(new { success = true });
                }
                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult Edit(PushTemplateEditModel item)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Direct request" }, JsonRequestBehavior.AllowGet);
            }
            item.PushContent = Regex.Replace(item.PushContent, @"<[^>]+>|", "").Trim();
            item.PushContent = item.PushContent.Replace("&nbsp;", string.Empty);
            try
            {
                if (ModelState.IsValid)
                {
                    var pushTemplate = pushTemplatesRepository.Find(item.Id);
                    if (pushTemplate != null)
                    {
                        pushTemplate.Name = item.Name;
                        pushTemplate.PushCaption = item.PushCaption;
                        pushTemplate.PushContent = item.PushContent;
                        pushTemplate.IsDeleted = item.IsDeleted;

                        pushTemplatesRepository.InsertOrUpdate(pushTemplate);
                        pushTemplatesRepository.Save();
                        return Json(new {success = true});
                    }

                    return Json(new {sucess = false, message = $"Шаблон {item.Id} не существует"});
                }
                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Direct request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var pushTemplate = pushTemplatesRepository.Find(id);
                if (pushTemplate != null)
                {
                    pushTemplatesRepository.Delete(id);
                    pushTemplatesRepository.Save();
                    return Json(new { success = true, message = "Шаблон удален" });
                }

                return Json(new {success = false, message = $"Шаблон {id} не существует"});
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }
    }
}