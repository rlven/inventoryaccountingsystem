﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin, moderator")]
    public class ChatController : BaseController
    {
        public ChatController(IChatGroupsRepository chatGroupsRepository,
            IChatPrivateGroupsUsersRepository chatPrivateGroupsUsersRepository,
            IUsersRepository usersRepository)
        {
            ChatGroupsRepository = chatGroupsRepository;
            ChatPrivateGroupsUsersRepository = chatPrivateGroupsUsersRepository;
            UsersRepository = usersRepository;
        }

        public IChatGroupsRepository ChatGroupsRepository { get; set; }
        public IChatPrivateGroupsUsersRepository ChatPrivateGroupsUsersRepository { get; set; }
        public IUsersRepository UsersRepository { get; }

        // GET: Chat
        public ActionResult Index()
        {
            ViewBag.ChatToken = AuthenticationContext.CurrentUser.ChatToken;
            return View();
        }

        [HttpPost]
        public ActionResult GetAdminInfo()
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"});
            try
            {
                return Json(
                    new
                    {
                        success = true,
                        userInfo = new
                        {
                            id = AuthenticationContext.CurrentUser.Id,
                            firstName = AuthenticationContext.CurrentUser.FirstName,
                            lastName = AuthenticationContext.CurrentUser.LastName,
                            token = AuthenticationContext.CurrentUser.ChatToken
                        }
                    });
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult GetCreditails()
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"});
            try
            {
                string login = ConfigurationManager.AppSettings["ChatLogin"];
                string password = ConfigurationManager.AppSettings["ChatPassword"];

                return Json(
                    new
                    {
                        success = true,
                        creditails = new
                        {
                            token = "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes($"{login}:{password}"))
                        }
                    });
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        public ActionResult GetChatGroups()
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                var data = ChatGroupsRepository.All.Where(x => !x.IsDeleted).Select(x => new
                {
                    x.Id, x.Name, x.Image, x.CreationDate, isActive = x.IsActive, x.IsPrivate,
                    users = x.ChatPrivateGroupsUsers.Count
                });
                return Json(new {success = true, data}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult Delete(int[] ids)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                foreach (var id in ids)
                {
                    var chatGroup = ChatGroupsRepository.Find(id);
                    if (chatGroup != null)
                    {
                        chatGroup.IsDeleted = true;
                        ChatGroupsRepository.InsertOrUpdate(chatGroup);
                    }
                }

                ChatGroupsRepository.Save();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddChatUsers(int chatGroupId, int[] userIds)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);

            try
            {
                var chatGroup = ChatGroupsRepository.Find(chatGroupId);
                if (chatGroup == null)
                {
                    return Json(new {success = false, message = $"Группы [{chatGroupId}] не существует"},
                        JsonRequestBehavior.AllowGet);
                }

                int userCounts = chatGroup.ChatPrivateGroupsUsers.Count;
                var users = SelectUsers(userIds).Select(u => u.Id).ToArray();
                var distinctUsers = users.Distinct();

                foreach (var id in distinctUsers)
                {
                    var chatPrivateGroudUser = chatGroup.ChatPrivateGroupsUsers.FirstOrDefault(x => x.UserId == id);
                    if (chatPrivateGroudUser == null)
                    {
                        var newChatUser = new ChatPrivateGroupsUsers();
                        newChatUser.ChatGroupId = chatGroupId;
                        newChatUser.UserId = id;

                        ChatPrivateGroupsUsersRepository.InsertOrUpdate(newChatUser);
                        userCounts++;
                    }
                }

                ChatPrivateGroupsUsersRepository.Save();

                return Json(new {success = true, data = userCounts}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult DeleteChatUsers(int chatGroupId, int[] userIds)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                var chatGroup = ChatGroupsRepository.Find(chatGroupId);
                if (chatGroup == null)
                {
                    return Json(new {success = false, message = $"Группы [{chatGroupId}] не существует"},
                        JsonRequestBehavior.AllowGet);
                }

                foreach (var id in userIds)
                {
                    var chatPrivateGroudUser = chatGroup.ChatPrivateGroupsUsers.FirstOrDefault(x => x.UserId == id);
                    if (chatPrivateGroudUser != null)
                    {
                        ChatPrivateGroupsUsersRepository.Delete(chatPrivateGroudUser.Id);
                    }
                }

                ChatPrivateGroupsUsersRepository.Save();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult Deactivate(int[] ids)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                foreach (var id in ids)
                {
                    var chatGroup = ChatGroupsRepository.Find(id);
                    if (chatGroup != null)
                    {
                        chatGroup.IsActive = false;
                        ChatGroupsRepository.InsertOrUpdate(chatGroup);
                    }
                }

                ChatGroupsRepository.Save();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult Activate(int[] ids)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                foreach (var id in ids)
                {
                    var chatGroup = ChatGroupsRepository.Find(id);
                    if (chatGroup != null)
                    {
                        chatGroup.IsActive = true;
                        ChatGroupsRepository.InsertOrUpdate(chatGroup);
                    }
                }

                ChatGroupsRepository.Save();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult Create(ChatGroupCreateModel chatGroup, HttpPostedFileBase image)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                if (!chatGroup.Name.IsNullOrWhiteSpace())
                {
                    var newChatGroup = new ChatGroups()
                    {
                        IsDeleted = false,
                        Name = chatGroup.Name,
                        Image = "/Images/Chats/Default.jpg",
                        CreationDate = DateTime.Now,
                        IsActive = true,
                        IsPrivate = chatGroup.IsPrivate
                    };

                    ChatGroupsRepository.InsertOrUpdate(newChatGroup);
                    ChatGroupsRepository.Save();

                    if (image != null)
                    {
                        newChatGroup.Image = ImageHelper.SaveImage(image, "Chats", newChatGroup.Id.ToString());
                        ChatGroupsRepository.Save();
                    }

                    return Json(new {success = true, chatGroup = newChatGroup}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {success = false, message = "Имя группы пустое"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        [HttpPost]
        public ActionResult GetPrivateChatUsers(int id, ChatPrivateGroupsUsersFilterModel filter, int? page)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, debug = "Not Ajax request"}, JsonRequestBehavior.AllowGet);
            try
            {
                if (!filter.PageSize.HasValue)
                {
                    filter.PageSize = 50;
                }

                var group = ChatGroupsRepository.Find(id);
                if (group == null)
                {
                    return Json(new {success = false, message = $"Группы с id= {id} не существует"},
                        JsonRequestBehavior.AllowGet);
                }

                var pageNumber = page ?? 1;

                var users = group.ChatPrivateGroupsUsers.AsQueryable();
                var filteredModel = new ChatGroupsUsersFilteredModel(filter);
                filteredModel.ProcessData(users, "Id", true, pageNumber);


                return Json(new {success = true, data = filteredModel}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, debug = e.Message});
            }
        }

        private IQueryable<Users> SelectUsers(int[] ids)
        {
            return UsersRepository.All.Where(x => !x.IsDeleted && ids.Contains(x.Id));
        }
    }
}