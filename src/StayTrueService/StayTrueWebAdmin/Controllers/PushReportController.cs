﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "superadmin, administrator")]
    public class PushReportController : Controller
    {
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryPath"];

        private readonly IPushNotificationsRepository pushNotificationsRepository;

        public PushReportController(IPushNotificationsRepository pushNotificationsRepository)
        {
            this.pushNotificationsRepository = pushNotificationsRepository;
        }

        public ActionResult Index(PushReportFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var users = pushNotificationsRepository.All;
            var filteredModel = new PushReportFilteredModel(filter);
            filteredModel.ProcessData(users, "Id", true, pageNumber);
            return View(filteredModel);
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int[] ids)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Direct request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                foreach (var id in ids)
                {
                    pushNotificationsRepository.Delete(id);
                }
                pushNotificationsRepository.Save();
                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CreateExcelReport(int reportId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Direct Request" });
            try
            {
                var notification = pushNotificationsRepository.Find(reportId);
                if (notification == null)
                    return Json(new {success = false, message = $"Невозможно создать отчет. Уведомления {reportId} не существует"});

                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Рассылка");
                    var headerRow = new List<string[]> { new []{ "Рассылка", "Сообщение", "Дата", "Получателей" } };
                    // Determine the header range (e.g. A1:D1)
                    string notificationHeaderRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    // Target a worksheet
                    var worksheet = excel.Workbook.Worksheets["Рассылка"];

                    // Popular header row data
                    worksheet.Cells[notificationHeaderRange].LoadFromArrays(headerRow);
                    if (!Directory.Exists($"{FolderPath}/Reports/PushReports"))
                        Directory.CreateDirectory($"{FolderPath}/Reports/PushReports");
                    worksheet.Cells[notificationHeaderRange].Style.Font.Bold = true;
                    worksheet.Cells[notificationHeaderRange].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[2, 1].Value = notification.TemplateName;
                    worksheet.Cells[2, 2].Value = notification.Content;
                    worksheet.Cells[2, 3].Value = notification.NotificationDate;
                    worksheet.Cells[2, 4].Value = notification.Recipients;
                    worksheet.Cells[2, 3].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                    if (notification.PushNotificationsUsers.Any())
                    {
                        var usersHeaderRow = new List<string[]> {new[] {"Id", "Телефон", "Имя", "Фамилия"}};
                        string usersHeaderRange = "A4:Z4";
                        worksheet.Cells[usersHeaderRange].LoadFromArrays(usersHeaderRow);
                        worksheet.Cells[usersHeaderRange].Style.Font.Bold = true;
                        worksheet.Cells[usersHeaderRange].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[5, 1].LoadFromCollection(notification.PushNotificationsUsers
                            .Select(s => s.Users)
                            .Select(u => new {u.Id, u.Phone, u.FirstName, u.LastName}));
                    }

                    worksheet.Cells.AutoFitColumns();
                    var path = $"{FolderPath}/Reports/PushReports/{notification.TemplateName}.xlsx";
                    FileInfo excelFile = new FileInfo(path);
                    excel.SaveAs(excelFile);
                    var urlPath = $"{FolderUrl}/Reports/PushReports/{notification.TemplateName}.xlsx";
                    return Json(new { success = true, url = urlPath });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }
    }
}