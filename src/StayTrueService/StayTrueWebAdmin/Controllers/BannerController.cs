﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Logger;
using DataAccessLayer;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;

namespace StayTrueWebAdmin.Controllers
{
    public class BannerController : Controller
    {
        private readonly IBannersRepository bannersRepository;
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];

        public BannerController(IBannersRepository bannersRepository)
        {
            this.bannersRepository = bannersRepository;
        }

        public ActionResult Index()
        {
            var banners = bannersRepository.All;
            ViewBag.FolderUrl = FolderUrl;
            return View(banners);
        }

        [HttpGet]
        public ActionResult AddBanner()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult AddBanner(Banner banner)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = "Заполнены не все поля" });
            }
            try
            {
                var imageName = banner.Name.Replace(" ", string.Empty);
                var imagePath = ImageHelper.SaveBannerImage(banner.Image, "/Images/Banners/", imageName);
                var newBanner = new Banners()
                {
                    Name = banner.Name,
                    IsActive = banner.IsActive,
                    ImagePath = imagePath
                };
                bannersRepository.InsertOrUpdate(newBanner);
                bannersRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                Log.Error(e.Message + e.StackTrace);
                return Json(new { success = false, message = e.Message });
            }
        }

        [HttpPost]
        public ActionResult DeleteBanner(int id)
        {
            var banner = bannersRepository.Find(id);
            if(banner==null)
                return Json(new { success = false, message = "Баннер не найден"});
            try
            {
                bannersRepository.Delete(id);
                bannersRepository.Save();
                return Json(new {success = true});
            }
            catch (Exception e)
            {
                Log.Error(e.Message+e.StackTrace);
                return Json(new { success = false, message = e.Message });
            }
        }
    }
}