﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class PushController : BaseController
    {
        private readonly IPushSubscribtionsRepository pushSubscribtionsRepository;
        private readonly IPushTemplatesRepository pushTemplatesRepository;
        private readonly IPushNotificationsRepository pushNotificationsRepository;
        private readonly ICitiesRepository citiesRepository;
        private readonly IAccountStatusesRepository accountStatusesRepository;

        public PushController(IPushSubscribtionsRepository pushSubscribtionsRepository,
            IPushTemplatesRepository pushTemplatesRepository, IPushNotificationsRepository pushNotificationsRepository,
            ICitiesRepository citiesRepository, IAccountStatusesRepository accountStatusesRepository)
        {
            this.pushSubscribtionsRepository = pushSubscribtionsRepository;
            this.pushTemplatesRepository = pushTemplatesRepository;
            this.pushNotificationsRepository = pushNotificationsRepository;
            this.citiesRepository = citiesRepository;
            this.accountStatusesRepository = accountStatusesRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUsers(PushFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
            var users = pushSubscribtionsRepository.All.GroupBy(p => p.Users).Select(p => p.Key);
            var filteredModel = new PushFilteredModel(filter);
            filteredModel.ProcessData(users, "Id", true, pageNumber);
            filteredModel.Cities = GetCitiesListAll();
            filteredModel.AccountStatuses = GetAccountStatusesAll();
            return PartialView("_PushUsers", filteredModel);
        }

        public ActionResult CreatePushNotification()
        {
            ViewBag.PushNotifications = GetTemplates();
            return PartialView("_CreatePushNotification");
        }

        [HttpPost]
        public ActionResult CreateNotification(PushFilterModel filter, PushNotificationEditModel notification)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, message = "Direct Request"});
            try
            {
                if (ModelState.IsValid)
                {
                    notification.UserIds = SelectUsers(filter, notification.UserIds)?.Select(u => u.Id).ToArray();
                    Dictionary<string, string> content = new Dictionary<string, string>();
                    content.Add("ru", notification.Content);
                    content.Add("en", notification.Content);
                    Dictionary<string, string> title = new Dictionary<string, string>();
                    title.Add("ru", notification.Title);
                    title.Add("en", notification.Title);

                    string[] playerIds = null;
                    if (notification.UserIds != null)
                    {
                        playerIds = pushSubscribtionsRepository.All
                            .Where(p => notification.UserIds.Contains(p.UserId)).Select(p => p.PlayerId)
                            .ToArray();
                    }

                    var result = new OneSignalHelper().CreateNotification(content, title, notification.SendTime,
                        notification.Segments, playerIds, notification.Url);
                    
                    List<PushSubscribtions> invalidPlayers = null;
                    if (result.Recipients != null && result.Recipients == 0 && playerIds != null)
                    {
                        invalidPlayers = pushSubscribtionsRepository.All.Where(s => playerIds.Contains(s.PlayerId)).ToList();
                    }

                    if (result.InvalidPlayers != null)
                    {
                        invalidPlayers = pushSubscribtionsRepository.All.Where(s => result.InvalidPlayers.Contains(s.PlayerId)).ToList();
                    }

                    if (invalidPlayers != null)
                    {
                        pushSubscribtionsRepository.DeleteRange(invalidPlayers);
                        pushSubscribtionsRepository.Save();
                    }

                    List<int> sentTo = null;
                    if (result.Recipients != null && result.Recipients != 0 && result.ResponseStatusCode == HttpStatusCode.OK)
                    {
                        if (playerIds != null)
                        {
                            var validPlayers = invalidPlayers != null
                                ? playerIds.Where(p => !result.InvalidPlayers.Contains(p)).ToList()
                                : playerIds.ToList();
                            sentTo = pushSubscribtionsRepository.All.Where(s => validPlayers.Contains(s.PlayerId))
                                .Select(s => s.UserId).Distinct().ToList();
                        }
                        string message = "Уведомление принято к отправке";
                        if (result.InvalidPlayers != null)
                        {
                            message += "\nПользователи отписавшиеся от рассылки удалены из списка рассылки";
                        }
                        SaveNotification(notification, result, sentTo);
                        
                        return Json(new { success = true, message },
                            JsonRequestBehavior.AllowGet);
                    }

                    if (result.Recipients != null && result.Recipients == 0)
                    {
                        return Json(new { success = false, message = "Выбранные пользователи не подписаны на уведомления" },
                            JsonRequestBehavior.AllowGet);
                    }

                    return Json(new
                    {
                        success = false, message = "Удаленный сервер возвратил ошибку. Недопустимый запрос.",
                        debug = result.ResponseErrors
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    success = false,
                    message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage)
                });
            }
            catch (Exception ex)
            {
                return Json(new {success = false, message= "Уведомление не принято к отправке. Выбранные пользователи не подписаны на уведомления"
                    , debug = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CountFilteredUsers(UsersFilterModel filter)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Direct request" });
            try
            {
                var filtered = new UsersFilteredModel(filter);
                var users = pushSubscribtionsRepository.All.GroupBy(p => p.Users).Select(p => p.Key);
                var usersQuantity = filtered.FilterData(users).Count();
                return Json(new { success = true, count = usersQuantity });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        private int SaveNotification(PushNotificationEditModel pushNotification, NotificationResult notificationResult, List<int> sentTo)
        {
            string template = pushTemplatesRepository.Find(pushNotification.TemplateId)?.Name;
            PushNotifications notification = new PushNotifications
            {
                Content = pushNotification.Content,
                Caption = pushNotification.Title,
                TemplateName = template ?? pushNotification.Title,
                NotificationDate = pushNotification.SendTime ?? DateTime.Now,
                AdministratorId = AuthenticationContext.CurrentUser.Id,
                Recipients = notificationResult.Recipients ?? 0,
                NotificationId = notificationResult.Id
            };
            pushNotificationsRepository.InsertOrUpdate(notification);
            pushNotificationsRepository.Save();
            if (sentTo != null)
            {
                notification.PushNotificationsUsers = sentTo.Select(s => new PushNotificationsUsers
                    {UserId = s, NoitificationId = notification.Id}).ToList();
                pushNotificationsRepository.Save();
            }

            return notification.Id;
        }

        private SelectList GetCitiesListAll()
        {
            return new SelectList(citiesRepository.All
                .OrderBy(a => a.CityName)
                .Select(a => new SelectListItem {Value = a.Id.ToString(), Text = a.CityName})
                .ToList(), "Value", "Text");
        }

        private IEnumerable<SelectListItem> GetAccountStatusesAll()
        {
            return new SelectList(accountStatusesRepository.All
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem {Value = a.Id.ToString(), Text = a.Status})
                .ToList(), "Value", "Text");
        }

        private IEnumerable<SelectListItem> GetTemplates()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return new SelectList(pushTemplatesRepository.All
                .OrderBy(a => a.Id).ToList()
                .Select(a => new SelectListItem
                    {Value = serializer.Serialize(new {a.Id, Title = a.PushCaption, Content = a.PushContent}), Text = a.Name})
                .ToList(), "Value", "Text");
        }

        private IQueryable<Users> SelectUsers(PushFilterModel filter, int[] ids)
        {
            IQueryable<Users> users = pushSubscribtionsRepository.All.GroupBy(p => p.Users).Select(p => p.Key);
            if (filter != null && filter.IsAllSelected)
            {
                var filteredModel = new PushFilteredModel(filter);
                if (ids != null)
                {
                    users = users.Where(x => !ids.Contains(x.Id));
                }
                users = filteredModel.FilterData(users);
            }
            else if (ids != null)
            {
                users = users.Where(x => ids.Contains(x.Id));
            }
            else
            {
                return null;
            }
            return users;
        }
    }
}