﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;
using Questionnaire = StayTrueWebAdmin.Models.Questionnaire;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class QuestionnaireController : BaseController
    {
        public QuestionnaireController(IQuestionnairesRepository questionnairesRepository)
        {
            QuestionnairesRepository = questionnairesRepository;
        }

        public IQuestionnairesRepository QuestionnairesRepository { get; set; }
        // GET: Questionnaire
        public ActionResult Index(QuestionnairesFilterModel filter, int? page)
        {
            int pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var games = QuestionnairesRepository.All;

            var filteredModel = new QuestionnairesFilteredModel(filter);
            filteredModel.ProcessData(games, "Id", true, pageNumber);
            
            return View(filteredModel);
        }

        public ActionResult Create()
        {
            var model = new Questionnaire
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                IsActive = false,
                Wincoins = 0
            };
            model.Questions = new List<Question>();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var questionnaire = QuestionnairesRepository.Find(id);
            if (questionnaire == null)
            {
                return HttpNotFound();
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            var model = new Questionnaire
            {
                Id = questionnaire.Id,
                EndDate = questionnaire.EndDate,
                Description = questionnaire.Description,
                IsActive = questionnaire.IsActive,
                IsDeleted = questionnaire.IsDeleted,
                Name = questionnaire.Name,
                StartDate = questionnaire.StartDate,
                Questions = jss.Deserialize<List<Question>>(questionnaire.Questions),
                Wincoins = questionnaire.Wincoins
            };
            ViewBag.AnswerTypes = GetAnswerTypes();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Questionnaire model)
        {
            if (ModelState.IsValid)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();

                var questionnaires = QuestionnairesRepository.Find(model.Id);

                if (questionnaires == null)
                {
                    return HttpNotFound();
                }

                foreach (var modelQuestion in model.Questions)
                {
                    if (modelQuestion.Answers == null && modelQuestion.AnswerType != "text")
                    {
                        ModelState.AddModelError("Answers", "Поле Варианты ответов не может быть пустое для этого типа вопросов \" Текстовый\" ");

                        ViewBag.AnswerTypes = GetAnswerTypes();
                        return View(model);
                    }
                }

                questionnaires.Name = model.Name;
                questionnaires.Description = model.Description;
                questionnaires.EndDate = model.EndDate;
                questionnaires.IsActive = model.IsActive;
                questionnaires.StartDate = model.StartDate;
                questionnaires.Questions = js.Serialize(model.Questions);
                questionnaires.Wincoins = model.Wincoins;

                if (model.IsActive)
                {
                    var activeQuestionnaires = QuestionnairesRepository.All.Where(x => x.IsActive).ToList();
                    foreach (var activeQuestionnaire in activeQuestionnaires)
                    {
                        activeQuestionnaire.IsActive = false;
                        QuestionnairesRepository.InsertOrUpdate(activeQuestionnaire);
                    }
                }


                QuestionnairesRepository.InsertOrUpdate(questionnaires);
                QuestionnairesRepository.Save();


                return RedirectToAction("Index");
            }

              ViewBag.AnswerTypes = GetAnswerTypes();

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Questionnaire model)
        {
            if (ModelState.IsValid)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();

                foreach (var modelQuestion in model.Questions)
                {
                    if (modelQuestion.Answers == null && modelQuestion.AnswerType != "text") {
                        ModelState.AddModelError("Answers", @"Поле Варианты ответов не может быть пустое для этого типа вопросов");

                        ViewBag.AnswerTypes = GetAnswerTypes();
                        return View(model);
                    }
                }

                Questionnaires questionnaires = new Questionnaires
                {
                    Name = model.Name,
                    Description = model.Description,
                    EndDate = model.EndDate,
                    IsActive = model.IsActive,
                    StartDate = model.StartDate,
                    Questions = js.Serialize(model.Questions),
                    Wincoins = model.Wincoins,
                    IsDeleted = false
                    
                };

                if (model.IsActive)
                {
                    var activeQuestionnaires = QuestionnairesRepository.All.Where(x => x.IsActive).ToList();
                    foreach (var activeQuestionnaire in activeQuestionnaires)
                    {
                        activeQuestionnaire.IsActive = false;
                        QuestionnairesRepository.InsertOrUpdate(activeQuestionnaire);
                    }
                }

                QuestionnairesRepository.InsertOrUpdate(questionnaires);
                QuestionnairesRepository.Save();


                return RedirectToAction("Index");
            }

            ViewBag.AnswerTypes = GetAnswerTypes();

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Запрос напрямую. Вы че хакер?" }, JsonRequestBehavior.AllowGet);
            }

            var questionnaire = QuestionnairesRepository.Find(id);
            if (questionnaire == null)
            {
                return Json(new { success = false, message = "Такого опросника не существует" }, JsonRequestBehavior.AllowGet);
            }

            questionnaire.IsDeleted = true;
            QuestionnairesRepository.InsertOrUpdate(questionnaire);
            QuestionnairesRepository.Save();

            return Json(new { success =  true }, JsonRequestBehavior.AllowGet);
        }

        private SelectList GetAnswerTypes()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem() {Text = @"Один выбор", Value = "select" });
            selectList.Add(new SelectListItem() {Text = @"Множественный выбор", Value = "multiple-select" });
            selectList.Add(new SelectListItem() {Text = @"Текстовый", Value = "text" });
            return new SelectList(selectList, "Value", "Text");
        }
    }
}