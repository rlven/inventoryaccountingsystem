using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Common.Models;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using StayTrueWebAdmin.Models.ViewModels;
using StayTrueWebAdmin.Repositories;
using PartyGames = DataAccessLayer.PartyGames;
using PartyQuestionnaire = DataAccessLayer.PartyQuestionnaire;
using PartyQuizzes = DataAccessLayer.PartyQuizzes;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class PartyController : Controller
    {
        private IPartyRepository partyRepository;
        private IPartyQuestionnaireRepository questionnairesRepository;
        private IPartyGamesRepository partyGamesRepository;
        private IPartyQuizzesRepository partyQuizzesRepository;
        private static readonly string FolderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryUrl"]}";

        public PartyController(IPartyRepository partyRepository, IPartyQuestionnaireRepository questionnairesRepository, IPartyGamesRepository partyGamesRepository,
            IPartyQuizzesRepository partyQuizzesRepository)
        {
            this.partyRepository = partyRepository;
            this.partyGamesRepository = partyGamesRepository;
            this.questionnairesRepository = questionnairesRepository;
            this.partyQuizzesRepository = partyQuizzesRepository;
        }

        // GET: Party
        public ActionResult Index(PartyFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var parties = partyRepository.All.Where(p=>p.IsDeleted==false);
            

            var filteredModel = new PartyFilteredModel(filter);
            filteredModel.ProcessData(parties, "Id", true, pageNumber);

            ViewBag.GetPartiesViewModel = filteredModel.Parties.Select(x => new PartiesViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                RegistrationBeginDate = x.RegistrationBeginDate,
                EndDate = x.EndDate,
                CreationDate = x.CreationDate,
                IsActive = x.IsActive,
                IsFinished = x.IsFinished
            }).ToList();

            return View(filteredModel);
        }
        [HttpGet]
        public ActionResult Create()
        {
            PartyCreateEditModel model = new PartyCreateEditModel()
            {
                CreationDate = DateTime.Now,
                PartyQuestionnaireModels = new PartyQuestionnaireModel[1],
                PartyGameModels = new PartyGameModel[1]
            };
            model.PartyQuestionnaireModels[0] = new PartyQuestionnaireModel()
            {
                Id = 0,
                Question = ""
            };
            model.PartyGameModels[0] = new PartyGameModel()
            {
                Id = 0,
                GameName = ""
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(PartyCreateEditModel model)
        {
            string mobileDescription = Regex.Replace(model.Description, "<p.*?>", "\n\t");
            mobileDescription = Regex.Replace(mobileDescription, "<.*?>", String.Empty).Replace("&nbsp;", String.Empty);

            try
            {
                if (ModelState.IsValid)
                {
                    Party party = new Party()
                    {
                        Name = model.Name,
                        Description = model.Description,
                        DescriptionForMobile = mobileDescription,
                        CreationDate = DateTime.Now,
                        EndDate = model.EndDate,
                        RegistrationBeginDate = model.RegistrationBeginDate,
                        IsActive = model.IsActive,
                        IsFinished = model.IsFinished
                    };

                    PartyQuizzes quizzes = new PartyQuizzes();

                    partyRepository.InsertOrUpdate(party);
                    partyRepository.Save();

                    var partyQuestionare = new PartyQuestionnaire()
                    {
                        PartyId = party.Id
                    };

                    if (model.PartyQuestionnaireModels.Length >= 1)
                    {
                        partyQuestionare.Question1 = model.PartyQuestionnaireModels[0].Question;
                    }

                    if (model.PartyQuestionnaireModels.Length >= 2)
                    {
                        partyQuestionare.Question2 = model.PartyQuestionnaireModels[1].Question;
                    }

                    if (model.PartyQuestionnaireModels.Length >= 3)
                    {
                        partyQuestionare.Question3 = model.PartyQuestionnaireModels[2].Question;
                    }

                    if (model.PartyQuestionnaireModels.Length >= 4)
                    {
                        partyQuestionare.Question4 = model.PartyQuestionnaireModels[3].Question;
                    }

                    if (model.PartyQuestionnaireModels.Length >= 5)
                    {
                        partyQuestionare.Question5 = model.PartyQuestionnaireModels[4].Question;
                    }

                    if (model.PartyQuestionnaireModels.Length >= 6)
                    {
                        partyQuestionare.Question6 = model.PartyQuestionnaireModels[5].Question;
                    }
                    if (model.PartyQuestionnaireModels.Length >= 7)
                    {
                        partyQuestionare.Question7 = model.PartyQuestionnaireModels[6].Question;
                    }
                    if (model.PartyQuestionnaireModels.Length >= 8)
                    {
                        partyQuestionare.Question8 = model.PartyQuestionnaireModels[7].Question;
                    }
                    if (model.PartyQuestionnaireModels.Length >= 9)
                    {
                        partyQuestionare.Question9 = model.PartyQuestionnaireModels[8].Question;
                    }
                    if (model.PartyQuestionnaireModels.Length >= 10)
                    {
                        partyQuestionare.Question10 = model.PartyQuestionnaireModels[9].Question;
                    }
                    party.PartyQuestionnaire.Add(partyQuestionare);

                    if (model.Image != null)
                    {
                        party.ImagePreview =
                            ImageHelper.SaveImage(model.Image, "Party/Images/Previews/Big", "Preview" + party.Id.ToString());
                    }

                    if (model.MobileImage != null)
                    {
                        party.ImagePreviewSmall = ImageHelper.SaveImage(model.MobileImage, "Party/Images/Previews/Small", "Preview" + party.Id.ToString());
                    }

                    if (model.PartyGameModels != null)
                    {
                        foreach (var game in model.PartyGameModels)
                        {
                            PartyGames games = new PartyGames()
                            {
                                PartyId = party.Id,
                                Name = game.GameName                                
                            };
                            if (game.GameArchive != null)
                            {
                                games.GamePath =
                                    FileHelper.UploadZipParty(game.GameArchive, $"Party/Games/{party.Id.ToString()}");
                                string replace = games.GamePath = games.GamePath.Replace("/images", "");
                                games.GamePath = replace;
                            }

                            if (game.GameImage != null)
                            {
                                games.GameImage = ImageHelper.SaveImage(game.GameImage, $"Party/Games/{party.Id}", game.GameName);
                            }
                            partyGamesRepository.InsertOrUpdate(games);
                        }
                        partyGamesRepository.Save();
                    }

                    if (model.QuizzArchive != null)
                    {
                        quizzes.QuizzPth =
                            FileHelper.UploadZip(model.QuizzArchive, "Party/Quizzes", party.Id.ToString());
                        string replace = quizzes.QuizzPth = quizzes.QuizzPth.Replace("/images", "");
                        quizzes.QuizzPth = replace;
                        quizzes.QuizzName = model.QuizzName;
                        quizzes.IsActive = true;
                    }

                    if (model.QuizzImage != null)
                    {
                        quizzes.QuizzImage = ImageHelper.SaveImage(model.QuizzImage, "Party/Images/Quizzes",
                            party.Id.ToString()+Guid.NewGuid());

                    }

                    quizzes.PartyId = party.Id;


                    partyQuizzesRepository.InsertOrUpdate(quizzes);
                    partyQuizzesRepository.Save();

                    partyRepository.InsertOrUpdate(party);                
                    partyRepository.Save();
                    return RedirectToAction("Index");
                }
                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var party = partyRepository.Find(id);

            if (party == null)
            {
                return Json(new { success = false, message = "Вечеринка не найдена" }, JsonRequestBehavior.AllowGet);
            }

            var games = partyGamesRepository.All.Where(p => p.PartyId == party.Id);


            PartyCreateEditModel model = new PartyCreateEditModel
            {
                Id = party.Id,
                Name = party.Name,
                Description = party.Description,
                IsActive = party.IsActive,
                ImagePreviewSmall = FolderPath+party.ImagePreviewSmall,
                ImagePreview = FolderPath+party.ImagePreview,
                IsFinished = party.IsFinished,
                EndDate = party.EndDate,
                CreationDate = party.CreationDate,
                RegistrationBeginDate = party.RegistrationBeginDate
            };

            

            List<PartyQuestionnaireModel> questionList = new List<PartyQuestionnaireModel>();

            var questionaire = party.PartyQuestionnaire.FirstOrDefault();

            if (!questionaire.Question1.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question1
                });
            }

            if (!questionaire.Question2.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question2
                });
            }

            if (!questionaire.Question3.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question3
                });
            }

            if (!questionaire.Question4.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question4
                });
            }
            if (!questionaire.Question5.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question5
                });
            }
            if (!questionaire.Question6.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question6
                });
            }
            if (!questionaire.Question7.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question7
                });
            }
            if (!questionaire.Question8.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question8
                });
            }
            if (!questionaire.Question9.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question9
                });
            }
            if (!questionaire.Question10.IsNullOrWhiteSpace())
            {
                questionList.Add(new PartyQuestionnaireModel()
                {
                    Id = questionaire.Id,
                    Question = questionaire.Question10
                });
            }

            model.PartyQuestionnaireModels = questionList.ToArray();

            List<PartyGameModel> partyGameModel = new List<PartyGameModel>();

            foreach (var game in games)
            {
                PartyGameModel newGame = new PartyGameModel()
                {
                    GameName = game.Name,
                    GamePath = game.GamePath,
                    Id = game.Id,
                    GameImagePath = FolderPath+game.GameImage
                };
                partyGameModel.Add(newGame);
            }

            model.PartyGameModels = partyGameModel.ToArray();

            var quizz = partyQuizzesRepository.All.FirstOrDefault(q => q.PartyId == party.Id);

            model.QuizzImagePath = FolderPath+quizz.QuizzImage;
            model.QuizzName = quizz.QuizzName;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PartyCreateEditModel model)
        {
            string mobileDescription = Regex.Replace(model.Description, "<p.*?>", "\n\t");
            mobileDescription = Regex.Replace(mobileDescription, "<.*?>", String.Empty).Replace("&nbsp;", String.Empty);


            try { 

            if (ModelState.IsValid)
            {
                var party = partyRepository.Find(model.Id);
                if (party == null)
                {
                    return Json(new { success = false, message = "вечеринка не найдена" }, JsonRequestBehavior.AllowGet);
                }

                party.Name = model.Name;
                party.Description = model.Description;
                party.IsActive = model.IsActive;
                party.IsFinished = model.IsFinished;
                party.EndDate = model.EndDate;
                party.CreationDate = model.CreationDate;
                party.RegistrationBeginDate = model.RegistrationBeginDate;

                partyRepository.InsertOrUpdate(party);
                partyRepository.Save();

                    PartyQuizzes quizzes = partyQuizzesRepository.All.Where(q => q.PartyId == party.Id).FirstOrDefault();

                var partyQuestionnaire = party.PartyQuestionnaire.FirstOrDefault();


                if (partyQuestionnaire != null)
                {

                    partyQuestionnaire.Question1 = model.PartyQuestionnaireModels.Length >= 1 ? model.PartyQuestionnaireModels[0].Question : null;

                    partyQuestionnaire.Question2 = model.PartyQuestionnaireModels.Length >= 2 ? model.PartyQuestionnaireModels[1].Question : null;

                    partyQuestionnaire.Question3 = model.PartyQuestionnaireModels.Length >= 3 ? model.PartyQuestionnaireModels[2].Question : null;

                    partyQuestionnaire.Question4 = model.PartyQuestionnaireModels.Length >= 4 ? model.PartyQuestionnaireModels[3].Question : null;

                    partyQuestionnaire.Question5 = model.PartyQuestionnaireModels.Length >= 5 ? model.PartyQuestionnaireModels[4].Question : null;

                    partyQuestionnaire.Question6 = model.PartyQuestionnaireModels.Length >= 6 ? model.PartyQuestionnaireModels[5].Question : null;

                    partyQuestionnaire.Question7 = model.PartyQuestionnaireModels.Length >= 7 ? model.PartyQuestionnaireModels[6].Question : null;

                    partyQuestionnaire.Question8 = model.PartyQuestionnaireModels.Length >= 8 ? model.PartyQuestionnaireModels[7].Question : null;

                    partyQuestionnaire.Question9 = model.PartyQuestionnaireModels.Length > 9 ? model.PartyQuestionnaireModels[8].Question : null;

                    partyQuestionnaire.Question10 = model.PartyQuestionnaireModels.Length > 9 ? model.PartyQuestionnaireModels[9].Question : null;
                    }

                    if (model.Image != null)
                    {
                        party.ImagePreview =
                            ImageHelper.SaveImage(model.Image, "Party/Images/Previews/Big", "Preview" + party.Id.ToString());
                    }

                    if (model.MobileImage != null)
                    {
                        party.ImagePreviewSmall = ImageHelper.SaveImage(model.MobileImage, "Party/Images/Previews/Small", "Preview" + party.Id.ToString());
                    }

                    if (model.PartyGameModels != null)
                    {
                        foreach (var game in model.PartyGameModels)
                        {
                            var currGame = partyGamesRepository.Find(game.Id);

                            if (currGame == null)
                            {
                                PartyGames newGames = new PartyGames()
                                {
                                    Name = game.GameName,
                                    PartyId = party.Id
                                };
                                if (game.GameArchive != null)
                                {
                                    FileHelper.DeleteFolder(newGames.GamePath);
                                    newGames.GamePath =
                                        FileHelper.UploadZip(game.GameArchive, $"Party/Games/{party.Id.ToString()}", game.GameName);
                                    string replace = newGames.GamePath = newGames.GamePath.Replace("/images", "");
                                    newGames.GamePath = replace;
                                }

                                if (game.GameImage != null)
                                {
                                    newGames.GameImage = ImageHelper.SaveImage(game.GameImage, $"Party/Games/{party.Id}", game.GameName);
                                }

                                partyGamesRepository.InsertOrUpdate(newGames);
                                partyGamesRepository.Save();
                            }
                            else
                            {
                                if (game.GameArchive != null)
                                {
                                    FileHelper.DeleteFolder(currGame.GamePath);
                                    currGame.GamePath =
                                        FileHelper.UploadZipParty(game.GameArchive, $"Party/Games/{party.Id.ToString()}");
                                    string replace = currGame.GamePath = currGame.GamePath.Replace("/images", "");
                                    currGame.GamePath = replace;
                                    
                                }

                                if (game.GameImage != null)
                                {
                                    currGame.GameImage = ImageHelper.SaveImage(game.GameImage, $"Party/Games/{party.Id}", game.GameName);
                                }
                                partyGamesRepository.InsertOrUpdate(currGame);
                                partyGamesRepository.Save();
                            }
                        }
                    }

                    if (model.QuizzArchive != null)
                    {     
                        FileHelper.DeleteFolder(quizzes.QuizzPth);
                        quizzes.QuizzPth =
                            FileHelper.UploadZip(model.QuizzArchive, "Party/Quizzes", party.Id.ToString());
                        string replace = quizzes.QuizzPth = quizzes.QuizzPth.Replace("/images", "");
                        quizzes.QuizzPth = replace;
                        quizzes.QuizzName = model.QuizzName;
                        quizzes.IsActive = true;
                        quizzes.PartyId = party.Id;
                    }

                    if (model.QuizzImage != null)
                    {
                        quizzes.QuizzImage = ImageHelper.SaveImage(model.QuizzImage, "Party/Images/Quizzes",
                            party.Id.ToString()+Guid.NewGuid());
                        quizzes.QuizzName = model.QuizzName;
                        quizzes.IsActive = model.IsActive;
                        quizzes.PartyId = party.Id;
                    }


                    partyQuizzesRepository.InsertOrUpdate(quizzes);
                    partyQuizzesRepository.Save();

                    partyRepository.InsertOrUpdate(party);
                    partyRepository.Save();


                    return RedirectToAction("Index");
            }
            return Json(new
            {
                success = false,
                message = "Поля заполнены неверно!",
                debug = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
            }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" },
                    JsonRequestBehavior.AllowGet);
            try
            {
                var party = partyRepository.Find(id);
                if (party != null)
                {
                    party.IsDeleted = true;
                    partyRepository.InsertOrUpdate(party);
                }

                partyRepository.Save();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}