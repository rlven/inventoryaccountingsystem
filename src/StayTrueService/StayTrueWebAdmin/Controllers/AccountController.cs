﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;

namespace StayTrueWebAdmin.Controllers
{
    public class AccountController : BaseController
    {
        public IAdministratorRepository AdministratorRepository { get; set; }
        public IChatTokensRepository ChatTokensRepository { get; set; }
             
        public AccountController(IAdministratorRepository administratorRepository, IChatTokensRepository chatTokensRepository)
        {
            AdministratorRepository = administratorRepository;
            ChatTokensRepository = chatTokensRepository;
        }


        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!AuthenticationContext.Authenticated)
            {
                return View("Login");
            }

            return RedirectToAction("Index", "Home");
        }


        public ActionResult Logout()
        {
            AuthenticationContext.LogOut();
            return View("Login"); 
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, String returnUrl)
        {
            if (ModelState.IsValid)
            {
                bool remeberMe = model.RememberMe != null ? model.RememberMe.Equals("on") : false;

                if (AuthenticationContext.Authenticate(model.Login, model.Password, remeberMe))
                {
                    return Json(new {success = true, redirectTo = "/Home/Index"});
                }
                else
                {
                    return Json(new { success = false, errorMessages = "Неправильные логин или пароль" });
                }
            }

             String[] errors = 
                    ModelState.Values.Where(E => E.Errors.Count > 0)
                        .SelectMany(E => E.Errors)
                        .Select(E => E.ErrorMessage)
                        .ToArray();

            return Json(new { success = false, errorMessages = errors });
        }

    }
}