﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using NLog.Targets.Wrappers;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;
using StayTrueWebAdmin.Repositories;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class HomeController : BaseController
    {
        public IAdministratorRepository AdministratorRepository { get; set; }
        public IUsersRepository UsersRepository { get; set; }
        public IAccountStatusesRepository AccountStatusesRepository { get; set; }
        public ICitiesRepository CitiesRepository { get; set; }
        private IGendersRepository GendersRepository { get; set; }

        public HomeController(IAdministratorRepository administratorRepository, IUsersRepository usersRepository,
            IAccountStatusesRepository accountStatusesRepository, ICitiesRepository citiesRepository,
            IGendersRepository gendersRepository)
        {
            AdministratorRepository = administratorRepository;
            UsersRepository = usersRepository;
            AccountStatusesRepository = accountStatusesRepository;
            CitiesRepository = citiesRepository;
            GendersRepository = gendersRepository;
        }


        public ActionResult Index(StaticticsFilterModel filter)
        {
            var users = UsersRepository.All.Where(x => x.IsDeleted == false);
            var filteredModel = new StaticticsFilteredModel(filter);
            filteredModel.AccountStatuses = GetAccountstatuses();
            filteredModel.Genders = GetGenders();
            filteredModel.Cities = GetCities();
            filteredModel.Age = GetAgeList();
            
            filteredModel.ProcessData(users, "Id", true);

            //  filteredModel.Users 
            Dictionary<Genders, int> byGender = new Dictionary<Genders, int>();
            foreach (var genderse in GendersRepository.All.ToList())
            {
                byGender.Add(genderse, filteredModel.Users.Count(x => x.GenderId == genderse.Id));
            }

            Dictionary<String, int> byPassportPresent = new Dictionary<String, int>();
            byPassportPresent.Add("С паспортом", filteredModel.Users.Count(x => (x.PassportBack != null ||  x.PassportFront != null)));
            byPassportPresent.Add("Без паспорта", filteredModel.Users.Count(x => x.PassportBack == null && x.PassportFront == null));

            Dictionary<Cities, int> byCities = new Dictionary<Cities, int>();
            foreach (var cities in CitiesRepository.All.ToList())
            {
                byCities.Add(cities, filteredModel.Users.Count(x => x.CityId == cities.Id));
            }
            Dictionary<String, int> byAge = new Dictionary<String, int>();
            byAge.Add("18-25",
                    filteredModel.Users.Count(
                        x => DbFunctions.DiffYears(x.BirthDate, DateTime.Now) >= 18 &&
                             DbFunctions.DiffYears(x.BirthDate, DateTime.Now) < 25))
                ;
            byAge.Add("25-40", filteredModel.Users.Count(x => DbFunctions.DiffYears(x.BirthDate, DateTime.Now) >= 25 && DbFunctions.DiffYears(x.BirthDate, DateTime.Now) < 40));
            byAge.Add("40+", filteredModel.Users.Count(x => DbFunctions.DiffYears(x.BirthDate, DateTime.Now) >= 40));


            StatisticsModel model = new StatisticsModel()
            {
                ByCity = byCities,
                ByGender = byGender,
                ByPassport = byPassportPresent,
                ByAge = byAge
            };
            ViewBag.Statistics = model;

            return View(filteredModel);
        }

        private SelectList GetCities()
        {
            return new SelectList(
                CitiesRepository.All
                    .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.CityName }).ToList(), "Value", "Text");
        }

        private SelectList GetGenders()
        {
            return new SelectList(
                GendersRepository.All
                    .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name }).ToList(), "Value", "Text");
        }

        private SelectList GetAccountstatuses()
        {
            return new SelectList(
                AccountStatusesRepository.All
                    .Select(a => new SelectListItem() {Value = a.Id.ToString(), Text = a.Status}).ToList(), "Value",
                "Text");
        }

        private SelectList GetAgeList()
        {
            var seletList = new List<SelectListItem>();
            seletList.Add(new SelectListItem() {Value = "1", Text = @"18 - 25" });
            seletList.Add(new SelectListItem() {Value = "2", Text = @"25 - 40" });
            seletList.Add(new SelectListItem() {Value = "3", Text = @"40+" });
            return new SelectList(seletList.AsEnumerable(), "Value", "Text");
        }
    }

    public enum Age
    {
        Yonger = 1,
        Midlle,
        Older
    }
}