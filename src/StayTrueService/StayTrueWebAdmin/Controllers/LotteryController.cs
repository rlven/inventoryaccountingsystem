﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Common.Logger;
using DataAccessLayer;
using OfficeOpenXml;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using StayTrueWebAdmin.Models.ViewModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class LotteryController : Controller
    {
        private readonly ILotteryRepository lotteryRepository;
        private readonly ILotteryUsersRepository lotteryUsersRepository;
        private readonly ICitiesRepository citiesRepository;
        private static readonly string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static readonly string apiUrl = $"{ConfigurationManager.AppSettings["VirtualDirectoryUrl"]}";

        public LotteryController(ILotteryRepository lotteryRepository, ILotteryUsersRepository lotteryUsersRepository,
            ICitiesRepository citiesRepository)
        {
            this.lotteryRepository = lotteryRepository;
            this.lotteryUsersRepository = lotteryUsersRepository;
            this.citiesRepository = citiesRepository;
        }

        public ActionResult Index(LotteryFilterModel filter, int? page)
        {
            var lotteries = lotteryRepository.All.Where(l=>!l.IsDeleted);
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var filteredModel = new LotteryFilteredModel(filter);
            filteredModel.ProcessData(lotteries, "IsActive", true, pageNumber);

            ViewBag.GetLotteriesViewModel = filteredModel.Lotteries.Select(x => new LotteryViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                AvailableTickets = x.AvailableTickets,
                TicketPrice = x.TicketPrice,
                MaxTicketsForUser = x.MaxTicketsForUser,
                DateStart = x.DateStart,
                DateEnd = x.DateEnd,
                IsActive = x.IsActive
            }).ToList();

            return View(filteredModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            LotteryCreateEditModel model = new LotteryCreateEditModel()
            {
                DateStart = DateTime.Now,
                DateEnd = DateTime.Now
            };
            model.Statuses = GetLotteryStatuses();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(LotteryCreateEditModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsActive)
                {
                    var activeLottery = lotteryRepository.All.FirstOrDefault(l => l.IsActive && !l.IsDeleted);
                    if(activeLottery!=null)
                        return Json(new
                        {
                            success = false,
                            message = "Активный шанс уже существует!"
                        }, JsonRequestBehavior.AllowGet);
                }

                var newLottery = new Lottery
                {
                    Name = model.Name,
                    DateStart = model.DateStart,
                    DateEnd = model.DateEnd,
                    MaxTicketsForUser = model.MaxTicketsForUser,
                    TicketPrice = model.TicketPrice,
                    AvailableTickets = model.AvailableTickets,
                    IsActive = model.IsActive
                };
                try
                {
                    lotteryRepository.InsertOrUpdate(newLottery);
                    lotteryRepository.Save();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return Json(new
                    {
                        success = false,
                        message = "Ошибка создания!",
                        debug = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new
            {
                success = false,
                message = "Поля заполнены неверно!",
                debug = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var lottery = lotteryRepository.Find(id);

            if (lottery != null)
            {
                var model = new LotteryCreateEditModel()
                {
                    Id = lottery.Id,
                    Name = lottery.Name,
                    DateStart = lottery.DateStart,
                    DateEnd = lottery.DateEnd,
                    AvailableTickets = lottery.AvailableTickets,
                    TicketPrice = lottery.TicketPrice,
                    IsActive = lottery.IsActive,
                    MaxTicketsForUser = lottery.MaxTicketsForUser
                };
                model.Statuses = GetLotteryStatuses();
                return View(model);
            }

            return Json(new
            {
                success = false,
                message = "Шанс не найден!"
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edit(LotteryCreateEditModel model, bool isActive)
        {
            if (ModelState.IsValid)
            {
                if (model.IsActive)
                {
                    var activeLottery = lotteryRepository.All.FirstOrDefault(l => l.IsActive && !l.IsDeleted);
                    if (activeLottery!=null && activeLottery.Id != model.Id)
                        return Json(new
                        {
                            success = false,
                            message = "Активный шанс уже существует!"
                        }, JsonRequestBehavior.AllowGet);
                }
                try
                {
                    var lottery = lotteryRepository.Find(model.Id);
                    if (lottery != null)
                    {
                        lottery.AvailableTickets = model.AvailableTickets;
                        lottery.DateEnd = model.DateEnd;
                        lottery.DateStart = model.DateStart;
                        lottery.IsActive = model.IsActive;
                        lottery.MaxTicketsForUser = model.MaxTicketsForUser;
                        lottery.Name = model.Name;
                        lottery.TicketPrice = model.TicketPrice;

                        lotteryRepository.InsertOrUpdate(lottery);
                        lotteryRepository.Save();
                        return RedirectToAction(nameof(Index));
                    }
                    return Json(new
                    {
                        success = false,
                        message = "Шанс не найден!"
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return Json(new
                    {
                        success = false,
                        message = "Ошибка редактирования!",
                        debug = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                success = false,
                message = "Поля заполнены неверно!",
                debug = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var lottery = lotteryRepository.Find(id);
            if (lottery != null)
            {
                lottery.IsDeleted = true;
                lotteryRepository.InsertOrUpdate(lottery);
                lotteryRepository.Save();
                return Json(new
                {
                    success = true,
                    message = "ok"
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                success = false,
                message = "Шанс не найден!"
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LotteryMembers(int lotteryId, LotteryMembersFilterModel filter, int? page)
        {
            var lotterymembers = lotteryUsersRepository.All.Where(l => l.LotteryId == lotteryId);
            if (lotterymembers != null)
            {
                var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
                var filteredModel = new LotteryMembersFilteredModel(filter);
                filteredModel.Filter.LotteryId = lotteryId;
                filteredModel.Cities = GetCitiesListAll(filter.CityId);
                filteredModel.ProcessData(lotterymembers, "Id", true, pageNumber);
                return View(filteredModel);
            }
            return Json(new
            {
                success = false,
                message = "Шанс не найден!"
            }, JsonRequestBehavior.AllowGet);
        }

        private SelectList GetCitiesListAll(int? selected)
        {
            return new SelectList(citiesRepository.All
                .OrderBy(a => a.CityName)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.CityName })
                .ToList(), "Value", "Text", selected);
        }

        private SelectList GetLotteryStatuses()
        {
            return new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem {Selected = false, Text = "Активно", Value = "true"},
                    new SelectListItem {Selected = false, Text = "Скрыто", Value = "false"},
                }, "Value", "Text", 1);
        }

        public string ExportExcel(int lotteryId, LotteryMembersFilterModel filter)
        {
            var lotterymembers = lotteryUsersRepository.All.Where(l => l.LotteryId == lotteryId);
            var filteredModel = new LotteryMembersFilteredModel(filter);
            filteredModel.ProcessFilterData(lotterymembers, "Id", true);

            string reportName = "Отчет участников " + DateTime.Now.ToString();
            reportName = reportName.Replace(":", ".");
            reportName = reportName.Replace("/", ".");

            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("LoterryMembersReports");
                var headerRow = new List<string[]>()
                {
                    new string[]
                        {"Id", "Имя", "Фамилия", "Дата рождения", "Город", "Номер телефона", "Кол-во винкоинов", "Email", "Дата регистрации", "Кол-во билетов"}
                };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets["LoterryMembersReports"];

                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                if (!Directory.Exists($"{folderPath}/Reports/LoterryMembersReports"))
                    Directory.CreateDirectory($"{folderPath}/Reports/LoterryMembersReports");
                worksheet.Cells["D1:D100"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";
                worksheet.Cells["I1:I100"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";
                worksheet.Cells["A1:L100"].AutoFitColumns();

                worksheet.Cells[2, 1].LoadFromCollection(filteredModel.LotteryUserses.Select(x =>
                    new { x.Id, x.Users.FirstName, x.Users.LastName, x.Users.BirthDate, x.Users.Cities.CityName,
                        x.Users.Phone, x.Users.Wincoins, x.Users.Email, x.Users.RegistrationDate, x.TicketsCount }));

                var path = $"{folderPath}/Reports/LoterryMembersReports/{reportName}.xlsx";

                FileInfo excelFile = new FileInfo(path);
                excel.SaveAs(excelFile);

                var uriPath = $"{apiUrl}/Reports/LoterryMembersReports/{reportName}.xlsx";
                return uriPath;

            }
        }
    }
}