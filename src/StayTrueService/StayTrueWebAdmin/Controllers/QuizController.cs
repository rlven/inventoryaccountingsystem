﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class QuizController : Controller
    {
        public IQuizzesRepository QuizzesRepository { get; set; }

        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        // GET: Quiz

        public QuizController(IQuizzesRepository quizzesRepository)
        {
            QuizzesRepository = quizzesRepository;
        }

        public ActionResult Index(QuizzesFilterModel filter, int? page)
        {
            int pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var games = QuizzesRepository.All;

            var filteredModel = new QuizzesFilteredModel(filter);
            filteredModel.ProcessData(games, "Id", true, pageNumber);

            filteredModel.Quizzes.ForEach(x =>
            {
                x.ImagePath = $"{FolderPath}{x.ImagePath}";
            });

            return View(filteredModel);

        }

        public ActionResult CreateQuiz()
        {
            var model = new Quizzes();
            model.PublicationDate = DateTime.Now;
            model.IsActive = false;
            return PartialView("_CreateQuiz", model);
        }



        [HttpPost]
        public ActionResult CreateQuizPost(Quizzes model, HttpPostedFileBase preview, HttpPostedFileBase quizZip)
        {
          
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    var games = QuizzesRepository.All.Where(x=>!x.IsDeleted).FirstOrDefault(x => x.Name == model.Name);
                    if (games == null)
                    {
                        model.Description = Regex.Replace(model.Description, "<.*?[^>]+>|&nbsp;", String.Empty);
                        QuizzesRepository.InsertOrUpdate(model);
                        QuizzesRepository.Save();

                        if (preview != null)
                            model.ImagePath = ImageHelper.SaveImage(preview, "Quizzes", model.Id.ToString());
                        if (quizZip != null)
                            model.Url = FileHelper.UploadZip(quizZip, "Quizzes", model.Id.ToString());
                        QuizzesRepository.Save();

                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new
                    {
                        success = false,
                        message = "Такая викторна уже существует",
                        JsonRequestBehavior.AllowGet

                    });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Edit(QuizEditModel item, HttpPostedFileBase preview, HttpPostedFileBase quizZip)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    Quizzes model = QuizzesRepository.Find(item.Id);
                    if (model == null)
                    {
                        return Json(new { success = false, message = "Такой викторины не существует" });
                    }

                    model.IsActive = item.IsActive;
                    model.Name = item.Name;
                    model.PublicationDate = item.PublicationDate;
                    model.Description = item.Description;
                    model.Wincoins = item.Wincoins;

                    if (preview != null)
                    {
                        model.ImagePath = ImageHelper.SaveImage(preview, "Quizzes", item.Id.ToString());
                    }

                    if (quizZip != null)
                    {
                        FileHelper.DeleteFolder(model.Url);
                        model.Url = FileHelper.UploadZip(quizZip, "Quizzes", item.Id.ToString());
                    }

                    // item.GameUrl = FileHelper.SaveGame(quizZip, item.Id.ToString());
                    QuizzesRepository.Save();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            var game = QuizzesRepository.All.FirstOrDefault(x => x.Id == id);

            if (game == null)
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            QuizzesRepository.Delete(id);
            QuizzesRepository.Save();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

    }
}