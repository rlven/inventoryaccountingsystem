﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using System.Web.Mvc;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using System.Threading.Tasks;
using NLog.Internal;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin, moderator")]
    public class SupportController : Controller
    {
        public SupportController(ISupportRepository supportsRepository, IUsersRepository usersRepository)
        {
            SupportRepository = supportsRepository;
            UsersRepository = usersRepository;
        }

        public ISupportRepository SupportRepository { get; set; }
        public IUsersRepository UsersRepository { get; set; }

        public ActionResult Index(SupportFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var supports = SupportRepository.All.Where(s=>s.IsPasswordLost==false && s.IsAnswered==false && !s.IsDeleted);

            var filteredModel = new SupportFilteredModel(filter);
            filteredModel.ProcessData(supports, "Id", true, pageNumber);

            return View(filteredModel);
        }

        [HttpPost]
        public ActionResult Edit(SupportRequestsEditModel item)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    SupportRequests model = SupportRepository.Find(item.Id);
                    if (model == null)
                        return Json(new { success = false, message = "Такого обращения не существует" });

                    model.Body = item.Body;
                    model.Email = item.Email;
                    model.IsAnswered = item.IsAnswered;
                    model.Phone = item.Phone;
                    model.CreationDate = item.CreationDate;
                   // model.UserId = item.UserId;

                   SupportRepository.InsertOrUpdate(model);
                    SupportRepository.Save();

                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        public ActionResult ResetUserPasswordIndex(SupportFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var suppRequests = SupportRepository.All.Where(s => s.IsPasswordLost && !s.IsAnswered && !s.IsDeleted);
            var filteredModel = new SupportFilteredModel(filter);

            filteredModel.ProcessData(suppRequests, "Id", true, pageNumber);

            return View(filteredModel);
        }

        [HttpGet]
        public ActionResult GetRequestHistory(int userId)
        {
            var allRequests = SupportRepository.All.Where(s => s.IsPasswordLost && s.UserId== userId).OrderByDescending(r => r.CreationDate);

            var req = allRequests.Select(q => new
            {
                q.Email,
                q.Phone,
                IsAnswered = q.IsAnswered ? "Сброшен" : "Не сброшен",
                CreationDate = q.CreationDate.ToString(),
                q.Body,
                AnswerDate = q.AnswerDate.ToString(),
                IsDeleted = q.IsDeleted ? "Удален" : "Не удален"
            });

            return Json(req, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ResetUserPasswordConfirm(int[] ids)
        {
            List<Users> notApproved = new List<Users>();
            List<Users> deleted = new List<Users>();
            foreach (var id in ids)
            {
                var supportRequest = SupportRepository.Find(id);
                var user = UsersRepository.Find((int)supportRequest.UserId);
                if (supportRequest==null || user == null || !user.IsApproved || user.IsDeleted || user.StatusId!=1)
                {
                    if(!user.IsApproved)
                        notApproved.Add(user);
                    if(user.IsDeleted)
                        deleted.Add(user);
                    continue;
                }
                else
                {
                    try
                    {
                        supportRequest.IsAnswered = true;
                        supportRequest.AnswerDate = DateTime.Now;
                        SupportRepository.InsertOrUpdate(supportRequest);
                        SupportRepository.Save();
                    }
                    catch (DbUpdateException ex)
                    {
                        return Json(new
                        {
                            success = false,
                            message = "Ошибка сброса пароля",
                            debug = ex.Message
                        });
                    }
                }
            }
            if(deleted.Count>0)
                return Json(new { success = false, isDeleted = true, deletList = deleted.Select(u=>u.Phone) });
            if (notApproved.Count > 0)
                return Json(new { success = false, phonelist = notApproved.Select(u=>u.Phone) });

            return Json(new { success = true });
        }
        [HttpPost]
        public ActionResult DeleteSupportReport(int[] ids)
        {
            foreach (var id in ids)
            {
                try
                {
                    SupportRepository.Delete(id);
                    SupportRepository.Save();
                }
                catch (DbUpdateException ex)
                {
                    return Json(new
                    {
                        success = false,
                        message = "Ошибка удаления",
                        debug = ex.Message
                    });
                }
            }

            return Json(new {success = true, message = "ok"});
        }

    }
}