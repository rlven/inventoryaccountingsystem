﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Logger;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class MobileVersionController : Controller
    {
        private readonly IMobileVersionRepository mobileVersionRepository;
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryPath"];
        private static readonly string MobilePath = ConfigurationManager.AppSettings["MobileVersionPath"];
        private static readonly string IosUrl = ConfigurationManager.AppSettings["IosUrl"];
        private static readonly string AndroidUrl = ConfigurationManager.AppSettings["AndroidUrl"];


        public MobileVersionController(IMobileVersionRepository mobileVersionRepository)
        {
            this.mobileVersionRepository = mobileVersionRepository;
        }

        public ActionResult Index(MobileVersionFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
            var mobileVersions = mobileVersionRepository.All;
            var filteredModel = new MobileVersionFilteredModel(filter);
            filteredModel.ProcessData(mobileVersions, "ID", true, pageNumber);

            return View(filteredModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MobileVersionEditModel model)
        {
            if (ModelState.IsValid)
            {
                var lastVersion = mobileVersionRepository.All.OrderByDescending(m => m.Id).FirstOrDefault();
                if (lastVersion == null)
                    lastVersion = new MobileVersions
                    {
                        IosPath = IosUrl,
                        AndroidPath = AndroidUrl,
                        AndroidVersion = "1",
                        IosVersion = "1"
                    };
                try
                {
                    if (model.AndroidVersion == null)
                        model.AndroidVersion = lastVersion.AndroidVersion;
                    if (model.IosVersion == null)
                        model.IosVersion = lastVersion.IosVersion;
                    if (!Directory.Exists($"{MobilePath}/Backups"))
                        Directory.CreateDirectory($"{MobilePath}/Backups");
                    double folderNameNumber = FileHelper.GetDirectoryFileNameNumber();
                    FileHelper.SaveOldVersion("v"+folderNameNumber.ToString(), "STM_android_sec.apk", "stm.ipa", model);
                    if(model.AndroidBuild!=null)
                        FileHelper.UploadZip(model.AndroidBuild, MobilePath);
                    if (model.IosBuild != null)
                        FileHelper.UploadZip(model.IosBuild, MobilePath);

                    var newVersion = new MobileVersions()
                    {
                        AndroidVersion = model.AndroidVersion,
                        IosVersion = model.IosVersion,
                        AndroidPath = AndroidUrl,
                        IosPath = IosUrl
                    };
                    mobileVersionRepository.InsertOrUpdate(newVersion);
                    mobileVersionRepository.Save();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                     
                    return Json(new
                    {
                        success = false,
                        message = ex.Message
                    });
                }
                finally{Dispose();}
            }

            return Json(new
            {
                success = false,
                message = "Поля заполнены неверно!",
                debug = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
            }, JsonRequestBehavior.AllowGet);
        }
    }
}