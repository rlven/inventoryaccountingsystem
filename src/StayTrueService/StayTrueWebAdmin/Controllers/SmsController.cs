﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Logger;
using Common.Security;
using DataAccessLayer;
using SmsSendingService;
using SmsSendingService.Enums;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;
using OfficeOpenXml;
using WebGrease.Css.Extensions;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "superadmin")]
    public class SmsController : Controller
    {
        public IUsersRepository UsersRepository { get; set; }
        public ISmsTemplateRepository SmsTemplateRepository { get; set; }
        public ISmsMessageMsgRepository SmsMessageMsgRepository { get; set; }
        public ISmsStatusesRepository SmsStatusesRepository { get; set; }
        public ISmsMessageRepository SmsMessageRepository { get; set; }
        private static readonly string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static readonly string apiUrl = $"{ConfigurationManager.AppSettings["VirtualDirectoryUrl"]}";
        private static readonly string phoneRegex = "(^996)(22[\\d]|77[\\d])|(755|55[\\d]|999)|(50[\\d]|70[\\d])\\d{6}$";


        public SmsController(IUsersRepository usersRepository, ISmsTemplateRepository smsTemplateRepository, ISmsMessageRepository smsMessageRepository,
            ISmsMessageMsgRepository smsMessageMsgRepository, ISmsStatusesRepository smsStatusesRepository)
        {
            UsersRepository = usersRepository;
            SmsTemplateRepository = smsTemplateRepository;
            SmsMessageRepository = smsMessageRepository;
            SmsStatusesRepository = smsStatusesRepository;
            SmsMessageMsgRepository = smsMessageMsgRepository;
        }

        public ActionResult Index(SmsFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var users = UsersRepository.All.Where(x=>!x.IsDeleted);
           
            var filteredModel = new SmsFilteredModel(filter);
            filteredModel.ProcessData(users, "Id", true, pageNumber);

            return View(filteredModel);
        }

        [HttpPost]
        public ActionResult SendSms(SmsSendModel model, UsersFilterModel filter)
        {
            if (filter.IsAllSelected)
            {
                var users = UsersRepository.All.Where(u=>!u.IsDeleted);

                var filteredModel = new UsersFilteredModel(filter);

                users = filteredModel.FilterData(users);

                var userIds = users.Select(u => u.Id).ToList();

                if (model.UserIds.Length != 0)
                {
                    try
                    {
                        foreach (var usr in model.UserIds)
                        {
                            userIds.Remove(usr);
                        }
                    }
                    catch(Exception e)
                    {
                        Log.Error(e.Message);
                        return Json(new {success = false, message = "unexpected error see logs for detailed error"});
                    }
                }
                model.UserIds = userIds.ToArray();
            }

            var template = SmsTemplateRepository.Find(model.TemplateId);

            if (template == null)
            {
                return Json(new { success = false, message = "template not found" });
            }

            if (model.UserIds == null)
            {
                return Json(new { success = false, message = "no users selected" });
            }

            var currUser = AuthenticationContext.GetCurrentUser();

            SmsMessage smsMessage = new SmsMessage()
            {
                SendTime = model.SendDate,
                Text = template.Text,
                UsersCount = model.UserIds.Length,
                SmsCost = template.SmsCost,
                Title = template.Title,
                AdminLogin = currUser.Login

            };
            SmsMessageRepository.InsertOrUpdate(smsMessage);
            SmsMessageRepository.Save();

            foreach (int userId in model.UserIds)
            {
                Users user = UsersRepository.Find(userId);

                List<string> phones = new List<string>();

                string messgeText = template.Text;

                phones.Add(user.Phone);

                if (template.IsPasswordRequired)
                {
                    string password = PasswordCryptography.GenerateNewPassword();
                    user.Password = PasswordCryptography.CreateBCrypt(password);
                    messgeText += $"\nЛогин:{user.Phone.Substring(3)}\nПароль:{password}";
                    UsersRepository.Save();
                }

                message message = new message
                {
                    id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
                    login = ConfigurationManager.AppSettings["loginNikita"],
                    pwd = ConfigurationManager.AppSettings["pwdNikita"],
                    time = model.SendDate.ToString("yyyyMMddHHmmss"),
                    sender = ConfigurationManager.AppSettings["smsSenderName"],
                    phones = phones.ToArray(),
                    text = messgeText
                };

                MessageResponseStatuses result = SendMessage(message);

                SmsMessageMsg smsMessageMsg = new SmsMessageMsg()
                {
                    SendDate = model.SendDate,
                    Phone = user.Phone,
                    Text = messgeText,
                    MessageId = message.id,
                    SmsMessageId = smsMessage.Id,
                    SmsCost = template.SmsCost
                };
                SmsMessageMsgRepository.InsertOrUpdate(smsMessageMsg);
                SmsMessageMsgRepository.Save();
            }

            return Json(new { success = true, message = "ok" });

        }

        [HttpPost]
        public ActionResult SendSingleSms(int templateId, string phone, DateTime date)
        {
            var template = SmsTemplateRepository.Find(templateId);
            if (template == null || string.IsNullOrEmpty(phone) || date == null)
                return Json(new {success = false, message = "Есть пустые парамертры"});

            Users user = UsersRepository.All.FirstOrDefault(x=>x.Phone==phone);
            if(user==null)
                return Json(new { success = false, message = "Пользователь с таким номером не найден" });

            List<string> phones = new List<string>();

            string messgeText = template.Text;

            phones.Add(user.Phone);

            if (template.IsPasswordRequired)
            {
                string password = PasswordCryptography.GenerateNewPassword();
                user.Password = PasswordCryptography.CreateBCrypt(password);
                messgeText += $"\nЛогин:{user.Phone.Substring(3)}\nПароль:{password}";
                UsersRepository.Save();
            }

            message message = new message
            {
                id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
                login = ConfigurationManager.AppSettings["loginNikita"],
                pwd = ConfigurationManager.AppSettings["pwdNikita"],
                time = date.ToString("yyyyMMddHHmmss"),
                sender = ConfigurationManager.AppSettings["smsSenderName"],
                phones = phones.ToArray(),
                text = messgeText
            };

            MessageResponseStatuses result = SendMessage(message);

            SmsMessageMsg smsMessageMsg = new SmsMessageMsg()
            {
                SendDate = date,
                Phone = user.Phone,
                Text = messgeText,
                MessageId = message.id,
                SmsCost = template.SmsCost
            };
            SmsMessageMsgRepository.InsertOrUpdate(smsMessageMsg);
            SmsMessageMsgRepository.Save();

            return Json(new { success = true});
        }

        [HttpPost]
        public ActionResult CreateSmsTemplate(SmsTemplateFilterModel filter)
        {
            filter.Text = filter.Text.Replace("<p>", "\n");
            string smsText = Regex.Replace(filter.Text, @"<[^>]+>|", "").Trim();
            smsText = smsText.Replace("&nbsp;", string.Empty);
            double smsCost = GetMessageCost(smsText, 1.8, filter.IsPasswordRequired);
            if (smsCost == 0)
            {
                ModelState.AddModelError("smsCost", "Error getting Nikita information");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    SmsTemplate smsTemplate = new SmsTemplate()
                    {
                        Text = smsText,
                        Title = filter.Title,
                        SmsCost = smsCost,
                        IsPasswordRequired = filter.IsPasswordRequired
                    };
                    SmsTemplateRepository.InsertOrUpdate(smsTemplate);
                    SmsTemplateRepository.Save();

                    return RedirectToAction("SmsTemplateList");
                }
                catch (DbUpdateException ex)
                {
                    return Json(new
                    {
                        success = false,
                        message = "Ошибка добавления",
                        debug = ex.Message
                    });
                }
            }

            return RedirectToAction("SmsTemplateList");
        }

        [HttpPost]
        public ActionResult GetUserStatus(string phone)
        {
            var user = UsersRepository.All.FirstOrDefault(x => x.Phone == phone);
            if (user == null)
                return Json(new {success = "false", message = "Пользователь не найден"});
            if(user.IsApproved || user.StatusId!=1)
                return Json(new { success = "false", message = "Пользователь не подтвержден"});
            return Json(new {success = true});
        }

        public ActionResult SmsTemplateList(SmsTemplateFilterModel filter, int? page)
        {
            var smsTemplates = SmsTemplateRepository.All.Where(t=>!t.IsDeleted);

            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var filteredModel = new SmsTemplateFilteredModel(filter);

            filteredModel.ProcessData(smsTemplates, "Id", true, pageNumber);

            return View(filteredModel);
        }

        public ActionResult EditSmsTemplate(SmsTemplate item)
        {
            if (ModelState.IsValid)
            {
                item.Text = item.Text.Replace("<p>", "\n");
                item.Text = Regex.Replace(item.Text, @"<[^>]+>|", "").Trim();
                item.Text = item.Text.Replace("&nbsp;", string.Empty);
                double smsCost = GetMessageCost(item.Text, 1.8, item.IsPasswordRequired);
                var smsTemplate = SmsTemplateRepository.Find(item.Id);
                if (smsTemplate != null)
                {
                    try
                    {
                        smsTemplate.Title = item.Title;
                        smsTemplate.Text = item.Text;
                        smsTemplate.IsPasswordRequired = item.IsPasswordRequired;
                        smsTemplate.SmsCost = smsCost;
                        SmsTemplateRepository.InsertOrUpdate(smsTemplate);
                        SmsTemplateRepository.Save();
                        return RedirectToAction("SmsTemplateList");
                    }
                    catch (DbUpdateException ex)
                    {
                        return Json(new
                        {
                            success = false,
                            message = "Ошибка редактирования",
                            debug = ex.Message
                        });
                    }
                }
            }

            return RedirectToAction("SmsTemplateList");
        }

        public ActionResult DeleteSmsTemplate(int id)
        {
            var smsTemplate = SmsTemplateRepository.Find(id);
            if (smsTemplate == null)
            {
                return Json(new {success = false, message = "template not found"});
            }

            try
            {
                smsTemplate.IsDeleted = true;
                SmsTemplateRepository.InsertOrUpdate(smsTemplate);
                SmsTemplateRepository.Save();
                return Json(new {success = true, message = "successfuly deleted"});
            }
            catch (DbUpdateException ex)
            {
                Log.Error(ex.Message);
                return Json(new {success = false, message = ex.Message});
            }
        }

        public ActionResult GetSmsReports(SmsMessageFilterModel filter, int? page)
        {
            var smsMessages = SmsMessageRepository.AllIncluding(q=>q.SmsMessageStatuses).Where(s=>!s.IsDeleted);

            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var filteredModel = new SmsMessageFilteredModel(filter);

            filteredModel.ProcessData(smsMessages, "Id", true, pageNumber);

            filteredModel.Statuses = GetSmsStatuses();
            ViewBag.SmsTemplates = GetSmsTemplates();
            /* var smsMessages = SmsMessageRepository.All;
             List<ResponseReport> reports = new List<ResponseReport>();
             foreach (var message in smsMessages)
             {
                 dr report = new dr()
                 {
                     login = ConfigurationManager.AppSettings["loginNikita"],
                     pwd = ConfigurationManager.AppSettings["pwdNikita"],
                     id = message.MessageId
                 };
                 string request = XmlSerializator.GetXMLFromObject(report);
                 var result = SendHttpRequest(request, ConfigurationManager.AppSettings["nikitaReport"]);

                 var requestStatus = GetBetween(result, "<status>", "</status>");
                 var requestReport = GetBetween(result, "<report>", "</report>");
                 var requestSendTime = GetBetween(result, "<sendTime>", "</sendTime>");
                 var requestRcvTime = GetBetween(result, "<rcvTime>", "</rcvTime>");

                 ResponseReport responseReport = new ResponseReport()
                 {
                     Status = requestStatus,
                     Number = GetBetween(result, "<number>", "</number>"),
                     Report = requestReport
                 };
            //     if (!String.IsNullOrEmpty(requestSendTime) && !String.IsNullOrEmpty(requestRcvTime))
            //     {
           //          responseReport.SendTime = Convert.ToDateTime(requestSendTime);
            //         responseReport.RcvTime = Convert.ToDateTime(requestRcvTime);
           //      }

                 reports.Add(responseReport);
             }
             */
            return View(filteredModel);
        }

        public string GetReportMessageStatus(int id)
        {
            var smsMessages = SmsMessageMsgRepository.All.Where(s => s.SmsMessageId == id);
            if (smsMessages == null)
            {
                return "рассылка не найдена";
            }

            foreach (var msg in smsMessages)
            {
                dr report = new dr()
                {
                    login = ConfigurationManager.AppSettings["loginNikita"],
                    pwd = ConfigurationManager.AppSettings["pwdNikita"],
                    id = msg.MessageId
                };
                string request = XmlSerializator.GetXMLFromObject(report);
                var result = SendHttpRequest(request, ConfigurationManager.AppSettings["nikitaReport"]);

                var requestStatus = GetBetween(result, "<status>", "</status>");
                var requestReport = GetBetween(result, "<report>", "</report>");
                try
                {
                    msg.Status = Convert.ToInt32(requestStatus);
                    msg.Report = Convert.ToInt32(requestReport);
                    SmsMessageMsgRepository.InsertOrUpdate(msg);
                }
                catch (DbUpdateException ex)
                {
                    return ex.Message;
                }
            }

            SmsMessageMsgRepository.Save();

            return CreateAndUploadExcel(smsMessages, id);            
        }

        [HttpPost]
        public ActionResult DeleteSmsReport(int id)
        {
            var smsReport = SmsMessageRepository.Find(id);
            if (smsReport == null)
            {
                return Json(new {success = false, message = "отчет не найден"});
            }

            try
            {
                smsReport.IsDeleted = true;
                SmsMessageRepository.InsertOrUpdate(smsReport);
                SmsMessageRepository.Save();
                return Json(new { success = true, message = "ok" });
            }
            catch(DbUpdateException ex)
            {
                return Json(new {success = false, message = ex.Message});
            }
        }

        [HttpPost]
        public ActionResult CancelSmsReport(int id)
        {
            var smsReport = SmsMessageRepository.Find(id);
            if (smsReport == null)
            {
                return Json(new { success = false, message = "отчет не найден" });
            }

            try
            {
                if (smsReport.StatusId != 2)
                {
                    return Json(new { success = false, message = "Сообщения уже отправлены или отменены, невозможно отменить" });
                }

                smsReport.StatusId = 3;
                SmsMessageRepository.InsertOrUpdate(smsReport);
                SmsMessageRepository.Save();
                return Json(new { success = true, message = "ok" });
            }
            catch (DbUpdateException ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [HttpGet]
        public ActionResult GetUploadSmsSendPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadExcelForSmsSend(FormCollection formCollection)
        {
            if (Request != null)
            {
                try
                {
                    var users = UsersRepository.All;
                    HttpPostedFileBase file = Request.Files["UploadedFile"];
                    if ((file != null) && !string.IsNullOrEmpty(file.FileName))
                    {
                        List<string> phonesList = new List<string>();
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            for (int rowIterator = 1; rowIterator <= noOfRow; rowIterator++)
                            {
                                string phone = workSheet.Cells[rowIterator, 1].Value.ToString();
                                phonesList.Add(phone);
                            }
                        }

                        SmsSendPhonesModel model = new SmsSendPhonesModel();
                        var usersExist = (from u in users where phonesList.Contains(u.Phone) select u);

             //           var usersNotExistInDb = (from u in users where !phonesList.Contains(u.Phone) select u.Phone);

                        model.ApprovedUsers = usersExist.Where(u => u.IsApproved && u.StatusId == 1 && !u.IsDeleted)
                            .Select(u => u.Phone).ToList();
                        model.NotApprovedUsers = usersExist.Where(u => !u.IsApproved).Select(u => u.Phone).ToList();
                        
                        model.Count = phonesList.Count;

                        List<string> newList = new List<string>();
                        newList.AddRange(model.ApprovedUsers);
                        newList.AddRange(model.NotApprovedUsers);

                        model.NotFoundUsers = (from u in phonesList where !newList.Contains(u) select u).ToList();


                    //    var mobileOperatorRegularExpressions = GetMobileOperatorRegularExpressions();

                        model.NotCorrectNumbers = phonesList.Where(p => !Regex.IsMatch(p, phoneRegex)).ToList();
                        return View(model);
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                    return Json(new {success = false, errorMessage = e.Message});
                }

            }

            return Json(new { success = false, errorMessage = "Выберите файл" });
        }
         

        private string CreateAndUploadExcel(IEnumerable<SmsMessageMsg> data, int reportId)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("SmsReports");
                var headerRow = new List<string[]>()
                {
                    new string[] { "Дата и время рассылки", "Номер пользователя", "Текст сообщения", "Стоимость сообщения", "Статус сообщения" }
                };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets["SmsReports"];

                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                if (!Directory.Exists($"{folderPath}/SmsReports"))
                    Directory.CreateDirectory($"{folderPath}/SmsReports");
                worksheet.Cells[headerRange].Style.Font.Bold = true;
                worksheet.Cells[headerRange].Style.Font.Size = 14;
                worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);
                worksheet.Cells["A1:G100"].AutoFitColumns();
                worksheet.Cells["A1:A100"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                worksheet.Cells["G1"].Value = "Статусы сообщения";
                worksheet.Cells["G2"].Value = "0-Сообщение находится в очереди на отправку";
                worksheet.Cells["G3"].Value = "1-Сообщение отправлено (передано оператору)";
                worksheet.Cells["G4"].Value = "2-Сообщение отклонено (не передано. Например, из-за несуществующего регионального кода)";
                worksheet.Cells["G5"].Value = "3-Сообщение успешно доставлено";
                worksheet.Cells["G6"].Value = "4-Сообщение не доставлено";
                worksheet.Cells["G7"].Value = "5-Сообщение не отправлено из-за нехватки средств на счету партнера";
                worksheet.Cells["G8"].Value = "6-Неизвестный (новый) статус отправки";
                worksheet.Cells["G9"].Value = "7-Истек период ожидания отчета о доставке от SMSC. Обработка сообщения прекращена";

                worksheet.Cells[2, 1].LoadFromCollection(data.Select(x=> new{x.SendDate, x.Phone, x.Text, x.SmsCost, x.Report}));

                var path = $"{folderPath}/Reports/SmsReports/{reportId}.xlsx";

                FileInfo excelFile = new FileInfo(path);
                excel.SaveAs(excelFile);

                var uriPath = $"{apiUrl}/Reports/SmsReports/{reportId}.xlsx";
                return uriPath;
            }
        }

        private List<string> GetMobileOperatorRegularExpressions()
        {
            return new List<string>()
            {
                "(^996)(22[\\d]|77[\\d])\\d{6}$",
                "(^996)(755|55[\\d]|999)\\d{6}$",
                "(^996)(50[\\d]|70[\\d])\\d{6}$"
            };
        }

        public ActionResult SmsSendForm()
        {
            ViewBag.SmsTemplates = GetSmsTemplates();
            return PartialView("SmsSendForm");
        }

        public ActionResult SmsPhoneListSend(List<string> approvedUsers, List<string> notApprovedUsers, List<string> notFoundUsers)
        {
            ViewBag.SmsTemplates = GetSmsTemplates();
            ViewBag.UsersTypeSelectList = GetUserTypes(approvedUsers,notApprovedUsers,notFoundUsers);

            return PartialView("SmsSendPhones");
        }

        public ActionResult GetSmsMessageCostByTemplate(int templateId, string usersGroup)
        {
            string[] stringList = usersGroup.Split(',');
            var template = SmsTemplateRepository.Find(templateId);
            if (template != null && !string.IsNullOrEmpty(usersGroup))
            {
                var cost = (double) template.SmsCost * stringList.Length;
                return Json(new {success = true, cost, userCount = stringList.Length});
            }
            return Json(new { success = false, cost=0, userCount=0, message = "В списке нет пользователей"});
        }

        public ActionResult SendSmsUsersGroup(SmsSendPhonesModel model)
        {
            if(model.SendDate < DateTime.Now.AddMinutes(9))
                return Json(new { success = false, message = "Дата должна быть позже на 10 минут текущей" });
            var currUser = AuthenticationContext.GetCurrentUser();
            var template = SmsTemplateRepository.Find(model.TemplateId);
            if (template == null || string.IsNullOrEmpty(model.Phones))
                return Json(new {success = false, message = "Шаблон не найден или выбрано 0 пользователей"});
            string[] usersPhones = model.Phones.Split(',');
            try
            {
                var smsMessage = new SmsMessage()
                {
                    SendTime = model.SendDate,
                    Text = template.Text,
                    UsersCount = usersPhones.Length,
                    SmsCost = template.SmsCost * usersPhones.Length,
                    Title = template.Title,
                    AdminLogin = currUser.Login,
                    StatusId = 2,
                    IsPasswordRequired = template.IsPasswordRequired
                };
                SmsMessageRepository.InsertOrUpdate(smsMessage);
                SmsMessageRepository.Save();

                List<SmsMessageMsg> messages = new List<SmsMessageMsg>();

                foreach (var phone in usersPhones)
                {
                    var msg = new SmsMessageMsg()
                    {
                        SmsCost = template.SmsCost,
                        SendDate = model.SendDate,
                        Phone = phone,
                        Text = template.Text,
                        SmsMessageId = smsMessage.Id,
                    };
                    messages.Add(msg);
                }

                SmsMessageMsgRepository.AddRange(messages);
                SmsMessageMsgRepository.Save();

                return Json(new { success = true});
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return Json(new { success = false, message = ex.Message });
            }
        }

        /*   [HttpGet]
           public double GetSmsCost(string smsText)
           {
               info info = new info();

               string sms = XmlSerializator.GetXMLFromObject(info);

               try
               {
                   var result = SendHttpRequest(sms, ConfigurationManager.AppSettings["nikitaInfo"]);
                   string cost = GetBetween(result, "<smsprice>", "</smsprice>");
                   cost = cost.Replace('.', ',');
                   double smsCost = double.Parse(cost);

                   double templateCost = GetMessageCost(smsText, smsCost);

                   return templateCost;
               }
               catch(Exception)
               {
                   return 0;
               }
           }*/



        private static string SendHttpRequest(string parameters, string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            var postData = parameters;

            var data = Encoding.UTF8.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/xml";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }


        private static MessageResponseStatuses SendMessage(message message)
        {
            string result = String.Empty;

            try
            {
                string sms = XmlSerializator.GetXMLFromObject(message);

                result = SendHttpRequest(sms, ConfigurationManager.AppSettings["nikitaServiceUrl"]+"message");

                response response = (response)XmlSerializator.ObjectToXML(result, new response().GetType());

                MessageResponseStatuses status = NikitaResponseStatusCodes.GetResultByCode(response.status);

                if (status == MessageResponseStatuses.Error)
                {
                    Log.Error("An error occurred while sending sms message");
                    Log.Error(result);
                }

                return status;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Log.Error(result);

                return MessageResponseStatuses.Error;
            }

        }
        [ValidateInput(false)]
        public double GetMessageCost(string smsText, double smsCost, bool isChecked)
        {
            smsText = smsText.Replace("<p>", "\n");
            smsText = Regex.Replace(smsText, @"<[^>]+>|", "").Trim();
            smsText = smsText.Replace("&nbsp;", string.Empty);
            if (isChecked)
            {
                smsText += $"\nЛогин:555555555\nПароль:KOp9iZBdV9";
            }

            List<string> phones = new List<string>();
            phones.Add("996555555555");//любой номер для тестового запроса
            message message = new message
            {
                id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
                login = ConfigurationManager.AppSettings["loginNikita"],
                pwd = ConfigurationManager.AppSettings["pwdNikita"],
                time = DateTime.Now.ToString("yyyyMMddHHmmss"),
                sender = ConfigurationManager.AppSettings["smsSenderName"],
                phones = phones.ToArray(),
                text = smsText,
                test = "1"
            };

            string sms = XmlSerializator.GetXMLFromObject(message);

            try
            {
                var result = SendHttpRequest(sms, ConfigurationManager.AppSettings["nikitaServiceUrl"] + "message");

                response response = (response)XmlSerializator.ObjectToXML(result, new response().GetType());

                return response.smscnt * smsCost;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return 0;
            }
        }

        private static string GetBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        private IEnumerable<SelectListItem> GetSmsTemplates()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return new SelectList(SmsTemplateRepository.All.Where(t=>!t.IsDeleted)
                .OrderBy(a => a.Id).ToList()
                .Select(a => new SelectListItem
                    { Value = serializer.Serialize(new { a.Id, Content = a.Text, cost = a.SmsCost }), Text = a.Title})
                .ToList(), "Value", "Text");
        }

        private IEnumerable<SelectListItem> GetUserTypes(List<string> approvedUsers, List<string> notApprovedUsers, List<string> notFoundUsers)
        {
            return new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem { Selected = true, Text = "Номера, которые есть в базе", Value = string.Join(",", approvedUsers.ToArray())},
                    new SelectListItem { Selected = false, Text = "Неподтвержденные пользователи", Value = string.Join(",", notApprovedUsers.ToArray())},
                    new SelectListItem { Selected = false, Text = "Номера, которых нет в базе", Value = string.Join(",", notFoundUsers.ToArray())},
                }, "Value", "Text", 1);
        }

        private IEnumerable<SelectListItem> GetSmsStatuses()
        {
            return new SelectList(SmsStatusesRepository.All
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.StatusName })
                .ToList(), "Value", "Text");
        }
    

        public double? GetMessageCostUserPage(int id)
        {
            var template = SmsTemplateRepository.Find(id);

            return template.SmsCost;
        }

    }

    /*public enum NikitaReportStatuses
    {
        InQueue = 0,
        Sent,
        Canceled,
        Success,
        Failed,
        NoMoney,
        Undefined,
        LongWaitTime
    }

    public enum NikitaStatus
    {
        RequestOk = 0,
        RequestBodyError,
        Unauthorized,
        WrongIp,
        ReportNotFound
    }*/
}