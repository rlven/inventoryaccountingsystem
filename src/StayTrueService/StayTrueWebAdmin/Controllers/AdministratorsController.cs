﻿using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Models;
using DataAccessLayer;
using Microsoft.AspNet.Identity;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "superadmin, administrator")]
    public class AdministratorsController : BaseController
    {
        public AdministratorsController(IAdministratorRepository administratorRepository, IRolesRepository rolesRepository)
        {
            AdministratorRepository = administratorRepository;
            RolesRepository = rolesRepository;
        }

        public IAdministratorRepository AdministratorRepository { get; set; }
        public IRolesRepository RolesRepository { get; set; }

        public ActionResult Index(AdministratorFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var administrators = AdministratorRepository.All;

            var filteredModel = new AdministratorFilteredModel(filter);
            filteredModel.ProcessData(administrators, "Id", true, pageNumber);
            filteredModel.Roles = GetRoles();
            return View(filteredModel);
        }

        [AdminAuthorize(Roles = "superadmin, administrator")]
        public ActionResult CreateAdministrator()
        {
            var model = new Administrators();
            ViewBag.Roles = GetRoles();
            return PartialView("_CreateAdministrator", model);
        }


        [HttpPost]
        [AdminAuthorize(Roles = "superadmin")]
        public ActionResult CreateAdministratorPost(Administrators model)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    if(AdministratorRepository.All.Any(x=>x.Login.Equals(model.Login)))
                        return Json(new
                        {
                            success = false,
                            message = "Поля заполнены неверно, такой логин уже существует!"
                        });
                    model.PhoneNumber = model.PhoneNumber.Replace(" ", "");
                    model.Password = AdministratorRepository.GetHash(model.Password);
                    AdministratorRepository.InsertOrUpdate(model);
                    AdministratorRepository.Save();
                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполения программы",
                    debug = ex.Message
                });
            }

        }

        private SelectList GetRoles()
        {
            var roles = RolesRepository.Find(AuthenticationContext.CurrentUser.RoleId)
                .RolesAccess1.Select(x => x.Roles).ToList();
            return new SelectList(roles.Select(x => new SelectListItem {Value = x.Id.ToString(), Text = x.RoleName})
                .ToList(), "Value", "Text");
        }
    }
}