﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class QuestionnaireReportController : BaseController
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];

        public QuestionnaireReportController(IQuestionnaireAnswersRepository questionnaireAnswersRepository, IChatGroupsRepository chatGroupRepository)
        {
            QuestionnaireAnswersRepository = questionnaireAnswersRepository;
            ChatGroupRepository = chatGroupRepository;
        }

        public IQuestionnaireAnswersRepository QuestionnaireAnswersRepository { get; set; }

        public IChatGroupsRepository ChatGroupRepository { get; set; }
        // GET: QuestionnaireReport
        public ActionResult Index(QuestionnaireAnswersFilterModel filter, int? page)
        {
            int pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var questionnaireAnswers = QuestionnaireAnswersRepository.All.Where(x => x.IsDeleted == false);

            var filteredModel = new QuestionnaireAnswersFilteredModel(filter);
            filteredModel.ProcessData(questionnaireAnswers, "Id", true, pageNumber);
            var chatsGroups = ChatGroupRepository.All.Where(x => x.IsPrivate && x.IsDeleted == false && x.IsActive).ToList();
            foreach (var chatGroup in chatsGroups)
            {
                chatGroup.Image = $"{FolderPath}{chatGroup.Image}";
            }
            ViewBag.Chats = chatsGroups;

            return View(filteredModel);
        }

        [HttpPost]
        public ActionResult Delete(int[] ids)
        {

            if (!Request.IsAjaxRequest())
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            try
            {
                foreach (var id in ids)
                {
                    var questionnaireAnswers = QuestionnaireAnswersRepository.Find(id);
                    if (questionnaireAnswers != null)
                    {
                          questionnaireAnswers.IsDeleted = true;
                    }
                }

                QuestionnaireAnswersRepository.Save();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }
    }
}