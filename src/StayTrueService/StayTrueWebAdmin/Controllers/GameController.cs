﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Common.Models;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using WebGrease.Css.Extensions;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class GameController : Controller
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        public GameController(IGamesRepository gamesRepository)
        {
            GamesRepository = gamesRepository;
        }

        public IGamesRepository GamesRepository { get; set; }

        // GET: Game list
        public ActionResult Index(GamesFilterModel filter, int? page)
        {
            int pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var games = GamesRepository.All;

            var filteredModel = new GamesFilteredModel(filter);
            filteredModel.ProcessData(games, "Id", true, pageNumber);

            filteredModel.Games.ForEach(x =>
            {
                x.ImagePath = $"{FolderPath}{x.ImagePath}";
            });

            return View(filteredModel);
        }

        public ActionResult CreateGame()
        {
            var model = new Games();
            model.PublicationDate = DateTime.Now;
            return PartialView("_CreateGame", model);
        }

        [HttpPost]
        public ActionResult GetBestScores(int gameId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new {success = false, message = "Запрос напрямую. Вы че хакер?"});

            try
            {
                using (var context = new StayTrueDBEntities())
                {
                    IQueryable<GameSession> gameSession = context.GameSession.OrderByDescending(x => x.Id);

                    if (gameId > 0)
                        gameSession = gameSession.Where(x => x.GameId == gameId);

                    List<GameScore> gamesScores = gameSession.ToList().Select(g => new GameScore { UserFullName = g.Users.FirstName + " " + g.Users.LastName, GameId = g.GameId, Points = g.Score, UserId = g.UserId }).ToList();


                    List<GameScore> groupedGamesScores = gamesScores.GroupBy(gs => new { gs.UserId, gs.GameId, gs.UserFullName }).Select(gs =>
                        new GameScore()
                        {
                            GameId = gs.Key.GameId,
                            UserId = gs.Key.UserId,
                            UserFullName = gs.Key.UserFullName,
                            Score = gs.Max(m => m.Points)
                        }).ToList();

                    return Json(new {success=true, liders = groupedGamesScores});
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult CreateGamePost(Games model, HttpPostedFileBase preview, HttpPostedFileBase gameZip)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    var games = GamesRepository.All.FirstOrDefault(x => x.Name == model.Name);
                    if (games == null)
                    {
                        model.IsActive = true;
                        GamesRepository.InsertOrUpdate(model);
                        GamesRepository.Save();

                        if (preview != null)
                            model.ImagePath = ImageHelper.SaveImage(preview, "Games", model.Id.ToString());
                        if (gameZip != null)
                            model.GameUrl = FileHelper.UploadZip(gameZip, "Games", model.Id.ToString());
                        GamesRepository.Save();

                        return Json(new {success = true});
                    }
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        public ActionResult Edit(GameEditModel item, HttpPostedFileBase preview, HttpPostedFileBase gameZip)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    Games model = GamesRepository.Find(item.Id);
                    if (model == null)
                    {
                        return Json(new {success = false, message = "Такой игры не существует"});
                    }

                    model.WincoinsExchange = item.WincoinsExchange;
                    model.IsActive = item.IsActive;
                    model.Name = item.Name;
                    model.PublicationDate = item.PublicationDate;
                    model.Wincoins = item.Wincoins;

                    if (preview != null)
                    {
                        model.ImagePath = ImageHelper.SaveImage(preview, "Games", item.Id.ToString());
                    }

                    if (gameZip != null)
                    {
                        FileHelper.DeleteFolder(item.GameUrl);
                        model.GameUrl = FileHelper.UploadZip(gameZip, "Games", item.Id.ToString());
                    }

                    // item.GameUrl = FileHelper.SaveGame(gameZip, item.Id.ToString());
                    GamesRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            var game = GamesRepository.All.FirstOrDefault(x => x.Id == id);

            if (game == null)
            {
                return Json(new { success = true });
            }
            
            GamesRepository.Delete(id);
            GamesRepository.Save();

            return Json(new { success = true });
        }
    }
}