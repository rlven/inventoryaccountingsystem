﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using Common.Models;
using DataAccessLayer;
using OfficeOpenXml;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.FilterModels;
using StayTrueWebAdmin.Models.ViewModels;
using GreatBattleQuestionnaireAnswer = DataAccessLayer.GreatBattleQuestionnaireAnswer;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class GreatBattleMembersController : BaseController
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly string apiUrl = $"{ConfigurationManager.AppSettings["VirtualDirectoryUrl"]}";
        private static readonly string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";

        public GreatBattleMembersController(IGreatBattleMembersRepository greatBattleMembersRepository, IChatGroupsRepository chatGroupRepository,
            IGreatBattleQuestionnaireAnswersRepository greatBattleQuestionnaireAnswersRepository, ICitiesRepository citiesRepository, IGreatBattlesRepository greatBattlesRepository,
            IGreatBattleTourPointsRepository greatBattleTourPointsRepository, IChatPrivateGroupsUsersRepository chatPrivateGroupsUsersRepository)
        {
            GreatBattleMembersRepository = greatBattleMembersRepository;
            ChatGroupsRepository = chatGroupRepository;
            GreatBattleQuestionnaireAnswersRepository = greatBattleQuestionnaireAnswersRepository;
            CitiesRepository = citiesRepository;
            GreatBattlesRepository = greatBattlesRepository;
            GreatBattleTourPointsRepository = greatBattleTourPointsRepository;
            ChatPrivateGroupsUsersRepository = chatPrivateGroupsUsersRepository;
        }

        public IGreatBattleMembersRepository GreatBattleMembersRepository { get; set; }
        public IGreatBattleTourPointsRepository GreatBattleTourPointsRepository { get; set; }
        public IChatPrivateGroupsUsersRepository ChatPrivateGroupsUsersRepository { get; }
        public IGreatBattleQuestionnaireAnswersRepository GreatBattleQuestionnaireAnswersRepository { get; set; }

        public IChatGroupsRepository ChatGroupsRepository { get; set; }
        public ICitiesRepository CitiesRepository { get; set; }
        public IGreatBattlesRepository GreatBattlesRepository { get; set; }

        public ActionResult Index(GreatBattleMembersFilterModel filter ,int? page, int greatBattleId)
        {
            int pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;


            //     var greatBattleUserAnswers = GreatBattleQuestionnaireAnswersRepository.All;//get all user answers
            var greatBattleMembers = GreatBattleMembersRepository.All.Where(x=>x.GreatBattleID == greatBattleId);

            var filterPoints = filter.Points;
            filter.Points = null;
            var filteredModel = new GreatBattleMembersFilteredModel(filter);
            filteredModel.ProcessData(greatBattleMembers, "Id", true, pageNumber);
            var splitedCollection = new List<GreatBattleMemberViewModel>();

            foreach (var filterModel in filteredModel.GreatBattlesUsers)
            {
                var answer = filterModel.Users.GreatBattleQuestionnaireAnswer.FirstOrDefault(y => y.GreatBattleQuestionnaire.IDGreatBattle == filterModel.GreatBattleID);
                var greatBattleMemberViewModel = new GreatBattleMemberViewModel()
                {
                    Id = filterModel.ID,
                    Answer1 = answer == null ? "" : answer.Answer1,
                    Answer2 = answer == null ? "" : answer.Answer2,
                    Answer3 = answer == null ? "" : answer.Answer3,
                    Answer4 = answer == null ? "" : answer.Answer4,
                    GreatBattleGroups = filterModel.GreatBattleGroups,
                    Points = CountUserPoints(filterModel.UserID, greatBattleId),
                    IsBlocked = filterModel.IsBlocked,
                    Users = filterModel.Users
                };
                splitedCollection.Add(greatBattleMemberViewModel);
            }

            if (filterPoints != null)
            {
                splitedCollection = splitedCollection.Where(x => x.Points == filterPoints).ToList();
            }

            var chatsGroups = ChatGroupsRepository.All.Where(x => x.IsPrivate && x.IsDeleted == false && x.IsActive).ToList();
            foreach (var chatGroup in chatsGroups)
            {
                chatGroup.Image = $"{FolderPath}{chatGroup.Image}";
            }
            ViewBag.Chats = chatsGroups;
            ViewBag.GreatBattleMembers = splitedCollection;
            ViewBag.GreatBattleId = greatBattleId;
            ViewBag.Cities = GetCitiesSelectList(filter.City);

            return View(filteredModel);
        }

        private int CountUserPoints(int userId, int greatBattleId)
        {
            return GreatBattleTourPointsRepository.All
                .Where(x => x.UserID == userId && x.GreatBattleTours.GreatBattleID == greatBattleId)
                .Sum(x => (int?) x.PointsCount) ?? 0;
        }

        private IQueryable<GreatBattleMemberViewModel> GetUserAnswers(IQueryable<GreatBattlesUsers> greatBattleMembers, 
            IQueryable<GreatBattleQuestionnaireAnswer> greatBattleUserAnswers)
        {
            List<GreatBattleMemberViewModel> greatBattleMemberViewModels = new List<GreatBattleMemberViewModel>();
            foreach (var member in greatBattleMembers)
            {
                GreatBattleMemberViewModel vm = new GreatBattleMemberViewModel();
                vm.Users = member.Users;
                vm.GreatBattleGroups = member.GreatBattleGroups;
                vm.Points = member.Points;
                var answers = greatBattleUserAnswers.FirstOrDefault(u => u.UserId == member.UserID);
                if (answers != null)
                {
                    vm.Answer1 = answers.Answer1;
                    vm.Answer2 = answers.Answer2;
                    vm.Answer3 = answers.Answer3;
                    vm.Answer4 = answers.Answer4;
                }
                greatBattleMemberViewModels.Add(vm);
            }

            return greatBattleMemberViewModels.AsQueryable();
        }

        private SelectList GetCitiesSelectList(int? selected)
        {
            return new SelectList(CitiesRepository.All.Select(x => new SelectListItem()
            {
                Text=x.CityName,
                Value=x.Id.ToString()
            }).ToList(), "Value", "Text", selected);
        }

        [HttpPost]
        public ActionResult BlockMembers(int[] membersId, int greatBattleId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                foreach (var id in membersId)
                {
                    var member = GreatBattleMembersRepository.All.FirstOrDefault(m =>
                            m.UserID == id && m.GreatBattleID == greatBattleId);
                    if (member != null)
                    {
                        member.IsBlocked = true;
                        GreatBattleMembersRepository.Save();

                        RemoveUserFromGreatBattleChats(member.UserID, member.GreatBattleGroups.Name);
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }
        
        private void RemoveUserFromGreatBattleChats(int userId, string groupName)
        {
            ChatGroups group = ChatGroupsRepository.All.FirstOrDefault(g => g.Name == groupName);
            ChatPrivateGroupsUsers groupUser = group?.ChatPrivateGroupsUsers.FirstOrDefault(g => g.UserId == userId);
            if (groupUser != null)
            {
                ChatPrivateGroupsUsersRepository.Delete(groupUser.Id);
                ChatPrivateGroupsUsersRepository.Save();
            }
        }

        [HttpPost]
        public ActionResult UnblockMembers(int[] membersId, int greatBattleId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                foreach (var id in membersId)
                {
                    var member = GreatBattleMembersRepository.All.FirstOrDefault(m =>
                        m.UserID == id && m.GreatBattleID == greatBattleId);
                    if (member != null)
                    {
                        member.IsBlocked = false;
                        GreatBattleMembersRepository.Save();

                        AddUserToGreatBattleChats(member.UserID, member.GreatBattleGroups.Name);
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }

        private void AddUserToGreatBattleChats(int userId, string groupName)
        {
            var group = ChatGroupsRepository.All.FirstOrDefault(g => g.Name == groupName);
            if (group != null)
            {
                var groupUser =
                    group.ChatPrivateGroupsUsers.FirstOrDefault(g => g.UserId == userId);
                if (groupUser == null)
                {
                    var newGroup = new ChatPrivateGroupsUsers
                    {
                        ChatGroupId = group.Id,
                        UserId = userId
                    };
                    ChatPrivateGroupsUsersRepository.InsertOrUpdate(newGroup);
                    ChatPrivateGroupsUsersRepository.Save();
                }
            }
        }

        [HttpPost]
        public string GetBattleUsersReport(int id)
        {
            var battleUsers = GreatBattleMembersRepository.AllIncluding().Where(b => b.GreatBattleID == id);

            var redUsers = battleUsers.Where(r => r.GroupID == 2);

            var blueUsers = battleUsers.Where(r => r.GroupID == 1);

            List<GreatBattleMembersReportViewModel> redModel = new List<GreatBattleMembersReportViewModel>();

            foreach (var redUser in redUsers)
            {
                var model = new GreatBattleMembersReportViewModel()
                {
                    Name = redUser.Users.FirstName + " " + redUser.Users.LastName,
                    Id = redUser.UserID,
                    PhoneNumber = redUser.Users.Phone,
                    Wincoins = redUser.Users.Wincoins,
                    BattleCoins = CountUserPoints(redUser.UserID, id)
                };
                redModel.Add(model);
            }

            List<GreatBattleMembersReportViewModel> blueModel = new List<GreatBattleMembersReportViewModel>();

            foreach (var blueUser in blueUsers)
            {
                var model = new GreatBattleMembersReportViewModel()
                {
                    Name = blueUser.Users.FirstName + " " + blueUser.Users.LastName,
                    Id = blueUser.UserID,
                    PhoneNumber = blueUser.Users.Phone,
                    Wincoins = blueUser.Users.Wincoins,
                    BattleCoins = CountUserPoints(blueUser.UserID, id)
                };

                blueModel.Add(model);
            }

            return CreateAndUploadExcel(redModel.OrderByDescending(x=>x.Wincoins), blueModel.OrderByDescending(x=>x.Wincoins), id);
        }

        private string CreateAndUploadExcel(IEnumerable<GreatBattleMembersReportViewModel> data1, IEnumerable<GreatBattleMembersReportViewModel> data2, int reportId)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("BattleUsersRed");
                var headerRowRed = new List<string[]>()
                {
                    new string[] { "ФИО", "ID", "Номер телефона", "Текущий баланс в винкоинах", "Батл коины" }
                };

                excel.Workbook.Worksheets.Add("BattleUsersBlue");
                var headerRowBlue = new List<string[]>()
                {
                    new string[] { "ФИО", "ID", "Номер телефона", "Текущий баланс в винкоинах", "Батл коины" }
                };

                // Determine the header range (e.g. A1:D1)
                string headerRangeRed = "A1:" + Char.ConvertFromUtf32(headerRowRed[0].Length + 64) + "1";
                string headerRangeBlue = "A1:" + Char.ConvertFromUtf32(headerRowBlue[0].Length + 64) + "1";

                // Target a worksheet
                var worksheetRed = excel.Workbook.Worksheets["BattleUsersRed"];
                var worksheetBlue = excel.Workbook.Worksheets["BattleUsersBlue"];

                // Popular header row data
                worksheetRed.Cells[headerRangeRed].LoadFromArrays(headerRowRed);
                worksheetBlue.Cells[headerRangeBlue].LoadFromArrays(headerRowBlue);

                if (!Directory.Exists($"{folderPath}/Reports/BattleUsers"))
                    Directory.CreateDirectory($"{folderPath}/Reports/BattleUsers");

                worksheetRed.Cells["A1:G100"].AutoFitColumns();
                worksheetBlue.Cells["A1:G100"].AutoFitColumns();

                worksheetRed.Cells[2, 1].LoadFromCollection(data1.Select(x => new { x.Name, x.Id, x.PhoneNumber, x.Wincoins, x.BattleCoins }));
                worksheetBlue.Cells[2, 1].LoadFromCollection(data2.Select(x => new { x.Name, x.Id, x.PhoneNumber, x.Wincoins, x.BattleCoins }));

                var path = $"{folderPath}/Reports/BattleUsers/{reportId}.xlsx";

                FileInfo excelFile = new FileInfo(path);
                excel.SaveAs(excelFile);

                var uriPath = $"{apiUrl}/Reports/BattleUsers/{reportId}.xlsx";
                return uriPath;
            }
        }

    }
}