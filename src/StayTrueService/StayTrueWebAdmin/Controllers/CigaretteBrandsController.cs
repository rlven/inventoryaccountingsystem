﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using GleamTech.VideoUltimate;
using Microsoft.Ajax.Utilities;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class CigaretteBrandsController : Controller
    {
        public ICigaretteBrandsRepository CigaretteBrandsRepository { get; }
        public ICigarettePackSizesRepository CigarettePackSizesRepository { get; }
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];

        public CigaretteBrandsController(ICigaretteBrandsRepository cigaretteBrandsRepository, ICigarettePackSizesRepository cigarettePackSizesRepository)
        {
            CigaretteBrandsRepository = cigaretteBrandsRepository;
            CigarettePackSizesRepository = cigarettePackSizesRepository;
        }
        
        public ActionResult Index(CigaretteBrandsFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
            var brands = CigaretteBrandsRepository.All;
            var filteredModel = new CigaretteBrandsFilteredModel(filter);
            filteredModel.ProcessData(brands, "BrandPosition", true, pageNumber);
            filteredModel.Brands.ForEach(b =>
            {
                b.AdminImage = $"{FolderPath}{b.AdminImage}";
                b.BrandImageP = $"{FolderPath}{b.BrandImageP}";
                b.BrandImageM = $"{FolderPath}{b.BrandImageM}";
                b.DescriptionImageP = $"{FolderPath}{b.DescriptionImageP}";
                b.DescriptionImageP = $"{FolderPath}{b.DescriptionImageM}";
                b.AdditionalImage1 = $"{FolderPath}{b.AdditionalImage1}";
                b.AdditionalImage2 = $"{FolderPath}{b.AdditionalImage2}";
                b.AdditionalImage3 = $"{FolderPath}{b.AdditionalImage3}";
                b.AdditionalImage4 = $"{FolderPath}{b.AdditionalImage4}";
            });
            filteredModel.Positions = GetPositions();
            return View(filteredModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Positions = GetPositions();
            ViewBag.PackSizes = GetCategories(null);
            return View();
        }

        [HttpPost]
        public ActionResult Create(CigaretteBrandsEditModel brand)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Direct request" });
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var newBrand = new CigaretteBrands
                    {
                        BrandName = brand.BrandName,
                        BrandCompany = brand.CompanyBrand,
                        Description = brand.Description,
                        BrandImageP = "",
                        BrandImageM = "",
                        DescriptionImageP = "",
                        DescriptionImageM = "",
                        AdminImage = "",
                        Tar = brand.Tar,
                        Nicotine = brand.Nicotine,
                        PackSize = brand.PackSize,
                        AdditionalInfo1 = brand.AdditionalInfo1,
                        AdditionalInfo2 = brand.AdditionalInfo2,
                        AdditionalInfo3 = brand.AdditionalInfo3,
                        AdditionalInfo4 = brand.AdditionalInfo4,
                        BrandPosition = brand.BrandPosition,
                        CreationDate = DateTime.Now,
                        IsActive = brand.IsActive
                    };

                    ShiftPosition(brand.BrandPosition, 1);
                    CigaretteBrandsRepository.InsertOrUpdate(newBrand);
                    CigaretteBrandsRepository.Save();
                    
                    if (brand.BrandImageP != null)
                    {
                        newBrand.BrandImageP = ImageHelper.SaveImage(brand.BrandImageP,
                            "CigaretteBrands/Web/Brand",
                              newBrand.Id.ToString());
                    }

                    if (brand.DescriptionImageP != null)
                    {
                        newBrand.DescriptionImageP = ImageHelper.SaveImage(brand.DescriptionImageP,
                            "CigaretteBrands/Web/Description",
                            newBrand.Id.ToString());
                    }

                    if (brand.BrandImageM != null)
                    {
                        newBrand.BrandImageM = ImageHelper.SaveImage(brand.BrandImageM,
                            "CigaretteBrands/Mobile/Brand",
                            newBrand.Id.ToString());
                    }

                    if (brand.DescriptionImageM != null)
                    {
                        newBrand.DescriptionImageM = ImageHelper.SaveImage(brand.DescriptionImageM,
                            "CigaretteBrands/Mobile/Description",
                            newBrand.Id.ToString());
                    }

                    if (brand.AdminImage != null)
                    {
                        newBrand.AdminImage = ImageHelper.SaveImage(brand.AdminImage,
                            "CigaretteBrands/Admin",
                            newBrand.Id.ToString());
                    }

                    if (brand.AdditionalImage1 != null)
                    {
                        newBrand.AdditionalImage1 = ImageHelper.SaveImage(brand.AdditionalImage1,
                            "CigaretteBrands/Icons",
                            newBrand.Id + "_1");
                    }
                    if (brand.AdditionalImage2 != null)
                    {
                        newBrand.AdditionalImage2 = ImageHelper.SaveImage(brand.AdditionalImage2,
                            "CigaretteBrands/Icons",
                            newBrand.Id + "_2");
                    }
                    if (brand.AdditionalImage3 != null)
                    {
                        newBrand.AdditionalImage3 = ImageHelper.SaveImage(brand.AdditionalImage3,
                            "CigaretteBrands/Icons",
                            newBrand.Id + "_3");
                    }
                    if (brand.AdditionalImage4 != null)
                    {
                        newBrand.AdditionalImage4 = ImageHelper.SaveImage(brand.AdditionalImage4,
                            "CigaretteBrands/Icons",
                            newBrand.Id + "_4");
                    }

                    CigaretteBrandsRepository.Save();
                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Positions = GetPositions();
            ViewBag.PackSizes = GetCategories(null);
            var brand = CigaretteBrandsRepository.Find(id);
            var editBrand = new CigaretteBrandsEditModel
            {
                BrandPosition = brand.BrandPosition,
                Id = brand.Id,
                BrandName = brand.BrandName,
                Tar = brand.Tar,
                Nicotine = brand.Nicotine,
                PackSize = brand.PackSize,
                IsActive = brand.IsActive,
                Description = brand.Description,
                CompanyBrand = brand.BrandCompany,
                AdditionalInfo1 = brand.AdditionalInfo1,
                AdditionalInfo2 = brand.AdditionalInfo2,
                AdditionalInfo3 = brand.AdditionalInfo3,
                AdditionalInfo4 = brand.AdditionalInfo4,
                PathAdminImage = $"{FolderPath}{brand.AdminImage}",
                PathBrandImageP = $"{FolderPath}{brand.BrandImageP}",
                PathDescriptionImageP = $"{FolderPath}{brand.DescriptionImageP}",
                PathBrandImageM = $"{FolderPath}{brand.BrandImageM}",
                PathDescriptionImageM = $"{FolderPath}{brand.DescriptionImageM}",
                PathAdditionalImage1 = !string.IsNullOrEmpty(brand.AdditionalImage1) ? $"{FolderPath}{brand.AdditionalImage1}" : null,
                PathAdditionalImage2 = !string.IsNullOrEmpty(brand.AdditionalImage2) ? $"{FolderPath}{brand.AdditionalImage2}" : null,
                PathAdditionalImage3 = !string.IsNullOrEmpty(brand.AdditionalImage3) ? $"{FolderPath}{brand.AdditionalImage3}" : null,
                PathAdditionalImage4 = !string.IsNullOrEmpty(brand.AdditionalImage4) ? $"{FolderPath}{brand.AdditionalImage4}" : null
            };
            return View(editBrand);
        }

        [HttpPost]
        public ActionResult Edit(CigaretteBrandsEditModel model)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Not Ajax request" });
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var brand = CigaretteBrandsRepository.Find(model.Id);
                    if (brand == null)
                    {
                        return Json(new { success = false, message = "Такого бренда не существует" });
                    }

                    ShiftPosition(brand.BrandPosition, -1);
                    ShiftPosition(model.BrandPosition, 1);

                    brand.BrandName = model.BrandName;
                    brand.BrandPosition = model.BrandPosition;
                    brand.BrandCompany = model.CompanyBrand;
                    brand.Description = model.Description;
                    brand.Tar = model.Tar;
                    brand.Nicotine = model.Nicotine;
                    brand.IsActive = model.IsActive;
                    brand.AdditionalInfo1 = model.AdditionalInfo1;
                    brand.AdditionalInfo2 = model.AdditionalInfo2;
                    brand.AdditionalInfo3 = model.AdditionalInfo3;
                    brand.AdditionalInfo4 = model.AdditionalInfo4;
                    brand.PackSize = model.PackSize;

                    if (model.BrandImageP != null)
                    {
                        ImageHelper.DeleteImage(brand.BrandImageP);
                        brand.BrandImageP = ImageHelper.SaveImage(model.BrandImageP,
                            "CigaretteBrands/Web/Brand",
                            brand.Id.ToString());
                    }

                    if (model.DescriptionImageP != null)
                    {
                        ImageHelper.DeleteImage(brand.DescriptionImageP);
                        brand.DescriptionImageP = ImageHelper.SaveImage(model.DescriptionImageP,
                            "CigaretteBrands/Web/Description",
                            brand.Id.ToString());
                    }

                    if (model.BrandImageM != null)
                    {
                        brand.BrandImageM = ImageHelper.SaveImage(model.BrandImageM,
                            "CigaretteBrands/Mobile/Brand",
                            brand.Id.ToString());
                    }

                    if (model.DescriptionImageM != null)
                    {
                        brand.DescriptionImageM = ImageHelper.SaveImage(model.DescriptionImageM,
                            "CigaretteBrands/Mobile/Description",
                            brand.Id.ToString());
                    }

                    if (model.AdminImage != null)
                    {
                        ImageHelper.DeleteImage(brand.AdminImage);
                        brand.AdminImage = ImageHelper.SaveImage(model.AdminImage,
                            "CigaretteBrands/Admin",
                            brand.Id.ToString());
                    }

                    if (model.AdditionalImage1 != null)
                    {
                        ImageHelper.DeleteImage(brand.AdditionalImage1);
                        brand.AdditionalImage1 = ImageHelper.SaveImage(model.AdditionalImage1,
                            "CigaretteBrands/Icons",
                            brand.Id + "_1");
                    }
                    if (model.AdditionalImage2 != null)
                    {
                        ImageHelper.DeleteImage(brand.AdditionalImage2);
                        brand.AdditionalImage2 = ImageHelper.SaveImage(model.AdditionalImage2,
                            "CigaretteBrands/Icons",
                            brand.Id + "_2");
                    }
                    if (model.AdditionalImage3 != null)
                    {
                        ImageHelper.DeleteImage(brand.AdditionalImage3);
                        brand.AdditionalImage3 = ImageHelper.SaveImage(model.AdditionalImage3,
                            "CigaretteBrands/Icons",
                            brand.Id + "_3");
                    }
                    if (model.AdditionalImage4 != null)
                    {
                        ImageHelper.DeleteImage(brand.AdditionalImage4);
                        brand.AdditionalImage4 = ImageHelper.SaveImage(model.AdditionalImage4,
                            "CigaretteBrands/Icons",
                            brand.Id + "_4");
                    }

                    CigaretteBrandsRepository.InsertOrUpdate(brand);
                    CigaretteBrandsRepository.Save();
                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Not Ajax request" });
            }

            var brand = CigaretteBrandsRepository.Find(id);
            if (brand == null)
            {
                return Json(new {success = false, message = "Такого бренда не существует"});
            }
            ShiftPosition(brand.BrandPosition, -1);
            CigaretteBrandsRepository.Delete(id);
            CigaretteBrandsRepository.Save();
            return Json(new {success = true});
        }

        private void ShiftPosition(int position, int shift)
        {
            var brand = CigaretteBrandsRepository.All.FirstOrDefault(x => x.BrandPosition == position);
            if (brand != null)
            {
                CigaretteBrandsRepository.All.Where(c => c.BrandPosition >= position).ToList()
                    .ForEach(x =>
                    {
                        x.BrandPosition = x.BrandPosition + shift;
                        CigaretteBrandsRepository.InsertOrUpdate(x);
                    });
            }
        }

        public SelectList GetCategories(int? selected)
        {
            return new SelectList(CigarettePackSizesRepository.All.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.SizeName }).ToList(), "Value", "Text", selected);
        }

        public SelectList GetPositions()
        {
            var positions = CigaretteBrandsRepository.All.Where(b => !b.IsDeleted).Select(b => b.BrandPosition)
                .OrderBy(b => b).ToList();
            int maxPosition = positions.Any() ? positions.Max() : 0;
            positions.Add(maxPosition + 1);
            return new SelectList(positions.Select((r, i) => new SelectListItem {Text = r.ToString(), Value = r.ToString()}).ToList(),
                "Value", "Text");
        }

        [HttpPost]
        public ActionResult DeleteAdditionalImage(int id, int image)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" });
            try
            {
                var brand = CigaretteBrandsRepository.Find(id);
                if (brand == null)
                {
                    return Json(new { success = false, message = $"Не удалось найти бренд {id}" });
                }

                switch (image)
                {
                    case 1:
                        if (!string.IsNullOrEmpty(brand.AdditionalImage1))
                        {
                            ImageHelper.DeleteImage(brand.AdditionalImage1);
                            brand.AdditionalImage1 = null;
                        }
                        break;
                    case 2:
                        if (!string.IsNullOrEmpty(brand.AdditionalImage2))
                        {
                            ImageHelper.DeleteImage(brand.AdditionalImage2);
                            brand.AdditionalImage2 = null;
                        }
                        break;
                    case 3:
                        if (!string.IsNullOrEmpty(brand.AdditionalImage3))
                        {
                            ImageHelper.DeleteImage(brand.AdditionalImage3);
                            brand.AdditionalImage3 = null;
                        }
                        break;
                    case 4:
                        if (!string.IsNullOrEmpty(brand.AdditionalImage4))
                        {
                            ImageHelper.DeleteImage(brand.AdditionalImage4);
                            brand.AdditionalImage4 = null;
                        }
                        break;
                    default:
                        break;
                }
                CigaretteBrandsRepository.Save();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, debug = ex.Message, message = "Ошибка при удалении изображения" });
            }
        }
    }
}