﻿using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Management;
using System.Web.Mvc;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using WebGrease.Css.Extensions;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin, retailer")]
    public class ShopController : BaseController
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        public ShopController(
            IProductsRepository productsRepository, 
            IShopsRepository shopsRepository,
            IProductsCategoriesRepository productsCategoriesRepository, 
            IPurchaseStatusesRepository purchaseStatusesRepository,
            IPurchaseHistoriesRepository purchaseHistoriesRepository)
        {
            ProductsRepository = productsRepository;
            ShopsRepository = shopsRepository;
            ProductsCategoriesRepository = productsCategoriesRepository;
            PurchaseStatusesRepository = purchaseStatusesRepository;
            PurchaseHistoriesRepository = purchaseHistoriesRepository;
        }

        public IProductsRepository ProductsRepository { get; set; }
        public IProductsCategoriesRepository ProductsCategoriesRepository { get; set; }
        public IShopsRepository ShopsRepository { get; set; }
        public IPurchaseStatusesRepository PurchaseStatusesRepository { get; set; }
        public IPurchaseHistoriesRepository PurchaseHistoriesRepository { get; set; }


        public ActionResult Index(ProductsFilterModel filter, int? page)
        {
            //ViewBag.UserEmail = AuthenticationContext.CurrentUser.Email;

            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var products = ProductsRepository.All;

            var filteredModel = new ProductsFilteredModel(filter);
            filteredModel.Shops = GetShopsListAll();
            filteredModel.Categories = GetCategories();
            filteredModel.ProcessData(products, "Id", true, pageNumber);
            ViewBag.ShopsEnum = GetShopsListAll();

            filteredModel.Products.ForEach(x =>
            {
                x.ImagePath = $"{FolderPath}{x.ImagePath}";
            });

            ViewBag.PurchaseStatuses = GetPurchaseStatuses();

            return View(filteredModel);
        }

        
        public ActionResult CreateProduct(SelectList shopsEnum)
        {
            var model = new Products {PublicationDate = DateTime.Now};
            ViewBag.ShopsEnum = shopsEnum;
            ViewBag.Categories = GetCategories();
            model.LanguageId = 1;
            return PartialView("_CreateProduct", model);
        }

        

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            var product = ProductsRepository.All.FirstOrDefault(x => x.Id == id);

            if (product == null)
            {
                return Json(new { success = true });
            }
            
            ProductsRepository.Delete(id);
            ProductsRepository.Save();

            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult EditShopHistory(int id, int statusId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            }

            try
            {
                PurchaseHistories histories = PurchaseHistoriesRepository.Find(id);
                if(histories==null)
                    return Json(new
                    {
                        success = false,
                        message = "Такая запись не найдена в БД"
                    });

                histories.StatusId = statusId;
                histories.AdministratorId = AuthenticationContext.CurrentUser.Id;

                
                PurchaseHistoriesRepository.Save();
                return Json(new
                {
                    success = true,
                    administratorName = AuthenticationContext.CurrentUser.LastName + " " + AuthenticationContext.CurrentUser.FirstName
                });


            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult CreateProductPost(Products model, HttpPostedFileBase productImage)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false , message = "Запрос  напрямую. Вы че хакер?" });
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.InformationRu = Regex.Replace(model.InformationRu, "<.*?[^>]+>|&nbsp;", String.Empty);
                    model.CreationDate = DateTime.Now;

                    ProductsRepository.InsertOrUpdate(model);
                    ProductsRepository.Save();

                    model.ImagePath = ImageHelper.SaveImage(productImage, "Products", model.Id.ToString());
                    ProductsRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult Edit(ProductsEditModel item,  HttpPostedFileBase productImage)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    Products model = ProductsRepository.Find(item.Id);
                    if (model == null)
                    {
                        return Json(new
                        {
                            success = false,
                            message = "Такой товар не найдет в БД"
                        });
                    }

                    model.Amount = item.Amount;
                    model.AvailableAmount = item.AvailableAmount;
                    model.CategoryId = item.CategoryId;
                    model.CreationDate= item.CreationDate;
                    model.InformationRu= Regex.Replace(item.InformationRu, "<.*?[^>]+>|&nbsp;", String.Empty);
                    model.InformationKg = item.InformationKg;
                    model.IsAvailable = item.IsAvailable;
                    model.IsBrand = item.IsBrand;
                    model.LanguageId = model.LanguageId;
                    model.NameKg = item.NameKg;
                    model.NameRu = item.NameRu;
                    model.Price = item.Price;
                    model.PublicationDate = item.PublicationDate;
                    model.ShopId = item.ShopId;

                    if (productImage != null)
                    {
                       model.ImagePath = ImageHelper.SaveImage(productImage, "Products", item.Id.ToString());
                    }

                    ProductsRepository.InsertOrUpdate(model);
                    ProductsRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        private SelectList GetCategories()
        {
            return new SelectList(ProductsCategoriesRepository.All.Where(a => a.IsActive)
                .OrderBy(a => a.Name)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name })
                .ToList(), "Value", "Text");
        }


        public SelectList GetShopsListAll()
        {
            return new SelectList(ShopsRepository.All.Where(a => a.IsActive)
                .OrderBy(a => a.Name)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name })
                .ToList(), "Value", "Text");
        }

        private SelectList GetPurchaseStatuses()
        {
            return new SelectList(PurchaseStatusesRepository.All
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name })
                .ToList(), "Value", "Text");
        }
    }
}