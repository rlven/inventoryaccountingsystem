﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.FilterModels;
using StayTrueWebAdmin.Models.ViewModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class PartyMembersController : Controller
    {
        private static readonly string FolderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryPath"];
        public IPartyMembersRepository PartyMembersRepository { get; }
        public IChatGroupsRepository ChatGroupsRepository { get; }
        public IChatPrivateGroupsUsersRepository PrivateGroupsUsersRepository { get; }

        public PartyMembersController(IPartyMembersRepository partyMembersRepository, IChatGroupsRepository chatGroupsRepository, IChatPrivateGroupsUsersRepository privateGroupsUsersRepository)
        {
            PartyMembersRepository = partyMembersRepository;
            ChatGroupsRepository = chatGroupsRepository;
            PrivateGroupsUsersRepository = privateGroupsUsersRepository;
        }
        
        public ActionResult Index()
        {
            var chatsGroups = ChatGroupsRepository.All.Where(x => x.IsPrivate && x.IsDeleted == false && x.IsActive).ToList();
            foreach (var chatGroup in chatsGroups)
            {
                chatGroup.Image = $"{FolderUrl}{chatGroup.Image}";
            }

            ViewBag.Chats = chatsGroups;
            return View();
        }

        public ActionResult GetMembers(PartyMembersFilterModel filter, int? page, int partyId)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
            var partyMembers = PartyMembersRepository.All.Where(x => x.PartyId == partyId);
            var filteredModel = new PartyMembersFilteredModel(filter);
            filteredModel.ProcessData(partyMembers, "Id", true, pageNumber);
            var members = new List<PartyMembersViewModel>();

            foreach (var partyMember in filteredModel.PartyMembers)
            {
                var answer = partyMember.Users.PartyQuestionnaireAnswer.FirstOrDefault(y => y.PartyQuestionnaire.PartyId == partyMember.PartyId);
                var partyMembersViewModel = new PartyMembersViewModel
                {
                    Id = partyMember.Id,
                    User = partyMember.Users,
                    IsBlocked = partyMember.IsBlocked,
                    IsInvited = partyMember.IsInvited,
                    Points = partyMember.Points,
                    HasCome = partyMember.HasCome
                };
                if (answer != null)
                {
                    partyMembersViewModel.Answer1 = answer.Answer1;
                    partyMembersViewModel.Answer2 = answer.Answer2;
                    partyMembersViewModel.Answer3 = answer.Answer3;
                    partyMembersViewModel.Answer4 = answer.Answer4;
                    partyMembersViewModel.Answer5 = answer.Answer5;
                    partyMembersViewModel.Answer6 = answer.Answer6;
                    partyMembersViewModel.Answer7 = answer.Answer7;
                    partyMembersViewModel.Answer8 = answer.Answer8;
                    partyMembersViewModel.Answer9 = answer.Answer9;
                    partyMembersViewModel.Answer10 = answer.Answer10;
                }
                members.Add(partyMembersViewModel);
            }

            ViewBag.Members = members;
            ViewBag.PartyId = partyId;
            return PartialView("_PartyMembers", filteredModel);
        }

        [HttpPost]
        public ActionResult BlockMembers(PartyMembersFilterModel filter, int[] membersId, int partyId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var members = SelectMembers(filter, membersId, partyId);

                foreach (var member in members)
                {
                    member.IsBlocked = true;
                }

                PartyMembersRepository.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }

        [HttpPost]
        public ActionResult UnblockMembers(PartyMembersFilterModel filter, int[] membersId, int partyId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var members = SelectMembers(filter, membersId, partyId);
                foreach (var member in members)
                {
                    member.IsBlocked = false;
                }

                PartyMembersRepository.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }

        [HttpPost]
        public ActionResult InviteUsers(PartyMembersFilterModel filter, int[] membersId, int partyId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var members = SelectMembers(filter, membersId, partyId);
                foreach (var member in members)
                {
                    member.IsInvited = true;
                }

                PartyMembersRepository.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CancelUsersInvitation(PartyMembersFilterModel filter, int[] membersId, int partyId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var members = SelectMembers(filter, membersId, partyId);
                foreach (var member in members)
                {
                    member.IsInvited = false;
                }

                PartyMembersRepository.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }

        public ActionResult CountFilteredUsers(PartyMembersFilterModel filter, int partyId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                var membersQuantity = SelectMembers(filter, null, partyId).Count();
                return Json(new { success = true, count = membersQuantity });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        private IQueryable<PartyUsers> SelectMembers(PartyMembersFilterModel filter, int[] ids, int partyId)
        {
            IQueryable<PartyUsers> partyMembers;
            if (filter != null && filter.IsAllSelected)
            {
                var filteredModel = new PartyMembersFilteredModel(filter);
                partyMembers = PartyMembersRepository.All.Where(x => x.PartyId == partyId);
                if (ids != null)
                {
                    partyMembers = partyMembers.Where(x => !ids.Contains(x.UserId));
                }
                partyMembers = filteredModel.FilterData(partyMembers);
            }
            else
            {
                partyMembers = PartyMembersRepository.All.Where(x => ids.Contains(x.UserId));
            }

            return partyMembers;
        }

        [HttpPost]
        public ActionResult CreateExcelReport(PartyMembersFilterModel filter, int partyId)
        {
            try
            {
                var partyMembers = PartyMembersRepository.All.Where(x => x.PartyId == partyId);
                var filteredModel = new PartyMembersFilteredModel(filter);
                var requestedMembers = filteredModel.FilterData(partyMembers);

                var members = new List<PartyMembersViewModel>();
                foreach (var partyMember in requestedMembers)
                {
                    var answer = partyMember.Users.PartyQuestionnaireAnswer.FirstOrDefault(y => y.PartyQuestionnaire.PartyId == partyMember.PartyId);
                    var partyMembersViewModel = new PartyMembersViewModel
                    {
                        Id = partyMember.Id,
                        User = partyMember.Users,
                        IsBlocked = partyMember.IsBlocked,
                        IsInvited = partyMember.IsInvited,
                        Points = partyMember.Points,
                        HasCome = partyMember.HasCome
                    };
                    if (answer != null)
                    {
                        partyMembersViewModel.Answer1 = answer.Answer1;
                        partyMembersViewModel.Answer2 = answer.Answer2;
                        partyMembersViewModel.Answer3 = answer.Answer3;
                        partyMembersViewModel.Answer4 = answer.Answer4;
                        partyMembersViewModel.Answer5 = answer.Answer5;
                        partyMembersViewModel.Answer6 = answer.Answer6;
                        partyMembersViewModel.Answer7 = answer.Answer7;
                        partyMembersViewModel.Answer8 = answer.Answer8;
                        partyMembersViewModel.Answer9 = answer.Answer9;
                        partyMembersViewModel.Answer10 = answer.Answer10;
                    }
                    members.Add(partyMembersViewModel);
                }
                
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("Участники");
                    var headers = new List<string>
                    {
                        "Имя", "Фамилия", "Дата рождения", "Город", "Дата последнего посещения", "Номер телефона",
                        "Баллы", "Заблокирован", "Приглашение", "Пришли", "Вопрос 1", "Вопрос 2", "Вопрос 3","Вопрос 4",
                        "Вопрос 5", "Вопрос 6", "Вопрос 7", "Вопрос 8", "Вопрос 9", "Вопрос 10"
                    };
                    var headerRow = new List<string[]> {headers.ToArray()};
                    // Determine the header range (e.g. A1:D1)
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    // Target a worksheet
                    var worksheet = excel.Workbook.Worksheets["Участники"];

                    // Popular header row data
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    if (!Directory.Exists($"{FolderPath}/Reports/PartyReports"))
                        Directory.CreateDirectory($"{FolderPath}/Reports/PartyReports");
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[2, 1].LoadFromCollection(members.Select(m => new
                    {
                        m.User.FirstName,
                        m.User.LastName,
                        m.User.BirthDate,
                        m.User.Cities.CityName,
                        m.User.LastSignInP,
                        m.User.Phone,
                        m.Points,
                        m.IsBlocked,
                        m.IsInvited,
                        m.HasCome,
                        m.Answer1,
                        m.Answer2,
                        m.Answer3,
                        m.Answer4,
                        m.Answer5,
                        m.Answer6,
                        m.Answer7,
                        m.Answer8,
                        m.Answer9,
                        m.Answer10,
                    }));

                    worksheet.Cells.AutoFitColumns();
                    var dates = headers.Where(h => h.Contains("Дата"));
                    var columns = dates.Select(h => headers.IndexOf(h) + 1).ToList();
                    columns.ForEach(h => worksheet.Column(h).Style.Numberformat.Format = "dd-MM-yyyy HH:mm");

                    var fileId = Guid.NewGuid();
                    var path = $"{FolderPath}/Reports/PartyReports/{fileId}.xlsx";
                    var excelFile = new FileInfo(path);
                    excel.SaveAs(excelFile);
                    var urlPath = $"{FolderUrl}/Reports/PartyReports/{fileId}.xlsx";
                    return Json(new {success = true, url = urlPath});
                }
            }
            catch (Exception ex)
            {
                return Json(new {success = false, message = ex.Message});
            }
        }

        public ActionResult AddChatUsers(PartyMembersFilterModel filter, int partyId, int[] userIds, int chatGroupId)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, debug = "Not Ajax request" }, JsonRequestBehavior.AllowGet);

            try
            {
                var chatGroup = ChatGroupsRepository.Find(chatGroupId);
                if (chatGroup == null)
                {
                    return Json(new { success = false, message = $"Группы [{chatGroupId}] не существует" }, JsonRequestBehavior.AllowGet);
                }

                int userCounts = chatGroup.ChatPrivateGroupsUsers.Count;
                var users = SelectMembers(filter, userIds, partyId).Select(u => u.UserId).ToArray();
                var distinctUsers = users.Distinct();

                foreach (var id in distinctUsers)
                {
                    var chatPrivateGroudUser = chatGroup.ChatPrivateGroupsUsers.FirstOrDefault(x => x.UserId == id);
                    if (chatPrivateGroudUser == null)
                    {
                        var newChatUser = new ChatPrivateGroupsUsers();
                        newChatUser.ChatGroupId = chatGroupId;
                        newChatUser.UserId = id;

                        PrivateGroupsUsersRepository.InsertOrUpdate(newChatUser);
                        userCounts++;
                    }
                }

                PrivateGroupsUsersRepository.Save();

                return Json(new { success = true, data = userCounts }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, debug = e.Message });
            }
        }
    }

}