﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Common.Enums;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class FortuneProductsController : BaseController
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        private readonly IFortuneProductsRepository productsRepository;
        private readonly IPurchaseStatusesRepository purchaseStatusesRepository;
        private readonly IFortunePurchasesHistoryRepository purchasesHistoryRepository;

        public FortuneProductsController(IFortuneProductsRepository productsRepository,
            IPurchaseStatusesRepository purchaseStatusesRepository,
            IFortunePurchasesHistoryRepository purchasesHistoryRepository)
        {
            this.productsRepository = productsRepository;
            this.purchaseStatusesRepository = purchaseStatusesRepository;
            this.purchasesHistoryRepository = purchasesHistoryRepository;
        }

        public ActionResult Index(FortuneProductsFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var products = productsRepository.All;

            var filteredModel = new FortuneProductsFilteredModel(filter);
            filteredModel.ProcessData(products, "Id", true, pageNumber);
            ViewBag.DirectoryPath = FolderPath;
            ViewBag.PurchaseStatuses = GetPurchaseStatuses();

            return View(filteredModel);
        }

        [HttpPost]
        public ActionResult Edit(FortuneProductsEditModel item, HttpPostedFileBase image)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Error. Direct request" });
            try
            {
                if (ModelState.IsValid)
                {
                    var product = productsRepository.Find(item.Id);
                    if (product == null)
                    {
                        return Json(new
                        {
                            success = false,
                            message = Resources.ProductNotFound
                        });
                    }

                    product.Name = item.Name;
                    product.Cost = item.Cost;
                    product.Quantity = item.Quantity;
                    product.Type = item.Type;

                    if (image != null)
                    {
                        product.Image = ImageHelper.SaveImage(image, "Fortune/Products", product.Id.ToString());
                    }

                    productsRepository.InsertOrUpdate(product);
                    productsRepository.Save();

                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpGet]
        public ActionResult CreateProduct()
        {
            var model = new FortuneProductsEditModel { CreationDate = DateTime.Now, Type = (int)ProductType.Default};
            return PartialView("_CreateProduct", model);
        }

        [HttpPost]
        public ActionResult CreateProduct(FortuneProductsEditModel product, HttpPostedFileBase image)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Error. Direct request" });
            }

            try
            {
                if (ModelState.IsValid && image != null)
                {
                    var newProduct = new FortuneProducts
                    {
                        Name = product.Name,
                        Cost = product.Cost,
                        CreationDate = product.CreationDate,
                        Image = "",
                        Quantity = product.Quantity,
                        Type = product.Type
                    };
                    productsRepository.InsertOrUpdate(newProduct);
                    productsRepository.Save();

                    newProduct.Image = ImageHelper.SaveImage(image, "Fortune/Products", newProduct.Id.ToString());
                    productsRepository.Save();

                    return Json(new { success = true });
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DeleteProduct(int id)
        {
            productsRepository.Delete(id);
            productsRepository.Save();
            return Json(new { success = true });
        }

        private SelectList GetPurchaseStatuses()
        {
            return new SelectList(purchaseStatusesRepository.All
                .OrderBy(a => a.Id)
                .Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.Name })
                .ToList(), "Value", "Text");
        }

        [HttpPost]
        public ActionResult EditShopHistory(int id, int statusId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            }

            try
            {
                var history = purchasesHistoryRepository.Find(id);
                if (history == null)
                    return Json(new
                    {
                        success = false,
                        message = "Приз не найден"
                    });

                history.StatusId = statusId;
                history.AdministratorId = AuthenticationContext.CurrentUser.Id;


                purchasesHistoryRepository.Save();
                return Json(new
                {
                    success = true,
                    administratorName = AuthenticationContext.CurrentUser.LastName + " " + AuthenticationContext.CurrentUser.FirstName
                });


            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }
    }
}