﻿using StayTrueWebAdmin.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    public class ErrorController : BaseController
    {
        // GET: Error
        public ActionResult AccessDenied()
        {
            ViewBag.ErrorMessage = "Доступ к данной странице закрыт";
            return View("Index");
        }
    }
}