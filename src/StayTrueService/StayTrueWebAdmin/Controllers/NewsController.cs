﻿using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using StayTrueWebAdmin.Attributes;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Interfaces;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Models.EditModels;
using StayTrueWebAdmin.Models.FilterModels;
using WebGrease.Css.Extensions;

namespace StayTrueWebAdmin.Controllers
{
    [ActivityLog]
    [AdminAuthorize(Roles = "administrator, superadmin")]
    public class NewsController : BaseController
    {
        private static readonly string FolderPath = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
        public INewsRepository NewsRepository { get; set; }
        public INewsCategoryRepository NewsCategoryRepository { get; set; }
        public INewsTypeRepository NewsTypeRepository { get; set; }

        public NewsController(INewsRepository newsRepository, INewsCategoryRepository newsCategoryRepository, INewsTypeRepository newsTypeRepository)
        {
            NewsRepository = newsRepository;
            NewsCategoryRepository = newsCategoryRepository;
            NewsTypeRepository = newsTypeRepository;
        }

        public ActionResult Index(NewsFilterModel filter, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            var news = NewsRepository.All;

            var filteredModel = new NewsFilteredModel(filter);
            filteredModel.NewsCategories = GetCategories(null);
            filteredModel.ProcessData(news, "Id", true, pageNumber);
            filteredModel.News.ForEach(x =>
            {
                x.ImageM = $"{FolderPath}{x.ImageM}";
                x.ImageP = $"{FolderPath}{x.ImageP}";
                x.VideoM = $"{FolderPath}{x.VideoM}";
                x.VideoP = $"{FolderPath}{x.VideoP}";
            });
            ViewBag.NewsTypes = GetNewsTypes();

            return View(filteredModel);
        }

        public ActionResult CreateNews()
        {
            var model = new News {PublicationDate = DateTime.Now};
            ViewBag.NewsCategories = GetCategories(null);
            ViewBag.NewsTypes = GetNewsTypes();
            return PartialView("_CreateNews", model);
        }

        [HttpPost]
        public ActionResult Edit(NewsEditModel item, HttpPostedFileBase imageP,
            HttpPostedFileBase videoP, HttpPostedFileBase imageM, HttpPostedFileBase videoM)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false, message = "Запрос  напрямую. Вы че хакер?" });
            try
            {
                if (ModelState.IsValid)
                {
                    News model = NewsRepository.Find(item.Id);
                    if (model == null)
                        return Json(new {success = false, message = "Такой новости не существует"});

                    model.DescriptionKg = string.Empty;
                    model.NameKg = string.Empty;
                    model.NewsType = item.NewsType;
                    model.DescriptionRu = Regex.Replace(item.DescriptionRu, "<.*?[^>]+>|&nbsp;", String.Empty);
                    model.PublicationDate = item.PublicationDate;
                    model.IsActive = item.IsActive;
                    model.IsBrand = item.IsBrand;
                    model.IsDeleted = item.IsDeleted;
                    model.NameRu = item.NameRu;
                    model.NewsCategory = item.NewsCategory;

                    //item.DescriptionKg = Regex.Replace(item.DescriptionKg, "<.*?[^>]+>|&nbsp;", String.Empty);

                    if (imageP != null)
                    {
                        ImageHelper.DeleteImage(item.ImageP);
                        model.ImageP = ImageHelper.SaveImage(imageP, "News/Web",  item.Id.ToString());
                    }


                    if (videoP != null)
                    {
                        ImageHelper.DeleteImage(item.VideoP);
                        model.VideoP = VideoHelper.SaveVideo(videoP, "News/web", item.Id.ToString());
                    }


                    if (imageM != null)
                    {
                        ImageHelper.DeleteImage(item.ImageM);
                        model.ImageM = ImageHelper.SaveImage(imageM, "News/Mobile", item.Id.ToString());
                    }

                    if (videoM != null)
                    {
                        ImageHelper.DeleteImage(item.VideoM);
                        model.VideoM = VideoHelper.SaveVideo(videoM, "News/Mobile", item.Id.ToString());
                    }

                    NewsRepository.InsertOrUpdate(model);
                    NewsRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполнения программы",
                    debug = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult CreateNewsPost(News model, HttpPostedFileBase imageP,
            HttpPostedFileBase videoP, HttpPostedFileBase imageM, HttpPostedFileBase videoM)
        {
            if (!Request.IsAjaxRequest())
                return Json(new { success = false , message = "Запрос  напрямую. Вы че хакер?"});

            try
            {
                if (ModelState.IsValid)
                {

                    model.IsDeleted = false;
                    model.CreationDate = DateTime.Now;
                    
                    //model.IsActive = true;

                    model.DescriptionKg = model.DescriptionKg ?? string.Empty;
                    model.NameKg = model.NameKg ?? string.Empty;

                    model.DescriptionRu = Regex.Replace(model.DescriptionRu, "<.*?[^>]+>|&nbsp;", String.Empty);
                    model.DescriptionKg = Regex.Replace(model.DescriptionKg, "<.*?[^>]+>|&nbsp;", String.Empty);

                    NewsRepository.InsertOrUpdate(model);
                    NewsRepository.Save();

                    if (imageP != null)
                        model.ImageP = ImageHelper.SaveImage(imageP, "News/Web", model.Id.ToString());
                    if (videoP != null)
                    {
                        model.VideoP = VideoHelper.SaveVideo(videoP, "News/Web", model.Id.ToString());
                    }

                    if (imageM != null)
                        model.ImageM = ImageHelper.SaveImage(imageM, "News/Mobile", model.Id.ToString());
                    if (videoM != null)
                    {
                        model.VideoM = VideoHelper.SaveVideo(videoM, "News/Mobile", model.Id.ToString());
                    }


                    NewsRepository.Save();

                    return Json(new {success = true});
                }

                return Json(new
                {
                    success = false,
                    message = "Поля заполнены неверно!",
                    debug = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Ошибка выполения программы",
                    debug = ex.Message
                });
            }
        }

        public SelectList GetNewsTypes()
        {
            return new SelectList(NewsTypeRepository.All.Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name }).ToList(), "Value", "Text");
        }
        public SelectList GetCategories(int? selected)
        {
            return new SelectList(NewsCategoryRepository.All.Select(a => new SelectListItem() { Value = a.Id.ToString(), Text = a.Name }).ToList(), "Value", "Text", selected);
        }
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            var news = NewsRepository.All.FirstOrDefault(x => x.Id == id);

            if (news == null)
            {
                return Json(new { success = true });
            }
            NewsRepository.Delete(news.Id);
            NewsRepository.Save();
            return Json(new { success = true });
        }
    }
}