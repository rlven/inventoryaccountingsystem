﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Xml.Serialization;
using ExtendedXmlSerialization;
using Microsoft.SqlServer.Server;

namespace StayTrueWebAdmin.Helpers
{
    public class SmsService
    {
        public void UsersToXml(List<LoginPwds> toSend)
        {

            ExtendedXmlSerializer serializer = new ExtendedXmlSerializer();

            List<message> msgs = new List<message>();

            int i = 100000;

            foreach (var user in toSend)
            {
                i++;
                if (!State.wrongMessages.Contains("996" + user.number))
                {
                    msgs.Add(new message
                    {
                        id = user.id,

                        text = string.Format("{0}\nЛогин: {1}\nПароль: {2}\nПредназначено для определенного круга лиц.", user.txt, user.number, user.pwd)

               //         phones = new List<string> { "996" + user.number }
                    });
                }
            }

            foreach (var obj in msgs)
            {
                try
                {
                    var xml = serializer.Serialize(obj).Replace(" type=\"SmsService.message\"", "").Replace("string", "phone");
                    var responce = postXMLData(State.url_SendMessage, xml);
                    var obj2 = serializer.Deserialize<response>(responce);
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.UTF8.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }
    }
    static class State
    {
        public const string login = "irokez";

        public const string pwd = "ZsTib92o";

        public const string name = "StayTrue.kg";

        public const string url_SendMessage = "https://smspro.nikita.kg/api/message";

        public const string url_CheckMessage = "https://smspro.nikita.kg/api/dr";
        public static List<string> wrongMessages = new List<string>
        {
        };

        //        public static List<loginPwds> sending = new List<loginPwds>() {

        ////new loginPwds { id = "appppp_10015", number = "555947000", pwd="d6x57w"},

        //        };

    }
    public class LoginPwds
    {
        public string id;
        public string number;
        public string pwd;
        public string txt;
    }

    //Класс для отправки смс
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class message
    {
        private string loginField;

        private string pwdField;

        private string idField;

        private string senderField;

        private string textField;

        private string timeField;
        private string testField;

        private string[] phonesField;


        /// <remarks/>
        public string login
        {
            get
            {
                return this.loginField;
            }
            set
            {
                this.loginField = value;
            }
        }

        /// <remarks/>
        public string pwd
        {
            get
            {
                return this.pwdField;
            }
            set
            {
                this.pwdField = value;
            }
        }

        /// <remarks/>
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string sender
        {
            get
            {
                return this.senderField;
            }
            set
            {
                this.senderField = value;
            }
        }

        /// <remarks/>
        public string text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }

        /// <remarks/>
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        public string test
        {
            get { return this.testField; }
            set { this.testField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("phone", IsNullable = false)]
        public string[] phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

    }
    /// <summary>
    /// Ответ от сервера после отправки сообщений
    /// </summary>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://Giper.mobi/schema/Message")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://Giper.mobi/schema/Message", IsNullable = false)]
    public class response
    {
        private ulong idField;

        private byte statusField;

        private byte phonesField;

        private byte smscntField;

        /// <remarks/>
        public ulong id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public byte status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public byte phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        public byte smscnt
        {
            get
            {
                return this.smscntField;
            }
            set
            {
                this.smscntField = value;
            }
        }
    }

    public class info
    {
        [XmlElement("login", Order = 0)]
        public string login = State.login;
        [XmlElement("pwd", Order = 1)]
        public string pwd = State.pwd;
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://Giper.mobi/schema/Message")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://Giper.mobi/schema/Message", IsNullable = false)]
    public class infoResponse
    {
        [XmlElement("status")]
        public string status { get; set; }
        [XmlElement("state")]
        public string state { get; set; }
        [XmlElement("account")]
        public string account { get; set; }
        [XmlElement("smsprice")]
        public string smsprice { get; set; }
    }

    /// <summary>
    /// Запрос о доставке
    /// </summary>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://Giper.mobi/schema/DeliveryReport")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://Giper.mobi/schema/DeliveryReport", IsNullable = false)]
    public class dr
    {
        [XmlElement("login")]
        public string login { get; set; }

        [XmlElement("pwd")]
        public string pwd { get; set; }

        [XmlElement("id")]
        public string id { get; set; }
    }
    [XmlRoot("response")]
    class result
    {
        [XmlElement("status")]
        public string status;
        [XmlElement("phone")]
        public string phone;
    }
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://Giper.mobi/schema/DeliveryReport")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://Giper.mobi/schema/DeliveryReport", IsNullable = false)]
    public class ResponseReport
    {
        public string Status { get; set; }
        public string Number { get; set; }
        public string Report { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime RcvTime { get; set; }

    }
}