﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Logger;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using StayTrueWebAdmin.Models.EditModels;

namespace StayTrueWebAdmin.Helpers
{
    public class FileHelper
    {
        private static readonly string FolderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static readonly string MobileVersionPath = $"{ConfigurationManager.AppSettings["MobileVersionPath"]}"; 

        public static string UploadZip(HttpPostedFileBase file, string folderName, string fileName)
        {
            if (file == null)
            {
                return String.Empty;
            } 

            String outFolder = $"{FolderPath}/{folderName}/{fileName}/";

            UnzipFromStream(file.InputStream, outFolder);
            String[] dirs = Directory.GetDirectories(outFolder);
            string archiveName = "";

            if (dirs.Length > 0)
            {
                archiveName = new DirectoryInfo(dirs[0]).Name;
            }
            return $"/{folderName}/{fileName}/{archiveName}";
        }

        public static void UploadZip(HttpPostedFileBase file, string folderPath)
        {
            string outFolder = $"{folderPath}";

            UnzipFromStream(file.InputStream, outFolder);
            string[] dirs = Directory.GetDirectories(outFolder);
            string archiveName = "";

            if (dirs.Length > 0)
            {
                archiveName = new DirectoryInfo(dirs[0]).Name;
            }
        }

        public static string UploadZipParty(HttpPostedFileBase file, string folderName)
        {
            if (file == null)
            {
                return String.Empty;
            }

            String outFolder = $"{FolderPath}/{folderName}";

            UnzipFromStream(file.InputStream, outFolder);
            String[] dirs = Directory.GetDirectories(outFolder);
            string archiveName = "";

            if (dirs.Length > 0)
            {
                archiveName = new DirectoryInfo(dirs[0]).Name;
            }
            return $"/{folderName}/{archiveName}";
        }

        public static double GetDirectoryFileNameNumber()
        {
            DirectoryInfo d = new DirectoryInfo($"{MobileVersionPath}/Backups");//Assuming Test is your Folder
            DirectoryInfo[] Files = d.GetDirectories(); //Getting Text files
            if (Files.Length == 0)
                return 1;
            var lastFolderName = Files.OrderByDescending(x=>x.LastWriteTime).First().Name.Replace("v", string.Empty);
            lastFolderName = lastFolderName.Replace(".", ",");
            double parseNameToInt = double.Parse(lastFolderName);
            return parseNameToInt + 1;
        }

        public static void UnzipFromStream(Stream zipStream, string outFolder)
        {

            ZipInputStream zipInputStream = new ZipInputStream(zipStream);
            ZipEntry zipEntry = zipInputStream.GetNextEntry();
            String folderName = zipEntry.Name;

            while (zipEntry != null)
            {
                 String entryFileName = zipEntry.Name;
                // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                // Optionally match entrynames against a selection list here to skip as desired.
                // The unpacked length is available in the zipEntry.Size property.

                byte[] buffer = new byte[4096];     // 4K is optimum

                // Manipulate the output filename here as desired.
                String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    //  String fullZipToPath = outFolder;
                string directoryName = Path.GetDirectoryName(fullZipToPath);
                if (directoryName.Length > 0)
                    Directory.CreateDirectory(directoryName);

                // Skip directory entry
                string fileName = Path.GetFileName(fullZipToPath);
                if (fileName.Length == 0)
                {
                    zipEntry = zipInputStream.GetNextEntry();
                    continue;
                }

                // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                // of the file, but does not waste memory.
                // The "using" will close the stream even if an exception occurs.
                using (FileStream streamWriter = File.Create(fullZipToPath))
                {
                    StreamUtils.Copy(zipInputStream, streamWriter, buffer);
                }
                zipEntry = zipInputStream.GetNextEntry();
            }
            zipInputStream.Close();
        }

        public static void UploadFile(HttpPostedFileBase file)
        {
            var fileName = Path.GetFileName(file.FileName);

            var path = Path.Combine(MobileVersionPath, fileName);
            file.SaveAs(path);
        }

        public static void SaveOldVersion(string folderName, string apkFileName, string ipaFileName, MobileVersionEditModel model)
        {
            var path = $"{MobileVersionPath}/Backups/{folderName}";
            Directory.CreateDirectory(path);

            if (File.Exists($"{MobileVersionPath}/{apkFileName}") && model.AndroidBuild!=null)
                File.Move($"{MobileVersionPath}/{apkFileName}", $"{path}/{apkFileName}");

            if (File.Exists($"{MobileVersionPath}/{ipaFileName}") && model.IosBuild != null)
                File.Move($"{MobileVersionPath}/{ipaFileName}", $"{path}/{ipaFileName}");
        }

        public static void DeleteFolder(string folderName)
        {
            if(folderName == null)
                return;
            
            if (!Directory.Exists($"{FolderPath}/{folderName}"))
                return;

            DirectoryInfo di = new DirectoryInfo($"{FolderPath}/{folderName}");
            
            if (di.Exists)
            {
                di.Delete(true);
            }
        }
    }
}