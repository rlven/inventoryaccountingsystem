﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace StayTrueWebAdmin.Helpers
{
    public class OneSignalHelper
    {
        private readonly string appId = ConfigurationManager.AppSettings["OneSignalAppId"];
        private readonly string appKey = ConfigurationManager.AppSettings["OneSignalAppKey"];

        public NotificationResult CreateNotification(Dictionary<string, string> content, Dictionary<string, string> title,
            DateTime? sendTime, string[] segments = null, string[] playerIds = null, string url = null, int timeToLive = 259200)
        {
            var notification = new CreateNotificationModel
            {
                AppId = appId,
                Content = content,
                Title = title,
                TimeToLive = timeToLive,
                SendAfter = sendTime?.ToUniversalTime().ToString("U"),
                Url = url,
                IncludePlayerIds = playerIds,
                Segments = segments
            };
            if (segments == null && playerIds == null)
            {
                notification.Segments = new[] {"Subscribed Users"};
            }

            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("authorization", $"Basic {appKey}");
            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(notification));

            NotificationResult notificationResult = new NotificationResult();
            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return new NotificationResult { ResponseErrors = new[] {"Ошибка, невозможно сделать рассылку, введены неверные данные"} };
                    }
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var parse = reader.ReadToEnd();
                        dynamic result = JObject.Parse(parse);

                        notificationResult.Id = result.id;
                        notificationResult.Recipients = result.recipients;
                        if (result.errors?.invalid_player_ids != null || result.errors?.invalid_external_user_ids != null)
                        {
                            notificationResult.InvalidPlayers = ((JArray)result.errors?.invalid_player_ids)?.ToObject<string[]>();
                            notificationResult.InvalidExternalUsers = ((JArray)result.errors?.invalid_external_user_ids)?.ToObject<string[]>();
                        }
                        else if (result.errors != null)
                        {
                            notificationResult.ResponseErrors = ((JArray)result.errors).ToObject<string[]>();
                        }

                        notificationResult.ResponseStatusCode = response.StatusCode;
                    }
                }
            }
            catch (WebException ex)
            {
                var responseMessage = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                return new NotificationResult {ResponseErrors = new[]{ responseMessage, ex.Message} };
            }

            return notificationResult;
        }
    }

    public class CreateNotificationModel
    {
        [JsonProperty(PropertyName = "app_id")]
        public string AppId { get; set; }
        [JsonProperty(PropertyName = "contents")]
        public Dictionary<string, string> Content { get; set; }
        [JsonProperty(PropertyName = "headings")]
        public Dictionary<string, string> Title { get; set; }
        [JsonProperty(PropertyName = "ttl")]
        public int TimeToLive { get; set; }
        [JsonProperty(PropertyName = "included_segments", NullValueHandling = NullValueHandling.Ignore)]
        public string[] Segments { get; set; }
        [JsonProperty(PropertyName = "include_player_ids", NullValueHandling = NullValueHandling.Ignore)]
        public string[] IncludePlayerIds { get; set; }
        [JsonProperty(PropertyName = "send_after", NullValueHandling = NullValueHandling.Ignore)]
        public string SendAfter { get; set; }
        [JsonProperty(PropertyName = "url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }
    }

    public class NotificationResult
    {
        public HttpStatusCode ResponseStatusCode { get; set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "recipients")]
        public int? Recipients { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public string[] ResponseErrors { get; set; }
        [JsonProperty(PropertyName = "invalid_player_ids")]
        public string[] InvalidPlayers { get; set; }
        [JsonProperty(PropertyName = "invalid_external_user_ids")]
        public string[] InvalidExternalUsers { get; set; }
    }
}