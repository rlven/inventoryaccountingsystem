﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Repositories;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System.Net;

namespace StayTrueWebAdmin.Helpers
{
    public class AuthenticationContext
    {
        ///     The current user session key.
        /// </summary>
        private const string CurrentUserSessionKey = "StayTrueAuthenticationContext::CurrentUser";

        /// <summary>
        ///     The request context.
        /// </summary>
        private readonly HttpContextBase _contextBase;

        /// <summary>
        ///     The authentication wrapper.
        /// </summary>
        private readonly IAuthenticationWrapperHelper _authenticationWrapper;

        /// <summary>
        ///     The current user token.
        /// </summary>
        public AdminUserModel CurrentUser { get; private set; }

        /// <summary>
        ///     Creates an instance of the authentication helper
        /// </summary>
        public AuthenticationContext(HttpContextBase httpContext, IAuthenticationWrapperHelper authenticationWrapper, AdminUserModel user, string username)
        {
            _contextBase = httpContext;
            _authenticationWrapper = authenticationWrapper;

            // reading current user from session
            CurrentUser = !string.IsNullOrWhiteSpace(username) && user != null ? user : httpContext.Get<AdminUserModel>(CurrentUserSessionKey, null);
        }

        /// <summary>
        ///     Retrieves whether the user is logged on.
        /// </summary>
        public bool Authenticated
        {
            get { return CurrentUser != null; }
        }



        /// <summary>
        ///     Makes attempt to log on.
        /// </summary>
        public bool Authenticate(string username, string password, bool remember)
        {
            var administratorRepository = new AdministratorRepository();
            var chatTokensrepository = new ChatTokensRepository();
            var user = administratorRepository.Check(username, password);
            if (user != null)
            {
                string chatToken = Guid.NewGuid().ToString();
                long utcTime = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                ChatTokens token = new ChatTokens() { insert_date = utcTime, token = chatToken };
                chatTokensrepository.InsertOrUpdate(token);
                chatTokensrepository.Save();


                var userContext = new AdminUserModel()
                {
                    Id = user.Id,
                    Login = user.Login,
                    Email = user.Email,
                    Password = user.Password,
                    RoleId = user.Roles.Id,
                    RoleName = user.Roles.RoleName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    ChatToken = chatToken
                };

                user.LastSignIn = DateTime.Now;
                administratorRepository.Save();

                SaveAuthentication(userContext, remember);
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Logs the user out and removes authentication token.
        /// </summary>
        public void LogOut()
        {
            _authenticationWrapper.SignOut();
            CurrentUser = null;
            if (_contextBase.Session != null)
                _contextBase.Session[CurrentUserSessionKey] = null;
            HttpContext.Current.Response.Cookies.Clear();
        }


        /// <summary>
        ///     Defines whether the user is stored to session
        /// </summary>
        public static bool IsAuthorized(HttpContextBase httpContext)
        {
            var currentUser = httpContext.Get<AdminUserModel>(CurrentUserSessionKey, null);
            return UserIsAuthorized(currentUser);
        }

        public static bool IsInRole(HttpContextBase httpContext, string roleName)
        {
            var rolesRepository = new RolesRepository();
            var currentUser = httpContext.Get<AdminUserModel>(CurrentUserSessionKey, null);
            if (currentUser == null)
            {
                return false;
            }

            var roles = rolesRepository.All;

            if (roleName == null)
            {
                return false;
            }

            var currRoles = GetRoles(roleName);

            return UserIsInRole(currentUser, currRoles) && UserIsAuthorized(currentUser);
        }

        public static string GetUserRole(HttpContextBase httpContext)
        {
            var currUser = httpContext.Get<AdminUserModel>(CurrentUserSessionKey, null);

            if (currUser == null)
            {
                return String.Empty;
            }

            return currUser.RoleName;
        }

        /// <summary>
        ///     Checks if user is authorized.
        /// </summary>
        /// <returns></returns>
        public static bool UserIsAuthorized(AdminUserModel user)
        {
            return user != null;
        }

        private static bool UserIsInRole(AdminUserModel user, List<string> roles)
        {
            foreach (var role in roles)
            {
                if (user.RoleName.Trim().ToLower().Contains(role.Trim().ToLower()))
                {
                    return true;
                }
            }

            return false;
        }
        // implementation

        /// <summary>
        ///     Saves authentication.
        /// </summary>
        private void SaveAuthentication(AdminUserModel user, bool remember)
        {
            _authenticationWrapper.Authenticate(user.Login, remember);
            CurrentUser = user;
            if (_contextBase.Session != null) _contextBase.Session[CurrentUserSessionKey] = user;
            
        }

        private static List<string> GetRoles(string roles)
        {
            List<string> userRoles = new List<string>();
            userRoles = roles.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList();
            return userRoles;
        }

        public static AdminUserModel GetCurrentUser()
        {
            HttpContextBase httpContextBase = new HttpContextWrapper(System.Web.HttpContext.Current);
            return httpContextBase.Get<AdminUserModel>(CurrentUserSessionKey, null);
        }

        public static string GetIP(HttpRequestBase request)
        {
            if (request.Headers["CF-CONNECTING-IP"] != null)
                return request.Headers["CF-CONNECTING-IP"];

            var ipAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                var addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                    return addresses[0];
            }

            return request.UserHostAddress;

        }
    }
}