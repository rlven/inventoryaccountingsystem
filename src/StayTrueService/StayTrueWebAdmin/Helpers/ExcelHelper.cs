﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Helpers
{
    internal class ExcelHelper
    {
        internal virtual string CreateAndAndUploadExcelFile<T>(string sheetName, string apiUrl, string folderPath, IEnumerable<T> data, string reportId)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add($"{sheetName}");
                var headerRow = new List<string[]>()
                {
                    new string[] { "Дата и время рассылки", "Номер пользователя", "Текст сообщения", "Стоимость сообщения", "Статус сообщения" }
                };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets[$"{sheetName}"];

                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                if (!Directory.Exists($"{folderPath}/{sheetName}"))
                    Directory.CreateDirectory($"{folderPath}/{sheetName}");
                worksheet.Cells[headerRange].Style.Font.Bold = true;
                worksheet.Cells[headerRange].Style.Font.Size = 14;
                worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);
                worksheet.Cells["A1:O100"].AutoFitColumns();
                worksheet.Cells["A1:A100"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                worksheet.Cells["G1"].Value = "Статусы сообщения";
                worksheet.Cells["G2"].Value = "0-Сообщение находится в очереди на отправку";
                worksheet.Cells["G3"].Value = "1-Сообщение отправлено (передано оператору)";
                worksheet.Cells["G4"].Value = "2-Сообщение отклонено (не передано. Например, из-за несуществующего регионального кода)";
                worksheet.Cells["G5"].Value = "3-Сообщение успешно доставлено";
                worksheet.Cells["G6"].Value = "4-Сообщение не доставлено";
                worksheet.Cells["G7"].Value = "5-Сообщение не отправлено из-за нехватки средств на счету партнера";
                worksheet.Cells["G8"].Value = "6-Неизвестный (новый) статус отправки";
                worksheet.Cells["G9"].Value = "7-Истек период ожидания отчета о доставке от SMSC. Обработка сообщения прекращена";

        //        worksheet.Cells[2, 1].LoadFromCollection(data.Select(x => new { x.SendDate, x.Phone, x.Text, x.SmsCost, x.Report }));

        //        var path = $"{folderPath}/SmsReports/{reportId}.xlsx";

      //          FileInfo excelFile = new FileInfo(path);
       //         excel.SaveAs(excelFile);

                var uriPath = $"{apiUrl}/{sheetName}/{reportId}.xlsx";
                return uriPath;
            }
        }
    }
}