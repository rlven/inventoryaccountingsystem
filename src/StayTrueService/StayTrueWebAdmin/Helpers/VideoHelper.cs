﻿using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace StayTrueWebAdmin.Helpers
{
    public class VideoHelper
    {
        private static readonly string FolderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";


        public static string SaveVideo(HttpPostedFileBase video, string folderName, string fileName)
        {
            if (video == null || video.ContentLength <= 0) return null;

            var match = Regex.Match(video.FileName, "^(?<Name>.+)[.](?<Extension>.*)$");

            var filePath = $"{FolderPath}/Videos/{folderName}/{fileName}.{match.Groups["Extension"].Value}";
            if (!Directory.Exists($"{FolderPath}/Videos/{folderName}/"))
                Directory.CreateDirectory($"{FolderPath}/Videos/{folderName}/");

            video.SaveAs(filePath);
            var fileUrl = $"/Videos/{folderName}/{fileName}.{match.Groups["Extension"].Value}";
            return fileUrl;
        }
   
    }
}