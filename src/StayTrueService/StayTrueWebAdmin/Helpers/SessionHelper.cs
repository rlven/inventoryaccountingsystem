﻿using System.Web;

namespace StayTrueWebAdmin.Helpers
{
    public static class SessionHelper
    {
        /// <summary>
        ///     Retrieves variable from session or provides default value if it is null.
        /// </summary>
        public static T Get<T>(this HttpSessionStateBase session, string key, T @default)
        {
            object o = session[key];
            return o == null ? @default : (T)o;
        }

        /// <summary>
        ///     Retrieves variable from session or provides default value if it is null.
        /// </summary>
        public static T Get<T>(this HttpContextBase context, string key, T @default)
        {
            return context.Session.Get(key, @default);
        }
    }
}