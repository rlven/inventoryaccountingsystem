﻿using System.Web.Security;

namespace StayTrueWebAdmin.Helpers
{
    public class FormsAuthenticationWrapperHelper : IAuthenticationWrapperHelper
    {
        /// <summary>
        ///     Stores authentication token.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="remember"></param>
        public void Authenticate(string username, bool remember)
        {
            FormsAuthentication.SetAuthCookie(username, remember);
        }

        /// <summary>
        ///     Removes authentication token.
        /// </summary>
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}