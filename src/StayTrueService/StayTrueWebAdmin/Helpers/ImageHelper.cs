﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using GleamTech.VideoUltimate;

namespace StayTrueWebAdmin.Helpers
{
    public class ImageHelper
    {
        private static string folderPath = $"{ConfigurationManager.AppSettings["VirtualDirectoryPath"]}";
        private static string folderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];

        public static string ConvertToBase64(string filename)
        {
            try
            {
                if (String.IsNullOrEmpty(filename))
                {
                    return null;
                }

                string base64String = null;
                string ext = Path.GetExtension(filename);
                if (ext == null)
                    return null;

                using (Image image = Image.FromFile(Path.Combine(folderPath, filename)))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        Image newImage = ScalePassportImage(image, 700,700);
                        newImage.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        base64String = $"data:image/{ext.Replace(".", "")};base64," +
                                       Convert.ToBase64String(imageBytes);
                    }
                }

                return base64String;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SaveImage(HttpPostedFileBase file, string folderName, string fileName)
        {
            if (file == null || file.ContentLength <= 0) return null;

            var match = Regex.Match(file.FileName, "^(?<Name>.+)[.](?<Extension>.*)$");

            var filePath = $"{folderPath}/Images/{folderName}/{fileName}.{match.Groups["Extension"].Value}";
            if (!Directory.Exists($"{folderPath}/Images/{folderName}/"))
                Directory.CreateDirectory($"{folderPath}/Images/{folderName}/");
            
            file.SaveAs(filePath);
            
            var fileUrl = $"/Images/{folderName}/{fileName}.{match.Groups["Extension"].Value}";
            return fileUrl;
        }

        public static string SaveBannerImage(HttpPostedFileBase file, string folderName, string fileName)
        {
            if (file == null || file.ContentLength <= 0) return null;

            var match = Regex.Match(file.FileName, "^(?<Name>.+)[.](?<Extension>.*)$");

            var extension = match.Groups["Extension"].Value;

            var image = Image.FromStream(file.InputStream, true, true);

            var newImage = ScalePassportImage(image, 2000, 521);

            if (!Directory.Exists($"{folderPath}/{folderName}"))
                Directory.CreateDirectory($"{folderPath}/{folderName}");
            Bitmap clone = new Bitmap(newImage);
            newImage.Dispose();

            clone.Save($@"{folderPath}/{folderName}/{fileName}.{extension}");
            clone.Dispose();
            return $"{folderName}{fileName}.{extension}";
        }

        public static bool DeleteImage(string fileName)
        {
            if (String.IsNullOrEmpty(fileName)) return true;

            var filePath = fileName.Replace(folderUrl, "");

            filePath = $"{folderPath}{filePath}";

            if (File.Exists(filePath))
                File.Delete(filePath);

            return true;

        }


        public static string SaveSmallImage (string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return null;

            
            var image = Image.FromFile($@"{folderPath}/{fileName}");
            if (image.Equals(null)) return null;

            var newImage = ScaleImage(image);
            var filePath = $@"{folderPath}{fileName.Replace("Big", "Small")}";

            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            image.Dispose();

            Bitmap clone =  new Bitmap(newImage);
            newImage.Dispose();

            clone.Save(filePath);
            clone.Dispose();

            var fileUrl = fileName.Replace("Big", "Small");
            return fileUrl;
        }

        private static Image ScaleImage(Image image, int imageHeight = 360)
        {

            var newImage = new Bitmap(imageHeight * image.Width / image.Height, imageHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, imageHeight * image.Width / image.Height, imageHeight);
                graphics.Dispose();
            }
            
            return newImage;
        }

        private static Image ScalePassportImage(Image image, int imageHeight, int imageWidth)
        {

            var newImage = new Bitmap(imageHeight, imageWidth);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, imageHeight, imageWidth);
                graphics.Dispose();
            }

            return newImage;
        }

        public static string SaveSmallAvatar(int userId, string fileName)
        {
            var image = Image.FromFile($@"{folderPath}{fileName}");

            if (image.Equals(null)) return null;

            var newImage = ScaleImage(image, 150);

            var filePath = $@"{folderPath}{fileName.Replace("/NotApproved", "").Replace("Avatars", "SmallAvatars")}";

            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            image.Dispose();

            string smallImagePath = fileName.Replace("/NotApproved", "").Replace("Avatars", "SmallAvatars");

            Bitmap clone = new Bitmap(newImage);
            newImage.Dispose();

            clone.Save(filePath);
            clone.Dispose();


            return smallImagePath;
        }


        public static string SaveApprovedAvatar(int userId, string fileName)
        {
            var image = Image.FromFile($@"{folderPath}{fileName}");
            if (image.Equals(null)) return null;

            var filePath = $@"{folderPath}{fileName.Replace("/NotApproved", "")}";
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            string smallImagePath = fileName.Replace("/NotApproved", "");

            Bitmap clone = new Bitmap(image);
            clone.Save(filePath);
            clone.Dispose();
            image.Dispose();

            return smallImagePath;
        }


        public static string SaveFirstFrame(string videoPath, string folderName, string fileName)
        {
            string fileUrl = "";
            using (var videoFrameReader = new VideoFrameReader($@"{folderPath}{videoPath}"))
            {
                if (videoFrameReader.Read()) //Only if frame was read successfully
                {
                    //Get a System.Drawing.Bitmap for the current frame
                    //You are responsible for disposing the bitmap when you are finished with it.
                    //So it's good practice to have a "using" statement for the retrieved bitmap.
                    using (var frame = videoFrameReader.GetFrame())
                    {
                        if (!Directory.Exists($"{folderPath}/Images/{folderName}/"))
                            Directory.CreateDirectory($"{folderPath}/Images/{folderName}/");
                        fileUrl = $"/Images/{folderName}/{fileName}.jpg";
                        
                        //Reference System.Drawing and use System.Drawing.Imaging namespace for the following line.
                        frame.Save(
                            $@"{folderPath}{fileUrl}",
                            ImageFormat.Jpeg);
                    }
                }
            }

            return fileUrl;
        }

        public static string SaveScaledPngImage(HttpPostedFileBase file, String folderName, string fileName)
        {
            if (file == null || file.ContentLength <= 0) return null;

            var match = Regex.Match(file.FileName, "^(?<Name>.+)[.](?<Extension>.*)$");

            var filePath = $"{folderPath}/Images/{folderName}/{fileName}.png";
            if (!Directory.Exists($"{folderPath}/Images/{folderName}/"))
                Directory.CreateDirectory($"{folderPath}/Images/{folderName}/");
            var scaled = ScaleImage(Image.FromStream(file.InputStream, true, true));
            scaled.Save(filePath);

            var fileUrl = $"/Images/{folderName}/{fileName}.png";
            return fileUrl;
        }
    }
}