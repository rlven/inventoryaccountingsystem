﻿
namespace StayTrueWebAdmin.Helpers
{
    public interface IAuthenticationWrapperHelper
    {
        /// <summary>
        ///     Store authetnication token.
        /// </summary>
        void Authenticate(string username, bool remember);

        /// <summary>
        ///     Remove authentication token.
        /// </summary>
        void SignOut();
    }
}