using Common.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace StayTrueWebAdmin.Helpers
{
    public static class PaymentServiceHelper
    {
        private static readonly string login = ConfigurationManager.AppSettings["PaymentServiceLogin"];

        private static readonly string password = ConfigurationManager.AppSettings["PaymentServicePassword"];

        private static readonly string addres = ConfigurationManager.AppSettings["PaymentServiceUrl"];


        private static void DisableCertificateValidation()
        {

            ServicePointManager.ServerCertificateValidationCallback =
                delegate (
                    object s,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors
                ) {
                    return true;
                };

        }


        public static string GetPaymentReport(DateTime dateFrom, DateTime dateTo)
        {
            string dateFromStr = dateFrom.ToString("yyyy-MM-dd");
            string dateToStr = dateTo.ToString("yyyy-MM-dd");
            var url = addres + $"getreportsdata?login={login}&password={password}&reportID=1&dateFrom={dateFromStr}&dateTo={dateToStr}";
            try
            {
                DisableCertificateValidation();
                var result = SendRequest(url);
                result = result.Replace("&lt;", "<");
                result = result.Replace("&gt;", ">");
                result = result.Replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">",
                    string.Empty);
                result = result.Replace("</string>", string.Empty);

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return string.Empty;
            }
        }

        private static string SendRequest(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "GET";

            var response = (HttpWebResponse)request.GetResponse();

            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }

    }
}