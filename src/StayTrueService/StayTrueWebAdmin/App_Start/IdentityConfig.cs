﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer.Utilities;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using StayTrueWebAdmin.Models.Identity;

namespace StayTrueWebAdmin
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        //public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        //{
        //    var bll = new BusinessLogicProvider();
        //    var result = bll.AuthorizationAdmin(new AdminAccount{Login = userName, Password = password});
        //    if (result.Equals("OK"))
        //    {
        //        return await SignInOrTwoFactor(new ApplicationUser{Id = "1", Email = userName}, isPersistent).WithCurrentCulture();
        //    }
        //    return SignInStatus.Failure;
        //}

        //private async Task<SignInStatus> SignInOrTwoFactor(ApplicationUser user, bool isPersistent)
        //{
        //    SignInStatus signInStatu;
        //    string str = Convert.ToString(user.Id);
        //    Task<bool> cultureAwaiter = this.UserManager.GetTwoFactorEnabledAsync(user.Id);
        //    if (await cultureAwaiter)
        //    {
        //        Task<IList<string>> providerAwaiter = this.UserManager.GetValidTwoFactorProvidersAsync(user.Id);
        //        IList<string> listProviders = await providerAwaiter;
        //        if (listProviders.Count > 0)
        //        {
        //            Task<bool> cultureAwaiter2 = this.AuthenticationManager.TwoFactorBrowserRememberedAsync(str);
        //            if (!await cultureAwaiter2)
        //            {
        //                ClaimsIdentity claimsIdentity = new ClaimsIdentity("TwoFactorCookie");
        //                claimsIdentity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", str));
        //                this.AuthenticationManager.SignIn(claimsIdentity);
        //                signInStatu = SignInStatus.RequiresVerification;
        //                return signInStatu;
        //            }
        //        }
        //    }
        //    Task cultureAwaiter3 = SignInAsync(user, isPersistent, false);
        //    await cultureAwaiter3;
        //    signInStatu = SignInStatus.Success;
        //    return signInStatu;
        //}

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
