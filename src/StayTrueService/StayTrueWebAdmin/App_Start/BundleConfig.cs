﻿using System;
using System.Web.Optimization;

namespace StayTrueWebAdmin
{
    public class BundleConfig
    {
        private static string updateDate = string.Empty;

        public static string Version
        {
            get
            {
                if (string.IsNullOrEmpty(updateDate))
                    updateDate = DateTime.Now.ToString("ddMMyyyyHHmm");
                return updateDate;
            }
        }

        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            // Default Script bundle

            bundles.Add(new ScriptBundle($"~/bundles/js/jquery-{Version}").Include(
                $"~/Scripts/jquery/jquery-3.3.1.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/jquery-ui-{Version}",
                "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js").Include(
                "~/Scripts/jquery/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/jquery-datetimepicker-{Version}").Include(
                "~/Scripts/jquery/jquery-ui-sliderAccess.js",
                "~/Scripts/jquery/jquery-ui-timepicker-addon.js",
                "~/Scripts/jquery/jquery-ui-timepicker-ru.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/jquery-datetimepicker-{Version}",
                "https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.js").Include(
                "~/Scripts/jquery/jquery-ui-timepicker-addon.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/jquery-datetimepicker-lang-{Version}",
                 "https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/i18n/jquery-ui-timepicker-addon-i18n.js").Include(
                 "~/Scripts/jquery/jquery-ui-timepicker-addon-i18n.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/jquery-datetimepicker-default-{Version}",
                "https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/i18n/jquery-ui-timepicker-ru.js").Include(
                "~/Scripts/jquery/jquery-ui-timepicker-ru.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/js/bootstrap").Include(
                "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/common-{Version}").Include(
                "~/Scripts/js/common.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/loader-{Version}",
                "https://www.gstatic.com/charts/loader.js").Include(
                "~/Scripts/js/loader.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/user-page-{Version}").Include(
                "~/Scripts/logic/users-multiple-select.js",
                "~/Scripts/logic/user-page.js",
                "~/Scripts/logic/sms-page.js"));

            //Default Styles bundle

            bundles.Add(new StyleBundle($"~/bundles/css/jquery-ui-{Version}",
                "//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css").Include(
                "~/Content/css/jquery-ui.css"));

            bundles.Add(new StyleBundle($"~/bundles/css-{Version}").Include(
                "~/Content/bootstrap.css",
                "~/Content/css/style.css"));

            bundles.Add(new StyleBundle("~/bundles/css/fonts",
                "https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic-ext").Include(
                "~/Content/css/Roboto.css"));

            bundles.Add(new StyleBundle("~/bundles/css/fonts",
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css").Include(
                "~/Content/css/font-awesome.me.css"));

            //Pages scripts bundle
            bundles.Add(new ScriptBundle($"~/bundles/js/login-page-{Version}").Include(
                "~/Scripts/logic/login-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/news-page-{Version}").Include(
                "~/Scripts/logic/news-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/payment-report-{Version}").Include(
                "~/Scripts/logic/payment-report.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/game-page-{Version}").Include(
                "~/Scripts/logic/game-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/game-page-pvp-{Version}").Include(
                "~/Scripts/logic/game-page-pvp.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/quiz-page-{Version}").Include(
                "~/Scripts/logic/quiz-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/questionnaire-page-{Version}").Include(
                "~/Scripts/logic/questionnaire-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/questionnaire-report-page-{Version}").Include(
                "~/Scripts/logic/questionnaire-report-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/gallery-page-{Version}").Include(
                "~/Scripts/logic/gallery-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/shop-page-{Version}").Include(
                "~/Scripts/logic/shop-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/brand-page-{Version}").Include(
                "~/Scripts/logic/brand-page.js",
                "~/Scripts/logic/news-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/great-battle-page-{Version}").Include(
                "~/Scripts/logic/great-battle-page.js",
                "~/Scripts/logic/great-battle-create-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/great-battle-members-page-{Version}").Include(
                "~/Scripts/logic/questionnaire-report-page.js",
                "~/Scripts/logic/great-battle-members-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/party-page-{Version}").Include(
                "~/Scripts/logic/party-create-page.js",
                "~/Scripts/logic/party-create.js"));


            bundles.Add(new ScriptBundle($"~/bundles/js/party-members-page-{Version}").Include(
                "~/Scripts/logic/users-multiple-select.js",
                "~/Scripts/logic/party-members-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/sms-page-{Version}").Include(
                "~/Scripts/logic/sms-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/sms-template-page-{Version}").Include(
                "~/Scripts/logic/smsTemplate-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/push-templates-page-{Version}").Include(
                "~/Scripts/logic/push-templates-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/push-page-{Version}").Include(
                "~/Scripts/logic/push-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/push-report-page-{Version}").Include(
                "~/Scripts/logic/push-report-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/support-page-{Version}").Include(
                "~/Scripts/logic/support-page.js",
                "~/Scripts/logic/support-reset-pass.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/administrators-page-{Version}").Include(
                "~/Scripts/logic/administrators-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/quizz-answer-{Version}").Include(
                "~/Scripts/logic/quizz-answer.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/lottery-page-{Version}").Include(
                "~/Scripts/logic/lottery-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/mobileversion-page-{Version}").Include(
                "~/Scripts/logic/mobileversion-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/fortune-products-page-{Version}").Include(
                "~/Scripts/logic/fortune-products-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/banner-page-{Version}").Include(
                "~/Scripts/logic/banner-page.js"));

            bundles.Add(new ScriptBundle($"~/bundles/js/fortune-wheel-page-{Version}").Include(
                "~/Scripts/logic/fortune-wheel-page.js"));
        }
    }
}
