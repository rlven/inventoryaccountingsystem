﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IAlbumImagesRepository : IRepository<AlbumImages>
    {
    }
}
