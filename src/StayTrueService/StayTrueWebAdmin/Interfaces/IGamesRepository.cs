﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGamesRepository : IRepository<Games>
    {

    }
}