﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IPurchaseStatusesRepository : IRepository<PurchaseStatuses>
    {
    }
}