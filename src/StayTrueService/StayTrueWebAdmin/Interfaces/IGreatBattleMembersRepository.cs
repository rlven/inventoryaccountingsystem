﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models;
using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGreatBattleMembersRepository : IRepository<GreatBattlesUsers>
    {
    }
}
