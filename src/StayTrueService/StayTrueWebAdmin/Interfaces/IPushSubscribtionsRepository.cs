﻿using System.Collections.Generic;
using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IPushSubscribtionsRepository : IRepository<PushSubscribtions>
    {
        void DeleteRange(List<PushSubscribtions> subscribtions);
    }
}
