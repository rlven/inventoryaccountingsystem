﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IQuestionnaireAnswersRepository : IRepository<QuestionnaireAnswers>
    {

    }
}