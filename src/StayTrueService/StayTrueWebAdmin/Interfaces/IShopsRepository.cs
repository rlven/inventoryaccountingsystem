﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IShopsRepository : IRepository<Shops>
    {
    }
}