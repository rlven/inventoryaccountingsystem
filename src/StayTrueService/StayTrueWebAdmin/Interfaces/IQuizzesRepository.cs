﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IQuizzesRepository : IRepository<Quizzes>
    {

    }
}