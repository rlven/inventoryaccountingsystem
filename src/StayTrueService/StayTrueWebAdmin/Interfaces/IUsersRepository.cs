﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IUsersRepository : IRepository<Users>
    {

    }
}