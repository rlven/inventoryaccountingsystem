﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGendersRepository : IRepository<Genders>
    {
    }
}