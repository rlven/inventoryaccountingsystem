﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IFortunePurchasesHistoryRepository : IRepository<FortunePurchasesHistory>
    {
    }
}
