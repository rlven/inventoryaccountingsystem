﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGreatBattlesRepository : IRepository<GreatBattles>
    {
    }
}