﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGreatBattleToursRepository : IRepository<GreatBattleTours>
    {
    }
}