﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IAdministratorRepository : IRepository<Administrators>
    {
        bool AnyExist();
        Administrators Check(string login, string password);
        string GetHash(string password);
    }
}