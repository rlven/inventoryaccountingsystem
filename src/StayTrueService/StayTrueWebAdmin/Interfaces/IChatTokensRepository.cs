﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IChatTokensRepository : IRepository<ChatTokens>
    {
    }
}