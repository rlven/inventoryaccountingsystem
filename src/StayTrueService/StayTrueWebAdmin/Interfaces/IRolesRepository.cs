﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IRolesRepository : IRepository<Roles>
    { 
    }
}