﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGreatBattleQuestionnaireRepository : IRepository<GreatBattleQuestionnaire>
    {
    }
}