﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IChatPrivateGroupsUsersRepository : IRepository<ChatPrivateGroupsUsers>
    {

    }
}