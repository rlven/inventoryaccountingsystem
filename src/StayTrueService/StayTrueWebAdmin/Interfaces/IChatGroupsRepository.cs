﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IChatGroupsRepository : IRepository<ChatGroups>
    {

    }
}