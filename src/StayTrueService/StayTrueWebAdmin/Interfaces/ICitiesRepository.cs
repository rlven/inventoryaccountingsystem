﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface ICitiesRepository : IRepository<Cities>
    {
    }
}