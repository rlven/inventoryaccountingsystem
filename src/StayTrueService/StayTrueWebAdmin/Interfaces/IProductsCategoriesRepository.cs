﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IProductsCategoriesRepository : IRepository<ProductsCategories>
    {
    }
}