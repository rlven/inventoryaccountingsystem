﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IPushNotificationsRepository : IRepository<PushNotifications>
    {
    }
}
