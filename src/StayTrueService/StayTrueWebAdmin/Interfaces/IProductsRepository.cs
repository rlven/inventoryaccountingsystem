﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IProductsRepository : IRepository<Products>
    {
    }
}