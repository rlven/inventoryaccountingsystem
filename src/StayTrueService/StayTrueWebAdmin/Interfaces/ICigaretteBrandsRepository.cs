﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface ICigaretteBrandsRepository : IRepository<CigaretteBrands>
    {
    }
}
