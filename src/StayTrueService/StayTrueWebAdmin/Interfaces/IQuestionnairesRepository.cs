﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IQuestionnairesRepository : IRepository<Questionnaires>
    {

    }
}