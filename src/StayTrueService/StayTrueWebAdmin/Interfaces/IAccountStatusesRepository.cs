﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IAccountStatusesRepository : IRepository<AccountStatuses>
    {
      
    }
}