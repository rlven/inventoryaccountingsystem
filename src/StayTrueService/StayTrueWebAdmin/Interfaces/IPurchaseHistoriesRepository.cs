﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IPurchaseHistoriesRepository : IRepository<PurchaseHistories>
    {
    }
}