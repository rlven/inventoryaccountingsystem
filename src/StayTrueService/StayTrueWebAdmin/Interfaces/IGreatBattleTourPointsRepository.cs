﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGreatBattleTourPointsRepository : IRepository<GreatBattleTourPoints>
    {
    }
}
