﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface INewsRepository : IRepository<News>
    {

    }
}