﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IPushTemplatesRepository : IRepository<PushTemplates>
    {
    }
}
