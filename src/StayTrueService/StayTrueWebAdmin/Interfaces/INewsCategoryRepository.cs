﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface INewsCategoryRepository : IRepository<NewsCategories>
    {
    }
}