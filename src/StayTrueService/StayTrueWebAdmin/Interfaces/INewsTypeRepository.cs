﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface INewsTypeRepository : IRepository<NewsTypes>
    {
    }
}