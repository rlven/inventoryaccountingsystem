﻿using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IBlockUsersRepository : IRepository<BlockedUsers>
    {

    }
}