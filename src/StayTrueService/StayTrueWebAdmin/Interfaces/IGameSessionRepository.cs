﻿using System.Collections.Generic;
using DataAccessLayer;

namespace StayTrueWebAdmin.Interfaces
{
    public interface IGameSessionRepository : IRepository<GameSession>
    {
        ICollection<GameSession> GetByUserId(int userId);
    }
}