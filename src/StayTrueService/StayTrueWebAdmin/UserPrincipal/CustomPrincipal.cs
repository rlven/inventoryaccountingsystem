﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Security.Principal;
using System.Web;
using DataAccessLayer;
using Microsoft.AspNet.Identity;
using StayTrueWebAdmin.Controllers;
using StayTrueWebAdmin.Helpers;
using StayTrueWebAdmin.Models;
using StayTrueWebAdmin.Repositories;

namespace StayTrueWebAdmin.UserPrincipal
{
    public class CustomPrincipal :  ICustomPrincipal
    {
        private const string CurrentUserSessionKey = "StayTrueAuthenticationContext::CurrentUser";

        public IIdentity Identity { get; private set; }


        public bool IsInRole(string role)
        {
            HttpContextBase context = new HttpContextWrapper(HttpContext.Current);
            var currentUser = context.Get<AdminUserModel>(CurrentUserSessionKey, null);
                        
            return currentUser.RoleName.Trim().ToLower().Contains(role)? true: false;
        }

        public CustomPrincipal()
        {
            HttpContextBase context = new HttpContextWrapper(HttpContext.Current);
            var currentUser = context.Get<AdminUserModel>(CurrentUserSessionKey, null);

            this.Identity = new GenericIdentity(currentUser.Login);
            Id = currentUser.Id;
            FirstName = currentUser.FirstName;
            LastName = currentUser.LastName;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public interface ICustomPrincipal
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}