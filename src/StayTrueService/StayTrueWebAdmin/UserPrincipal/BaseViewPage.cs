﻿using System;
using System.Web;
using System.Web.Mvc;


namespace StayTrueWebAdmin.UserPrincipal
{
    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new CustomPrincipal User
        {
            get
            {
                return new CustomPrincipal();
            }
        }
    }
}