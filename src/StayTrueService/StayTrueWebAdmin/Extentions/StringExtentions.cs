﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Extentions
{
    public static class StringExtentions
    {
        public static string TrimSpaces(this string str)
        {
            if (!string.IsNullOrEmpty(str))
                str = str.Trim();
            return str;
        }

        public static string StripHtml(this string html, bool allowHarmlessTags)
        {
            if (string.IsNullOrEmpty(html))
                return string.Empty;

            if (allowHarmlessTags)
                return System.Text.RegularExpressions.Regex.Replace(html, "", string.Empty);

            return System.Text.RegularExpressions.Regex.Replace(html, "<[^>]*>", string.Empty);
        }
    }
}