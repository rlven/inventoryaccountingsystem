﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Extentions
{
    public static class DateTimeExtensions
    {
        public static string DateFormatToString(this DateTime? date)
        {
            return date.HasValue? date.Value.ToString(@"dd/MM/yyyy", CultureInfo.InvariantCulture) : "";
        }

        public static string DateFormatToString(this DateTime date)
        {
            return date.ToString(@"dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public static string ToShortDateString(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("yyyy-MM-dd") : "";
            //return date.HasValue ? date.Value.ToShortDateString() : "";
        }

        public static string ToShortDateTimeString(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToShortDateTimeString() : "";
            //return date.HasValue ? date.Value.ToShortDateString() : "";
        }

        public static string ToShortDateTimeString(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy HH:mm");
            //return date.HasValue ? date.Value.ToShortDateString() : "";
        }
    }
}