﻿using StayTrueWebAdmin.Helpers;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Attributes
{
    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return AuthenticationContext.IsInRole(httpContext, Roles);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            filterContext.Result = Redirect(filterContext);
        }

        private ActionResult Redirect(AuthorizationContext filterContext)
        {
            var urlHelper = new UrlHelper(filterContext.RequestContext);
            HttpContextBase httpContext = new HttpContextWrapper(HttpContext.Current);
            var currUserRole = AuthenticationContext.GetUserRole(httpContext);
            switch (currUserRole.ToLower())
            {
                case "administrator":
                    return new RedirectResult(urlHelper.Action("Index", "Home"));
                case "moderator":
                    return new RedirectResult(urlHelper.Action("Index", "Chat"));
                case "retailer":
                    return new RedirectResult(urlHelper.Action("Index", "Shop"));
                default:
                    return new RedirectResult(urlHelper.Action("Login", "Account"));
            }
        }
    }
}