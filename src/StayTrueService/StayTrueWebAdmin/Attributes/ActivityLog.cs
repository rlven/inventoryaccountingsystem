﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Common.Logger;
using StayTrueWebAdmin.Helpers;

namespace StayTrueWebAdmin.Attributes
{
    public class ActivityLog : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            RouteData route_data = filterContext.RouteData;
            string controller = (string)route_data.Values["controller"];
            string action = (string)route_data.Values["action"];


            var currUser = AuthenticationContext.GetCurrentUser();
         //   HttpRequestBase request = new HttpRequestWrapper(HttpContext.Current.Request);
       //     var ipAddres = AuthenticationContext.GetIP(request);

            var ipAddres = HttpContext.Current.Request.UserHostAddress;
        //    var upAddressTest2 = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            string logData = string.Empty;
            logData += currUser.Id;
            logData += $" Login: {currUser.Login}";
            logData += $" IpAddress: {ipAddres}";
            logData += $" Controller: {controller} Action: {action}";
            Log.Info(logData);

            //Save all your required values, including user id and whatnot here.
            //The duration variable will allow you to see expensive page loads on the controller, this can be useful when clients complain about something being slow.
        }
    }
}