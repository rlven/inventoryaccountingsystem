﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class FortuneWheelRepository : IFortuneWheelRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<FortuneWheel> All => context.FortuneWheel.Where(w => !w.IsDeleted);
        public IQueryable<FortuneWheel> AllIncluding(params Expression<Func<FortuneWheel, object>>[] includeProperties)
        {
            IQueryable<FortuneWheel> query = context.FortuneWheel;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public FortuneWheel Find(int id)
        {
            return context.FortuneWheel.Find(id);
        }

        public void InsertOrUpdate(FortuneWheel entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.FortuneWheel.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var wheel = context.FortuneWheel.Find(id);
            if (wheel != null)
            {
                wheel.IsDeleted = true;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}