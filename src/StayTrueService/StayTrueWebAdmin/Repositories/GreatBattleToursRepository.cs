﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GreatBattleToursRepository : IGreatBattleToursRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<GreatBattleTours> All => _context.GreatBattleTours;

        public IQueryable<GreatBattleTours> AllIncluding(params Expression<Func<GreatBattleTours, object>>[] includeProperties)
        {
            IQueryable<GreatBattleTours> query = _context.GreatBattleTours;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public GreatBattleTours Find(int id)
        {
            return _context.GreatBattleTours.Find(id);
        }

        public void InsertOrUpdate(GreatBattleTours entity)
        {
            if (entity.ID == default(int))
            {
                _context.GreatBattleTours.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.GreatBattleTours.Find(id);
            if (entity != null)
            {
                _context.GreatBattleTours.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}