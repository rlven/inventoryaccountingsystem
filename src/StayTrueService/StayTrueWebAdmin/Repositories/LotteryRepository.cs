﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class LotteryRepository : ILotteryRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public IQueryable<Lottery> All => context.Lottery;

        public IQueryable<Lottery> AllIncluding(params Expression<Func<Lottery, object>>[] includeProperties)
        {
            IQueryable<Lottery> query = context.Lottery;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Lottery Find(int id)
        {
            return context.Lottery.Find(id);
        }

        public void InsertOrUpdate(Lottery entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Lottery.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Lottery.Find(id);
            
            context.Lottery.Remove(entity);
        }

        public bool AnyExist()
        {
            return context.Lottery.Any();
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}