﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class BannersRepository : IBannersRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<Banners> All => context.Banners;
        public IQueryable<Banners> AllIncluding(params Expression<Func<Banners, object>>[] includeProperties)
        {
            IQueryable<Banners> query = context.Banners;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Banners Find(int id)
        {
            return context.Banners.Find(id);
        }

        public void InsertOrUpdate(Banners entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Banners.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var banner = context.Banners.Find(id);
            if (banner != null)
            {
                context.Banners.Remove(banner);                
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}