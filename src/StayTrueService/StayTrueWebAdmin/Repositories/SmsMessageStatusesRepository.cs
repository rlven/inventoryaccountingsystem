﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class SmsMessageStatusesRepository : ISmsStatusesRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public IQueryable<SmsMessageStatuses> All => _context.SmsMessageStatuses;

        public IQueryable<SmsMessageStatuses> AllIncluding(params Expression<Func<SmsMessageStatuses, object>>[] includeProperties)
        {
            IQueryable<SmsMessageStatuses> query = _context.SmsMessageStatuses;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            var entity = _context.SmsMessageStatuses.Find(id);
            if (entity != null)
            {
                _context.SmsMessageStatuses.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public SmsMessageStatuses Find(int id)
        {
            return _context.SmsMessageStatuses.Find(id);
        }

        public void InsertOrUpdate(SmsMessageStatuses entity)
        {
            if (entity.Id == default(int))
            {
                _context.SmsMessageStatuses.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}