﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class SmsMessageRepository : ISmsMessageRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public IQueryable<SmsMessage> All => _context.SmsMessage;

        public IQueryable<SmsMessage> AllIncluding(params Expression<Func<SmsMessage, object>>[] includeProperties)
        {
            IQueryable<SmsMessage> query = _context.SmsMessage;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            var entity = _context.SmsMessage.Find(id);
            if (entity != null)
            {
                _context.SmsMessage.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public SmsMessage Find(int id)
        {
            return _context.SmsMessage.Find(id);
        }

        public void InsertOrUpdate(SmsMessage entity)
        {
            if (entity.Id == default(int))
            {
                _context.SmsMessage.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}