﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class QuizzSessionAnswerRepository : IQuizzSessionAnswerRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public IQueryable<QuizzSessionAnswers> All => context.QuizzSessionAnswers;

        public IQueryable<QuizzSessionAnswers> AllIncluding(params Expression<Func<QuizzSessionAnswers, object>>[] includeProperties)
        {
            IQueryable<QuizzSessionAnswers> query = context.QuizzSessionAnswers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public QuizzSessionAnswers Find(int id)
        {
            return context.QuizzSessionAnswers.Find(id);
        }

        public void InsertOrUpdate(QuizzSessionAnswers entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.QuizzSessionAnswers.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.QuizzSessionAnswers.Find(id);
            context.QuizzSessionAnswers.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}