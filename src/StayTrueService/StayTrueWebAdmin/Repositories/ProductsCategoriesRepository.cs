﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class ProductsCategoriesRepository : IProductsCategoriesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<ProductsCategories> All => context.ProductsCategories;

        public IQueryable<ProductsCategories> AllIncluding(params Expression<Func<ProductsCategories, object>>[] includeProperties)
        {
            IQueryable<ProductsCategories> query = context.ProductsCategories;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public ProductsCategories Find(int id)
        {
            return context.ProductsCategories.Find(id);
        }

        public void InsertOrUpdate(ProductsCategories entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.ProductsCategories.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.ProductsCategories.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}