﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class QuestionnaireAnswersRepository : IQuestionnaireAnswersRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<QuestionnaireAnswers> All => _context.QuestionnaireAnswers;

        public IQueryable<QuestionnaireAnswers> AllIncluding(params Expression<Func<QuestionnaireAnswers, object>>[] includeProperties)
        {
            IQueryable<QuestionnaireAnswers> query = _context.QuestionnaireAnswers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public QuestionnaireAnswers Find(int id)
        {
            return _context.QuestionnaireAnswers.Find(id);
        }

        public void InsertOrUpdate(QuestionnaireAnswers entity)
        {
            if (entity.Id == default(int))
            {
                _context.QuestionnaireAnswers.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.QuestionnaireAnswers.Find(id);
            _context.QuestionnaireAnswers.Remove(entity);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}