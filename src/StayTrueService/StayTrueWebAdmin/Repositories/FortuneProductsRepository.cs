﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System.Data.Entity;

namespace StayTrueWebAdmin.Repositories
{
    public class FortuneProductsRepository : IFortuneProductsRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<FortuneProducts> All => context.FortuneProducts.Where(p => !p.IsDeleted);
        public IQueryable<FortuneProducts> AllIncluding(params Expression<Func<FortuneProducts, object>>[] includeProperties)
        {
            IQueryable<FortuneProducts> query = context.FortuneProducts;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public FortuneProducts Find(int id)
        {
            return context.FortuneProducts.Find(id);
        }

        public void InsertOrUpdate(FortuneProducts entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.FortuneProducts.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var product = context.FortuneProducts.Find(id);
            if (product != null)
            {
                product.IsDeleted = true;
                InsertOrUpdate(product);
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}