﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class ProductsRepository : IProductsRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<Products> All => context.Products;

        public IQueryable<Products> AllIncluding(params Expression<Func<Products, object>>[] includeProperties)
        {
            IQueryable<Products> query = context.Products;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Products Find(int id)
        {
            return context.Products.Find(id);
        }

        public void InsertOrUpdate(Products entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Products.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Products.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}