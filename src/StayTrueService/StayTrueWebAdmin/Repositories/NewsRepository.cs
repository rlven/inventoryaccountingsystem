﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class NewsRepository : INewsRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<News> All => context.News.Where(x => x.IsDeleted == false);

        public IQueryable<News> AllIncluding(params Expression<Func<News, object>>[] includeProperties)
        {
            IQueryable<News> query = context.News;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public News Find(int id)
        {
            return context.News.Find(id);
        }

        public void InsertOrUpdate(News entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.News.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.News.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}