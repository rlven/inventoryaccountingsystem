﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class SmsTemplateRepository : ISmsTemplateRepository
    {
        readonly StayTrueDBEntities context = new StayTrueDBEntities();

        public IQueryable<SmsTemplate> All => context.SmsTemplate;

        public IQueryable<SmsTemplate> AllIncluding(params Expression<Func<SmsTemplate, object>>[] includeProperties)
        {
            IQueryable<SmsTemplate> query = context.SmsTemplate;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public void Delete(int id)
        {
            var entity = context.SmsTemplate.Find(id);
            if (entity != null)
            {
                context.SmsTemplate.Remove(entity);
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public SmsTemplate Find(int id)
        {
            return context.SmsTemplate.Find(id);
        }

        public void InsertOrUpdate(SmsTemplate entity)
        {
            if (entity.Id == default(int))
            {
                context.SmsTemplate.Add(entity);
            }
            else
            {
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}