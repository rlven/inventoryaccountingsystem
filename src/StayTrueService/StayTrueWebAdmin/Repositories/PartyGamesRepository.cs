﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PartyGamesRepository : IPartyGamesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();


        public IQueryable<PartyGames> All => context.PartyGames;


        public IQueryable<PartyGames> AllIncluding(params Expression<Func<PartyGames, object>>[] includeProperties)
        {
            IQueryable<PartyGames> query = context.PartyGames;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PartyGames Find(int id)
        {
            return context.PartyGames.Find(id);
        }

        public void InsertOrUpdate(PartyGames entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.PartyGames.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.PartyGames.Find(id);
            context.PartyGames.Remove(entity);
        }

        public bool AnyExist()
        {
            return context.Party.Any();
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}