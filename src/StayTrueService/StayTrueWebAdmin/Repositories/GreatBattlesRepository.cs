﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GreatBattlesRepository : IGreatBattlesRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<GreatBattles> All => _context.GreatBattles;

        public IQueryable<GreatBattles> AllIncluding(params Expression<Func<GreatBattles, object>>[] includeProperties)
        {
            IQueryable<GreatBattles> query = _context.GreatBattles;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public GreatBattles Find(int id)
        {
            return _context.GreatBattles.Find(id);
        }

        public void InsertOrUpdate(GreatBattles entity)
        {
            if (entity.ID == default(int))
            {
                _context.GreatBattles.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.GreatBattles.Find(id);
            if (entity != null)
            {
                _context.GreatBattles.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}