﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class AlbumImagesRepository : IAlbumImagesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<AlbumImages> All => context.AlbumImages;

        public IQueryable<AlbumImages> AllIncluding(params Expression<Func<AlbumImages, object>>[] includeProperties)
        {
            IQueryable<AlbumImages> query = context.AlbumImages;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public AlbumImages Find(int id)
        {
            return context.AlbumImages.Find(id);
        }

        public void InsertOrUpdate(AlbumImages entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.AlbumImages.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.AlbumImages.Find(id);
            context.Entry(entity).State = EntityState.Deleted;
            context.AlbumImages.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}