﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PartyQuizzesRepository : IPartyQuizzesRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PartyQuizzes> All => _context.PartyQuizzes;

        public IQueryable<PartyQuizzes> AllIncluding(params Expression<Func<PartyQuizzes, object>>[] includeProperties)
        {
            IQueryable<PartyQuizzes> query = _context.PartyQuizzes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PartyQuizzes Find(int id)
        {
            return _context.PartyQuizzes.Find(id);
        }

        public void InsertOrUpdate(PartyQuizzes entity)
        {
            if (entity.Id == default(int))
            {
                _context.PartyQuizzes.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PartyQuizzes.Find(id);
            if (entity != null)
            {
                _context.PartyQuizzes.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}