﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PushNotificationsRepository : IPushNotificationsRepository
    {
        private readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PushNotifications> All => _context.PushNotifications.Where(p => !p.IsDeleted);
        public IQueryable<PushNotifications> AllIncluding(params Expression<Func<PushNotifications, object>>[] includeProperties)
        {
            IQueryable<PushNotifications> query = _context.PushNotifications;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PushNotifications Find(int id)
        {
            return _context.PushNotifications.Find(id);
        }

        public void InsertOrUpdate(PushNotifications entity)
        {
            if (entity.Id == default(int))
            {
                _context.PushNotifications.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PushNotifications.Find(id);
            if (entity != null)
            {
                entity.IsDeleted = true;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}