﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class PurchaseStatusesRepository : IPurchaseStatusesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<PurchaseStatuses> All => context.PurchaseStatuses;

        public IQueryable<PurchaseStatuses> AllIncluding(params Expression<Func<PurchaseStatuses, object>>[] includeProperties)
        {
            IQueryable<PurchaseStatuses> query = context.PurchaseStatuses;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PurchaseStatuses Find(int id)
        {
            return context.PurchaseStatuses.Find(id);
        }

        public void InsertOrUpdate(PurchaseStatuses entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.PurchaseStatuses.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.PurchaseStatuses.Find(id);
            context.PurchaseStatuses.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}