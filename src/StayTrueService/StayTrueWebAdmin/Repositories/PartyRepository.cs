﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PartyRepository : IPartyRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();


        public IQueryable<Party> All => context.Party;


        public IQueryable<Party> AllIncluding(params Expression<Func<Party, object>>[] includeProperties)
        {
            IQueryable<Party> query = context.Party;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Party Find(int id)
        {
            return context.Party.Find(id);
        }

        public void InsertOrUpdate(Party entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Party.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Party.Find(id);
            context.Party.Remove(entity);
        }

        public bool AnyExist()
        {
            return context.Party.Any();
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}