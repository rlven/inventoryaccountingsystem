﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class InvitedUsersRepository : IInvitedUsersRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public IQueryable<InvitedUsers> All => context.InvitedUsers;


        public IQueryable<InvitedUsers> AllIncluding(params Expression<Func<InvitedUsers, object>>[] includeProperties)
        {
            IQueryable<InvitedUsers> query = context.InvitedUsers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public InvitedUsers Find(int id)
        {
            return context.InvitedUsers.Find(id);
        }

        public void InsertOrUpdate(InvitedUsers entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.InvitedUsers.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.InvitedUsers.Find(id);
            context.InvitedUsers.Remove(entity);
        }

        public bool AnyExist()
        {
            return context.InvitedUsers.Any();
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}