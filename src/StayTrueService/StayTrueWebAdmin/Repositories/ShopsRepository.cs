﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class ShopsRepository : IShopsRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<Shops> All => context.Shops;

        public IQueryable<Shops> AllIncluding(params Expression<Func<Shops, object>>[] includeProperties)
        {
            IQueryable<Shops> query = context.Shops;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Shops Find(int id)
        {
            return context.Shops.Find(id);
        }

        public void InsertOrUpdate(Shops entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Shops.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Shops.Find(id);
            context.Shops.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}