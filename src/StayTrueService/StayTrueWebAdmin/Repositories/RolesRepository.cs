﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class RolesRepository : IRolesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();


        public IQueryable<Roles> All => context.Roles; 
        

        public IQueryable<Roles> AllIncluding(params Expression<Func<Roles, object>>[] includeProperties)
        {
            IQueryable<Roles> query = context.Roles;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Roles Find(int id)
        {
            return context.Roles.Find(id);
        }

        public void InsertOrUpdate(Roles entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Roles.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Roles.Find(id);
            context.Roles.Remove(entity);
        }
        
        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}