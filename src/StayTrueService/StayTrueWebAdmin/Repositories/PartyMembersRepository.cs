﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PartyMembersRepository : IPartyMembersRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PartyUsers> All => _context.PartyUsers;
        public IQueryable<PartyUsers> AllIncluding(params Expression<Func<PartyUsers, object>>[] includeProperties)
        {
            IQueryable<PartyUsers> query = _context.PartyUsers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PartyUsers Find(int id)
        {
            return _context.PartyUsers.Find(id);
        }

        public void InsertOrUpdate(PartyUsers entity)
        {
            if (entity.Id == default(int))
            {
                _context.PartyUsers.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PartyUsers.Find(id);
            if (entity != null)
            {
                _context.PartyUsers.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}