﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class QuestionnairesRepository : IQuestionnairesRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Questionnaires> All => _context.Questionnaires.Where(x => x.IsDeleted == false);

        public IQueryable<Questionnaires> AllIncluding(params Expression<Func<Questionnaires, object>>[] includeProperties)
        {
            IQueryable<Questionnaires> query = _context.Questionnaires;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Questionnaires Find(int id)
        {
            return _context.Questionnaires.Find(id);
        }

        public void InsertOrUpdate(Questionnaires entity)
        {
            if (entity.Id == default(int))
            {
                _context.Questionnaires.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.Questionnaires.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}