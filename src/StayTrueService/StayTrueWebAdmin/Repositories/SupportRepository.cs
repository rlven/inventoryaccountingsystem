﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class SupportRepository:ISupportRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();
        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<SupportRequests> All => _context.SupportRequests;

        public IQueryable<SupportRequests> AllIncluding(params Expression<Func<SupportRequests, object>>[] includeProperties)
        {
            IQueryable<SupportRequests> query = _context.SupportRequests;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public SupportRequests Find(int id)
        {
            return _context.SupportRequests.Find(id);
        }

        public void InsertOrUpdate(SupportRequests entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                _context.SupportRequests.Add(entity);
            }
            else
            {
                // Existing entity
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.SupportRequests.Find(id);
            if (entity != null)
            {
                entity.IsDeleted = true;
            }
            else
            {
                throw new DbUpdateException();
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}