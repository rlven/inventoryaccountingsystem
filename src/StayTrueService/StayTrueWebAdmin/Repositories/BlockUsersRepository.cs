﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class BlockUsersRepository : IBlockUsersRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<BlockedUsers> All => _context.BlockedUsers;

        public IQueryable<BlockedUsers> AllIncluding(params Expression<Func<BlockedUsers, object>>[] includeProperties)
        {
            IQueryable<BlockedUsers> query = _context.BlockedUsers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public BlockedUsers Find(int id)
        {
            return _context.BlockedUsers.Find(id);
        }

        public void InsertOrUpdate(BlockedUsers entity)
        {
            if (entity.Id == default(int))
            {
                _context.BlockedUsers.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.BlockedUsers.Find(id);
            _context.BlockedUsers.Remove(entity);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}