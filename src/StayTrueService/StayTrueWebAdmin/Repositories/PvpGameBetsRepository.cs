﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PvpGameBetsRepository : IPvpGameBetsRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PvpGameBets> All => _context.PvpGameBets;
        public IQueryable<PvpGameBets> AllIncluding(params Expression<Func<PvpGameBets, object>>[] includeProperties)
        {
            IQueryable<PvpGameBets> query = _context.PvpGameBets;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PvpGameBets Find(int id)
        {
            return _context.PvpGameBets.Find(id);
        }

        public void InsertOrUpdate(PvpGameBets entity)
        {
            if (entity.Id == default(int))
            {
                _context.PvpGameBets.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var bet = _context.PvpGameBets.Find(id);
            if (bet != null)
            {
                _context.PvpGameBets.Remove(bet);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}