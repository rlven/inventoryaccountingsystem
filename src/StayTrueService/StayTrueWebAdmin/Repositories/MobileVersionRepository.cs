﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class MobileVersionRepository : IMobileVersionRepository
    {
        private readonly StayTrueDBEntities context = new StayTrueDBEntities();

        public IQueryable<MobileVersions> All => context.MobileVersions;

        public IQueryable<MobileVersions> AllIncluding(params Expression<Func<MobileVersions, object>>[] includeProperties)
        {
            IQueryable<MobileVersions> query = context.MobileVersions;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public void Delete(int id)
        {
            var entity = context.MobileVersions.Find(id);
            context.Entry(entity).State = EntityState.Deleted;
            context.MobileVersions.Remove(entity);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public MobileVersions Find(int id)
        {
            return context.MobileVersions.Find(id);
        }

        public void InsertOrUpdate(MobileVersions entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.MobileVersions.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
