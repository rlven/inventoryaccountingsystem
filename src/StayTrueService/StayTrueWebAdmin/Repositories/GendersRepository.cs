﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GendersRepository : IGendersRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Genders> All => _context.Genders;

        public IQueryable<Genders> AllIncluding(params Expression<Func<Genders, object>>[] includeProperties)
        {
            IQueryable<Genders> query = _context.Genders;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Genders Find(int id)
        {
            return _context.Genders.Find(id);
        }

        public void InsertOrUpdate(Genders entity)
        {
            if (entity.Id == default(int))
            {
                _context.Genders.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}