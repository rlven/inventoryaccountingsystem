﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class FortunePurchaseHistoryRepository : IFortunePurchasesHistoryRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<FortunePurchasesHistory> All => context.FortunePurchasesHistory;
        public IQueryable<FortunePurchasesHistory> AllIncluding(params Expression<Func<FortunePurchasesHistory, object>>[] includeProperties)
        {
            IQueryable<FortunePurchasesHistory> query = context.FortunePurchasesHistory;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public FortunePurchasesHistory Find(int id)
        {
            return context.FortunePurchasesHistory.Find(id);
        }

        public void InsertOrUpdate(FortunePurchasesHistory entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.FortunePurchasesHistory.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var purchase = context.FortunePurchasesHistory.Find(id);
            if (purchase != null)
            {
                context.FortunePurchasesHistory.Remove(purchase);
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}