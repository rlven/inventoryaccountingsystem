﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class ChatPrivateGroupsUsersRepository : IChatPrivateGroupsUsersRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<ChatPrivateGroupsUsers> All => _context.ChatPrivateGroupsUsers;

        public IQueryable<ChatPrivateGroupsUsers> AllIncluding(params Expression<Func<ChatPrivateGroupsUsers, object>>[] includeProperties)
        {
            IQueryable<ChatPrivateGroupsUsers> query = _context.ChatPrivateGroupsUsers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public ChatPrivateGroupsUsers Find(int id)
        {
            return _context.ChatPrivateGroupsUsers.Find(id);
        }

        public void InsertOrUpdate(ChatPrivateGroupsUsers entity)
        {
            if (entity.Id == default(int))
            {
                _context.ChatPrivateGroupsUsers.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.ChatPrivateGroupsUsers.Find(id);
            if (entity != null)
            {
                _context.ChatPrivateGroupsUsers.Remove(entity);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}