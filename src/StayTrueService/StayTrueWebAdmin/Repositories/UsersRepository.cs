﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Users> All => _context.Users;

        public IQueryable<Users> AllIncluding(params Expression<Func<Users, object>>[] includeProperties)
        {
            IQueryable<Users> query = _context.Users;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Users Find(int id)
        {
            return _context.Users.Find(id);
        }

        public void InsertOrUpdate(Users entity)
        {
            if (entity.Id == default(int))
            {
                _context.Users.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.Users.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        private static readonly string AvatarFolderName = @"/Image/Avatars/";
        public string GetAvatarImage(string phoneHash)
        {
            string folderUrl = ConfigurationManager.AppSettings["VirtualDirectoryUrl"];
            var user = _context.Users.SingleOrDefault(x => x.Id == 1);
            if (user != null)
            {
               
                if (user.Avatar != null)
                {
                    return $"{folderUrl}{ AvatarFolderName }{user.Id}{user.Avatar}";
                }
            }

            return null;
        }
    }
}