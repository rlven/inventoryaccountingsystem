﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class LotteryUsersRepository : ILotteryUsersRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public IQueryable<LotteryUsers> All => context.LotteryUsers;

        public IQueryable<LotteryUsers> AllIncluding(params Expression<Func<LotteryUsers, object>>[] includeProperties)
        {
            IQueryable<LotteryUsers> query = context.LotteryUsers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public LotteryUsers Find(int id)
        {
            return context.LotteryUsers.Find(id);
        }

        public void InsertOrUpdate(LotteryUsers entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.LotteryUsers.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.LotteryUsers.Find(id);
            context.LotteryUsers.Remove(entity);
        }

        public bool AnyExist()
        {
            return context.LotteryUsers.Any();
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}