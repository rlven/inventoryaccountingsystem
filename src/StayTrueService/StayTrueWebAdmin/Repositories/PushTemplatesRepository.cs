﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PushTemplatesRepository : IPushTemplatesRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PushTemplates> All => _context.PushTemplates.Where(t => !t.IsDeleted);
        public IQueryable<PushTemplates> AllIncluding(params Expression<Func<PushTemplates, object>>[] includeProperties)
        {
            IQueryable<PushTemplates> query = _context.PushTemplates;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PushTemplates Find(int id)
        {
            return _context.PushTemplates.Find(id);
        }

        public void InsertOrUpdate(PushTemplates entity)
        {
            if (entity.Id == default(int))
            {
                _context.PushTemplates.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PushTemplates.Find(id);
            if (entity != null)
            {
                entity.IsDeleted = true;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}