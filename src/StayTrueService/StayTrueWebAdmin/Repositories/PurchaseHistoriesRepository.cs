﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class PurchaseHistoriesRepository : IPurchaseHistoriesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<PurchaseHistories> All => context.PurchaseHistories;

        public IQueryable<PurchaseHistories> AllIncluding(params Expression<Func<PurchaseHistories, object>>[] includeProperties)
        {
            IQueryable<PurchaseHistories> query = context.PurchaseHistories;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PurchaseHistories Find(int id)
        {
            return context.PurchaseHistories.Find(id);
        }

        public void InsertOrUpdate(PurchaseHistories entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.PurchaseHistories.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.PurchaseHistories.Find(id);
            context.PurchaseHistories.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}