﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class AccountStatusesRepository : IAccountStatusesRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();


        public IQueryable<AccountStatuses> All => context.AccountStatuses; 
        

        public IQueryable<AccountStatuses> AllIncluding(params Expression<Func<AccountStatuses, object>>[] includeProperties)
        {
            IQueryable<AccountStatuses> query = context.AccountStatuses;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public AccountStatuses Find(int id)
        {
            return context.AccountStatuses.Find(id);
        }

        public void InsertOrUpdate(AccountStatuses entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.AccountStatuses.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.AccountStatuses.Find(id);
            context.AccountStatuses.Remove(entity);
        }
        
        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}