﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class SmsMessageMsgRepository : ISmsMessageMsgRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<SmsMessageMsg> All => context.SmsMessageMsg;

        public IQueryable<SmsMessageMsg> AllIncluding(params Expression<Func<SmsMessageMsg, object>>[] includeProperties)
        {
            IQueryable<SmsMessageMsg> query = context.SmsMessageMsg;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public SmsMessageMsg Find(int id)
        {
            return context.SmsMessageMsg.Find(id);
        }

        public void InsertOrUpdate(SmsMessageMsg entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.SmsMessageMsg.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void AddRange(List<SmsMessageMsg> entity)
        {
            if (entity!=null)
            {
                // New entity
                context.SmsMessageMsg.AddRange(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.SmsMessageMsg.Find(id);
            context.SmsMessageMsg.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}