﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class FortuneWheelComponentsRepository : IFortuneWheelComponentsRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<FortuneWheelComponents> All => context.FortuneWheelComponents;

        public IQueryable<FortuneWheelComponents> AllIncluding(
            params Expression<Func<FortuneWheelComponents, object>>[] includeProperties)
        {
            IQueryable<FortuneWheelComponents> query = context.FortuneWheelComponents;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public FortuneWheelComponents Find(int id)
        {
            return context.FortuneWheelComponents.Find(id);
        }

        public void InsertOrUpdate(FortuneWheelComponents entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.FortuneWheelComponents.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var component = context.FortuneWheelComponents.Find(id);
            if (component != null)
            {
                context.FortuneWheelComponents.Remove(component);
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void AddRange(List<FortuneWheelComponents> entities)
        {
            context.FortuneWheelComponents.AddRange(entities);
        }
    }
}