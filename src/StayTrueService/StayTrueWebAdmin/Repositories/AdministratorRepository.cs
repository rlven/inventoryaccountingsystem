﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class AdministratorRepository : IAdministratorRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();


        public IQueryable<Administrators> All => context.Administrators; 
        

        public IQueryable<Administrators> AllIncluding(params Expression<Func<Administrators, object>>[] includeProperties)
        {
            IQueryable<Administrators> query = context.Administrators;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Administrators Find(int id)
        {
            return context.Administrators.Find(id);
        }

        public void InsertOrUpdate(Administrators entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Administrators.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Administrators.Find(id);
            context.Administrators.Remove(entity);
        }

        public Administrators Check(string login, string password)
        {
            var entity = context.Administrators.SingleOrDefault(o => o.Login == login);
            
            if (entity != null && BCrypt.Net.BCrypt.Verify(password, entity.Password))
            {
                return entity;
            }
            return null;
        }

        public string GetHash(string password)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            string hash = BCrypt.Net.BCrypt.HashPassword(password, salt);
            return hash;
        }

        public bool AnyExist()
        {
            return context.Administrators.Any();
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}