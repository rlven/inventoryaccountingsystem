﻿using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace StayTrueWebAdmin.Repositories
{
    public class NewsCategoryRepository : INewsCategoryRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<NewsCategories> All => context.NewsCategories;

        public IQueryable<NewsCategories> AllIncluding(params Expression<Func<NewsCategories, object>>[] includeProperties)
        {
            IQueryable<NewsCategories> query = context.NewsCategories;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public NewsCategories Find(int id)
        {
            return context.NewsCategories.Find(id);
        }

        public void InsertOrUpdate(NewsCategories entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.NewsCategories.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.NewsCategories.Find(id);
            context.NewsCategories.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}