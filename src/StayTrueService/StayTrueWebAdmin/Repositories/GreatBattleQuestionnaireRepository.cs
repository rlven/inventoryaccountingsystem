﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GreatBattleQuestionnaireRepository : IGreatBattleQuestionnaireRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<GreatBattleQuestionnaire> All => _context.GreatBattleQuestionnaire;

        public IQueryable<GreatBattleQuestionnaire> AllIncluding(params Expression<Func<GreatBattleQuestionnaire, object>>[] includeProperties)
        {
            IQueryable<GreatBattleQuestionnaire> query = _context.GreatBattleQuestionnaire;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public GreatBattleQuestionnaire Find(int id)
        {
            return _context.GreatBattleQuestionnaire.Find(id);
        }

        public void InsertOrUpdate(GreatBattleQuestionnaire entity)
        {
            if (entity.ID == default(int))
            {
                _context.GreatBattleQuestionnaire.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.GreatBattleQuestionnaire.Find(id);
            if (entity != null)
            {
                _context.GreatBattleQuestionnaire.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}