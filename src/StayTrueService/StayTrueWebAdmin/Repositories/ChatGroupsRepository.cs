﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class ChatGroupsRepository : IChatGroupsRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<ChatGroups> All => _context.ChatGroups.Where(x => x.IsDeleted == false);

        public IQueryable<ChatGroups> AllIncluding(params Expression<Func<ChatGroups, object>>[] includeProperties)
        {
            IQueryable<ChatGroups> query = _context.ChatGroups;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public ChatGroups Find(int id)
        {
            return _context.ChatGroups.Find(id);
        }

        public void InsertOrUpdate(ChatGroups entity)
        {
            if (entity.Id == default(int))
            {
                _context.ChatGroups.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.ChatGroups.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}