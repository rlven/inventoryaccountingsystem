﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GameSessionRepository : IGameSessionRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<GameSession> All => _context.GameSession;

        public IQueryable<GameSession> AllIncluding(params Expression<Func<GameSession, object>>[] includeProperties)
        {
            IQueryable<GameSession> query = _context.GameSession;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public GameSession Find(int id)
        {
            return _context.GameSession.Find(id);
        }

        public void InsertOrUpdate(GameSession entity)
        {
            if (entity.Id == default(int))
            {
                _context.GameSession.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {

        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public ICollection<GameSession> GetByUserId(int userId)
        {
            return _context.GameSession.Where(x=>x.UserId == userId).ToList();
        }

       
    }
}