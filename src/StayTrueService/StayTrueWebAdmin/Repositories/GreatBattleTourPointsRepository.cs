﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GreatBattleTourPointsRepository : IGreatBattleTourPointsRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<GreatBattleTourPoints> All => _context.GreatBattleTourPoints;
        public IQueryable<GreatBattleTourPoints> AllIncluding(params Expression<Func<GreatBattleTourPoints, object>>[] includeProperties)
        {
            IQueryable<GreatBattleTourPoints> query = _context.GreatBattleTourPoints;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public GreatBattleTourPoints Find(int id)
        {
            return _context.GreatBattleTourPoints.Find(id);
        }

        public void InsertOrUpdate(GreatBattleTourPoints entity)
        {
            if (entity.ID == default(int))
            {
                _context.GreatBattleTourPoints.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.GreatBattleTourPoints.Find(id);
            if (entity != null)
            {
                _context.GreatBattleTourPoints.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}