﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class CitiesRepository : ICitiesRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Cities> All => _context.Cities;

        public IQueryable<Cities> AllIncluding(params Expression<Func<Cities, object>>[] includeProperties)
        {
            IQueryable<Cities> query = _context.Cities;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Cities Find(int id)
        {
            return _context.Cities.Find(id);
        }

        public void InsertOrUpdate(Cities entity)
        {
            if (entity.Id == default(int))
            {
                _context.Cities.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.Users.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}