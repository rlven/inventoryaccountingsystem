﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PartyQuestionnaireRepository : IPartyQuestionnaireRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<PartyQuestionnaire> All => context.PartyQuestionnaire;

        public IQueryable<PartyQuestionnaire> AllIncluding(params Expression<Func<PartyQuestionnaire, object>>[] includeProperties)
        {
            IQueryable<PartyQuestionnaire> query = context.PartyQuestionnaire;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PartyQuestionnaire Find(int id)
        {
            return context.PartyQuestionnaire.Find(id);
        }

        public void InsertOrUpdate(PartyQuestionnaire entity)
        {
            if (entity.Id == default(int))
            {
                context.PartyQuestionnaire.Add(entity);
            }
            else
            {
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Party.Find(id);
            context.Party.Remove(entity);
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Exception raise = ex;
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
    }
}