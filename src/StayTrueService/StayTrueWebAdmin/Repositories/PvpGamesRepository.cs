﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PvpGamesRepository : IPvpGamesRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PvpGames> All => _context.PvpGames.Where(x => x.IsDelete == false);

        public IQueryable<PvpGames> AllIncluding(params Expression<Func<PvpGames, object>>[] includeProperties)
        {
            IQueryable<PvpGames> query = _context.PvpGames;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PvpGames Find(int id)
        {
            return _context.PvpGames.Find(id);
        }

        public void InsertOrUpdate(PvpGames entity)
        {
            if (entity.Id == default(int))
            {
                _context.PvpGames.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PvpGames.Find(id);
            entity.IsDelete = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}