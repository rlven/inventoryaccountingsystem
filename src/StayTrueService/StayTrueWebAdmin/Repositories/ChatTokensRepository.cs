﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class ChatTokensRepository : IChatTokensRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<ChatTokens> All => _context.ChatTokens;

        public IQueryable<ChatTokens> AllIncluding(params Expression<Func<ChatTokens, object>>[] includeProperties)
        {
            IQueryable<ChatTokens> query = _context.ChatTokens;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public ChatTokens Find(int id)
        {
            return _context.ChatTokens.Find(id);
        }

        public void InsertOrUpdate(ChatTokens entity)
        {
            if (entity.id == default(int))
            {
                _context.ChatTokens.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.Users.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}