﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GreatBattleQuestionnaireAnswersRepository : IGreatBattleQuestionnaireAnswersRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<GreatBattleQuestionnaireAnswer> All => _context.GreatBattleQuestionnaireAnswer;

        public IQueryable<GreatBattleQuestionnaireAnswer> AllIncluding(params Expression<Func<GreatBattleQuestionnaireAnswer, object>>[] includeProperties)
        {
            IQueryable<GreatBattleQuestionnaireAnswer> query = _context.GreatBattleQuestionnaireAnswer;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            var entity = _context.GreatBattleQuestionnaireAnswer.Find(id);
            if (entity != null)
            {
                _context.GreatBattleQuestionnaireAnswer.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public GreatBattleQuestionnaireAnswer Find(int id)
        {
            return _context.GreatBattleQuestionnaireAnswer.Find(id);
        }

        public void InsertOrUpdate(GreatBattleQuestionnaireAnswer entity)
        {
            if (entity.Id == default(int))
            {
                _context.GreatBattleQuestionnaireAnswer.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}