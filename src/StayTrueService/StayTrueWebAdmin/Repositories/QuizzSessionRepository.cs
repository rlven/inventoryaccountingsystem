﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class QuizzSessionRepository : IQuizzSessionRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<QuizzesSession> All => context.QuizzesSession;

        public IQueryable<QuizzesSession> AllIncluding(params Expression<Func<QuizzesSession, object>>[] includeProperties)
        {
            IQueryable<QuizzesSession> query = context.QuizzesSession;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public QuizzesSession Find(int id)
        {
            return context.QuizzesSession.Find(id);
        }

        public void InsertOrUpdate(QuizzesSession entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.QuizzesSession.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.QuizzesSession.Find(id);
            if (entity != null)
            {
                context.QuizzesSession.Remove(entity);
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}