﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GreatBattleMembersRepository : IGreatBattleMembersRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public IQueryable<GreatBattlesUsers> All => _context.GreatBattlesUsers;

        public IQueryable<GreatBattlesUsers> AllIncluding(params Expression<Func<GreatBattlesUsers, object>>[] includeProperties)
        {
            IQueryable<GreatBattlesUsers> query = _context.GreatBattlesUsers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            var entity = _context.GreatBattlesUsers.Find(id);
            if (entity != null)
            {
                _context.GreatBattlesUsers.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public GreatBattlesUsers Find(int id)
        {
            return _context.GreatBattlesUsers.Find(id);
        }

        public void InsertOrUpdate(GreatBattlesUsers entity)
        {
            if (entity.ID == default(int))
            {
                _context.GreatBattlesUsers.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}