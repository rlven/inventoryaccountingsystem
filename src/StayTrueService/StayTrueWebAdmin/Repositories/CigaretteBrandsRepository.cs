﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class CigaretteBrandsRepository : ICigaretteBrandsRepository
    {
        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<CigaretteBrands> All => _context.CigaretteBrands.Where(b => !b.IsDeleted);
        public IQueryable<CigaretteBrands> AllIncluding(params Expression<Func<CigaretteBrands, object>>[] includeProperties)
        {
            IQueryable<CigaretteBrands> query = _context.CigaretteBrands;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public CigaretteBrands Find(int id)
        {
            return _context.CigaretteBrands.Find(id);
        }

        public void InsertOrUpdate(CigaretteBrands entity)
        {
            if (entity.Id == default(int))
            {
                _context.CigaretteBrands.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.CigaretteBrands.Find(id);
            if (entity != null)
            {
                entity.IsDeleted = true;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}