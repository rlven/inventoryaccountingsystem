﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class CigarettePackSizesRepository : ICigarettePackSizesRepository
    {
        StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<CigarettePackSizes> All => _context.CigarettePackSizes;
        public IQueryable<CigarettePackSizes> AllIncluding(params Expression<Func<CigarettePackSizes, object>>[] includeProperties)
        {
            IQueryable<CigarettePackSizes> query = _context.CigarettePackSizes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public CigarettePackSizes Find(int id)
        {
            return _context.CigarettePackSizes.Find(id);
        }

        public void InsertOrUpdate(CigarettePackSizes entity)
        {
            if (entity.Id == default(int))
            {
                _context.CigarettePackSizes.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PartyUsers.Find(id);
            if (entity != null)
            {
                _context.PartyUsers.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}