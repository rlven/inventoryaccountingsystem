﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class AlbumsRepository: IAlbumsRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<Albums> All => context.Albums;

        public IQueryable<Albums> AllIncluding(params Expression<Func<Albums, object>>[] includeProperties)
        {
            IQueryable<Albums> query = context.Albums;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Albums Find(int id)
        {
            return context.Albums.Find(id);
        }

        public void InsertOrUpdate(Albums entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.Albums.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.Albums.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}