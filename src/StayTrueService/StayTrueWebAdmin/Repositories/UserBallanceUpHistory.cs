﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class UserBallanceUpHistory : IUserBallanceUpHistory
    {
        StayTrueDBEntities context = new StayTrueDBEntities();
        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<DataAccessLayer.UserBallanceUpHistory> All => context.UserBallanceUpHistory;

        public IQueryable<DataAccessLayer.UserBallanceUpHistory> AllIncluding(params Expression<Func<DataAccessLayer.UserBallanceUpHistory, object>>[] includeProperties)
        {
            IQueryable<DataAccessLayer.UserBallanceUpHistory> query = context.UserBallanceUpHistory;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public DataAccessLayer.UserBallanceUpHistory Find(int id)
        {
            return context.UserBallanceUpHistory.Find(id);
        }

        public void InsertOrUpdate(DataAccessLayer.UserBallanceUpHistory entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.UserBallanceUpHistory.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.UserBallanceUpHistory.Find(id);
            context.UserBallanceUpHistory.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}