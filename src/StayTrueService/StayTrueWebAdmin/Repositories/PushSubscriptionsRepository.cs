﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class PushSubscriptionsRepository : IPushSubscribtionsRepository
    {
        StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<PushSubscribtions> All => _context.PushSubscribtions;
        public IQueryable<PushSubscribtions> AllIncluding(params Expression<Func<PushSubscribtions, object>>[] includeProperties)
        {
            IQueryable<PushSubscribtions> query = _context.PushSubscribtions;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public PushSubscribtions Find(int id)
        {
            return _context.PushSubscribtions.Find(id);
        }

        public void InsertOrUpdate(PushSubscribtions entity)
        {
            if (entity.Id == default(int))
            {
                _context.PushSubscribtions.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.PushSubscribtions.Find(id);
            if (entity != null)
            {
                _context.PushSubscribtions.Remove(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void DeleteRange(List<PushSubscribtions> subscriptions)
        {
            _context.PushSubscribtions.RemoveRange(subscriptions);
        }
    }
}