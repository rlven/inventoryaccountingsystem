﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class QuizzesRepository : IQuizzesRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Quizzes> All => _context.Quizzes.Where(x => x.IsDeleted == false);

        public IQueryable<Quizzes> AllIncluding(params Expression<Func<Quizzes, object>>[] includeProperties)
        {
            IQueryable<Quizzes> query = _context.Quizzes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Quizzes Find(int id)
        {
            return _context.Quizzes.Find(id);
        }

        public void InsertOrUpdate(Quizzes entity)
        {
            if (entity.Id == default(int))
            {
                _context.Quizzes.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.Quizzes.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}