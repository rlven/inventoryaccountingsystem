﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class NewsTypeRepository : INewsTypeRepository
    {
        StayTrueDBEntities context = new StayTrueDBEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<NewsTypes> All => context.NewsTypes;

        public IQueryable<NewsTypes> AllIncluding(params Expression<Func<NewsTypes, object>>[] includeProperties)
        {
            IQueryable<NewsTypes> query = context.NewsTypes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public NewsTypes Find(int id)
        {
            return context.NewsTypes.Find(id);
        }

        public void InsertOrUpdate(NewsTypes entity)
        {
            if (entity.Id == default(int))
            {
                // New entity
                context.NewsTypes.Add(entity);
            }
            else
            {
                // Existing entity
                context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = context.NewsTypes.Find(id);
            context.NewsTypes.Remove(entity);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}