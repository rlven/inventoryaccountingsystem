﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccessLayer;
using StayTrueWebAdmin.Interfaces;

namespace StayTrueWebAdmin.Repositories
{
    public class GameRepository : IGamesRepository
    {

        readonly StayTrueDBEntities _context = new StayTrueDBEntities();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Games> All => _context.Games.Where(x => x.IsDeleted == false);

        public IQueryable<Games> AllIncluding(params Expression<Func<Games, object>>[] includeProperties)
        {
            IQueryable<Games> query = _context.Games;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Games Find(int id)
        {
            return _context.Games.Find(id);
        }

        public void InsertOrUpdate(Games entity)
        {
            if (entity.Id == default(int))
            {
                _context.Games.Add(entity);
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var entity = _context.Games.Find(id);
            entity.IsDeleted = true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}