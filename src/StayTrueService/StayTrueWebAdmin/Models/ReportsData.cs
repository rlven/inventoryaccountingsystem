﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models
{
    public class ReportsData
    {
        public decimal Amount { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
    }
}