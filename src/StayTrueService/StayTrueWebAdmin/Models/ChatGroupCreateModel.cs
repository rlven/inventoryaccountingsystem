﻿using System;
using System.Web;

namespace StayTrueWebAdmin.Models
{
    public class ChatGroupCreateModel
    {
        public String Name { get; set; }
        public bool IsPrivate { get; set; }
    }
}