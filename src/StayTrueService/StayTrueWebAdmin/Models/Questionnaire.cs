﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using StayTrueWebAdmin.Meta;

namespace StayTrueWebAdmin.Models
{
    [MetadataType(typeof(QuestionnaireMeta))]
    public class Questionnaire 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Question> Questions { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int Wincoins { get; set; }
    }
}