﻿using System.ComponentModel.DataAnnotations;
using StayTrueWebAdmin.Meta;

namespace StayTrueWebAdmin.Models
{
    [MetadataType(typeof(QuestionMeta))]
    public class Question : Common.Models.Question
    {
    }
}