﻿using System;
using Postal;

namespace StayTrueWebAdmin.Models

{
    [Serializable]
    public class EmailRegisterUserModel : Email
    {
        public string[] To { get; set; }
        public string UserName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string ApproveCode { get; set; }
        public string Subject { get; set; }
    }
}