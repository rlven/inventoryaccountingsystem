﻿using System;
using Postal;

namespace StayTrueWebAdmin.Models
{
    [Serializable]
    public class EmailProfileChangedModel : Email
    {
        public string[] To { get; set; }
        public string UserName { get; set; }
        public string Subject { get; set; }
    }
}