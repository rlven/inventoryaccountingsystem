﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Models
{
    public class SmsSendModel
    {
        public string Title { get; set; }
        [Required(ErrorMessage = "Тело сообщения обязательное")]
        [AllowHtml]
        public string Text { get; set; }
        public DateTime SendDate { get; set; }
        public bool IsPasswordRequired { get; set; }
        public int TemplateId { get; set; }
        public int[] UserIds { get; set; }
        public string SmsCost { get; set; }
        public bool IsChecked { get; set; }
        public string Phone { get; set; }
    }
}