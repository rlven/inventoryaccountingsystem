﻿using System.ComponentModel.DataAnnotations;
namespace StayTrueWebAdmin.Models
{
    public class LoginModel
    {
        [Display(Name = "Login")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Логин обязательное")]
        //[StringLength(250, MinimumLength = 1, ErrorMessage = "StringRangeLengthLimitErrorMessage")]
        [DataType(DataType.Text)]
        public string Login { get; set; }

        [Display(Name = "Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Пароль обязательное")]
        //[StringLength(15, MinimumLength = 6, ErrorMessage = "StringRangeLengthLimitErrorMessage")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "RememberMe")]
        public string RememberMe { get; set; }
    }
}