﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models
{
    public class PaymentReport
    {
        public string Id { get; set; }
        public string FirstName { get; set; } = "NameNotExist";
        public string LastName { get; set; } = "LastNameNotExist";
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
    }
}