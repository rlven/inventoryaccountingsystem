﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class SupportFilterModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Body { get; set; }
        public bool? IsAnswered { get; set; }

        public int PageSize = 10;
    }

    public class SupportFilteredModel
    {
        public SupportFilterModel Filter;

        public IPagedList<SupportRequests> Supports { get; set; }


        public SupportFilteredModel(SupportFilterModel filter)
        {
            Filter = filter;
        }
        public void ProcessData(IQueryable<SupportRequests> supports, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Supports = SortData(FilterData(supports), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<SupportRequests> FilterData(IQueryable<SupportRequests> supports)
        {

            if (Filter.DateFrom.HasValue)
                supports = supports.Where(a => a.CreationDate >= Filter.DateFrom.Value);

            if (Filter.DateTo.HasValue)
                supports = supports.Where(a => a.CreationDate < Filter.DateTo.Value);

            if (Filter.IsAnswered.HasValue)
                supports = supports.Where(a => a.IsAnswered == Filter.IsAnswered.Value);

            if (!String.IsNullOrEmpty(Filter.Body))
                supports = supports.Where(a => a.Body.Contains(Filter.Body));

            if (!String.IsNullOrEmpty(Filter.Email))
                supports = supports.Where(a => a.Email.Contains(Filter.Email));

            if (!String.IsNullOrEmpty(Filter.PhoneNumber))
                supports = supports.Where(a => a.Phone.Contains(Filter.PhoneNumber));


            return supports.AsQueryable();
        }

        public IOrderedQueryable<SupportRequests> SortData(IQueryable<SupportRequests> supports, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? supports.OrderByDescending(a => a.Id) : supports.OrderBy(a => a.Id);
            return supports.OrderByDescending(a => a.Id);
        }

        private void CleanData()
        {
        }
    }

}
