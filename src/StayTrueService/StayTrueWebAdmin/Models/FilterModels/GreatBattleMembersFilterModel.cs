﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using StayTrueWebAdmin.Extentions;
using StayTrueWebAdmin.Models.ViewModels;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class GreatBattleMembersFilterModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? DobFrom { get; set; }
        public DateTime? DobTo { get; set; }
        public int? City { get; set; }
        public DateTime? LastLoginFrom { get; set; }
        public DateTime? LastLoginTo { get; set; }
        public string Phone { get; set; }
        public string Group { get; set; }
        public int? Points { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public bool? IsBlocked { get; set; }
        public int PageSize = 10;
    }

    public class GreatBattleMembersFilteredModel
    {
        public GreatBattleMembersFilterModel Filter;

        public IPagedList<GreatBattlesUsers> GreatBattlesUsers { get; set; }

        public GreatBattleMembersFilteredModel(GreatBattleMembersFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<GreatBattlesUsers> greatBattleUsers, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            GreatBattlesUsers = SortData(FilterData(greatBattleUsers), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

         public IQueryable<GreatBattlesUsers> FilterData(IQueryable<GreatBattlesUsers> greaBattleUsers)
        {
            if (!string.IsNullOrEmpty(Filter.Name))
                greaBattleUsers = greaBattleUsers.Where(x => x.Users.FirstName.Contains(Filter.Name));

            if (!String.IsNullOrWhiteSpace(Filter.Surname))
            {
                greaBattleUsers = greaBattleUsers.Where(x => x.Users.LastName.Contains(Filter.Surname));
            }

            if (Filter.DobFrom.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.Users.BirthDate >= Filter.DobFrom.Value);
            }

            if (Filter.DobTo.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.Users.BirthDate <= Filter.DobTo.Value);
            }

            if (Filter.City.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.Users.Cities.Id == Filter.City.Value);
            }

            if (Filter.LastLoginFrom.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.Users.LastSignInP.HasValue && a.Users.LastSignInP >= Filter.LastLoginFrom.Value);
            }

            if (Filter.LastLoginTo.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.Users.LastSignInP.HasValue && a.Users.LastSignInP <= Filter.LastLoginTo.Value);
            }

            if (Filter.IsBlocked.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.IsBlocked == Filter.IsBlocked);
            }

            if (!String.IsNullOrWhiteSpace(Filter.Phone))
            {
                greaBattleUsers = greaBattleUsers.Where(a => a.Users.Phone.Contains(Filter.Phone));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Group))
            {
                greaBattleUsers = greaBattleUsers.Where(g => g.GreatBattleGroups.Name.Contains(Filter.Group));
            }

            if (Filter.Points.HasValue)
            {
                greaBattleUsers = greaBattleUsers.Where(g => g.Points == Filter.Points);
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer1))
            {
                greaBattleUsers = greaBattleUsers.Where(g => g.Users.GreatBattleQuestionnaireAnswer.FirstOrDefault(x=>x.GreatBattleQuestionnaire.IDGreatBattle == g.GreatBattleID).Answer1.Contains(Filter.Answer1));
            }
            if (!String.IsNullOrWhiteSpace(Filter.Answer2))
            {
                greaBattleUsers = greaBattleUsers.Where(g => g.Users.GreatBattleQuestionnaireAnswer.FirstOrDefault(x => x.GreatBattleQuestionnaire.IDGreatBattle == g.GreatBattleID).Answer2.Contains(Filter.Answer2));
            }
            if (!String.IsNullOrWhiteSpace(Filter.Answer3))
            {
                greaBattleUsers = greaBattleUsers.Where(g => g.Users.GreatBattleQuestionnaireAnswer.FirstOrDefault(x => x.GreatBattleQuestionnaire.IDGreatBattle == g.GreatBattleID).Answer3.Contains(Filter.Answer3));
            }
            if (!String.IsNullOrWhiteSpace(Filter.Answer4))
            {
                greaBattleUsers = greaBattleUsers.Where(g => g.Users.GreatBattleQuestionnaireAnswer.FirstOrDefault(x => x.GreatBattleQuestionnaire.IDGreatBattle == g.GreatBattleID).Answer4.Contains(Filter.Answer4));
            }


            return greaBattleUsers;
        }

        public IOrderedQueryable<GreatBattlesUsers> SortData(IQueryable<GreatBattlesUsers> greatBattleUsers, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? greatBattleUsers.OrderByDescending(x => x.ID) : greatBattleUsers.OrderBy(x => x.ID);
            return greatBattleUsers.OrderByDescending(x => x.ID);
        }

        public void CleanData()
        {
            Filter.Name = Filter.Name.TrimSpaces().StripHtml(false);
            Filter.Surname = Filter.Surname.TrimSpaces().StripHtml(false);
            Filter.Phone = Filter.Phone.TrimSpaces().StripHtml(false);
        }
    }

}