﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class QuestionnairesFilterModel
    {
        public string PublicationDateFrom { get; set; }
        public string PublicationDateTo { get; set; }
        public string Title { get; set; }
        public int? QuestionsCount { get; set; }
        public int? AnswersCount { get; set; }
        public bool? Status { get; set; }
        public int PageSize = 10;
    }

    public class QuestionnairesFilteredModel
    {
        public QuestionnairesFilterModel Filter;

        public IPagedList<Questionnaires> Questionnaires { get; set; }

        public QuestionnairesFilteredModel(QuestionnairesFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Questionnaires> questionnaires, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Questionnaires = SortData(FilterData(questionnaires), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Questionnaires> FilterData(IQueryable<Questionnaires> questionnaires)
        {
            if (!string.IsNullOrEmpty(Filter.Title))
                questionnaires = questionnaires.Where(x => x.Name.Contains(Filter.Title));

            if (!String.IsNullOrWhiteSpace(Filter.PublicationDateFrom))
            {
                DateTime dateFrom = DateTime.ParseExact(Filter.PublicationDateFrom, "dd/MM/yyyy", null);
                questionnaires = questionnaires.Where(a => a.StartDate >= dateFrom);
            }

            if (!String.IsNullOrWhiteSpace(Filter.PublicationDateTo))
            {
                DateTime dateTo = DateTime.ParseExact(Filter.PublicationDateTo, "dd/MM/yyyy", null);
                questionnaires = questionnaires.Where(a => a.StartDate <= dateTo);
            }

            if (Filter.Status.HasValue)
            {
                questionnaires = questionnaires.Where(a => a.IsActive == Filter.Status).AsQueryable();
            }

            if (Filter.AnswersCount.HasValue)
            {
                questionnaires = questionnaires.Where(a => a.QuestionnaireAnswers.Count > Filter.AnswersCount.Value).AsQueryable();
            }


            if (Filter.QuestionsCount.HasValue)
                questionnaires = questionnaires.Where(a => a.Questions.Length >= Filter.QuestionsCount).AsQueryable();

           return questionnaires;
        }

        public IOrderedQueryable<Questionnaires> SortData(IQueryable<Questionnaires> questionnaires, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? questionnaires.OrderByDescending(x => x.Id) : questionnaires.OrderBy(x => x.Id);
            return questionnaires.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
            Filter.Title = Filter.Title.TrimSpaces().StripHtml(false);
        }
    }
}