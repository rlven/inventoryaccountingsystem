﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class QuizzesFilterModel
    {
        public string PublicationDateFrom { get; set; }
        public string PublicationDateTo { get; set; }
        public string Title { get; set; }
        public int? Wincointsfrom { get; set; }
        public int? Wincointsto { get; set; }
        public bool? Status { get; set; }
        public int PageSize = 10;
    }

    public class QuizzesFilteredModel
    {
        public QuizzesFilterModel Filter;

        public IPagedList<Quizzes> Quizzes { get; set; }

        public QuizzesFilteredModel(QuizzesFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Quizzes> quizzes, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Quizzes = SortData(FilterData(quizzes), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Quizzes> FilterData(IQueryable<Quizzes> quizzes)
        {
            if (!string.IsNullOrEmpty(Filter.Title))
                quizzes = quizzes.Where(x => x.Name.Contains(Filter.Title));

            if (!String.IsNullOrWhiteSpace(Filter.PublicationDateFrom))
            {
                DateTime dateFrom = DateTime.ParseExact(Filter.PublicationDateFrom, "dd/MM/yyyy", null);
                quizzes = quizzes.Where(a => a.PublicationDate >= dateFrom);
            }

            if (!String.IsNullOrWhiteSpace(Filter.PublicationDateTo))
            {
                DateTime dateTo = DateTime.ParseExact(Filter.PublicationDateTo, "dd/MM/yyyy", null);
                quizzes = quizzes.Where(a => a.PublicationDate <= dateTo);
            }

            if (Filter.Status.HasValue)
                quizzes = quizzes.Where(a => a.IsActive == Filter.Status).AsQueryable();

            if (Filter.Wincointsfrom.HasValue)
                quizzes = quizzes.Where(a => a.Wincoins >= Filter.Wincointsfrom).AsQueryable();

            if (Filter.Wincointsto.HasValue)
                quizzes = quizzes.Where(a => a.Wincoins <= Filter.Wincointsto).AsQueryable();

            return quizzes;
        }

        public IOrderedQueryable<Quizzes> SortData(IQueryable<Quizzes> quizzes, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? quizzes.OrderByDescending(x => x.Id) : quizzes.OrderBy(x => x.Id);
            return quizzes.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
            Filter.Title = Filter.Title.TrimSpaces().StripHtml(false);
        }
    }
}