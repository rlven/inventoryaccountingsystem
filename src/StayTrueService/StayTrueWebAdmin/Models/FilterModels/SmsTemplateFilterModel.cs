﻿using System;
using System.Linq;
using System.Web.Mvc;
using Common.Logger;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class SmsTemplateFilterModel
    {
        public int? Id { get; set; }
        [AllowHtml]
        public string Title { get; set; }
        [AllowHtml]
        public string Text { get; set; }
        public bool IsPasswordRequired { get; set; }
        public string SmsCost { get; set; }
        public int PageSize = 20;

    }

    public class SmsTemplateFilteredModel 
    {
        public SmsTemplateFilterModel Filter;

        public IPagedList<SmsTemplate> SmsTemplates { get; set; }


        public SmsTemplateFilteredModel(SmsTemplateFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<SmsTemplate> smsTemplates, string sortColumn, bool dir, int pageNumber)
        {
            SmsTemplates = SortData(FilterData(smsTemplates), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<SmsTemplate> FilterData(IQueryable<SmsTemplate> smsTemplates)
        {
            if (!string.IsNullOrEmpty(Filter.Title))
                smsTemplates = smsTemplates.Where(x => x.Title.Contains(Filter.Title));
            if (!string.IsNullOrEmpty(Filter.Text))
                smsTemplates = smsTemplates.Where(x => x.Text.Contains(Filter.Text));

            if (Filter.IsPasswordRequired == true)
            {
                smsTemplates = smsTemplates.Where(x => x.IsPasswordRequired == true);
            }

            if (!string.IsNullOrEmpty(Filter.SmsCost))
            {
                try
                {
                    Filter.SmsCost = Filter.SmsCost.Replace('.', ',');
                    double smsCost = double.Parse(Filter.SmsCost);
                    smsTemplates = smsTemplates.Where(x => x.SmsCost == smsCost);
                }
                catch (FormatException ex)
                {
                    Log.Error(ex.Message);
                }
            }

            return smsTemplates;
        }

        public IOrderedQueryable<SmsTemplate> SortData(IQueryable<SmsTemplate> smsTemplates, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? smsTemplates.OrderByDescending(x => x.Id) : smsTemplates.OrderBy(x => x.Id);
            return smsTemplates.OrderByDescending(x => x.Id);
        }

    }
}