﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{


    public class UsersFilterModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? BirthDateFrom { get; set; }

        public DateTime? BirthDateTo { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public int? Status { get; set; }

        public int? City { get; set; }

        public int? StpFrom { get; set; }
        public int? StpTo { get; set; }

        public int? StmFrom { get; set; }
        public int? StmTo { get; set; }

        public DateTime? RegistrationDateFrom { get; set; }

        public DateTime? RegistrationDateTo { get; set; }

        public DateTime? LastSignInFromM { get; set; }

        public DateTime? LastSignInToM { get; set; }

        public DateTime? LastSignInFromP { get; set; }

        public DateTime? LastSignInToP { get; set; }

        public int? WincoinsFrom { get; set; }

        public int? WincoinsTo { get; set; }

        public Boolean? HasPassport { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsAvatarApproved { get; set; }
        public int PageSize = 50;
        public bool IsAllSelected { get; set; } = false;
        public int FilterMode { get; set; } = 0;
        public int? PromoCode { get; set; }
        public int? PromoUserCountFrom { get; set; }
        public int? PromoUsersCountTo { get; set; }
        public string PhoneFilterList { get; set; }
    }

    public class UsersFilteredModel
    {
        public UsersFilterModel Filter;
        public IEnumerable<SelectListItem> Cities { get; set; }
        public int NotApprovedUsersCount { get; set; }

      //  private const String dateTimeFormat = "dd/MM/yyyy HH:mm";

        public UsersFilteredModel(UsersFilterModel filter)
        {
            Filter = filter;
        }
        public IPagedList<Users> Users { get; set; }
        public IEnumerable<SelectListItem> AccountStatuses { get; set; }
        public IEnumerable<Genders> Genders { get; set; }

        public IQueryable<Users> SortData(IQueryable<Users> users, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? users.OrderByDescending(a => a.Id) : users.OrderBy(a => a.Id);
            if (sortColumn == "SignInCounterP") return users.OrderByDescending(x => x.SignInCounterP);
            return users.OrderByDescending(x => x.Id);
        }

        public void ProcessData(IQueryable<Users> users, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Users = SortData(FilterData(users), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }


        public IQueryable<Users> FilterData(IQueryable<Users> users)
        {
            if (!string.IsNullOrEmpty(Filter.PhoneFilterList))
            {
                var numbers = Filter.PhoneFilterList.Split(',');
                var res = (from u in users where numbers.Contains(u.Phone) select u);
                return res;
            }
            if (Filter.FilterMode == 1)
            {
                users = users.Where(u => u.StatusId == 1 && u.SignInCounterP > 0);
            }

            if (Filter.FilterMode == 2)
            {
                users = users.Where(u => u.StatusId == 2);
            }

            if (Filter.FilterMode == 3)
            {
                users = users.Where(u => u.StatusId == 3 && u.IsApproved == false);
            }

            if (!String.IsNullOrWhiteSpace(Filter.FirstName))
                users = users.Where(x => x.FirstName.Contains(Filter.FirstName));
            if (!String.IsNullOrWhiteSpace(Filter.LastName))
                users = users.Where(x => x.LastName.Contains(Filter.LastName));
            if (!String.IsNullOrWhiteSpace(Filter.Email))
                users = users.Where(x => x.Email.Contains(Filter.Email));
            if (Filter.Status.HasValue)
                users = users.Where(a => a.StatusId == Filter.Status).AsQueryable();
            if (Filter.City.HasValue)
                users = users.Where(a => a.CityId == Filter.City).AsQueryable();

            if (Filter.StmFrom.HasValue)
                users = users.Where(a => a.SignInCounterM >= Filter.StmFrom).AsQueryable();

            if (Filter.StmTo.HasValue)
                users = users.Where(a => a.SignInCounterM <= Filter.StmTo).AsQueryable();

            if (Filter.StpFrom.HasValue)
                users = users.Where(a => a.SignInCounterP >= Filter.StpFrom).AsQueryable();

            if (Filter.StpTo.HasValue)
                users = users.Where(a => a.SignInCounterP <= Filter.StpTo).AsQueryable();

            if (!String.IsNullOrWhiteSpace(Filter.PhoneNumber))
                users = users.Where(x => x.Phone.Contains(Filter.PhoneNumber));

            if (Filter.WincoinsFrom.HasValue)
                users = users.Where(a => a.Wincoins >= Filter.WincoinsFrom).AsQueryable();

            if (Filter.WincoinsTo.HasValue)
                users = users.Where(a => a.Wincoins <= Filter.WincoinsTo).AsQueryable();

            //Birth date filter
            if (Filter.BirthDateFrom.HasValue)
            {
                users = users.Where(a => a.BirthDate >= Filter.BirthDateFrom.Value);
            }
            if (Filter.BirthDateTo.HasValue)
            {
                users = users.Where(a => a.BirthDate <= Filter.BirthDateTo.Value);
            }

            //Registration filter
            if (Filter.RegistrationDateTo.HasValue)
            {
                users = users.Where(a => a.RegistrationDate <= Filter.RegistrationDateTo.Value);
            }
            if (Filter.RegistrationDateFrom.HasValue)
            {
                users = users.Where(a => a.RegistrationDate >= Filter.RegistrationDateFrom.Value);
            }

            //Last sign in to M
            if (Filter.LastSignInToM.HasValue)
            {
                users = users.Where(a => a.LastSignInM <= Filter.LastSignInToM.Value);
            }
            if (Filter.LastSignInFromM.HasValue)
            {
                users = users.Where(a => a.LastSignInM >= Filter.LastSignInFromM.Value);
            }

            //Last sign in to P
            if (Filter.LastSignInToP.HasValue)
            {
                users = users.Where(a => a.LastSignInP <= Filter.LastSignInToP.Value);
            }
            if (Filter.LastSignInFromP.HasValue)
            {
               users = users.Where(a => a.LastSignInP >= Filter.LastSignInFromP.Value);
            }

            if (Filter.IsApproved.HasValue)
            {
                users = users.Where(a => a.IsApproved == Filter.IsApproved.Value);
            }

            if (Filter.IsAvatarApproved.HasValue)
            {
                users = users.Where(a => a.IsAvatarApproved == Filter.IsAvatarApproved.Value);
            }
            if (Filter.PromoCode.HasValue)
            {
                users = users.Where(a => a.PromoCode == Filter.PromoCode);
            }
            if (Filter.PromoUserCountFrom.HasValue)
            {
                users = users.Where(a => a.PromoUsersCount >= Filter.PromoUserCountFrom);
            }
            if (Filter.PromoUsersCountTo.HasValue)
            {
                users = users.Where(a => a.PromoUsersCount <= Filter.PromoUsersCountTo);
            }

            return users;
        }

        private void CleanData()
        {
            Filter.FirstName = Filter.FirstName.TrimSpaces().StripHtml(false);
            Filter.LastName = Filter.LastName.TrimSpaces().StripHtml(false);
            Filter.Email = Filter.Email.TrimSpaces().StripHtml(false);
        }
    }

}