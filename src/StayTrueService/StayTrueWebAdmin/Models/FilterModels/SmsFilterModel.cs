﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class SmsFilterModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public int? City { get; set; }
        public int? STPfrom { get; set; }
        public int? STPto { get; set; }
        public int? STMfrom { get; set; }
        public int? STMto { get; set; }
        
        public DateTime? RegistrationDateFrom { get; set; }
        public DateTime? RegistrationDateTo { get; set; }
        public DateTime? LastSignInMFrom { get; set; }
        public DateTime? LastSignInMTo { get; set; }
        public DateTime? LastSignInPFrom { get; set; }
        public DateTime? LastSignInPTo { get; set; }
        public int PageSize = 100;
    }

    public class SmsFilteredModel
    {
        public SmsFilterModel Filter;

        public IPagedList<Users> Users { get; set; }

        public SmsFilteredModel(SmsFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Users> users, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Users = SortData(FilterData(users), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Users> FilterData(IQueryable<Users> users)
        {
            users = users.Where(x => x.StatusId == 1 || x.StatusId == 4);
            if (!string.IsNullOrEmpty(Filter.FirstName))
                users = users.Where(x => x.FirstName.Contains(Filter.FirstName));
            if (!string.IsNullOrEmpty(Filter.LastName))
                users = users.Where(x => x.LastName.Contains(Filter.LastName));
            if (Filter.BirthDateFrom.HasValue )
                users = users.Where(x => x.BirthDate >= Filter.BirthDateFrom);
            if (Filter.BirthDateTo.HasValue)
                users = users.Where(x => x.BirthDate <= Filter.BirthDateTo);
            if (!string.IsNullOrEmpty(Filter.Email))
                users = users.Where(x => x.Email.Contains(Filter.Email));
            if (Filter.Status.HasValue)
                users = users.Where(x => x.StatusId == Filter.Status);
            if (Filter.City.HasValue)
                users = users.Where(x => x.CityId == Filter.City);
            if (Filter.STPfrom.HasValue)
                users = users.Where(x => x.SignInCounterP >= Filter.STPfrom);
            if (Filter.STPto.HasValue)
                users = users.Where(x => x.SignInCounterP <= Filter.STPto);
            if (Filter.STMfrom.HasValue)
                users = users.Where(x => x.SignInCounterM >= Filter.STMfrom);
            if (Filter.STMto.HasValue)
                users = users.Where(x => x.SignInCounterM <= Filter.STMto);
            if (Filter.RegistrationDateFrom.HasValue)
                users = users.Where(x => x.RegistrationDate >= Filter.RegistrationDateFrom);
            if (Filter.RegistrationDateTo.HasValue)
                users = users.Where(x => x.RegistrationDate <= Filter.RegistrationDateTo);
            if (Filter.LastSignInMFrom.HasValue)
                users = users.Where(x => x.LastSignInM >= Filter.LastSignInMFrom);
            if (Filter.LastSignInMTo.HasValue)
                users = users.Where(x => x.LastSignInM <= Filter.LastSignInMTo);
            if (Filter.LastSignInPFrom.HasValue)
                users = users.Where(x => x.LastSignInP >= Filter.LastSignInPFrom);
            if (Filter.LastSignInPTo.HasValue)
                users = users.Where(x => x.LastSignInP == Filter.LastSignInPTo);
            return users;
        }

        public IOrderedQueryable<Users> SortData(IQueryable<Users> users, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? users.OrderByDescending(x => x.Id) : users.OrderBy(x => x.Id);
            return users.OrderByDescending(x => x.Id);
        }

        private void CleanData()
        {
            Filter.FirstName = Filter.FirstName.TrimSpaces().StripHtml(false);
            Filter.LastName = Filter.LastName.TrimSpaces().StripHtml(false);
            Filter.Email = Filter.Email.TrimSpaces().StripHtml(false);
        }
    }

    public class SmsFilteredViewModel
    {
        public SmsFilterModel Filter;

        public List<Users> Users;
    }
}