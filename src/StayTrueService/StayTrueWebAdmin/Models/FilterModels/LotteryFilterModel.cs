﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class LotteryFilterModel
    {
        public int? Id { get; set; }
        public int? AvailableTickets { get; set; }
        public string Name { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? TicketPrice { get; set; }
        public int? MaxTicketsForUser { get; set; }
        public bool? IsActive { get; set; }
        public int PageSize = 5;
    }

    public class LotteryFilteredModel
    {
        public LotteryFilterModel Filter;

        public IPagedList<Lottery> Lotteries { get; set; }

        public LotteryFilteredModel(LotteryFilterModel filter)
        {
            Filter = filter;
        }
        public void ProcessData(IQueryable<Lottery> loteries, string sortColumn, bool dir, int pageNumber)
        {
            Lotteries = SortData(FilterData(loteries), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Lottery> FilterData(IQueryable<Lottery> loteries)
        {

            return loteries;
        }

        public IOrderedQueryable<Lottery> SortData(IQueryable<Lottery> loteries, string sortColumn, bool dir)
        {
            if (sortColumn == "IsActive") return dir ? loteries.OrderByDescending(a => a.IsActive) : loteries.OrderBy(a => a.IsActive);
            return loteries.OrderByDescending(a => a.IsActive);
        }

    }
}
