﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class GamesFilterModel
    {
        public string PublicationDateFrom { get; set; }
        public string PublicationDateTo { get; set; }
        public string Title { get; set; }
        public int? Wincointsfrom { get; set; }
        public int? Wincointsto { get; set; }
        public bool? Status { get; set; }
        public int PageSize = 10;
    }

    public class GamesFilteredModel
    {
        public GamesFilterModel Filter;

        public IPagedList<Games> Games { get; set; }

        public GamesFilteredModel(GamesFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Games> games, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Games = SortData(FilterData(games), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Games> FilterData(IQueryable<Games> games)
        {
            if (!string.IsNullOrEmpty(Filter.Title))
                games = games.Where(x => x.Name.Contains(Filter.Title));

            if (!String.IsNullOrWhiteSpace(Filter.PublicationDateFrom))
            {
                DateTime dateFrom = DateTime.ParseExact(Filter.PublicationDateFrom, "dd/MM/yyyy", null);
                games = games.Where(a => a.PublicationDate >= dateFrom);
            }

            if (!String.IsNullOrWhiteSpace(Filter.PublicationDateTo))
            {
                DateTime dateTo = DateTime.ParseExact(Filter.PublicationDateTo, "dd/MM/yyyy", null);
                games = games.Where(a => a.PublicationDate <= dateTo);
            }

            if (Filter.Status.HasValue)
                games = games.Where(a => a.IsActive == Filter.Status).AsQueryable();

            if (Filter.Wincointsfrom.HasValue)
                games = games.Where(a => a.Wincoins >= Filter.Wincointsfrom).AsQueryable();

            if (Filter.Wincointsto.HasValue)
                games = games.Where(a => a.Wincoins <= Filter.Wincointsto).AsQueryable();

            return games;
        }

        public IOrderedQueryable<Games> SortData(IQueryable<Games> games, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? games.OrderByDescending(x => x.Id) : games.OrderBy(x => x.Id);
            return games.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
            Filter.Title = Filter.Title.TrimSpaces().StripHtml(false);
        }
    }
}