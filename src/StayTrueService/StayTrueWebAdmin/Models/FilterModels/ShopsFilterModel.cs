﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class ProductsFilterModel
    {
        public String DateFrom { get; set; }
        public String DateTo { get; set; }
        public int ShopId { get; set; }
        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        public int AmountFrom { get; set; }
        public int AmountTo { get; set; }
        public int AvailableAmountFrom { get; set; }
        public int AvailableAmountTo { get; set; }
        
        public bool? IsAvailable { get; set; }

        public int PageSize = 10;
    }

    public class ProductsFilteredModel
    {
        public ProductsFilterModel Filter;
        public IEnumerable<SelectListItem> Shops { get; set; }
        public IPagedList<Products> Products { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }

        public ProductsFilteredModel(ProductsFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Products> albums, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Products = SortData(FilterData(albums), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Products> FilterData(IQueryable<Products> products)
        {

            if (!String.IsNullOrWhiteSpace(Filter.DateFrom))
            {
                DateTime dateFrom = DateTime.ParseExact(Filter.DateFrom, "dd/MM/yyyy hh:mm", null);
                products = products.Where(a => a.CreationDate >= dateFrom);
            }

            if (!String.IsNullOrWhiteSpace(Filter.DateTo))
            {
                DateTime dateTo = DateTime.ParseExact(Filter.DateTo, "dd/MM/yyyy hh:mm", null);
                products = products.Where(a => a.CreationDate <= dateTo);
            }

            if (Filter.AmountFrom != 0)
                products = products.Where(a => a.Amount >= Filter.AmountFrom);

            if (Filter.AmountTo != 0)
                products = products.Where(a => a.Amount <= Filter.AmountTo);

            if (Filter.PriceFrom != 0)
                products = products.Where(a => a.Price >= Filter.PriceFrom);

            if (Filter.PriceTo != 0)
                products = products.Where(a => a.Price <= Filter.PriceTo);

            if (Filter.AvailableAmountFrom != 0)
                products = products.Where(a => a.AvailableAmount >= Filter.AvailableAmountFrom);

            if (Filter.AvailableAmountTo != 0)
                products = products.Where(a => a.AvailableAmount <= Filter.AvailableAmountTo);

            if (Filter.ShopId != 0)
                products = products.Where(a => a.ShopId == Filter.ShopId);

            if (Filter.IsAvailable.HasValue)
            {
                products = products.Where(x => x.IsAvailable == Filter.IsAvailable).AsQueryable();
            }
            products = products.Where(x => x.IsDeleted == false);

            return products;
        }

        public IOrderedQueryable<Products> SortData(IQueryable<Products> products, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? products.OrderByDescending(a => a.Id) : products.OrderBy(a => a.Id);
            return products.OrderByDescending(a => a.Id);
        }

        private void CleanData()
        {
            
        }
    }


}