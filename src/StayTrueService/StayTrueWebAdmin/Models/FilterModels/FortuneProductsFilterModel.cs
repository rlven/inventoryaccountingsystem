﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class FortuneProductsFilterModel
    {
        public int ProductId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Name { get; set; }
        public int? CostFrom { get; set; }
        public int? CostTo { get; set; }
        public byte? Type { get; set; }
        public int? QuantityFrom { get; set; }
        public int? QuantityTo { get; set; }
        public int PageSize = 10;
    }

    public class FortuneProductsFilteredModel
    {
        public FortuneProductsFilterModel Filter { get; set; }
        public IPagedList<FortuneProducts> Products { get; set; }

        public FortuneProductsFilteredModel(FortuneProductsFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<FortuneProducts> products, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Products = SortData(FilterData(products), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IOrderedQueryable<FortuneProducts> SortData(IQueryable<FortuneProducts> products, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? products.OrderByDescending(a => a.Id) : products.OrderBy(a => a.Id);
            return products.OrderByDescending(a => a.Id);
        }

        public IQueryable<FortuneProducts> FilterData(IQueryable<FortuneProducts> products)
        {
            if (Filter.DateFrom.HasValue)
            {
                products = products.Where(a => a.CreationDate >= Filter.DateFrom);
            }

            if (Filter.DateTo.HasValue)
            {
                products = products.Where(a => a.CreationDate <= Filter.DateTo);
            }

            if (Filter.CostFrom.HasValue)
            {
                products = products.Where(a => a.Cost >= Filter.CostFrom);
            }

            if (Filter.CostTo.HasValue)
            {
                products = products.Where(a => a.Cost <= Filter.CostTo);
            }

            if (!string.IsNullOrEmpty(Filter.Name))
            {
                products = products.Where(a => a.Name.Contains(Filter.Name));
            }

            if (Filter.QuantityFrom.HasValue)
            {
                products = products.Where(a => a.Quantity >= Filter.QuantityFrom);
            }

            if (Filter.QuantityTo.HasValue)
            {
                products = products.Where(a => a.Quantity <= Filter.QuantityTo);
            }

            if (Filter.Type.HasValue)
            {
                products = products.Where(a => a.Type == Filter.Type);
            }
            return products;
        }

        public void CleanData()
        {
            Filter.Name = Filter.Name.TrimSpaces().StripHtml(false);
        }
    }
}