﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class LotteryMembersFilterModel
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public string Email { get; set; }
        public int? CityId { get; set; }
        public DateTime? RegistrationDateFrom { get; set; }
        public DateTime? RegistrationDateTo { get; set; }
        public string Phone { get; set; }
        public int? WincoinsFrom { get; set; }
        public int? WincoinsTo { get; set; }
        public int? TicketsCountFrom { get; set; }
        public int? TicketsCountTo { get; set; }
        public int? LotteryId { get; set; }
        public int PageSize = 10;
    }

    public class LotteryMembersFilteredModel
    {
        public LotteryMembersFilterModel Filter;

        public IEnumerable<SelectListItem> Cities { get; set; }

        public IPagedList<LotteryUsers> Lotteries { get; set; }

        public List<LotteryUsers> LotteryUserses { get; set; }    

        public LotteryMembersFilteredModel(LotteryMembersFilterModel filter)
        {
            Filter = filter;
        }
        public void ProcessData(IQueryable<LotteryUsers> loteries, string sortColumn, bool dir, int pageNumber)
        {
            Lotteries = SortData(FilterData(loteries), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public void ProcessFilterData(IQueryable<LotteryUsers> loteries, string sortColumn, bool dir)
        {
            LotteryUserses = SortData(FilterData(loteries), sortColumn, dir).ToList();
        }

        public IQueryable<LotteryUsers> FilterData(IQueryable<LotteryUsers> loteries)
        {
            if (!string.IsNullOrEmpty(Filter.FirstName))
            {
                loteries = loteries.Where(l => l.Users.FirstName.Contains(Filter.FirstName));
            }
            if (!string.IsNullOrEmpty(Filter.LastName))
            {
                loteries = loteries.Where(l => l.Users.FirstName.Contains(Filter.LastName));
            }
            if (!string.IsNullOrEmpty(Filter.Email))
            {
                loteries = loteries.Where(l => l.Users.FirstName.Contains(Filter.Email));
            }
            if (Filter.BirthDateFrom.HasValue)
            {
                loteries = loteries.Where(l => l.Users.BirthDate>Filter.BirthDateFrom);
            }
            if (Filter.BirthDateTo.HasValue)
            {
                loteries = loteries.Where(l => l.Users.BirthDate < Filter.BirthDateTo);
            }
            if (Filter.RegistrationDateFrom.HasValue)
            {
                loteries = loteries.Where(l => l.Users.RegistrationDate > Filter.RegistrationDateFrom);
            }
            if (Filter.RegistrationDateTo.HasValue)
            {
                loteries = loteries.Where(l => l.Users.RegistrationDate < Filter.RegistrationDateTo);
            }
            if (Filter.CityId.HasValue)
            {
                loteries = loteries.Where(l => l.Users.CityId == Filter.CityId);
            }
            if (!string.IsNullOrEmpty(Filter.Phone))
            {
                loteries = loteries.Where(l => l.Users.Phone.Contains(Filter.Phone));
            }
            if (Filter.WincoinsFrom.HasValue)
            {
                loteries = loteries.Where(l => l.Users.Wincoins > Filter.WincoinsFrom);
            }
            if (Filter.WincoinsTo.HasValue)
            {
                loteries = loteries.Where(l => l.Users.Wincoins < Filter.WincoinsTo);
            }
            if (Filter.TicketsCountFrom.HasValue)
            {
                loteries = loteries.Where(l => l.TicketsCount > Filter.TicketsCountFrom);
            }
            if (Filter.TicketsCountTo.HasValue)
            {
                loteries = loteries.Where(l => l.TicketsCount < Filter.TicketsCountTo);
            }
            return loteries;
        }

        public IOrderedQueryable<LotteryUsers> SortData(IQueryable<LotteryUsers> loteries, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? loteries.OrderByDescending(a => a.Id) : loteries.OrderBy(a => a.Id);
            return loteries.OrderByDescending(a => a.Id);
        }
    }
}