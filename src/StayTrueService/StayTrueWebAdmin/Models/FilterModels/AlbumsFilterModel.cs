﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class AlbumsFilterModel
    {
        public string Name { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public bool? IsActive { get; set; }

        public int PageSize = 10;
    }

    public class AlbumsFilteredModel
    {
        public AlbumsFilterModel Filter;

        public IPagedList<Albums> Albums { get; set; }


        public AlbumsFilteredModel(AlbumsFilterModel filter)
        {
            Filter = filter;
        }
        public void ProcessData(IQueryable<Albums> albums, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Albums = SortData(FilterData(albums), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Albums> FilterData(IQueryable<Albums> albums)
        {
             if (!String.IsNullOrEmpty(Filter.Name))
                albums = albums.Where(a => a.Name.Contains(Filter.Name));
            if (Filter.DateFrom.HasValue)
                albums = albums.Where(a => a.CreationDate >= Filter.DateFrom.Value);
            if (Filter.DateTo.HasValue)
                albums = albums.Where(a => a.CreationDate < Filter.DateTo.Value);
            if (Filter.IsActive.HasValue)
            {
                albums = albums.Where(x=>x.IsActive == Filter.IsActive.Value).AsQueryable();
            }
            return albums;
        }

        public IOrderedQueryable<Albums> SortData(IQueryable<Albums> albums, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir? albums.OrderByDescending(a => a.Id) : albums.OrderBy(a => a.Id);
            return albums.OrderByDescending(a => a.Id);
        }

        private void CleanData()
        {
            Filter.Name = Filter.Name.TrimSpaces().StripHtml(false);
        }
    }

}