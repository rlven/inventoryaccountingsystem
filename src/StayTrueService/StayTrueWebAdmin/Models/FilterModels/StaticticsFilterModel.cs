﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class StaticticsFilterModel
    {
        public DateTime? NewUsersFromDate { get; set; }
        public DateTime? NewUsersToDate { get; set; }
        public int? Gender { get; set; }
        public bool? PasportStatus { get; set; }
        public int? Status { get; set; }
        public int? City { get; set; }
        public int? AgeRange { get; set; }
        public int PageSize = 10;
    }

    public class StaticticsFilteredModel
    {
        public StaticticsFilterModel Filter;

        public IQueryable<Users> Users { get; set; }
        public IEnumerable<SelectListItem> AccountStatuses { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }
        public IEnumerable<SelectListItem> Genders { get; set; }
        public IEnumerable<SelectListItem> Age { get; set; }

        public StaticticsFilteredModel(StaticticsFilterModel filter)
        {
            Filter = filter;
        }
        public void ProcessData(IQueryable<Users> users, string sortColumn, bool dir)
        {
            Users = SortData(FilterData(users), sortColumn, dir);
        }

        public IQueryable<Users> FilterData(IQueryable<Users> users)
        {

            if (Filter.NewUsersFromDate.HasValue)
            {

                users = users.Where(a => a.RegistrationDate >= Filter.NewUsersFromDate.Value);
            }

            if (Filter.NewUsersToDate.HasValue)
            {
                users = users.Where(a => a.RegistrationDate <= Filter.NewUsersToDate.Value);
            }

            if (Filter.AgeRange.HasValue)
            {
                if(Filter.AgeRange.Value==1)
                    users = users.Where(x => DbFunctions.DiffYears(x.BirthDate, DateTime.Now) >= 18 &&
                                         DbFunctions.DiffYears(x.BirthDate, DateTime.Now) < 25).AsQueryable();
                if (Filter.AgeRange.Value == 2)
                    users = users.Where(x => DbFunctions.DiffYears(x.BirthDate, DateTime.Now) >= 25 &&
                                             DbFunctions.DiffYears(x.BirthDate, DateTime.Now) < 40).AsQueryable();
                if (Filter.AgeRange.Value == 3)
                    users = users.Where(x => DbFunctions.DiffYears(x.BirthDate, DateTime.Now) >= 40).AsQueryable();
            }

            if (Filter.City.HasValue)
                users = users.Where(a => a.CityId == Filter.City.Value).AsQueryable();

            if (Filter.PasportStatus.HasValue)
                users = Filter.PasportStatus.Value 
                    ? users.Where(a => a.PassportBack!=null && a.PassportFront!=null).AsQueryable() 
                    : users.Where(a => a.PassportBack == null || a.PassportFront == null).AsQueryable();


            if (Filter.Gender.HasValue)
                users = users.Where(a => a.GenderId == Filter.Gender).AsQueryable();

            if (Filter.Status.HasValue)
                users = users.Where(a => a.StatusId == Filter.Status).AsQueryable();


            return users;
        }

        public IOrderedQueryable<Users> SortData(IQueryable<Users> users, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? users.OrderByDescending(a => a.Id) : users.OrderBy(a => a.Id);
            return users.OrderByDescending(a => a.Id);
        }

    }
}


