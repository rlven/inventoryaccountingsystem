﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;
using static System.String;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class CigaretteBrandsFilterModel
    {
        public string BrandName { get; set; }
        public DateTime? CreationDateFrom { get; set; }
        public DateTime? CreationDateTo { get; set; }
        public int? BrandPositionFrom { get; set; }
        public int? BrandPositionTo { get; set; }
        public bool? IsActive { get; set; }

        public int PageSize = 10;
    }

    public class CigaretteBrandsFilteredModel
    {
        public CigaretteBrandsFilterModel Filter;

        public IPagedList<CigaretteBrands> Brands { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }

        public CigaretteBrandsFilteredModel(CigaretteBrandsFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<CigaretteBrands> brands, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Brands = SortData(FilterData(brands), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<CigaretteBrands> FilterData(IQueryable<CigaretteBrands> brands)
        {
            if (!IsNullOrWhiteSpace(Filter.BrandName))
            {
                brands = brands.Where(b => b.BrandName.Contains(Filter.BrandName)).AsQueryable();
            }

            if (Filter.CreationDateFrom.HasValue)
            {
                brands = brands.Where(b => b.CreationDate >= Filter.CreationDateFrom.Value).AsQueryable();
            }

            if (Filter.CreationDateTo.HasValue)
            {
                brands = brands.Where(b => b.CreationDate <= Filter.CreationDateTo.Value).AsQueryable();
            }
            
            if (Filter.BrandPositionFrom.HasValue)
            {
                brands = brands.Where(b => b.BrandPosition >= Filter.BrandPositionFrom).AsQueryable();
            }

            if (Filter.BrandPositionTo.HasValue)
            {
                brands = brands.Where(b => b.BrandPosition <= Filter.BrandPositionTo).AsQueryable();
            }

            if (Filter.IsActive.HasValue)
            {
                brands = brands.Where(b => b.IsActive == Filter.IsActive);
            }

            return brands;
        }

        public IOrderedQueryable<CigaretteBrands> SortData(IQueryable<CigaretteBrands> brands, string sortColumn, bool dir)
        {
            if (sortColumn == "BrandPosition")
            {
                return dir ? brands.OrderByDescending(b => b.BrandPosition) : brands.OrderBy(b => b.BrandPosition);
            }
            return brands.OrderByDescending(b=>b.BrandPosition);
        }

        private void CleanData()
        {
            Filter.BrandName = Filter.BrandName.TrimSpaces().StripHtml(false);
        }
    }
}