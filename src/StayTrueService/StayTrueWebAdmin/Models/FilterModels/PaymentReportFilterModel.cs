﻿using PagedList;
using StayTrueWebAdmin.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class PaymentReportFilterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public DateTime DateFrom { get; set; } = Convert.ToDateTime("2019.05.01");
        public DateTime DateTo { get; set; } = DateTime.Now;
        public int? AmountFrom { get; set; }
        public int? AmountTo { get; set; }
        public string Status { get; set; }
        public int PageSize = 20;
    }
    public class PaymentReportFilteredModel 
    {
        public PaymentReportFilterModel Filter;
        public IPagedList<PaymentReport> Reports { get; set; }

        public PaymentReportFilteredModel(PaymentReportFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<PaymentReport> reports, int pageNumber)
        {
            Reports = FilterData(reports).ToPagedList(pageNumber,Filter.PageSize);
        }

        public IQueryable<PaymentReport> FilterData(IQueryable<PaymentReport> reports)
        {
            if (!string.IsNullOrEmpty(Filter.FirstName))
                reports = reports.Where(x => x.FirstName.Contains(Filter.FirstName));
            if (!string.IsNullOrEmpty(Filter.LastName))
                reports = reports.Where(x => x.LastName.Contains(Filter.LastName));
            if (!string.IsNullOrEmpty(Filter.Phone))
                reports = reports.Where(x => x.Phone.Contains(Filter.Phone));
            if (Filter.AmountFrom.HasValue)
                reports = reports.Where(x => x.Amount > Filter.AmountFrom);
            if (Filter.AmountTo.HasValue)
                reports = reports.Where(x => x.Amount < Filter.AmountTo);
            if (!string.IsNullOrEmpty(Filter.Status))
                reports = reports.Where(x => x.Status.Contains(Filter.Status));

            return reports;
        }
    }
}