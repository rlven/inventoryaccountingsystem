﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class FortuneWheelFilterModel
    {
        public string Name { get; set; }
        public int? BetFrom { get; set; }
        public int? BetTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int PageSize = 10;
    }

    public class FortuneWheelFilteredModel
    {
        public FortuneWheelFilterModel Filter { get; set; }
        public IPagedList<FortuneWheel> Wheels { get; set; }

        public FortuneWheelFilteredModel(FortuneWheelFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<FortuneWheel> wheel, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Wheels = SortData(FilterData(wheel), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IOrderedQueryable<FortuneWheel> SortData(IQueryable<FortuneWheel> products, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? products.OrderByDescending(a => a.Id) : products.OrderBy(a => a.Id);
            return products.OrderByDescending(a => a.Id);
        }

        public IQueryable<FortuneWheel> FilterData(IQueryable<FortuneWheel> wheel)
        {
            if (Filter.DateFrom.HasValue)
            {
                wheel = wheel.Where(a => a.CreationTime >= Filter.DateFrom);
            }

            if (Filter.DateTo.HasValue)
            {
                wheel = wheel.Where(a => a.CreationTime <= Filter.DateFrom);
            }

            if (Filter.BetFrom.HasValue)
            {
                wheel = wheel.Where(a => a.Bet >= Filter.BetFrom);
            }

            if (Filter.BetTo.HasValue)
            {
                wheel = wheel.Where(a => a.Bet <= Filter.BetTo);
            }
            
            if (!string.IsNullOrEmpty(Filter.Name))
            {
                wheel = wheel.Where(a => a.Name.Contains(Filter.Name));
            }

            return wheel;
        }

        public void CleanData()
        {
            Filter.Name = Filter.Name.TrimSpaces().StripHtml(false);
        }
    }
}