﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class PartyFilterModel
    {
        public int PageSize = 10;
    }

    public class PartyFilteredModel
    {
        public PartyFilterModel Filter;

        public IPagedList<Party> Parties { get; set; }
        public PartyGames PartyGames { get; set; }

        public PartyFilteredModel(PartyFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Party> parties, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Parties = SortData(FilterData(parties), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Party> FilterData(IQueryable<Party> news)
        {
            return news;
        }

        public IOrderedQueryable<Party> SortData(IQueryable<Party> parties, string sortColumn,
            bool dir)
        {
            if (sortColumn == "ID")
                return dir ? parties.OrderByDescending(a => a.Id) : parties.OrderBy(a => a.Id);
            return parties.OrderByDescending(a => a.Id);
        }

        private void CleanData()
        {
        }
    }
}