﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Common.Enums;
using DataAccessLayer;
using Microsoft.Ajax.Utilities;
using PagedList;
using StayTrueWebAdmin.Extentions;
using StayTrueWebAdmin.Models.ViewModels;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class GreatBattlesFilterModel
    {
        public int PageSize = 10;
    }

    public class GreatBattlesFilteredModel
    {
        public GreatBattlesFilterModel Filter;

        public IPagedList<GreatBattles> GreatBattles { get; set; }

        public GreatBattlesFilteredModel(GreatBattlesFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<GreatBattles> greatBattles, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            GreatBattles = SortData(FilterData(greatBattles), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<GreatBattles> FilterData(IQueryable<GreatBattles> news)
        {
            return news;
        }

        public IOrderedQueryable<GreatBattles> SortData(IQueryable<GreatBattles> greatBattles, string sortColumn,
            bool dir)
        {
            if (sortColumn == "ID")
                return dir ? greatBattles.OrderByDescending(a => a.ID) : greatBattles.OrderBy(a => a.ID);
            return greatBattles.OrderByDescending(a => a.ID);
        }

        private void CleanData()
        {
        }
    }
}