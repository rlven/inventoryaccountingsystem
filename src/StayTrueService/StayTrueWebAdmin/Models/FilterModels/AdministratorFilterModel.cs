﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class AdministratorFilterModel
    {
       
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Login { get; set; }
            public string PhoneNumber { get; set; }
            public string Email { get; set; }
            public int? RoleId { get; set; }
            
           // public DateTime? DateFrom { get; set; }
           // public DateTime? DateTo { get; set; }

          //  public bool? IsActive { get; set; }

            public int PageSize = 10;
        }

        public class AdministratorFilteredModel
    {
            public AdministratorFilterModel Filter;

            public IPagedList<Administrators> Administrators { get; set; }

        public IEnumerable<SelectListItem> Roles { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }

        public AdministratorFilteredModel(AdministratorFilterModel filter)
            {
                Filter = filter;
            }
            public void ProcessData(IQueryable<Administrators> administrators, string sortColumn, bool dir, int pageNumber)
            {
                CleanData();
                Administrators = SortData(FilterData(administrators), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
            }

        public IQueryable<Administrators> FilterData(IQueryable<Administrators> administrators)
        {
            if (!String.IsNullOrEmpty(Filter.FirstName))
                administrators = administrators.Where(a => a.FirstName.Contains(Filter.FirstName));
            if (!String.IsNullOrEmpty(Filter.LastName))
                administrators = administrators.Where(a => a.LastName.Contains(Filter.LastName));
            if (!String.IsNullOrEmpty(Filter.Login))
                administrators = administrators.Where(a => a.Login.Contains(Filter.Login));
            if (!String.IsNullOrEmpty(Filter.PhoneNumber))
                administrators = administrators.Where(a => a.PhoneNumber.Contains(Filter.PhoneNumber));
            if (!String.IsNullOrEmpty(Filter.Email))
                administrators = administrators.Where(a => a.Email.Contains(Filter.Email));
            if (Filter.RoleId.HasValue)
                administrators = administrators.Where(a => a.Role == Filter.RoleId);
            administrators.AsQueryable();
            return administrators;
        }

        public IOrderedQueryable<Administrators> SortData(IQueryable<Administrators> administrators, string sortColumn, bool dir)
            {
                if (sortColumn == "ID") return dir ? administrators.OrderByDescending(a => a.Id) : administrators.OrderBy(a => a.Id);
                return administrators.OrderByDescending(a => a.Id);
            }

            private void CleanData()
            {
                Filter.FirstName = Filter.FirstName.TrimSpaces().StripHtml(false);
                Filter.LastName = Filter.LastName.TrimSpaces().StripHtml(false);
                Filter.Login = Filter.Login.TrimSpaces().StripHtml(false);
                Filter.PhoneNumber = Filter.PhoneNumber.TrimSpaces().StripHtml(false);
        }
        }
}
