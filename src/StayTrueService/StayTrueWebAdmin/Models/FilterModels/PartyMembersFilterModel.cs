﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class PartyMembersFilterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public string City { get; set; }
        public DateTime? LastSignInFrom { get; set; }
        public DateTime? LastSignInTo { get; set; }
        public string Phone { get; set; }
        public int? PointsFrom { get; set; }
        public int? PointsTo { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string Answer5 { get; set; }
        public string Answer6 { get; set; }
        public bool? IsBlocked { get; set; }
        public bool? IsInvited { get; set; }
        public bool? HasCome { get; set; }
        public bool IsAllSelected { get; set; } = false;
        public int PageSize = 10;
    }

    public class PartyMembersFilteredModel
    {
        public PartyMembersFilterModel Filter;
        public IPagedList<PartyUsers> PartyMembers { get; set; }

        public PartyMembersFilteredModel(PartyMembersFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<PartyUsers> partyMembers, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            PartyMembers = SortData(FilterData(partyMembers), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<PartyUsers> FilterData(IQueryable<PartyUsers> partyMembers)
        {
            if (!string.IsNullOrWhiteSpace(Filter.FirstName))
            {
                partyMembers = partyMembers.Where(u => u.Users.FirstName.Contains(Filter.FirstName));
            }

            if (!string.IsNullOrWhiteSpace(Filter.LastName))
            {
                partyMembers = partyMembers.Where(u => u.Users.LastName.Contains(Filter.LastName));
            }

            if (Filter.BirthDateFrom.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.Users.BirthDate >= Filter.BirthDateFrom.Value);
            }

            if (Filter.BirthDateTo.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.Users.BirthDate <= Filter.BirthDateTo.Value);
            }

            if (!string.IsNullOrWhiteSpace(Filter.City))
            {
                partyMembers = partyMembers.Where(u => u.Users.Cities.CityName.Contains(Filter.City));
            }

            if (Filter.LastSignInFrom.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.Users.LastSignInP >= Filter.LastSignInFrom.Value);
            }

            if (Filter.LastSignInTo.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.Users.LastSignInP <= Filter.LastSignInTo.Value);
            }

            if (!string.IsNullOrWhiteSpace(Filter.Phone))
            {
                partyMembers = partyMembers.Where(u => u.Users.Phone.Contains(Filter.Phone));
            }

            if (Filter.PointsFrom.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.Points >= Filter.PointsFrom.Value);
            }

            if (Filter.PointsTo.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.Points <= Filter.PointsTo.Value);
            }

            if (!string.IsNullOrWhiteSpace(Filter.Answer1))
            {
                partyMembers = partyMembers.Where(u => u.Users.PartyQuestionnaireAnswer.FirstOrDefault(q=>q.PartyQuestionnaire.PartyId == u.PartyId).Answer1.Contains(Filter.Answer1));
            }

            if (!string.IsNullOrWhiteSpace(Filter.Answer2))
            {
                partyMembers = partyMembers.Where(u => u.Users.PartyQuestionnaireAnswer.FirstOrDefault(q => q.PartyQuestionnaire.PartyId == u.PartyId).Answer2.Contains(Filter.Answer2));
            }

            if (!string.IsNullOrWhiteSpace(Filter.Answer3))
            {
                partyMembers = partyMembers.Where(u => u.Users.PartyQuestionnaireAnswer.FirstOrDefault(q => q.PartyQuestionnaire.PartyId == u.PartyId).Answer3.Contains(Filter.Answer3));
            }

            if (!string.IsNullOrWhiteSpace(Filter.Answer4))
            {
                partyMembers = partyMembers.Where(u => u.Users.PartyQuestionnaireAnswer.FirstOrDefault(q => q.PartyQuestionnaire.PartyId == u.PartyId).Answer4.Contains(Filter.Answer4));
            }

            if (!string.IsNullOrWhiteSpace(Filter.Answer5))
            {
                partyMembers = partyMembers.Where(u => u.Users.PartyQuestionnaireAnswer.FirstOrDefault(q => q.PartyQuestionnaire.PartyId == u.PartyId).Answer5.Contains(Filter.Answer5));
            }

            if (!string.IsNullOrWhiteSpace(Filter.Answer6))
            {
                partyMembers = partyMembers.Where(u => u.Users.PartyQuestionnaireAnswer.FirstOrDefault(q => q.PartyQuestionnaire.PartyId == u.PartyId).Answer6.Contains(Filter.Answer6));
            }

            if (Filter.IsInvited.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.IsInvited == Filter.IsInvited);
            }

            if (Filter.HasCome.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.HasCome == Filter.HasCome);
            }

            if (Filter.IsBlocked.HasValue)
            {
                partyMembers = partyMembers.Where(u => u.IsBlocked == Filter.IsBlocked);
            }

            return partyMembers;
        }

        public IOrderedQueryable<PartyUsers> SortData(IQueryable<PartyUsers> partyMembers, string sortColumn, bool dir)
        {
            if (sortColumn == "Id") return dir ? partyMembers.OrderByDescending(x => x.Id) : partyMembers.OrderBy(x => x.Id);
            return partyMembers.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
            Filter.FirstName = Filter.FirstName.TrimSpaces().StripHtml(false);
        }
    }
}