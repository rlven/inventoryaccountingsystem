﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class QuizzAnswerFilterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public bool? IsIssued { get; set; }
        public string Phone { get; set; }
        public int PageSize = 20;
    }

    public class QuizzAnswerFilteredModel
    {
        public QuizzAnswerFilterModel Filter;

        public IPagedList<UserAnswer> Quizzes { get; set; }
        public IQueryable<UserAnswer> QuizzesFilter { get; set; }
        public List<UserAnswer> Answers { get; set; }

        public SelectList Statuses { get; set; }
        public SelectList StatusesList { get; set; }
        public int AnswersCount { get; set; } 

        public QuizzAnswerFilteredModel(QuizzAnswerFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<UserAnswer> quizzes, string sortColumn, bool dir, int pageNumber)
        {
            QuizzesFilter = SortData(FilterData(quizzes), sortColumn, dir);
            AnswersCount = QuizzesFilter.Count();
            Quizzes = QuizzesFilter.ToPagedList(pageNumber, Filter.PageSize);
        }

        public void ProcessFilterData(IQueryable<UserAnswer> quizzes, string sortColumn, bool dir)
        {
            Answers = SortData(FilterData(quizzes), sortColumn, dir).ToList();
        }

        public IQueryable<UserAnswer> FilterData(IQueryable<UserAnswer> quizzes)
        {
            if (!string.IsNullOrEmpty(Filter.Phone))
                quizzes = quizzes.Where(x => x.Phone.Contains(Filter.Phone));
            if (!string.IsNullOrEmpty(Filter.FirstName))
                quizzes = quizzes.Where(x => x.FirstName.Contains(Filter.FirstName));
            if (!string.IsNullOrEmpty(Filter.LastName))
                quizzes = quizzes.Where(x => x.LastName.Contains(Filter.LastName));
            if (Filter.DateFrom.HasValue)
                quizzes = quizzes.Where(x => x.Date > Filter.DateFrom);
            if (Filter.DateTo.HasValue)
                quizzes = quizzes.Where(x => x.Date < Filter.DateTo);
            if(Filter.IsIssued.HasValue)
                quizzes = quizzes.Where(x => x.IsIssued== Filter.IsIssued);
            return quizzes;
        }

        public IOrderedQueryable<UserAnswer> SortData(IQueryable<UserAnswer> quizzes, string sortColumn, bool dir)
        {
            if (sortColumn == "Date") return dir ? quizzes.OrderByDescending(x => x.Date) : quizzes.OrderBy(x => x.Date);
            return quizzes.OrderByDescending(x => x.Id);
        }
    }
}