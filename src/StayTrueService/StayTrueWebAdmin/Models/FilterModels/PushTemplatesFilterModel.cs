﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class PushTemplatesFilterModel
    {
        public string Name { get; set; }
        public DateTime? CreationDateFrom { get; set; }
        public DateTime? CreationDateTo { get; set; }
        public int PageSize = 10;
    }

    public class PushTemplatesFilteredModel
    {
        public PushTemplatesFilterModel Filter;
        public IPagedList<PushTemplates> PushTemplates { get; set; }

        public PushTemplatesFilteredModel(PushTemplatesFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<PushTemplates> pushTemplates, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            PushTemplates = SortData(FilterData(pushTemplates), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<PushTemplates> FilterData(IQueryable<PushTemplates> pushTemplates)
        {
            if (!string.IsNullOrWhiteSpace(Filter.Name))
                pushTemplates = pushTemplates.Where(x => x.Name.Contains(Filter.Name));
            if (Filter.CreationDateFrom.HasValue)
                pushTemplates = pushTemplates.Where(a => a.CreationDate >= Filter.CreationDateFrom.Value);
            if (Filter.CreationDateTo.HasValue)
                pushTemplates = pushTemplates.Where(a => a.CreationDate <= Filter.CreationDateTo.Value);
            return pushTemplates;
        }

        public IOrderedQueryable<PushTemplates> SortData(IQueryable<PushTemplates> pushTemplates, string sortColumn, bool dir)
        {
            if (sortColumn == "Id") return dir ? pushTemplates.OrderByDescending(x => x.Id) : pushTemplates.OrderBy(x => x.Id);
            return pushTemplates.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
        }
    }
}