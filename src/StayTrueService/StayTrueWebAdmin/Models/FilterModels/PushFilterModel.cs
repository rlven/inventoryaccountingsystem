﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class PushFilterModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public int? City { get; set; }
        public string PhoneNumber { get; set; }
        public int? StpFrom { get; set; }
        public int? StpTo { get; set; }
        public int? StmFrom { get; set; }
        public int? StmTo { get; set; }
        public DateTime? RegistrationDateFrom { get; set; }
        public DateTime? RegistrationDateTo { get; set; }
        public DateTime? LastSignInFromM { get; set; }
        public DateTime? LastSignInToM { get; set; }
        public DateTime? LastSignInFromP { get; set; }
        public DateTime? LastSignInToP { get; set; }
        public int? WincoinsFrom { get; set; }
        public int? WincoinsTo { get; set; }
        public bool IsAllSelected { get; set; }
        public int PageSize = 50;
    }

    public class PushFilteredModel
    {
        public PushFilterModel Filter;
        public IPagedList<Users> Users { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }
        public IEnumerable<SelectListItem> AccountStatuses { get; set; }

        public PushFilteredModel(PushFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<Users> users, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            Users = SortData(FilterData(users), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<Users> FilterData(IQueryable<Users> users)
        {
            users = users.Where(x => x.StatusId == 1 || x.StatusId == 4);
            if (!string.IsNullOrEmpty(Filter.FirstName))
                users = users.Where(x => x.FirstName.Contains(Filter.FirstName));
            if (!string.IsNullOrEmpty(Filter.LastName))
                users = users.Where(x => x.LastName.Contains(Filter.LastName));
            if (Filter.BirthDateFrom.HasValue)
                users = users.Where(x => x.BirthDate >= Filter.BirthDateFrom);
            if (Filter.BirthDateTo.HasValue)
                users = users.Where(x => x.BirthDate <= Filter.BirthDateTo);
            if (!string.IsNullOrEmpty(Filter.Email))
                users = users.Where(x => x.Email.Contains(Filter.Email));
            if (Filter.Status.HasValue)
                users = users.Where(x => x.StatusId == Filter.Status);
            if (Filter.City.HasValue)
                users = users.Where(x => x.CityId == Filter.City);
            if (!String.IsNullOrWhiteSpace(Filter.PhoneNumber))
                users = users.Where(x => x.Phone.Contains(Filter.PhoneNumber));
            if (Filter.StpFrom.HasValue)
                users = users.Where(x => x.SignInCounterP >= Filter.StpFrom);
            if (Filter.StpTo.HasValue)
                users = users.Where(x => x.SignInCounterP <= Filter.StpTo);
            if (Filter.StmFrom.HasValue)
                users = users.Where(x => x.SignInCounterM >= Filter.StmFrom);
            if (Filter.StmTo.HasValue)
                users = users.Where(x => x.SignInCounterM <= Filter.StmTo);
            if (Filter.RegistrationDateFrom.HasValue)
                users = users.Where(x => x.RegistrationDate >= Filter.RegistrationDateFrom);
            if (Filter.RegistrationDateTo.HasValue)
                users = users.Where(x => x.RegistrationDate <= Filter.RegistrationDateTo);
            if (Filter.LastSignInFromM.HasValue)
                users = users.Where(x => x.LastSignInM >= Filter.LastSignInFromM);
            if (Filter.LastSignInToM.HasValue)
                users = users.Where(x => x.LastSignInM <= Filter.LastSignInToM);
            if (Filter.LastSignInFromP.HasValue)
                users = users.Where(x => x.LastSignInP >= Filter.LastSignInFromP);
            if (Filter.LastSignInToP.HasValue)
                users = users.Where(x => x.LastSignInP == Filter.LastSignInToP);
            if (Filter.WincoinsFrom.HasValue)
                users = users.Where(a => a.Wincoins >= Filter.WincoinsFrom);
            if (Filter.WincoinsTo.HasValue)
                users = users.Where(a => a.Wincoins <= Filter.WincoinsTo);
            return users;
        }

        public IOrderedQueryable<Users> SortData(IQueryable<Users> users, string sortColumn, bool dir)
        {
            if (sortColumn == "Id") return dir ? users.OrderByDescending(x => x.Id) : users.OrderBy(x => x.Id);
            return users.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
        }
    }
}