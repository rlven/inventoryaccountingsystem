﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class ChatPrivateGroupsUsersFilterModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public int? City { get; set; }
        public DateTime? LastVisitDateFrom { get; set; }
        public DateTime? LastVisitDateTo { get; set; }
        public  string Phone { get; set; }
        
        public int? PageSize { get; set; }
    }

    public class ChatGroupsUsersFilteredModel
    {
        public ChatPrivateGroupsUsersFilterModel Filter;

        public IPagedList<Users> ChatGroupsUsers { get; set; }

        public int PageCount { get; set; }

        public ChatGroupsUsersFilteredModel(ChatPrivateGroupsUsersFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<ChatPrivateGroupsUsers> chatPrivateGroupsUsers, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();

            var data = SortData(FilterData(chatPrivateGroupsUsers), sortColumn, dir).Select(x => new Users()
            {
                Id = x.UserId,
                Phone = x.Users.Phone,
                LastName = x.Users.LastName,
                LastSignInP = x.Users.LastSignInP,
                BirthDate = x.Users.BirthDate,
                Cities = new Cities() {CityName = x.Users.Cities.CityName, Id = x.Users.CityId},
                FirstName = x.Users.FirstName,
                LastSignInM = x.Users.LastSignInM

            });

            var count = data.Count();
            PageCount = count/ Filter.PageSize.Value;
            PageCount += count % Filter.PageSize.Value > 0 ? 1 : 0;

            ChatGroupsUsers = data.ToPagedList(pageNumber, Filter.PageSize.Value);
        }

        public IQueryable<ChatPrivateGroupsUsers> FilterData(IQueryable<ChatPrivateGroupsUsers> chatPrivateGroupsUsers)
        {
            if (!string.IsNullOrEmpty(Filter.Name))
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(x => x.Users.FirstName.Contains(Filter.Name));
            }

            if (!string.IsNullOrEmpty(Filter.Surname))
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(x => x.Users.LastName.Contains(Filter.Surname));
            }

            if (Filter.BirthDateFrom.HasValue)
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(a => a.Users.BirthDate >= Filter.BirthDateFrom.Value);
            }

            if (Filter.BirthDateTo.HasValue)
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(a => a.Users.BirthDate <= Filter.BirthDateTo.Value);
            }

            if (Filter.City.HasValue)
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(a => a.Users.CityId == Filter.City.Value).AsQueryable();
            }
            if (Filter.LastVisitDateFrom.HasValue)
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(a => a.Users.LastSignInP >= Filter.LastVisitDateFrom.Value);
            }

            if (Filter.LastVisitDateTo.HasValue)
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(a => a.Users.LastSignInP <= Filter.LastVisitDateTo.Value);
            }

            if (!string.IsNullOrEmpty(Filter.Phone))
            {
                chatPrivateGroupsUsers = chatPrivateGroupsUsers.Where(x => x.Users.Phone.Contains(Filter.Phone));
            }

            return chatPrivateGroupsUsers;
        }

        public IOrderedQueryable<ChatPrivateGroupsUsers> SortData(IQueryable<ChatPrivateGroupsUsers> chatPrivateGroupsUsers, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? chatPrivateGroupsUsers.OrderByDescending(x => x.Id) : chatPrivateGroupsUsers.OrderBy(x => x.Id);
            return chatPrivateGroupsUsers.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
            Filter.Name = Filter.Name.TrimSpaces().StripHtml(false);
            Filter.Surname = Filter.Surname.TrimSpaces().StripHtml(false);
            Filter.Phone = Filter.Phone.TrimSpaces().StripHtml(false);
        }
    }
}