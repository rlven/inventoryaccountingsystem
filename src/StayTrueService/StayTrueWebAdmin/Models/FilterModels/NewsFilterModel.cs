﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class NewsFilterModel
    {
        public DateTime? CreationDateFrom { get; set; }
        public DateTime? CreationDateTo { get; set; }
        public string Header { get; set; }
        public int? Category { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsBrand { get; set; }
        public int? CountLikesFrom { get; set; }
        public int? CountLikesTo { get; set; }

        public int PageSize = 10;
    }

    public class NewsFilteredModel
    {
        public NewsFilterModel Filter;

        public IPagedList<News> News { get; set; }
        public IEnumerable<SelectListItem> NewsCategories { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }
        public IEnumerable<SelectListItem> NewsTypes { get; set; }

        public NewsFilteredModel(NewsFilterModel filter)
        {
            Filter = filter;
        }
        public void ProcessData(IQueryable<News> news, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            News = SortData(FilterData(news), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<News> FilterData(IQueryable<News> news)
        {

            if (Filter.CreationDateFrom.HasValue)
            {
     
                news = news.Where(a => a.CreationDate >= Filter.CreationDateFrom.Value);
            }

            if (Filter.CreationDateTo.HasValue)
            {
                news = news.Where(a => a.CreationDate <= Filter.CreationDateTo.Value);
            }

            if (Filter.IsBrand.HasValue)
                news = news.Where(x => x.IsBrand == Filter.IsBrand).AsQueryable();

            if (Filter.Category.HasValue)
                news = news.Where(a => a.NewsCategory == Filter.Category).AsQueryable();

            if (!String.IsNullOrWhiteSpace(Filter.Header))
                news = news.Where(a => a.NameRu.Contains(Filter.Header)).AsQueryable();

            if (Filter.IsActive.HasValue)
                news = news.Where(a => a.IsActive == Filter.IsActive).AsQueryable();

            if (Filter.CountLikesFrom.HasValue)
                news = news.Where(a => a.Likes.Count(x => x.Id == a.Id) >= Filter.CountLikesFrom).AsQueryable();

            if (Filter.CountLikesTo.HasValue)
                news = news.Where(a => a.Likes.Count(x => x.Id == a.Id) <= Filter.CountLikesTo).AsQueryable();

            return news;
        }

        public IOrderedQueryable<News> SortData(IQueryable<News> news, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? news.OrderByDescending(a => a.Id) : news.OrderBy(a => a.Id);
            return news.OrderByDescending(a => a.Id);
        }

        private void CleanData()
        {
            Filter.Header = Filter.Header.TrimSpaces().StripHtml(false);
        }
    }
}