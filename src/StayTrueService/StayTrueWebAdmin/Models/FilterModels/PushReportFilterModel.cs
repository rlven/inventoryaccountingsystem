﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class PushReportFilterModel
    {
        public DateTime? NotificationDateFrom { get; set; }
        public DateTime? NotificationDateTo { get; set; }
        public string TemplateName { get; set; }
        public string Content { get; set; }
        public int? RecipientsFrom { get; set; }
        public int? RecipientsTo { get; set; }
        public string AdministratorLogin { get; set; }
        public int PageSize = 20;
    }

    public class PushReportFilteredModel
    {
        public PushReportFilterModel Filter;
        public IPagedList<PushNotifications> PushNotifications { get; set; }

        public PushReportFilteredModel(PushReportFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<PushNotifications> pushNotifications, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            PushNotifications = SortData(FilterData(pushNotifications), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<PushNotifications> FilterData(IQueryable<PushNotifications> pushNotifications)
        {
            if (Filter.NotificationDateFrom.HasValue)
                pushNotifications = pushNotifications.Where(x => x.NotificationDate >= Filter.NotificationDateFrom);
            if (Filter.NotificationDateTo.HasValue)
                pushNotifications = pushNotifications.Where(x => x.NotificationDate <= Filter.NotificationDateTo);
            if (!string.IsNullOrEmpty(Filter.TemplateName))
                pushNotifications = pushNotifications.Where(x => x.TemplateName.Contains(Filter.TemplateName));
            if (Filter.RecipientsFrom.HasValue)
                pushNotifications = pushNotifications.Where(x => x.Recipients >= Filter.RecipientsFrom);
            if (Filter.RecipientsTo.HasValue)
                pushNotifications = pushNotifications.Where(x => x.Recipients <= Filter.RecipientsTo);
            if (!string.IsNullOrEmpty(Filter.Content))
                pushNotifications = pushNotifications.Where(x => x.Content.Contains(Filter.Content));
            if (!string.IsNullOrEmpty(Filter.AdministratorLogin))
                pushNotifications = pushNotifications.Where(x => x.Administrators.Login.Contains(Filter.AdministratorLogin));
            return pushNotifications;
        }

        public IOrderedQueryable<PushNotifications> SortData(IQueryable<PushNotifications> pushNotifications, string sortColumn, bool dir)
        {
            if (sortColumn == "Id") return dir ? pushNotifications.OrderByDescending(x => x.Id) : pushNotifications.OrderBy(x => x.Id);
            return pushNotifications.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
        }
    }
}