﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class MobileVersionFilterModel
    {
        public string AndroidVersion { get; set; }
        public string IosVersion { get; set; }
        public string AndroidPath { get; set; }
        public string IosPath { get; set; }
        public int PageSize = 10;
    }

    public class MobileVersionFilteredModel
    {
        public MobileVersionFilterModel Filter;

        public IPagedList<MobileVersions> MobileVersions { get; set; }

        public MobileVersionFilteredModel(MobileVersionFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<MobileVersions> mobileVersions, string sortColumn, bool dir, int pageNumber)
        {
            MobileVersions = SortData(FilterData(mobileVersions), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<MobileVersions> FilterData(IQueryable<MobileVersions> mobileVersions)
        {
            if (!string.IsNullOrWhiteSpace(Filter.AndroidVersion))
            {
                mobileVersions = mobileVersions.Where(b => b.AndroidVersion.Contains(Filter.AndroidVersion)).AsQueryable();
            }
            if (!string.IsNullOrWhiteSpace(Filter.IosVersion))
            {
                mobileVersions = mobileVersions.Where(b => b.IosVersion.Contains(Filter.IosVersion)).AsQueryable();
            }
            if (!string.IsNullOrWhiteSpace(Filter.AndroidPath))
            {
                mobileVersions = mobileVersions.Where(b => b.AndroidPath.Contains(Filter.AndroidPath)).AsQueryable();
            }
            if (!string.IsNullOrWhiteSpace(Filter.IosPath))
            {
                mobileVersions = mobileVersions.Where(b => b.IosPath.Contains(Filter.IosPath)).AsQueryable();
            }

            return mobileVersions;
        }

        public IOrderedQueryable<MobileVersions> SortData(IQueryable<MobileVersions> mobileVersions, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? mobileVersions.OrderByDescending(x => x.Id) : mobileVersions.OrderBy(x => x.Id);
            return mobileVersions.OrderByDescending(x => x.Id);
        }
    }
}