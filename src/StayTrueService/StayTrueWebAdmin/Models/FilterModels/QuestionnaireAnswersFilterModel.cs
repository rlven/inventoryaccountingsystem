﻿using System;
using System.Linq;
using DataAccessLayer;
using PagedList;
using StayTrueWebAdmin.Extentions;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class QuestionnaireAnswersFilterModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? DobFrom { get; set; }
        public DateTime? DobTo { get; set; }
        public int? City { get; set; }
        public DateTime? LastLoginFrom { get; set; }
        public DateTime? LastLoginTo { get; set; }
        public  string Phone { get; set; }
        public string QuizName { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string Answer5 { get; set; }
        public string Answer6 { get; set; }
        public string Answer7 { get; set; }
        public string Answer8 { get; set; }
        public string Answer9 { get; set; }
        public string Answer10 { get; set; }
        public string Answer11 { get; set; }
        public string Answer12 { get; set; }

        public int PageSize = 10;
    }

    public class QuestionnaireAnswersFilteredModel
    {
        public QuestionnaireAnswersFilterModel Filter;

        public IPagedList<QuestionnaireAnswers> QuestionnaireAnswerses { get; set; }

        public QuestionnaireAnswersFilteredModel(QuestionnaireAnswersFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<QuestionnaireAnswers> questionnaireAnswers, string sortColumn, bool dir, int pageNumber)
        {
            CleanData();
            QuestionnaireAnswerses = SortData(FilterData(questionnaireAnswers), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<QuestionnaireAnswers> FilterData(IQueryable<QuestionnaireAnswers> questionnaireAnswerses)
        {
            if (!string.IsNullOrEmpty(Filter.Name))
                questionnaireAnswerses = questionnaireAnswerses.Where(x => x.Users.FirstName.Contains(Filter.Name));

            if (!String.IsNullOrWhiteSpace(Filter.Surname))
            {
               questionnaireAnswerses = questionnaireAnswerses.Where(x => x.Users.LastName.Contains(Filter.Surname));
            }

            if (Filter.DobFrom.HasValue)
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Users.BirthDate >= Filter.DobFrom.Value);
            }

            if (Filter.DobTo.HasValue)
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Users.BirthDate <= Filter.DobTo.Value);
            }

            if (Filter.City.HasValue)
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Users.Cities.Id == Filter.City.Value);
            }

            if (Filter.LastLoginFrom.HasValue)
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Users.LastSignInP.HasValue && a.Users.LastSignInP >= Filter.LastLoginFrom.Value);
            }

            if (Filter.LastLoginTo.HasValue)
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Users.LastSignInP.HasValue && a.Users.LastSignInP <= Filter.LastLoginTo.Value);
            }

            if (!String.IsNullOrWhiteSpace(Filter.Phone))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Users.Phone.Contains(Filter.Phone));
            }
            if (!String.IsNullOrWhiteSpace(Filter.QuizName))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Questionnaires.Name.Contains(Filter.QuizName));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer1))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer1 != null && a.Answer1.Contains(Filter.Answer1));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer2))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer2 != null && a.Answer2.Contains(Filter.Answer2));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer3))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer3 != null && a.Answer3.Contains(Filter.Answer3));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer4))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer4 != null && a.Answer4.Contains(Filter.Answer4));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer5))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer5 != null && a.Answer5.Contains(Filter.Answer5));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer6))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer6 != null && a.Answer6.Contains(Filter.Answer6));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer7))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer7 != null && a.Answer7.Contains(Filter.Answer7));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer8))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer8 != null && a.Answer8.Contains(Filter.Answer8));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer9))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer9 != null && a.Answer9.Contains(Filter.Answer9));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer10))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer10 != null && a.Answer10.Contains(Filter.Answer10));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer11))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer11 != null && a.Answer11.Contains(Filter.Answer11));
            }

            if (!String.IsNullOrWhiteSpace(Filter.Answer12))
            {
                questionnaireAnswerses = questionnaireAnswerses.Where(a => a.Answer12 != null && a.Answer12.Contains(Filter.Answer12));
            }

          return questionnaireAnswerses;
        }

        public IOrderedQueryable<QuestionnaireAnswers> SortData(IQueryable<QuestionnaireAnswers> questionnaireAnswers, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? questionnaireAnswers.OrderByDescending(x => x.Id) : questionnaireAnswers.OrderBy(x => x.Id);
            return questionnaireAnswers.OrderByDescending(x => x.Id);
        }

        public void CleanData()
        {
            Filter.Name = Filter.Name.TrimSpaces().StripHtml(false);
            Filter.Surname = Filter.Surname.TrimSpaces().StripHtml(false);
            Filter.Phone = Filter.Phone.TrimSpaces().StripHtml(false);
            Filter.QuizName = Filter.QuizName.TrimSpaces().StripHtml(false);
            Filter.Answer1 = Filter.Answer1.TrimSpaces().StripHtml(false);
            Filter.Answer2 = Filter.Answer2.TrimSpaces().StripHtml(false);
            Filter.Answer3 = Filter.Answer3.TrimSpaces().StripHtml(false);
            Filter.Answer4 = Filter.Answer4.TrimSpaces().StripHtml(false);
            Filter.Answer5 = Filter.Answer5.TrimSpaces().StripHtml(false);
            Filter.Answer6 = Filter.Answer6.TrimSpaces().StripHtml(false);
            Filter.Answer7 = Filter.Answer7.TrimSpaces().StripHtml(false);
            Filter.Answer8 = Filter.Answer8.TrimSpaces().StripHtml(false);
            Filter.Answer9 = Filter.Answer9.TrimSpaces().StripHtml(false);
            Filter.Answer10 = Filter.Answer10.TrimSpaces().StripHtml(false);
            Filter.Answer11 = Filter.Answer11.TrimSpaces().StripHtml(false);
            Filter.Answer12 = Filter.Answer12.TrimSpaces().StripHtml(false);
        }
    }
}