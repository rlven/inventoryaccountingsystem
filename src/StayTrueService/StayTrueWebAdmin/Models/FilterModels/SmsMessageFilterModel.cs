﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Models.FilterModels
{
    public class SmsMessageFilterModel
    {
        public DateTime? SendDate { get; set; }
        public DateTime? SendDateEnd { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int? UsersCount { get; set; }
        public int? StatusId { get; set; }
        public int? Cost { get; set; }
        public string AdminLogin { get; set; }
        public int PageSize = 20;
        public int? UsersCountEnd { get; set; }
        public int? CostEnd { get; set; }
        public int TemplateId { get; set; }
    }

    public class SmsMessageFilteredModel
    {
        public SmsMessageFilterModel Filter;

        public IPagedList<SmsMessage> SmsMessages { get; set; }

        public IEnumerable<SelectListItem> Statuses { get; set; }

        public SmsMessageFilteredModel(SmsMessageFilterModel filter)
        {
            Filter = filter;
        }

        public void ProcessData(IQueryable<SmsMessage> smsMessages, string sortColumn, bool dir, int pageNumber)
        {
            SmsMessages = SortData(FilterData(smsMessages), sortColumn, dir).ToPagedList(pageNumber, Filter.PageSize);
        }

        public IQueryable<SmsMessage> FilterData(IQueryable<SmsMessage> smsTemplates)
        {
            if (!string.IsNullOrEmpty(Filter.Title))
                smsTemplates = smsTemplates.Where(x => x.Title.Contains(Filter.Title));
            if (!string.IsNullOrEmpty(Filter.Text))
                smsTemplates = smsTemplates.Where(x => x.Text.Contains(Filter.Text));
            if (Filter.SendDate.HasValue)
                smsTemplates = smsTemplates.Where(x => x.SendTime>=Filter.SendDate);
            if (Filter.SendDateEnd.HasValue)
                smsTemplates = smsTemplates.Where(x => x.SendTime <= Filter.SendDateEnd);
            if (Filter.UsersCount.HasValue)
                smsTemplates = smsTemplates.Where(x => x.UsersCount >= Filter.UsersCount);
            if (Filter.UsersCountEnd.HasValue)
                smsTemplates = smsTemplates.Where(x => x.UsersCount <= Filter.UsersCountEnd);
            if (Filter.Cost.HasValue)
                smsTemplates = smsTemplates.Where(x => x.SmsCost >= Filter.Cost);
            if (Filter.CostEnd.HasValue)
                smsTemplates = smsTemplates.Where(x => x.SmsCost <= Filter.CostEnd);
            if (Filter.StatusId.HasValue)
                smsTemplates = smsTemplates.Where(x => x.StatusId == Filter.StatusId);
            if (!string.IsNullOrEmpty(Filter.AdminLogin))
                smsTemplates = smsTemplates.Where(x => x.AdminLogin.Contains(Filter.AdminLogin));

            return smsTemplates;
        }

        public IOrderedQueryable<SmsMessage> SortData(IQueryable<SmsMessage> smsMessages, string sortColumn, bool dir)
        {
            if (sortColumn == "ID") return dir ? smsMessages.OrderByDescending(x => x.Id) : smsMessages.OrderBy(x => x.Id);
            return smsMessages.OrderByDescending(x => x.Id);
        }
    }
}