﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models
{
    public class Banner
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public HttpPostedFileBase Image { get; set; }
    }
}