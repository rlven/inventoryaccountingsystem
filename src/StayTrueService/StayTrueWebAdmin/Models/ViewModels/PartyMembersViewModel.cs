﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.ViewModels
{
    public class PartyMembersViewModel
    {
        public int Id { get; set; }
        public Users User { get; set; }
        public int Points { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string Answer5 { get; set; }
        public string Answer6 { get; set; }
        public string Answer7 { get; set; }
        public string Answer8 { get; set; }
        public string Answer9 { get; set; }
        public string Answer10 { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsInvited { get; set; }
        public bool HasCome { get; set; }
    }
}