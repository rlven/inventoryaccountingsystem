﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Models;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.ViewModels
{
    public class GreatBattleMemberViewModel
    {
        public long Id { get; set; }
        public Users Users { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public int Points { get; set; }
        public bool IsBlocked { get; set; }
        public GreatBattleGroups GreatBattleGroups { get; set; }
    }
}