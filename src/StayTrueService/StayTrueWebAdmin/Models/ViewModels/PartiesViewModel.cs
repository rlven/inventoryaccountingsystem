﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.ViewModels
{
    public class PartiesViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsFinished { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? RegistrationBeginDate { get; set; }
    }
}