﻿using System;

namespace StayTrueWebAdmin.Models.ViewModels
{
    public class GreatBattleViewModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public BattleCommand RedCommand { get; set; }
        public BattleCommand BlueCommand { get; set; }
        public DateTime BeginDateTime { get; set; } 
        public DateTime EndDateTime { get; set; } 
        public bool IsActive { get; set; }
    }

    public class BattleCommand
    {
        public String Name { get; set; }
        public int TotalPoinsCount { get; set; }
        public int ParticipantsCount { get; set; }
        public int Score { get; set; }
    }
}