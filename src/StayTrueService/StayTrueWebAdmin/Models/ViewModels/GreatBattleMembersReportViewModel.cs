﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.ViewModels
{
    public class GreatBattleMembersReportViewModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public int Wincoins { get; set; }
        public int BattleCoins { get; set; }
    }
}