﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.ViewModels
{
    public class LotteryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AvailableTickets { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int TicketPrice { get; set; }
        public int MaxTicketsForUser { get; set; }
        public bool IsActive { get; set; }
    }
}