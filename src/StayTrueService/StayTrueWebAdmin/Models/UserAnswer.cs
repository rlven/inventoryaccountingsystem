﻿using StayTrueWebAdmin.Models.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models
{
    public class UserAnswer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public bool? IsIssued { get; set; }
        public QuizzAnswerFilterModel Filter { get; set; }
        public IEnumerable<QuizzSessionAnswers> Answers { get; set; }
    }
}