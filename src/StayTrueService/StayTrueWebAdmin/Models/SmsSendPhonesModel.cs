﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models
{
    public class SmsSendPhonesModel : SmsSendModel
    {
        public List<string> ApprovedUsers { get; set; } 
        public List<string> NotFoundUsers { get; set; } 
        public List<string> NotApprovedUsers { get; set; } 
        public List<string> NotCorrectNumbers { get; set; }
        public int Count { get; set; }
        public string Phones { get; set; }

        public SmsSendPhonesModel()
        {
            ApprovedUsers = new List<string>();
            NotFoundUsers = new List<string>();
            NotApprovedUsers = new List<string>();
            NotCorrectNumbers = new List<string>();
        }
    }
}