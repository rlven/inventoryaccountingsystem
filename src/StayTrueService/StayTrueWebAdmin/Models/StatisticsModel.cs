﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models
{
    public class StatisticsModel
    {
        public Dictionary<Genders, int> ByGender { get; set; }
        public Dictionary<String, int> ByPassport { get; set; }
        public Dictionary<String, int> ByAge { get; set; }
        public Dictionary<Cities, int> ByCity { get; set; }

    }
}