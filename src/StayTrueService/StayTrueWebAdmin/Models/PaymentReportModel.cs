﻿
// Примечание. Для запуска созданного кода может потребоваться NET Framework версии 4.5 или более поздней версии и .NET Core или Standard версии 2.0 или более поздней.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class ArrayOfRestReportData
{

    private ArrayOfRestReportDataRestReportData[] restReportDataField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("RestReportData")]
    public ArrayOfRestReportDataRestReportData[] RestReportData
    {
        get
        {
            return this.restReportDataField;
        }
        set
        {
            this.restReportDataField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ArrayOfRestReportDataRestReportData
{

    private System.DateTime periodDateField;

    private string dealerContactNameField;

    private string serviceNameField;

    private string receiptIDField;

    private string pointContactNameField;

    private ulong personalAccountField;

    private decimal paymentAmountField;

    private decimal pointComissionField;

    private bool pointComissionFieldSpecified;

    private System.DateTime paymentDateTimeField;

    private System.DateTime transactionDateTimeField;

    private string paymentStatusNameField;

    private byte paymentStatusIDField;

    private string servicePaymentIdField;

    /// <remarks/>
    public System.DateTime PeriodDate
    {
        get
        {
            return this.periodDateField;
        }
        set
        {
            this.periodDateField = value;
        }
    }

    /// <remarks/>
    public string DealerContactName
    {
        get
        {
            return this.dealerContactNameField;
        }
        set
        {
            this.dealerContactNameField = value;
        }
    }

    /// <remarks/>
    public string ServiceName
    {
        get
        {
            return this.serviceNameField;
        }
        set
        {
            this.serviceNameField = value;
        }
    }

    /// <remarks/>
    public string ReceiptID
    {
        get
        {
            return this.receiptIDField;
        }
        set
        {
            this.receiptIDField = value;
        }
    }

    /// <remarks/>
    public string PointContactName
    {
        get
        {
            return this.pointContactNameField;
        }
        set
        {
            this.pointContactNameField = value;
        }
    }

    /// <remarks/>
    public ulong PersonalAccount
    {
        get
        {
            return this.personalAccountField;
        }
        set
        {
            this.personalAccountField = value;
        }
    }

    /// <remarks/>
    public decimal PaymentAmount
    {
        get
        {
            return this.paymentAmountField;
        }
        set
        {
            this.paymentAmountField = value;
        }
    }

    /// <remarks/>
    public decimal PointComission
    {
        get
        {
            return this.pointComissionField;
        }
        set
        {
            this.pointComissionField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PointComissionSpecified
    {
        get
        {
            return this.pointComissionFieldSpecified;
        }
        set
        {
            this.pointComissionFieldSpecified = value;
        }
    }

    /// <remarks/>
    public System.DateTime PaymentDateTime
    {
        get
        {
            return this.paymentDateTimeField;
        }
        set
        {
            this.paymentDateTimeField = value;
        }
    }

    /// <remarks/>
    public System.DateTime TransactionDateTime
    {
        get
        {
            return this.transactionDateTimeField;
        }
        set
        {
            this.transactionDateTimeField = value;
        }
    }

    /// <remarks/>
    public string PaymentStatusName
    {
        get
        {
            return this.paymentStatusNameField;
        }
        set
        {
            this.paymentStatusNameField = value;
        }
    }

    /// <remarks/>
    public byte PaymentStatusID
    {
        get
        {
            return this.paymentStatusIDField;
        }
        set
        {
            this.paymentStatusIDField = value;
        }
    }

    /// <remarks/>
    public string ServicePaymentId
    {
        get
        {
            return this.servicePaymentIdField;
        }
        set
        {
            this.servicePaymentIdField = value;
        }
    }
}

