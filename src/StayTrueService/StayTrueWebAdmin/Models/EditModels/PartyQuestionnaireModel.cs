﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class PartyQuestionnaireModel
    {
        public int Id { get; set; }
        [Display(Name = "Вопрос")]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "Длина вопроса должна быть от 3 до 1000 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Вопрос не может быть пустым")]
        public String Question { get; set; }
    }

    public class PartyGameModel
    {
        public int Id { get; set; }
        public HttpPostedFileBase GameArchive { get; set; }
        public HttpPostedFileBase GameImage { get; set; }
        public string GameName { get; set; }
        public string GamePath { get; set; }
        public string GameImagePath { get; set; }
    }
}