﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class FortuneWheelComponentsEditModel
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public bool IsProduct { get; set; }
        public int Wincoins { get; set; }
    }
}