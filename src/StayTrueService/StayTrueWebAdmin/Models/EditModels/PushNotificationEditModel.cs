﻿using System;
using System.ComponentModel.DataAnnotations;


namespace StayTrueWebAdmin.Models.EditModels
{
    public class PushNotificationEditModel
    {
        [Display(Name = "Содержание")]
        [Required(ErrorMessage = "Поле содержание обязательное")]
        public string Content { get; set; }
        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Поле заголовок обязательное")]
        public string Title { get; set; }
        public string[] Segments { get; set; }
        public int[] UserIds { get; set; }
        [Display(Name = "Время отправки")]
        public DateTime? SendTime { get; set; }
        [Display(Name = "Ссылка")]
        public string Url { get; set; }
        public int TemplateId { get; set; }
    }
}