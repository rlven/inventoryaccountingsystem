﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class FortuneProductsEditModel
    {
        public int Id { get; set; }
        public DateTime CreationDate{ get; set; }
        [Required(ErrorMessage = "Имя продукта не может быть пустым")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Стоимость продукта не может быть пуста")]
        public int Cost { get; set; }
        public int Quantity { get; set; }
        public byte Type { get; set; }
    }
}