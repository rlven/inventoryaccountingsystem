﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Metas;

namespace StayTrueWebAdmin.Models.EditModels
{
    [MetadataType(typeof(QuizzesMeta))]
    public class QuizEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public int Wincoins { get; set; }
        public DateTime? PublicationDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}