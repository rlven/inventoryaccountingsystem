﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.EditModels
{
    [MetadataType(typeof(DataAccessLayer.Metas.PvpGamesMeta))]
    public class PvpGameEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GameUrl { get; set; }
        public string ImagePath { get; set; }
        public int Wincoins { get; set; }
        public int WincoinsExchange { get; set; }
        public DateTime? PublicationDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IncludeBots { get; set; }
        public int GameId { get; set; }
        public int PlayerQty { get; set; }
        public int MatchTime { get; set; }
        public string[] Bets { get; set; }
    }
}