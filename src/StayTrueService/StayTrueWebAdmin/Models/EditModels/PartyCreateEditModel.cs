﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using PagedList;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class PartyCreateEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsFinished { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? RegistrationBeginDate { get; set; }
        public string QuizzPath { get; set; }
        public string QuizzImagePath { get; set; }
        public string ImagePreview { get; set; }
        public string ImagePreviewSmall { get; set; }

        public string QuizzName { get; set; }
        public PartyQuestionnaireModel[] PartyQuestionnaireModels { get; set; }

        public PartyGameModel[] PartyGameModels { get; set; }
        public HttpPostedFileBase QuizzArchive { get; set; }

        public HttpPostedFileBase QuizzImage { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public HttpPostedFileBase MobileImage { get; set; }

    }
}