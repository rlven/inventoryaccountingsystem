﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class FortuneWheelEditModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле название обязательное")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Поле ставка обязательное")]
        public int Bet { get; set; }
        public DateTime CreationTime { get; set; }
        [Required(ErrorMessage = "Поле джекпот обязательное")]
        public bool IsJackpotActive { get; set; }
        [Required(ErrorMessage = "Поле статус обязательное")]
        public bool IsActive { get; set; }
        [MaxLength(12, ErrorMessage = "Количество слотов в колесе должно быть = 12")]
        [MinLength(12, ErrorMessage ="Количество слотов в колесе должно быть = 12")]
        public FortuneWheelComponentsEditModel[] Components { get; set; }
    }
}