﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class PushTemplateEditModel
    {
        public int Id { get; set; }
        [Display(Name = "Имя шаблона")]
        [Required(ErrorMessage = "Поле имя шаблона обязательное")]
        [MaxLength(30)]
        public string Name { get; set; }
        [Display(Name = "Заголовок уведомления")]
        [Required(ErrorMessage = "Поле имя шаблона обязательное")]
        [MaxLength(30)]
        public string PushCaption { get; set; }
        [Display(Name = "Содержание уведомления")]
        [Required(ErrorMessage = "Поле содержание уведомления обязательное")]
        [MaxLength(300)]
        [AllowHtml]
        public string PushContent { get; set; }
        public bool IsDeleted { get; set; }
    }
}