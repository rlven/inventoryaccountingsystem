﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class MobileVersionEditModel
    {

        public string AndroidVersion { get; set; }

        public string IosVersion { get; set; }

        public string AndroidPath { get; set; }
        public string IosPath { get; set; }


        public HttpPostedFileBase AndroidBuild { get; set; }

        public HttpPostedFileBase IosBuild { get; set; }
    }
}