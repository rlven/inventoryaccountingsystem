﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Metas;

namespace StayTrueWebAdmin.Models.EditModels
{
    [MetadataType(typeof(GamesMeta))]
    public class GameEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GameUrl { get; set; }
        public string ImagePath { get; set; }
        public int Wincoins { get; set; }
        public int WincoinsExchange { get; set; }
        public DateTime? PublicationDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}