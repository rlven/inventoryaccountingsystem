﻿using System.ComponentModel.DataAnnotations;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class SupportRequestsEditModel
    {
        public int Id { get; set; }
        public int? UserId { get; set; }

        [Required(ErrorMessage = "Поле Сообщение обязательное")]
        public string Body { get; set; }

        [Required(ErrorMessage = "Поле Дата обращения обязательное")]
        public System.DateTime CreationDate { get; set; }

        public string Phone { get; set; }
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле Ответ обязательное")]
        public bool IsAnswered { get; set; }
    }
}