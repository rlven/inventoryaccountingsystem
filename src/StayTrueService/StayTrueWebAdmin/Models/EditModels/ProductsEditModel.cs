﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Metas;

namespace StayTrueWebAdmin.Models.EditModels
{
    [MetadataType(typeof(ProductsMeta))]
    public class ProductsEditModel
    {
        public int Id { get; set; }
        public string NameRu { get; set; }
        public string InformationRu { get; set; }
        public string NameKg { get; set; }
        public string InformationKg { get; set; }
        public int Price { get; set; }
        public int Amount { get; set; }
        public bool IsAvailable { get; set; }
        public string ImagePath { get; set; }
        public int ShopId { get; set; }
        public int CategoryId { get; set; }
        public int AvailableAmount { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? PublicationDate { get; set; }
        public int LanguageId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsBrand { get; set; }
    }
}