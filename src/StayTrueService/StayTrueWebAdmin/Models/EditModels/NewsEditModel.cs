﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAccessLayer;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class NewsEditModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле Заголовок обязательное")]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "Поле заголовок должно содержать от 3 до 100 символов")]
        public string NameRu { get; set; }

        [AllowHtml]
        [DataType(DataType.Html)]
        [Display(Name = "DescriptionRu")]
        [Required(ErrorMessage = "Поле Описание обязательное")]
        public string DescriptionRu { get; set; }
        public string ImageP { get; set; }
        public string ImageM { get; set; }
        public string VideoP { get; set; }
        public string VideoM { get; set; }
        public int NewsType { get; set; }
        public int NewsCategory { get; set; }

        [Required(ErrorMessage = "Поле Дата создания обязательное")]
        public System.DateTime CreationDate { get; set; }
        [Required(ErrorMessage = "Поле Дата публикации обязательное")]
        public System.DateTime PublicationDate { get; set; }

        [Required(ErrorMessage = "Поле Показать обязательное")]
        public bool IsActive { get; set; }
        public bool? IsBrand { get; set; }
        public bool? IsDeleted { get; set; }

        public  ICollection<Likes> Likes { get; set; }

    }
}