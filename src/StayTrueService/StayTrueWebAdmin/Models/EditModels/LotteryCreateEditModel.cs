﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml.Utils;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class LotteryCreateEditModel
    {
        public int Id { get; set; }
        [Required]
        public int AvailableTickets { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateStart { get; set; }
        [Required]
        public DateTime DateEnd { get; set; }
        [Required]
        public int TicketPrice { get; set; }
        [Required]
        public int MaxTicketsForUser { get; set; }
        [Required]
        public bool IsActive { get; set; }

        public SelectList Statuses { get; set; }
        public int PageSize = 10;
    }
}