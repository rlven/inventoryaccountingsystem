﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class UsersEditModel
    {
        public int Id { get; set; }


        public string Password { get; set; }

        [Required(ErrorMessage = "Поле Имя обязательное")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Поле Фамилия обязательное")]
        public string LastName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Поле Телефон обязательное")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Поле Email обязательное")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле Email обязательное")]
        public System.DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Поле Город обязательное")]
        public int CityId { get; set; }

        [Required(ErrorMessage = "Поле Wincoins обязательное")]
        public int Wincoins { get; set; }

        [Required(ErrorMessage = "Поле Дата регистрации обязательное")]
        public System.DateTime RegistrationDate { get; set; }

        [Required(ErrorMessage = "Поле Статус обязательное")]
        public int StatusId { get; set; }

        [Required(ErrorMessage = "Поле Пол обязательное")]
        public int GenderId { get; set; }

        public string PhoneHash { get; set; }
        public int? PromoCode { get; set; }
        public int SignInCounterP { get; set; }
        public int SignInCounterM { get; set; }
        public DateTime? LastSignInM { get; set; }
        public DateTime? LastSignInP { get; set; }

        public bool IsAvatarApproved { get; set; }

        [Required(ErrorMessage = "Поле Удален обязательное")]
        public bool IsDeleted { get; set; }

        public bool IsApproved { get; set; }
    }
}