﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class CigaretteBrandsEditModel
    {
        public int Id { get; set; }
        
        [Display(Name = "Бренд")]
        [Required(ErrorMessage = "Поле Заголовок обязательное")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Поле заголовок должно содержать от 3 до 30 символов")]
        public string BrandName { get; set; }

        [AllowHtml]
        [DataType(DataType.Html)]
        [Display(Name = "Описание")]
        [StringLength(460, MinimumLength = 3, ErrorMessage = "Поле описание должно содержать от 3 до 460 символов")]
        [Required(ErrorMessage = "Поле Описание обязательное")]
        public string Description { get; set; }

        [Display(Name = "Смола")]
        [Required(ErrorMessage = "Поле Смола обязательное")]
        public double Tar { get; set; }

        [Display(Name = "Никотин")]
        [Required(ErrorMessage = "Поле Никотин обязательное")]
        public double Nicotine { get; set; }

        [Display(Name = "Порядковый номер")]
        public int BrandPosition { get; set; }

        [Display(Name = "Активно")]
        public bool IsActive { get; set; }

        [Display(Name = "Бренд компании")]
        public string CompanyBrand { get; set; }

        [Display(Name = "Размер")]
        public int PackSize { get; set; }
        [Display(Name = "Дополнительная информация")]
        public string AdditionalInfo1 { get; set; }
        [Display(Name = "Дополнительная информация")]
        public string AdditionalInfo2 { get; set; }
        [Display(Name = "Дополнительная информация")]
        public string AdditionalInfo3 { get; set; }
        [Display(Name = "Дополнительная информация")]
        public string AdditionalInfo4 { get; set; }
        [Required(ErrorMessage = "Изображение бренда для сайта обязательно")]
        public string PathBrandImageP { get; set; }
        public HttpPostedFileBase BrandImageP { get; set; }
        [Required(ErrorMessage = "Изображение для описания бренда для сайта обязательно")]
        public string PathDescriptionImageP { get; set; }
        public HttpPostedFileBase DescriptionImageP { get; set; }
        [Required(ErrorMessage = "Изображение для панели администратора обязательно")]
        public string PathAdminImage { get; set; }
        public HttpPostedFileBase AdminImage { get; set; }
        [Required(ErrorMessage = "Изображение бренда для мобильного приложения обязательно")]
        public string PathBrandImageM { get; set; }
        public HttpPostedFileBase BrandImageM { get; set; }
        [Required(ErrorMessage = "Изображение для описания бренда для приложения обязательно")]
        public string PathDescriptionImageM { get; set; }
        public HttpPostedFileBase DescriptionImageM { get; set; }
        public string PathAdditionalImage1 { get; set; }
        public HttpPostedFileBase AdditionalImage1 { get; set; }
        public string PathAdditionalImage2 { get; set; }
        public HttpPostedFileBase AdditionalImage2 { get; set; }
        public string PathAdditionalImage3 { get; set; }
        public HttpPostedFileBase AdditionalImage3 { get; set; }
        public string PathAdditionalImage4 { get; set; }
        public HttpPostedFileBase AdditionalImage4 { get; set; }
    }
}