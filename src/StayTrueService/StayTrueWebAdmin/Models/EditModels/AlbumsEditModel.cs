using System.ComponentModel.DataAnnotations;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class AlbumsEditModel
    {
        public int Id { get; set; }

        [Display(Name = "Название альбома")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина названия должно быть от 3 до 100 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Имя не может быть пустым")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Дата создания не может быть пустым")]
        // [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm", ApplyFormatInEditMode = true)]
        public System.DateTime CreationDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Дата публикации не может быть пустым")]
        // [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public System.DateTime? PublicationDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Статус не может быть пустым")]
        public bool IsActive { get; set; }


        public string Image { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Брендинг не может быть пустым")]
        public bool IsBrended { get; set; }

        public bool IsDeleted { get; set; }
    }
}