﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StayTrueWebAdmin.Models.EditModels
{
    public class GreatBattleCreateEditModel
    {
        public int Id { get; set; }
        [Display(Name = "Название битвы")]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "Длина названия должно быть от 3 до 250 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Имя не может быть пустым")]
        public String Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Дата начала не может быть пустым")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public DateTime BeginDateTime { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Статус не может быть пустым")]
        public bool IsActive { get; set; }
        [AllowHtml]
        public string Desription { get; set; }

        public bool autofillPlayer { get; set; }

        public string PointCoef { get; set; }
        public bool IsFinished { get; set; }
        public bool IsDeleted { get; set; }

        public string DescriptionMobile { get; set; }

        public int? Rank1Limit { get; set; }
        public int? Rank2Limit { get; set; }
        public int? Rank3Limit { get; set; }
        public int? Rank4Limit { get; set; }
        public int? Rank5Limit { get; set; }
        [Range(1, 10000, ErrorMessage = "Коэффициент викторин должен быть от 1 до 10000")]
        public int? QuizCoefficient { get; set; }

        public GreatBattleQuizzQuestionModel[] QuestionsModel { get; set; }

        public GreatBattleTourEditModel[] ToursModel { get; set; }
    }


    public class GreatBattleQuizzQuestionModel
    {
        public int Id { get; set; }
        [Display(Name = "Вопрос")]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "Длина вопроса должна быть от 3 до 1000 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Вопрос не может быть пустым")]
        public String Question { get; set; }

    }

    public class GreatBattleTourEditModel
    {
        public int Id { get; set; }

        public DateTime? CreateDateTime { get; set; }

        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        [Display(Name = "Коэффициент конвертации очков")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле конвертация винкоинов не может быть пустым")]
        public string PointRate { get; set; }
        [Display(Name = "Название игры")]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "Длина названия должно быть от 3 до 250 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Название игры не может быть пустым")]
        public string GameName { get; set; }

        [Display(Name = "Название опроса")]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "Длина опроса должно быть от 3 до 250 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Название опроса не может быть пустым")]
        public string QuizzName { get; set; }

        public string GameImagePath { get; set; }
        public string QuizzImagePath { get; set; }

        public HttpPostedFileBase GameArchive { get; set; }
        public HttpPostedFileBase QuizzArchive { get; set; }
        public HttpPostedFileBase GameImage { get; set; }
        public HttpPostedFileBase QuizzImage { get; set; }
    }
}