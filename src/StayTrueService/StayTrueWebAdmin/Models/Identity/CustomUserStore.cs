﻿using System;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace StayTrueWebAdmin.Models.Identity
{
    public class CustomUserStore<TUser> : UserStore<TUser, IdentityRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>, IUserStore<TUser>, IUserStore<TUser, string>, IDisposable where TUser : IdentityUser
    {
        /// <summary>
        /// Default constuctor which uses a new instance of a default EntityyDbContext
        /// 
        /// </summary>
        public CustomUserStore() : this((DbContext)new IdentityDbContext())
        {
            this.DisposeContext = true;
        }

        /// <summary>
        /// Constructor
        /// 
        /// </summary>
        /// <param name="context"/>
        public CustomUserStore(DbContext context) : base(context)
        {

        }

        public Task<TUser> FindByExpressionAsync(Expression<Func<TUser, bool>> filter)
        {
            return this.GetUserAggregateAsync(filter as Expression<Func<TUser, bool>>);
        }
    }
}