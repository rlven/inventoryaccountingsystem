﻿using Microsoft.Owin;
using Owin;
using StayTrueWebAdmin;

[assembly: OwinStartup(typeof(Startup))]
namespace StayTrueWebAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureHangFire(app);
        }
    }
}
