//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class FortuneWheelComponents
    {
        public int Id { get; set; }
        public int WheelId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public bool IsProduct { get; set; }
        public int Wincoins { get; set; }
    
        public virtual FortuneProducts FortuneProducts { get; set; }
        public virtual FortuneWheel FortuneWheel { get; set; }
    }
}
