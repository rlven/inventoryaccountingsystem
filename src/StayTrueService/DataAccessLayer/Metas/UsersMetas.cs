﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Metas
{
    public class UsersMetas
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "Телефон")]
        public string Phone { get; set; }
        public string Password { get; set; }
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Дата рождения")]
        public System.DateTime BirthDate { get; set; }
        [Display(Name = "Дата регистрации")]
        public System.DateTime RegistrationDate { get; set; }
        public Nullable<System.DateTime> RefreshDate { get; set; }
        [Display(Name = "Город")]
        public int CityId { get; set; }
        [Display(Name = "Статус")]
        public int StatusId { get; set; }
        [Display(Name = "Пол")]
        public int GenderId { get; set; }
        public int RoleId { get; set; }
        [Display(Name = "Wincoins")]
        public int Wincoins { get; set; }
        [Display(Name = "Промокод")]
        public string PromoCode { get; set; }
        [Display(Name = "Дата входа в приложение")]
        public Nullable<System.DateTime> LastSignInM { get; set; }
        [Display(Name = "Дата входа на сайт")]
        public Nullable<System.DateTime> LastSignInP { get; set; }
        [Display(Name = "Кол-во посещений приложения")]
        public int SignInCounterM { get; set; }
        [Display(Name = "Кол-во посещений сайта")]
        public int SignInCounterP { get; set; }
        [Display(Name = "Удален")]
        public bool IsDeleted { get; set; }
        [Display(Name = "Подтвержден")]
        public bool IsApproved { get; set; }
        public string Avatar { get; set; }
        public string PassportFront { get; set; }
        public string PassportBack { get; set; }
        [Display(Name = "Предпочитаемый бренд")]
        public string CigarettesFavorite { get; set; }
        [Display(Name = "Предпочитаемый суб-бренд")]
        public string CigarettesFavoriteType { get; set; }
        [Display(Name = "Альтернативный бренд")]
        public string CigarettesAlternative { get; set; }
        [Display(Name = "Альтернативный суб-бренд")]
        public string CigarettesAlternativeType { get; set; }
    }
}
