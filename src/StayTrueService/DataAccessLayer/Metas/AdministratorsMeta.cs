﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Metas
{
    public class AdministratorsMeta
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле Логин обязательное")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Поле Логин должно содержать от 3 до 100 символов")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Поле Пароль обязательное")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Поле Имя обязательное")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Поле Имя должно содержать от 3 до 100 символов")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Поле Фамилия обязательное")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Поле Фамилия должно содержать от 3 до 100 символов")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Поле Телефон обязательное")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "Поле Телефон должно содержать от 10 до 50 символов")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Поле Email обязательное")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Поле Email должно содержать от 3 до 100 символов")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле Дата рождения обязательное")]
        public System.DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Поле Роль обязательное")]
        public int Role { get; set; }
        public Nullable<System.DateTime> LastSignIn { get; set; }
    }
}
