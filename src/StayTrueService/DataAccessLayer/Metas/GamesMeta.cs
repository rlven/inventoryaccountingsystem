﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Metas
{
    public class GamesMeta
    {
        public int Id { get; set; }
        [Display(Name = "Название")]
        [Required(ErrorMessage = "Поле Заголовок обязательное")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Поле Название должно содержать от 3 до 100 символов")]
        public string Name { get; set; }

        [Display(Name = "Архив")]
        public string GameUrl { get; set; }

        [Display(Name = "Изображение игры")]
        public string ImagePath { get; set; }

        [Display(Name = "Wincoins")]
        [Required(ErrorMessage = "Поле Wincoins обязательное")]
        public int Wincoins { get; set; }

        [Display(Name = "Курс Wincoin")]
        [Required(ErrorMessage = "Поле Курс Wincoin обязательное")]
        public int WincoinsExchange { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime? PublicationDate { get; set; }

        [Display(Name = "Статус")]
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class PvpGamesMeta
    {
        public int Id { get; set; }
        [Display(Name = "Название")]
        [Required(ErrorMessage = "Поле Заголовок обязательное")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Поле Название должно содержать от 3 до 100 символов")]
        public string Name { get; set; }

        [Display(Name = "Архив")]
        public string GameUrl { get; set; }

        [Display(Name = "Изображение игры")]
        public string ImagePath { get; set; }

        [Display(Name = "Wincoins")]
        [Required(ErrorMessage = "Поле Wincoins обязательное")]
        public int Wincoins { get; set; }

        [Display(Name = "Курс Wincoin")]
        [Required(ErrorMessage = "Поле Курс Wincoin обязательное")]
        public int WincoinsExchange { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime? PublicationDate { get; set; }

        [Display(Name = "Статус")]
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        [Display(Name = "Id игры")]
        public int GameId { get; set; }

        [Display(Name = "Включить ботов")]
        public bool IncludeBots { get; set; }
        [Display(Name = "Количество игроков")]
        public int PlayerQty { get; set; }
        [Display(Name = "Время матча")]
        public int MatchTime { get; set; }
    }
}