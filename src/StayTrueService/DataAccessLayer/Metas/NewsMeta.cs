﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DataAccessLayer.Metas
{
    public class NewsMeta
    {
        public int Id { get; set; }

        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Поле Заголовок обязательное")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Поле заголовок должно содержать от 3 до 100 символов")]
        public string NameRu { get; set; }

        [Display(Name = "Заголовок")] public string NameKg { get; set; }

        [AllowHtml]
        [DataType(DataType.Html)]
        [Display(Name = "DescriptionRu")]
        [Required(ErrorMessage = "Поле Описание обязательное")]
        public string DescriptionRu { get; set; }

        [AllowHtml]
        [DataType(DataType.Html)]
        [Display(Name = "DescriptionKg")]
        public string DescriptionKg { get; set; }

        public string ImageP { get; set; }
        public string ImageM { get; set; }
        public string VideoP { get; set; }
        public string VideoM { get; set; }

        [Display(Name = "Тип")]
        public int NewsType { get; set; }

        [Display(Name = "Категория")]
        public int NewsCategory { get; set; }

        [Required(ErrorMessage = "Поле Дата создания обязательное")]
        public System.DateTime CreationDate { get; set; }

        [Required(ErrorMessage = "Поле Показать обязательное")]
        public bool IsActive { get; set; }

        [Display(Name = "Брендинг")]
        public bool? IsBrand { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime? PublicationDate { get; set; }
    }
}