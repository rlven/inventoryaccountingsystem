﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DataAccessLayer.Metas
{
    public class QuizzesMeta
    {
        public int Id { get; set; }
        [Display(Name = "Название")]
        [Required(ErrorMessage = "Поле Заголовок обязательное")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Поле Название должно содержать от 3 до 100 символов")]
        public string Name { get; set; }

        [Display(Name = "Архив")]
        public string Url { get; set; }

        [Display(Name = "Изображение")]
        public string ImagePath { get; set; }

        [Display(Name = "Wincoins")]
        [Required(ErrorMessage = "Поле Wincoins обязательное")]
        public int Wincoins { get; set; }

        [Display(Name = "Описание")]
        [AllowHtml]
        public string Description { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime? PublicationDate { get; set; }

        [Display(Name = "Статус")]
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}