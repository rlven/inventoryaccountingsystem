﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DataAccessLayer.Metas
{
    public class ProductsMeta
    {

        public int Id { get; set; }

        [Display(Name = "Название(рус)")]
        public string NameRu { get; set; }

        [Display(Name = "Информация (рус)")]
        [AllowHtml]
        [DataType(DataType.Html)]
        public string InformationRu { get; set; }

        [Display(Name = "Название (кырг)")]
        public string NameKg { get; set; }

        [Display(Name = "Информация(кырг)")]
        [AllowHtml]
        [DataType(DataType.Html)]
        public string InformationKg { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Поле Цена обязательное")]
        public int Price { get; set; }

        [Display(Name = "Количество шт")]
        [Required(ErrorMessage = "Поле Количество шт обязательное")]
        public int Amount { get; set; }

        [Display(Name = "Доступность")]
        [Required(ErrorMessage = "Поле Доступность обязательное")]
        public bool IsAvailable { get; set; }

        [Display(Name = "Изображение")]
        public string ImagePath { get; set; }

        [Display(Name = "Магазин")]
        [Required(ErrorMessage = "Поле Магазин обязательное")]
        public int ShopId { get; set; }

        [Display(Name = "Можно взять (шт.)")]
        [Required(ErrorMessage = "Поле Можно взять (шт.) обязательное")]
        public int AvailableAmount { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime? CreationDate { get; set; }

        [Display(Name = "Язык")]
        [Required(ErrorMessage = "Поле Язык обязательное")]
        public int LanguageId { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Поле Категория обязательное")]
        public int CategoryId { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime? PublicationDate { get; set; }

        [Display(Name = "Брендинг")]
        [Required(ErrorMessage = "Поле Брендинг обязательное")]
        public bool IsBrand { get; set; }

        [Display(Name = "Удален")]
        public bool IsDeleted { get; set; }


    }
}