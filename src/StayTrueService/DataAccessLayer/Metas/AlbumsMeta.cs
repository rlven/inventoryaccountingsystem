﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Metas
{
    public class AlbumsMeta
    {
        public int Id { get; set; }

        [Display(Name = "Название альбома")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина названия должно быть от 3 до 100 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Имя не может быть пустым")]
        public string Name { get; set; }

        [Display(Name = "Дата создания")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Дата создания не может быть пустым")]
       // [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public System.DateTime CreationDate { get; set; }

        [Display(Name = "Статус")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Статус не может быть пустым")]
        public bool IsActive { get; set; }

        [Display(Name = "Дата публикации")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле Дата публикации не может быть пустым")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public DateTime? PublicationDate { get; set; }


        public string Image { get; set; }

        [Display(Name = "Брендинг")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле  Брендинг не может быть пустым")]
        public bool IsBrended { get; set; }

       public virtual ICollection<AlbumImages> AlbumImages { get; set; }
    }
}
