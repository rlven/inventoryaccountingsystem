//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class FortunePurchasesHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public double Cost { get; set; }
        public int StatusId { get; set; }
        public Nullable<int> AdministratorId { get; set; }
    
        public virtual Administrators Administrators { get; set; }
        public virtual FortuneProducts FortuneProducts { get; set; }
        public virtual PurchaseStatuses PurchaseStatuses { get; set; }
        public virtual Users Users { get; set; }
    }
}
