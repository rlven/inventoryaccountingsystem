//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class GreatBattlesUsers
    {
        public long ID { get; set; }
        public int UserID { get; set; }
        public int GreatBattleID { get; set; }
        public int Score { get; set; }
        public System.DateTime CreationDate { get; set; }
        public int GroupID { get; set; }
        public int Points { get; set; }
        public bool CanChangeSide { get; set; }
        public bool IsBlocked { get; set; }
    
        public virtual GreatBattleGroups GreatBattleGroups { get; set; }
        public virtual GreatBattles GreatBattles { get; set; }
        public virtual Users Users { get; set; }
    }
}
