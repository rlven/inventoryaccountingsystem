//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class SurveyGiftsHistory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SurveyGiftsHistory()
        {
            this.ArchiveSurveyGifts = new HashSet<ArchiveSurveyGifts>();
        }
    
        public int Id { get; set; }
        public int QuestionnairesId { get; set; }
        public int UserId { get; set; }
        public int GiftId { get; set; }
        public bool IsIssued { get; set; }
        public int WhereIssued { get; set; }
        public System.DateTime Date { get; set; }
        public bool IsInArchive { get; set; }
        public System.DateTime DateSendingArchive { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ArchiveSurveyGifts> ArchiveSurveyGifts { get; set; }
        public virtual Questionnaires Questionnaires { get; set; }
        public virtual Users Users { get; set; }
    }
}
