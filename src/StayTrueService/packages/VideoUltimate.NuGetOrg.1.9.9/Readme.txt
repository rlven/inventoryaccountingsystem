VideoUltimate v1.9.9.0
.NET Video Reader and Thumbnailer
Copyright C 2016-2019 GleamTech
https://www.gleamtech.com/videoultimate

Version 1.9.9 Release Notes:
https://docs.gleamtech.com/videoultimate/html/version-history.htm#v1.9.9

Online Documentation:
https://docs.gleamtech.com/videoultimate/

Support Portal:
https://support.gleamtech.com/

------------------------------------------------------------------------------------------------------
To use VideoUltimate in a project, do the following in Visual Studio:
------------------------------------------------------------------------------------------------------

1.  Set VideoUltimate's global configuration. For example, you may want to set the license key 
    Insert the following line into the Application_Start method of your Global.asax.cs for Web projects 
    or Main method for other project types:

    ----------------------------------
    //Set this property only if you have a valid license key, otherwise do not
    //set it so VideoUltimate runs in trial mode.
    VideoUltimateConfiguration.Current.LicenseKey = "QQJDJLJP34...";
    ----------------------------------

    Alternatively you can specify the configuration in <appSettings> tag of your Web.config (or App.exe.config).

    ----------------------------------
    <appSettings>
      <add key="VideoUltimate:LicenseKey" value="QQJDJLJP34..." />
    </appSettings>
    ----------------------------------

    As you would notice, VideoUltimate: prefix maps to VideoUltimateConfiguration.Current.

2.  Open one of your class files (eg. Program.cs) and at the top of your file add GleamTech.VideoUltimate namespace:

    ----------------------------------
    using GleamTech.VideoUltimate;
    ----------------------------------

    Now in some method insert these lines:

    ----------------------------------
    using (var videoFrameReader = new VideoFrameReader(@"C:\Video.mp4"))
    {
        if (videoFrameReader.Read()) //Only if frame was read successfully
        {
            //Get a System.Drawing.Bitmap for the current frame
            //You are responsible for disposing the bitmap when you are finished with it.
            //So it's good practice to have a "using" statement for the retrieved bitmap.
            using (var frame = videoFrameReader.GetFrame())
                //Reference System.Drawing and use System.Drawing.Imaging namespace for the following line.
                frame.Save(@"C:\Frame1.jpg", ImageFormat.Jpeg);
        }
    }
    ----------------------------------

    This will open the source video C:\Video.mp4, read the first frame, and if the frame is read and 
    decoded successfully, it will get a Bitmap instance of the frame and save it as C:\Frame1.jpg.

    Sometimes you may only need to quickly generate a meaningful thumbnail for a video, you can use 
    VideoThumbnailer class for this:
    
    ----------------------------------
    using (var videoThumbnailer = new VideoThumbnailer(@"C:\Video.mp4"))
    //Generate a meaningful thumbnail of the video and
    //get a System.Drawing.Bitmap with 100x100 maximum size.
    //You are responsible for disposing the bitmap when you are finished with it.
    //So it's good practice to have a "using" statement for the retrieved bitmap.
    using (var thumbnail = videoThumbnailer.GenerateThumbnail(100))
        //Reference System.Drawing and use System.Drawing.Imaging namespace for the following line.
        thumbnail.Save(@"C:\Thumbnail1.jpg", ImageFormat.Jpeg);
    ----------------------------------