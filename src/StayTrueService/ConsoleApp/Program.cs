﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;




namespace ConsoleApp
{
    class Program
    {
        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                Console.WriteLine("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        static List<User> GetUsers()
        {
            List<User> list = new List<User>();
            User user = new User();
            user.Id = 1;
            user.FullName = "1";
            user.Address = "1";
            list.Add(user);
            user.Id = 2;
            user.FullName = "2";
            user.Address = "2";
            list.Add(user);
            return list;
        }
        public static System.Data.DataTable ConvertToDataTable<T>(IList<T> data)

        {

            PropertyDescriptorCollection properties =

            TypeDescriptor.GetProperties(typeof(T));

            System.Data.DataTable table = new System.Data.DataTable();

            foreach (PropertyDescriptor prop in properties)

                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            foreach (T item in data)

            {

                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)

                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;

                table.Rows.Add(row);

            }

            return table;

        }
        static void Main(string[] args)
        {
        }
            //    try
            //    {
            //        SqlConnection cnn;
            //        string connectionString = null;
            //        string sql = null;
            //        string data = null;
            //        int i = 0;
            //        int j = 0;

            //        Microsoft.Office.Interop.Excel.Application xlApp;
            //        Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            //        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            //        object misValue = System.Reflection.Missing.Value;

            //        xlApp = new Microsoft.Office.Interop.Excel.Application();
            //        xlWorkBook = xlApp.Workbooks.Add(misValue);
            //        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //        //connectionString = "Data Source=(local);Initial Catalog=StayTrueDB; Integrated Security=True; User Id=sa; Password=1234!@QW;";
            //        //cnn = new SqlConnection(connectionString);
            //        //cnn.Open();
            //        //sql = "SELECT dbo.Users.FirstName, dbo.Users.LastName, dbo.Users.MiddleName, dbo.Users.Login, dbo.Users.Birthdate, dbo.Users.PhoneNumber, dbo.Users.Email, dbo.Cities.CityName, dbo.AccountStatuses.Status,dbo.Users.RegistrationDate, dbo.Users.Wincoins, dbo.Users.PromoCode, dbo.Users.STP, dbo.Users.STM, dbo.Users.LastSignIn FROM dbo.UserMedia INNER JOIN dbo.Users ON dbo.UserMedia.UserId = dbo.Users.Id INNER JOIN dbo.Cities ON dbo.Users.CityId = dbo.Cities.Id INNER JOIN dbo.AccountStatuses ON dbo.Users.StatusId = dbo.AccountStatuses.Id"; ;
            //        //SqlDataAdapter dscmd = new SqlDataAdapter(sql, cnn);
            //        //DataSet ds = new DataSet();
            //        //dscmd.Fill(ds);
            //        System.Data.DataTable ds = ConvertToDataTable(GetUsers());

            //        for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            //        {
            //            for (j = 0; j <= ds.Tables[0].Columns.Count - 1; j++)
            //            {
            //                data = ds.Tables[0].Rows[i].ItemArray[j].ToString();
            //                xlWorkSheet.Cells[i + 1, j + 1] = data;
            //            }
            //        }

            //        xlWorkBook.SaveAs("Tables.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            //        xlWorkBook.Close(true, misValue, misValue);
            //        xlApp.Quit();

            //        releaseObject(xlWorkSheet);
            //        releaseObject(xlWorkBook);
            //        releaseObject(xlApp);

            //        Console.WriteLine("Excel file created , you can find the file c:\\Tables.xls");
            //    }
            //    catch (Exception e)
            //    {
            //        Console.WriteLine("Ошибка: {0}", e);
            //    }


            //}





        }
}
