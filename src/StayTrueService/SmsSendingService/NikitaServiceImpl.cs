﻿using Common.Logger;
using Common.Security;
using DataAccessLayer;
using SmsSendingService.Enums;
using SmsSendingService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace SmsSendingService
{
    public class NikitaServiceImpl
    {

        public static void NotifyVerifiedUsers()
        {
            try
            {    
                using (var entities = new StayTrueDBEntities())
                {
                    var users = entities.Users.Where(u => u.IsApproved && u.StatusId == 1 && String.IsNullOrEmpty(u.Password) && !String.IsNullOrEmpty(u.Phone)).ToList();

                    foreach (var user in users)
                    {
                        string password = PasswordCryptography.GenerateNewPassword();

                        MessageResponseStatuses result = SendMessage(CreateMessage("registerText", user.Phone, password, ConfigurationManager.AppSettings["registerTextExtra"]));

                        if (result == MessageResponseStatuses.Successful)
                        {
                            user.IsApproved = true;
                            user.StatusId = 1; //dbo.AccountStatuses; 1 = Активный
                            user.Password = PasswordCryptography.CreateBCrypt(password);

                            entities.SaveChanges();
                        }
                    }

                    var resetPasswordSupportRequests = entities.SupportRequests.Where(u => u.IsAnswered && u.IsPasswordLost && !u.IsMessageSent).ToList();

                    foreach (var request in resetPasswordSupportRequests)
                    {
                        var usr = entities.Users.Find(request.UserId);

                        if (usr != null && !String.IsNullOrEmpty(usr.Phone))
                        {
                            string password = PasswordCryptography.GenerateNewPassword();

                            MessageResponseStatuses result = SendMessage(CreateMessage("dropPassText", usr.Phone, password, ConfigurationManager.AppSettings["dropPassExtra"]));

                            if (result == MessageResponseStatuses.Successful)
                            {
                                usr.Password = string.Empty;
                                usr.Password = PasswordCryptography.CreateBCrypt(password);
                                request.IsMessageSent = true;
                                entities.SaveChanges();
                            }
                        }
                    }
                }

                using (var context = new StayTrueDBEntities())
                {
                    var activeSendingMessages =
                        context.SmsMessage.FirstOrDefault(s => !s.IsDeleted && s.StatusId == 2 && s.SendTime < DateTime.Now);
                    if (activeSendingMessages != null)
                    {
                        try
                        {
                            var messagesForSent = activeSendingMessages.SmsMessageMsg;
                            if (activeSendingMessages.IsPasswordRequired)
                            {
                                foreach (var message in messagesForSent)
                                {
                                    var user = context.Users.FirstOrDefault(u => u.Phone == message.Phone);
                                    if (user != null)
                                    {
                                        string password = PasswordCryptography.GenerateNewPassword();
                                        var msg = CreateMessage(message, password);
                                        MessageResponseStatuses result = SendMessage(msg);
                                        if (result == MessageResponseStatuses.Successful)
                                        {
                                            user.Password = string.Empty;
                                            message.MessageId = msg.id;
                                            user.Password = PasswordCryptography.CreateBCrypt(password);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (var message in messagesForSent)
                                {
                                    var msg = CreateMessage(message);
                                    MessageResponseStatuses result = SendMessage(msg);
                                    if (result == MessageResponseStatuses.Successful)
                                    {
                                        message.MessageId = msg.id;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error(e.Message);
                            Log.Error(e.StackTrace);
                        }
                        finally
                        {
                            activeSendingMessages.StatusId = 1;
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

        }

        private static MessageResponseStatuses SendMessage(message message)
        {
            string result = String.Empty;

            try
            {
                string sms = XmlSerializator.GetXMLFromObject(message);

                result = SendHttpRequest(sms, "message");

                response response = (response)XmlSerializator.ObjectToXML(result, new response().GetType());

                MessageResponseStatuses status = NikitaResponseStatusCodes.GetResultByCode(response.status);

                if (status == MessageResponseStatuses.Error)
                {
                    Log.Error("An error occurred while sending sms message");
                    Log.Error(result);
                }

                return status;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Log.Error(result);

                return MessageResponseStatuses.Error;
            }
           
        }

        private static string SendHttpRequest(string parameters, string method)
        {
            var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["url"] + method);

            var postData = parameters;

            var data = Encoding.UTF8.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/xml";
            request.ContentLength = data.Length;
            
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }

        private static message CreateMessage(string textKey, string phone, string password, string additionalText)
        {
            List<string> phones = new List<string>();

            string messgeText = ConfigurationManager.AppSettings[textKey];

            messgeText += "\n"+additionalText;

            messgeText += $"\nЛогин: {phone.Substring(3)} \nПароль: {password}";

            phones.Add(phone);

            return new message
            {
                id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
                login = ConfigurationManager.AppSettings["login"],
                pwd = ConfigurationManager.AppSettings["pwd"],
                sender = ConfigurationManager.AppSettings["sender"],
                time = DateTime.Now.ToString("yyyyMMddHHmmss"),
                phones = phones.ToArray(),
                text = messgeText
            };
        }

        private static message CreateMessage(SmsMessageMsg message, string password)
        {
            List<string> phones = new List<string>();

            string messgeText = message.Text;

            messgeText += $"\nЛогин:{message.Phone.Substring(3)}\nПароль:{password}";

            phones.Add(message.Phone);

            return new message
            {
                id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
                login = ConfigurationManager.AppSettings["login"],
                pwd = ConfigurationManager.AppSettings["pwd"],
                sender = ConfigurationManager.AppSettings["sender"],
                time = DateTime.Now.ToString("yyyyMMddHHmmss"),
                phones = phones.ToArray(),
                text = messgeText
            };
        }
        private static message CreateMessage(SmsMessageMsg message)
        {
            List<string> phones = new List<string>();

            string messgeText = message.Text;

            phones.Add(message.Phone);

            return new message
            {
                id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString(),
                login = ConfigurationManager.AppSettings["login"],
                pwd = ConfigurationManager.AppSettings["pwd"],
                sender = ConfigurationManager.AppSettings["sender"],
                time = DateTime.Now.ToString("yyyyMMddHHmmss"),
                phones = phones.ToArray(),
                text = messgeText
            };
        }
    }
}
