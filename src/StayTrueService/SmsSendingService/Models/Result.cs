﻿
namespace SmsSendingService.Models
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://Giper.mobi/schema/Message")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://Giper.mobi/schema/Message", IsNullable = false)]
    public partial class response
    {

        private ulong idField;

        private byte statusField;

        private byte phonesField;

        private byte smscntField;

        /// <remarks/>
        public ulong id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public byte status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public byte phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        public byte smscnt
        {
            get
            {
                return this.smscntField;
            }
            set
            {
                this.smscntField = value;
            }
        }
    }
}
