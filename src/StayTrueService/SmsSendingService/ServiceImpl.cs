﻿using Common.Logger;
using System;
using System.Web.Security;

namespace SmsSendingService
{
    public static class ServiceImpl
    {


        static ServiceImpl()
        {
            //            NikitaServiceImpl.NotifyVerifiedUsers();
        }


        //Public methods

        public static void Run()
        {
            try
            {
                NikitaServiceImpl.NotifyVerifiedUsers();
            }
            catch (Exception ex)
            {
               Log.Error(ex);
            }
            finally
            {
               //Log.Info("Sending was finished");
            }
        }

    }
}

