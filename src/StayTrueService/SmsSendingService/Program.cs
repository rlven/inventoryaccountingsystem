﻿using Common.Logger;
using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace SmsSendingService
{
    static class Program
    {
        static void Main(string[] arg)
        {
            try
            {
                Log.Info("Start Application");

                if (arg.Length != 1)
                {
                    ServiceBase[] servicesToRun =
                    {
                        new Service()
                    };

                    ServiceBase.Run(servicesToRun);
                }
                else
                {
                    new Service().Run();
                }
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
        }

        private static void WriteEventLog(Exception ex)
        {
            const string sourceName = "WindowsService.ExceptionLog";

            if (!EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(sourceName, "Application");
            }

            var eventLog = new EventLog { Source = sourceName };

            var message = $"Exception: {ex.Message} \n\nStack: {ex.StackTrace}";
            eventLog.WriteEntry(message, EventLogEntryType.Error);
        }
    }
}
