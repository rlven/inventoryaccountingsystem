﻿using Common.Logger;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Timer = System.Timers.Timer;

namespace SmsSendingService
{
    public partial class Service : ServiceBase
    {
        private readonly double senderPeriod;
        private Thread senderThread;

        public Service()
        {
            InitializeComponent();
            senderPeriod = Convert.ToDouble(ConfigurationManager.AppSettings["PollingPeriodSec"]);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Log.Info("Start service");

                senderThread = new Thread(InitWaitingTimer);
                senderThread.Start();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }

        private void InitWaitingTimer()
        {
            timer = new Timer();
            timer.Elapsed += WaitingElapsed;
            timer.Interval = senderPeriod * 1000;
            timer.Enabled = true;
        }


        protected override void OnStop()
        {
            try
            {
                Log.Info("Stop service");
                senderThread.Abort();
                
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }

        public void Run()
        {
            Log.Info("Run service.");
            ServiceImpl.Run();
        }


        private void WaitingElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                ServiceImpl.Run();
                timer.Start();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }
    }
}

    
