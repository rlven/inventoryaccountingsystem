﻿using System.ComponentModel;
using System.ServiceProcess;

namespace SmsSendingService
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        private readonly ServiceProcessInstaller processInstaller;
        private readonly ServiceInstaller serviceInstaller;

        public Installer()
        {
            InitializeComponent();

            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            serviceInstaller.ServiceName = "SmsSendingService";
            serviceInstaller.DisplayName = "SmsSendingService";
            serviceInstaller.Description = "Service  for sending sms to StayTrue users";

            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}